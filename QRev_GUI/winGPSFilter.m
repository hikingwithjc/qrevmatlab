function varargout = winGPSFilter(varargin)
% WINGPSFILTER MATLAB code for winGPSFilter.fig
%      WINGPSFILTER, by itself, creates a new WINGPSFILTER or raises the existing
%      singleton*.
%
%      H = WINGPSFILTER returns the handle to a new WINGPSFILTER or the handle to
%      the existing singleton*.
%
%      WINGPSFILTER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINGPSFILTER.M with the given input arguments.
%
%      WINGPSFILTER('Property','Value',...) creates a new WINGPSFILTER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winGPSFilter_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winGPSFilter_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winGPSFilter

% Last Modified by GUIDE v2.5 23-Feb-2016 12:38:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winGPSFilter_OpeningFcn, ...
                   'gui_OutputFcn',  @winGPSFilter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before winGPSFilter is made visible.
function winGPSFilter_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winGPSFilter (see VARARGIN)

% Choose default command line output for winGPSFilter
handles.output = hObject;

    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end

    setWindowPosition(hObject,handles);
    
    commentIcon(handles.pbComment);
    
    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    handles.checkedIdx=find([meas.transects.checked]==1);
    
    % Set reference
    set(handles.txtNavRef,'String',meas.transects(handles.checkedIdx(1)).wVel.navRef);
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Set units for column names
    colNames=get(handles.tblGPS,'ColumnName');
    colNames{10}=[colNames{10},' ',uLabelQ];
    colNames{11}=[colNames{11},' ',uLabelQ];
    set(handles.tblGPS,'ColumnName',colNames); 
    
    % Get filter and interpolation settings. They are same for GGA and
    % VTG
    hasData=[meas.transects(handles.checkedIdx).boatVel];
    if ~isempty([hasData.ggaVel])
        hasData=[hasData.ggaVel];
    elseif ~isempty([hasData.vtgVel])
        hasData=[hasData.vtgVel];
    else
        hasData.gpdDiffQualFilter=1;
        hasData.gpsAltitudeFilter='Off';
        hasData.gpsHDOPFilter='Off';
        hasData.smoothFilter='Off';
        hasData.interpolate='Linear';
    end
 
    diffQualFilter=hasData(1).gpsDiffQualFilter;
    altitudeFilter=hasData(1).gpsAltitudeFilter;
    hdopFilter=hasData(1).gpsHDOPFilter;
    smoothFilter=hasData(1).smoothFilter;
    interpolate=hasData(1).interpolate;

    % Differential quality filter
    switch diffQualFilter
        case 1
            set(handles.puDQ,'Value',1);
        case 2
            set(handles.puDQ,'Value',2);
        case 4
            set(handles.puDQ,'Value',3);
    end
    
    % Altitude filter  
    switch altitudeFilter
        case 'Off'          
            set(handles.puAltitude,'Value',3); 
            set(handles.edAltitude,'Enable','off');
        case 'Manual'
            set(handles.puAltitude,'Value',2);
            set(handles.edAltitude,'Enable','on');
            set(handles.edAltitude,'String',num2str(meas.transects(handles.checkedIdx(1)).boatVel.ggaVel.gpsAltitudeFilterChange.*unitsL,'%4.1f'));
        case 'Auto'
            set(handles.puAltitude,'Value',1); 
            set(handles.edAltitude,'Enable','off');
    end % switch
    
    % HDOP filter  
    switch hdopFilter
        case 'Off'
            set(handles.puHDOP,'Value',3); 
            set(handles.edHDOPMax,'Enable','off');
            set(handles.edHDOPChng,'Enable','off');
        case 'Manual'
            set(handles.puHDOP,'Value',2); 
            set(handles.edHDOPMax,'Enable','on');
            set(handles.edHDOPMax,'String',num2str(meas.transects(handles.checkedIdx(1)).boatVel.ggaVel.gpsHDOPFilterMax,'%4.1f'));
            set(handles.edHDOPChng,'Enable','on');
            set(handles.edHDOPChng,'String',num2str(meas.transects(handles.checkedIdx(1)).boatVel.ggaVel.gpsHDOPFilterChange,'%4.1f'));
        case 'Auto'
            set(handles.puHDOP,'Value',1) ;
            set(handles.edHDOPMax,'Enable','off');
            set(handles.edHDOPChng,'Enable','off');
    end % switch
    
    % Other filter settings
    switch smoothFilter
        case 'On'
            set(handles.puOther,'Value',2);
        case 'Off'
            set(handles.puOther,'Value',1);
    end
    
    % Interpolation settings  
    set(handles.puInterp,'Visible',getappdata(handles.hMainGui,'InterpVisible'));
    set(handles.uipanelInterp,'Visible',getappdata(handles.hMainGui,'InterpVisible'));
    switch interpolate
        case 'None'
            set(handles.puInterp,'Value',1)
        case 'ExpandedT'
            set(handles.puInterp,'Value',2);
        case 'Hold9'
            set(handles.puInterp,'Value',3);
        case 'HoldLast'
            set(handles.puInterp,'Value',4);
        case 'Smooth'
            set(handles.puInterp,'Value',5);
        case 'Linear'
            set(handles.puInterp,'Value',6);
    end
    
    % Check for HDOP and Number of Satellites data availability
    nTransects=length(handles.checkedIdx);
    for n=1:nTransects
        if ~isempty(meas.transects(handles.checkedIdx(n)).gps)
            nSats(n)=nansum(abs(meas.transects(handles.checkedIdx(n)).gps.numSatsEns));
            nHDOP(n)=nansum(abs(meas.transects(handles.checkedIdx(n)).gps.hdopEns));
        else
            nSats(n)=0;
            nHDOP(n)=0;
        end
    end % for n
    
    % If there is no data for number of satellites disable button
    if sum(nSats)>0
        set(handles.rbSats,'Enable','on');
    else
        set(handles.rbSats,'Enable','off');
    end % if nSats
    
    % If there is no data for HDOP disable button
    if sum(nHDOP)>0
        set(handles.rbHDOP,'Enable','on');
        set(handles.puHDOP,'Enable','on');
    else
        set(handles.rbHDOP,'Enable','off');
        set(handles.puHDOP,'Enable','off');
        set(handles.edHDOPMax,'Enable','off');
        set(handles.edHDOPChng,'Enable','off');
        
    end % if HDOP
    handles.plottedTransect=1;
    updateData(handles,meas);
    updateGraphics(handles);
    drawnow;
    
% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = winGPSFilter_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function tblGPS_CellSelectionCallback(hObject, eventdata, handles)
% Handles file selection to be plotted. Allows the selection to work as
% radio buttons so that only one file can be selected

    % Determine row selected
    selection = eventdata.Indices(:,1);
       
    % If statement due to code executed twice. Not sure why but this fixes
    % the problem.
    if ~isempty(selection)
        % Get table data
        tableData=get(handles.tblGPS,'Data');            
        handles.plottedTransect=selection;
        set(handles.plottedtxt,'String',tableData{selection,1});
    end
    % Update graphics
    updateGraphics(handles)
    % Update handles structure
    guidata(hObject, handles);
    
function pbComment_Callback(hObject, eventdata, handles)
% Add comment
    [handles]=commentButton(handles, 'GPS');
    % Update handles structure
    guidata(hObject, handles);    
    
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%open('helpFiles\QRev_Users_Manual.pdf')      
  web('QRev_Help_Files\HTML\gps_filters.htm')  
function pbClose_Callback(hObject, eventdata, handles)
    delete (handles.output);  
    
function figure1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

% ALT-g to plot to Google Earth
if strcmp(eventdata.Key,'g') && strcmp(eventdata.Modifier,'alt')
    
    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Initialize
    pointstr=['http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'];
    a=0;
    nTransects=length(handles.checkedIdx); 
     
    % Process transects
    for n=1:nTransects
        
        % Check so to see if GGA data present
        if ~isempty(meas.transects(handles.checkedIdx(n)).gps.ggaLonEns_deg)
            lon=meas.transects(handles.checkedIdx(n)).gps.ggaLonEns_deg';
            lat=meas.transects(handles.checkedIdx(n)).gps.ggaLatEns_deg';
            idx=find(meas.transects(handles.checkedIdx(n)).fileName=='\');
            if ~isempty(idx)
                name2=meas.transects(handles.checkedIdx(n)).fileName(idx(end)+1:end-4);
            else
                name2=meas.transects(handles.checkedIdx(n)).fileName(1:end-4);
            end
            connect = diag(ones(size(lon,1),1)) + diag(ones(size(lon,1)-1,1),1);
            temp = ge_gplot(connect,[lon, lat]);
            klmStr{n}=ge_folder(name2,temp);
            klmStr2{n}=ge_point(nanmean(lon),nanmean(lat),0,'name',name2);
            a=a+1;
            klmStr3{a}=klmStr{n};
            klmStr4{a}=klmStr2{n};
        end % if empty
    end
    
    % Proceed if GGA data were processed
    if a>0
        prefPath=getUserPref('Folder');
        prefix=datestr(now,'yyyymmddHHMMSS');
        kmlName=[prefPath prefix '_QRev.kml'];
        trans=ge_folder('Transect',[klmStr{:}]);
        loc=ge_folder('Location',[klmStr2{:}]);
        ge_output(kmlName,[trans, loc],'name',kmlName(1:end-4));
        clear klmStr klmStr2 trans loc
        winopen(kmlName);
    end % if a
end    

% Filters

function puDQ_Callback(hObject, eventdata, handles)
    
% Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get data from GUI
    setting=get(handles.puDQ,'Value');
       
    % Set selected setting
    switch setting
        case 1 % Autonomus
            s.ggaDiffQualFilter=1;
                            
        case 2 % Differential
            s.ggaDiffQualFilter=2;
            
        case 3 % RTK
            s.ggaDiffQualFilter=4;
    end

    % Update data and GUI
    updateData(handles,meas,s);
    updateGraphics(handles)
    drawnow 
    guidata(hObject, handles);

function puAltitude_Callback(hObject, eventdata, handles)
    
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get data from GUI
    setting=get(handles.puAltitude,'Value');
        
    % Apply selected setting
    switch setting
        case 1 % Auto
            s.ggaAltitudeFilter='Auto';
            set(handles.edAltitude,'Enable','off')
                            
        case 2 % Manual
            s.ggaAltitudeFilter='Manual';
            set(handles.edAltitude,'Enable','on');
            if isempty(s.ggaAltitudeFilterChange)
                s.ggaAltitudeFilterChange=3;
            end
            
        case 3 % No filter applies
            s.ggaAltitudeFilter='Off';
            set(handles.edAltitude,'Enable','off')
    end
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Update display
    set(handles.edAltitude,'String',num2str(s.ggaAltitudeFilterChange.*unitsL,'%4.1f'));

    % Update data and GUI
    updateData(handles,meas,s);
    updateGraphics(handles)
    drawnow 
    guidata(hObject, handles);

function edAltitude_Callback(hObject, eventdata, handles)
    
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Get data from GUI
    setting=str2num(get(handles.edAltitude,'String'));
    
    s.ggaAltitudeFilterChange=setting./unitsL;
            
    % Update display
    set(handles.edAltitude,'String',num2str(setting,'%4.1f'));

    % Update data and GUI
    updateData(handles,meas,s);
    updateGraphics(handles)
    drawnow 
    guidata(hObject, handles);
    
function puHDOP_Callback(hObject, eventdata, handles)
    
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get data from GUI
    setting=get(handles.puHDOP,'Value');
    
    % Apply selected setting
    switch setting
        case 1 % Auto
            s.GPSHDOPFilter='Auto';
            set(handles.edHDOPMax,'Enable','off')
            set(handles.edHDOPChng,'Enable','off')
                            
        case 2 % Manual
            set(handles.edHDOPMax,'Enable','on')
            set(handles.edHDOPChng,'Enable','on')
            s.GPSHDOPFilter='Manual';
            if isempty(s.GPSHDOPFilterMax)
                s.GPSHDOPFilterMax=2;
            end
            if isempty(s.GPSHDOPFilterChange)
                s.GPSHDOPFilterChange=1;
            end

        case 3 % No filter applies
            s.GPSHDOPFilter='Off';
            set(handles.edHDOPMax,'Enable','off')
            set(handles.edHDOPChng,'Enable','off')
    end
    
    % Update display
    set(handles.edHDOPChng,'String',num2str(s.GPSHDOPFilterChange,'%4.1f'));
    set(handles.edHDOPMax,'String',num2str(s.GPSHDOPFilterMax,'%4.1f'));

    % Update data and GUI
    updateData(handles,meas,s);
    updateGraphics(handles)
    drawnow 
    guidata(hObject, handles);

function edHDOPMax_Callback(hObject, eventdata, handles)
    
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get data from GUI
    s.GPSHDOPFilterMax=str2num(get(handles.edHDOPMax,'String'));
    s.GPSHDOPFilterChange=str2num(get(handles.edHDOPChng,'String'));
            
    % Update display
    set(handles.edHDOPChng,'String',num2str(s.GPSHDOPFilterChange,'%4.1f'));
    set(handles.edHDOPMax,'String',num2str(s.GPSHDOPFilterMax,'%4.1f'));
    
    % Update data and GUI
    updateData(handles,meas,s);
    updateGraphics(handles);
    drawnow 
    guidata(hObject, handles);

function edHDOPChng_Callback(hObject, eventdata, handles)
    
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get data from GUI
    s.GPSHDOPFilterMax=str2num(get(handles.edHDOPMax,'String'));
    s.GPSHDOPFilterChange=str2num(get(handles.edHDOPChng,'String'));
            
    % Update display
    set(handles.edHDOPChng,'String',num2str(s.GPSHDOPFilterChange,'%4.1f'));
    set(handles.edHDOPMax,'String',num2str(s.GPSHDOPFilterMax,'%4.1f'));
    
    % Update data and GUI
    updateData(handles,meas,s);
    updateGraphics(handles)
    drawnow 
    guidata(hObject, handles);

function puOther_Callback(hObject, eventdata, handles)
% Processes request for smooth or other filter
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get data from GUI
    setting=get(handles.puOther,'Value');

    % Apply filter requested
    switch setting
        case 1 % Off
            s.GPSSmoothFilter='Off';  
                                     
        case 2 % On
            s.GPSSmoothFilter='On'; 
    end

    % Update data and GUI
    updateData(handles,meas,s);
    updateGraphics(handles)
    drawnow 
    guidata(hObject, handles);

% Interpolation

function puInterp_Callback(hObject, eventdata, handles)
% Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get data from GUI
    setting=get(handles.puInterp,'Value');
    
    % Apply selected interpolation method
    switch setting
        case 1 % None
            % Sets invalid data to nan with no interpolation
            s.GPSInterpolation='None';
                        
        case 2 % Expanded Ensemble Time
            % Set interpolate to None as the interpolation is done in the
            % clsQComp
            s.GPSInterpolation='ExpandedT';
                        
        case 3 % SonTek Method
            % Interpolates using SonTeks method of holding last valid for
            % up to 9 samples.
            s.GPSInterpolation='Hold9';
            
        case 4 % Hold Last Valid
            % Interpolates by holding last valid indefinitely
            s.GPSInterpolation='HoldLast';
            
        case 5 % Smooth
            % Interpolates using a robust Loess smooth
            s.GPSInterpolation='Smooth';
            
        case 6 % Linear
            % Interpolates using linear interpolation
            s.GPSInterpolation='Linear';
    end % switch
    
    % Update data and GUI
    updateData(handles,meas,s);
    updateGraphics(handles)
    drawnow 
    guidata(hObject, handles);          
 
% Graphics

function updateGraphics(handles)
    topPanel_SelectionChangeFcn(nan,nan,handles);
    speedPlot(handles);
    shiptrackPlot(handles);
    linkaxes([handles.axBottom,handles.axTop],'x');
   
% Top Plot

function topPanel_SelectionChangeFcn(hObject, eventdata, handles)
   
    tableData=get(handles.tblGPS,'Data');
    
    radioTag=get(get(handles.topPanel,'SelectedObject'),'Tag');
    
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    transectSelected=handles.checkedIdx(handles.plottedTransect);
    if isempty(transectSelected)
        transectSelected=1;
    end
    set(handles.plottedtxt,'String',tableData{handles.plottedTransect,1});
    transect=meas.transects(transectSelected);
    cla(handles.axTop)
    if ~isempty(transect.boatVel.ggaVel)

        validData=transect.boatVel.ggaVel.validData;

        % Generate selected plot
        ensembles=1:length(transect.gps.diffQualEns);
        switch radioTag
            case 'rbDQ'
                plot(handles.axTop,ensembles,transect.gps.diffQualEns,'*b')
                hold (handles.axTop,'on')
                plot(handles.axTop,ensembles(~validData(3,:)),transect.gps.diffQualEns(~validData(3,:)),'or')
                ylabel(handles.axTop,'Differential Quality')
            case 'rbAlt'
                plot(handles.axTop,ensembles,transect.gps.altitudeEns_m.*unitsL,'*b')
                hold (handles.axTop,'on')
                plot(handles.axTop,ensembles(~validData(4,:)),transect.gps.altitudeEns_m(~validData(4,:)).*unitsL,'or')
                ylabel(handles.axTop,['Altitude ',uLabelL])
            case 'rbHDOP'
                plot(handles.axTop,ensembles,transect.gps.hdopEns,'*b')
                hold (handles.axTop,'on')
                plot(handles.axTop,ensembles(~validData(6,:)),transect.gps.hdopEns(~validData(6,:)),'or')
                ylabel(handles.axTop,'HDOP')
            case 'rbSats'
                plot(handles.axTop,ensembles,transect.gps.numSatsEns,'*b')
                ylabel(handles.axTop,'Number of Satellites')
        end % switch 
        grid(handles.axTop,'On')
        hold (handles.axTop,'off')
        if strcmpi(transect.startEdge,'Right')
            set(handles.axTop,'XDir','reverse');
        else
            set(handles.axTop,'XDir','normal');
        end
    end
    
function rbHDOP_Callback(hObject, eventdata, handles)

function rbSats_Callback(hObject, eventdata, handles)

function rbAlt_Callback(hObject, eventdata, handles)

function rbDQ_Callback(hObject, eventdata, handles)

% Bottom Plot

function speedPlot(handles)
    cla(handles.axBottom)
    
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    transectSelected=handles.checkedIdx(handles.plottedTransect);
    if isempty(transectSelected)
        transectSelected=1;
    end
    transect=meas.transects(transectSelected);
    
    ensembles=1:length(transect.boatVel.btVel.u_mps);
    hold(handles.axBottom,'on');
    
    if get(handles.cbSpdGGA,'Value')
        if ~isempty(transect.boatVel.ggaVel)
            speed=sqrt(transect.boatVel.ggaVel.u_mps.^2+transect.boatVel.ggaVel.v_mps.^2);
            speedProcessed=sqrt(transect.boatVel.ggaVel.uProcessed_mps.^2+transect.boatVel.ggaVel.vProcessed_mps.^2);
            plot(handles.axBottom,ensembles,speed.*unitsV,':b')
            plot(handles.axBottom,ensembles,speedProcessed.*unitsV,'-b')
            validData=transect.boatVel.ggaVel.validData;
            if sum(validData(1,:))<length(ensembles)
                plot(handles.axBottom,ensembles(~validData(1,:)),0,'ob')
            end % if sum
        end
    end 

    if get(handles.cbSpdVTG,'Value')
        if ~isempty(transect.boatVel.vtgVel)
            speed=sqrt(transect.boatVel.vtgVel.u_mps.^2+transect.boatVel.vtgVel.v_mps.^2);
            speedProcessed=sqrt(transect.boatVel.vtgVel.uProcessed_mps.^2+transect.boatVel.vtgVel.vProcessed_mps.^2);
            plot(handles.axBottom,ensembles,speed.*unitsV,':','Color',[0 0.498 0])
            plot(handles.axBottom,ensembles,speedProcessed.*unitsV,'-','Color',[0 0.498 0])
            validData=transect.boatVel.vtgVel.validData;
            if sum(validData(1,:))<length(ensembles)
                plot(handles.axBottom,ensembles(~validData(1,:)),0,'o','Color',[0 0.498 0])
            end % if sum
        end
    end 

    if get(handles.cbSpdBT,'Value')
        speed=sqrt(transect.boatVel.btVel.u_mps.^2+transect.boatVel.btVel.v_mps.^2);
        speedProcessed=sqrt(transect.boatVel.btVel.uProcessed_mps.^2+transect.boatVel.btVel.vProcessed_mps.^2);
        plot(handles.axBottom,ensembles,speed.*unitsV,':r')
        plot(handles.axBottom,ensembles,speedProcessed.*unitsV,'-r')    
    end
    
    xlabel(handles.axBottom,'Ensemble')
    ylabel(handles.axBottom,['Boat Speed ',uLabelV])
    grid(handles.axBottom,'On')
    hold (handles.axBottom, 'off')
    if strcmpi(transect.startEdge,'Right')
        set(handles.axBottom,'XDir','reverse');
    else
        set(handles.axBottom,'XDir','normal'); 
    end
    
function cbSpdGGA_Callback(hObject, eventdata, handles)
    speedPlot(handles)

function cbSpdVTG_Callback(hObject, eventdata, handles)
    speedPlot(handles)

function cbSpdBT_Callback(hObject, eventdata, handles)
    speedPlot(handles)

% ShipTrack

function shiptrackPlot(handles)
    cla(handles.axShipTrack)

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');

    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    transectSelected=handles.checkedIdx(handles.plottedTransect);
    if isempty(transectSelected)
        transectSelected=1;
    end
    transect=meas.transects(transectSelected);
    

    % Compute track
    ensDuration=transect.dateTime.ensDuration_sec;
    % BT
    btUProcessed=transect.boatVel.btVel.uProcessed_mps;
    btVProcessed=transect.boatVel.btVel.vProcessed_mps;
    btXProcessed=nancumsum(btUProcessed.*ensDuration);
    btYProcessed=nancumsum(btVProcessed.*ensDuration);
    % GGA
    if ~isempty(transect.boatVel.ggaVel)
        ggaUProcessed=transect.boatVel.ggaVel.uProcessed_mps;
        ggaVProcessed=transect.boatVel.ggaVel.vProcessed_mps;
        ggaXProcessed=nancumsum(ggaUProcessed.*ensDuration);
        ggaYProcessed=nancumsum(ggaVProcessed.*ensDuration);
    end
    % VTG
    if ~isempty(transect.boatVel.vtgVel)
        vtgUProcessed=transect.boatVel.vtgVel.uProcessed_mps;
        vtgVProcessed=transect.boatVel.vtgVel.vProcessed_mps;
        vtgXProcessed=nancumsum(vtgUProcessed.*ensDuration);
        vtgYProcessed=nancumsum(vtgVProcessed.*ensDuration);
    end

    % Compute water vectors for select reference
    waterRef=transect.wVel.navRef;
    wVelX=transect.wVel.uProcessed_mps;
    wVelY=transect.wVel.vProcessed_mps;
    meanVelX=nanmean(wVelX);
    meanVelY=nanmean(wVelY);
     
    % Plot shiptrack
    hold(handles.axShipTrack,'on')
    
    % Plot BT
    if get(handles.cbShipBT,'Value') 
        plotted(1,1)=1;
        plot(handles.axShipTrack,btXProcessed.*unitsL,btYProcessed.*unitsL,'-r','LineWidth',2)
        % Only plot vectors for selected water track nav reference
        if strcmp(waterRef,'BT') && get(handles.cbShipVectors,'Value')
            plotted(2,1)=1;
            quiver(handles.axShipTrack,btXProcessed.*unitsL,btYProcessed.*unitsL,meanVelX.*unitsV,meanVelY.*unitsV,'k','ShowArrowHead','on','AutoScaleFactor',5);
        else
            plotted(2,1)=0;
        end % if
    else
        plotted(1,1)=0;
        plotted(2,1)=0;
    end % if
    
    % Plot VTG
    if ~isempty(transect.boatVel.vtgVel)
        if get(handles.cbShipVTG,'Value')
            plotted(3,1)=1;
            plot(handles.axShipTrack,vtgXProcessed.*unitsL,vtgYProcessed.*unitsL,'-','Color',[0 0.498 0],'LineWidth',2)
            % Only plot vectors for selected water track nav reference
            if strcmp(waterRef,'VTG') && get(handles.cbShipVectors,'Value') 
                plotted(4,1)=1;
                quiver(handles.axShipTrack,vtgXProcessed.*unitsL,vtgYProcessed.*unitsL,meanVelX.*unitsV,meanVelY.*unitsV,'k','ShowArrowHead','on','AutoScaleFactor',5);
            else
                plotted(4,1)=0;
            end % if
        else
            plotted(3,1)=0;
            plotted(4,1)=0;
        end % if
    end
    
    % Plot GGA
    if ~isempty(transect.boatVel.ggaVel)
        if get(handles.cbShipGGA,'Value') 
            plotted(5,1)=1;
            plot(handles.axShipTrack,ggaXProcessed.*unitsL,ggaYProcessed.*unitsL,'-b','LineWidth',2)
            % Only plot vectors for selected water track nav reference
            if strcmp(waterRef,'GGA') && get(handles.cbShipVectors,'Value')
                plotted(6,1)=1;
                quiver(handles.axShipTrack,ggaXProcessed.*unitsL,ggaYProcessed.*unitsL,meanVelX.*unitsV,meanVelY.*unitsV,'k','ShowArrowHead','on','AutoScaleFactor',5);
            else
                plotted(6,1)=0;
            end % if
        else
            plotted(5,1)=0;
            plotted(6,1)=0;
        end % if 
    end
    validData=[0 0 0 0 0 0]';
    if ~isempty(transect.boatVel.ggaVel)
        validData=transect.boatVel.ggaVel.validData;
        if get(handles.cbShipGGA,'Value') 
            plot(handles.axShipTrack,ggaXProcessed(~validData(2,:)).*unitsL,ggaYProcessed(~validData(2,:)).*unitsL,'+r');
            plot(handles.axShipTrack,ggaXProcessed(~validData(3,:)).*unitsL,ggaYProcessed(~validData(3,:)).*unitsL,'*r');
            plot(handles.axShipTrack,ggaXProcessed(~validData(4,:)).*unitsL,ggaYProcessed(~validData(4,:)).*unitsL,'or');
            plot(handles.axShipTrack,ggaXProcessed(~validData(5,:)).*unitsL,ggaYProcessed(~validData(5,:)).*unitsL,'ob');
        end
    end
    plottedGGA=logical(sum(~validData(2:6,:),2));
    if ~isempty(transect.boatVel.vtgVel)
        validData=transect.boatVel.vtgVel.validData;
        if get(handles.cbShipVTG,'Value')
            plot(handles.axShipTrack,vtgXProcessed(~validData(2,:)).*unitsL,vtgYProcessed(~validData(2,:)).*unitsL,'+r');
            plot(handles.axShipTrack,vtgXProcessed(~validData(5,:)).*unitsL,vtgYProcessed(~validData(5,:)).*unitsL,'ob');
            plot(handles.axShipTrack,vtgXProcessed(~validData(6,:)).*unitsL,vtgYProcessed(~validData(6,:)).*unitsL,'sr');
        end
    end
    plottedVTG=logical(sum(~validData(2:6,:),2));
    plottedAll=any([plottedVTG, plottedGGA],2);
    plotted=logical([plotted;plottedAll]);
    hold(handles.axShipTrack,'off')
    set(handles.axShipTrack,'DataAspectRatio',[1 1 1]);
    set(handles.axShipTrack,'Box','on');
    set(handles.axShipTrack,'PlotBoxAspectRatio',[1 1 1]);
    legendText={'BT','Water Vector','VTG','Water Vector','GGA','Water Vector','Invalid Raw','Differential Quality','Altitude','Smooth','HDOP'};
    legend(handles.axShipTrack,legendText{plotted},'Location','best')    
    xlabel(handles.axShipTrack,['East Distance ',uLabelL]);
    ylabel(handles.axShipTrack,['North Distance ',uLabelL]);
    grid(handles.axShipTrack,'On')

function cbShipGGA_Callback(hObject, eventdata, handles)
    shiptrackPlot(handles)

function cbShipVTG_Callback(hObject, eventdata, handles)
    shiptrackPlot(handles)

function cbShipBT_Callback(hObject, eventdata, handles)
    shiptrackPlot(handles)

function cbShipVectors_Callback(hObject, eventdata, handles)
    shiptrackPlot(handles)

% Table & Data

function updateData(handles,meas,varargin)

    % Get previous and compute new discharge
    dischargeOld=meas.discharge;
    if ~isempty(varargin)
        oldpointer = get(gcf, 'pointer');      
        set(gcf, 'pointer', 'watch');     
        drawnow; 
        meas=applySettings(meas,varargin{1});         
        set(gcf, 'pointer', oldpointer);     
        drawnow;  
    end
    
    % Get table from gui
    tableData=get(handles.tblGPS,'Data');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Get data for table from each transect
    nTransects=length(handles.checkedIdx);
    
    for n=1:nTransects
    % Get data needed to populate the table
        % HDOP
        if ~isempty(meas.transects(handles.checkedIdx(n)).gps)
            HDOP{n}=meas.transects(handles.checkedIdx(n)).gps.hdopEns;
            meanHDOP=nanmean(HDOP{n});
            maxDiffHDOP=nanmax(abs(HDOP{n}-meanHDOP));
            % Satellites
            sats{n}=meas.transects(handles.checkedIdx(n)).gps.numSatsEns;
            if nansum(sats{n})==0
                numSatChanges=nan;
            else
                nsats=sats{n};
                nsats=nsats(~isnan(nsats));
                diffSats=diff(nsats);
                numSatChanges=nansum(diffSats~=0);
            end % if sats
            % Differential Quality
            diffQual{n}=meas.transects(handles.checkedIdx(n)).gps.diffQualEns;
            % Altitude
            altitude{n}=meas.transects(handles.checkedIdx(n)).gps.altitudeEns_m;
            meanAlt=nanmean(altitude{n});
            maxDiffAlt=nanmax(abs(altitude{n}-meanAlt));
            % Valid Data
            if ~isempty(meas.transects(handles.checkedIdx(n)).boatVel.ggaVel)
                ggaValidData=meas.transects(handles.checkedIdx(n)).boatVel.ggaVel.validData;
            else
                ggaValidData=[];
            end
            if ~isempty(meas.transects(handles.checkedIdx(n)).boatVel.vtgVel)
                vtgValidData=meas.transects(handles.checkedIdx(n)).boatVel.vtgVel.validData;
            else
                vtgValidData=[];
            end
        else
            HDOP{n}=nan;
            meanHDOP=[];
            maxDiffHDOP=nan;
            sats{n}=[];
            numSatChanges=nan;
            diffQual{n}=nan;
            altitude{n}=[];
            meanAlt=0;
            maxDiffAlt=[];
            ggaValidData=[];
            vtgValidData=[];
        end
        % Populate the table
        idx=find(meas.transects(handles.checkedIdx(n)).fileName=='\',1,'last');
        if isempty(idx)
            idx=0;
        end
        tableData{n,1}=meas.transects(handles.checkedIdx(n)).fileName(idx+1:end);
        if ~isempty(ggaValidData)
            nens=size(ggaValidData,2);
        elseif ~isempty(vtgValidData)
            nens=size(vtgValidData,2);
        else
            nens=0;
        end
        tableData{n,2}=sprintf('% 10.0f',nens);  
        if isempty(ggaValidData)
            tableData{n,3}=sprintf('   N/A'); 
        else
            tableData{n,3}=sprintf('% 6.0f',nansum(~ggaValidData(1,:)));
        end
        if isempty(vtgValidData)
            tableData{n,4}=sprintf('   N/A'); 
        else
            tableData{n,4}=sprintf('% 6.0f',nansum(~vtgValidData(1,:)));
        end
        temp=unique(diffQual{n});
        temp=temp(~isnan(temp));
        if isempty(temp)
             tableData{n,5}=sprintf('   N/A');
        else
            tableData{n,5}= sprintf('% 4d ',temp);
        end
        if isempty(maxDiffAlt)
             tableData{n,6}=sprintf('   N/A'); 
        else
            tableData{n,6}= sprintf('% 12.2f',maxDiffAlt.*unitsL);
        end
        if isnan(nanmax(HDOP{n}))
           tableData{n,7}= sprintf('   N/A'); 
        else
           tableData{n,7}= sprintf('% 14.1f',nanmax(HDOP{n}));
        end % if HDOP
        if isnan(maxDiffHDOP)
           tableData{n,8}= sprintf('   N/A');
        else
            tableData{n,8}= sprintf('% 14.1f',maxDiffHDOP);
        end % if maxDiffHDOP
        if isnan(numSatChanges)
            tableData{n,9}= sprintf('   N/A');
        else
            tableData{n,9}= sprintf('% 12.0f',numSatChanges);
        end % if numSatChanges
        tableData{n,10}=sprintf('% 12.2f',dischargeOld(handles.checkedIdx(n)).total.*unitsQ);
        tableData{n,11}=sprintf('% 12.2f',meas.discharge(handles.checkedIdx(n)).total.*unitsQ);
        tableData{n,12}=sprintf('% 12.2f',((meas.discharge(handles.checkedIdx(n)).total-dischargeOld(handles.checkedIdx(n)).total)./dischargeOld(handles.checkedIdx(n)).total).*100);
    end % for n
    tableData=applyFormat(handles,tableData,meas);
    % Update table
    set(handles.tblGPS,'Data',tableData(1:nTransects,:));

    % Store data
    setappdata(handles.hMainGui,'measurement',meas);

function tableData=applyFormat(handles,tableData,meas)
% Applys a format to the table that highlights values that have generated
% warnings or caution about the quality of the data and its affect on Q.
%
% INPUT:
%
% handles: handles data structure from figure
%
% tableData: data from table to be formatted
%
% meas: object of clsMeasurement
%
% OUTPUT:
%
% tableData: data for table that has been formatted.

    % Combine caution notices into single matrix
    cautionGGA=meas.qa.ggaVel.qTotalCaution(handles.checkedIdx,:) | meas.qa.ggaVel.qRunCaution(handles.checkedIdx,:);
    
    % Combine warning notices into single matrix
    warningGGA=meas.qa.ggaVel.qRunWarning(handles.checkedIdx,:) | meas.qa.ggaVel.qTotalWarning(handles.checkedIdx,:);
    
    % Combine caution notices into single matrix
    cautionVTG=meas.qa.vtgVel.qTotalCaution(handles.checkedIdx,:) | meas.qa.vtgVel.qRunCaution(handles.checkedIdx,:);
    
    % Combine warning notices into single matrix
    warningVTG=meas.qa.vtgVel.qRunWarning(handles.checkedIdx,:) | meas.qa.vtgVel.qTotalWarning(handles.checkedIdx,:);
    
    % Rearrange matrix to match table
    caution=[cautionGGA(:,1),cautionVTG(:,1),cautionGGA(:,3:4),cautionGGA(:,6),cautionGGA(:,6)];
    warning=[warningGGA(:,1),warningVTG(:,1),warningGGA(:,3:4),warningGGA(:,6),warningGGA(:,6)];
    
    % Combine caution and warning into single matrix
    code=double(caution);
    code(warning)=2;
    
    % Creat idx of cells to apply caution and warning formats
    idxCaution=find(code==1);
    idxWarning=find(code==2);
    
    % Get subset of table to be formatted
    nTransects=length(handles.checkedIdx);
    tempData=tableData(1:nTransects,3:8);
    
    % Apply caution format
    for n=1:length(idxCaution)
        tempData(idxCaution(n))={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tempData{idxCaution(n)}),'</center></BODY></HTML>']};
    end
    
    % Apply warning format
    for n=1:length(idxWarning)
        tempData(idxWarning(n))={['<HTML><BODY bgColor="red" width="50px"><strong><center>',num2str(tempData{idxWarning(n)}),'</center></strong></BODY></HTML>']};
    end
    
    % Put formatted cells back in table
    tableData(1:nTransects,3:8)=tempData;    
    
% --- Executes on key press with focus on tblGPS and none of its controls.
function tblGPS_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to tblGPS (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)    

% Make table inactive to avoid synching issues associated with multiple
% keypresses before the processing of the first can be completed.
set(handles.tblGPS,'Enable','inactive');

if strcmp(eventdata.Key,'uparrow') || strcmp(eventdata.Key,'downarrow')
    tableData=get(handles.tblGPS,'Data');
    rowTrue=handles.plottedTransect;
    if strcmp(eventdata.Key,'downarrow')
        if rowTrue+1<=size(tableData,1)
            newRow=rowTrue+1;          
        else
            newRow=size(tableData,1);
        end
    elseif strcmp(eventdata.Key,'uparrow')
        if rowTrue-1>0
            newRow=rowTrue-1;
        else
            newRow=1;
        end
    end
      
    set(handles.plottedtxt,'String',tableData{newRow,1});
    handles.plottedTransect=newRow;

    drawnow

    % Update graphics
    updateGraphics(handles)
    % Update handles structure
    guidata(hObject, handles);
    drawnow
end
set(handles.tblGPS,'Enable','on');

%==========================================================================
%================NO MODIFIED CODE BELOW HERE===============================
%==========================================================================

function edit1_Callback(hObject, eventdata, handles)

function edit1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit2_Callback(hObject, eventdata, handles)

function edit2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit3_Callback(hObject, eventdata, handles)

function edit3_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function puInterp_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function puOther_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edHDOPMax_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function puAltitude_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edAltitude_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function puDQ_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function puHDOP_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edHDOPChng_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end






% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

