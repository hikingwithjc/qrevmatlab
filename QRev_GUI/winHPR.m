function varargout = winHPR(varargin)
% WINHPR MATLAB code for winHPR.fig
%      WINHPR, by itself, creates a new WINHPR or raises the existing
%      singleton*.
%
%      H = WINHPR returns the handle to a new WINHPR or the handle to
%      the existing singleton*.
%
%      WINHPR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINHPR.M with the given input arguments.
%
%      WINHPR('Property','Value',...) creates a new WINHPR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winHPR_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winHPR_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winHPR

% Last Modified by GUIDE v2.5 22-May-2017 13:03:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winHPR_OpeningFcn, ...
                   'gui_OutputFcn',  @winHPR_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before winHPR is made visible.
function winHPR_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winHPR (see VARARGIN)

% Choose default command line output for winHPR
handles.output = hObject;

    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end
    
    setWindowPosition(hObject,handles);
    
    commentIcon(handles.pbComment);
    
    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    handles.checkedIdx=find([meas.transects.checked]==1);
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    handles.colPlot=1;
    handles.colFilename=2;
    handles.colMagVar=3;
    handles.colOffset=4;
    handles.colHeadSource=5;
    handles.colPitchMean=6;
    handles.colPitchSD=7;
    handles.colRollMean=8;
    handles.colRollSD=9;
    handles.colQPrev=10;
    handles.colQNow=11;
    handles.colQChng=12;
    
    % Set units for column names
    colNames=get(handles.tblHPR,'ColumnName');
    colNames{handles.colQPrev}=[colNames{handles.colQPrev},'|',uLabelQ];
    colNames{handles.colQNow}=[colNames{handles.colQNow},'|',uLabelQ];
    set(handles.tblHPR,'ColumnName',colNames);
    tableData=get(handles.tblHPR,'Data');
    nTransects=length(handles.checkedIdx);
    tableData(1:nTransects,1)={false};
    tableData{1,1}=true;
    set(handles.tblHPR,'Data',tableData(1:nTransects,:));
    set(handles.txtNavRef,'String',meas.transects(1).wVel.navRef);
    % Update table
    updateTable(handles,meas);
    drawnow
    
    % Set available heading options
    trans=[meas.transects(handles.checkedIdx)];
    sensor=[trans.sensors];
    heading=[sensor.heading_deg];
    external=[heading.external];
    if isempty(external)
        set(handles.cbExt,'Enable','off')
    end
    % Check if magnetic error data is available
    if isempty(heading(1).internal.magError)||length(heading(1).internal.magError)==sum(isnan(heading(1).internal.magError))
        set(handles.cbMag,'Enable','off')
    else
        set(handles.cbMag,'Enable','on')
        set(handles.cbMag,'Value',1)
    end
    
    handles.cursor=false;
    % Plot graphs
    handles=plotGraphs(handles);
    
    %set compass error
    if isfield(meas.compassEval(end).result, 'compass')
        if ~strcmp(meas.compassEval(end).result.compass.error,'N/A')
            label = sprintf('%2.2f', meas.compassEval(end).result.compass.error);
            set(handles.compassError(end), 'String', label);
        end
     elseif isfield(meas.compassCal(end).result, 'compass') & strcmp(meas.transects(1, 1).adcp.manufacturer,'SonTek')
        if ~strcmp(meas.compassCal(end).result.compass.error,'N/A')
            label = sprintf('%2.2f', meas.compassCal(end).result.compass.error);
            set(handles.compassError, 'String', label);
        end
    else
        set(handles.compassError, 'String', 'N/A');
    end
    
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes winHPR wait for user response (see UIRESUME)
% uiwait(handles.winHPR);


function varargout = winHPR_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function pbClose_Callback(hObject, eventdata, handles)
% hObject    handle to pbClose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    guidata(hObject, handles);
    delete (handles.output);  

function pbComment_Callback(hObject, eventdata, handles)
% hObject    handle to pbComment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Add comment
    [handles]=commentButton(handles, 'Compass/P/R');
    % Update handles structure
    guidata(hObject, handles);   

function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    %open('helpFiles\QRev_Users_Manual.pdf')  
    web('QRev_Help_Files\HTML\compass_p_r.htm')
    
function pbView_Callback(hObject, eventdata, handles)
% hObject    handle to pbView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Open figure
    handles.hWinCompassCal=winCompassCal('hMainGui',handles.hMainGui);
    set(handles.hWinCompassCal,'WindowStyle','modal');
    % Wait for figure to close before proceeding
    waitfor(handles.hWinCompassCal);   
   
function handles=plotGraphs(handles)

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    cla (handles.axH)
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Get settings from GUI
    tableData=get(handles.tblHPR,'Data');
    transectSelected=[tableData{:,1}];
    transectIdx=find(transectSelected==true);
%     if isempty(transectSelected)
%         transectSelected=1;
%     end
    transectIdx=handles.checkedIdx(transectIdx);
    % Top plot
    tpADCP=get(handles.cbADCP,'Value');
    tpExt=get(handles.cbExt,'Value');
    tpMag=get(handles.cbMag,'Value');
    
    % Bottom plot
    bpRoll=get(handles.cbRoll,'Value');
    bpPitch=get(handles.cbPitch,'Value');   
    cla(handles.axH(1))
    if length(handles.axH)>1
        cla(handles.axH(2))
    end
    hold(handles.axH(1),'on')
    
    if tpADCP
        if strcmpi(meas.transects(transectIdx(1)).startEdge,'Right');
            plotData=fliplr(meas.transects(transectIdx(1)).sensors.heading_deg.('internal').data);
        else
            plotData=meas.transects(transectIdx(1)).sensors.heading_deg.('internal').data;
        end 
        
        plot(handles.axH(1),plotData,'.-r')
        xLimit=length(plotData);
%         if xLimit>handles.axH(1).XLim(2)
%             set(handles.axH(1),'XLim',[handles.axH(1).XLim(1),xLimit]);
%         end
        set(handles.axH(1),'XLim',[0,xLimit]);
        if length(transectIdx)>1
            for n=2:length(transectIdx)
                if strcmpi(meas.transects(transectIdx(n)).startEdge,'Right');
                    plotData=fliplr(meas.transects(transectIdx(n)).sensors.heading_deg.('internal').data);
                else
                    plotData=meas.transects(transectIdx(n)).sensors.heading_deg.('internal').data;
                end
                plot(handles.axH(1),plotData,'.-r')
                xLimit=length(plotData);
                if xLimit>handles.axH(1).XLim(2)
                    set(handles.axH(1),'XLim',[handles.axH(1).XLim(1),xLimit]);
                end
                    
            end
        end % if multiple plots
    end % if tpADCP
    
    if tpExt
        if ~isempty(meas.transects(transectIdx(1)).sensors.heading_deg.external)
            if strcmpi(meas.transects(transectIdx(1)).startEdge,'Right');
                plotData=fliplr(meas.transects(transectIdx(1)).sensors.heading_deg.external.data);
            else
                plotData=meas.transects(transectIdx(1)).sensors.heading_deg.external.data;
            end 
            plot(handles.axH(1),plotData,'Color',[0 0 0.498],'LineStyle','-','Marker','.')
            xLimit=length(plotData);
%             if xLimit>handles.axH(1).XLim(2)
%                 set(handles.axH(1),'XLim',[handles.axH(1).XLim(1),xLimit]);
%             end
        set(handles.axH(1),'XLim',[0,xLimit]);
        end
        if length(transectIdx)>1
            for n=2:length(transectIdx)
                if ~isempty(meas.transects(transectIdx(n)).sensors.heading_deg.external)
                    if strcmpi(meas.transects(transectIdx(n)).startEdge,'Right');
                        plotData=fliplr(meas.transects(transectIdx(n)).sensors.heading_deg.external.data);
                    else
                        plotData=meas.transects(transectIdx(n)).sensors.heading_deg.external.data;
                    end 
                    plot(handles.axH(1),plotData,'Color',[0 0 0.498],'LineStyle','-','Marker','.')
                    xLimit=length(plotData);
                    if xLimit>handles.axH(1).XLim(2)
                        set(handles.axH(1),'XLim',[handles.axH(1).XLim(1),xLimit]);
                    end
                end
            end
        end % if multiple plots
    end % if tpExt
    
    if tpMag
        if strcmpi(meas.transects(transectIdx(1)).startEdge,'Right');
            plotData=fliplr(meas.transects(transectIdx(1)).sensors.heading_deg.internal.magError);
        else
            plotData=meas.transects(transectIdx(1)).sensors.heading_deg.internal.magError;
        end 

            xplotData=1:length(plotData); 
            if length(handles.axH)<2
                [handles.axH,hLine1,hLine2]=plotyy(handles.axH,0,0,xplotData',plotData);
            else
                plot(handles.axH(2),xplotData',plotData,'-k');
            end
            hold(handles.axH(2),'on');
            set(handles.axH,'YColor','k');
            hLine1.Color='k';
            hLine2.Color='k';
            maxData=nanmax(plotData);
            maxLimit=ceil(maxData./4).*4;
            if ~isnan(maxLimit)  ;%>handles.axH(2).YLim(2)
                set(handles.axH(2),'YLim',[0,maxLimit]);
                ticklabels=[0:ceil(maxLimit./4):maxLimit];
                set(handles.axH(2),'YTick',ticklabels);
            end
            xLimit=length(plotData);
    %         if xLimit>handles.axH(2).XLim(2)
    %             set(handles.axH(2),'XLim',[handles.axH(2).XLim(1),xLimit]);
    %         end
            set(handles.axH(2),'XLim',[0,xLimit]);
            if length(transectIdx)>1
                axes(handles.axH(2))
                maxData=0;
                for n=2:length(transectIdx)
                    if strcmpi(meas.transects(transectIdx(n)).startEdge,'Right');
                        plotData=fliplr(meas.transects(transectIdx(n)).sensors.heading_deg.internal.magError);
                    else
                        plotData=meas.transects(transectIdx(n)).sensors.heading_deg.internal.magError;
                    end 
                    xplotData=1:length(plotData);
                    axes(handles.axH(2));
                    plot(handles.axH(2),xplotData',plotData,'-k');
                    rowcol=size(plotData);
                    if rowcol(1)==1
                        maxData=nanmax([maxData,plotData]);
                    else
                        maxData=nanmax([maxData;plotData]);
                    end
                    xLimit=length(plotData);
                    if xLimit>handles.axH(2).XLim(2)
                        set(handles.axH(2),'XLim',[handles.axH(2).XLim(1),xLimit]);
                    end

                end

                if maxData>handles.axH(2).YLim(2)
                    maxLimit=ceil(maxData./4).*4;
                    set(handles.axH(2),'YLim',[handles.axH(2).YLim(1),maxLimit]);
                    ticklabels=[0:ceil(maxLimit./4):maxLimit];
                    set(handles.axH(2),'YTick',ticklabels);
                end
            end % if multiple plots
            plot(handles.axH(2),handles.axH(2).XLim,[2,2],'--k');

    end


    xlabel(handles.axH(1),'Ensemble Left to Right');
    ylabel(handles.axH(1),'Heading');
    if length(handles.axH)>1
        ylabel(handles.axH(2),'Percent Magnetic Error');
    end
    ylim(handles.axH(1),[0,360]);
    set(handles.axH(1),'YTick',[0,90,180,270,360]);
%     box(handles.axH(1),'on');
    set(handles.axH(1),'YAxisLocation', 'left');
    
% Bottom Plot
    cla(handles.axPR)
    hold(handles.axPR,'on')
    maxPR=0;
    minPR=0;
    if bpPitch
        if strcmpi(meas.transects(transectIdx(1)).startEdge,'Right');
            plotData=fliplr(meas.transects(transectIdx(1)).sensors.pitch_deg.('internal').data);
        else
            plotData=meas.transects(transectIdx(1)).sensors.pitch_deg.('internal').data;
        end 
        plot(handles.axPR,plotData,'.-r')
       
        maxTemp=nanmax(plotData);
        if maxTemp>maxPR
            maxPR=maxTemp;
        end
       
        minTemp=nanmin(plotData);
        if minTemp<minPR
            minPR=minTemp;
        end
        if length(transectIdx)>1
            for n=2:length(transectIdx)
                if strcmpi(meas.transects(transectIdx(n)).startEdge,'Right');
                    plotData=fliplr(meas.transects(transectIdx(n)).sensors.pitch_deg.('internal').data);
                else
                    plotData=meas.transects(transectIdx(n)).sensors.pitch_deg.('internal').data;
                end
                plot(handles.axPR,plotData,'.-r')
                maxTemp=nanmax(plotData);
                if maxTemp>maxPR
                    maxPR=maxTemp;
                end

                minTemp=nanmin(plotData);
                if minTemp<minPR
                    minPR=minTemp;
                end
            end
        end % if multiple plots
        if ~isempty(meas.transects(transectIdx(1)).sensors.heading_deg.internal.pitchLimit)
            limit=[meas.transects(transectIdx(1)).sensors.heading_deg.internal.pitchLimit(1),meas.transects(transectIdx(1)).sensors.heading_deg.internal.pitchLimit(1)];
            plot(handles.axPR,handles.axPR.XLim,limit,'--r');
            limit=[meas.transects(transectIdx(1)).sensors.heading_deg.internal.pitchLimit(2),meas.transects(transectIdx(1)).sensors.heading_deg.internal.pitchLimit(2)];
            plot(handles.axPR,handles.axPR.XLim,limit,'--r');
        end
    end % if b1
    
    if bpRoll
        if strcmpi(meas.transects(transectIdx(1)).startEdge,'Right');
            plotData=fliplr(meas.transects(transectIdx(1)).sensors.roll_deg.('internal').data);
        else
            plotData=meas.transects(transectIdx(1)).sensors.roll_deg.('internal').data;
        end 
        plot(handles.axPR,plotData,'.-b')
        
        maxTemp=nanmax(plotData);
        if maxTemp>maxPR
            maxPR=maxTemp;
        end
       
        minTemp=nanmin(plotData);
        if minTemp<minPR
            minPR=minTemp;
        end
        
        if length(transectIdx)>1
            for n=2:length(transectIdx)
                if strcmpi(meas.transects(transectIdx(n)).startEdge,'Right');
                    plotData=fliplr(meas.transects(transectIdx(n)).sensors.roll_deg.('internal').data);
                else
                    plotData=meas.transects(transectIdx(n)).sensors.roll_deg.('internal').data;
                end
                plot(handles.axPR,plotData,'.-b')
                
                maxTemp=nanmax(plotData);
                if maxTemp>maxPR
                    maxPR=maxTemp;
                end

                minTemp=nanmin(plotData);
                if minTemp<minPR
                    minPR=minTemp;
                end
            end
        end % if multiple plots
        if ~isempty(meas.transects(transectIdx(1)).sensors.heading_deg.internal.rollLimit)
            limit=[meas.transects(transectIdx(1)).sensors.heading_deg.internal.rollLimit(1),meas.transects(transectIdx(1)).sensors.heading_deg.internal.rollLimit(1)];
            plot(handles.axPR,handles.axPR.XLim,limit,'--b');
            limit=[meas.transects(transectIdx(1)).sensors.heading_deg.internal.rollLimit(2),meas.transects(transectIdx(1)).sensors.heading_deg.internal.rollLimit(2)];
            plot(handles.axPR,handles.axPR.XLim,limit,'--b');
        end
    end % if b2
    xlabel(handles.axPR,'Ensembles Left to Right');
    ylabel(handles.axPR,'Pitch or Roll (deg)');
    if minPR~=0 && maxPR~=0
        ylim(handles.axPR,[sign(minPR).*ceil(abs(minPR.*1.2)),sign(maxPR).*ceil(abs(maxPR.*1.2))]);
    end
%        
    grid(handles.axPR,'on')
    box(handles.axPR,'on');

%     
     linkaxes([handles.axH,handles.axPR],'x');
 
    
function cbPitch_Callback(hObject, eventdata, handles)
    handles=plotGraphs(handles)
    guidata(hObject, handles);

function cbRoll_Callback(hObject, eventdata, handles)
    handles=plotGraphs(handles)
    guidata(hObject, handles);

function cbADCP_Callback(hObject, eventdata, handles)
    handles=plotGraphs(handles)
    guidata(hObject, handles);

function cbExt_Callback(hObject, eventdata, handles)
    handles=plotGraphs(handles)
    guidata(hObject, handles);

function cbMag_Callback(hObject, eventdata, handles)
    handles=plotGraphs(handles)
    guidata(hObject, handles);

function updateTable(handles,meas)
    tblData=get(handles.tblHPR,'Data');

    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    % Get data for table from each transect
    nTransects=length(handles.checkedIdx);
    for n=1:nTransects
        idx=find(meas.transects(handles.checkedIdx(n)).fileName=='\',1,'last');
        if isempty(idx)
            idx=0;
        end
        headingSource=meas.transects(handles.checkedIdx(n)).sensors.heading_deg.selected;
        tblData{n,handles.colFilename}=meas.transects(handles.checkedIdx(n)).fileName(idx+1:end);
        tblData{n,handles.colMagVar}=sprintf('% 12.1f',meas.transects(handles.checkedIdx(n)).sensors.heading_deg.(headingSource).magVar_deg);
        tblData{n,handles.colOffset}=sprintf('% 12.1f',meas.transects(handles.checkedIdx(n)).sensors.heading_deg.(headingSource).alignCorrection_deg);
        switch headingSource
            case 'internal'
               tblData{n,handles.colHeadSource}='Internal';
            case 'external'
               tblData{n,handles.colHeadSource}='External'; 
        end
        tblData{n,handles.colPitchMean}=sprintf('% 8.1f',nanmean(meas.transects(handles.checkedIdx(n)).sensors.pitch_deg.('internal').data));
        tblData{n,handles.colPitchSD}=sprintf('% 10.1f',nanstd(meas.transects(handles.checkedIdx(n)).sensors.pitch_deg.('internal').data));
        tblData{n,handles.colRollMean}=sprintf('% 8.1f',nanmean(meas.transects(handles.checkedIdx(n)).sensors.roll_deg.('internal').data));
        tblData{n,handles.colRollSD}=sprintf('% 10.1f',nanstd(meas.transects(handles.checkedIdx(n)).sensors.roll_deg.('internal').data));
        oldDischarge=tblData(n,handles.colQNow);
        oldDischarge=str2double(oldDischarge{1})./unitsQ;
        if isnan(oldDischarge)
            oldDischarge=meas.discharge(handles.checkedIdx(n)).total;
        end
        tblData{n,handles.colQPrev}=sprintf('% 12.1f',oldDischarge.*unitsQ);
        tblData{n,handles.colQNow}=sprintf('% 12.1f',meas.discharge(handles.checkedIdx(n)).total.*unitsQ);
        tblData{n,handles.colQChng}=sprintf('% 12.1f',((meas.discharge(handles.checkedIdx(n)).total-oldDischarge)./oldDischarge).*100);
        
       
    end

    % Apply formatting based on qa checks for magvar
    switch meas.qa.compass.magvar
        % Caution
        case 1
            for n=1:nTransects
                tblData(n,handles.colMagVar)={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tblData{n,handles.colMagVar}),'</center></BODY></HTML>']};
            end
        % Warning
        case 2
%             nIdx=length(meas.qa.compass.magvarIdx);
%             for n=1:nIdx
%                 idx=meas.qa.compass.magvarIdx(n);
%                 tblData(idx,handles.colMagVar)={['<HTML><BODY bgColor="red" width="50px"><strong><center>',num2str(tblData{idx,handles.colMagVar}),'</center></strong></BODY></HTML>']};
            for n=1:nTransects
                tblData(n,handles.colMagVar)={['<HTML><BODY bgColor="red" width="50px"><strong><center>',num2str(tblData{n,handles.colMagVar}),'</center></strong></BODY></HTML>']};
            end
    end
    
    % Apply formatting for pitch
    if ~isempty(meas.qa.compass.meanPitchIdxW)
        formatIdx=intersect(meas.qa.compass.meanPitchIdxW, handles.checkedIdx);
        nIdx=length(formatIdx);
        for n=1:nIdx
%             idx=find(handles.checkedIdx==formatIdx(n));
            tblData(formatIdx(n),handles.colPitchMean)={['<HTML><BODY bgColor="red" width="50px"><strong><center>',num2str(tblData{formatIdx(n),handles.colPitchMean}),'</center></strong></BODY></HTML>']};
        end
    end
    
    if ~isempty(meas.qa.compass.meanPitchIdxC)
		formatIdx=intersect(meas.qa.compass.meanPitchIdxC, handles.checkedIdx);
        nIdx=length(formatIdx);
        for n=1:nIdx
%             idx=find(handles.checkedIdx==formatIdx(n));
            tblData(formatIdx(n),handles.colPitchMean)={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tblData{formatIdx(n),handles.colPitchMean}),'</center></BODY></HTML>']};
        end
    end
    
    if ~isempty(meas.qa.compass.stdPitchIdx)
		formatIdx=intersect(meas.qa.compass.stdPitchIdx, handles.checkedIdx);
        nIdx=length(formatIdx);
        for n=1:nIdx
%             idx=find(handles.checkedIdx==formatIdx(n));
            tblData(formatIdx(n),handles.colPitchSD)={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tblData{formatIdx(n),handles.colPitchSD}),'</center></BODY></HTML>']};
        end
    end
    
    % Apply formatting for roll
    if ~isempty(meas.qa.compass.meanRollIdxW)
		formatIdx=intersect(meas.qa.compass.meanRollIdxW, handles.checkedIdx);
        nIdx=length(formatIdx);
        for n=1:nIdx
%             idx=find(handles.checkedIdx==formatIdx(n));
            tblData(formatIdx(n),handles.colRollMean)={['<HTML><BODY bgColor="red" width="50px"><strong><center>',num2str(tblData{formatIdx(n),handles.colRollMean}),'</center></strong></BODY></HTML>']};
        end
    end
    
    if ~isempty(meas.qa.compass.meanRollIdxC)
		formatIdx=intersect(meas.qa.compass.meanRollIdxC, handles.checkedIdx);
        nIdx=length(formatIdx);
        for n=1:nIdx
%             idx=find(handles.checkedIdx==formatIdx(n));
            tblData(formatIdx(n),handles.colRollMean)={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tblData{formatIdx(n),handles.colRollMean}),'</center></BODY></HTML>']};
        end
    end
    
    if ~isempty(meas.qa.compass.stdRollIdx)
		formatIdx=intersect(meas.qa.compass.stdRollIdx, handles.checkedIdx);
        nIdx=length(formatIdx);
        for n=1:nIdx
%             idx=find(handles.checkedIdx==formatIdx(n));
            tblData(formatIdx(n),handles.colRollSD)={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tblData{formatIdx(n),handles.colRollSD}),'</center></BODY></HTML>']};
        end
    end    
    
    % Apply formatting based on compass calibration characteristics
    if ~isempty(meas.qa.compass.magErrorIdx)
        formatIdx=intersect(meas.qa.compass.magErrorIdx, handles.checkedIdx);
        nIdx=length(formatIdx);
        for n=1:nIdx
%             idx=handles.checkedIdx(formatIdx(n));
            tblData(formatIdx(n),handles.colFilename)={['<HTML><BODY bgColor="yellow" width="200px">',tblData{formatIdx(n),handles.colFilename},'</BODY></HTML>']};
        end
    end
    
    if ~isempty(meas.qa.compass.calRollIdx)
		formatIdx=intersect(meas.qa.compass.calRollIdx, handles.checkedIdx);
        nIdx=length(formatIdx);
        for n=1:nIdx
%             idx=find(handles.checkedIdx==formatIdx(n));
            tblData(formatIdx(n),handles.colFilename)={['<HTML><BODY bgColor="yellow" width="200px">',tblData{formatIdx(n),handles.colFilename},'</BODY></HTML>']};
        end
    end
    
    if ~isempty(meas.qa.compass.calPitchIdx)
		formatIdx=intersect(meas.qa.compass.calPitchIdx, handles.checkedIdx);
        nIdx=length(formatIdx);
        for n=1:nIdx
%             idx=find(handles.checkedIdx==formatIdx(n));
            tblData(formatIdx(n),handles.colFilename)={['<HTML><BODY bgColor="yellow" width="200px">',tblData{formatIdx(n),handles.colFilename},'</BODY></HTML>']};
        end
    end
     % Reset column names
     colNames=get(handles.tblHPR,'ColumnName');
     set(handles.tblHPR,'ColumnName',colNames);
    % Update table with formatting
     set(handles.tblHPR,'Data',tblData);
     

     
    % Format compass cal/eval button
     
    % Set color variables
    handles.goodColor=[0 1 0];
    handles.goodFontW='Normal';
    handles.goodFontA='Normal';
    handles.cautionColor=[1,1,0];
    handles.cautionFontW='Normal';
    handles.cautionFontA='Italic';
    handles.warningColor=[1 0 0];
    handles.warningFontW='Bold';
    handles.warningFontA='Normal';
    handles.defaultColor=[0.94 0.94 0.94];
    handles.defaultFontW='Normal';
    handles.defaultFontA='Normal';
    % Enable button
    button='pbView';
    set(handles.(button),'Enable','on');
    
    % Set color
    switch meas.qa.compass.status1
        case 'good'
            set(handles.(button),'BackgroundColor',handles.goodColor,'FontWeight',handles.goodFontW,'FontAngle',handles.goodFontA);
        case 'caution'
            set(handles.(button),'BackgroundColor',handles.cautionColor,'FontWeight',handles.cautionFontW,'FontAngle',handles.cautionFontA);
        case 'warning'
            set(handles.(button),'BackgroundColor',handles.warningColor,'FontWeight',handles.warningFontW,'FontAngle',handles.warningFontA);
        case 'default'
            set(handles.(button),'BackgroundColor',handles.defaultColor,'FontWeight',handles.defaultFontW,'FontAngle',handles.defaultFontA);    
        case 'inactive'
            set(handles.(button),'BackgroundColor',handles.defaultColor,'FontWeight',handles.defaultFontW,'FontAngle',handles.defaultFontA);    
            set(handles.(button),'Enable','off');
    end % switch 
    
function tblHPR_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to tblHPR (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)
    % Get row and col of selected cell
    
    % If statement due to code executed twice. Not sure why but this fixes
    % the problem. Added handles.cursor to fix problem with using cursor
    % keys and then mouse.
    selection = handles.checkedIdx(eventdata.Indices(:,1));
    if ~isempty(selection) && handles.cursor~=true

        col=eventdata.Indices(:,2);
        row=eventdata.Indices(:,1);

        % Get table data
        tableData=get(handles.tblHPR,'Data');

        
        % Plot logic
        if col==1
            plotLogic=tableData(:,1);
            plotLogic{row}=~plotLogic{row};
            tableData(:,1)=plotLogic;
            %set(handles.tblHPR,'Data',tableData);
            handles=plotGraphs(handles);
            
        % Magvar
        elseif col==3
            % Open winMagVar
            handles.hWinMagVar=winMagVar('hMainGui',handles.hMainGui,selection);
            set(handles.hWinMagVar,'WindowStyle','modal');
            % Wait for figure to close before proceeding
            waitfor(handles.hWinMagVar);

            % Get update measurement data
            meas=getappdata(handles.hMainGui,'measurement');

            % Update GUI & Data
            updateTable(handles, meas)
            handles=plotGraphs (handles);    
            
        % Heading Offset
        elseif col==4
            % Get update measurement data
            meas=getappdata(handles.hMainGui,'measurement');
            
            % Set available heading options
            trans=[meas.transects];
            sensor=[trans.sensors];
            heading=[sensor.heading_deg];
            external=[heading.external];
            
            if ~isempty(external)
               
                % Open winHOffset
                handles.hWinHOffset=winHOffset('hMainGui',handles.hMainGui,selection);
                set(handles.hWinHOffset,'WindowStyle','modal');
                % Wait for figure to close before proceeding
                waitfor(handles.hWinHOffset);

                % Get update measurement data
                meas=getappdata(handles.hMainGui,'measurement');
                
                % Update GUI & Data
                updateTable(handles, meas)
                plotGraphs (handles); 
            end % if external
            
        % Heading Source
        elseif col==5
            
            % Get update measurement data
            meas=getappdata(handles.hMainGui,'measurement');
            
            % Set available heading options
            trans=[meas.transects];
            sensor=[trans.sensors];
            heading=[sensor.heading_deg];
            external=[heading.external];
            
            if ~isempty(external) 
                                
                % Open winHOffset
                handles.hWinHSource=winHSource('hMainGui',handles.hMainGui,selection);
                set(handles.hWinHSource,'WindowStyle','modal');
                % Wait for figure to close before proceeding
                waitfor(handles.hWinHSource);

                % Get update measurement data
                meas=getappdata(handles.hMainGui,'measurement');

                % Update GUI & Data
                updateTable(handles, meas)
                plotGraphs (handles); 
            end % if external
                
        % Any other column
        else
            plotLogic=tableData(:,1);
            plotLogic=[plotLogic{:}];
            plotLogic(:)=false;
            plotLogic(row)=true;
            for n=1:length(plotLogic)
                tableData{n,1}=plotLogic(n);
            end
            set(handles.tblHPR,'Data',tableData);
            handles=plotGraphs(handles);
        end
    end
    
    % Set cursor to false to help with switch between arrow keys and mouse
    handles.cursor=false;    
guidata(hObject, handles);

function tblHPR_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to tblHPR (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
        
    % Plot data
    handles=plotGraphs(handles);
    guidata(hObject, handles);

function tblHPR_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to tblDepths (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
    
% Make table inactive to avoid synching issues associated with multiple
% keypresses before the processing of the first can be completed.
set(handles.tblHPR,'Enable','inactive');

    % Check for up or down arrow
    if strcmp(eventdata.Key,'uparrow') || strcmp(eventdata.Key,'downarrow')
        
        % Get data
        tableData=get(handles.tblHPR,'Data');
        
        % Get current plot logic
        plotLogic=tableData(:,1);
        plotLogic=[plotLogic{:}];
        rowTrue=find(plotLogic==true,1,'last');
        
        % Set plot data to next row
        if strcmp(eventdata.Key,'downarrow')
            if rowTrue+1<=size(tableData,1)
                newRow=rowTrue+1;          
            else
                newRow=size(tableData,1);
            end
        % Set plot data to previous row     
        elseif strcmp(eventdata.Key,'uparrow')
            if rowTrue-1>0
                newRow=rowTrue-1;
            else
                newRow=1;
            end
        end
        
        % Set plot logic to new row
        plotLogic(:)=false;
        plotLogic(newRow)=true;
        for n=1:length(plotLogic)
            tableData{n,1}=plotLogic(n);
        end
        
        % Update table
        set(handles.tblHPR,'Data',tableData);
        
        % Trick to try and make ui work properly
        handles.cursor=true;
        % Update handles structure
         guidata(hObject, handles);
         
        % Plot data
        handles=plotGraphs(handles);    
        drawnow

        % Update handles structure
        guidata(hObject, handles);
    end    
    
    set(handles.tblHPR,'Enable','on');


% --- Executes during object creation, after setting all properties.
function compassError_CreateFcn(hObject, eventdata, handles)
   
    
    

% hObject    handle to compassError (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
