function varargout = winSystemTest(varargin)
% WINSYSTEMTEST MATLAB code for winSystemTest.fig
%      WINSYSTEMTEST, by itself, creates a new WINSYSTEMTEST or raises the existing
%      singleton*.
%
%      H = WINSYSTEMTEST returns the handle to a new WINSYSTEMTEST or the handle to
%      the existing singleton*.
%
%      WINSYSTEMTEST('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINSYSTEMTEST.M with the given input arguments.
%
%      WINSYSTEMTEST('Property','Value',...) creates a new WINSYSTEMTEST or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winSystemTest_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winSystemTest_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winSystemTest

% Last Modified by GUIDE v2.5 30-May-2017 11:29:45

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winSystemTest_OpeningFcn, ...
                   'gui_OutputFcn',  @winSystemTest_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before winSystemTest is made visible.
function winSystemTest_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winSystemTest (see VARARGIN)

% Choose default command line output for winSystemTest
handles.output = hObject;

% Get main gui handle
hInput=find(strcmp(varargin,'hMainGui'));
if ~isempty(hInput)
    handles.hMainGui=varargin{hInput+1};
end

setWindowPosition(hObject,handles);

commentIcon(handles.pbComment);

% Get measurement data
meas=getappdata(handles.hMainGui,'measurement');
sysTest=meas.sysTest;

% Setup drop-down lists for available data
if ~isempty(sysTest)
    nTests=size(sysTest,2);
    for n=1:nTests
        tsn=length(sysTest(n).timeStamp);
        ts(n,1:tsn)=sysTest(n).timeStamp;
    end
    if isempty(ts)
        set (handles.puTimeStamp,'String',' ');
        set (handles.puTimeStamp,'Value',1);
        set (handles.txtSystemTest,'String','No test data loaded'); 
    else
        set (handles.puTimeStamp,'String',ts);    
        set (handles.puTimeStamp,'Value',nTests);
        set (handles.txtSystemTest,'String',sysTest(nTests).data);
        set (handles.txtFailed,'String',num2str(sysTest(nTests).result.sysTest.nFailed));
        set (handles.txtNum,'String',num2str(sysTest(nTests).result.sysTest.nTests));
    end

else
    % No system test
    set (handles.txtSystemTest,'String','No Test Data');
end
drawnow

% Update handles structure
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = winSystemTest_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pbCloseSystemTest.
function pbCloseSystemTest_Callback(hObject, eventdata, handles)

delete (handles.output);


% --- Executes on selection change in puTimeStamp.
function puTimeStamp_Callback(hObject, eventdata, handles)
% Display data for selected time stamp
    meas=getappdata(handles.hMainGui,'measurement');
    puValue=get(handles.puTimeStamp,'Value');
    set (handles.txtSystemTest,'String',meas.sysTest(puValue).data);
    failedIdx=strfind(meas.sysTest(puValue).data,'FAIL');
    if isempty(failedIdx)
        set(handles.txtFailed,'String','0');
    else
        set(handles.txtFailed,'String',length(failedIdx));
    end
    drawnow
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function puTimeStamp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to puTimeStamp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtSystemTest_Callback(hObject, eventdata, handles)
% hObject    handle to txtSystemTest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtSystemTest as text
%        str2double(get(hObject,'String')) returns contents of txtSystemTest as a double


% --- Executes during object creation, after setting all properties.
function txtSystemTest_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtSystemTest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbLoad.
function pbLoad_Callback(hObject, eventdata, handles)
% Function to load text file with system test data

    % User selects file
    prefPath=getUserPref('Folder');
    [fileName,pathName]=uigetfile('*.txt','SELECT TEXT FILE',prefPath);
    
    if length(pathName)>1
    
        % Get measurement data
        meas=getappdata(handles.hMainGui,'measurement');

        % Add system test to measurement data
        meas=addSysTest(meas,[pathName,fileName]);

        % Store system test
        setappdata(handles.hMainGui,'measurement',meas);

        % Update GUI
        sysTest=meas.sysTest;

        % Setup drop-down lists for available data
        if ~isempty(sysTest)
            nTests=size(sysTest,2);
            for n=1:nTests
                tsn=length(sysTest(n).timeStamp);
                ts(n,1:tsn)=sysTest(n).timeStamp;
            end
            if isempty(ts)
                set (handles.puTimeStamp,'String',' ');
                set (handles.puTimeStamp,'Value',1);
                set (handles.txtSystemTest,'String','No test data loaded'); 
            else
                set (handles.puTimeStamp,'String',ts);    
                set (handles.puTimeStamp,'Value',nTests);
                set (handles.txtSystemTest,'String',sysTest(nTests).data);
                failedIdx=strfind(sysTest(nTests).data,'FAIL');
                if isempty(failedIdx)
                    set(handles.txtFailed,'String','0');
                else
                    set(handles.txtFailed,'String',length(failedIdx));
                end
            end

        else
            % No system test
            set (handles.txtSystemTest,'String','No Test Data');
        end
        drawnow        
        guidata(hObject, handles);
    end
    
function pbComment_Callback(hObject, eventdata, handles)
% Add comment
    [handles]=commentButton(handles, 'System Test');
    % Update handles structure
    guidata(hObject, handles);
    
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%open('helpFiles\QRev_Users_Manual.pdf') 
web('QRev_Help_Files\HTML\system_test_button.htm')


function txtFailed_Callback(hObject, eventdata, handles)
% hObject    handle to txtFailed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtFailed as text
%        str2double(get(hObject,'String')) returns contents of txtFailed as a double


% --- Executes during object creation, after setting all properties.
function txtFailed_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtFailed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

 



function txtNum_Callback(hObject, eventdata, handles)
% hObject    handle to txtNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtNum as text
%        str2double(get(hObject,'String')) returns contents of txtNum as a double


% --- Executes during object creation, after setting all properties.
function txtNum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
