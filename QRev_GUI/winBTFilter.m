function varargout = winBTFilter(varargin)
% WINBTFILTER MATLAB code for winBTFilter.fig
%      WINBTFILTER, by itself, creates a new WINBTFILTER or raises the existing
%      singleton*.
%
%      H = WINBTFILTER returns the handle to a new WINBTFILTER or the handle to
%      the existing singleton*.
%
%      WINBTFILTER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINBTFILTER.M with the given input arguments.
%
%      WINBTFILTER('Property','Value',...) creates a new WINBTFILTER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winBTFilter_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winBTFilter_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winBTFilter

% Last Modified by GUIDE v2.5 03-Jan-2017 13:14:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winBTFilter_OpeningFcn, ...
                   'gui_OutputFcn',  @winBTFilter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function winBTFilter_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winBTFilter (see VARARGIN)

    % Choose default command line output for winBTFilter
    handles.output = hObject;
    
    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end
    
    % Set window positon on screen
    setWindowPosition(hObject,handles);
    
    commentIcon(handles.pbComment);
    
    % Get table from gui
    tableData=get(handles.tblBTFilters,'Data');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Set units for column names
    colNames=get(handles.tblBTFilters,'ColumnName');
    colNames{10}=[colNames{10},' ',uLabelQ];
    colNames{11}=[colNames{11},' ',uLabelQ];
    set(handles.tblBTFilters,'ColumnName',colNames);
    
    
    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    discharge=meas.discharge;
    oldDischarge=discharge;
    handles.checkedIdx=find([meas.transects.checked]==1);
    
    % Set reference
    set(handles.txtNavRef,'String',meas.transects(1).wVel.navRef);
  
    nTransects=length(handles.checkedIdx);
    selected='btVel';
    for n=1:nTransects
        beamFilter(n)=meas.transects(handles.checkedIdx(n)).boatVel.(selected).beamFilter;
        dFilter{n}=meas.transects(handles.checkedIdx(n)).boatVel.(selected).dFilter;
        dFilterThreshold(n)=meas.transects(handles.checkedIdx(n)).boatVel.(selected).dFilterThreshold;
        wFilter{n}=meas.transects(handles.checkedIdx(n)).boatVel.(selected).wFilter;
        wFilterThreshold(n)=meas.transects(handles.checkedIdx(n)).boatVel.(selected).wFilterThreshold;
        smoothFilter{n}=meas.transects(handles.checkedIdx(n)).boatVel.(selected).smoothFilter;
        interpolate{n}=meas.transects(handles.checkedIdx(n)).boatVel.(selected).interpolate;
    end % for n

    handles=updateTable(handles,meas,oldDischarge);
    tableData=get(handles.tblBTFilters,'Data');
    
    % Set transect to initially plot
    handles.plottedTransect=1;
    set(handles.plottedtxt,'String',tableData{handles.plottedTransect,1});
    
    set(handles.tblBTFilters,'Data',tableData);

    % Set beam filter option
    switch beamFilter(1)
        case 3
            set(handles.pu3Beam,'Value',2);
        case 4
            set(handles.pu3Beam,'Value',3);
        otherwise
            set(handles.pu3Beam,'Value',1);
    end % switch
   
    % Set difference filter options
    switch dFilter{1}
        case 'Auto'
            set(handles.puDiffVel,'Value',1);
            set(handles.edDiffVelThreshold,'Enable','off')
            set(handles.edDiffVelThreshold,'String','Auto')
        case 'Manual'
            set(handles.puDiffVel,'Value',2);
            set(handles.edDiffVelThreshold,'Enable','on')
            set(handles.edDiffVelThreshold,'String',num2str(dFilterThreshold(1).*unitsV,'%6.3f'))
        case 'Off'
            set(handles.puDiffVel,'Value',3);
            set(handles.edDiffVelThreshold,'Enable','off')
            set(handles.edDiffVelThreshold,'String','None')
    end

    % Set vertical velocity filter options
    switch wFilter{1}
        case 'Auto'
            set(handles.puVertVel,'Value',1);
            set(handles.edVertVelThreshold,'Enable','off')
            set(handles.edVertVelThreshold,'String','Auto')
        case 'Manual'
            set(handles.puVertVel,'Value',2);
            set(handles.edVertVelThreshold,'Enable','on')
            set(handles.edVertVelThreshold,'String',num2str(wFilterThreshold(1).*unitsV,'%6.3f'))
        case 'Off'
            set(handles.puVertVel,'Value',3);
            set(handles.edVertVelThreshold,'Enable','off')
            set(handles.edVertVelThreshold,'String','None')
    end

    % Set smooth filter options
    switch smoothFilter{1}
        case 'On'
            set(handles.puOtherFilter,'Value',2);
        case 'Off'
            set(handles.puOtherFilter,'Value',1);
    end
    
    % Set interpolation options
    set(handles.puBTInterp,'Visible',getappdata(handles.hMainGui,'InterpVisible'));
    set(handles.uipanelInterp,'Visible',getappdata(handles.hMainGui,'InterpVisible'));
    switch interpolate{1}
        case 'None'
            set(handles.puBTInterp,'Value',1)
        case 'Expand Delta Time'
            set(handles.puBTInterp,'Value',2);
        case 'Hold9'
            set(handles.puBTInterp,'Value',3);
        case 'HoldLast'
            set(handles.puBTInterp,'Value',4);
        case 'Smooth'
            set(handles.puBTInterp,'Value',5);
        case 'Linear'
            set(handles.puBTInterp,'Value',6);
        case 'TRDI'
            set(handles.puBTInterp,'Value',7);
    end

    % Compute number of 3 beam solutions
    temp=meas.transects(handles.checkedIdx);
    temp=boatFilters(temp,0,'Beam',4);
    tableData=get(handles.tblBTFilters,'Data');
    for n=1:nTransects
        tableData{n,3}=sprintf('% 8.0f',nansum(~temp(n).boatVel.(selected).validData(6,:)));
    end % for n    
    
    % Update table
    set(handles.tblBTFilters,'Data',tableData(1:nTransects,:));
    rbGraphicsPanel_SelectionChangeFcn(hObject, eventdata, handles)
    drawnow
    
% Update handles structure
guidata(hObject, handles);

function varargout = winBTFilter_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function tableData=applyFormat(handles,tableData,meas)
% Applys a format to the table that highlights values that have generated
% warnings or caution about the quality of the data and its affect on Q.
%
% INPUT:
%
% handles: handles data structure from figure
%
% tableData: data from table to be formatted
%
% meas: object of clsMeasurement
%
% OUTPUT:
%
% tableData: data for table that has been formatted.

    % Combine caution notices into single matrix
    
    caution=meas.qa.btVel.qTotalCaution(handles.checkedIdx,:) | meas.qa.btVel.qRunCaution(handles.checkedIdx,:);
    
    % Combine warning notices into single matrix
    warning=meas.qa.btVel.qRunWarning(handles.checkedIdx,:) | meas.qa.btVel.qTotalWarning(handles.checkedIdx,:);
    
    % Combine caution and warning into single matrix
    code=double(caution);
    code(warning)=2;
    
    % Rearrange matrix to match table
    applyCode=code(:,[1:2,6,3:5]);
    
    % Creat idx of cells to apply caution and warning formats
    idxCaution=find(applyCode==1);
    idxWarning=find(applyCode==2);
    
    % Get subset of table to be formatted
    tempData=tableData(:,4:9);
    
    % Apply caution format
    for n=1:length(idxCaution)
        tempData(idxCaution(n))={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tempData{idxCaution(n)}),'</center></BODY></HTML>']};
    end
    
    % Apply warning format
    for n=1:length(idxWarning)
        tempData(idxWarning(n))={['<HTML><BODY bgColor="red" width="50px"><strong><center>',num2str(tempData{idxWarning(n)}),'</center></strong></BODY></HTML>']};
    end
    
    % Put formatted cells back in table
    tableData(:,4:9)=tempData;
    
function handles=updateTable(handles,meas,oldDischarge)
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);

    tableData=get(handles.tblBTFilters,'Data');

    nTransects=length(handles.checkedIdx);
    for n=1:nTransects

        validData=meas.transects(handles.checkedIdx(n)).boatVel.btVel.validData;  
        idx=find(meas.transects(handles.checkedIdx(n)).fileName=='\',1,'last');
        if isempty(idx)
            idx=0;
        end
        tableData{n,1}=meas.transects(handles.checkedIdx(n)).fileName(idx+1:end);
        tableData{n,2}=sprintf('% 8.0f',size(validData,2));
        tableData{n,4}=sprintf('% 8.0f',nansum(~validData(1,:)));
        tableData{n,5}=sprintf('% 8.0f',nansum(~validData(2,:)));
        tableData{n,6}=sprintf('% 8.0f',nansum(~validData(6,:)));
        tableData{n,7}=sprintf('% 8.0f',nansum(~validData(3,:)));
        tableData{n,8}=sprintf('% 8.0f',nansum(~validData(4,:)));
        tableData{n,9}=sprintf('% 8.0f',nansum(~validData(5,:)));
        tableData{n,11}=sprintf('% 12.2f',meas.discharge(handles.checkedIdx(n)).total.*unitsQ);
        tableData{n,10}=sprintf('% 12.2f',oldDischarge(handles.checkedIdx(n)).total.*unitsQ);
        tableData{n,12}=sprintf('% 12.2f',((meas.discharge(handles.checkedIdx(n)).total-oldDischarge(handles.checkedIdx(n)).total)./oldDischarge(handles.checkedIdx(n)).total).*100); 
    end % for n
    tableData=applyFormat(handles,tableData,meas);
    set(handles.tblBTFilters,'Data',tableData(1:nTransects,:));

% Filter settings

function pu3Beam_Callback(hObject, eventdata, handles)

    selected='btVel';

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get data from GUI
    setting=get(handles.pu3Beam,'Value');

    
    % Apply selected setting
    switch setting
        case 1 % Auto
            s.BTbeamFilter=-1;
                            
        case 2 % Allow 3 beam solutions
            s.BTbeamFilter=3;
            
        case 3 % 4-beam solutions only
            s.BTbeamFilter=4;
    end
    
    % Update data and computations
    oldDischarge=meas.discharge;
    oldpointer = get(gcf, 'pointer'); 
    set(gcf, 'pointer', 'watch');
    drawnow;
    meas=applySettings(meas,s);
    set(gcf, 'pointer', oldpointer);
    drawnow;

    % Update table
    handles=updateTable(handles,meas,oldDischarge);
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Update graphics
    rbGraphicsPanel_SelectionChangeFcn(hObject, eventdata, handles)
    guidata(hObject, handles);
    
function puDiffVel_Callback(hObject, eventdata, handles)
% Process difference velocity filter as selected by the user

    selected='btVel';

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);       
    
    % Get data from GUI
    setting=get(handles.puDiffVel,'Value');
    tableData=get(handles.tblBTFilters,'Data');
    
    % Apply selected setting
    switch setting
        case 1 % Auto
            s.BTdFilter='Auto';
            set(handles.edDiffVelThreshold,'Enable','off')
            set(handles.edDiffVelThreshold,'String','Auto');
                            
        case 2 % Manual
            s.BTdFilter='Manual';
            s.BTdFilterThreshold=meas.transects(1).boatVel.(selected).dFilterThreshold;
            set(handles.edDiffVelThreshold,'Enable','on');
            set(handles.edDiffVelThreshold,'String',num2str(meas.transects(1).boatVel.(selected).dFilterThreshold.*unitsV,'%6.3f'));

        case 3 % No filter applies
            meas.transects=boatFilters(meas.transects,'Difference','Off');
            s.BTdFilter='Off';
            set(handles.edDiffVelThreshold,'Enable','off')
            set(handles.edDiffVelThreshold,'String','None');
    end
    
    % Update data and computations
    oldDischarge=meas.discharge;
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    meas=applySettings(meas,s);     
    set(gcf, 'pointer', oldpointer);     
    drawnow;

    % Update table
    handles=updateTable(handles,meas,oldDischarge);
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
       
    % Update graphics
    rbGraphicsPanel_SelectionChangeFcn(hObject, eventdata, handles)
    guidata(hObject, handles);    
    
function edDiffVelThreshold_Callback(hObject, eventdata, handles)
% Process manually entered difference velocity filter
    selected='btVel';
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get data from GUI
    setting=str2num(get(handles.edDiffVelThreshold,'String'));
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Apply filter
    s.BTdFilterThreshold=setting./unitsV;

    % Update data and computations
    oldDischarge=meas.discharge;
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    meas=applySettings(meas,s);     
    set(gcf, 'pointer', oldpointer);     
    drawnow;
        
    % Update display
    set(handles.edDiffVelThreshold,'String',meas.transects(1).boatVel.(selected).dFilterThreshold.*unitsV);
    
    % Update table
    handles=updateTable(handles,meas,oldDischarge);
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Update graphics
    rbGraphicsPanel_SelectionChangeFcn(hObject, eventdata, handles)
    guidata(hObject, handles);        
    
function puVertVel_Callback(hObject, eventdata, handles)
% Processes vertical velocity filter as selected by the user
    selected='btVel';
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);    
    
    % Get data from GUI
    setting=get(handles.puVertVel,'Value');
    
    % Apply selected setting
    switch setting
        case 1 % Auto
            s.BTwFilter='Auto';
            set(handles.edVertVelThreshold,'Enable','off')
            set(handles.edVertVelThreshold,'String','Auto');
                            
        case 2 % Manual
            s.BTwFilter='Manual';
            s.BTwFilterThreshold=meas.transects(1).boatVel.(selected).wFilterThreshold;
            set(handles.edVertVelThreshold,'Enable','on');
            set(handles.edVertVelThreshold,'String',num2str(meas.transects(1).boatVel.(selected).wFilterThreshold.*unitsV,'%6.3f'));

        case 3 % Off, no filtering
            s.BTwFilter='Off';
            set(handles.edVertVelThreshold,'Enable','off')
            set(handles.edVertVelThreshold,'String','None');
    end

    % Update data and computations
    oldDischarge=meas.discharge;
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    meas=applySettings(meas,s);     
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    
    % Update table
    handles=updateTable(handles,meas,oldDischarge);
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Update graphics
    rbGraphicsPanel_SelectionChangeFcn(hObject, eventdata, handles)
    guidata(hObject, handles);    
    
function edVertVelThreshold_Callback(hObject, eventdata, handles)
% Applies manually set vertical velocity threshold
    selected='btVel';
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get data from GUI
    setting=str2num(get(handles.edVertVelThreshold,'String'));
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Apply filter
    s.BTwFilterThreshold=setting./unitsV;
    
    % Update data and computations
    oldDischarge=meas.discharge;
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    meas=applySettings(meas,s);     
    set(gcf, 'pointer', oldpointer);     
    drawnow;    
    
    % Update display
    set(handles.edVertVelThreshold,'String',meas.transects(1).boatVel.(selected).wFilterThreshold.*unitsV);
    
    % Update table
    handles=updateTable(handles,meas,oldDischarge);
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);    
    
    % Update graphics
    rbGraphicsPanel_SelectionChangeFcn(hObject, eventdata, handles)
    guidata(hObject, handles);
    
function puOtherFilter_Callback(hObject, eventdata, handles)
% Processes request for smooth or other filter
    % Get measurement data
    selected='btVel';
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get data from GUI
    setting=get(handles.puOtherFilter,'Value');
    tableData=get(handles.tblBTFilters,'Data');
    
    % Apply filter requested
    switch setting
        case 1 % Off
            s.BTsmoothFilter='Off';
                                     
        case 2 % On
            s.BTsmoothFilter='On';
    end
    
    % Update data and computations
    oldDischarge=meas.discharge;
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    meas=applySettings(meas,s);     
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Update table
    handles=updateTable(handles,meas,oldDischarge);
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Update graphics
    rbGraphicsPanel_SelectionChangeFcn(hObject, eventdata, handles)
    guidata(hObject, handles);    

% Interpolation

function puBTInterp_Callback(hObject, eventdata, handles)
% Processes the bottom track interplation selected by the user
    selected='btVel';
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get data from GUI
    setting=get(handles.puBTInterp,'Value');
    
    % Apply selected interpolation method
    switch setting
        case 1 % None
            % Sets invalid data to nan with no interpolation
            s.BTInterpolation='None';
                        
        case 2 % Expanded Ensemble Time
            % Set interpolate to None as the interpolation is done in the
            % clsQComp
            s.BTInterpolation='ExpandedT';
                        
        case 3 % SonTek Method
            % Interpolates using SonTeks method of holding last valid for
            % up to 9 samples.
            s.BTInterpolation='Hold9';
            
        case 4 % Hold Last Valid
            % Interpolates by holding last valid indefinitely
            s.BTInterpolation='HoldLast';
            
        case 5 % Smooth
            % Interpolates using a robust Loess smooth
            s.BTInterpolation='Smooth';
            
        case 6 % Linear
            % Interpolates using linear interpolation
            s.BTInterpolation='Linear';
            
        case 7 % TRDI
            % Applies TRDI method where depths are ignored and next valid
            % ensemble is used to back fill the discharge
            meas.transects=boatInterpolations(meas.transects,'BT','TRDI');   
    end % switch
    
    % Update data and computations
    oldDischarge=meas.discharge;
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    meas=applySettings(meas,s);     
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);

    % Update table
    handles=updateTable(handles,meas,oldDischarge);
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    
    %Update graphics
    rbGraphicsPanel_SelectionChangeFcn(hObject, eventdata, handles)
    guidata(hObject, handles);   

% Other user events    
    
function pbClose_Callback(hObject, eventdata, handles)
    delete (handles.output);    

function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% open('helpFiles\QRev_Users_Manual.pdf')    
web('QRev_Help_Files\HTML\bt_filters.htm');
    
function tblBTFilters_CellSelectionCallback(hObject, eventdata, handles)
% Handles file selection to be plotted. Allows the selection to work as
% radio buttons so that only one file can be selected

    % Determine row selected
    selection = eventdata.Indices(:,1);
    
    % If statement due to code executed twice. Not sure why but this fixes
    % the problem.
    if ~isempty(selection)
        % Get table data
        tableData=get(handles.tblBTFilters,'Data');
        handles.plottedTransect=selection;
        set(handles.plottedtxt,'String',tableData{selection,1});
    end
    
    % Update graphics
    rbGraphicsPanel_SelectionChangeFcn(hObject, eventdata, handles)
    guidata(hObject, handles);

% Graphics Functions ******************************************************

function rbGraphicsPanel_SelectionChangeFcn(hObject, eventdata, handles)
% Processes selected graphics output
    selected='btVel';
    % Get settings from GUI
    tableData=get(handles.tblBTFilters,'Data');
    radioTag=get(get(handles.rbGraphicsPanel,'SelectedObject'),'Tag');
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    transectSelected=handles.checkedIdx(handles.plottedTransect);
    if isempty(transectSelected)
        transectSelected=handles.checkedIdx(1);
    end
    transect=meas.transects(transectSelected);
    validData=transect.boatVel.(selected).validData;
    
    % Raw boat speed and position
    boatU=transect.boatVel.(selected).u_mps;
    boatV=transect.boatVel.(selected).v_mps;
    ensDuration=transect.dateTime.ensDuration_sec;
    boatX=nancumsum(boatU.*ensDuration);
    boatY=nancumsum(boatV.*ensDuration);

    % Compute processed boat speed and position
    boatUProcessed=transect.boatVel.(selected).uProcessed_mps;
    boatVProcessed=transect.boatVel.(selected).vProcessed_mps;
    boatXProcessed=nancumsum(boatUProcessed.*ensDuration);
    boatYProcessed=nancumsum(boatVProcessed.*ensDuration);
    
    ensembles=1:length(boatUProcessed);
    % Compute boat speed
    speed=sqrt(boatU.^2+boatV.^2);
    speedProcessed=sqrt(boatUProcessed.^2+boatVProcessed.^2);  
    trackPlotted=ensembles;
    % Generate selected plot
    switch radioTag
        case 'rbThreeBeam' % 3 beam filter
            % Clear unused axes
            cla(handles.axAll);
            legend(handles.axAll,'off')
            cla(handles.axTop);
            % Set axes visibility
            set(handles.axTop,'Visible','on');
            set(handles.axBottom,'Visible','on');
            set(handles.axAll,'Visible','off');
            set(handles.axAll,'HandleVisibility','off');
            
            % Process data
           
            temp=transect.boatVel.(selected);
            temp=applyFilter(temp,transect,'Beam',4);
            tempSelectedValidData=temp.validData(6,:);
            topData=double(tempSelectedValidData);
            topData(tempSelectedValidData)=4;
            topData(~tempSelectedValidData)=3;
            topData(~validData(2,:))=0;
            selectedValidData=validData(6,:);
            
            % Plot top figure
            plot(handles.axTop,trackPlotted,topData,'*b')
            hold(handles.axTop,'on')
            plot(handles.axTop,trackPlotted(~selectedValidData),topData(~selectedValidData),'or');
            ylim(handles.axTop,[-0.5, 4.5])
            xlim(handles.axTop, [nanmin(trackPlotted)-nanmax(trackPlotted).*0.01, nanmax(trackPlotted)+nanmax(trackPlotted).*0.01]);
            ylabel(handles.axTop,'Number of Beams')
            xlabel(handles.axTop,'Ensembles')
            hold(handles.axTop,'off')
            
            % Plot shiptrack
            btPlotShiptrack(handles.axShipTrack,boatXProcessed,boatYProcessed,boatX,boatY,validData,handles);

            % Plot bottom plot
            btBottomPlot(handles.axBottom,trackPlotted,speed,...
                speedProcessed,validData,transect.boatVel.btVel.interpolate,handles);
            
        case 'rbDiffVel' % Difference velocity
            % Clear unused axes
            cla(handles.axAll);
            legend(handles.axAll,'off')
            cla(handles.axTop);
            % Set axes visibility
            set(handles.axTop,'Visible','on');
            set(handles.axBottom,'Visible','on');
            set(handles.axAll,'Visible','off');
            set(handles.axAll,'HandleVisibility','off');
            
            % Process data
            dVel=transect.boatVel.(selected).d_mps;
            selectedValidData=validData(3,:);
            
            % Plot top figure
            plot(handles.axTop,trackPlotted,dVel.*unitsV,'.b');
            hold(handles.axTop,'on');
            plot(handles.axTop,trackPlotted(~selectedValidData),dVel(~selectedValidData).*unitsV,'or');
            xlabel(handles.axTop,'Ensembles');
            ylabel(handles.axTop,['Error Velocity ',uLabelV]);
            box(handles.axTop,'on');
            ylim(handles.axTop,'auto')
            hold(handles.axTop,'off');
            
            % Plot shiptrack
            btPlotShiptrack(handles.axShipTrack,boatXProcessed,boatYProcessed,boatX,boatY,validData,handles);
            
            % Plot bottom plot
            btBottomPlot(handles.axBottom,trackPlotted,speed,...
                speedProcessed,validData,transect.boatVel.btVel.interpolate,handles);
            
        case 'rbVertVel' % Vertical velocity
            % Clear unused axes
            cla(handles.axAll);
            legend(handles.axAll,'off')
            % Set axes visibility
            set(handles.axTop,'Visible','on');
            set(handles.axBottom,'Visible','on');
            set(handles.axAll,'Visible','off');
            set(handles.axAll,'HandleVisibility','off');
            
            % Process data
            wVel=transect.boatVel.(selected).w_mps;
            selectedValidData=validData(4,:);
            
            % Plot top figure
            plot(handles.axTop,trackPlotted,wVel.*unitsV,'.b');
            hold(handles.axTop,'on');
            plot(handles.axTop,trackPlotted(~selectedValidData),wVel(~selectedValidData).*unitsV,'or');
            xlim(handles.axTop, [nanmin(trackPlotted)-nanmax(trackPlotted).*0.01, nanmax(trackPlotted)+nanmax(trackPlotted).*0.01]);
            ylim(handles.axTop,'auto');
            xlabel(handles.axTop,'Ensembles');
            ylabel(handles.axTop,['Vertical Velocity ',uLabelV]);                                
            hold(handles.axTop,'off');
            
            % Plot shiptrack
            btPlotShiptrack(handles.axShipTrack,boatXProcessed,boatYProcessed,boatX,boatY,validData,handles);           
           
            % Plot bottom plot
            btBottomPlot(handles.axBottom,trackPlotted,speed,...
                speedProcessed,validData,transect.boatVel.btVel.interpolate,handles);
       
         case 'rbOther' % Smooth or other filter
            % Clear all axes
            cla(handles.axTop);
            cla(handles.axBottom);
            cla(handles.axAll);
            % Set visibility
            set(handles.axTop,'Visible','off');
            set(handles.axBottom,'Visible','off');
            set(handles.axAll,'Visible','on');
            set(handles.axAll,'HandleVisibility','on');
            
            % Get valid data logical array
            validData=validData;
            selectedValidData=validData(5,:);

            % Plot times series figure
            upperLimit=transect.boatVel.(selected).smoothUpperLimit;
            lowerLimit=transect.boatVel.(selected).smoothLowerLimit;
            upperLimit(isnan(upperLimit))=0;
            lowerLimit(isnan(lowerLimit))=0;
            legendStart=4;
            
            % If plot requested but smooth filter not applied area and
            % smooth should not be plotted
            if get(handles.puOtherFilter,'Value')>1                
                area(handles.axAll,trackPlotted,upperLimit.*unitsV,min(lowerLimit.*unitsV),'facecolor',...
                    [0.8275 0.8275 0.8275],'edgecolor',[0.8275 0.8275 0.8275]);
                hold(handles.axAll,'on')
                area(handles.axAll,trackPlotted,lowerLimit.*unitsV,min(lowerLimit.*unitsV),'facecolor',...
                    'white','edgecolor','white');
                plot(handles.axAll,trackPlotted,transect.boatVel.(selected).smoothSpeed.*unitsV,'r');
                
                legendStart=1;
            end
                
            plot (handles.axAll,trackPlotted,speed.*unitsV,'.-b');
            hold(handles.axAll,'on')
            plot (handles.axAll,trackPlotted,transect.boatVel.(selected).d_mps.*unitsV,'g'); %Error
            plot (handles.axAll,trackPlotted,transect.boatVel.(selected).w_mps.*unitsV,'m'); %Vertical
            plot(handles.axAll,trackPlotted(~validData(5,:)),speed(~validData(5,:)).*unitsV,'ok');
            xlim(handles.axAll, [nanmin(trackPlotted)-nanmax(trackPlotted).*0.01, nanmax(trackPlotted)+nanmax(trackPlotted).*0.01]);
            xlabel(handles.axAll,'Ensembles');
            ylabel(handles.axAll,['Speed ',uLabelV]);
            axis (handles.axAll,'tight');
            box(handles.axAll,'on');
            legendText={'Filter Limits',' ','Loess Smooth','Raw Data','Error Velocity',...
                    'Vertical Velocity','Filtered Bad Data'};
            legend (handles.axAll,legendText(legendStart:end));
            legend (handles.axAll,'Location','best');
            hold(handles.axAll,'off')   
            
            % Plot shiptrack
            btPlotShiptrack(handles.axShipTrack,boatXProcessed,boatYProcessed,boatX,boatY,validData,handles);
            
        case 'rbAll' % Plot results of all filters
            % Clear all axes
            cla(handles.axTop);
            cla(handles.axBottom);
            cla(handles.axAll);
            % Set visibility
            set(handles.axTop,'Visible','off');
            set(handles.axBottom,'Visible','off');
            set(handles.axAll,'Visible','on');
            set(handles.axAll,'HandleVisibility','on');
            
            % Plot bottom plot
            btBottomPlot(handles.axAll,trackPlotted,speed,...
                speedProcessed,validData,transect.boatVel.btVel.interpolate,handles);         
    end
    grid(handles.axTop,'On')
    grid(handles.axAll,'On')
    if strcmpi(transect.startEdge,'Right')
        set(handles.axTop,'XDir','reverse');
        set(handles.axBottom,'XDir','reverse');
        set(handles.axAll,'XDir','reverse');
    else
        set(handles.axTop,'XDir','normal');
        set(handles.axBottom,'XDir','normal'); 
        set(handles.axAll,'XDir','normal');
    end
    % Plot shiptrack
    btPlotShiptrack(handles.axShipTrack,boatXProcessed,boatYProcessed,boatX,boatY,validData,handles);
    linkaxes([handles.axTop,handles.axBottom],'x');

function btPlotShiptrack(h,xProcessed,yProcessed,x,y,validData,handles)
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);

% Plot shiptrack
    plot(h,xProcessed.*unitsL,yProcessed.*unitsL,'-r')
    hold(h,'on')
    plot(handles.axShipTrack,xProcessed(end).*unitsL,yProcessed(end).*unitsL,'sk','MarkerFaceColor','k','MarkerSize',8);
    plot(h,x.*unitsL,y.*unitsL,'-b')
    plot(h,x(~validData(2,:)).*unitsL,y(~validData(2,:)).*unitsL,'+r');
    plot(h,x(~validData(3,:)).*unitsL,y(~validData(3,:)).*unitsL,'*r');
    plot(h,x(~validData(4,:)).*unitsL,y(~validData(4,:)).*unitsL,'or');
    plot(h,x(~validData(5,:)).*unitsL,y(~validData(5,:)).*unitsL,'ob');    
    plot(h,x(~validData(6,:)).*unitsL,y(~validData(6,:)).*unitsL,'ok');
    plotted=logical(sum(~validData(2:6,:),2));
    plotted=[true;true;true;plotted];
    hold(h,'off')
    set(h,'DataAspectRatio',[1 1 1]);
    set(h,'Box','on');
    set(h,'PlotBoxAspectRatio',[1 1 1]);
    legendText={'Processed','End','Raw','Invalid Raw','Error Vel','Vertical Vel','Other','3-Beam'};
    legend(h,legendText{plotted},'Location','best')
    xlabel(h,['Distance East ',uLabelL]);
    ylabel(h,['Distance North ',uLabelL]);
    grid(h,'On')
    
function btBottomPlot(h,track,speed,speedProcessed,validData,interpolation,handles)

    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Plot interpolated data first
    if ~strcmpi(interpolation,'None')
        plot(h,track,speedProcessed.*unitsV,'-r')
        hold(h,'on')
        plot(h,track,speed.*unitsV,'-b')
    else
        % Plot raw data
        plot(h,track,speed.*unitsV,'-b')
        hold(h,'on')
    end
    % Set selected valid data variable 
    selectedValidData=validData(1,:);
    % Plot results of each filter in time series of speed
    invalidSpeed=zeros(size(track));

    plot(h,track(~validData(2,:)),invalidSpeed(~validData(2,:)).*unitsV,'+r');
    plot(h,track(~validData(3,:)),speed(~validData(3,:)).*unitsV,'*r');
    plot(h,track(~validData(4,:)),speed(~validData(4,:)).*unitsV,'or');
    plot(h,track(~validData(5,:)),speed(~validData(5,:)).*unitsV,'ob');
    plot(h,track(~validData(6,:)),speed(~validData(6,:)).*unitsV,'ok');
    plotted=logical(sum(~validData(2:6,:),2));
    xlim(h, [nanmin(track)-nanmax(track).*0.01, nanmax(track)+nanmax(track).*0.01]);
    % Adjust legend text based on whether interpolation was applied
    if strcmpi(interpolation,'None')
        plotted=[false;true;plotted];
    else
        plotted=[true;true;plotted];
    end
    legendText={'Processed','Raw','Invalid Raw','Error Vel','Vertical Vel','Other','3-Beam'};
    legend(h,legendText{plotted},'Location','best')
    xlabel(h,'Ensembles')
    ylabel(h,['BT Speed ',uLabelV])
    hold(h,'off')
    grid(h,'On')
    
function pbComment_Callback(hObject, eventdata, handles)
% Add comment
    [handles]=commentButton(handles,'BT');
    % Update handles structure
    guidata(hObject, handles);    

% --- Executes on key press with focus on tblGPS and none of its controls.
function tblBTFilters_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to tblGPS (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA) 

% Make table inactive to avoid synching issues associated with multiple
% keypresses before the processing of the first can be completed.
set(handles.tblBTFilters,'Enable','inactive');

if strcmp(eventdata.Key,'uparrow') || strcmp(eventdata.Key,'downarrow')
    tableData=get(handles.tblBTFilters,'Data');
    rowTrue=handles.plottedTransect;
    if strcmp(eventdata.Key,'downarrow')
        if rowTrue+1<=size(tableData,1)
            newRow=rowTrue+1;          
        else
            newRow=size(tableData,1);
        end
    elseif strcmp(eventdata.Key,'uparrow')
        if rowTrue-1>0
            newRow=rowTrue-1;
        else
            newRow=1;
        end
    end
       
    set(handles.plottedtxt,'String',tableData{newRow,1});
    handles.plottedTransect=newRow;
    drawnow

    % Update graphics
    rbGraphicsPanel_SelectionChangeFcn(hObject, eventdata, handles)
       
    guidata(hObject, handles);
    drawnow
end    
set(handles.tblBTFilters,'Enable','on');

%==========================================================================
%================NO MODIFIED CODE BELOW HERE===============================
%==========================================================================
function rbThreeBeam_Callback(hObject, eventdata, handles)

function rbDiffVel_Callback(hObject, eventdata, handles)

function rbVertVel_Callback(hObject, eventdata, handles)

function rbOther_Callback(hObject, eventdata, handles)

function rbAll_Callback(hObject, eventdata, handles)

function edDiffVelThreshold_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

function textTableQ_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
function puBTInterp_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
function puOtherFilter_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
function edVertVelThreshold_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
function puVertVel_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
function pu3Beam_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
function puDiffVel_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


 
