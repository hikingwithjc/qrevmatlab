function varargout = winHSource(varargin)
% WINHSOURCE MATLAB code for winHSource.fig
%      WINHSOURCE, by itself, creates a new WINHSOURCE or raises the existing
%      singleton*.
%
%      H = WINHSOURCE returns the handle to a new WINHSOURCE or the handle to
%      the existing singleton*.
%
%      WINHSOURCE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINHSOURCE.M with the given input arguments.
%
%      WINHSOURCE('Property','Value',...) creates a new WINHSOURCE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winHSource_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winHSource_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winHSource

% Last Modified by GUIDE v2.5 26-Jul-2016 16:47:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winHSource_OpeningFcn, ...
                   'gui_OutputFcn',  @winHSource_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function winHSource_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winHSource (see VARARGIN)

% Choose default command line output for winHSource
handles.output = hObject;

    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end
    
    % Get transect selected
    handles.selected=varargin{hInput+2};
    
    % Set the window position to lower left
    setWindowPosition(hObject,handles);
    
    % Change comment icon
    commentIcon(handles.pbComment);
    
    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Display the current heading source
    handles.HSource=meas.transects(handles.selected).sensors.heading_deg.selected;
    if strcmpi(handles.HSource,'internal')
        set(handles.rbInt,'Value',1)
    else
        set(handles.rbExt,'Value',1)
    end
    
% Update handles structure
guidata(hObject, handles);

function varargout = winHSource_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function pbOne_Callback(hObject, eventdata, handles)
% hObject    handle to pbOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Apply heading
    meas=changeHeadingSource(meas,handles.HSource,handles.selected);
    meas.qa=clsQAData(meas);
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
   
    % Return pointer to previous
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    guidata(hObject, handles);
    
    % Close winHSource
    delete (handles.output);  

function pbAll_Callback(hObject, eventdata, handles)
% hObject    handle to pbAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
       
    % Apply heading
    meas=changeHeadingSource(meas,handles.HSource);
    meas.qa=clsQAData(meas);
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Set pointer to previous
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    guidata(hObject, handles);
    
    % Close winHSource
    delete (handles.output);  

function pbCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pbCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Close winHSource
    guidata(hObject, handles);
    delete (handles.output);  

function pbComment_Callback(hObject, eventdata, handles)
% hObject    handle to pbComment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Add comment
    [handles]=commentButton(handles, 'Depth');
    % Update handles structure
    guidata(hObject, handles);    
    
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    %open('helpFiles\QRev_Users_Manual.pdf') 
web('QRev_Help_Files\HTML\compass_p_r.htm')

% --- Executes when selected object is changed in bgHSource.
function bgHSource_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in bgHSource 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.rbInt,'Value')
    handles.HSource='internal';
else
    handles.HSource='external';
end
guidata(hObject, handles);
