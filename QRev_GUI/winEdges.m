function varargout = winEdges(varargin)
% WINEDGES MATLAB code for winEdges.fig
%      WINEDGES, by itself, creates a new WINEDGES or raises the existing
%      singleton*.
%
%      H = WINEDGES returns the handle to a new WINEDGES or the handle to
%      the existing singleton*.
%
%      WINEDGES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINEDGES.M with the given input arguments.
%
%      WINEDGES('Property','Value',...) creates a new WINEDGES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winEdges_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winEdges_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winEdges

% Last Modified by GUIDE v2.5 02-Feb-2016 13:09:25

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winEdges_OpeningFcn, ...
                   'gui_OutputFcn',  @winEdges_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function winEdges_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winEdges (see VARARGIN)

% Choose default command line output for winEdges
handles.output = hObject;

    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end % if
    
    setWindowPosition(hObject,handles);
    
    commentIcon(handles.pbComment);
    
    % Get table from gui
    tableData=get(handles.tblEdges,'Data');
    
    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    handles.checkedIdx=find([meas.transects.checked]==1);
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Set column numbers
    handles.colFilename=1;
    handles.colStartEdge=2;
    handles.colLeftType=3;
    handles.colLeftCoef=4;
    handles.colLeftDist=5;
    handles.colLeftNum=6;
    handles.colLeftValid=7;
    handles.colLeftQ=8;
    handles.colLeftPer=9;
    handles.colRightType=10;
    handles.colRightCoef=11;
    handles.colRightDist=12;
    handles.colRightNum=13;
    handles.colRightValid=14;
    handles.colRightQ=15;
    handles.colRightPer=16;
    % Set units for column names
    colNames=get(handles.tblEdges,'ColumnName');
    colNames{handles.colLeftDist}=[colNames{handles.colLeftDist},'| ',uLabelL];
    colNames{handles.colLeftQ}=[colNames{handles.colLeftQ},'| ',uLabelQ];
    colNames{handles.colRightDist}=[colNames{handles.colRightDist},'| ',uLabelL];
    colNames{handles.colRightQ}=[colNames{handles.colRightQ},'| ',uLabelQ];
    set(handles.tblEdges,'ColumnName',colNames); 
   
    tableData=updateTable(meas,handles);
    
    handles.plottedTransect=1;

    % Update tables
    handles=applyFormat(handles,tableData,meas);
        
    % Display velocity averaging method
    visibility=getappdata(handles.hMainGui,'InterpVisible');
    set(handles.rbpVelocity,'Visible',visibility);
    if strcmp(meas.transects(1).edges.velMethod,'MeasMag')
        set(handles.rbVelTRDI,'Value',1);
    else
        set(handles.rbVelSonTek,'Value',1);
    end % if
    
    % Display rectangular edge method
    set(handles.rbpVerticalEdge,'Visible',visibility);
    if strcmp(meas.transects(1).edges.recEdgeMethod,'Fixed')
        set(handles.rbRecEdTRDI,'Value',1);
    else
        set(handles.rbRecEdSonTek,'Value',1);
    end

     % Initialize handles variable for colorbar
     setappdata(handles.hMainGui,'colorbar',[nan,nan]);
     updateGraphics(handles);
% Update handles structure
guidata(hObject, handles);

function varargout = winEdges_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function tableData=updateTable(meas,handles)

    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Get data for table from each transect
    nTransects=length(handles.checkedIdx);
    for n=1:nTransects
        
        % Determine number of valid ensembles in each edge
        edgeIdxLeft=clsQComp.edgeEnsembles('left',meas.transects(handles.checkedIdx(n)));
        edgeIdxRight=clsQComp.edgeEnsembles('right',meas.transects(handles.checkedIdx(n)));
        validData=meas.transects(handles.checkedIdx(n)).wVel.validData(:,:,1);
        validLeft=validData(:,edgeIdxLeft);
        validRight=validData(:,edgeIdxRight);
        validLeftEns=any(validLeft);
        validRightEns=any(validRight);
        nValidLeft=sum(validLeftEns);
        nValidRight=sum(validRightEns);

        % Edges Table
        % ===========
        idx=find(meas.transects(handles.checkedIdx(n)).fileName=='\',1,'last');
        if isempty(idx)
            idx=0;
        end
        tableData{n,handles.colFilename}=meas.transects(handles.checkedIdx(n)).fileName(idx+1:end);
        tableData{n,handles.colStartEdge}=meas.transects(handles.checkedIdx(n)).startEdge;
        tableData{n,handles.colLeftType}=meas.transects(handles.checkedIdx(n)).edges.left.type;
        coef=clsQComp.edgeCoef('left',meas.transects(handles.checkedIdx(n)));
        tableData{n,handles.colLeftCoef}=sprintf('% 8.4f',coef);
        tableData{n,handles.colLeftDist}=sprintf('% 10.1f',meas.transects(handles.checkedIdx(n)).edges.left.dist_m.*unitsL);
        tableData{n,handles.colLeftNum}=sprintf('%6.0f',meas.transects(handles.checkedIdx(n)).edges.left.numEns2Avg);
        tableData{n,handles.colLeftValid}=sprintf('%6.0f',nValidLeft);
        tableData{n,handles.colLeftQ}=sprintf('% 12.2f',meas.discharge(handles.checkedIdx(n)).left.*unitsQ);
        tableData{n,handles.colLeftPer}=sprintf('%5.2f',(meas.discharge(handles.checkedIdx(n)).left./meas.discharge(handles.checkedIdx(n)).total).*100);
        tableData{n,handles.colRightType}=meas.transects(handles.checkedIdx(n)).edges.right.type;
        coef=clsQComp.edgeCoef('right',meas.transects(handles.checkedIdx(n)));
        tableData{n,handles.colRightCoef}=sprintf('% 8.4f', coef);
        tableData{n,handles.colRightDist}=sprintf('% 10.1f',meas.transects(handles.checkedIdx(n)).edges.right.dist_m.*unitsL);
        tableData{n,handles.colRightNum}=sprintf('%6.0f',meas.transects(handles.checkedIdx(n)).edges.right.numEns2Avg);
        tableData{n,handles.colRightValid}=sprintf('%6.0f',nValidRight);
        tableData{n,handles.colRightQ}=sprintf('% 12.2f',meas.discharge(handles.checkedIdx(n)).right.*unitsQ);
        tableData{n,handles.colRightPer}=sprintf('%5.2f',(meas.discharge(handles.checkedIdx(n)).right./meas.discharge(handles.checkedIdx(n)).total).*100);
        
    end % for n
   
    
function handles=applyFormat(handles,tableData,meas)
% Applys a format to the table that highlights values that have generated
% warnings or caution about the quality of the data and its affect on Q.
%
% INPUT:
%
% handles: handles data structure from figure
%
% tableData: data from table to be formatted
%
% meas: object of clsMeasurement
%
% OUTPUT:
%
% tableData: data for table that has been formatted.

    nTransects=length(handles.checkedIdx);
    
    % Check edge sign
    if meas.qa.edges.leftSign==1
        for n=1:nTransects
            
            tableData(n,handles.colLeftQ)={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tableData{n,handles.colLeftQ}),'</center></BODY></HTML>']};
        end
    end
          
    if meas.qa.edges.rightSign==1
        for n=1:nTransects
            tableData(n,handles.colRightQ)={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tableData{n,handles.colRightQ}),'</center></BODY></HTML>']};
        end
    end
    
    % Distance moved
    if ~isempty(meas.qa.edges.leftDistMovedIdx)
% 		formatIdx=intersect(meas.qa.edges.leftDistMovedIdx, handles.checkedIdx);
%         nIdx=length(formatIdx);   
%         for n=1:nIdx
        for n=1:length(meas.qa.edges.leftDistMovedIdx)
            k=meas.qa.edges.leftDistMovedIdx(n);
            tableData(k,handles.colLeftDist)={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tableData{k,handles.colLeftDist}),'</center></BODY></HTML>']};
            tableData(k,handles.colLeftNum)={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tableData{k,handles.colLeftNum}),'</center></BODY></HTML>']};
        end % for nIdx
    end % for leftDistMoved
    
    if ~isempty(meas.qa.edges.rightDistMovedIdx)
% 		formatIdx=intersect(meas.qa.edges.rightDistMovedIdx, handles.checkedIdx);
%         nIdx=length(formatIdx);
%         for n=1:nIdx
%             k=find(handles.checkedIdx==formatIdx(n));
        for n=1:length(meas.qa.edges.rightDistMovedIdx)
            k=meas.qa.edges.rightDistMovedIdx(n);
            tableData(k,handles.colRightDist)={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tableData{k,handles.colRightDist}),'</center></BODY></HTML>']};
            tableData(k,handles.colRightNum)={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tableData{k,handles.colRightNum}),'</center></BODY></HTML>']};
        end % for nIdx
    end % for rightDistMoved
    
    % Check edge = 0
    leftQ=[meas.discharge(handles.checkedIdx).left];
    leftZero=find(leftQ==0);
    if ~isempty(leftZero)
        for n=1:length(leftZero)
            idx1=strfind(tableData{leftZero(n),handles.colLeftQ},'<center>')+8;
            idx2=strfind(tableData{leftZero(n),handles.colLeftQ},'</')-1;
            if isempty(idx1)
                temp=tableData{leftZero(n),handles.colLeftQ};
            else
                temp=tableData{leftZero(n),handles.colLeftQ}(idx1:idx2);
            end
            tableData(leftZero(n),handles.colLeftQ)={['<HTML><BODY bgColor="red" width="50px"><strong><center>',num2str(temp),'</center></strong></BODY></HTML>']};
        end
    end
    
    rightQ=[meas.discharge(handles.checkedIdx).right];
    rightZero=find(rightQ==0);
    if ~isempty(rightZero)
        for n=1:length(rightZero)
            idx1=strfind(tableData{rightZero(n),handles.colRightQ},'<center>')+8;
            idx2=strfind(tableData{rightZero(n),handles.colRightQ},'</')-1;
            if isempty(idx1)
                temp=tableData{rightZero(n),handles.colRightQ};
            else
                temp=tableData{rightZero(n),handles.colRightQ}(idx1:idx2);
            end
            tableData(rightZero(n),handles.colRightQ)={['<HTML><BODY bgColor="red" width="50px"><strong><center>',num2str(temp),'</center></strong></BODY></HTML>']};
        end
    end
    
    % Check edge > 5%
    leftQper=([meas.discharge(handles.checkedIdx).left]./[meas.discharge(handles.checkedIdx).total]).*100;
    leftZero=find(leftQper>5);
    if ~isempty(leftZero)
        for n=1:length(leftZero)
            idx1=strfind(tableData{leftZero(n),handles.colLeftPer},'<center>')+8;
            idx2=strfind(tableData{leftZero(n),handles.colLeftPer},'</')-1;
            if isempty(idx1)
                temp=tableData{leftZero(n),handles.colLeftPer};
            else
                temp=tableData{leftZero(n),handles.colLeftPer}(idx1:idx2);
            end
            tableData(leftZero(n),handles.colLeftPer)={['<HTML><BODY bgColor="yellow" width="40px"><strong>',num2str(temp),'</strong></BODY></HTML>']};
        end
    end
    
    rightQper=([meas.discharge(handles.checkedIdx).right]./[meas.discharge(handles.checkedIdx).total]).*100;
    rightZero=find(rightQper>5);
    if ~isempty(rightZero)
        for n=1:length(rightZero)
            idx1=strfind(tableData{rightZero(n),handles.colRightPer},'<center>')+8;
            idx2=strfind(tableData{rightZero(n),handles.colRightPer},'</')-1;
            if isempty(idx1)
                temp=tableData{rightZero(n),handles.colRightPer};
            else
                temp=tableData{rightZero(n),handles.colRightPer}(idx1:idx2);
            end
            tableData(rightZero(n),handles.colRightPer)={['<HTML><BODY bgColor="yellow" width="40px"><strong>',num2str(temp),'</strong></BODY></HTML>']};
        end
    end
    
    % Check edge type
    if meas.qa.edges.leftType==2
        temp=['<HTML><BODY bgColor="red" width="70px"><strong><center><br /><br />Left<br />Type</center></strong></BODY></HTML>'];
    else
        temp=['<HTML><BODY><center><br />Left<br />Type</center></BODY></HTML>'];
    end
    colNames=get(handles.tblEdges,'ColumnName');
    colNames{handles.colLeftType}=temp;
    set(handles.tblEdges,'ColumnName',colNames);
    
    if meas.qa.edges.rightType==2
        temp=['<HTML><BODY bgColor="red" width="70px"><strong><center><br /><br />Right<br />Type</center></strong></BODY></HTML>'];
    else
        temp=['<HTML><BODY><center><br />Right<br />Type</center></BODY></HTML>'];
    end
    colNames=get(handles.tblEdges,'ColumnName');
    colNames{handles.colRightType}=temp;
    set(handles.tblEdges,'ColumnName',colNames);
    
    set(handles.tblEdges,'Data',tableData(1:nTransects,:));
    
function pbComment_Callback(hObject, eventdata, handles)
% Add comment
    [handles]=commentButton(handles, 'Edges');
    % Update handles structure
    guidata(hObject, handles);
    
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% open('helpFiles\QRev_Users_Manual.pdf')    
web('QRev_Help_Files\HTML\edges.htm')
    
function tblEdges_CellSelectionCallback(hObject, eventdata, handles)

    selection = eventdata.Indices(:,1);
    
    % If statement due to code executed twice. Not sure why but this fixes
    % the problem.
    if ~isempty(selection)
         % Get table data
         tableData=get(handles.tblEdges,'Data');
        
        % Retrieve data from hMainGui
        meas=getappdata(handles.hMainGui,'measurement');

        % Get units multiplier
        [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);    

        % Identify location in table being edited
        row=eventdata.Indices(1,1);
        col=eventdata.Indices(1,2);

        % col determines the parameter being edited
        switch col
            
            % Start edge
            % ----------
            case handles.colStartEdge 

                handles.hWinStartBank=winStartBank(handles.hMainGui,handles.figure1,tableData{row,col});
                set(handles.hWinStartBank,'WindowStyle','modal');
                % Wait for figure to close before proceeding
                waitfor(handles.hWinStartBank);
                
                % If Cancel don't process
                if ~strcmp(handles.figure1.UserData.process,'Cancel')
                    
                    % Set selected start bank
                    if strcmp(handles.figure1.UserData.bank,'Left')
                        setting='Left';
                    else
                        setting='Right';
                    end
                    
                    % Warn about potential issues
                    warnh=warndlg('You changed the start edge, verify that the left and right distances and edge types are correct.','Start Edge Change');
                    uiwait(warnh);
                    oldpointer = get(gcf, 'pointer');      
                    set(gcf, 'pointer', 'watch');     
                    drawnow;  
                    
                    % Determine range of transects to be processed
                    if strcmp(handles.figure1.UserData.process,'All')
                        nStart=1;
                        nEnd=size(tableData,1);
                    else
                        nStart=row;
                        nEnd=row;
                    end % if All
                    
                    for n=nStart:nEnd 
                        meas.transects(handles.checkedIdx(n))=changeStartEdge(meas.transects(handles.checkedIdx(n)),setting);
                    end % for n
                    
                    % Recompute discharge
                    s=clsMeasurement.currentSettings(meas);
                    meas=applySettings(meas,s);
                    set(gcf, 'pointer', oldpointer);     
                    drawnow;

                    % Update table
                    tableData=updateTable(meas,handles);
                    
                end % if Cancel
                % Update GUI 
                handles=applyFormat(handles,tableData,meas);                
                
            % Left edge type, coefficient, Q 
            % -------------------------------
            case {handles.colLeftType, handles.colLeftCoef, handles.colLeftQ}
                
                % Get information to pass to user input window
                if strcmpi(tableData{row,handles.colLeftType},'User Q') 
                    type='User Q';
                    value=str2double(tableData{row,handles.colLeftQ});
                elseif strcmpi(tableData{row,handles.colLeftType},'Custom')
                    type='Custom';
                    value=str2double(tableData{row,handles.colLeftCoef});
                else
                    type=tableData{row,handles.colLeftType};
                    value=[];
                end
                
                % Open window for user input
                handles.hWinEdgeType=winEdgeType(handles.hMainGui,handles.figure1,'Left',type,value);
                set(handles.hWinEdgeType,'WindowStyle','modal');

                % Wait for figure to close before proceeding
                waitfor(handles.hWinEdgeType);
                
                % If Cancel don't process
                if ~strcmp(handles.figure1.UserData.process,'Cancel')
  
                    % Begin processing
                    oldpointer = get(gcf, 'pointer');      
                    set(gcf, 'pointer', 'watch');     
                    drawnow;     
                
                    % Determine range of transects to be processed
                    if strcmp(handles.figure1.UserData.process,'All')
                        nStart=1;
                        nEnd=size(tableData,1);
                    else
                        nStart=row;
                        nEnd=row;
                    end % if All
                    
                    for n=nStart:nEnd 
                        meas.transects(handles.checkedIdx(n))=changeEdge(meas.transects(handles.checkedIdx(n)),'left','type',handles.figure1.UserData.type);
                        % Set editable properties of table
                        if strcmp(handles.figure1.UserData.type, 'Custom')
                            meas.transects(handles.checkedIdx(n))=changeEdge(meas.transects(handles.checkedIdx(n)),'left','custCoef',handles.figure1.UserData.value);
                         elseif strcmp(handles.figure1.UserData.type,'User Q')
                            meas.transects(handles.checkedIdx(n))=changeEdge(meas.transects(handles.checkedIdx(n)),'left','userQ_cms',handles.figure1.UserData.value);
                        end % if Custom
                    end % for n

                    % Recompute discharge
                    s=clsMeasurement.currentSettings(meas);
                    meas=applySettings(meas,s);
                    set(gcf, 'pointer', oldpointer);     
                    drawnow;
                    
                    tableData=updateTable(meas,handles);
                end
                % Update GUI 
                handles=applyFormat(handles,tableData,meas);                
                
            % Left distance
            % -------------
            case handles.colLeftDist 
                % Open window for user input
                handles.hWinEdgeDistNum=winEdgeDistNum(handles.hMainGui,handles.figure1,'Left ','Edge Distance',meas.transects(handles.checkedIdx(row)).edges.left.dist_m);
                set(handles.hWinEdgeDistNum,'WindowStyle','modal');

                % Wait for figure to close before proceeding
                waitfor(handles.hWinEdgeDistNum);

                % If Cancel don't process
                if ~strcmp(handles.figure1.UserData.process,'Cancel')

                    oldpointer = get(gcf, 'pointer');      
                    set(gcf, 'pointer', 'watch');     
                    drawnow; 

                    % Determine range of transects to be processed
                    if strcmp(handles.figure1.UserData.process,'All')
                        nStart=1;
                        nEnd=size(tableData,1);
                    else
                        nStart=row;
                        nEnd=row;
                    end % if All                

                    for n=nStart:nEnd 
                        meas.transects(handles.checkedIdx(n))=changeEdge(meas.transects(handles.checkedIdx(n)),'left','dist_m',handles.figure1.UserData.value);
                    end % for n

                    % Recompute discharge
                    s=clsMeasurement.currentSettings(meas);
                    meas=applySettings(meas,s);
                    set(gcf, 'pointer', oldpointer);     
                    drawnow;

                    tableData=updateTable(meas,handles);
                end % if Cancel
                % Update GUI 
                handles=applyFormat(handles,tableData,meas);            
             
            % Left # ens to average 
            % ---------------------
            case handles.colLeftNum

                % Open window for user input
                handles.hWinEdgeDistNum=winEdgeDistNum(handles.hMainGui,handles.figure1,'Left',' Number of Ensembles',meas.transects(handles.checkedIdx(row)).edges.left.numEns2Avg);
                set(handles.hWinEdgeDistNum,'WindowStyle','modal');

                % Wait for figure to close before proceeding
                waitfor(handles.hWinEdgeDistNum);

                % If Cancel don't process
                if ~strcmp(handles.figure1.UserData.process,'Cancel')
                    oldpointer = get(gcf, 'pointer');      
                    set(gcf, 'pointer', 'watch');     
                    drawnow;  

                    % Determine range of transects to be processed
                    if strcmp(handles.figure1.UserData.process,'All')
                        nStart=1;
                        nEnd=size(tableData,1);
                    else
                        nStart=row;
                        nEnd=row;
                    end % if All 

                    for n=nStart:nEnd 
                        meas.transects(handles.checkedIdx(n))=changeEdge(meas.transects(handles.checkedIdx(n)),'left','numEns2Avg',handles.figure1.UserData.value);
                    end % for n

                    % Recompute discharge
                    s=clsMeasurement.currentSettings(meas);
                    meas=applySettings(meas,s);
                    set(gcf, 'pointer', oldpointer);     
                    drawnow;

                    tableData=updateTable(meas,handles);

                end % if Cancel
                % Update GUI 
                handles=applyFormat(handles,tableData,meas);

                % Right edge type, coefficient, Q 
                % -------------------------------
                case {handles.colRightType, handles.colRightCoef, handles.colRightQ}

                    % Get information to pass to user input window
                    if strcmpi(tableData{row,handles.colRightType},'User Q') 
                        type='User Q';
                        value=str2double(tableData{row,handles.colRightQ});
                    elseif strcmpi(tableData{row,handles.colRightType},'Custom')
                        type='Custom';
                        value=str2double(tableData{row,handles.colRightCoef});
                    else
                        type=tableData{row,handles.colRightType};
                        value=[];
                    end

                    % Open window for user input
                    handles.hWinEdgeType=winEdgeType(handles.hMainGui,handles.figure1,'Right',type,value);
                    set(handles.hWinEdgeType,'WindowStyle','modal');

                    % Wait for figure to close before proceeding
                    waitfor(handles.hWinEdgeType);

                    % If Cancel don't process
                    if ~strcmp(handles.figure1.UserData.process,'Cancel')

                        % Begin processing
                        oldpointer = get(gcf, 'pointer');      
                        set(gcf, 'pointer', 'watch');     
                        drawnow;     

                        % Determine range of transects to be processed
                        if strcmp(handles.figure1.UserData.process,'All')
                            nStart=1;
                            nEnd=size(tableData,1);
                        else
                            nStart=row;
                            nEnd=row;
                        end % if All

                        for n=nStart:nEnd 
                            meas.transects(handles.checkedIdx(n))=changeEdge(meas.transects(handles.checkedIdx(n)),'right','type',handles.figure1.UserData.type);
                            % Set editable properties of table
                            if strcmp(handles.figure1.UserData.type, 'Custom')
                                meas.transects(handles.checkedIdx(n))=changeEdge(meas.transects(handles.checkedIdx(n)),'right','custCoef',handles.figure1.UserData.value);
                             elseif strcmp(handles.figure1.UserData.type,'User Q')
                                meas.transects(handles.checkedIdx(n))=changeEdge(meas.transects(handles.checkedIdx(n)),'right','userQ_cms',handles.figure1.UserData.value);
                            end % if Custom
                        end % for n

                        % Recompute discharge
                        s=clsMeasurement.currentSettings(meas);
                        meas=applySettings(meas,s);
                        set(gcf, 'pointer', oldpointer);     
                        drawnow;

                        tableData=updateTable(meas,handles);
                    end
                % Update GUI 
                handles=applyFormat(handles,tableData,meas);                

            % Right distance
            % -------------
            case handles.colRightDist 
                % Open window for user input
                handles.hWinEdgeDistNum=winEdgeDistNum(handles.hMainGui,handles.figure1,'Right ','Edge Distance',meas.transects(handles.checkedIdx(row)).edges.right.dist_m);
                set(handles.hWinEdgeDistNum,'WindowStyle','modal');

                % Wait for figure to close before proceeding
                waitfor(handles.hWinEdgeDistNum);

                % If Cancel don't process
                if ~strcmp(handles.figure1.UserData.process,'Cancel')

                    oldpointer = get(gcf, 'pointer');      
                    set(gcf, 'pointer', 'watch');     
                    drawnow; 

                    % Determine range of transects to be processed
                    if strcmp(handles.figure1.UserData.process,'All')
                        nStart=1;
                        nEnd=size(tableData,1);
                    else
                        nStart=row;
                        nEnd=row;
                    end % if All                

                    for n=nStart:nEnd 
                        meas.transects(handles.checkedIdx(n))=changeEdge(meas.transects(handles.checkedIdx(n)),'right','dist_m',handles.figure1.UserData.value);
                    end % for n

                    % Recompute discharge
                    s=clsMeasurement.currentSettings(meas);
                    meas=applySettings(meas,s);
                    set(gcf, 'pointer', oldpointer);     
                    drawnow;

                    tableData=updateTable(meas,handles);
                end % if Cancel
                % Update GUI 
                handles=applyFormat(handles,tableData,meas);            

            % Right # ens to average 
            % ---------------------
            case handles.colRightNum

                % Open window for user input
                handles.hWinEdgeDistNum=winEdgeDistNum(handles.hMainGui,handles.figure1,'Right',' Number of Ensembles',meas.transects(handles.checkedIdx(row)).edges.right.numEns2Avg);
                set(handles.hWinEdgeDistNum,'WindowStyle','modal');

                % Wait for figure to close before proceeding
                waitfor(handles.hWinEdgeDistNum);

                % If Cancel don't process
                if ~strcmp(handles.figure1.UserData.process,'Cancel')
                    oldpointer = get(gcf, 'pointer');      
                    set(gcf, 'pointer', 'watch');     
                    drawnow;  

                    % Determine range of transects to be processed
                    if strcmp(handles.figure1.UserData.process,'All')
                        nStart=1;
                        nEnd=size(tableData,1);
                    else
                        nStart=row;
                        nEnd=row;
                    end % if All 

                    for n=nStart:nEnd 
                        meas.transects(handles.checkedIdx(n))=changeEdge(meas.transects(handles.checkedIdx(n)),'right','numEns2Avg',handles.figure1.UserData.value);
                    end % for n

                    % Recompute discharge
                    s=clsMeasurement.currentSettings(meas);
                    meas=applySettings(meas,s);
                    set(gcf, 'pointer', oldpointer);     
                    drawnow;

                    tableData=updateTable(meas,handles); 
                end % if Cancel
                % Update GUI 
                handles=applyFormat(handles,tableData,meas);
        end
        handles.plottedTransect=selection;
        set(handles.plottedtxt,'String',tableData{row,1});
        drawnow
    end
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    % Update graphics
    updateGraphics(handles)

    drawnow


%     updateGraphics (handles)
    guidata(hObject, handles);   
    
    
% Radio buttons

function rbpVelocity_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in rbpVelocity 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
% NOT USED dsm 9/16/2016
    % Retrieve data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    tblEdgesData=get(handles.tblEdges,'Data');
    nTransects=length(meas.transects);
    
    if get(handles.rbVelTRDI,'value')
        setting='MeasMag';
    else
        setting='VectorProf';
    end % if
    for n=1:nTransects
        meas.transects(n).edges=changeProperty(meas.transects(n).edges,'velMethod',setting);  
    end % for n
    meas=updateQ(meas);
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles); 
    
    for n=1:nTransects
       tblEdgesData{n+1,7}=sprintf('% 10.2f',meas.discharge(n).left.*unitsQ);
       tblEdgesData{n+1,12}=sprintf('% 10.2f',meas.discharge(n).right.*unitsQ);
    end % for
   
    % Update GUI 
    set(handles.tblEdges,'Data',tblEdgesData);
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    setappdata(handles.hMainGui,'discharge',discharge);

function rbpVerticalEdge_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in rbpVerticalEdge 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
% NOT USED dsm 9/16/2016
    % Retrieve data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    tblEdgesData=get(handles.tblEdges,'Data');
    nTransects=length(meas.transects);
    
    if get(handles.rbRecEdTRDI,'value')
        setting='Fixed';
    else
        setting='Variable';
    end % if
    for n=1:nTransects
        meas.transects(n).edges=changeProperty(meas.transects(n).edges,'recEdgeMethod',setting);  
    end % for n
    meas=updateQ(meas);
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);     
    
    for n=1:nTransects
        tblEdgesData{n+1,4}=sprintf('% 6.4f',clsQComp.edgeCoef('left',meas.transects(n)));
        tblEdgesData{n+1,9}=sprintf('% 6.4f',clsQComp.edgeCoef('right',meas.transects(n)));
        tblEdgesData{n+1,7}=sprintf('% 10.2f',meas.discharge(n).left.*unitsQ);
        tblEdgesData{n+1,12}=sprintf('% 10.2f',meas.discharge(n).right.*unitsQ);
    end % for
   
    % Update GUI 
    set(handles.tblEdges,'Data',tblEdgesData);
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);

% Graphics
function updateGraphics (handles)

    % Clear axes
    cla(handles.axContL);
    cla(handles.axContR);
    cla(handles.axShipL);
    cla(handles.axShipR);
    
    % Get settings from GUI
    tableData=get(handles.tblEdges,'Data');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);     
    
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    transectSelected=handles.checkedIdx(handles.plottedTransect);
    set(handles.plottedtxt,'String',tableData{handles.plottedTransect,1});
    transect=meas.transects(transectSelected);
    validData=transect.wVel.validData;
    selectedRef=meas.transects(transectSelected).boatVel.selected;
    
    % Compute processed track
    ensDuration=transect.dateTime.ensDuration_sec;
    if ~isempty(transect.boatVel.(selectedRef))
        boatUProcessed=transect.boatVel.(selectedRef).uProcessed_mps;
        boatVProcessed=transect.boatVel.(selectedRef).vProcessed_mps;
    else
        boatUProcessed=nan(size(transect.boatVel.btVel.uProcessed_mps));
        boatVProcessed=nan(size(transect.boatVel.btVel.vProcessed_mps));
    end
    boatXProcessed=nancumsum(boatUProcessed.*ensDuration);
    boatYProcessed=nancumsum(boatVProcessed.*ensDuration);
    ensembles=1:length(boatUProcessed);
    
    % Prep data needed for plots
    allValidData=validData(:,:,1);
    nanValidData=double(allValidData);
    nanValidData(nanValidData==0)=nan;
    % Compute water speed
    wVelX=transect.wVel.uProcessed_mps.*nanValidData;
    wVelY=transect.wVel.vProcessed_mps.*nanValidData;
    meanVelX=nanmean(wVelX);
    meanVelY=nanmean(wVelY);
    wSpeed=sqrt(wVelX.^2+wVelY.^2);

    ensembleMatrix=repmat(ensembles,size(allValidData,1),1);
    depthCellMatrix=transect.depths.btDepths.depthCellDepth_m;
    depthCellSize=transect.depths.btDepths.depthCellSize_m;
    
    % Setup axes handles as variables
    hContour=[handles.axContL,handles.axContR];
    hShip=[handles.axShipL,handles.axShipR];
    c=getappdata(handles.hMainGui,'colorbar');
    if ~isnan(c(1))
        delete(c(1))
        delete(c(2))
    end
    % Loop to plot left and right
    for n=1:2
        
        % Get appropriate left and right data
        if n==1
            edgeIdx=clsQComp.edgeEnsembles('left',transect);
            if ~isempty(edgeIdx)
                if strcmpi(transect.startEdge,'left')
                    edgeEnsIdx=1:edgeIdx(end);
                    boatX=boatXProcessed(edgeEnsIdx)-boatXProcessed(edgeEnsIdx(1));
                    boatY=boatYProcessed(edgeEnsIdx)-boatYProcessed(edgeEnsIdx(1));
                else
                    edgeEnsIdx=edgeIdx(1):ensembles(end);
                    boatX=boatXProcessed(edgeEnsIdx)-boatXProcessed(edgeEnsIdx(end));
                    boatY=boatYProcessed(edgeEnsIdx)-boatYProcessed(edgeEnsIdx(end));
                end
            else
                boatX=nan;
                boatY=nan;
            end
            
        else
            edgeIdx=clsQComp.edgeEnsembles('right',transect);
            if ~isempty(edgeIdx)
                if strcmpi(transect.startEdge,'right')
                    edgeEnsIdx=1:edgeIdx(end);
                    boatX=boatXProcessed(edgeEnsIdx)-boatXProcessed(edgeEnsIdx(1));
                    boatY=boatYProcessed(edgeEnsIdx)-boatYProcessed(edgeEnsIdx(1));
                else
                    edgeEnsIdx=edgeIdx(1):ensembles(end);
                    boatX=boatXProcessed(edgeEnsIdx)-boatXProcessed(edgeEnsIdx(end));
                    boatY=boatYProcessed(edgeEnsIdx)-boatYProcessed(edgeEnsIdx(end));
                end
            else
                boatX=nan;
                boatY=nan;
            end
        end
        
        if ~isempty(edgeIdx)
            validDataTest=find(~isnan(meanVelX(edgeEnsIdx)));
        else
            validDataTest=[];
        end

        if ~isempty(validDataTest)
            % Plot shiptrack
            plot(hShip(n),boatX.*unitsL,boatY.*unitsL,'-r','LineWidth',3)
            hold(hShip(n),'on')
            quiver(hShip(n),boatX.*unitsL,boatY.*unitsL,meanVelX(edgeEnsIdx).*unitsV,meanVelY(edgeEnsIdx).*unitsV,'b','ShowArrowHead','on');
            set(hShip(n),'DataAspectRatio',[1 1 1]);
            set(hShip(n),'Box','on');
            set(hShip(n),'PlotBoxAspectRatio',[1 1 1]);
            shore=plot(hShip(n),0,0,'sk','MarkerFaceColor','k');
            hold(hShip(n),'off');
            xlabel(hShip(n),['Distance East ',uLabelL]);
            ylabel(hShip(n),['Distance North ',uLabelL]);
            legend(shore,'Near-shore','Location','Northoutside');
            grid(hShip(n),'On')
            % Plot contour

            set(handles.figure1,'CurrentAxes',hContour(n));
            hold (hContour(n),'off')
            wVelPlt=wSpeed(:,edgeEnsIdx);
            maxLimit=nanmax(nanmax(wVelPlt));
            minLimit=-1.*maxLimit/64;
            caxis([minLimit.*unitsV,maxLimit.*unitsV]);
            wVelPlt(~allValidData(:,edgeEnsIdx))=-999;
            trackPlt=ensembleMatrix(:,edgeEnsIdx);
            depthCellPlt=depthCellMatrix(:,edgeEnsIdx);
            colormap('jet');
            cmap=ones(size(colormap,1)+1,size(colormap,2));
            cmap(2:end,:)=colormap;
            colormap(cmap);
            maxBins=nanmax(nansum(allValidData(:,edgeEnsIdx),1));
            depthPlt=transect.depths.(transect.depths.selected).depthProcessed_m(edgeEnsIdx);
            [trackPlt,depthCellPlt,wVelPlt,depthPlt]=contourDataPrep(trackPlt,depthCellPlt,depthCellSize(:,edgeEnsIdx),wVelPlt,depthPlt);
            h_surface=surface(trackPlt,depthCellPlt.*unitsL,wVelPlt.*unitsV);
            hold(hContour(n),'on')
            
            trackPlt2=ensembles(edgeEnsIdx);
            plot(hContour(n),trackPlt,depthPlt.*unitsL,'-k')            

            set(h_surface,'EdgeColor','none');
            set(hContour(n),'YDir','reverse');
            colorbar('Location','Northoutside');
            title(hContour(n),['Water Speed ',uLabelV]); 
            xlabel(hContour(n),'Ensemble');
            ylabel(hContour(n),['Depth ',uLabelL]);
            ylim(hContour(n),[0,nanmax(nanmax(depthPlt.*unitsL.*1.1))]);
            xlim(hContour(n), [nanmin(trackPlt2)-nanmax(trackPlt2).*0.01, nanmax(trackPlt2)+nanmax(trackPlt2).*0.01]);
            box('on');
            if strcmpi(transect.startEdge,'right')
                set(hContour(n),'XDir','reverse');
            else
                set(hContour(n),'XDir','normal');
            end
        end

    end % for n
       box(handles.axContL,'on');
    box(handles.axContR,'on');
    box(handles.axShipL,'on');
    box(handles.axShipR,'on');
    setappdata(handles.hMainGui,'colorbar',c);
    
function [trackPlt,depthCellPlt,wVelPlt,depthPlt]=contourDataPrep(track,cellDepth,cellSize,vel,depth)
    nMax=size(track,2);
    j=0;
    k=0;
    for n=1:nMax
        j=j+1;
        if n==1
            halfBack=abs(0.5.*(track(:,n+1)-track(:,n)));
            halfForward=abs(0.5.*(track(:,n+1)-track(:,n)));
        elseif n==nMax
            halfForward=abs(0.5.*(track(:,n)-track(:,n-1)));
            halfBack=abs(0.5.*(track(:,n)-track(:,n-1)));
        else
            halfBack=abs(0.5.*(track(:,n)-track(:,n-1)));
            halfForward=abs(0.5.*(track(:,n+1)-track(:,n)));
        end
        trackNew(:,j)=track(:,n)-halfBack;
        cellDepthNew(:,j)=cellDepth(:,n);
        velNew(:,j)=vel(:,n);
        cellSizeNew(:,j)=cellSize(:,n);
        depthPltNew(j)=depth(n);
        j=j+1;
        trackNew(:,j)=track(:,n)+halfForward;
        cellDepthNew(:,j)=cellDepth(:,n);
        velNew(:,j)=vel(:,n);
        cellSizeNew(:,j)=cellSize(:,n);
        depthPltNew(j)=depth(n);
    end

    nMax=size(cellSize,1);
    j=0;
    for n=1:nMax
        j=j+1;
        trackPlt(j,:)=trackNew(n,:);
        depthCellPlt(j,:)=cellDepthNew(n,:)-0.5.*cellSizeNew(n,:);
        wVelPlt(j,:)=velNew(n,:);
        j=j+1;
        trackPlt(j,:)=trackNew(n,:);
        depthCellPlt(j,:)=cellDepthNew(n,:)+0.5.*cellSizeNew(n,:);
        wVelPlt(j,:)=velNew(n,:);
    end       
    depthPlt=depthPltNew;
    
% --- Executes on button press in pbClose.
function pbClose_Callback(hObject, eventdata, handles)
% hObject    handle to pbClose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    delete (handles.output);  

% --- Executes on key press with focus on tblDepths and none of its controls.
function tblEdges_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to tblDepths (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

% Make table inactive to avoid synching issues associated with multiple
% keypresses before the processing of the first can be completed.
set(handles.tblEdges,'Enable','inactive');

if strcmp(eventdata.Key,'uparrow') || strcmp(eventdata.Key,'downarrow')
    
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    tableData=get(handles.tblEdges,'Data');
    rowTrue=handles.plottedTransect;
    if strcmp(eventdata.Key,'downarrow')
        if rowTrue+1<=size(tableData,1)
            newRow=rowTrue+1;          
        else
            newRow=size(tableData,1);
        end
    elseif strcmp(eventdata.Key,'uparrow')
        if rowTrue-1>0
            newRow=rowTrue-1;
        else
            newRow=1;
        end
    end
      
    set(handles.plottedtxt,'String',tableData{newRow,1});
    handles.plottedTransect=newRow;

    % Update graphics
    updateGraphics(handles)
    
    % These two lines removes highlighting of Filename which always
    % defaults to the 1st row even when a different row is graphed.
%     meas=getappdata(handles.hMainGui,'measurement');
%     handles=applyFormat(handles,tableData,meas);

    drawnow
    
    set(gcf, 'pointer', oldpointer);

    
    % Update handles structure
    guidata(hObject, handles);
    drawnow
end   
set(handles.tblEdges,'Enable','on');

