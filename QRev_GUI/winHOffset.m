function varargout = winHOffset(varargin)
% WINHOFFSET MATLAB code for winHOffset.fig
%      WINHOFFSET, by itself, creates a new WINHOFFSET or raises the existing
%      singleton*.
%
%      H = WINHOFFSET returns the handle to a new WINHOFFSET or the handle to
%      the existing singleton*.
%
%      WINHOFFSET('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINHOFFSET.M with the given input arguments.
%
%      WINHOFFSET('Property','Value',...) creates a new WINHOFFSET or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winHOffset_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winHOffset_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winHOffset

% Last Modified by GUIDE v2.5 14-Feb-2017 10:17:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winHOffset_OpeningFcn, ...
                   'gui_OutputFcn',  @winHOffset_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function winHOffset_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winHOffset (see VARARGIN)

% Choose default command line output for winHOffset
handles.output = hObject;

    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end
    
    % Get transect selected
    handles.selected=varargin{hInput+2};
    
    % Set the window position to lower left
    setWindowPosition(hObject,handles);
    
    % Change comment icon
    commentIcon(handles.pbComment);
    
    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get selected heading source
    hSource=meas.transects(handles.selected).sensors.heading_deg.selected; 
       
    % Display warning if heading source not internal
    if ~strcmp(hSource,'external')
        set(handles.txtWarn,'String','Heading source is internal. This setting only applies to external heading source.')
    end
    % Display the current Offset
    handles.HOffset=meas.transects(handles.selected).sensors.heading_deg.(hSource).alignCorrection_deg;
    set(handles.edHOffset,'String',sprintf('% 8.2f',handles.HOffset));
    
% Update handles structure
guidata(hObject, handles);

function varargout = winHOffset_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function edHOffset_Callback(hObject, eventdata, handles)
% hObject    handle to edHOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Get user input
    handles.HOffset=str2double(get(handles.edHOffset,'String'));
    guidata(hObject, handles);

function edHOffset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edHOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pbOne_Callback(hObject, eventdata, handles)
% hObject    handle to pbOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
      
    % Get new offset
    handles.HOffset=str2double(get(handles.edHOffset,'String'));

    % Apply offset
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    meas=changeOffset(meas,handles.HOffset,handles.selected);     
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    meas.qa=clsQAData(meas);
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
   
    % Return pointer to previous
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    guidata(hObject, handles);
    
    % Close winHOffset
    delete (handles.output);  

function pbAll_Callback(hObject, eventdata, handles)
% hObject    handle to pbAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
     
    % Get new offset
    handles.HOffset=str2double(get(handles.edHOffset,'String'));
    
    % Apply draft
    meas=changeOffset(meas,handles.HOffset);
    meas.qa=clsQAData(meas);
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Set pointer to previous
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    guidata(hObject, handles);
    
    % Close winHOffset
    delete (handles.output);  

function pbCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pbCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Close winHOffset
    guidata(hObject, handles);
    delete (handles.output);  

function pbComment_Callback(hObject, eventdata, handles)
% hObject    handle to pbComment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Add comment
    [handles]=commentButton(handles, 'Depth');
    % Update handles structure
    guidata(hObject, handles);    
    
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    %open('helpFiles\QRev_Users_Manual.pdf') 
web('QRev_Help_Files\HTML\compass_p_r.htm')
