function varargout = winOptions(varargin)
% WINOPTIONS MATLAB code for winOptions.fig
%      WINOPTIONS, by itself, creates a new WINOPTIONS or raises the existing
%      singleton*.
%
%      H = WINOPTIONS returns the handle to a new WINOPTIONS or the handle to
%      the existing singleton*.
%
%      WINOPTIONS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINOPTIONS.M with the given input arguments.
%
%      WINOPTIONS('Property','Value',...) creates a new WINOPTIONS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winOptions_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winOptions_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winOptions

% Last Modified by GUIDE v2.5 27-Nov-2017 14:59:27

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winOptions_OpeningFcn, ...
                   'gui_OutputFcn',  @winOptions_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before winOptions is made visible.
function winOptions_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winOptions (see VARARGIN)

% Choose default command line output for winOptions
handles.output = hObject;
handles.hMainGui=varargin{1};
handles.parent=varargin{2};
handles.saveChecked=varargin{3};
handles.styleSheet=varargin{4};

setWindowPosition(hObject,handles);

displayUnits=getappdata(handles.hMainGui,'Units');

if strcmp(displayUnits,'English')
    set(handles.rbEng,'Value',1)
else
    set(handles.rbSI,'Value',1)
end

if handles.saveChecked
    set(handles.rbChecked,'Value',1)
else
    set(handles.rbAll,'Value',1)
end

if handles.styleSheet
    set(handles.cbStyle,'Value',1)
else
    set(handles.cbStyle,'Value',0)
end

commentIcon(handles.pbComment);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes winOptions wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = winOptions_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pbOK.
function pbOK_Callback(hObject, eventdata, handles)
% hObject    handle to pbOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
unitsIndicator=get(handles.rbEng,'Value');
if unitsIndicator==0
    options.displayUnits='SI';
else
    options.displayUnits='English';
end

saveIndicator=get(handles.rbAll,'Value');
if saveIndicator==0
    options.saveChecked=true;
else
    options.saveChecked=false;
end

styleIndicator=get(handles.cbStyle,'Value');
if styleIndicator==0
    options.styleSheet=false;
else
    options.styleSheet=true;
end
 
handles.parent.UserData=options;

% Update handles structure
guidata(hObject, handles);
    delete (handles.output);
% --- Executes on button press in pbComment.
function pbComment_Callback(hObject, eventdata, handles)
% hObject    handle to pbComment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pbHelp.
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in cbStyle.
function cbStyle_Callback(hObject, eventdata, handles)
% hObject    handle to cbStyle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbStyle

