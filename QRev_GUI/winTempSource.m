function varargout = winTempSource(varargin)
% WINTEMPSOURCE MATLAB code for winTempSource.fig
%      WINTEMPSOURCE, by itself, creates a new WINTEMPSOURCE or raises the existing
%      singleton*.
%
%      H = WINTEMPSOURCE returns the handle to a new WINTEMPSOURCE or the handle to
%      the existing singleton*.
%
%      WINTEMPSOURCE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINTEMPSOURCE.M with the given input arguments.
%
%      WINTEMPSOURCE('Property','Value',...) creates a new WINTEMPSOURCE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winTempSource_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winTempSource_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winTempSource

% Last Modified by GUIDE v2.5 21-Jul-2016 14:00:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winTempSource_OpeningFcn, ...
                   'gui_OutputFcn',  @winTempSource_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function winTempSource_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winTempSource (see VARARGIN)

    % Choose default command line output for winTempSource
    handles.output = hObject;

    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end
    
    % Get transect selected
    handles.selected=varargin{hInput+2};
    handles.tempUnits=varargin{hInput+3};
    
    % Set the window position to lower left
    setWindowPosition(hObject,handles);
    
    % Change comment icon
    commentIcon(handles.pbComment);
    
    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Display the current temperature source
    set(handles.edUser,'Enable','off');
    set(handles.txtUser,'String',handles.tempUnits);
    handles.tempSource=meas.transects(handles.selected).sensors.temperature_degC.selected;
    if strcmpi(handles.tempSource,'internal')
        set(handles.rbInt,'Value',1)
    elseif strcmpi(handles.tempSource,'external')
        set(handles.rbExt,'Value',1)
    elseif strcmpi(handles.tempSource,'user')
        set(handles.rbUser,'Value',1)
        set(handles.edUser,'Enable','on');

    end
    if ~isempty(meas.transects(handles.selected).sensors.temperature_degC.user)
        handles.userTemp=meas.transects(handles.selected).sensors.temperature_degC.user.data(1);
        if strcmp(handles.tempUnits,'C')
            handles.userTempFormatted=sprintf('% 4.1f',handles.userTemp);
        else
            handles.userTempFormatted=sprintf('% 4.1f',((9./5).*handles.userTemp)+32);
        end
        set(handles.edUser,'String',handles.userTempFormatted);
    end
    

    if isempty(get(handles.edUser,'String'))
        set(handles.pbOne,'Enable','off');
        set(handles.pbAll,'Enable','off');
    end
    
    if isempty(meas.transects(handles.selected).sensors.temperature_degC.external)
        set(handles.rbExt,'Enable','off');
    end
    
    % Update handles structure
    guidata(hObject, handles);

function varargout = winTempSource_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes when selected object is changed in bgHSource.
function bgHSource_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in bgHSource 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    if get(handles.rbInt,'Value')
        handles.tempSource='internal';
    elseif get(handles.rbExt,'Value')
        handles.tempSource='external';
    elseif get(handles.rbUser,'Value')
        handles.tempSource='user';  
        set(handles.edUser,'Enable','on')
        if isempty(get(handles.edUser,'String'))
            set(handles.pbOne,'Enable','off');
            set(handles.pbAll,'Enable','off');
        end
    end
guidata(hObject, handles);



function edUser_Callback(hObject, eventdata, handles)
% hObject    handle to edUser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    handles.userTemp=str2double(get(handles.edUser,'String'));
    if strcmp(handles.tempUnits,'F')
        handles.userTemp=(5./9).*(handles.userTemp-32);
    end
    set(handles.pbOne,'Enable','on');
    set(handles.pbAll,'Enable','on');
    guidata(hObject, handles);

function pbOne_Callback(hObject, eventdata, handles)
% hObject    handle to pbOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Apply new temperature
    meas=changeSOS(meas,handles.selected,'temperatureSrc',handles.tempSource); 
    if strcmp(handles.tempSource,'user')      
        meas=changeSOS(meas,handles.selected,'temperature',handles.userTemp);      
    end
    
    % Reset cursor
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Set pointer to previous
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    guidata(hObject, handles);
    
    % Close winTempSource
    delete (handles.output);   

function pbAll_Callback(hObject, eventdata, handles)
% hObject    handle to pbAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Set busy cursor
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;   
    
    % Apply new temperature
    meas=changeSOS(meas,'temperatureSrc',handles.tempSource); 
    if strcmp(handles.tempSource,'user')      
        meas=changeSOS(meas,'temperature',handles.userTemp);      
    end
    
    % Reset cursor
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Set pointer to previous
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    guidata(hObject, handles);
    
    % Close winTempSource
    delete (handles.output);  

function pbCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pbCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Close winTempSource
    guidata(hObject, handles);
    delete (handles.output);  

function pbComment_Callback(hObject, eventdata, handles)
% hObject    handle to pbComment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Add comment
    [handles]=commentButton(handles, 'Temperature');
    % Update handles structure
    guidata(hObject, handles);    
    
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    %open('helpFiles\QRev_Users_Manual.pdf') 
web('QRev_Help_Files\HTML\temp_salinity.htm')


% --- Executes during object creation, after setting all properties.
function edUser_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edUser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
