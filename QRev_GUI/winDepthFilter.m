function varargout = winDepthFilter(varargin)
% WINDEPTHFILTER MATLAB code for winDepthFilter.fig
%      WINDEPTHFILTER, by itself, creates a new WINDEPTHFILTER or raises the existing
%      singleton*.
%
%      H = WINDEPTHFILTER returns the handle to a new WINDEPTHFILTER or the handle to
%      the existing singleton*.
%
%      WINDEPTHFILTER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINDEPTHFILTER.M with the given input arguments.
%
%      WINDEPTHFILTER('Property','Value',...) creates a new WINDEPTHFILTER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winDepthFilter_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winDepthFilter_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winDepthFilter

% Last Modified by GUIDE v2.5 02-Feb-2016 13:08:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winDepthFilter_OpeningFcn, ...
                   'gui_OutputFcn',  @winDepthFilter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before winDepthFilter is made visible.
function winDepthFilter_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winDepthFilter (see VARARGIN)

% Choose default command line output for winDepthFilter
handles.output = hObject;

    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end
    
    setWindowPosition(hObject,handles);
    
    commentIcon(handles.pbComment);
    
    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    discharge=meas.discharge;
    oldDischarge=discharge;
    handles.checkedIdx=find([meas.transects.checked]==1);
    selectionOptions={};
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Set units for column names
    colNames=get(handles.tblDepths,'ColumnName');
    colNames{2}=[colNames{2},'|',uLabelL];
    colNames{10}=[colNames{10},' ',uLabelQ];
    colNames{11}=[colNames{11},' ',uLabelQ];
    set(handles.tblDepths,'ColumnName',colNames);
    
    % Get data for table from each transect
    nTransects=length(handles.checkedIdx);
    for n=1:nTransects
        composite{n}=meas.transects(handles.checkedIdx(n)).depths.composite;
        selected{n}=meas.transects(handles.checkedIdx(n)).depths.selected; 
        nEnsembles=size(meas.transects(handles.checkedIdx(n)).depths.(selected{n}).depthProcessed_m,2);
        avgMethod{n}='None';
        optionAvailable=[];
        tableData{n,2}=sprintf('% 8.2f',meas.transects(handles.checkedIdx(n)).depths.(selected{n}).draftUse_m.*unitsL);
        % Bottom track depths
        if ~isempty(meas.transects(handles.checkedIdx(n)).depths.btDepths)
            % Get valid data logical array
            validBT{n}=meas.transects(handles.checkedIdx(n)).depths.btDepths.validBeams;    
            
            % Compute number of invalid depths for each beam
            tableData{n,4}=sprintf('% 8.0f',nEnsembles-nansum(validBT{n}(1,:)));
            tableData{n,5}=sprintf('% 8.0f',nEnsembles-nansum(validBT{n}(2,:)));
            tableData{n,6}=sprintf('% 8.0f',nEnsembles-nansum(validBT{n}(3,:)));
            tableData{n,7}=sprintf('% 8.0f',nEnsembles-nansum(validBT{n}(4,:)));
            
            % Get depth averaging, filtering, and interpolation information
            avgMethod{n}=meas.transects(handles.checkedIdx(n)).depths.btDepths.avgMethod;
            filterType{n}=meas.transects(handles.checkedIdx(n)).depths.btDepths.filterType;
            interpolation{n}=meas.transects(handles.checkedIdx(n)).depths.btDepths.interpType;
            
            % Set GUI check boxes
            set(handles.cb1,'Enable','on');
            set(handles.cb2,'Enable','on');
            set(handles.cb3,'Enable','on');
            set(handles.cb4,'Enable','on');
            set(handles.cbBot4Beam,'Enable','on');
            
            optionAvailable=[optionAvailable,1];
            
        else
            % If no bottom track depth data
            tableData{n,4}='';
            tableData{n,5}='';
            tableData{n,6}='';
            tableData{n,7}='';
            set(handles.cb1,'Enable','off');
            set(handles.cb2,'Enable','off');
            set(handles.cb3,'Enable','off');
            set(handles.cb4,'Enable','off');
            set(handles.cbBot4Beam,'Enable','off');
                
        end
        
        % Vertical beam depth data
        if ~isempty(meas.transects(handles.checkedIdx(n)).depths.vbDepths)
            
            % Get valid data logical array 
            validVert{n}=meas.transects(handles.checkedIdx(n)).depths.vbDepths.validData;
            
            % Compute number of invalid depths
            tableData{n,8}=sprintf('% 10.0f',nEnsembles-nansum(validVert{n}));
            
            % Get depth averaging, filtering, and interpolation information
            filterType{n}=meas.transects(handles.checkedIdx(n)).depths.vbDepths.filterType;
            interpolation{n}=meas.transects(handles.checkedIdx(n)).depths.vbDepths.interpType;  
            
            % Set GUI check boxes
            set(handles.cbVB,'Enable','on'); 
            set(handles.cbBotVB,'Enable','on');
            
            % Make this option available.
            optionAvailable=[optionAvailable,2];          
        else
            % No vertical beam data
            tableData{n,8}='';
            set(handles.cbVB,'Enable','off'); 
            set(handles.cbBotVB,'Enable','off');  
        end
        
        % External depth sounder data
        if ~isempty(meas.transects(handles.checkedIdx(n)).depths.dsDepths)
            
            % Get valid data logical array 
            validDS{n}=meas.transects(handles.checkedIdx(n)).depths.dsDepths.validData;
            
            % Compute number of invalid depths
            tableData{n,9}=sprintf('% 10.0f',nEnsembles-nansum(validDS{n}));
            
            % Get depth averaging, filtering, and interpolation information
            avgMethod{n}=meas.transects(handles.checkedIdx(n)).depths.dsDepths.avgMethod;
            filterType{n}=meas.transects(handles.checkedIdx(n)).depths.dsDepths.filterType;
            interpolation{n}=meas.transects(handles.checkedIdx(n)).depths.dsDepths.interpType;
            
            % Set GUI check boxes
            set(handles.cbDS,'Enable','on');
            set(handles.cbBotDS,'Enable','on');
            
            % Make this option available
            optionAvailable=[optionAvailable,3];   
        else
            % No depth sounder data
            tableData{n,9}='';
            set(handles.cbDS,'Enable','off');
            set(handles.cbBotDS,'Enable','off');
        end
        idx=find(meas.transects(n).fileName=='\',1,'last');
        if isempty(idx)
            idx=0;
        end
        % Put filename in table
        tableData{n,1}=meas.transects(handles.checkedIdx(n)).fileName(idx+1:end);
       
        % Get discharge data for table
        tableData{n,3}=sprintf('% 10.0f',nEnsembles);
        tableData{n,10}=sprintf('% 12.2f',oldDischarge(handles.checkedIdx(n)).total.*unitsQ);
        tableData{n,11}=sprintf('% 12.2f',discharge(handles.checkedIdx(n)).total.*unitsQ);
        tableData{n,12}=sprintf('% 12.2f',((discharge(handles.checkedIdx(n)).total-oldDischarge(handles.checkedIdx(n)).total)./oldDischarge(handles.checkedIdx(n)).total).*100);      
    end % for n
    
    % Setup drop-down list for available options
    selectionOptions={};
    if ~isempty(find(optionAvailable==1,1))
        selectionOptions(length(selectionOptions)+1)={'4-Beam Avg'};
        if length(unique(optionAvailable))>1
            selectionOptions(length(selectionOptions)+1)={'Comp 4-Beam Preferred'};
        else
            nmax=length(composite);
            for n=1:nmax
                composite{n}='Off';
            end
        end % if
    end % if

    if ~isempty(find(optionAvailable==2,1))
        selectionOptions(length(selectionOptions)+1)={'Vertical'}; 
        if length(unique(optionAvailable))>1
            selectionOptions(length(selectionOptions)+1)={'Comp Vertical Preferred'};
        end % if
    end % if

    if ~isempty(find(optionAvailable==3,1))
        selectionOptions(length(selectionOptions)+1)={'Depth Sounder'};
        if length(unique(optionAvailable))>1
            selectionOptions(length(selectionOptions)+1)={'Comp DS Preferred'};
        end % if
    end % if
    
    % Set initial plot to first file
    handles.plottedTransect=1;
    set(handles.plottedtxt,'String',tableData{handles.plottedTransect,1});
    drawnow
    
    % Update GUI
    tableData=applyFormat(handles,tableData,meas);
    set(handles.tblDepths,'Data',tableData(1:nTransects,:));
    set(handles.puSelect,'String',selectionOptions);
    set(handles.puSelect,'Value',1);
    
    % Reference Settings
    if nansum(strcmp(selected,selected(1)))~=nTransects
        warndlg('Selected depth reference inconsistent, value from 1st transect will be used.');
    end
    if nansum(strcmp(composite,composite(1)))~=nTransects && ~isempty(composite{1})
        warndlg('Composite setting is inconsistent, value from 1st transect will be used.');
    end
    if strcmp(selected(1),'btDepths')
        idx=find(ismember(selectionOptions,'4-Beam Avg'));
        set(handles.cb1,'Value',1);
        set(handles.cb2,'Value',1);
        set(handles.cb3,'Value',1);
        set(handles.cb4,'Value',1);
        if strcmp(composite,'On')
            set(handles.puSelect,'Value',idx+1);
            if strcmpi(get(handles.cbVB,'Enable'),'on')
                set(handles.cbVB,'Value',1);
            end
            if strcmpi(get(handles.cbDS,'Enable'),'on')
                set(handles.cbDS,'Value',1);
            end
        else
            set(handles.puSelect,'Value',idx);
        end
    elseif strcmp(selected(1),'vbDepths')
        idx=find(ismember(selectionOptions,'Vertical'));
        set(handles.cbVB,'Value',1);        
        if strcmp(composite,'On')
            set(handles.puSelect,'Value',idx+1);
            if strcmpi(get(handles.cb1,'Enable'),'on')
                set(handles.cb1,'Value',1);
                set(handles.cb2,'Value',1);
                set(handles.cb3,'Value',1);
                set(handles.cb4,'Value',1);
            end
            if strcmpi(get(handles.cbDS,'Enable'),'on')
                set(handles.cbDS,'Value',1);
            end
        else
            set(handles.puSelect,'Value',idx);
        end
    elseif strcmp(selected(1),'dsDepths')
        idx=find(ismember(selectionOptions,'Depth Sounder'));
        set(handles.cbDS,'Value',1);
        if strcmp(composite,'On')
            set(handles.puSelect,'Value',idx+1);
            if strcmpi(get(handles.cb1,'Enable'),'on')
                set(handles.cb1,'Value',1);
                set(handles.cb2,'Value',1);
                set(handles.cb3,'Value',1);
                set(handles.cb4,'Value',1);
            end
            if strcmpi(get(handles.cbVB,'Enable'),'on')
                set(handles.cbVB,'Value',1);
            end
        else
            set(handles.puSelect,'Value',idx);
        end
    end
    % Turn on final cross section plot
    set(handles.cbBotFinal,'Value',1);
    
    % Averaging settings
    if nansum(strcmp(avgMethod,avgMethod(1)))~=nTransects
        warndlg('Average method inconsistent, value from 1st transect will be used.');
    end
    if strcmp(avgMethod(1),'IDW')
        set(handles.puAverage,'Value',1);
    else
        set(handles.puAverage,'Value',2);
    end
    
    % Filter settings
    if nansum(strcmp(filterType,filterType(1)))~=nTransects
        warndlg('Filter type inconsistent, value from 1st transect will be used.');
    end
    switch filterType {1}
        case 'Off'
            set(handles.puFilter,'Value',1);
        case 'Smooth'
            set(handles.puFilter,'Value',2);
        case 'TRDI'
            set(handles.puFilter,'Value',3);
    end
    if isempty(meas.transects(handles.checkedIdx(1)).depths.(meas.transects(handles.checkedIdx(1)).depths.selected).smoothDepth)
    end
    
    % Interpolation settings
    if nansum(strcmp(interpolation,interpolation(1)))~=nTransects
        warndlg('Interpolation setting inconsistent, value from 1st transect will be used.');
    end    
    switch interpolation {1}
        case 'Off'
            set(handles.puInterpolation,'Value',1);
        case 'Smooth'
            set(handles.puInterpolation,'Value',2);
        case 'Linear'
            set(handles.puInterpolation,'Value',3);
    end 
    % Disable interpolation selection in GUI
    set(handles.puInterpolation,'Visible',getappdata(handles.hMainGui,'InterpVisible'));
    set(handles.uipanelInterp,'Visible',getappdata(handles.hMainGui,'InterpVisible'));

    plotGraphs(handles);
    
% Update handles structure
guidata(hObject, handles);

function varargout = winDepthFilter_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function tableData=applyFormat(handles,tableData,meas)
% Applys a format to the table that highlights values that have generated
% warnings or caution about the quality of the data and its affect on Q.
%
% INPUT:
%
% handles: handles data structure from figure
%
% tableData: data from table to be formatted
%
% meas: object of clsMeasurement
%
% OUTPUT:
%
% tableData: data for table that has been formatted.

    % Combine caution notices into single matrix
    caution=meas.qa.depths.qTotalCaution(handles.checkedIdx,:) | meas.qa.depths.qRunCaution(handles.checkedIdx,:);
    
    % Combine warning notices into single matrix
    warning=meas.qa.depths.qRunWarning(handles.checkedIdx,:) | meas.qa.depths.qTotalWarning(handles.checkedIdx,:);
    
    % Combine caution and warning into single matrix
    code=double(caution);
    code(warning)=2;
    applyCode=repmat(code,1,6);
    
    % Creat idx of cells to apply caution and warning formats
    idxCaution=find(applyCode==1);
    idxWarning=find(applyCode==2);
    
    % Get subset of table to be formatted
    tempData=tableData(:,4:9);
    
    % Apply caution format
    for n=1:length(idxCaution)
        tempData(idxCaution(n))={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tempData{idxCaution(n)}),'</center></BODY></HTML>']};
    end
    
    % Apply warning format
    for n=1:length(idxWarning)
        tempData(idxWarning(n))={['<HTML><BODY bgColor="red" width="50px"><strong><center>',num2str(tempData{idxWarning(n)}),'</center></strong></BODY></HTML>']};
    end
    
    % Put formatted cells back in table
    tableData(:,4:9)=tempData;

    % Determine number of transects
    nTransects=length(handles.checkedIdx);
    
    % Apply formatting based on qa checks for draft
    switch meas.qa.depths.draft
        % Caution
        case 1
            for n=1:nTransects
                tableData(n,2)={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(tableData{n,2}),'</center></BODY></HTML>']};
            end
        % Warning    
        case 2
            for n=1:nTransects
                tableData(n,2)={['<HTML><BODY bgColor="red" width="50px"><strong><center>',num2str(tableData{n,2}),'</center></strong></BODY></HTML>']};
            end
    end
    
% User Controls

function puSelect_Callback(hObject, eventdata, handles)
    
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
       
    % Get data from GUI
    contents=get(handles.puSelect,'String');
    value=get(handles.puSelect,'Value');
    setting=contents{value};
    
    % Apply selected setting
    switch setting
        case '4-Beam Avg'
            s.depthReference='btDepths';
            s.depthComposite='Off';
            set(handles.puAverage,'Enable','On');
        case 'Vertical'
            s.depthReference='vbDepths';
            s.depthComposite='Off';
            set(handles.puAverage,'Enable','Off');
        case 'Depth Sounder'
            s.depthReference='dsDepths';
            s.depthComposite='Off';            
            set(handles.puAverage,'Enable','Off');
        case 'Comp 4-Beam Preferred'
            s.depthReference='btDepths';
            s.depthComposite='On';            
            set(handles.puAverage,'Enable','On');
        case 'Comp Vertical Preferred'
            s.depthReference='vbDepths';
            s.depthComposite='On';            
            set(handles.puAverage,'Enable','On');
        case 'Comp DS Preferred'
            s.depthReference='dsDepths';
            s.depthComposite='On';            
            set(handles.puAverage,'Enable','On');
    end % switch
    
    % Update data and computations
    dischargeOld=meas.discharge;
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    meas=applySettings(meas,s);     
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    
    % Update GUI & Data
    updateData(handles, meas, dischargeOld)
    plotGraphs (handles);
    guidata(hObject, handles);
    
function puAverage_Callback(hObject, eventdata, handles)
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get data from GUI
    setting=get(handles.puAverage,'Value');
    
    if setting==1
        s.depthAvgMethod='IDW';
    else
        s.depthAvgMethod='Simple';
    end % if puAverage
    
    % Update data and computations
    dischargeOld=meas.discharge;
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    meas=applySettings(meas,s);     
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    
    % Update GUI & Data
    updateData(handles, meas,dischargeOld)
    plotGraphs (handles);
    guidata(hObject, handles);
    
function puFilter_Callback(hObject, eventdata, handles)

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
       
    % Get data from GUI
    setting=get(handles.puFilter,'Value');
       
    switch setting
        case 1 % None
            s.depthFilterType='None';
        case 2 % Smooth
            s.depthFilterType='Smooth';
        case 3 % TRDI
            s.depthFilterType='TRDI';
    end
  
    % Update data and computations
    dischargeOld=meas.discharge;
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    meas=applySettings(meas,s);     
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    
    % Update GUI & Data
    updateData(handles, meas, dischargeOld)
    plotGraphs (handles);
    guidata(hObject, handles);
            
function puInterpolation_Callback(hObject, eventdata, handles)
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    
    % Get data from GUI
    setting=get(handles.puInterpolation,'Value');

    switch setting
        case 1 % None
            s.depthInterpolation='None';
        case 2 % Smooth
            s.depthInterpolation='Smooth';
        case 3 % Linear
            s.depthInterpolation='Linear';
    end
  
    % Update data and computations
    dischargeOld=meas.discharge;
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    meas=applySettings(meas,s);     
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    
    % Update GUI & Data
    updateData(handles, meas, dischargeOld)
    plotGraphs (handles);
    guidata(hObject, handles);
    
function pbClose_Callback(hObject, eventdata, handles)
    guidata(hObject, handles);
    delete (handles.output);  
    
function pbComment_Callback(hObject, eventdata, handles)
% Add comment
    [handles]=commentButton(handles, 'Depth');
    % Update handles structure
    guidata(hObject, handles);    
    
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%open('helpFiles\QRev_Users_Manual.pdf')  
web('QRev_Help_Files\HTML\depth_filters_draft.htm')
% Update table

function updateData(handles, meas, dischargeOld)
% Updates the table showing effect of reference, averaging, filter, and
% interpolation settings.

    % Get data from table
    tableData=get(handles.tblDepths,'Data');

    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Determine number of transects
    nTransects=length(handles.checkedIdx);
    
    for n=1:nTransects
        nEnsembles=length(meas.transects(handles.checkedIdx(n)).depths.btDepths.depthProcessed_m);
        selected{n}=meas.transects(handles.checkedIdx(n)).depths.selected; 
        tableData{n,2}=sprintf('% 8.2f',meas.transects(handles.checkedIdx(n)).depths.(selected{n}).draftUse_m.*unitsL);
        % Bottom track data
        if ~isempty(meas.transects(handles.checkedIdx(n)).depths.btDepths)   
            % Get valid data logical array
            validBT{n}=meas.transects(handles.checkedIdx(n)).depths.btDepths.validBeams;    
            
            % Compute number of invalid depths for each beam
            tableData{n,4}=sprintf('% 8.0f',nEnsembles-nansum(validBT{n}(1,:)));
            tableData{n,5}=sprintf('% 8.0f',nEnsembles-nansum(validBT{n}(2,:)));
            tableData{n,6}=sprintf('% 8.0f',nEnsembles-nansum(validBT{n}(3,:)));
            tableData{n,7}=sprintf('% 8.0f',nEnsembles-nansum(validBT{n}(4,:)));
        else
            % If no bottom track depth data
            tableData{n,4}='';
            tableData{n,5}='';
            tableData{n,6}='';
            tableData{n,7}='';
        end % if BT
    
        % Vertical beam
        if ~isempty(meas.transects(handles.checkedIdx(n)).depths.vbDepths)
            % Get valid data logical array 
            validVert{n}=meas.transects(handles.checkedIdx(n)).depths.vbDepths.validData;

            % Compute number of invalid depths
            tableData{n,8}=sprintf('% 10.0f',nEnsembles-nansum(validVert{n}));
         else
            % No vertical beam data
            tableData{n,8}='';
        end

        % Depth sounder
        if ~isempty(meas.transects(handles.checkedIdx(n)).depths.dsDepths)
            % Get valid data logical array 
            validDS{n}=meas.transects(handles.checkedIdx(n)).depths.dsDepths.validData;

            % Compute number of invalid depths
            tableData{n,9}=sprintf('% 10.0f',nEnsembles-nansum(validDS{n}));
        else
            % No depth sounder data
            tableData{n,9}='';
        end
    
        % Ensembles
        tableData{n,3}=sprintf('% 10.0f',nEnsembles);
    end % for n
    
    for n=1:nTransects
        tableData{n,10}=sprintf('% 12.2f',dischargeOld(handles.checkedIdx(n)).total.*unitsQ);
        tableData{n,11}=sprintf('% 12.2f',meas.discharge(handles.checkedIdx(n)).total.*unitsQ);
        tableData{n,12}=sprintf('% 12.2f',((meas.discharge(handles.checkedIdx(n)).total-dischargeOld(handles.checkedIdx(n)).total)./dischargeOld(handles.checkedIdx(n)).total).*100);      
    end % for n
    
    % Write to table
    tableData=applyFormat(handles,tableData,meas);
    set(handles.tblDepths,'Data',tableData);
    drawnow

    % Store data
    setappdata(handles.hMainGui,'measurement',meas);

% Plot Graphs    
    
function plotGraphs(handles)

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Get settings from GUI
    tableData=get(handles.tblDepths,'Data');
    transectSelected=handles.checkedIdx(handles.plottedTransect);
    if isempty(transectSelected)
        transectSelected=1;
    end
    
    % Top plot
    b1=get(handles.cb1,'Value');
    b2=get(handles.cb2,'Value');
    b3=get(handles.cb3,'Value');
    b4=get(handles.cb4,'Value');
    vb=get(handles.cbVB,'Value');
    ds=get(handles.cbDS,'Value');
    
    % Bottom plot
    bt=get(handles.cbBot4Beam,'Value');
    vbb=get(handles.cbBotVB,'Value');
    dsb=get(handles.cbBotDS,'Value');
    final=get(handles.cbBotFinal,'Value');
    
         
    % Transect to plot
    transect=meas.transects(transectSelected);

    % Create position array
    if ~isempty(transect.boatVel.(transect.boatVel.selected))
        boatVelX=transect.boatVel.(transect.boatVel.selected).uProcessed_mps;
        boatVelY=transect.boatVel.(transect.boatVel.selected).vProcessed_mps;
    else
        boatVelX=nan(size(transect.boatVel.btVel.uProcessed_mps));
        boatVelY=nan(size(transect.boatVel.btVel.uProcessed_mps));
    end
    trackX=boatVelX.*transect.dateTime.ensDuration_sec;
    trackY=boatVelY.*transect.dateTime.ensDuration_sec;
    track=nancumsum(sqrt(trackX.^2+trackY.^2));
   
    % Depth data
    btDepths=transect.depths.btDepths;
    vbDepths=transect.depths.vbDepths;
    dsDepths=transect.depths.dsDepths;
    
    btPlotDepths=btDepths.depthBeams_m;
    btPlotDepths(isnan(btPlotDepths))=0;
    
    selected=transect.depths.(transect.depths.selected);
    cla(handles.axTop)
    if b1
        plot(handles.axTop,track.*unitsL,btDepths.depthBeams_m(1,:).*unitsL,'.-r')
        hold(handles.axTop,'on')
        maxdepth(1)=nanmax(btDepths.depthBeams_m(1,:).*unitsL);
        plot(handles.axTop,track(~btDepths.validBeams(1,:)).*unitsL,btPlotDepths(1,~btDepths.validBeams(1,:)).*unitsL,'or')
        if strcmp(transect.depths.btDepths.interpType,'Smooth')
             plot(handles.axTop,track(~btDepths.validBeams(1,:)).*unitsL,btDepths.smoothDepth(1,~btDepths.validBeams(1,:)).*unitsL,'*r')
        end % if interpType
    end % if b1
    
    if b2
        plot(handles.axTop,track.*unitsL,btDepths.depthBeams_m(2,:).*unitsL,'Color',[0 0.498,0],'LineStyle','-','Marker','.')
        hold(handles.axTop,'on')
        maxdepth(2)=nanmax(btDepths.depthBeams_m(2,:).*unitsL);
        plot(handles.axTop,track(~btDepths.validBeams(2,:)).*unitsL,btPlotDepths(2,~btDepths.validBeams(2,:)).*unitsL,'LineStyle','none','MarkerEdgeColor',[0 0.498,0],'Marker','o','LineStyle','none')
        if strcmp(transect.depths.btDepths.interpType,'Smooth')
             plot(handles.axTop,track(~btDepths.validBeams(2,:)).*unitsL,btDepths.smoothDepth(2,~btDepths.validBeams(2,:)).*unitsL,'LineStyle','none','MarkerEdgeColor',[0 0.498,0],'Marker','*','LineStyle','none')
        end % if interpType
    end % if b2
    
    if b3
        plot(handles.axTop,track.*unitsL,btDepths.depthBeams_m(3,:).*unitsL,'.-b')
        hold(handles.axTop,'on')
        maxdepth(3)=nanmax(btDepths.depthBeams_m(3,:).*unitsL);
        plot(handles.axTop,track(~btDepths.validBeams(3,:)).*unitsL,btPlotDepths(3,~btDepths.validBeams(3,:)).*unitsL,'ob')
        if strcmp(transect.depths.btDepths.interpType,'Smooth')
             plot(handles.axTop,track(~btDepths.validBeams(3,:)).*unitsL,btDepths.smoothDepth(3,~btDepths.validBeams(3,:)).*unitsL,'*b')
        end % if interpType
    end % if b3
    
    if b4
        plot(handles.axTop,track.*unitsL,btDepths.depthBeams_m(4,:).*unitsL,'Color',[0.682 0.467,0],'LineStyle','-','Marker','.')
        hold(handles.axTop,'on')
        maxdepth(4)=nanmax(btDepths.depthBeams_m(4,:).*unitsL);
        plot(handles.axTop,track(~btDepths.validBeams(4,:)).*unitsL,btPlotDepths(4,~btDepths.validBeams(4,:)).*unitsL,'LineStyle','none','MarkerEdgeColor',[0.682 0.467,0],'Marker','o','LineStyle','none')
        if strcmp(transect.depths.btDepths.interpType,'Smooth')
             plot(handles.axTop,track(~btDepths.validBeams(4,:)).*unitsL,btDepths.smoothDepth(4,~btDepths.validBeams(4,:)).*unitsL,'LineStyle','none','MarkerEdgeColor',[0.682 0.467,0],'Marker','*','LineStyle','none')
        end % if interpType
    end % if b4
    
    if vb
        vbPlotDepths=vbDepths.depthBeams_m;
        vbPlotDepths(isnan(vbPlotDepths))=0;
        plot(handles.axTop,track.*unitsL,vbDepths.depthBeams_m.*unitsL,'Color',[0.478 0.063,0.894],'LineStyle','-','Marker','.')
        hold(handles.axTop,'on')
        maxdepth(5)=nanmax(vbDepths.depthBeams_m.*unitsL);
        plot(handles.axTop,track(~vbDepths.validData).*unitsL,vbPlotDepths(~vbDepths.validData).*unitsL,'LineStyle','none','MarkerEdgeColor',[0.478 0.063,0.894],'Marker','o')
        if strcmp(transect.depths.vbDepths.interpType,'Smooth')
             plot(handles.axTop,track(~vbDepths.validData).*unitsL,vbDepths.smoothDepth(~vbDepths.validData).*unitsL,'LineStyle','none','MarkerEdgeColor',[0.478 0.063,0.894],'Marker','*')
        end % if interpType
    end % if vb
    
    if ds
        dsPlotDepths=dsDepths.depthBeams_m;
        dsPlotDepths(isnan(dsPlotDepths))=0;
        plot(handles.axTop,track.*unitsL,dsDepths.depthBeams_m.*unitsL,'Color',[0.043 0.518,0.78],'LineStyle','-');
        hold(handles.axTop,'on');
        maxdepth(6)=nanmax(dsDepths.depthBeams_m.*unitsL);
        plot(handles.axTop,track(~dsDepths.validData).*unitsL,dsPlotDepths(~dsDepths.validData).*unitsL,'LineStyle','none','MarkerEdgeColor',[0.043 0.518,0.78],'Marker','o');
        if strcmp(transect.depths.dsDepths.interpType,'Smooth');
             plot(handles.axTop,track(~dsDepths.validData).*unitsL,dsDepths.smoothDepth(~dsDepths.validData).*unitsL,'LineStyle','none','MarkerEdgeColor',[0.043 0.518,0.78],'Marker','*');
        end % if interpType
    end % if vb    
    set(handles.axTop,'YDir','reverse');
    if strcmpi(transect.startEdge,'Right');
        set(handles.axTop,'XDir','reverse');
    else
        set(handles.axTop,'XDir','normal');
    end 
    xlabel(handles.axTop,['Length ',uLabelL]);
    ylabel(handles.axTop,['Depth ',uLabelL]);
    grid(handles.axTop,'On')
    
% Bottom Plot
    cla(handles.axBottom)
    validBT=~isnan(trackX);
    if bt
        plot(handles.axBottom,track(validBT).*unitsL,btDepths.depthProcessed_m(validBT).*unitsL,'-r');
        hold(handles.axBottom,'on');
    end % if b1
    
    if vbb
        plot(handles.axBottom,track(validBT).*unitsL,vbDepths.depthProcessed_m(validBT).*unitsL,'Color',[0.478 0.063,0.894],'LineStyle','-');
        hold(handles.axBottom,'on');
    end % if b2
    
    if dsb
        plot(handles.axBottom,track(validBT).*unitsL,dsDepths.depthProcessed_m(validBT).*unitsL,'Color',[0.043 0.518,0.78],'LineStyle','-');
        hold(handles.axBottom,'on');
    end % if vb    
    
     if final
        plot(handles.axBottom,track(validBT).*unitsL,selected.depthProcessed_m(validBT).*unitsL,'-k','Linewidth',2);
        hold(handles.axBottom,'on');
    end % if vb  
    set(handles.axBottom,'YDir','reverse');
    if strcmpi(transect.startEdge,'Right');
        set(handles.axBottom,'XDir','reverse');
    else
        set(handles.axBottom,'XDir','normal');
    end 
    xlabel(handles.axBottom,['Length ',uLabelL]);
    ylabel(handles.axBottom,['Depth ',uLabelL]);
    
%     top=get(handles.axTop,'ylim');
%     bot=get(handles.axBottom,'ylim');
%     minAx=0;
%     maxAx=max([top(2),bot(2)]);
    set(handles.axTop,'xlim',[0,nanmax(track).*unitsL]);
    set(handles.axBottom,'xlim',[0,nanmax(track).*unitsL]);
%     top=get(handles.axTop,'xlim');
%     bot=get(handles.axBottom,'xlim');
%     maxAx=max([top(2),bot(2)]);
    set(handles.axTop,'ylim',[0,nanmax(maxdepth)]);
    set(handles.axBottom,'ylim',[0,nanmax(maxdepth)]);
    
    grid(handles.axBottom,'On')
    
    linkaxes([handles.axTop,handles.axBottom],'xy');

function cbBot4Beam_Callback(hObject, eventdata, handles)
    plotGraphs(handles)
    guidata(hObject, handles);

function cbBotVB_Callback(hObject, eventdata, handles)
    plotGraphs(handles)
    guidata(hObject, handles);

function cbBotDS_Callback(hObject, eventdata, handles)
    plotGraphs(handles)
    guidata(hObject, handles);

function cbBotFinal_Callback(hObject, eventdata, handles)
    plotGraphs(handles)
    guidata(hObject, handles);

% Top Plot

function cb1_Callback(hObject, eventdata, handles)
    plotGraphs(handles)
    guidata(hObject, handles);
    
function cb2_Callback(hObject, eventdata, handles)
    plotGraphs(handles)
    guidata(hObject, handles);

function cb3_Callback(hObject, eventdata, handles)
    plotGraphs(handles)
    guidata(hObject, handles);

function cb4_Callback(hObject, eventdata, handles)
    plotGraphs(handles)
    guidata(hObject, handles);

function cbVB_Callback(hObject, eventdata, handles)
    plotGraphs(handles)
    guidata(hObject, handles);

function cbDS_Callback(hObject, eventdata, handles)
    plotGraphs(handles)
    guidata(hObject, handles);

function tblDepths_CellSelectionCallback(hObject, eventdata, handles)
    
    % Get row and col of selected cell
    selection = handles.checkedIdx(eventdata.Indices(:,1));
    col=eventdata.Indices(:,2);
    
    % Get table data
    tableData=get(handles.tblDepths,'Data');
    
    % Change draft
    if col==2
        % Get update measurement data
        meas=getappdata(handles.hMainGui,'measurement');

        % Update data and computations
        dischargeOld=meas.discharge;
        
        % Open winDraft
        handles.hWinDraft=winDraft('hMainGui',handles.hMainGui,selection);
        set(handles.hWinDraft,'WindowStyle','modal');
        % Wait for figure to close before proceeding
        waitfor(handles.hWinDraft);
        
        % Get update measurement data
        meas=getappdata(handles.hMainGui,'measurement');
    
        % Update GUI & Data
        updateData(handles, meas, dischargeOld)
        plotGraphs (handles);

    end
    
    % If statement due to code executed twice. Not sure why but this fixes
    % the problem.
    if ~isempty(selection)
         % Get table data
%         tableData=get(handles.tblDepths,'Data');
        row=eventdata.Indices(:,1);
        handles.plottedTransect=row;
        set(handles.plottedtxt,'String',tableData{row,1});
        drawnow
    end
    
    % Update graphics
    plotGraphs(handles)
    guidata(hObject, handles);

% --- Executes on key press with focus on tblDepths and none of its controls.
function tblDepths_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to tblDepths (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

% Make table inactive to avoid synching issues associated with multiple
% keypresses before the processing of the first can be completed.
set(handles.tblDepths,'Enable','inactive');

if strcmp(eventdata.Key,'uparrow') || strcmp(eventdata.Key,'downarrow')
    tableData=get(handles.tblDepths,'Data');
    rowTrue=handles.plottedTransect;
    if strcmp(eventdata.Key,'downarrow')
        if rowTrue+1<=size(tableData,1)
            newRow=rowTrue+1;          
        else
            newRow=size(tableData,1);
        end
    elseif strcmp(eventdata.Key,'uparrow')
        if rowTrue-1>0
            newRow=rowTrue-1;
        else
            newRow=1;
        end
    end
      
    set(handles.plottedtxt,'String',tableData{newRow,1});
    handles.plottedTransect=newRow;
    drawnow

    % Update graphics
    plotGraphs(handles)
    % Update handles structure
    guidata(hObject, handles);
    drawnow
end    
set(handles.tblDepths,'Enable','on');
%==========================================================================
%================NO MODIFIED CODE BELOW HERE===============================
%==========================================================================

function edit1_Callback(hObject, eventdata, handles)

function edit2_Callback(hObject, eventdata, handles)

function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function puInterpolation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to puInterpolation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function puSelect_CreateFcn(hObject, eventdata, handles)
% hObject    handle to puSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function puFilter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to puFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





% --- Executes during object creation, after setting all properties.
function puAverage_CreateFcn(hObject, eventdata, handles)
% hObject    handle to puAverage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


