function varargout = winMovingBed(varargin)
% WINMOVINGBED MATLAB code for winMovingBed.fig
%      WINMOVINGBED, by itself, creates a new WINMOVINGBED or raises the existing
%      singleton*.
%
%      H = WINMOVINGBED returns the handle to a new WINMOVINGBED or the handle to
%      the existing singleton*.
%
%      WINMOVINGBED('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINMOVINGBED.M with the given input arguments.
%
%      WINMOVINGBED('Property','Value',...) creates a new WINMOVINGBED or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winMovingBed_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winMovingBed_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winMovingBed

% Last Modified by GUIDE v2.5 27-Jan-2017 10:31:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winMovingBed_OpeningFcn, ...
                   'gui_OutputFcn',  @winMovingBed_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



function winMovingBed_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winMovingBed (see VARARGIN)

    % Choose default command line output for winMovingBed
    handles.output = hObject;
    
    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end
    
    setWindowPosition(hObject,handles);
    
    commentIcon(handles.pbComment);
    
    % Get table from gui
    tableData=get(handles.tblMB,'Data');
    
    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Set units for column names
    colNames=get(handles.tblMB,'ColumnName');
    colNames{6}=[colNames{6},' ',uLabelL];
    colNames{7}=[colNames{7},' ',uLabelV];
    colNames{9}=[colNames{9},' ',uLabelV];
    set(handles.tblMB,'ColumnName',colNames);

    % Get data for table from each transect
    nTests=size(meas.mbTests,2);
    if nTests>0
        mbTests=meas.mbTests;
        handles=updateTblMsgs(handles,mbTests);
        tableData=get(handles.tblMB,'Data');
        % Set transect to initially plot
        handles.plottedTransect=find([meas.mbTests.selected]==1,1,'first');
        set(handles.plottedtxt,'String',tableData{handles.plottedTransect,3});
        
        updateGraphics(handles);
    else
        output={'No Moving-Bed Test Available'};    
        outstr=char(output);
        set(handles.txtMessage,'String',sprintf(outstr(1:end,:)'));
    end
    if strcmp(meas.transects(1).adcp.manufacturer,'TRDI')
        set(handles.pbLoad,'Enable','off')
    end
      
    % Update handles structure
    guidata(hObject, handles);

function varargout = winMovingBed_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function pbComment_Callback(hObject, eventdata, handles)
% Add comment
    [handles]=commentButton(handles, 'Moving-Bed Test');
    % Update handles structure
    guidata(hObject, handles);

function pbClose_Callback(hObject, eventdata, handles)
    delete (handles.output);

% Graphics

function updateGraphics(handles)
    timeSeriesPlot(handles);
    shiptrackPlot(handles);
    
function timeSeriesPlot(handles)
% Plot time series data

    % Clear axis
    cla(handles.axTimeSeries)
    
    % Get table data
    tableData=get(handles.tblMB,'Data');
    
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Determine transect to be plotted
    testSelected=handles.plottedTransect;
    if isempty(testSelected)
        testSelected=1;
    end
    mbVel=meas.mbTests(testSelected);
    if ~isempty(mbVel)
        
        % Get transect data
        transect=meas.mbTests(testSelected).transect;
        inTransectIdx=transect.inTransectIdx;        
        validData=transect.boatVel.btVel.validData(1,inTransectIdx);
        ensembles=1:length(validData);

        % Generate plot
        hold(handles.axTimeSeries,'on');
        if strcmp(meas.mbTests(testSelected).type,'Loop')
            speedProcessed=sqrt(transect.boatVel.btVel.uProcessed_mps(inTransectIdx).^2+transect.boatVel.btVel.vProcessed_mps(inTransectIdx).^2);
            plot(handles.axTimeSeries,ensembles,speedProcessed.*unitsV,'.-b')  
            if sum(validData)<length(validData)
                plot(handles.axTimeSeries,ensembles(~validData),0,'or')
            end
            ylabel(handles.axTimeSeries,['Bottom Track Speed ',uLabelV])   
        elseif ~isempty(mbVel.stationaryMBVel)
               plot(handles.axTimeSeries,ensembles,mbVel.stationaryMBVel.*unitsV,'.-b')
               plot(handles.axTimeSeries,ensembles(~validData),mbVel.stationaryMBVel(~validData).*unitsV,'or')
               ylabel(handles.axTimeSeries,['Average Moving-Bed Speed',' ',uLabelV]);
        end % if
        xlabel(handles.axTimeSeries,'Ensemble')
        hold (handles.axTimeSeries, 'off') 
        set(handles.axTimeSeries,'Box','on');
        grid(handles.axTimeSeries,'On')
    end
    
function shiptrackPlot(handles)
% Plot ship track

    % Clear axis
    cla(handles.axShipTrack)
    
    % Data from table
    tableData=get(handles.tblMB,'Data');
    
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Determine transect to be plotted
    testSelected=handles.plottedTransect;
    if isempty(testSelected)
        testSelected=1;
    end
    mbVel=meas.mbTests(testSelected);
    if ~isempty(mbVel)
        % Get selected transect
        transect=meas.mbTests(testSelected).transect;
        inTransectIdx=transect.inTransectIdx;        
        validData=transect.boatVel.btVel.validData(1,inTransectIdx);

        % Determine type of ship track plot to be plotted
        if strcmp(meas.mbTests(testSelected).type,'Loop')

            % Set check boxes
            set(handles.cbShipBT,'Visible','On');
            set(handles.cbShipGGA,'Visible','On');
            set(handles.cbShipVTG,'Visible','On');
            set(handles.cbShipVectors,'Visible','On');
            % Check for availability of GPS data and set shiptrack options
            if isempty([meas.transects.gps])
                set(handles.cbShipGGA,'Enable','off');
                set(handles.cbShipVTG,'Enable','off');
            else
                set(handles.cbShipGGA,'Enable','on');
                set(handles.cbShipVTG,'Enable','on');
            end
            set(handles.cbShipBT,'Enable','on');
            set(handles.cbShipVectors,'Enable','on');

            % Compute track
            ensDuration=clsTransectData.adjustedEnsembleDuration(transect);
            % Compute water vectors for select reference
            waterRef=transect.wVel.navRef;
            wVelX=transect.wVel.uProcessed_mps;
            wVelY=transect.wVel.vProcessed_mps;
            meanVelX=nanmean(wVelX);
            meanVelY=nanmean(wVelY);

            % Plot shiptrack
            hold(handles.axShipTrack,'on')

            % Plot BT
            if get(handles.cbShipBT,'Value') 
                % BT
                btUProcessed=transect.boatVel.btVel.uProcessed_mps;
                btVProcessed=transect.boatVel.btVel.vProcessed_mps;
                btXProcessed=nancumsum(btUProcessed.*ensDuration);
                btYProcessed=nancumsum(btVProcessed.*ensDuration);
                plot(handles.axShipTrack,btXProcessed.*unitsL,btYProcessed.*unitsL,'-r','LineWidth',3)
                plot(handles.axShipTrack,btXProcessed(end).*unitsL,btYProcessed(end).*unitsL,'sk','MarkerFaceColor','k','MarkerSize',8);
                % Only plot vectors for selected water track nav reference
                if strcmp(waterRef,'BT') && get(handles.cbShipVectors,'Value')
                    quiver(handles.axShipTrack,btXProcessed.*unitsL,btYProcessed.*unitsL,meanVelX.*unitsV,meanVelY.*unitsV,'k','ShowArrowHead','on','AutoScaleFactor',5);
                end % if
            end % if

            % Plot VTG
            if get(handles.cbShipVTG,'Value')
                % VTG
                vtgUProcessed=transect.boatVel.vtgVel.uProcessed_mps;
                vtgVProcessed=transect.boatVel.vtgVel.vProcessed_mps;
                vtgXProcessed=nancumsum(vtgUProcessed.*ensDuration);
                vtgYProcessed=nancumsum(vtgVProcessed.*ensDuration);
                plot(handles.axShipTrack,vtgXProcessed.*unitsL,vtgYProcessed.*unitsL,'-','Color',[0 0.498 0],'LineWidth',3)
                plot(handles.axShipTrack,vtgXProcessed(end).*unitsL,vtgYProcessed(end).*unitsL,'s','Color',[0 0.498 0])
                % Only plot vectors for selected water track nav reference
                if strcmp(waterRef,'VTG') && get(handles.cbShipVectors,'Value')
                    quiver(handles.axShipTrack,vtgXProcessed.*unitsV,vtgYProcessed.*unitsV,meanVelX.*unitsV,meanVelY.*unitsV,'k','ShowArrowHead','on','AutoScaleFactor',5);
                end % if
            end % if

            % Plot GGA
            if get(handles.cbShipGGA,'Value')
                 % GGA
                ggaUProcessed=transect.boatVel.ggaVel.uProcessed_mps;
                ggaVProcessed=transect.boatVel.ggaVel.vProcessed_mps;
                ggaXProcessed=nancumsum(ggaUProcessed.*ensDuration);
                ggaYProcessed=nancumsum(ggaVProcessed.*ensDuration);
                plot(handles.axShipTrack,ggaXProcessed.*unitsL,ggaYProcessed.*unitsL,'-b','LineWidth',3)
                plot(handles.axShipTrack,ggaXProcessed(end).*unitsL,ggaYProcessed(end).*unitsL,'sb')
                % Only plot vectors for selected water track nav reference
                if strcmp(waterRef,'GGA') && get(handles.cbShipVectors,'Value')
                    quiver(handles.axShipTrack,ggaXProcessed.*unitsL,ggaYProcessed.*unitsL,meanVelX.*unitsV,meanVelY.*unitsV,'k','ShowArrowHead','on','AutoScaleFactor',5);
                end % if
            end % if  
            xlabel(handles.axShipTrack,['Distance East',' ',uLabelL]);
            ylabel(handles.axShipTrack,['Distance North',' ',uLabelL]);
        else
            % Plot ship track for stationary test
            hold(handles.axShipTrack,'on')

            % Turn off check boxes
            set(handles.cbShipBT,'Visible','off');
            set(handles.cbShipGGA,'Visible','off');
            set(handles.cbShipVTG,'Visible','off');
            set(handles.cbShipVectors,'Visible','off');

            % Get data to be plotted
            u=transect.wVel.uProcessed_mps(:,inTransectIdx);
            v=transect.wVel.vProcessed_mps(:,inTransectIdx);
            uAvg=nanmean(u);
            vAvg=nanmean(v);
            spd=sqrt(uAvg.^2+vAvg.^2);
            btX=meas.mbTests(testSelected).stationaryCSTrack;
            btY=meas.mbTests(testSelected).stationaryUSTrack;

            % Generate plot
            
            plot(handles.axShipTrack,btX.*unitsL,btY.*unitsL,'-r')
            if ~isempty(btX)
                hold(handles.axShipTrack,'on');
                plot(handles.axShipTrack,btX(end).*unitsL,btY(end).*unitsL,'sk','MarkerFaceColor','k','MarkerSize',8);
                quiver(handles.axShipTrack,btX.*unitsL,btY.*unitsL,zeros(size(btY)),spd.*-1.*unitsV,'b','ShowArrowHead','on');
            end
            xlabel(handles.axShipTrack,['Distance Cross Stream',' ',uLabelL]);
            ylabel(handles.axShipTrack,['Distance Upstream',' ',uLabelL]);
        end % if

        % General plot settings
        hold(handles.axShipTrack,'off')
        set(handles.axShipTrack,'DataAspectRatio',[1 1 1]);
        set(handles.axShipTrack,'Box','on');
        set(handles.axShipTrack,'PlotBoxAspectRatio',[1 1 1]); 
        grid(handles.axShipTrack,'On')
    end

function cbShipGGA_Callback(hObject, eventdata, handles)
    shiptrackPlot(handles)

function cbShipVTG_Callback(hObject, eventdata, handles)
    shiptrackPlot(handles)

function cbShipBT_Callback(hObject, eventdata, handles)
    shiptrackPlot(handles)

function cbShipVectors_Callback(hObject, eventdata, handles)
    shiptrackPlot(handles)

% --- Executes when entered data in editable cell(s) in tblMB.
function tblMB_CellEditCallback(hObject, eventdata, handles)
        % Determine row selected
        selection = eventdata.Indices;
    
        % If statement due to code executed twice. Not sure why but this fixes
        % the problem.
        if ~isempty(selection)
            
            % Retrieve table information
            row=eventdata.Indices(1);
            col=eventdata.Indices(2);
            tableData=get(handles.tblMB,'Data');
            
            % Retrieve data from hMainGui
            meas=getappdata(handles.hMainGui,'measurement'); 
            
            % Valid test determined by user
            if col==1
                % Get user input from table
                setting=cell2mat(tableData(:,1));
                setting=setting(~cellfun(@isempty,tableData(:,3)));
                handles = commentButton( handles, 'Justification for User Changed MB Test Validity' );
                
                  
                
                % Apply setting
                meas = movingBedTestChange(meas,'userValid',setting);
                
                % Update table
                handles=updateTblMsgs(handles,meas.mbTests);
                
                % Store data
                setappdata(handles.hMainGui,'measurement',meas);
                
            % Use for discharge correction by user
            elseif col==2
                
                % Check if test is valid
                if tableData{row,1}
                    % Check to see that there has been a manual override
                    if strcmp('Manual',tableData(row,15))
                        tableData(eventdata.Indices(1),2)={false}; 

                        % Apply setting       
                        meas = movingBedTestChange(meas,'manualCancel',row);

                        % Store data
                        setappdata(handles.hMainGui,'measurement',meas);
                    elseif strcmp(tableData(row,15),'Errors')
                        response=questdlg('QRev has determined this moving-bed test has critical errors and does not recommend using it for correction. If you choose to use the test anyway you will be required to justify its use.', 'Override Moving-Bed Test','Justify and Use','Cancel','Cancel');
%                                 msgbox('Invalid test cannot be used for correction. Please select a valid test.');
                        if strcmp(response,'Cancel')
                            tableData(eventdata.Indices(1),2)={false};
                        else
                            % Add comment
                            [handles]=commentButton(handles, 'Moving-Bed Test - Manual Override');
                            tableData(eventdata.Indices(1),2)={true};
                            % Retrieve data from hMainGui
                            meas=getappdata(handles.hMainGui,'measurement');   

                            % Apply setting       
                            meas = movingBedTestChange(meas,'manualOverride',row);

                            % Store data
                            setappdata(handles.hMainGui,'measurement',meas);
                        end                        
                        
                    elseif sum(strcmp('Yes',tableData(:,14)))>0            
                        applyCorrection=cell2mat(tableData(:,2));
                        applyCorrection=applyCorrection(~cellfun(@isempty,tableData(:,3)));

                        % Check to make sure the selected tests are of the same type
                        if length(unique(tableData(applyCorrection,4)))<2

                            % Check to make sure selected tests are valid
                            if sum(strcmp('Errors',tableData(applyCorrection,15)))==0

                                if sum(strcmp('Loop',tableData(applyCorrection,4)))<2
                                    
                                    % Retrieve data from hMainGui
                                    meas=getappdata(handles.hMainGui,'measurement');   

                                    % Apply setting          
                                    meas = movingBedTestChange(meas,'use2Correct',applyCorrection);

                                    % Store data
                                    setappdata(handles.hMainGui,'measurement',meas);
                                    
                                else
                                    msgbox('Only one loop can be applied. Select the best loop.');
                                end % if
                            end % if
                        else
                            msgbox('Application of mixed moving-bed test types is not allowed. Select only one loop or one or more stationary test.');
                        end % if

                    else
                        % Display message
                        msgbox('No moving-bed condition exists. No corrections can be applied');

                        % Update table
                        tableData{eventdata.Indices(1),2}=false;

                        % Set value in measurement data
                        meas=getappdata(handles.hMainGui,'measurement');             
                        meas.mbTests=setUse(meas.mbTests,0);

                        % Store measurement data
                        setappdata(handles.hMainGui,'measurement',meas);
                    end % if
                else
                    % Display message
                    msgbox('You must first mark the test valid in order to use it.');
                    % Update table
                    tableData{eventdata.Indices(1),2}=false;
                    
                end % if valid            
                % Update GUI 
                handles=updateTblMsgs(handles,meas.mbTests);
            end % if

            drawnow;
            % Set table view
%             tableScrollView( hObject, selection(1), 1 );
            % Update graphics
            updateGraphics(handles);
            
            % Update handles structure
            guidata(hObject, handles);
            
        end % if

% --- Executes when selected cell(s) is changed in tblMB.
function tblMB_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to tblMB (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)

 % Determine row selected
    selection = eventdata.Indices(:,1);
    
    % If statement due to code executed twice. Not sure why but this fixes
    % the problem.
    if ~isempty(selection)
        % Get table data
        tableData=get(handles.tblMB,'Data');

        handles.plottedTransect=selection;
        set(handles.plottedtxt,'String',tableData{selection,3});
    end
    
    % Update graphics
    updateGraphics(handles);
    guidata(hObject, handles);

% --- Executes on button press in pbLoad.
function pbLoad_Callback(hObject, eventdata, handles)
% hObject    handle to pbLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Determine source of data
    source=meas.transects(1).adcp.manufacturer;
    
    % Get previous or set current folder as the path
     prefPath=getUserPref('Folder');
    
    % Process TRDI data
    if strcmpi(source,'TRDI')
        
        [fileName,pathName]=uigetfile('*.mmt','SELECT *.mmt WITH THE APPROPRIATE MOVING-BED TEST ',prefPath);
        meas.mbTests=clsMovingBedTests('TRDI',fileName);
    
    % Process SonTek data
    elseif strcmpi(source,'SonTek')
        [fileNames,pathName]=uigetfile('*.mat','Multiselect','on','SELECT SONTEK MATLAB TRANSECT FILES FOR PROCESSING',prefPath);
        % Determine Number of Files to be Processed.
        if  isa(fileNames,'cell')
            numFiles=size(fileNames,2); 
            fileNames=sort(fileNames);       
        else
            numFiles=1;
            fileNames={fileNames};
        end
        setUserPref('Folder',pathName);
        for j=1:numFiles
            fullName(j)=strcat(pathName,fileNames(j));
            file=fileNames{j};
            if strcmp(file(1:4),'Loop')
                type={'Loop'};
            elseif strcmp(file(1:4),'Smba')
                type={'Stationary'};
            else
                question=['What type of moving-bed test is ' file '?'];
                type={questdlg(question,'Moving-Bed Test','Loop','Stationary','Loop')};
            end % if
            % Create objects
            n=length(meas.mbTests);
            if n==0
                meas.mbTests=clsMovingBedTests('SonTek',type,fullName(j));
            else  
                meas.mbTests(n+1)=clsMovingBedTests('SonTek',type,fullName(j));
            end
        end
    end % if source
    c=1;
    nTests=size(meas.mbTests,2);
    mbTests=meas.mbTests;
    handles=updateTblMsgs(handles,mbTests); 
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Update graphics
    updateGraphics(handles);
    
function handles=updateTblMsgs(handles,mbTests)
% Function to update GUI table and messages

    nmess=0;
    
    % Get table from gui
    tableData=get(handles.tblMB,'Data');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    output={};
    nTests=length(mbTests);
    
    % Put data in table for each test
    for n=1:nTests
        
        idx=find(mbTests(n).transect.fileName=='\',1,'last');
        if isempty(idx)
            idx=0;
        end
        
        tableData{n,1}=logical(mbTests(n).userValid);
        tableData{n,2}=logical(mbTests(n).use2Correct);
        tableData{n,3}=mbTests(n).transect.fileName(idx+1:end);
        if mbTests(n).selected
            tableData{n,4}=['<html><b>',mbTests(n).type];
        else
            tableData{n,4}=mbTests(n).type;
        end
        tableData{n,5}=mbTests(n).duration_sec;
        tableData{n,6}=mbTests(n).distUS_m.*unitsL;
        tableData{n,7}=mbTests(n).mbSpd_mps.*unitsV;
        tableData{n,8}=mbTests(n).mbDir_deg;
        tableData{n,9}=mbTests(n).flowSpd_mps.*unitsV;
        tableData{n,10}=mbTests(n).flowDir_deg;
        tableData{n,11}=mbTests(n).percentInvalidBT;
        tableData{n,12}=mbTests(n).compassDiff_deg;
        tableData{n,13}=mbTests(n).percentMB;
        tableData{n,14}=mbTests(n).movingBed;
        tableData{n,15}=mbTests(n).testQuality;
        
        % Get messages for each test
        messCell=mbTests(n).messages;
        kmessages=length(messCell);
        if kmessages>0
            nmess=nmess+1;
            output(nmess)={sprintf(tableData{n,3})};
            for k=1:kmessages
                nmess=nmess+1;
                output(nmess)={sprintf(messCell{k})};
            end % for
        end % if       
    end % for
    % Update GUI table and messages
    set(handles.tblMB,'Data',tableData(1:nTests,:));
    drawnow;
    outstr=char(output);
    set(handles.txtMessage,'String',outstr); 

% --- Executes on key press with focus on tblMB and none of its controls.
function tblMB_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to tblMB (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

% Make table inactive to avoid synching issues associated with multiple
% keypresses before the processing of the first can be completed.
set(handles.tblMB,'Enable','inactive');

if strcmp(eventdata.Key,'uparrow') || strcmp(eventdata.Key,'downarrow')
    tableData=get(handles.tblMB,'Data');
    rowTrue=handles.plottedTransect;
    if strcmp(eventdata.Key,'downarrow')
        if rowTrue+1<=size(tableData,1)
            newRow=rowTrue+1;          
        else
            newRow=size(tableData,1);
        end
    elseif strcmp(eventdata.Key,'uparrow')
        if rowTrue-1>0
            newRow=rowTrue-1;
        else
            newRow=1;
        end
    end
  
    set(handles.plottedtxt,'String',tableData{newRow,2});
    handles.plottedTransect=newRow;
    drawnow

    % Update graphics
    updateGraphics(handles);
    guidata(hObject, handles);
    drawnow
end    
set(handles.tblMB,'Enable','on');         


% --- Executes on button press in pbHelp.
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%open('helpFiles\QRev_Users_Manual.pdf')  
web('QRev_Help_Files\HTML\moving_bed_test.htm')
