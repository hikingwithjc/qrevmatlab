function varargout = selectTransect(varargin)
% SELECTTRANSECT MATLAB code for selectTransect.fig
%      SELECTTRANSECT, by itself, creates a new SELECTTRANSECT or raises the existing
%      singleton*.
%
%      H = SELECTTRANSECT returns the handle to a new SELECTTRANSECT or the handle to
%      the existing singleton*.
%
%      SELECTTRANSECT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SELECTTRANSECT.M with the given input arguments.
%
%      SELECTTRANSECT('Property','Value',...) creates a new SELECTTRANSECT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before selectTransect_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to selectTransect_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help selectTransect

% Last Modified by GUIDE v2.5 06-Jan-2017 12:21:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @selectTransect_OpeningFcn, ...
                   'gui_OutputFcn',  @selectTransect_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before selectTransect is made visible.
function selectTransect_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to selectTransect (see VARARGIN)

% Choose default command line output for selectTransect
handles.output = hObject;

    % Set window position

    % Get data passed to this GUI
    meas=varargin{1};
    handles.parentGUI=varargin{2};
    
    % Set window position
    set(hObject,'Units',handles.parentGUI.guiMain.Units);
    pos=hObject.Position;
    mainpos=handles.parentGUI.guiMain.Position;
    left=mainpos(1)+(mainpos(3)-pos(3))./2;
    right=mainpos(2)+(mainpos(4)-pos(4))./2;
    newpos=[left,right,pos(3:4)];
    set(hObject,'Position',newpos);
    
    % Use units used by QRev and stored in AppData/Local
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles.parentGUI);
        
    % Display transects used in QRev for discharge
    nTransects=length(meas.transects);
    handles.checkedIdx=find([meas.transects.checked]==1);
    k=0;
    for n=1:nTransects
        if meas.transects(n).checked
            k=k+1;
            tableData{k,1}=false;
            tableData{k,2}=meas.transects(n).fileName;
            tableData{k,3}=meas.transects(n).startEdge;
            tableData{k,4}=num2str(meas.discharge(n).total.*unitsQ,'%10.2f');
        end
    end
    set(handles.transectTbl,'Data',tableData);
    handles.selection=0;

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = selectTransect_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pbOK.
function pbOK_Callback(hObject, eventdata, handles)
% hObject    handle to pbOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Pass data to parent GUI using UserData
setappdata(handles.parentGUI.hMainGui,'EDI_Transect_Index',handles.selection);
guidata(hObject,handles);

% Close this window
delete(handles.output);


% --- Executes when selected cell(s) is changed in transectTbl.
function transectTbl_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to transectTbl (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)
    
    % Get row number selected
    row=eventdata.Indices(:,1);
    
    % This if statement prevents the function from using empty selection
    % and executing twice.
    if ~isempty(row)
        % Get data from table
        tableData=get(handles.transectTbl,'Data');
        % Set all selected to false
        tableData(:,1)={false};
        % Set selected row to true
        tableData(row,1)={true};
        % Update table
        set(handles.transectTbl,'Data',tableData);
        % Save selected transect number
        handles.selection=handles.checkedIdx(row);
        guidata(hObject, handles);
    end
