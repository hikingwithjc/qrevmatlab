function varargout = winStartBank(varargin)
% WINSTARTBANK MATLAB code for winStartBank.fig
%      WINSTARTBANK, by itself, creates a new WINSTARTBANK or raises the existing
%      singleton*.
%
%      H = WINSTARTBANK returns the handle to a new WINSTARTBANK or the handle to
%      the existing singleton*.
%
%      WINSTARTBANK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINSTARTBANK.M with the given input arguments.
%
%      WINSTARTBANK('Property','Value',...) creates a new WINSTARTBANK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winStartBank_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winStartBank_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winStartBank

% Last Modified by GUIDE v2.5 12-Aug-2016 17:31:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winStartBank_OpeningFcn, ...
                   'gui_OutputFcn',  @winStartBank_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function winStartBank_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winStartBank (see VARARGIN)

    % Choose default command line output for winStartBank
    handles.output = hObject;

    % Get main gui handle
    handles.hMainGui=varargin{1};
    handles.parentGUI=varargin{2};

    % Set the window position to lower left
    setWindowPosition(hObject,handles);
    
    % Change comment icon
    commentIcon(handles.pbComment);
       
    % Display current start bank
    if strcmpi(varargin{3},'Left')
        set(handles.rbLeft,'Value',1)
    else
        set(handles.rbRight,'Value',1)
    end
    handles.s=struct('process','Cancel','bank',varargin{3});
    handles.parentGUI.UserData=handles.s;
% Update handles structure
guidata(hObject, handles);

function varargout = winStartBank_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function pbOne_Callback(hObject, eventdata, handles)
% hObject    handle to pbOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
%     % Set pointer to busy
%     oldpointer = get(gcf, 'pointer');      
%     set(gcf, 'pointer', 'watch');     
%     drawnow;     
% 
%     % Get data from hMainGui
%     meas=getappdata(handles.hMainGui,'measurement');
%     
%     % Apply heading
%     meas=changeHeadingSource(meas,handles.HSource,handles.selected);
%     
%     % Store data
%     setappdata(handles.hMainGui,'measurement',meas);
%    
%     % Return pointer to previous
%     set(gcf, 'pointer', oldpointer);     
%     drawnow;
    handles.s.process='One';
    handles.parentGUI.UserData=handles.s;
    guidata(hObject, handles);
    
    % Close winStartBank
    delete (handles.output);  

function pbAll_Callback(hObject, eventdata, handles)
% hObject    handle to pbAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%     % Set pointer to busy
%     oldpointer = get(gcf, 'pointer');      
%     set(gcf, 'pointer', 'watch');     
%     drawnow;     
% 
%     % Get data from hMainGui
%     meas=getappdata(handles.hMainGui,'measurement');
%        
%     % Apply heading
%     meas=changeHeadingSource(meas,handles.HSource);
%    
%     % Store data
%     setappdata(handles.hMainGui,'measurement',meas);
%     
%     % Set pointer to previous
%     set(gcf, 'pointer', oldpointer);     
%     drawnow;
    handles.s.process='All';
    handles.parentGUI.UserData=handles.s;
    guidata(hObject, handles);
    
    % Close winStartBank
    delete (handles.output);  

function pbCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pbCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Close winStartBank
    handles.s.process='Cancel';
    handles.parentGUI.UserData=handles.s;
    guidata(hObject, handles);
    delete (handles.output);  

function pbComment_Callback(hObject, eventdata, handles)
% hObject    handle to pbComment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Add comment
    [handles]=commentButton(handles, 'Depth');
    % Update handles structure
    guidata(hObject, handles);    
    
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
   % open('helpFiles\QRev_Users_Manual.pdf') 
    web('QRev_Help_Files\HTML\edges.htm')

% --- Executes when selected object is changed in bgBank.
function bgBank_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in bgBank 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.rbLeft,'Value')
    handles.s.bank='Left';
else
    handles.s.bank='Right';
end
guidata(hObject, handles);
