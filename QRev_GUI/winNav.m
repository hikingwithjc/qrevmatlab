function varargout = winNav(varargin)
% WINNAV MATLAB code for winNav.fig
%      WINNAV, by itself, creates a new WINNAV or raises the existing
%      singleton*.
%
%      H = WINNAV returns the handle to a new WINNAV or the handle to
%      the existing singleton*.
%
%      WINNAV('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINNAV.M with the given input arguments.
%
%      WINNAV('Property','Value',...) creates a new WINNAV or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winNav_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winNav_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winNav

% Last Modified by GUIDE v2.5 02-Feb-2016 13:12:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winNav_OpeningFcn, ...
                   'gui_OutputFcn',  @winNav_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before winNav is made visible.
function winNav_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winNav (see VARARGIN)

% Choose default command line output for winNav
handles.output = hObject;
    
    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end

    setWindowPosition(hObject,handles);
    
    commentIcon(handles.pbComment);

    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    handles.checkedIdx=find([meas.transects.checked]==1);
    
    % Get reference settings
    boatVel=[meas.transects(handles.checkedIdx).boatVel];
    composite={boatVel.composite};
    ref={boatVel.selected};

    % Reference setting
    if length(unique(ref))>1
        warndlg('The navigation reference varies among transects. The reference will be set to the valued in first transect.');
    end
    
    switch ref{1}
        case 'btVel'
            set(handles.rbBT,'Value',1);
        case 'ggaVel'
            set(handles.rbGGA,'Value',1);
        case 'vtgVel'
            set(handles.rbVTG,'Value',1);
    end
    
    % Composite setting
    if strcmp(composite,'On')
        set(handles.rbOn,'Value',1);
    else
        set(handles.rbOff,'Value',1);
    end
    handles.plottedTransect=1;
    updateData(handles,meas,meas.discharge);
    updateGraphics(handles);
    drawnow;
    
% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = winNav_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes when selected object is changed in panelRef.
function panelRef_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in panelRef 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
% Get settings from GUI
    radioTag=get(get(handles.panelRef,'SelectedObject'),'String');
    meas=getappdata(handles.hMainGui,'measurement');
    dischargeOld=meas.discharge;
    settings=clsMeasurement.currentSettings(meas);
    nTransects=length(handles.checkedIdx);
    switch radioTag
        case 'BT'
            target='btVel';
        case 'GGA'
            target='ggaVel';
        case 'VTG'
            target='vtgVel';
    end
    proceed=true;
    for n=1:nTransects
        if logical(meas.transects(handles.checkedIdx(n)).checked)
            if isempty(meas.transects(handles.checkedIdx(n)).boatVel.(target))
                proceed=false;
            end
        end
    end
    if proceed
            
        settings=clsMeasurement.currentSettings(meas);
        settings.NavRef=radioTag;

        % Apply settings
        oldpointer = get(gcf, 'pointer');      
        set(gcf, 'pointer', 'watch');     
        drawnow;     
        meas=applySettings(meas,settings);     
        set(gcf, 'pointer', oldpointer);     
        drawnow;


        % Save measurement data
        setappdata(handles.hMainGui,'measurement',meas);

        % Update graphics
        updateGraphics(handles);
        updateData(handles,meas,dischargeOld)
    else
        errordlg('All checked transects do not have the selected reference','Reference Warning');
        switch settings.NavRef
            case 'btVel'
                set(handles.rbBT,'Value',1);
            case 'ggaVel'
                set(handles.rbGGA,'Value',1);
            case 'vtgVel'
                set(handles.rbVTG,'Value',1);
        end
    end

    guidata(hObject, handles);
    
function panelComp_SelectionChangeFcn(hObject, eventdata, handles)
    % Get settings from GUI
    radioTag=get(get(handles.panelComp,'SelectedObject'),'String');
    meas=getappdata(handles.hMainGui,'measurement');
    s=clsMeasurement.currentSettings(meas);
    dischargeOld=meas.discharge;
    s.CompTracks=radioTag;
    % Apply settings
        oldpointer = get(gcf, 'pointer');      
        set(gcf, 'pointer', 'watch');     
        drawnow;     
        meas=applySettings(meas,s);     
        set(gcf, 'pointer', oldpointer);     
        drawnow;
    setappdata(handles.hMainGui,'measurement',meas);
   % Update graphics
    updateGraphics(handles);
    updateData(handles,meas,dischargeOld)
    guidata(hObject, handles);

    % --- Executes when entered data in editable cell(s) in tblRef.
function tblRef_CellSelectionCallback(hObject, eventdata, handles)
% Handles file selection to be plotted. Allows the selection to work as
% radio buttons so that only one file can be selected

    % Determine row selected
    selection = eventdata.Indices(:,1);
    
    % If statement due to code executed twice. Not sure why but this fixes
    % the problem.
    if ~isempty(selection)
        % Get table data
        tableData=get(handles.tblRef,'Data');
        handles.plottedTransect=selection;
        set(handles.plottedtxt,'String',tableData{selection,1});
    end
    
    % Update graphics
    updateGraphics(handles);
    
    guidata(hObject, handles);
    
function pbComment_Callback(hObject, eventdata, handles)
% Add comment
    [handles]=commentButton(handles, 'Navigation Reference');
    % Update handles structure
    guidata(hObject, handles);    
    
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%open('helpFiles\QRev_Users_Manual.pdf')      
web('QRev_Help_Files\HTML\selected_reference.htm')
    
function pbClose_Callback(hObject, eventdata, handles)
    delete (handles.output);  

% Graphics

function updateGraphics(handles)
    refPlot(handles);
    speedPlot(handles);
    shiptrackPlot(handles);
    linkaxes([handles.axTop,handles.axBottom],'x');
% Top Plot

function refPlot(handles)
   
    tableData=get(handles.tblRef,'Data');
        
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    transectSelected=handles.checkedIdx(handles.plottedTransect);
    if isempty(transectSelected)
        transectSelected=1;
    end
    set(handles.plottedtxt,'String',tableData{handles.plottedTransect,1});
    boatVel=meas.transects(transectSelected).boatVel;
    if ~isempty(boatVel.(boatVel.selected))
        compSource=boatVel.(boatVel.selected).processedSource;
    else
        compSource=cell(size(boatVel.btVel.processedSource));
        compSource(1:end)={'INV'};
    end
    compSourcePlt=nan(size(compSource));
    
    % Plot INV as -1
    idxC=strfind(compSource,'INV');
    idx = find(not(cellfun('isempty', idxC)));
    if ~isempty(idx)
        compSourcePlt(idx)=-1;
    end
    
    % Plot INT as 0
    idxC=strfind(compSource,'INT');
    idx = find(not(cellfun('isempty', idxC)));
    if ~isempty(idx)
        compSourcePlt(idx)=0;
    end
    
    % Plot BT as 1
    idxC=strfind(compSource,'BT');
    idx = find(not(cellfun('isempty', idxC)));
    if ~isempty(idx)
        compSourcePlt(idx)=1;
    end
    
    % Plot GGA as 2
    idxC=strfind(compSource,'GGA');
    idx = find(not(cellfun('isempty', idxC)));
    if ~isempty(idx)
        compSourcePlt(idx)=2;
    end
    
    % Plot VTG as 3
    idxC=strfind(compSource,'VTG');
    idx = find(not(cellfun('isempty', idxC)));
    if ~isempty(idx)
        compSourcePlt(idx)=3;
    end
    
    % Generate selected plot
    cla(handles.axTop)

    plot(handles.axTop,compSourcePlt,'*b')
    xlabel(handles.axTop,'Ensembles');
    ylabel(handles.axTop,'Navigation Source');
    ylim(handles.axTop,[-1,3]);
    set(handles.axTop,'YTick',[-1,0,1,2,3]);
    set(handles.axTop,'YTickLabel',{'INV';'INT';'BT';'GGA';'VTG'});
    grid(handles.axTop,'On')
    if strcmpi(meas.transects(transectSelected).startEdge,'Right')
        set(handles.axTop,'XDir','reverse');
    else
        set(handles.axTop,'XDir','normal');
    end
% Bottom Plot

function speedPlot(handles)
    cla(handles.axBottom)
    tableData=get(handles.tblRef,'Data');
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);    
    
    transectSelected=handles.checkedIdx(handles.plottedTransect);
    if isempty(transectSelected)
        transectSelected=1;
    end
    transect=meas.transects(transectSelected);
    
    ensembles=1:length(transect.boatVel.btVel.uProcessed_mps);
    hold(handles.axBottom,'on');
    
    if ~isempty(transect.boatVel.ggaVel)
        if get(handles.cbSpdGGA,'Value')
            speed=sqrt(transect.boatVel.ggaVel.u_mps.^2+transect.boatVel.ggaVel.v_mps.^2);
            speedProcessed=sqrt(transect.boatVel.ggaVel.uProcessed_mps.^2+transect.boatVel.ggaVel.vProcessed_mps.^2);
            plot(handles.axBottom,ensembles,speed.*unitsV,':b')
            plot(handles.axBottom,ensembles,speedProcessed.*unitsV,'-b')
            validData=transect.boatVel.ggaVel.validData;
            if sum(validData(1,:))<length(ensembles)
                plot(handles.axBottom,ensembles(~validData(1,:)),0,'ob')
            end % if sum
        end 
    end
    
    if ~isempty(transect.boatVel.vtgVel)
        if get(handles.cbSpdVTG,'Value')
            speed=sqrt(transect.boatVel.vtgVel.u_mps.^2+transect.boatVel.vtgVel.v_mps.^2);
            speedProcessed=sqrt(transect.boatVel.vtgVel.uProcessed_mps.^2+transect.boatVel.vtgVel.vProcessed_mps.^2);
            plot(handles.axBottom,ensembles,speed.*unitsV,':','Color',[0 0.498 0])
            plot(handles.axBottom,ensembles,speedProcessed.*unitsV,'-','Color',[0 0.498 0])
            validData=transect.boatVel.vtgVel.validData;
            if sum(validData(1,:))<length(ensembles)
                plot(handles.axBottom,ensembles(~validData(1,:)),0,'o','Color',[0 0.498 0])
            end % if sum
        end 
    end

    if get(handles.cbSpdBT,'Value')
        speed=sqrt(transect.boatVel.btVel.u_mps.^2+transect.boatVel.btVel.v_mps.^2);
        speedProcessed=sqrt(transect.boatVel.btVel.uProcessed_mps.^2+transect.boatVel.btVel.vProcessed_mps.^2);
        plot(handles.axBottom,ensembles,speed.*unitsV,':r')
        plot(handles.axBottom,ensembles,speedProcessed.*unitsV,'-r')    
    end
    
    box (handles.axBottom,'on');
    xlabel(handles.axBottom,'Ensemble');
    ylabel(handles.axBottom,['Speed ',uLabelV]);
    grid(handles.axBottom,'On')
    hold (handles.axBottom, 'off');
    if strcmpi(meas.transects(transectSelected).startEdge,'Right')
        set(handles.axBottom,'XDir','reverse');
    else
        set(handles.axBottom,'XDir','normal');
    end
function cbSpdGGA_Callback(hObject, eventdata, handles)
    speedPlot(handles)

function cbSpdVTG_Callback(hObject, eventdata, handles)
    speedPlot(handles)

function cbSpdBT_Callback(hObject, eventdata, handles)
    speedPlot(handles)
    
% ShipTrack

function shiptrackPlot(handles)
    cla(handles.axShipTrack)
    tableData=get(handles.tblRef,'Data');
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);    
    
    transectSelected=handles.checkedIdx(handles.plottedTransect);
    if isempty(transectSelected)
        transectSelected=1;
    end
    transect=meas.transects(transectSelected);
    
    % Compute track
    ensDuration=transect.dateTime.ensDuration_sec;
    % BT
    btUProcessed=transect.boatVel.btVel.uProcessed_mps;
    btVProcessed=transect.boatVel.btVel.vProcessed_mps;
    btXProcessed=nancumsum(btUProcessed.*ensDuration);
    btYProcessed=nancumsum(btVProcessed.*ensDuration);
    % GGA
    if ~isempty(transect.boatVel.ggaVel)
        ggaUProcessed=transect.boatVel.ggaVel.uProcessed_mps;
        ggaVProcessed=transect.boatVel.ggaVel.vProcessed_mps;
        ggaXProcessed=nancumsum(ggaUProcessed.*ensDuration);
        ggaYProcessed=nancumsum(ggaVProcessed.*ensDuration);
    end
    % VTG
    if ~isempty(transect.boatVel.vtgVel)
        vtgUProcessed=transect.boatVel.vtgVel.uProcessed_mps;
        vtgVProcessed=transect.boatVel.vtgVel.vProcessed_mps;
        vtgXProcessed=nancumsum(vtgUProcessed.*ensDuration);
        vtgYProcessed=nancumsum(vtgVProcessed.*ensDuration);
    end

    % Compute water vectors for select reference
    waterRef=transect.wVel.navRef;
    wVelX=transect.wVel.uProcessed_mps;
    wVelY=transect.wVel.vProcessed_mps;
    meanVelX=nanmean(wVelX);
    meanVelY=nanmean(wVelY);
     
    % Plot shiptrack
    hold(handles.axShipTrack,'on')
    
    % Plot BT
    if get(handles.cbShipBT,'Value') 
        plotted(1,1)=1;
        plot(handles.axShipTrack,btXProcessed.*unitsL,btYProcessed.*unitsL,'-r','LineWidth',2)
        % Only plot vectors for selected water track nav reference
        if strcmp(waterRef,'BT') && get(handles.cbShipVectors,'Value')
            plotted(2,1)=1;
            quiver(handles.axShipTrack,btXProcessed.*unitsL,btYProcessed.*unitsL,meanVelX.*unitsV,meanVelY.*unitsV,'k','ShowArrowHead','on','AutoScaleFactor',5);
            plot(handles.axShipTrack,btXProcessed(end).*unitsL,btYProcessed(end).*unitsL,'sk','MarkerFaceColor','k','MarkerSize',8);
            plotted(3,1)=1;
        else
            plotted(2,1)=0;
            plotted(3,1)=0;
        end % if
    end % if
    
    % Plot VTG
    if ~isempty(transect.boatVel.vtgVel)
        if get(handles.cbShipVTG,'Value')
            plotted(4,1)=1;
            plot(handles.axShipTrack,vtgXProcessed.*unitsL,vtgYProcessed.*unitsL,'-','Color',[0 0.498 0],'LineWidth',2)
            % Only plot vectors for selected water track nav reference
            if strcmp(waterRef,'VTG') && get(handles.cbShipVectors,'Value') 
                plotted(5,1)=1;
                quiver(handles.axShipTrack,vtgXProcessed.*unitsL,vtgYProcessed.*unitsL,meanVelX.*unitsV,meanVelY.*unitsV,'k','ShowArrowHead','on','AutoScaleFactor',5);
                plot(handles.axShipTrack,vtgXProcessed(end).*unitsL,vtgYProcessed(end).*unitsL,'sk','MarkerFaceColor','k','MarkerSize',8);
                plotted(6,1)=1;
            else
                plotted(5,1)=0;
                plotted(6,1)=0;
            end % if
        end % if
    end
    
    % Plot GGA
    if ~isempty(transect.boatVel.ggaVel)
        if get(handles.cbShipGGA,'Value') 
            plotted(7,1)=1;
            plot(handles.axShipTrack,ggaXProcessed.*unitsL,ggaYProcessed.*unitsL,'-b','LineWidth',2)
            % Only plot vectors for selected water track nav reference
            if strcmp(waterRef,'GGA') && get(handles.cbShipVectors,'Value')
                plotted(8,1)=1;
                quiver(handles.axShipTrack,ggaXProcessed.*unitsL,ggaYProcessed.*unitsL,meanVelX.*unitsV,meanVelY.*unitsV,'k','ShowArrowHead','on','AutoScaleFactor',5);
                plot(handles.axShipTrack,ggaXProcessed(end).*unitsL,ggaYProcessed(end).*unitsL,'sk','MarkerFaceColor','k','MarkerSize',8);
                plotted(9,1)=1;
            else
                plotted(8,1)=0;
                plotted(9,1)=0;
            end % if
        end % if    
    end
    hold(handles.axShipTrack,'off')
    set(handles.axShipTrack,'DataAspectRatio',[1 1 1]);
    set(handles.axShipTrack,'Box','on');
    set(handles.axShipTrack,'PlotBoxAspectRatio',[1 1 1]);
    legendText={'BT','Water Vector','End','VTG','Water Vector','End','GGA','Water Vector','End'};
    legend(handles.axShipTrack,legendText{logical(plotted)},'Location','best')    
    xlabel(handles.axShipTrack,['East Distance ',uLabelL]);
    ylabel(handles.axShipTrack,['North Distance ',uLabelL]);
    grid(handles.axShipTrack,'On')

function cbShipGGA_Callback(hObject, eventdata, handles)
    shiptrackPlot(handles)

function cbShipVTG_Callback(hObject, eventdata, handles)
    shiptrackPlot(handles)

function cbShipBT_Callback(hObject, eventdata, handles)
    shiptrackPlot(handles)
    
function cbShipVectors_Callback(hObject, eventdata, handles)
    shiptrackPlot(handles)

% Table & Data

function updateData(handles,meas,dischargeOld)

    % Get table from gui
    tableData=get(handles.tblRef,'Data');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Set units for column names
    colNames=get(handles.tblRef,'ColumnName');
    colNames{7}=[colNames{7},' ',uLabelQ];
    colNames{8}=[colNames{8},' ',uLabelQ];
    set(handles.tblRef,'ColumnName',colNames);
    
    % Get data for table from each transect
    nTransects=length(handles.checkedIdx);
    for n=1:nTransects
    % Get data needed to populate the table
        if ~isempty(meas.transects(handles.checkedIdx(n)).boatVel.(meas.transects(handles.checkedIdx(n)).boatVel.selected))
            compSource=meas.transects(handles.checkedIdx(n)).boatVel.(meas.transects(handles.checkedIdx(n)).boatVel.selected).processedSource;
            idxBT=cell2mat(strfind(compSource,'BT'));
            idxGGA=cell2mat(strfind(compSource,'GGA'));
            idxVTG=cell2mat(strfind(compSource,'VTG'));
            idxINT=cell2mat(strfind(compSource,'INT'));
        else
            compSource='INV';
            idxBT=[];
            idxGGA=[];
            idxVTG=[];
            idxINT=[];
        end
       
        % Populate the table
        idx=find(meas.transects(handles.checkedIdx(n)).fileName=='\',1,'last');
        if isempty(idx)
            idx=0;
        end
        tableData{n,1}=meas.transects(handles.checkedIdx(n)).fileName(idx+1:end);
        tableData{n,2}=sprintf('% 10.0f',length(compSource));        
        tableData{n,3}=sprintf('% 6.0f',length(idxBT));
        tableData{n,4}=sprintf('% 6.0f',length(idxGGA));
        tableData{n,5}=sprintf('% 6.0f',length(idxVTG));
        tableData{n,6}=sprintf('% 6.0f',length(idxINT));
        tableData{n,7}=sprintf('% 12.2f',dischargeOld(handles.checkedIdx(n)).total.*unitsQ);
        tableData{n,8}=sprintf('% 12.2f',meas.discharge(handles.checkedIdx(n)).total.*unitsQ);
        tableData{n,9}=sprintf('% 12.2f',((meas.discharge(handles.checkedIdx(n)).total-dischargeOld(handles.checkedIdx(n)).total)./dischargeOld(handles.checkedIdx(n)).total).*100);
    end % for n
    
    % Update table
    set(handles.tblRef,'Data',tableData(1:nTransects,:));

    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
 
function tblRef_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to tblGPS (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)    

% Make table inactive to avoid synching issues associated with multiple
% keypresses before the processing of the first can be completed.
set(handles.tblRef,'Enable','inactive');

if strcmp(eventdata.Key,'uparrow') || strcmp(eventdata.Key,'downarrow')
    tableData=get(handles.tblRef,'Data');
    rowTrue=handles.plottedTransect;
    if strcmp(eventdata.Key,'downarrow')
        if rowTrue+1<=size(tableData,1)
            newRow=rowTrue+1;          
        else
            newRow=size(tableData,1);
        end
    elseif strcmp(eventdata.Key,'uparrow')
        if rowTrue-1>0
            newRow=rowTrue-1;
        else
            newRow=1;
        end
    end
        
    set(handles.plottedtxt,'String',tableData{newRow,1});
    handles.plottedTransect=newRow;
    drawnow
    
    % Update graphics
    updateGraphics(handles);
    guidata(hObject, handles);
    drawnow
end    
set(handles.tblRef,'Enable','on');

%==========================================================================
%================NO MODIFIED CODE BELOW HERE===============================
%==========================================================================

function edit1_Callback(hObject, eventdata, handles)

function edit1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit2_Callback(hObject, eventdata, handles)

function edit2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit3_Callback(hObject, eventdata, handles)

function edit3_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

