function varargout = winEdgeDistNum(varargin)
% WINEDGEDISTNUM MATLAB code for winEdgeDistNum.fig
%      WINEDGEDISTNUM, by itself, creates a new WINEDGEDISTNUM or raises the existing
%      singleton*.
%
%      H = WINEDGEDISTNUM returns the handle to a new WINEDGEDISTNUM or the handle to
%      the existing singleton*.
%
%      WINEDGEDISTNUM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINEDGEDISTNUM.M with the given input arguments.
%
%      WINEDGEDISTNUM('Property','Value',...) creates a new WINEDGEDISTNUM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winEdgeDistNum_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winEdgeDistNum_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winEdgeDistNum

% Last Modified by GUIDE v2.5 17-Aug-2016 16:23:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winEdgeDistNum_OpeningFcn, ...
                   'gui_OutputFcn',  @winEdgeDistNum_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function winEdgeDistNum_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winEdgeDistNum (see VARARGIN)

% Choose default command line output for winEdgeDistNum
handles.output = hObject;

    % Get main gui handle
    handles.hMainGui=varargin{1};
    handles.parentGUI=varargin{2};
    
    handles.edge=varargin{3};
    handles.prop=varargin{4};
    handles.value=varargin{5};
    
    % Set the window position to lower left
    setWindowPosition(hObject,handles);
    
    % Change comment icon
    commentIcon(handles.pbComment);
      
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Set the label to reflect the current units
    if strcmp(handles.prop,'Edge Distance')
        set(handles.txtEdgeData,'String',[handles.edge, ' ',handles.prop, ' ',uLabelL]); 
        set(handles.edEdgeData,'String',sprintf('% 8.2f',handles.value*unitsL));
    else
        set(handles.txtEdgeData,'String',[handles.edge, ' ',handles.prop]);
        set(handles.edEdgeData,'String',sprintf('% 8.0f',handles.value));
    end
    
    handles.s=struct('process','Cancel','value',handles.value);
    handles.parentGUI.UserData=handles.s;

    % Update handles structure
    guidata(hObject, handles);

function varargout = winEdgeDistNum_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function edEdgeData_Callback(hObject, eventdata, handles)
% hObject    handle to edEdgeData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    % Get units
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);

    % Get user input
    value=str2double(get(handles.edEdgeData,'String'));
    if value<0
         errordlg('Values less than zero not allowed','Entry Error');
         if strcmp(handles.prop,'Edge Distance')
            set(handles.edEdgeData,'String',sprintf('% 8.2f',handles.s.value*unitsL));
         else
            set(handles.edEdgeData,'String',handles.s.value);
         end
    else
        if strcmp(handles.prop,'Edge Distance')
            handles.s.value=value./unitsL;
        else
            handles.s.value=value;
        end
    end
    guidata(hObject, handles);

function edEdgeData_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edEdgeData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pbOne_Callback(hObject, eventdata, handles)
% hObject    handle to pbOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    handles.s.process='One';
    handles.parentGUI.UserData=handles.s;
    guidata(hObject, handles);
    
    % Close winStartBank
    delete (handles.output);  

function pbAll_Callback(hObject, eventdata, handles)
% hObject    handle to pbAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    handles.s.process='All';
    handles.parentGUI.UserData=handles.s;
    guidata(hObject, handles);
    
    % Close winStartBank
    delete (handles.output);    

function pbCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pbCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Close winEdgeDistNum
    handles.s.process='Cancel';
    handles.parentGUI.UserData=handles.s;
    guidata(hObject, handles);
    delete (handles.output);   

function pbComment_Callback(hObject, eventdata, handles)
% hObject    handle to pbComment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Add comment
    [handles]=commentButton(handles, 'Depth');
    % Update handles structure
    guidata(hObject, handles);    
    
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    %open('helpFiles\QRev_Users_Manual.pdf') 
web('QRev_Help_Files\HTML\edges.htm')
