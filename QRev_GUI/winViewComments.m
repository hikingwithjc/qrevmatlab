function varargout = winViewComments(varargin)
% WINVIEWCOMMENTS MATLAB code for winViewComments.fig
%      WINVIEWCOMMENTS, by itself, creates a new WINVIEWCOMMENTS or raises the existing
%      singleton*.
%
%      H = WINVIEWCOMMENTS returns the handle to a new WINVIEWCOMMENTS or the handle to
%      the existing singleton*.
%
%      WINVIEWCOMMENTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINVIEWCOMMENTS.M with the given input arguments.
%
%      WINVIEWCOMMENTS('Property','Value',...) creates a new WINVIEWCOMMENTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winViewComments_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winViewComments_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winViewComments

% Last Modified by GUIDE v2.5 27-Feb-2017 16:38:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winViewComments_OpeningFcn, ...
                   'gui_OutputFcn',  @winViewComments_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



function winViewComments_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winViewComments (see VARARGIN)

    % Choose default command line output for winViewComments
    handles.output = hObject;

    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end
    
    setWindowPosition(hObject,handles);
    
    commentIcon(handles.pbComment);
    
    handles=updateCommentsDisplay(handles);
    
    % Update handles structure
    guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = winViewComments_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function handles=updateCommentsDisplay(handles)
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    nComments=length(meas.comments);
    nout=0;
    if nComments>0
        for n=1:nComments
             comment=meas.comments{n};
             nLines=size(comment,1);
             if nLines>1
                for j=1:nLines
                    nout=nout+1;           
                    messageout(nout,1)={[comment{j}]};
                end
             else
                 nout=nout+1;           
                 messageout(nout,1)={[comment{:}]};
             end
        end
    else
        messageout='No Comments';
    end

    set(handles.txtComments,'String',messageout);
    drawnow

% --- Executes on button press in pbCloseSystemTest.
function pbCloseSystemTest_Callback(hObject, eventdata, handles)

delete (handles.output);

function pbComment_Callback(hObject, eventdata, handles)
    % Add comment
    [handles]=commentButton(handles, 'Comments');
    handles=updateCommentsDisplay(handles)
    % Update handles structure
    guidata(hObject, handles);
    
function txtComments_Callback(hObject, eventdata, handles)
% hObject    handle to txtComments (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtComments as text
%        str2double(get(hObject,'String')) returns contents of txtComments as a double


% --- Executes during object creation, after setting all properties.
function txtComments_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtComments (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbHelp.
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%open('helpFiles\QRev_Users_Manual.pdf')  
web('QRev_Help_Files\HTML\view_comments.htm')


% --- Executes on button press in pbCopy.
function pbCopy_Callback(hObject, eventdata, handles)
% hObject    handle to pbCopy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
str=[];
newline=sprintf('\n');
txt=get(handles.txtComments,'String');
nlines=size(txt,1);
for n=1:nlines
    row = sprintf('%s\t', txt{n,:});
    row(end) = newline;
    str = [str row]; %#ok<AGROW>
 end
 clipboard('copy',str);