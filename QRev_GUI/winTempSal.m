function varargout = winTempSal(varargin)
% WINTEMPSAL MATLAB code for winTempSal.fig
%      WINTEMPSAL, by itself, creates a new WINTEMPSAL or raises the existing
%      singleton*.
%
%      H = WINTEMPSAL returns the handle to a new WINTEMPSAL or the handle to
%      the existing singleton*.
%
%      WINTEMPSAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINTEMPSAL.M with the given input arguments.
%
%      WINTEMPSAL('Property','Value',...) creates a new WINTEMPSAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winTempSal_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winTempSal_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winTempSal

% Last Modified by GUIDE v2.5 21-Jul-2016 15:24:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winTempSal_OpeningFcn, ...
                   'gui_OutputFcn',  @winTempSal_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before winTempSal is made visible.
function winTempSal_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winTempSal (see VARARGIN)

% Choose default command line output for winTempSal
handles.output = hObject;

    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end
    setWindowPosition(hObject,handles);
    
    set(gcf,'CloseRequestFcn',{@optionalClose,handles});

    
    commentIcon(handles.pbComment);
    
    handles.tempUnits='C';
    meas=getappdata(handles.hMainGui,'measurement');
    handles.oldQ=meas.discharge;
    checkedIdx=find([meas.transects.checked]==1); 
    handles.checkedIdx=checkedIdx;
    
    % Call function to process and plot temperature time series
    timeSeriesData(handles)

    % Get table from gui
    tblSOSData=get(handles.tblSOS,'Data');
    tblSOSData=tblSOSData(1,:); 
    set(handles.tblSOS,'Data',tblSOSData);
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Set columns
    handles.colFilename=1;
    handles.colTempSource=2;
    handles.colTemp=3;
    handles.colSal=4;
    handles.colSOSSource=5;
    handles.colSOS=6;
    handles.colQ=7;
    handles.colQDiff=8;
    
    % Set units for column names
    colNames={'<html> <br />Filename<html>' 'Temperature|Source' 'Average | Temperature' 'Average | Salinity (ppt)' 'Speed of Sound | Source' 'Speed of | Sound ' 'Discharge' 'Discharge | % Change'};
    colNames{handles.colSOS}=[colNames{handles.colSOS},' ',uLabelV];
    colNames{handles.colQ}=[colNames{handles.colQ},'|',uLabelQ];
    set(handles.tblSOS,'ColumnName',colNames);
    
    % Setup table columns
    userColFormat={'char' {'Internal (ADCP)' 'Manual'}  'numeric' 'numeric' {'Internal (ADCP)' 'Manual' 'Computed'} 'numeric' 'numeric' 'numeric'};
    set(handles.tblSOS,'ColumnFormat',userColFormat);
    
       updateTable(handles)

    % Update handles structure
    guidata(hObject, handles);

function varargout = winTempSal_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function updateTable(handles)

    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);

    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    discharge=meas.discharge;
    
    % Get data for table from each transect
    nTransects=length(handles.checkedIdx);
    for n=1:nTransects
        
        % User Settings Table
        % ===================
        idx=find(meas.transects(handles.checkedIdx(n)).fileName=='\',1,'last');
        if isempty(idx)
            idx=1;
        end
        tblSOSData{n,handles.colFilename}=meas.transects(handles.checkedIdx(n)).fileName(idx:end);
        switch meas.transects(handles.checkedIdx(n)).sensors.temperature_degC.selected
            case 'internal'
               tblSOSData{n,handles.colTempSource}='Internal (ADCP)';
            case 'external'
               tblSOSData{n,handles.colTempSource}='External'; 
            case 'user'
                tblSOSData{n,handles.colTempSource}='User';
        end % switch 
        
        temp=nanmean(meas.transects(handles.checkedIdx(n)).sensors.temperature_degC.(meas.transects(handles.checkedIdx(n)).sensors.temperature_degC.selected).data);
        if get(handles.rbF,'Value')
            temp=temp.*(9/5)+32;
        end
                   
        tblSOSData{n,handles.colTemp}=sprintf('% 12.1f',temp);
        tblSOSData{n,handles.colSal}=sprintf('% 12.0f',nanmean(meas.transects(handles.checkedIdx(n)).sensors.salinity_ppt.(meas.transects(handles.checkedIdx(n)).sensors.salinity_ppt.selected).data));
         switch meas.transects(handles.checkedIdx(n)).sensors.speedOfSound_mps.selected
            case 'internal'
                if strcmp(meas.transects(handles.checkedIdx(n)).sensors.speedOfSound_mps.(meas.transects(handles.checkedIdx(n)).sensors.speedOfSound_mps.selected).source,'Computed')...
                    tblSOSData{n,handles.colSOSSource}='Computed';
                else
                    tblSOSData{n,handles.colSOSSource}='Internal (ADCP)';
                end % if

            case 'external'
               tblSOSData{n,handles.colSOSSource}='External'; 
               
            case 'user'
                tblSOSData{n,handles.colSOSSource}='Manual';
        end % switch
        tblSOSData{n,handles.colSOS}=sprintf('% 12.1f',nanmean(meas.transects(handles.checkedIdx(n)).sensors.speedOfSound_mps.(meas.transects(handles.checkedIdx(n)).sensors.speedOfSound_mps.selected).data.*unitsV));
        tblSOSData{n,handles.colQ}=sprintf('% 12.2f',discharge(handles.checkedIdx(n)).total.*unitsQ);    
        dischargeChng=((discharge(handles.checkedIdx(n)).total-handles.oldQ(handles.checkedIdx(n)).total)./handles.oldQ(handles.checkedIdx(n)).total).*100;
        tblSOSData{n,handles.colQDiff}=sprintf('% 8.1f',dischargeChng);
        
    end % for n

      set(handles.tblSOS,'Data',tblSOSData);
    % Update tables
%     handles=applyFormat(handles,tblSOSData,meas);
    
function optionalClose(hObject, eventdata, handles)
    pbCloseTemp_Callback(hObject, eventdata, handles);

function pbCloseTemp_Callback(hObject, eventdata, handles)
% hObject    handle to pbCloseTemp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    meas.qa=temperatureQA(meas.qa,meas);
    
    % Store measurement data
    setappdata(handles.hMainGui,'measurement',meas);
    
    delete (handles.output);

function rbF_Callback(hObject, eventdata, handles)
% Set temperature display units to degrees F

    % Clear C radio button
    set(handles.rbC,'Value',0);

    % Process and plot temperature time series
    timeSeriesData(handles);
    handles=updateTableTempUnits(handles);
    handles.tempUnits='F';
    
    % Update handles structure
    guidata(hObject, handles);

function rbC_Callback(hObject, eventdata, handles)
% Set temperature display units to degrees C

    % Clear F radio button
    set(handles.rbF,'Value',0);

    % Process and plot temperature time series 
    timeSeriesData(handles);
    handles=updateTableTempUnits(handles);
    handles.tempUnits='C';
    
    % Update handles structure
    guidata(hObject, handles);
    
function handles=updateTableTempUnits(handles)
    meas=getappdata(handles.hMainGui,'measurement'); 
    tblSOSData=get(handles.tblSOS,'Data');
    nStart=1;
    nEnd=length(handles.checkedIdx);
    % Loop through appropriate transects
    for n=nStart:nEnd
        % Compute mean temperature
        temp=nanmean(meas.transects(handles.checkedIdx(n)).sensors.temperature_degC.(meas.transects(handles.checkedIdx(n)).sensors.temperature_degC.selected).data);
        % Convert temperature if necessary and update table data
        if get(handles.rbC,'Value')
            tblSOSData{n,handles.colTemp}=sprintf('% 12.1f',temp);
        else
            tblSOSData{n,handles.colTemp}=sprintf('% 12.1f',((9./5).*temp)+32);
        end
    end % for n
    set(handles.tblSOS,'Data',tblSOSData);

function edTempUser_Callback(hObject, eventdata, handles)
% User input of independent temperature reading

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Assign new user temperature
    temp=str2double(get(handles.edTempUser,'String'));
    if isnan(temp);
        meas.extTempChk.user=[];
    else
        % Convert temperature to F as needed
        if (get(handles.rbF,'Value') == get(handles.rbF,'Max'))
            temp=convertTemp(temp,'F','C');
        end
        meas.extTempChk.user=temp;
    end
    
    % Complete quality check using new temperature input
    meas.qa=temperatureQA(meas.qa,meas);
    
    % Store measurement data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Complete QA check on external temperature check
    manualQACheck(handles);

    % Update handles structure
    guidata(hObject, handles);
    
function edTempADCP_Callback(hObject, eventdata, handles)
% User input of ADCP temperature

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Assing new manually entered ADCP temperature
        temp=str2double(get(handles.edTempADCP,'String'));
    if isnan(temp);
        meas.extTempChk.adcp=[];
    else
        % Convert temperature to F as needed
        if (get(handles.rbF,'Value') == get(handles.rbF,'Max'))
            temp=convertTemp(temp,'F','C');
        end        
        meas.extTempChk.adcp=temp;
    end
    
    % Complete quality check using new temperature input
    meas.qa=temperatureQA(meas.qa,meas);
   
    % Store measurement data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Complete QA check on external temperature check
    manualQACheck(handles);
    
    % Update handles structure
    guidata(hObject, handles);

function pbComment_Callback(hObject, eventdata, handles)
% Add comment
    [handles]=commentButton(handles, 'Temperature Check');
    % Update handles structure
    guidata(hObject, handles);

function temperature=convertTemp(tempIn,unitsIn,unitsOut)
% Convert temperature units
    % Temperature input in degrees F
    if strcmpi(unitsIn,'F')
        % Temperature output in degrees C
        if strcmpi(unitsOut,'C')
            temperature=(tempIn-32).*(5./9);
        else
            temperature=tempIn;
        end
    end
    
    % Temperature input in degrees C
    if strcmpi(unitsIn,'C')
        % Temperature output in degrees F
        if strcmpi(unitsOut,'F')
            temperature=(tempIn.*(9./5))+32;
        else
            temperature=tempIn;
        end
    end
  
function timeSeriesData(handles)
% Processes and plots the temperature time series data. Includes checks for
% the time series and external temperature check to update the messages.

    % Temperature threshold to trigger warning in C
    tempThreshold=1;
    
    % Get axes handle and clear axes
    h=handles.axTempTimeSeries;
    cla(h);

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get display units
    if get(handles.rbC,'Value')==get(handles.rbC,'Max')
        displayUnits='C';
    end
    if get(handles.rbF,'Value')==get(handles.rbF,'Max')
        displayUnits='F';
    end
    
    % Set GUI display of independent temperature reading
    temp=convertTemp(meas.extTempChk.user,'C',displayUnits);
    temp=num2str(temp,'%4.1f');
    if isnan(temp)
        set(handles.edTempUser,'String','');
    else
        set(handles.edTempUser,'String',temp);
    end
    
    % Set GUI display of ADCP temperature reading
    temp=convertTemp(meas.extTempChk.adcp,'C',displayUnits);
    temp=num2str(temp,'%4.1f');
    if length(temp)==0
        set(handles.edTempADCP,'String','');
    else
        set(handles.edTempADCP,'String',temp); 
    end
    
    % Process each transect to obtain a continuous time series
    transects=meas.transects;
    nTransects=length(handles.checkedIdx);
    [temp,serialTime]=computeTimeSeries(transects(handles.checkedIdx));
    
    % Remove leading nan
    temp=temp(2:end);
    serialTime=serialTime(2:end);
    
    % Convert temperature to F as needed
    if strcmpi(displayUnits,'F');
        temp=convertTemp(temp,'C','F');
        tempThreshold=tempThreshold.*(9./5);
    end
    
    % Plot time series
    plot(h,serialTime,temp,'.b')
    xmax=nanmax(serialTime);
    xmin=nanmin(serialTime);
    set(h,'XTick',linspace(xmin,xmax,6))
    datetick(h,'x','HHMM','keepticks')
%     ylim(h,[nanmin(temp)-2,prctile(temp,95)+2])
    ylim(h,[nanmin(temp)-2,nanmax(temp)+2])
    
    
    if strcmpi(displayUnits,'F');
        ylabel(h,'Degrees F')
    else
        ylabel(h,'Degrees C')
    end
    xlabel (h,'Time (hhmm)')
    grid on
    drawnow
    
    manualQACheck (handles)
       
function manualQACheck (handles)
% Performs a QA check on the external temperature data
    
    % Temperature threshold in C
    tempThreshold=2;
    
    % Get user and ADCP values from measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get display units
    if get(handles.rbC,'Value')==get(handles.rbC,'Max')
        displayUnits='C';
    end
    if get(handles.rbF,'Value')==get(handles.rbF,'Max')
        displayUnits='F';
    end
    
    % Check user supplied independent temperature
    user=meas.extTempChk.user;
    if ~isempty(user)

        % Compare independent and ADCP temperature
        adcp=meas.extTempChk.adcp;
        if isempty(adcp)
            % Use mean temperature from measurement if no user supplied
            % ADCP temperature
            [temp,serialTime]=computeTimeSeries(meas.transects(handles.checkedIdx));
            adcp=nanmean(temp);
        end

        % Compute range and display appropriate message
        range=abs(user-adcp);
        if strcmp(displayUnits,'F')
                tempThreshold=tempThreshold.*(9./5);
                range=range.*(9./5);
        end
%         if range < tempThreshold
%             message=['GOOD: External temperature check difference is ',num2str(range,'%4.1f'),' deg ',displayUnits];
%             set(handles.txtMsgTemp2,'String',message);
%         else
%             message=['WARNING: External temperature check difference is ',num2str(range,'%4.1f'),' deg ',displayUnits];
%             set(handles.txtMsgTemp2,'String',message);
%         end
    end
    
function [temp,serialTime]=computeTimeSeries(transects)
% Prepare temperature time series
    % Initialize variables
    temp=nan;
    serialTime=nan;
    nTransects=length(transects);
    
    % Loop through each transect
    for n=1:nTransects   
        % Check for situation where user has entered a constant temperature
        if length(transects(n).sensors.temperature_degC.(transects(n).sensors.temperature_degC.selected).data)>1
            % Temperatures for ADCP
            temp=[temp, transects(n).sensors.temperature_degC.(transects(n).sensors.temperature_degC.selected).data];
        else
            % User specified constant temperature
            temp=[temp, repmat(transects(n).sensors.temperature_degC.(transects(n).sensors.temperature_degC.selected).data,size(transects(n).sensors.temperature_degC.internal.data))];
        end
        serialTime=[serialTime, transects(n).dateTime.startSerialTime+nancumsum(transects(n).dateTime.ensDuration_sec)./(60*60*24)];
    end
    
function tblSOS_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to tblSOS (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)    
    col=eventdata.Indices(:,2);
    row=eventdata.Indices(:,1);
    meas=getappdata(handles.hMainGui,'measurement');
    handles.oldQ=meas.discharge;
    if ~isempty(col)
        handles.selected=handles.checkedIdx(row);
        tableData=get(handles.tblSOS,'Data');
        switch col
            case 2
                % Open winTempSource
                handles.hWinTempSource=winTempSource('hMainGui',handles.hMainGui,handles.selected,handles.tempUnits);
                set(handles.hWinTempSource,'WindowStyle','modal');
                % Wait for figure to close before proceeding
                waitfor(handles.hWinTempSource);

                % Update GUI & Data
                updateTable(handles);

            case 3
                % Only active if temperature source is user
                if strcmpi(tableData{row,handles.colTempSource},'User')
                    % Open winTempSource
                    handles.hWinTempSource=winTempSource('hMainGui',handles.hMainGui,handles.selected,handles.tempUnits);
                    set(handles.hWinTempSource,'WindowStyle','modal');
                    % Wait for figure to close before proceeding
                    waitfor(handles.hWinTempSource);

                    % Update GUI & Data
                    updateTable(handles);
                end

            case 4
                    % Open winSal
                    handles.hWinSal=winSal('hMainGui',handles.hMainGui,handles.selected);
                    set(handles.hWinSal,'WindowStyle','modal');
                    % Wait for figure to close before proceeding
                    waitfor(handles.hWinSal);

                    % Update GUI & Data
                    updateTable(handles);
                    
            case 5
                % Open winSOSSource
                    % Get units multiplier
                handles.hWinSOSSource=winSOSSource('hMainGui',handles.hMainGui,handles.selected);
                set(handles.hWinSOSSource,'WindowStyle','modal');
                % Wait for figure to close before proceeding
                waitfor(handles.hWinSOSSource);

                % Update GUI & Data
                updateTable(handles);
                
            case 6
                % Only active is SOS Source is User
                if strcmpi(tableData{row,handles.colSOSSource},'User')
                    % Open winSOS
                    handles.hWinSOS=winSOS('hMainGui',handles.hMainGui,handles.selected);
                    set(handles.hWinSOS,'WindowStyle','modal');
                    % Wait for figure to close before proceeding
                    waitfor(handles.hWinSOS);

                    % Update GUI & Data
                    updateTable(handles);
                    
                end % if user
        end
    end

function pbHelp_Callback(hObject, eventdata, handles)
% Open help
    %open('helpFiles\QRev_Users_Manual.pdf') 
    web('QRev_Help_Files\HTML\temp_salinity.htm')

%==========================================================================
%================NO MODIFIED CODE BELOW HERE===============================
%==========================================================================

function edTempADCP_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edTempADCP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edTempUser_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edTempUser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



