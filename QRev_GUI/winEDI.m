
% Equal Discharge Increment (winEDI)
%
% This program allows data collected with an ADCP to be used to compute the
% distance from starting bank to aid in collecting equal discharge
% increment sediment and water quality samples. Optionally, if GPS is
% collected with the ADCP, the program will create a file of latitudes and
% longitudes for each measurement location that is compatible with DeLorme
% TopoQuads.
% 
% Input for the program is an TRDI compatible Classic ASCII output file.
%
% The program allows the user to change the default increments of 10, 30,
% 50, 70, and 90 percent of the discharge. The location of the sample is
% computed by finding the 1st ensemble that results in a cumulative
% discharge that exceeds the target discharge of PercentQ*TotalQ. The
% distance from the starting bank is computed as the Distance Made Good to
% appropriate ensemble plus the starting edge distance and a user provided
% zero distance offset. The depth is the simple mean of the beam depths for
% the appropriate ensemble. The velocity is the magnitude of the mean 
% velocity of the measured portion of the velocity profile. No accounting 
% for the unmeasured portion of the velocity profile is provided.
%
% Program by
%
% David S. Mueller
% U.S. Geological Survey
% Office of Surface Water
%
% Version 1.0
% 10/26/07
%
% Version 2.0
% Reformatted source code 9/23/2008
% Added version number to GUI
% Added units display in GUI
% Added reading file display in GUI
%
% Version 2.1
% 9/24/2008
% Improved user interface when changing default percentages
%
% Version 2.2
% 12/9/2008
% Changed code to work with negative discharges.
%
% Version 3.0
% 1/27/2011 dsm
%
% 1. Added support for RiverSurveyor Live 1.5 or later
% 2. Added latitude and longitude output to GUI
% 3. Moved some code from pbCompute to pbOpen
% 4. Increase output resolution for SI units
%
% Version 3.1
% 8/29/2011 dsm
% 
% 1. Fixed bug in start distance computation for RiverSurveyor Live using
% ft as the units.
%
% Version 3.11
% 6/28/2012 dsm
%
% 1. Change version to 3.11 for compile under 2012a.
%
% 1/10/2017
% DSM
% Added to QRev, modified code to average depth and velocity +/1% of
% ensembles either side of selected vertical.


function varargout = winEDI(varargin)
% winedi M-file for winEDI.fig
%      winedi, by itself, creates a new winedi or raises the existing
%      singleton*.
%
%      H = winedi returns the handle to a new winedi or the handle to
%      the existing singleton*.
%
%      winedi('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in winedi.M with the given input arguments.
%
%      winedi('Property','Value',...) creates a new winedi or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_edi_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winEDI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% edit the above text to modify the response to help winEDI

% Last Modified by GUIDE v2.5 06-Jan-2017 15:07:40

% Begin initialization code - DO NOT ediT
warning off MATLAB:dispatcher:InexactMatch
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winEDI_OpeningFcn, ...
                   'gui_OutputFcn',  @winEDI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT ediT

function winEDI_OpeningFcn(hObject, eventdata, handles, varargin)
% --- Executes just before winEDI is made visible.
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winEDI (see VARARGIN)

% Choose default command line output for winEDI
handles.output = hObject;


    transect=varargin{1};
    discharge=varargin{2};
    handles.hMainGui=varargin{3};
    

    
    setWindowPosition(hObject,handles);
    set(handles.TQ1,'String','');
    set(handles.AQ1,'String','');
    set(handles.Dist1,'String','');
    set(handles.Depth1,'String','');
    set(handles.Vel1,'String','');
    set(handles.TQ2,'String','');
    set(handles.AQ2,'String','');
    set(handles.Dist2,'String','');
    set(handles.Depth2,'String','');
    set(handles.Vel2,'String','');
    set(handles.TQ3,'String','');
    set(handles.AQ3,'String','');
    set(handles.Dist3,'String','');
    set(handles.Depth3,'String','');
    set(handles.Vel3,'String','');
    set(handles.TQ4,'String','');
    set(handles.AQ4,'String','');
    set(handles.Dist4,'String','');
    set(handles.Depth4,'String','');
    set(handles.Vel4,'String','');
    set(handles.TQ5,'String','');
    set(handles.AQ5,'String','');
    set(handles.Dist5,'String','');
    set(handles.Depth5,'String','');
    set(handles.Vel5,'String','');
    set(handles.Lat1,'String','');
    set(handles.Lat2,'String','');
    set(handles.Lat3,'String','');
    set(handles.Lat4,'String','');
    set(handles.Lat5,'String','');
    set(handles.Lon1,'String','');
    set(handles.Lon2,'String','');
    set(handles.Lon3,'String','');
    set(handles.Lon4,'String','');
    set(handles.Lon5,'String','');
    set(handles.CBtq,'Enable','off');
    set(handles.CBtq,'Value',0);
    drawnow;

    
    % Update display with filename
    set(handles.textFileName,'String',transect.fileName);
    drawnow

    % Begin processing transect
    numEns=size(transect.wVel.uProcessed_mps,2);

    % Process GPS data if available
    if ~isempty(transect.gps)
        x=find(~isnan(transect.gps.ggaLatEns_deg));
        xi=1:numEns;
        handles.Lat=interp1q(x',transect.gps.ggaLatEns_deg(x)',xi');
        handles.Long=interp1q(x',transect.gps.ggaLonEns_deg(x)',xi');
        set(handles.CBtq,'Enable','on')
    else
        handles.Lat=nan(1,numEns);
        handles.Long=nan(1,numEns);
        set(handles.CBtq,'Enable','off')
        set(handles.CBtq,'Value',0)
    end

    % Determine start and end discharges and distances
    if strcmp(transect.startEdge,'Right')
        QStart=discharge.right;
        QEnd=discharge.left;
        startDist=transect.edges.right.dist_m;
    else
        QStart=discharge.left;
        QEnd=discharge.right;
        startDist=transect.edges.left.dist_m;
    end

    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    % Prepare Q
    handles.Q=nancumsum(discharge.middleEns+discharge.topEns+discharge.bottomEns);
    handles.Q=handles.Q .* discharge.correctionFactor
    handles.Q=handles.Q+QStart;
    handles.Q(1,end)=handles.Q(1,end)+QEnd;
    handles.Q=handles.Q'.*unitsQ;

    % Prepare Velocities
    handles.Veast=transect.wVel.uProcessed_mps.*unitsL;
    handles.Vnorth=transect.wVel.vProcessed_mps.*unitsL;

    handles.PathName=getUserPref('Folder');
    handles.Depth=transect.depths.(transect.depths.selected).depthProcessed_m'.*unitsL;

    % Prepare Distance
    ensDuration=transect.dateTime.ensDuration_sec;
    selected=transect.boatVel.selected;
    boatUProcessed=transect.boatVel.(selected).uProcessed_mps;
    boatVProcessed=transect.boatVel.(selected).vProcessed_mps;
    boatXProcessed=nancumsum(boatUProcessed.*ensDuration);
    boatYProcessed=nancumsum(boatVProcessed.*ensDuration);
    DMG=sqrt(boatXProcessed.^2+boatYProcessed.^2)';
    handles.Distance=(DMG+startDist).*unitsL;
 
    set(handles.pbCompute,'Enable','on');
    if unitsL==1
        handles.units='m';
        set(handles.TQtext,'String','(cms)');
        set(handles.AQtext,'String','(cms)');
        set(handles.Disttext,'String','(m)');
        set(handles.Depthtext,'String','(m)');  
        set(handles.Veltext,'String','(m/s)');    
    else
        handles.units='f';
        set(handles.TQtext,'String','(cfs)');
        set(handles.AQtext,'String','(cfs)');
        set(handles.Disttext,'String','(ft)');
        set(handles.Depthtext,'String','(ft)');  
        set(handles.Veltext,'String','(ft/s)');
    end

% Update handles structure
guidata(hObject, handles);

function pbCompute_Callback(hObject, eventdata, handles)
% --- Executes on button press in pbCompute.
% hObject    handle to pbCompute (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


Q=handles.Q;
Lat=handles.Lat;
Long=handles.Long;
Distance=handles.Distance;
Veast=handles.Veast;
Vnorth=handles.Vnorth;
PathName=handles.PathName;
Depth=handles.Depth';
if size(Depth,1)>1
    Depth=nanmean(Depth,1);
end
nPts2Avg=floor(length(Q).*0.01);
%
%  Open and prepare TopoQuad file if requested
%  -------------------------------------------
if get(handles.CBtq,'Value')
    answer=inputdlg({'Enter filename (no suffix, i.e. .???)for TopoQuad file:'},'TopoQuad File',1,{'tqlatlon'},'on');
    if size(answer{1})>0
        outfile=[PathName,char(answer),'.txt'];
        handles.tqFile=fopen(outfile, 'wt');
        outcount=fprintf(handles.tqFile,'BEGIN SYMBOL\n');
    else
        set(handles.CBtq,'Value',0);
        errordlg('Output File Not Defined. TopoQuad Output Turned OFF','File Error');
    end
end

%
% Compute Indices for user specified increments
% ---------------------------------------------
QIncrement(1)=Q(size(Q,1)).*str2double(get(handles.PQ1,'String'))./100;
QIncrement(2)=Q(size(Q,1)).*str2double(get(handles.PQ2,'String'))./100;
QIncrement(3)=Q(size(Q,1)).*str2double(get(handles.PQ3,'String'))./100;
QIncrement(4)=Q(size(Q,1)).*str2double(get(handles.PQ4,'String'))./100;
QIncrement(5)=Q(size(Q,1)).*str2double(get(handles.PQ5,'String'))./100;
distOffset=str2double(get(handles.distOffset,'String'));
for i=1:5
    if QIncrement(i)>0
        QIndex(i)=find(Q>QIncrement(i),1);
    else
        QIndex(i)=find(Q<QIncrement(i),1);
    end
    meanVeast=nanmean(Veast(:,QIndex(i)-nPts2Avg:QIndex(i)+nPts2Avg),2);
    meanVnorth=nanmean(Vnorth(:,QIndex(i)-nPts2Avg:QIndex(i)+nPts2Avg),2);
    MeanV(i)=(nanmean(meanVeast).^2+nanmean(meanVnorth).^2).^0.5;
    %
    % Use GPS, if available, to create TopoQuad output
    % ------------------------------------------------
    ediLat(i)=Lat(QIndex(i));
    LatD(i)=fix(ediLat(i));
    LatM(i)=abs((ediLat(i)-LatD(i)).*60);
    ediLon(i)=Long(QIndex(i));
    LonD(i)=fix(ediLon(i));
    LonM(i)=abs((ediLon(i)-LonD(i)).*60);
    if get(handles.CBtq,'Value')
        fprintf(handles.tqFile,'%15.10f, %15.10f, Yellow Dot\n',ediLat(i),ediLon(i));
    end
        
    %
    % Update GUI with results
    % -----------------------
    if strcmp(handles.units,'f')
        if i==1 
            set(handles.TQ1,'String',sprintf('%10.1f   ',QIncrement(i)));
            set(handles.AQ1,'String',sprintf('%10.1f   ',Q(QIndex(i))));
            set(handles.Dist1,'String',sprintf('%7.1f   ',distOffset+Distance(QIndex(i))));
            set(handles.Depth1,'String',sprintf('%7.1f    ',nanmean(Depth(:,QIndex(i)-nPts2Avg:QIndex(i)+nPts2Avg))));
            set(handles.Vel1,'String',sprintf('%5.1f     ',MeanV(i)));
            if strcmp(get(handles.CBtq,'Enable'),'on')
                set(handles.Lat1,'String',sprintf('%4.0f %7.4f    ',LatD(i),LatM(i)));
                set(handles.Lon1,'String',sprintf('%4.0f %7.4f    ',LonD(i),LonM(i)));
            end
        elseif i==2 
            set(handles.TQ2,'String',sprintf('%10.1f   ',QIncrement(i)));
            set(handles.AQ2,'String',sprintf('%10.1f   ',Q(QIndex(i))));
            set(handles.Dist2,'String',sprintf('%7.1f   ',distOffset+Distance(QIndex(i))));
            set(handles.Depth2,'String',sprintf('%7.1f    ',nanmean(Depth(:,QIndex(i)-nPts2Avg:QIndex(i)+nPts2Avg))));
            set(handles.Vel2,'String',sprintf('%5.1f     ',MeanV(i)));
            if strcmp(get(handles.CBtq,'Enable'),'on')
                set(handles.Lat2,'String',sprintf('%4.0f %7.4f    ',LatD(i),LatM(i)));
                set(handles.Lon2,'String',sprintf('%4.0f %7.4f    ',LonD(i),LonM(i)));
            end
        elseif i==3
            set(handles.TQ3,'String',sprintf('%10.1f   ',QIncrement(i)));
            set(handles.AQ3,'String',sprintf('%10.1f   ',Q(QIndex(i))));
            set(handles.Dist3,'String',sprintf('%7.1f   ',distOffset+Distance(QIndex(i))));
            set(handles.Depth3,'String',sprintf('%7.1f    ',nanmean(Depth(:,QIndex(i)-nPts2Avg:QIndex(i)+nPts2Avg))));
            set(handles.Vel3,'String',sprintf('%5.1f     ',MeanV(i)));
            if strcmp(get(handles.CBtq,'Enable'),'on')
                set(handles.Lat3,'String',sprintf('%4.0f %7.4f    ',LatD(i),LatM(i)));
                set(handles.Lon3,'String',sprintf('%4.0f %7.4f    ',LonD(i),LonM(i)));
            end            
        elseif i==4
            set(handles.TQ4,'String',sprintf('%10.1f   ',QIncrement(i)));
            set(handles.AQ4,'String',sprintf('%10.1f   ',Q(QIndex(i))));
            set(handles.Dist4,'String',sprintf('%7.1f   ',distOffset+Distance(QIndex(i))));
            set(handles.Depth4,'String',sprintf('%7.1f    ',nanmean(Depth(:,QIndex(i)-nPts2Avg:QIndex(i)+nPts2Avg))));
            set(handles.Vel4,'String',sprintf('%5.1f     ',MeanV(i)));
            if strcmp(get(handles.CBtq,'Enable'),'on')
                set(handles.Lat4,'String',sprintf('%4.0f %7.4f    ',LatD(i),LatM(i)));
                set(handles.Lon4,'String',sprintf('%4.0f %7.4f    ',LonD(i),LonM(i)));
            end            
        elseif i==5
            set(handles.TQ5,'String',sprintf('%10.1f   ',QIncrement(i)));
            set(handles.AQ5,'String',sprintf('%10.1f   ',Q(QIndex(i))));
            set(handles.Dist5,'String',sprintf('%7.1f   ',distOffset+Distance(QIndex(i))));
            set(handles.Depth5,'String',sprintf('%7.1f    ',nanmean(Depth(:,QIndex(i)-nPts2Avg:QIndex(i)+nPts2Avg))));
            set(handles.Vel5,'String',sprintf('%5.1f     ',MeanV(i)));
            if strcmp(get(handles.CBtq,'Enable'),'on')
                set(handles.Lat5,'String',sprintf('%4.0f %7.4f    ',LatD(i),LatM(i)));
                set(handles.Lon5,'String',sprintf('%4.0f %7.4f    ',LonD(i),LonM(i)));
            end            
        end
    else
        if i==1 
            set(handles.TQ1,'String',sprintf('%10.2f   ',QIncrement(i)));
            set(handles.AQ1,'String',sprintf('%10.2f   ',Q(QIndex(i))));
            set(handles.Dist1,'String',sprintf('%7.2f   ',distOffset+Distance(QIndex(i))));
            set(handles.Depth1,'String',sprintf('%7.2f    ',nanmean(Depth(:,QIndex(i)-nPts2Avg:QIndex(i)+nPts2Avg))));
            set(handles.Vel1,'String',sprintf('%5.1f     ',MeanV(i)));
            if strcmp(get(handles.CBtq,'Enable'),'on')
                set(handles.Lat1,'String',sprintf('%4.0f %7.4f    ',LatD(i),LatM(i)));
                set(handles.Lon1,'String',sprintf('%4.0f %7.4f    ',LonD(i),LonM(i)));
            end            
        elseif i==2 
            set(handles.TQ2,'String',sprintf('%10.2f   ',QIncrement(i)));
            set(handles.AQ2,'String',sprintf('%10.2f   ',Q(QIndex(i))));
            set(handles.Dist2,'String',sprintf('%7.2f   ',distOffset+Distance(QIndex(i))));
            set(handles.Depth2,'String',sprintf('%7.2f    ',nanmean(Depth(:,QIndex(i)-nPts2Avg:QIndex(i)+nPts2Avg))));
            set(handles.Vel2,'String',sprintf('%5.1f     ',MeanV(i)));
            if strcmp(get(handles.CBtq,'Enable'),'on')
                set(handles.Lat2,'String',sprintf('%4.0f %7.4f    ',LatD(i),LatM(i)));
                set(handles.Lon2,'String',sprintf('%4.0f %7.4f    ',LonD(i),LonM(i)));
            end            
        elseif i==3
            set(handles.TQ3,'String',sprintf('%10.2f   ',QIncrement(i)));
            set(handles.AQ3,'String',sprintf('%10.2f   ',Q(QIndex(i))));
            set(handles.Dist3,'String',sprintf('%7.2f   ',distOffset+Distance(QIndex(i))));
            set(handles.Depth3,'String',sprintf('%7.2f    ',nanmean(Depth(:,QIndex(i)-nPts2Avg:QIndex(i)+nPts2Avg))));
            set(handles.Vel3,'String',sprintf('%5.1f     ',MeanV(i)));
            if strcmp(get(handles.CBtq,'Enable'),'on')
                set(handles.Lat3,'String',sprintf('%4.0f %7.4f    ',LatD(i),LatM(i)));
                set(handles.Lon3,'String',sprintf('%4.0f %7.4f    ',LonD(i),LonM(i)));
            end            
        elseif i==4
            set(handles.TQ4,'String',sprintf('%10.2f   ',QIncrement(i)));
            set(handles.AQ4,'String',sprintf('%10.2f   ',Q(QIndex(i))));
            set(handles.Dist4,'String',sprintf('%7.2f   ',distOffset+Distance(QIndex(i))));
            set(handles.Depth4,'String',sprintf('%7.2f    ',nanmean(Depth(:,QIndex(i)-nPts2Avg:QIndex(i)+nPts2Avg))));
            set(handles.Vel4,'String',sprintf('%5.1f     ',MeanV(i)));
            if strcmp(get(handles.CBtq,'Enable'),'on')
                set(handles.Lat4,'String',sprintf('%4.0f %7.4f    ',LatD(i),LatM(i)));
                set(handles.Lon4,'String',sprintf('%4.0f %7.4f    ',LonD(i),LonM(i)));
            end            
        elseif i==5
            set(handles.TQ5,'String',sprintf('%10.2f   ',QIncrement(i)));
            set(handles.AQ5,'String',sprintf('%10.2f   ',Q(QIndex(i))));
            set(handles.Dist5,'String',sprintf('%7.2f   ',distOffset+Distance(QIndex(i))));
            set(handles.Depth5,'String',sprintf('%7.2f    ',nanmean(Depth(:,QIndex(i)-nPts2Avg:QIndex(i)+nPts2Avg))));
            set(handles.Vel5,'String',sprintf('%5.1f     ',MeanV(i)));
            if strcmp(get(handles.CBtq,'Enable'),'on')
                set(handles.Lat5,'String',sprintf('%4.0f %7.4f    ',LatD(i),LatM(i)));
                set(handles.Lon5,'String',sprintf('%4.0f %7.4f    ',LonD(i),LonM(i)));
            end            
        end
    end
end
if get(handles.CBtq,'Value')
    fprintf(handles.tqFile,'END\n');
    fclose(handles.tqFile);
end
drawnow
% Update handles structure
guidata(hObject, handles);

function PQ1_Callback(hObject, eventdata, handles)
% hObject    handle to PQ1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of PQ1 as text
%        str2double(get(hObject,'String')) returns contents of PQ1 as a double
set(handles.pbCompute,'Enable','on');
set(handles.TQ1,'String','');
set(handles.AQ1,'String','');
set(handles.Dist1,'String','');
set(handles.Depth1,'String','');
set(handles.Vel1,'String','');

function PQ2_Callback(hObject, eventdata, handles)
% hObject    handle to PQ2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of PQ2 as text
%        str2double(get(hObject,'String')) returns contents of PQ2 as a
%        double
set(handles.pbCompute,'Enable','on');
set(handles.TQ2,'String','');
set(handles.AQ2,'String','');
set(handles.Dist2,'String','');
set(handles.Depth2,'String','');
set(handles.Vel2,'String','');

function PQ3_Callback(hObject, eventdata, handles)
% hObject    handle to PQ2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of PQ2 as text
%        str2double(get(hObject,'String')) returns contents of PQ2 as a
%        double
set(handles.pbCompute,'Enable','on');
set(handles.TQ3,'String','');
set(handles.AQ3,'String','');
set(handles.Dist3,'String','');
set(handles.Depth3,'String','');
set(handles.Vel3,'String','');

function PQ4_Callback(hObject, eventdata, handles)
% hObject    handle to PQ2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of PQ2 as text
%        str2double(get(hObject,'String')) returns contents of PQ2 as a
%        double
set(handles.pbCompute,'Enable','on');
set(handles.TQ4,'String','');
set(handles.AQ4,'String','');
set(handles.Dist4,'String','');
set(handles.Depth4,'String','');
set(handles.Vel4,'String','');

function PQ5_Callback(hObject, eventdata, handles)
% hObject    handle to PQ2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of PQ2 as text
%        str2double(get(hObject,'String')) returns contents of PQ2 as a
%        double
set(handles.pbCompute,'Enable','on');
set(handles.TQ5,'String','');
set(handles.AQ5,'String','');
set(handles.Dist5,'String','');
set(handles.Depth5,'String','');
set(handles.Vel5,'String','');

% --- Executes on button press in pbClose.
function pbClose_Callback(hObject, eventdata, handles)
% hObject    handle to pbClose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(handles.output)




%==========================================================================
%===========Standard GUI Files Below -- No Custom Code=====================
%==========================================================================


function varargout = winEDI_OutputFcn(hObject, eventdata, handles)
% --- Outputs from this function are returned to the command line.
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Get default command line output from handles structure
varargout{1} = handles.output;


function PQ1_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to PQ1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


function PQ5_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to PQ5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


function PQ4_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to PQ4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function PQ3_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to PQ3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function PQ2_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to PQ2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function TQ1_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to TQ1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function TQ2_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to TQ2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function TQ3_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to TQ3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function TQ4_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to TQ4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function TQ5_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to TQ5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function AQ1_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to AQ1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function AQ2_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to AQ2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function AQ3_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to AQ3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function AQ4_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to AQ4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function AQ5_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to AQ5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Dist1_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Dist1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Dist2_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Dist2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Dist3_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Dist3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Dist4_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Dist4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Dist5_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Dist5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Depth1_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Depth1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Depth2_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Depth2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Depth3_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Depth3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Depth4_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Depth4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Depth5_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Depth5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Vel1_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Vel1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Vel2_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Vel2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Vel3_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Vel3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Vel4_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Vel4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function Vel5_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to Vel5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function distOffset_Callback(hObject, eventdata, handles)
% hObject    handle to distOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of distOffset as text
%        str2double(get(hObject,'String')) returns contents of distOffset as a double

function distOffset_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to distOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function CBtq_Callback(hObject, eventdata, handles)
% --- Executes on button press in CBtq.
% hObject    handle to CBtq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CBtq
% Update handles structure
guidata(hObject, handles);


% --- Executes when uipanel1 is resized.
function uipanel1_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to uipanel1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
