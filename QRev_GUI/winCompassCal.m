function varargout = winCompassCal(varargin)
% WINCOMPASSCAL MATLAB code for winCompassCal.fig
%      WINCOMPASSCAL, by itself, creates a new WINCOMPASSCAL or raises the existing
%      singleton*.
%
%      H = WINCOMPASSCAL returns the handle to a new WINCOMPASSCAL or the handle to
%      the existing singleton*.
%
%      WINCOMPASSCAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINCOMPASSCAL.M with the given input arguments.
%
%      WINCOMPASSCAL('Property','Value',...) creates a new WINCOMPASSCAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winCompassCal_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winCompassCal_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winCompassCal

% Last Modified by GUIDE v2.5 17-Nov-2016 08:21:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winCompassCal_OpeningFcn, ...
                   'gui_OutputFcn',  @winCompassCal_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before winCompassCal is made visible.
function winCompassCal_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winCompassCal (see VARARGIN)

    % Choose default command line output for winCompassCal
    handles.output = hObject;
    
    set (handles.txtMsgCompassCall,'String','OK');

    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end

    setWindowPosition(hObject,handles);
    
    commentIcon(handles.pbComment);
    
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get compass cal data from measurement
    checked=logical([meas.transects.checked]);
    idx=find(checked==true,1,'first');
    % If TRDI display evaluation if available
    if strcmp(meas.transects(idx).adcp.manufacturer,'TRDI')...
        && isfield(meas.compassEval(end).result, 'compass')...
        && ~strcmp(meas.compassEval(end).result.compass.error,'N/A')
            compassData=meas.compassEval;
            handles=updateGUI(handles,compassData,2);
    else
        compassData=meas.compassCal;
        handles=updateGUI(handles,compassData);
    end
    
    
    
    % Update handles structure
    guidata(hObject, handles);

% UIWAIT makes winCompassCal wait for user response (see UIRESUME)
% uiwait(handles.figCompCal);

function handles=updateGUI(handles,compassData,varargin)

    % If compass compass calibration data exist prepare dropdown list, if not
    % display message
    if ~isempty(compassData)
        nTests=size(compassData,2);

        for n=1:nTests
            calTSn=length(compassData(n).timeStamp);
            calTS(n,1:calTSn)=compassData(n).timeStamp;
        end
        if isempty(calTS)
            set (handles.puTimeStamp,'String',' ');
            set (handles.puTimeStamp,'Value',1);
            set (handles.txtCompCal,'String','No calibration or evaluation data loaded.');
        else
            set (handles.puTimeStamp,'String',calTS);
            set (handles.puTimeStamp,'Value',nTests);
            set (handles.txtCompCal,'String',compassData(nTests).data);
        end
    else
        set (handles.txtCompCal,'String','No compass calibration data loaded');
        set (handles.txtMsgCompassCall,'String','Compass calibration is critical for GPS and Loop applications');
    end
    if nargin>2
        set(handles.puDataType,'Value',varargin{1});
    end
    drawnow

% --- Outputs from this function are returned to the command line.
function varargout = winCompassCal_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pbCloseCompassCal.
function pbCloseCompassCal_Callback(hObject, eventdata, handles)
    delete(handles.output)
%     close (handles.output);

% --- Executes on selection change in puTimeStamp.
function puTimeStamp_Callback(hObject, eventdata, handles)
% Function displays selected data
    if get(handles.puDataType,'Value')==1
        meas=getappdata(handles.hMainGui,'measurement');
        compData=meas.compassCal;
    else
        meas=getappdata(handles.hMainGui,'measurement');
        compData=meas.compassEval;
    end
    puValue=get(handles.puTimeStamp,'Value');
    set (handles.txtCompCal,'String',compData(puValue).data);
    drawnow

function txtCompCal_Callback(hObject, eventdata, handles)
% hObject    handle to txtCompCal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtCompCal as text
%        str2double(get(hObject,'String')) returns contents of txtCompCal as a double


function puDataType_Callback(hObject, eventdata, handles)
% Sets the data type calibration or evaluation being displayed

    % Reset message window
    set (handles.txtMsgCompassCall,'String','OK');
    
    % Get selected data type from measurement
    if get(handles.puDataType,'Value')==1
        meas=getappdata(handles.hMainGui,'measurement');
        compData=meas.compassCal;
    else
        meas=getappdata(handles.hMainGui,'measurement');
        compData=meas.compassEval;
    end
    
    handles=updateGUI(handles,compData);
    drawnow

    % Update handles structure
    guidata(hObject, handles);

function pbCal_Callback(hObject, eventdata, handles)
% Function to read text file with compass calibration

    % User selects file
    prefPath=getUserPref('Folder');
    [fileName,pathName]=uigetfile('*.*','SELECT COMPASS CALIBRATION TEXT FILE',prefPath);
    
    if length(pathName)>1
        % Get measurement data
        meas=getappdata(handles.hMainGui,'measurement');

        % Add compass calibration to measurement data
        meas=addCompassCal(meas,[pathName,fileName]);

        % Save measurement
        setappdata(handles.hMainGui,'measurement',meas);

        % Update GUI
        compData=meas.compassCal;
        handles=updateGUI(handles,compData);
        set(handles.puDataType,'Value',1);
        guidata(hObject, handles);
    end

function pbEval_Callback(hObject, eventdata, handles)
% Function to read text file with compass evaluation

    % User selects file
    prefPath=getUserPref('Folder');
    [fileName,pathName]=uigetfile('*.*','SELECT COMPASS EVALUATION TEXT FILE',prefPath);
    
    if length(pathName)>1
        % Get measurement data
        meas=getappdata(handles.hMainGui,'measurement');

        % Add compass calibration to measurement data
        meas=addCompassEval(meas,[pathName,fileName]);

        % Save measurement
        setappdata(handles.hMainGui,'measurement',meas);

        % Update GUI
        compData=meas.compassEval;
        handles=updateGUI(handles,compData);
        set(handles.puDataType,'Value',2);
        guidata(hObject, handles);
    end
       
function pbComment_Callback(hObject, eventdata, handles)
% Add comment
    [handles]=commentButton(handles, 'Compass');
    % Update handles structure
    guidata(hObject, handles);    
%==========================================================================
%================NO MODIFIED CODE BELOW HERE===============================
%==========================================================================

function puDataType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to puDataType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function puTimeStamp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to puTimeStamp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function txtCompCal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtCompCal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbHelp.
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%open('helpFiles\QRev_Users_Manual.pdf')  
web('QRev_Help_Files\HTML\compass_calibration_evaluation.htm')
