function varargout = winSal(varargin)
% WINSAL MATLAB code for winSal.fig
%      WINSAL, by itself, creates a new WINSAL or raises the existing
%      singleton*.
%
%      H = WINSAL returns the handle to a new WINSAL or the handle to
%      the existing singleton*.
%
%      WINSAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINSAL.M with the given input arguments.
%
%      WINSAL('Property','Value',...) creates a new WINSAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winSal_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winSal_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winSal

% Last Modified by GUIDE v2.5 14-Feb-2017 10:18:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winSal_OpeningFcn, ...
                   'gui_OutputFcn',  @winSal_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function winSal_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winSal (see VARARGIN)

% Choose default command line output for winSal
handles.output = hObject;

    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end
    
    % Get transect selected
    handles.selected=varargin{hInput+2};
    
    % Set the window position to lower left
    setWindowPosition(hObject,handles);
    
    % Change comment icon
    commentIcon(handles.pbComment);
    
    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');

    % Display the current draft
    salSource=meas.transects(handles.selected).sensors.salinity_ppt.selected;
    handles.salinity=meas.transects(handles.selected).sensors.salinity_ppt.(salSource).data(1);
    set(handles.edSal,'String',sprintf('% 4.0f',handles.salinity));
    set(handles.pbOne,'Enable','off');
    set(handles.pbAll,'Enable','off');
    % Update handles structure
guidata(hObject, handles);

function varargout = winSal_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function edSal_Callback(hObject, eventdata, handles)
% hObject    handle to edSal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Get user input
    handles.salinity=str2double(get(handles.edSal,'String'));
    if handles.salinity>-0.0001 && handles.salinity<45.1
        set(handles.pbOne,'Enable','on');
        set(handles.pbAll,'Enable','on');
        set(handles.txtError,'Visible','off');
    else
        set(handles.txtError,'Visible','on')
    end
    guidata(hObject, handles);

function edSal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edSal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pbOne_Callback(hObject, eventdata, handles)
% hObject    handle to pbOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Apply salinity
    meas=changeSOS(meas,handles.selected,'salinity',handles.salinity);
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
   
    % Return pointer to previous
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    guidata(hObject, handles);
    
    % Close winSal
    delete (handles.output);  

function pbAll_Callback(hObject, eventdata, handles)
% hObject    handle to pbAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Apply salinity
    meas=changeSOS(meas,'salinity',handles.salinity);
   
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Set pointer to previous
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    guidata(hObject, handles);
    
    % Close winSal
    delete (handles.output);  

function pbCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pbCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Close winSal
    guidata(hObject, handles);
    delete (handles.output);  

function pbComment_Callback(hObject, eventdata, handles)
% hObject    handle to pbComment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Add comment
    [handles]=commentButton(handles, 'Depth');
    % Update handles structure
    guidata(hObject, handles);    
    
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    %open('helpFiles\QRev_Users_Manual.pdf') 
web('QRev_Help_Files\HTML\temp_salinity.htm')
