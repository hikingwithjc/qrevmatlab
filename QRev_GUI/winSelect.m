function varargout = winSelect(varargin)
% WINSELECT MATLAB code for winSelect.fig
%      WINSELECT, by itself, creates a new WINSELECT or raises the existing
%      singleton*.
%
%      H = WINSELECT returns the handle to a new WINSELECT or the handle to
%      the existing singleton*.
%
%      WINSELECT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINSELECT.M with the given input arguments.
%
%      WINSELECT('Property','Value',...) creates a new WINSELECT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winSelect_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winSelect_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winSelect

% Last Modified by GUIDE v2.5 14-Feb-2017 10:20:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winSelect_OpeningFcn, ...
                   'gui_OutputFcn',  @winSelect_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before winSelect is made visible.
function winSelect_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winSelect (see VARARGIN)

% Choose default command line output for winSelect
handles.output = hObject;

    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end

    % Position window
    set(hObject,'Units',handles.hMainGui.Units);
    pos=hObject.Position;
    mainpos=handles.hMainGui.Position;
    x=mainpos(1)+170;
    y=mainpos(2)+mainpos(4)-30-pos(4);
    newpos=[[x,y],pos(3:4)];
    set(hObject,'Position',newpos);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes winSelect wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = winSelect_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pbSonTek.
function pbSonTek_Callback(hObject, eventdata, handles)
% hObject    handle to pbSonTek (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(handles.hMainGui,'Source',{'SonTek',logical(0)});
delete (handles.output);

% --- Executes on button press in pbQRev.
function pbQRev_Callback(hObject, eventdata, handles)
% hObject    handle to pbQRev (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(handles.hMainGui,'Source',{'QRev',logical(0)});
delete (handles.output);

% --- Executes on button press in pbCancel.
function pbCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pbCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(handles.hMainGui,'Source',{'',logical(0)});
delete (handles.output);

% --- Executes on button press in pbTRDI.
function pbTRDI_Callback(hObject, eventdata, handles)
% hObject    handle to pbTRDI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.cbTRDI,'Value')
    setappdata(handles.hMainGui,'Source',{'TRDI',logical(1)});
else
    setappdata(handles.hMainGui,'Source',{'TRDI',logical(0)});
end
delete (handles.output);

% --- Executes on button press in cbTRDI.
function cbTRDI_Callback(hObject, eventdata, handles)
% hObject    handle to cbTRDI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbTRDI


% --- Executes on button press in pbHelp.
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
open('helpFiles\QRev_Users_Manual.pdf')  
