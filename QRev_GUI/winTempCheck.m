function varargout = winTempCheck(varargin)
% WINTEMPCHECK MATLAB code for winTempCheck.fig
%      WINTEMPCHECK, by itself, creates a new WINTEMPCHECK or raises the existing
%      singleton*.
%
%      H = WINTEMPCHECK returns the handle to a new WINTEMPCHECK or the handle to
%      the existing singleton*.
%
%      WINTEMPCHECK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINTEMPCHECK.M with the given input arguments.
%
%      WINTEMPCHECK('Property','Value',...) creates a new WINTEMPCHECK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winTempCheck_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winTempCheck_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winTempCheck

% Last Modified by GUIDE v2.5 28-Mar-2016 11:12:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winTempCheck_OpeningFcn, ...
                   'gui_OutputFcn',  @winTempCheck_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before winTempCheck is made visible.
function winTempCheck_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winTempCheck (see VARARGIN)

% Choose default command line output for winTempCheck
handles.output = hObject;

    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end
    set(gcf,'CloseRequestFcn',{@optionalClose,handles});
    setWindowPosition(hObject,handles);
    
    commentIcon(handles.pbComment);
    
    % Call function to process and plot temperature time series
    timeSeriesData(handles)

    % Update handles structure
    guidata(hObject, handles);

function varargout = winTempCheck_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function optionalClose(hObject, eventdata, handles)
    pbCloseTemp_Callback(hObject, eventdata, handles);

function pbCloseTemp_Callback(hObject, eventdata, handles)
% hObject    handle to pbCloseTemp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    meas.qa=temperatureQA(meas.qa,meas);
    
    % Store measurement data
    setappdata(handles.hMainGui,'measurement',meas);
    
    delete (handles.output);

function rbF_Callback(hObject, eventdata, handles)
% Set temperature display units to degrees F

    % Clear C radio button
    set(handles.rbC,'Value',0);

    % Process and plot temperature time series
    timeSeriesData(handles);
    
    % Update handles structure
    guidata(hObject, handles);

% --- Executes on button press in rbC.
function rbC_Callback(hObject, eventdata, handles)
% Set temperature display units to degrees C

    % Clear F radio button
    set(handles.rbF,'Value',0);

    % Process and plot temperature time series 
    timeSeriesData(handles);

    % Update handles structure
    guidata(hObject, handles);

function edTempUser_Callback(hObject, eventdata, handles)
% User input of independent temperature reading

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Assign new user temperature
    temp=str2double(get(handles.edTempUser,'String'));
    if isnan(temp);
        meas.extTempChk.user=[];
    else
        % Convert temperature to F as needed
        if (get(handles.rbF,'Value') == get(handles.rbF,'Max'))
            temp=convertTemp(temp,'F','C');
        end
        meas.extTempChk.user=temp;
    end
    
    % Complete quality check using new temperature input
    meas.qa=temperatureQA(meas.qa,meas);
    
    % Store measurement data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Complete QA check on external temperature check
    manualQACheck(handles);

    % Update handles structure
    guidata(hObject, handles);
    
function edTempADCP_Callback(hObject, eventdata, handles)
% User input of ADCP temperature

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Assing new manually entered ADCP temperature
        temp=str2double(get(handles.edTempADCP,'String'));
    if isnan(temp);
        meas.extTempChk.adcp=[];
    else
        % Convert temperature to F as needed
        if (get(handles.rbF,'Value') == get(handles.rbF,'Max'))
            temp=convertTemp(temp,'F','C');
        end        
        meas.extTempChk.adcp=temp;
    end
    
    % Complete quality check using new temperature input
    meas.qa=temperatureQA(meas.qa,meas);
   
    % Store measurement data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Complete QA check on external temperature check
    manualQACheck(handles);
    
    % Update handles structure
    guidata(hObject, handles);

function pbComment_Callback(hObject, eventdata, handles)
% Add comment
    [handles]=commentButton(handles, 'Temperature Check');
    % Update handles structure
    guidata(hObject, handles);

function temperature=convertTemp(tempIn,unitsIn,unitsOut)
% Convert temperature units
    % Temperature input in degrees F
    if strcmpi(unitsIn,'F')
        % Temperature output in degrees C
        if strcmpi(unitsOut,'C')
            temperature=(tempIn-32).*(5./9);
        else
            temperature=tempIn;
        end
    end
    
    % Temperature input in degrees C
    if strcmpi(unitsIn,'C')
        % Temperature output in degrees F
        if strcmpi(unitsOut,'F')
            temperature=(tempIn.*(9./5))+32;
        else
            temperature=tempIn;
        end
    end
  
    
function timeSeriesData(handles)
% Processes and plots the temperature time series data. Includes checks for
% the time series and external temperature check to update the messages.

    % Temperature threshold to trigger warning in C
    tempThreshold=1;
    
    % Get axes handle and clear axes
    h=handles.axTempTimeSeries;
    cla(h);

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get display units
    if get(handles.rbC,'Value')==get(handles.rbC,'Max')
        displayUnits='C';
    end
    if get(handles.rbF,'Value')==get(handles.rbF,'Max')
        displayUnits='F';
    end
    
    % Set GUI display of independent temperature reading
    temp=convertTemp(meas.extTempChk.user,'C',displayUnits);
    temp=num2str(temp,'%4.1f');
    if isnan(temp)
        set(handles.edTempUser,'String','');
    else
        set(handles.edTempUser,'String',temp);
    end
    
    % Set GUI display of ADCP temperature reading
    temp=convertTemp(meas.extTempChk.adcp,'C',displayUnits);
    temp=num2str(temp,'%4.1f');
    if length(temp)==0
        set(handles.edTempADCP,'String','');
    else
        set(handles.edTempADCP,'String',temp); 
    end
    
    % Process each transect to obtain a continuous time series
    transects=meas.transects;
    nTransects=length(transects);
    [temp,serialTime]=computeTimeSeries(transects);
    
    % Remove leading nan
    temp=temp(2:end);
    serialTime=serialTime(2:end);
    
    % Convert temperature to F as needed
    if strcmpi(displayUnits,'F');
        temp=convertTemp(temp,'C','F');
        tempThreshold=tempThreshold.*(9./5);
    end
    
    % Plot time series
    plot(h,serialTime,temp,'.b')
    xmax=nanmax(serialTime);
    xmin=nanmin(serialTime);
    set(h,'XTick',linspace(xmin,xmax,6))
    datetick(h,'x','HHMM','keepticks')
    ylim([nanmin(temp)-2,nanmax(temp)+2])
    
    if strcmpi(displayUnits,'F');
        ylabel(h,'Degrees F')
    else
        ylabel(h,'Degrees C')
    end
    xlabel (h,'Time (hhmm)')
    grid on
    drawnow
    
    % Compute range of time series and display appropriate message
    range=nanmax(temp)-nanmin(temp);
    if range < tempThreshold
        message=['GOOD: Range in recorded temperatures is ',num2str(range,'%4.1f'),' deg ',displayUnits];
        set(handles.txtMsgTemp1,'String',message);
    else
        message=['CAUTION: Range in recorded temperatures is ',num2str(range,'%4.1f'),' deg ',displayUnits];
        set(handles.txtMsgTemp1,'String',message);
    end
    
    manualQACheck (handles)

        
function manualQACheck (handles)
% Performs a QA check on the external temperature data
    
    % Temperature threshold in C
    tempThreshold=2;
    
    % Get user and ADCP values from measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get display units
    if get(handles.rbC,'Value')==get(handles.rbC,'Max')
        displayUnits='C';
    end
    if get(handles.rbF,'Value')==get(handles.rbF,'Max')
        displayUnits='F';
    end
    
    % Check user supplied independent temperature
    user=meas.extTempChk.user;
    if isempty(user)
         message=['No user supplied ADCP temperature'];
        set(handles.txtMsgTemp2,'String',message);
    else
        % Compare independent and ADCP temperature
        adcp=meas.extTempChk.adcp;
        if isempty(adcp)
            % Use mean temperature from measurement if no user supplied
            % ADCP temperature
            [temp,serialTime]=computeTimeSeries(meas.transects);
            adcp=nanmean(temp);
        end

        % Compute range and display appropriate message
        range=abs(user-adcp);
        if strcmp(displayUnits,'F')
                tempThreshold=tempThreshold.*(9./5);
                range=range.*(9./5);
        end
        if range < tempThreshold
            message=['GOOD: External temperature check difference is ',num2str(range,'%4.1f'),' deg ',displayUnits];
            set(handles.txtMsgTemp2,'String',message);
        else
            message=['WARNING: External temperature check difference is ',num2str(range,'%4.1f'),' deg ',displayUnits];
            set(handles.txtMsgTemp2,'String',message);
        end
    end
    
function [temp,serialTime]=computeTimeSeries(transects)
% Prepare temperature time series
    % Initialize variables
    temp=nan;
    serialTime=nan;
    nTransects=length(transects);
    
    % Loop through each transect
    for n=1:nTransects   
        % Check for situation where user has entered a constant temperature
        if length(transects(n).sensors.temperature_degC.(transects(n).sensors.temperature_degC.selected).data)>1
            % Temperatures for ADCP
            temp=[temp, transects(n).sensors.temperature_degC.(transects(n).sensors.temperature_degC.selected).data];
        else
            % User specified constant temperature
            temp=[temp, repmat(transects(n).sensors.temperature_degC.(transects(n).sensors.temperature_degC.selected).data,size(transects(n).sensors.temperature_degC.internal.data))];
        end
        serialTime=[serialTime, transects(n).dateTime.startSerialTime+nancumsum(transects(n).dateTime.ensDuration_sec)./(60*60*24)];
    end

% --- Executes on button press in pbHelp.
function pbHelp_Callback(hObject, eventdata, handles)
% Open help
    open('helpFiles\QRev_Users_Manual.pdf')  

%==========================================================================
%================NO MODIFIED CODE BELOW HERE===============================
%==========================================================================

function edTempADCP_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edTempADCP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edTempUser_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edTempUser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
