function varargout = winSOSSource(varargin)
%WINSOSSOURCE M-file for winSOSSource.fig
%      WINSOSSOURCE, by itself, creates a new WINSOSSOURCE or raises the existing
%      singleton*.
%
%      H = WINSOSSOURCE returns the handle to a new WINSOSSOURCE or the handle to
%      the existing singleton*.
%
%      WINSOSSOURCE('Property','Value',...) creates a new WINSOSSOURCE using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to winSOSSource_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      WINSOSSOURCE('CALLBACK') and WINSOSSOURCE('CALLBACK',hObject,...) call the
%      local function named CALLBACK in WINSOSSOURCE.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winSOSSource

% Last Modified by GUIDE v2.5 26-Jul-2016 16:51:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winSOSSource_OpeningFcn, ...
                   'gui_OutputFcn',  @winSOSSource_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before winSOSSource is made visible.
function winSOSSource_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for winTempSource
    handles.output = hObject;

    % Get main gui handle
    hInput=find(strcmp(varargin,'hMainGui'));
    if ~isempty(hInput)
        handles.hMainGui=varargin{hInput+1};
    end
    
    % Get transect selected
    handles.selected=varargin{hInput+2};
    
    % Set the window position to lower left
    setWindowPosition(hObject,handles);
    
    % Change comment icon
    commentIcon(handles.pbComment);
    
    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Display the current temperature source
    set(handles.edUser,'Enable','off');
    set(handles.txtUser,'String',uLabelV);
    handles.sosSelected=meas.transects(handles.selected).sensors.speedOfSound_mps.selected;
    handles.sosSource=meas.transects(handles.selected).sensors.speedOfSound_mps.(handles.sosSelected).source;
    if strcmpi(handles.sosSelected,'internal')
        if strcmpi(strtrim(handles.sosSource),'Calculated')
            set(handles.rbInt,'Value',1)
        else
            set(handles.rbComputed,'Value',1);
            handles.sosSelected='Computed';
        end
    elseif strcmpi(handles.sosSelected,'external')
        set(handles.rbExt,'Value',1)
    elseif strcmpi(handles.sosSelected,'user')
        set(handles.rbUser,'Value',1)
        set(handles.edUser,'Enable','on');
        handles.userSOS=meas.transects(handles.selected).sensors.speedOfSound_mps.user.data.*unitsV;
        set(handles.edUser,'String',handles.userSOS);
        set(handles.txtUser,'String',uLabelV);
        if isempty(get(handles.edUser,'String'))
            set(handles.pbOne,'Enable','off');
            set(handles.pbAll,'Enable','off');
        end
    end
    if isempty(meas.transects(handles.selected).sensors.speedOfSound_mps.external)
        set(handles.rbExt,'Enable','off');
    end
    
    % Update handles structure
    guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = winSOSSource_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pbOne.
function pbOne_Callback(hObject, eventdata, handles)
% hObject    handle to pbOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Apply new SOS Source
    
    if strcmp(handles.sosSelected,'user')      
        meas=changeSOS(meas,handles.selected,'sosSrc',handles.sosSelected,handles.userSOS); 
    else
        meas=changeSOS(meas,handles.selected,'sosSrc',handles.sosSelected);  
    end
    
    % Reset cursor
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Set pointer to previous
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    guidata(hObject, handles);
    
    % Close winSOSSource
    delete (handles.output);  

% --- Executes on button press in pbAll.
function pbAll_Callback(hObject, eventdata, handles)
% hObject    handle to pbAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Get data from hMainGui
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Apply new SOS Source
    if strcmp(handles.sosSelected,'user')      
        meas=changeSOS(meas,'sosSrc',handles.sosSelected,handles.userSOS); 
    else
        meas=changeSOS(meas,'sosSrc',handles.sosSelected);  
    end
    
    
    % Reset cursor
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Set pointer to previous
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    guidata(hObject, handles);
    
    % Close winSOSSource
    delete (handles.output);  

% --- Executes on button press in pbCancel.
function pbCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pbCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Close winSOSSource
    guidata(hObject, handles);
    delete (handles.output); 

% --- Executes on button press in pbComment.
function pbComment_Callback(hObject, eventdata, handles)
% hObject    handle to pbComment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Add comment
    [handles]=commentButton(handles, 'SOS');
    % Update handles structure
    guidata(hObject, handles);  

% --- Executes on button press in pbHelp.
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
   % open('helpFiles\QRev_Users_Manual.pdf') 
web('QRev_Help_Files\HTML\temp_salinity.htm')


function edUser_Callback(hObject, eventdata, handles)
% hObject    handle to edUser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    handles.userSOS=str2double(get(handles.edUser,'String'))./unitsV;
    
    set(handles.pbOne,'Enable','on');
    set(handles.pbAll,'Enable','on');
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edUser_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edUser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected object is changed in bgSOSSource.
function bgSOSSource_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in bgSOSSource 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    if get(handles.rbInt,'Value')
        handles.sosSelected='internal';
    elseif get(handles.rbComputed,'Value')
        handles.sosSelected='Computed';
    elseif get(handles.rbExt,'Value')
        handles.sosSelected='external';
    elseif get(handles.rbUser,'Value')
        handles.sosSelected='user';  
        set(handles.edUser,'Enable','on')
        if isempty(get(handles.edUser,'String'))
            set(handles.pbOne,'Enable','off');
            set(handles.pbAll,'Enable','off');
        end
    end
guidata(hObject, handles);
