function varargout = QRev2(varargin)
% QREV2 MATLAB code for QRev2.fig
%      QREV2, by itself, creates a new QREV2 or raises the existing
%      singleton*.
%
%      H = QREV2 returns the handle to a new QREV2 or the handle to
%      the existing singleton*.
%
%      QREV2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in QREV2.M with the given input arguments.
%
%      QREV2('Property','Value',...) creates a new QREV2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before QRev2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to QRev2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help QRev2

% Last Modified by GUIDE v2.5 24-May-2017 13:55:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @QRev2_OpeningFcn, ...
                   'gui_OutputFcn',  @QRev2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% ============================
% Opening and Output Functions
% ============================

function QRev2_OpeningFcn(hObject, eventdata, handles, varargin)
    % Opening function which executes prior to any of the other functions. All
    % initializations for the GUI are contained here.
    %
    % Arguments IN:
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to QRev2 (see VARARGIN)
    %
    % Arguments OUT:
    % ---

    % Choose default command line output for QRev2
    handles.output = hObject;
    movegui('center')
    
    % Get source folder
    handles.sourceFolder=[pwd,'\'];
    handles.cautionIcon=[pwd,'\caution.png'];
    handles.warningIcon=[pwd,'\warning.png'];
    

    % Initialize handles variable for computational data
    handles.hMainGui=hObject;
    handles.newData=false;
    handles.saveChecked=false;
    set(gcf,'CloseRequestFcn',{@optionalClose});
    
    % Set version number in header
    handles.version='QRev - 3.64';
    set(handles.guiMain,'Name',[handles.version,'  ',handles.sourceFolder]);

    % Set user preferences
    if isnan(getUserPref('Folder'))
        setUserPref('Folder');
    end
    if isnan(getUserPref('Units'))
        setUserPref('Units');
    end
    if isnan(getUserPref('styleSheet'))
        setUserPref('styleSheet')
    end
    
    displayUnits=getUserPref('Units');
    prefPath=getUserPref('Folder');
    handles.styleSheet=getUserPref('styleSheet');
       
    % Store app data
    setappdata(handles.hMainGui,'Units',displayUnits);
%     setappdata(handles.hMainGui,'PrefPath',prefPath);
    setappdata(handles.hMainGui,'InterpVisible','off');
    setappdata(handles.hMainGui,'change',0);
    
    commentIcon(handles.pbComment);
    
    % Uncheck Icon
     uncheckIcon=imread('Uncheck.png','png');
     set(handles.pbUncheck,'Units','pixels');
     pos=get(handles.pbUncheck,'Position');
     pos(3:4)=[35,35];
     set(handles.pbUncheck,'Position',pos);
     set(handles.pbUncheck,'Units','normalized');
     set(handles.pbUncheck,'CData',uncheckIcon);
     set(handles.pbUncheck,'String','');
    
    % For debugging purposes, doesn't work as expected
    % handles.dsm=1;

    % Set color variables
    handles.goodColor=[0 1 0];
    handles.goodFontW='Normal';
    handles.goodFontA='Normal';
    handles.cautionColor=[1,1,0];
    handles.cautionFontW='Normal';
    handles.cautionFontA='Italic';
    handles.warningColor=[1 0 0];
    handles.warningFontW='Bold';
    handles.warningFontA='Normal';
    handles.defaultColor=[0.94 0.94 0.94];
    handles.defaultFontW='Normal';
    handles.defaultFontA='Normal';

    % Prep details table    
    startCol=2;
    startRow=2;
    handles.startCol=startCol;
    handles.startRow=startRow;
    
    DetTableData=detailTableLabels(handles);

    % Apply labels to table
    set(handles.tblDetail,'Data',DetTableData);
    
    panelName=['Measurement Details (','Units: ',displayUnits,')'];
    set(handles.uipanelDetails,'Title',panelName);
    
    % QA Table row labels
    n=1;
    QATableData{n,1}='Random 95% Uncertainty';
    n=n+1;
    QATableData{n,1}='Invalid Data 95% Uncertainty';
    n=n+1;
    QATableData{n,1}='Edge Q 95% Uncertainty';
    n=n+1;
    QATableData{n,1}='Extrapolation 95% Uncertainty';
    n=n+1;
    QATableData{n,1}='Moving-Bed 95% Uncertainty';
    n=n+1;
    QATableData{n,1}='Systematic 68% Uncertainty';
    n=n+1;
    QATableData{n,1}='<html><b>Estimated 95% Uncertainty';
    
    % Apply labels to table
    set(handles.tblQA,'Data',QATableData);
    handles.ClearQATableData=QATableData;
    
    % Save select settings for access by other functions
    setappdata(handles.hMainGui,'QRevGui',handles);
    
    % Buttons enabled/disabled
    buttonsOff(handles)
    set(handles.pbSelDataFiles,'Enable','on');
    set(handles.pbClose,'Enable','on');    
    
    % Update handles structure
    guidata(hObject, handles);

function DetTableData=detailTableLabels(handles)
    
    % Get start row
    startRow=handles.startRow;
    
    % Get start time
    meas=getappdata(handles.hMainGui,'measurement');
    if ~isempty(meas)
        checked=logical([meas.transects.checked]);
        idx=find(checked==1,1,'first');
        if ~isempty(idx)
            startDate=['(',datestr(meas.transects(idx).dateTime.startSerialTime,'mm/dd/yyyy'),')'];
        else
            startDate=''; 
        end
    else
        startDate='';
    end
    
    % Get end time
    if ~isempty(meas)
        idx=find(checked==1,1,'last');
        if ~isempty(idx)
            endDate=['(',datestr(meas.transects(idx).dateTime.endSerialTime,'mm/dd/yyyy'),')'];
        else
            endDate=''; 
        end
    else
        endDate='';
    end
    
    % Details table row labels
    DetTableData{startRow-1,1}='<html><b><u>DISCHARGE';
    n=0;
    DetTableData{startRow+n,1}='Use';
    n=n+1;
    DetTableData{startRow+n,1}='<html><b>Total Q';
    n=n+1;
    DetTableData{startRow+n,1}='Top Q';
    n=n+1;
    DetTableData{startRow+n,1}='Middle Q';
    n=n+1;
    DetTableData{startRow+n,1}='Bottom Q';
    n=n+1;
    DetTableData{startRow+n,1}='Left Q';
    n=n+1;
    DetTableData{startRow+n,1}='Right Q';

    n=n+1;
    DetTableData{startRow+n,1}='<html><b><u>TIME';
    n=n+1;
    DetTableData{startRow+n,1}='Duration (s)';
    n=n+1;
    DetTableData{startRow+n,1}=['Start Time ',startDate];
    n=n+1;
    DetTableData{startRow+n,1}=['End Time ',endDate];
  
    n=n+1;
    DetTableData{startRow+n,1}='<html><b><u>REFERENCE';
    n=n+1;
    DetTableData{startRow+n,1}='Navigation Ref';
    n=n+1;
    DetTableData{startRow+n,1}='Composite Tracks';    
    n=n+1;
    DetTableData{startRow+n,1}='Depth Ref';    
  
    n=n+1;
    DetTableData{startRow+n,1}='<html><b><u>MOVING-BED';
    n=n+1;
    DetTableData{startRow+n,1}='Moving-bed';
    n=n+1;
    DetTableData{startRow+n,1}='Correction';
    
    n=n+1;
    DetTableData{startRow+n,1}='<html><b><u>CHARACTERISTICS';
    n=n+1;
    DetTableData{startRow+n,1}='Width';
    n=n+1;
    DetTableData{startRow+n,1}='Area';
    n=n+1;
    DetTableData{startRow+n,1}='Avg Boat Speed';
    n=n+1;
    DetTableData{startRow+n,1}='Course Made Good (deg)';
    n=n+1;
    DetTableData{startRow+n,1}='Q/A';
    n=n+1;
    DetTableData{startRow+n,1}='Avg Water Direction (deg)';
    
    n=n+1;
    DetTableData{startRow+n,1}='<html><b><u>SETTINGS';
    n=n+1;
    DetTableData{startRow+n,1}='Transducer Depth';
    n=n+1;
    DetTableData{startRow+n,1}='Excluded Distance';
    n=n+1;
    DetTableData{startRow+n,1}='Start Edge';
    n=n+1;
    DetTableData{startRow+n,1}='Magnetic Variation (deg)';
    n=n+1;
    DetTableData{startRow+n,1}='Top Method';
    n=n+1;
    DetTableData{startRow+n,1}='Bottom Method';
    n=n+1;
    DetTableData{startRow+n,1}='Exponent';
    n=n+1;
    DetTableData{startRow+n,1}='Left Distance';
    n=n+1;
    DetTableData{startRow+n,1}='Right Distance';
    n=n+1;
    DetTableData{startRow+n,1}='Left Type';
    n=n+1;
    DetTableData{startRow+n,1}='Right Type';
    
    n=n+1;
    DetTableData{startRow+n,1}='<html><b><u>PRE-MEASUREMENT';
    n=n+1;
    DetTableData{startRow+n,1}='ADCP Test';
    n=n+1;
    DetTableData{startRow+n,1}='Test Fails';
    n=n+1;
    DetTableData{startRow+n,1}='Compass Cal';
    n=n+1;
    DetTableData{startRow+n,1}='Compass Eval';
    n=n+1;
    DetTableData{startRow+n,1}='Moving-Bed Test';
    n=n+1;
    DetTableData{startRow+n,1}='Moving Bed?';
    n=n+1;
    DetTableData{startRow+n,1}='External Temperature (C)';
    n=n+1;
    DetTableData{startRow+n,1}='Temperature Change (C)';
    
    n=n+1;
    DetTableData{startRow+n,1}='<html><b><u>DEPTH QUALITY';
    n=n+1;
    DetTableData{startRow+n,1}='Number Invalid';
    n=n+1;
    DetTableData{startRow+n,1}='Interpolation Method';
    
    n=n+1;
    DetTableData{startRow+n,1}='<html><b><u>NAVIGATION QUALITY';
    n=n+1;
    DetTableData{startRow+n,1}='BT-Percent Invalid';
    n=n+1;
    DetTableData{startRow+n,1}='BT-Filters';
    n=n+1;
    DetTableData{startRow+n,1}='BT-Interpolation';
    n=n+1;
    DetTableData{startRow+n,1}='GGA-Percent Invalid';
    n=n+1;
    DetTableData{startRow+n,1}='GGA-Filters';
    n=n+1;
    DetTableData{startRow+n,1}='GGA-Interpolation';
    n=n+1;
    DetTableData{startRow+n,1}='VTG-Percent Invalid';
    n=n+1;
    DetTableData{startRow+n,1}='VTG-Filters';
    n=n+1;
    DetTableData{startRow+n,1}='VTG-Interpolation';
    
    n=n+1;
    DetTableData{startRow+n,1}='<html><b><u>WATER TRACK QUALITY';
    n=n+1;
    DetTableData{startRow+n,1}='Filters';
    n=n+1;
    DetTableData{startRow+n,1}='Interpolation';
    
function varargout = QRev2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% ================
% Button Callbacks
% ================

function pbOptions_Callback(hObject, eventdata, handles)
% Set/change display units

    % Pause added to fix problem with double clicking the button.
    pause (0.3);
    % Open figure
    handles.hOptions=winOptions(handles.hMainGui,handles.pbOptions,handles.saveChecked,handles.styleSheet);

    % Turn all buttons off so only this figure is active
    buttonsOff(handles);  
    
    % Wait for figure to close before proceeding
     waitfor(handles.hOptions);
      

    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    

%     % Get current display units
%     displayUnits=getappdata(handles.hMainGui,'Units');
%     
%     % Get new display units from user
%     newUnits=questdlg('Select units for display and output:','Units','English','SI',displayUnits);
%     
%     % If new units specified set display units
%     if length(newUnits)>1
%         displayUnits=newUnits;
%     end
    setappdata(handles.hMainGui,'Units',handles.pbOptions.UserData.displayUnits);
    setUserPref('Units',handles.pbOptions.UserData.displayUnits);
    
    panelName=['Measurement Details (','Units: ',handles.pbOptions.UserData.displayUnits,')'];
    set(handles.uipanelDetails,'Title',panelName);
    
    handles.saveChecked=handles.pbOptions.UserData.saveChecked;
    if handles.saveChecked
         set(handles.pbSave,'String','Save Checked');
    else
         set(handles.pbSave,'String','Save');
    end
    
    handles.styleSheet=handles.pbOptions.UserData.styleSheet;
    setUserPref('styleSheet',handles.pbOptions.UserData.styleSheet);
    
    % Update displayed data to reflect new units
    handles=updateQ(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
     % Turn buttons on
    buttonsOn(handles)
    
    % Update handles structure
    guidata(hObject, handles);
    
function pbSelDataFiles_Callback(hObject, eventdata, handles)
    % Opens a prompt for the user to select the manufacture and then opens the
    % file selection diaglog. Once the files are selected they are completely
    % processed the GUI updated.
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % meas  clsMeasurement (created)
    % discharge clsQComp (created)
    % 
    % CALLS: 
    % clsMeasurement
    % updateQ
    % updateMessages
    % qaAlerts
    % plotExtrap
    
    % Pause added to fix problem with double clicking the button.
    pause (0.3);
    
    % Open data files for processing
    % ------------------------------

    
    % Determine source of data
    handles.hWinSelect=winSelect('hMainGui',handles.hMainGui);
    
    % Wait for figure to close before proceeding
    waitfor(handles.hWinSelect);
    temp=getappdata(handles.hMainGui,'Source');
    source=temp{1};
    checkedOnly=temp{2};

    % If source selected determine file type
    if ~isempty(source)
        
        % Initialize status update
%         updateStatus('',handles.tblMsg);  
        
        % Get previous or set current folder as the path
        prefPath=getUserPref('Folder');
        
        % Process QRev data
        if strcmpi(source,'QRev')
            
            % Set source in app data
            setappdata(handles.hMainGui,'source','QRev');
            handles.newData=false;
            
            % User selects file to load
            if ~ischar(prefPath)
                prefPath='';
            end
            [fileName,pathName]=uigetfile('*_QRev.mat','SELECT QRev FILE FOR REPROCESSING',prefPath);
            
            % Set busy pointer while loading
            oldpointer = get(gcf, 'pointer');      
            set(gcf, 'pointer', 'watch');     
            drawnow;     
            
            % Update GUI with pathname
            set(handles.guiMain,'Name',[handles.version,'    ',pathName,fileName]);
            
            % Check to ensure a file was selected and load data
            if length(pathName)>1
                clearGUI(handles);
                fullName=strcat(pathName,fileName);
                setUserPref('Folder',pathName);
                load(fullName);
                if exist('meas_struct','var')
                    meas=clsMeasurement();
                    meas=loadQRev(meas,meas_struct);
                elseif exist('meas','var') && isa(meas,'clsMeasurement')
                    meas_struct=clsMeasurement.obj2struct (meas);
                    meas=clsMeasurement();
                    meas=loadQRev(meas,meas_struct);
%                     meas=updateDataStructure(meas);
                    % Store objects
                    setappdata(handles.hMainGui,'measurement',meas);
                end
                try 
                    % Alert and prompt user
                    % Version is a Matlab keyword so this is a work around
                    QRevVersion=load(fullName,'version');
                    % Files processed with QBatch have the version in a cell
                    % array.
                    if iscell(QRevVersion.version)
                        verNumber=str2double(QRevVersion.version{2}(end-3:end));
                        verNumbertxt=QRevVersion.version{2}(end-3:end);
                    else
                        verNumber=str2double(QRevVersion.version(end-4:end));
                        verNumbertxt=QRevVersion.version(end-4:end);
                    end
                    
                catch
                    verNumber=0;
                    verNumbertxt='0';
                end
                currentVerNumber=str2double(handles.version(end-4:end));
                
                if verNumber<currentVerNumber
                    response=questdlg(['QRev has been updated (version ',handles.version(end-4:end),') since this file was saved (version ',verNumbertxt,'). You can view the file without recomputing the discharge by pressing View. The QA messages will reflect the most recent version. NOTE: Any changes will reprocess the file using the QRev updates. You can reprocess the file now by pressing Reprocess.'],'QRev - Updated','View','Reprocess','View');
                
                
                    if strcmp(response,'Reprocess')
                        if ~isempty(meas.mbTests)
                            checked=[meas.transects.checked];
                            idx=find(checked==1,1,'first');
                            if isempty(idx)
                                idx=1;
                            end
                            navRef=meas.transects(idx).boatVel.(meas.transects(idx).boatVel.selected).navRef;
                            meas.mbTests=autoUse2Correct(meas.mbTests,navRef);
                        end
                        settings=clsMeasurement.QRevDefaultSettings(meas);
                        settings=clsMeasurement.QRevDefaultInterp(settings); 
                        settings.processing='QRev';  
                        meas=applySettings(meas,settings);     
                    end
                end
                
                % Store data
                setappdata(handles.hMainGui,'measurement',meas);  
            end % if fileNames
            
        % Process TRDI data    
        elseif strcmpi(source,'TRDI') 
                
            % Set source in app data
            setappdata(handles.hMainGui,'source','TRDI');
            handles.newData=true;

            % User selects file
            if ~ischar(prefPath)
                prefPath='';
            end
            [fileName,pathName]=uigetfile('*.mmt','SELECT MMT FILE FOR PROCESSING',prefPath);

            % Turn pointer to busy while loading
            oldpointer = get(gcf, 'pointer');      
            set(gcf, 'pointer', 'watch');     
            drawnow;     

            % Update GUI
            set(handles.guiMain,'Name',[handles.version,'    ',pathName,fileName]); 

            % Check to ensure a file was selected and process data
            if length(pathName)>1
                clearGUI(handles);
                fullName=strcat(pathName,fileName);
                setappdata(handles.hMainGui,'mmtFileName',fullName);
                setUserPref('Folder',pathName);

                % Create measurement
                meas=clsMeasurement(source,fullName,checkedOnly);

                % Store data
                setappdata(handles.hMainGui,'measurement',meas);
            end % if fileNames

        % Process SonTek data
        elseif strcmpi(source,'SonTek')
            
            % Set source in app data
            setappdata(handles.hMainGui,'source','SonTek');
            handles.newData=true;
            
            % User selects files
            if ~ischar(prefPath)
                prefPath='';
            end
            [fileName,pathName]=uigetfile('*.mat','Multiselect','on','SELECT SONTEK MATLAB TRANSECT FILES FOR PROCESSING',prefPath);
            
            % Pointer busy while processing
            oldpointer = get(gcf, 'pointer');      
            set(gcf, 'pointer', 'watch');     
            drawnow;     
            
            % Update GUI
            set(handles.guiMain,'Name',[handles.version,'    ',pathName]); 

            % Ensure at least one file was selected and process data
            if length(pathName)>1
                clearGUI(handles);
                
                % Determine Number of Files to be Processed.
                if  isa(fileName,'cell')
                    numFiles=size(fileName,2); 
                    fileName=sort(fileName);       
                else
                    numFiles=1;
                    fileName={fileName};
                end % if cell

                % Create full filenames with path
                setUserPref('Folder',pathName);
                
                for j=1:numFiles
                    fullName(j)=strcat(pathName,fileName(j));
                end % for j

                % Create measurement
                meas=clsMeasurement(source,fullName);
                
                % Store data
                setappdata(handles.hMainGui,'measurement',meas);                    
            end % if fileNames
       
        % No data selected
        else
            clearGUI(handles);
        end % if QRev 
        
        % Return pointer to arrow
        set(gcf, 'pointer', 'arrow');     
        drawnow;  
        
        % If a measurement was loaded
        if exist('meas')
                
                % Set cursor to busy
                oldpointer = get(gcf, 'pointer');      
                set(gcf, 'pointer', 'watch');     
                drawnow;     

                % Update tables
                handles=updateQ(handles);
                handles=qaAlerts(handles);

                % Update extrapolation graph
                plotExtrap(handles,meas);
                set(gcf, 'pointer', 'arrow');  
                set(handles.guiMain,'pointer','arrow');
                drawnow;
                

                % Check for GPS data and enable or disable associated buttons
                % as appropriate
                if isempty([meas.transects.gps])
                    set(handles.pbGPSFilters,'Enable','off');
                    set(handles.pbRef,'Enable','off');
                else
                    set(handles.pbGPSFilters,'Enable','on');
                    set(handles.pbRef,'Enable','on');
                end % if GPS

                % Check for heading information and enable or disable the
                % compass button
                heading=unique(meas.transects(1).sensors.heading_deg.internal.data);
                if length(heading==1) & heading==0
                    set(handles.pbCompassCal,'Enable','off');
                else
                    set(handles.pbCompassCal,'Enable','on');
                end
                buttonsOn(handles);
                drawnow
        end % if meas
        
        % Update handles
        guidata(hObject, handles);
    end % if source
    
function pbViewInstrument_Callback(hObject, eventdata, handles)
    % Opens a figure (window) that allows the user to view characteristics about the
    % instrument.
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % Only displays data 
    %
    % CALLS:
    % winInstrument
    
    % Pause added to fix problem with double clicking the button.
    pause (0.3);
    % Open figure
     handles.hWinInstrument=winInstrument('hMainGui',handles.hMainGui);

    % Turn all buttons off so only this figure is active
    buttonsOff(handles);  
    
    % Wait for figure to close before proceeding
     waitfor(handles.hWinInstrument);
      

    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    
    % Update GUI
    handles=updateQ(handles);
    meas=getappdata(handles.hMainGui,'measurement');
    plotExtrap(handles,meas);
    handles=qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;
    
    % Turn buttons on
    buttonsOn(handles);
    
    % Update handles
    guidata(hObject, handles);

function pbSystemTest_Callback(hObject, eventdata, handles)
    % Opens a figure (window) that allows the user to view/load the results
    % of the system test.
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % meas may be modified
    %
    % CALLS:
    % winSystemTest
    % qaAlerts
    
    % Pause added to fix problem with double clicking the button.
    pause (0.3);
    
    % Open figure
    handles.hWinSystemTest=winSystemTest('hMainGui',handles.hMainGui);

    % Turn all buttons off so only this figure is active
    buttonsOff(handles);    

    % Wait for figure to close before proceeding
    waitfor(handles.hWinSystemTest);

    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;   
    
    % Apply quality checks
    handles=qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;
    
    % Turn buttons on
    buttonsOn(handles)
    
    % Update handles
    guidata(hObject, handles);

function pbCompassCal_Callback(hObject, eventdata, handles)
    % Opens a figure (window) that allows the user to view/load the results of the compass
    % calibration and/or evaluation.
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % meas maybe modified
    %
    % CALLS:
    % winCompassCal
    % qaAlerts
    
    % Pause added to fix problem with double clicking the button.
    pause (0.3);
    
    % Open figure
%     handles.hWinCompassCal=winCompassCal('hMainGui',handles.hMainGui);
    handles.hWinHPR=winHPR('hMainGui',handles.hMainGui);
    
    % Turn all buttons off so only this figure is active
    buttonsOff(handles); 
    
    % Wait for figure to close before proceeding
%     waitfor(handles.hWinCompassCal);
    waitfor(handles.hWinHPR);
    
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;  
    
     % Update GUI
    handles=updateQ(handles);
    meas=getappdata(handles.hMainGui,'measurement');
    plotExtrap(handles,meas);
    handles=qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;   
    
    % Turn all buttons on
    buttonsOn(handles);
    
    % Update handles
    guidata(hObject, handles);

function pbTempChk_Callback(hObject, eventdata, handles)
    % Opens a figure (window) that allows the user to view the temperature time
    % series for the measurement and enter an independent and ADCP temperature
    % comparison.
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % meas maybe modified
    %
    % CALLS:
    % winTempCheck
    % qaAlerts
    
    % Pause added to fix problem with double clicking the button.
    pause (0.3);    
    % Open figure
%     handles.hWinTempCheck=winTempCheck('hMainGui',handles.hMainGui);
    handles.hWinTempSal=winTempSal('hMainGui',handles.hMainGui);
    % Turn all buttons off so only this figure is active
    buttonsOff(handles);  

    % Wait for figure to close before proceeding
%     waitfor(handles.hWinTempCheck); 
    waitfor(handles.hWinTempSal);
    
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;
    
    % Apply automated quality assurance
    handles=qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow; 
    
    % Update the discharge and GUI   
    handles=updateQ(handles);  
    
    % Turn buttons on
    buttonsOn(handles);
    
    % Update handles
    guidata(hObject, handles);
    
function pbMovBedTst_Callback(hObject, eventdata, handles)
    % Opens a figure (window) that allows the user to view and load moving bed
    % tests. Applying the moving-bed tests to the measurement is also
    % accomplished through this figure.
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % meas maybe modified
    % discharge maybe modified
    % 
    % CALLS:
    % winMovingBed
    % qaAlerts
    % updateQ
       
    % Pause added to fix problem with double clicking the button.
    pause (0.3);    
    
    % Open figure
    handles.hWinMovingBed=winMovingBed('hMainGui',handles.hMainGui);
    
    % Turn all buttons off so only this figure is active
    buttonsOff(handles);    

    % Wait for figure to close before proceeding
    waitfor(handles.hWinMovingBed);
    
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    
    % Update the discharge and GUI   
    handles=updateQ(handles);    
    
    % Apply automated quality assurance
    handles=qaAlerts(handles);

    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;
    
    % Turn buttons on
    buttonsOn(handles);
    
    % Update handles
    guidata(hObject, handles);

function pbUserInput_Callback(hObject, eventdata, handles)
% Opens a figure (window) to allow the user to view and edit various user
% settings such as, draft, magvar, temperature, salinity, etc.
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % meas maybe modified
    % discharge maybe modified
    %
    % CALLS:
    % winUser
    % qaAlerts
    % updateQ
    % plotExtrap
    
    % Pause added to fix problem with double clicking the button.
    pause (0.3);    
    
    % Open figure
    handles.hWinUser=winUser('hMainGui',handles.hMainGui);

    % Turn all buttons off so only this figure can be open
    buttonsOff(handles); 

    % Wait for figure to close before proceeding
    waitfor(handles.hWinUser);
    
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
 
    % Update discharge and GUI
    handles=updateQ(handles);
    meas=getappdata(handles.hMainGui,'measurement');
    plotExtrap(handles,meas);
    handles=qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;
    
    % Turn buttons on
    buttonsOn(handles);
    
    % Update handles
    guidata(hObject, handles);
    
function pbDepthFilters_Callback(hObject, eventdata, handles)
    % Opens a figure (window) for viewing and applying depth filters and 
    % interpolation methods.
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % meas maybe modified
    % discharge maybe modified
    %
    % CALLS:
    % winDepthFilter
    % updateQ
    % qaAlerts
    % plotExtrap
    
    % Pause added to fix problem with double clicking the button.
    pause (0.3);
    
    % Open figure
    handles.hWinDepthFilter=winDepthFilter('hMainGui',handles.hMainGui);

    % Turn all buttons off so only this figure can be open
    buttonsOff(handles); 

    % Wait for figure to close before proceeding
    waitfor(handles.hWinDepthFilter);
    
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Update discharge and GUI
    handles=updateQ(handles);
    meas=getappdata(handles.hMainGui,'measurement');
    plotExtrap(handles,meas);
    handles=qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;
    
    % Turn buttons on
    buttonsOn(handles);
    
    % Update handles
    guidata(hObject, handles);

function pbBTFilters_Callback(hObject, eventdata, handles)
    % Opens a figure (window) for viewing and applying bottom track filter and 
    % interpolation methods
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % meas maybe modified
    % discharge maybe modified
    %
    % CALLS:
    % winBTFilter
    % updateQ
    % qaAlerts
    % plotExtrap  
    
    % Pause added to fix problem with double clicking the button.
    pause (0.3);
    
    % Open figure
    handles.hWinBTFilter=winBTFilter('hMainGui',handles.hMainGui);

    % Turn all buttons off so only this figure can be open
    buttonsOff(handles);    
    
    % Wait for figure to close before proceeding
    waitfor(handles.hWinBTFilter);
    
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Update discharge and GUI
    handles=updateQ(handles);
    meas=getappdata(handles.hMainGui,'measurement');
    plotExtrap(handles,meas);
    handles=qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;
    
    % Turn buttons on
    buttonsOn(handles);
    
    % Update handles
    guidata(hObject, handles);

function pbGPSFilters_Callback(hObject, eventdata, handles)
    % Opens a figure (window) for viewing and applying GPS filters and
    % interpolation methods
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % meas maybe modified
    % discharge maybe modified
    %
    % CALLS:
    % winGPSFilter
    % updateQ
    % qaAlerts
    % plotExtrap
    
    % Pause added to fix problem with double clicking the button.
    pause (0.3);
    
    % Open figure
    handles.hWinGPSFilter=winGPSFilter('hMainGui',handles.hMainGui);
    
    % Turn all buttons off so only this figure can be open
    buttonsOff(handles);
    
    % Wait for figure to close before proceeding
    waitfor(handles.hWinGPSFilter);

    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Update discharge and GUI
    handles=updateQ(handles);
    meas=getappdata(handles.hMainGui,'measurement');
    plotExtrap(handles,meas);
    handles=qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;
    
    % Turn buttons on
    buttonsOn(handles);
    
    % Update handles
    guidata(hObject, handles);
    
function pbRef_Callback(hObject, eventdata, handles)    
    % Opens a figure (window) for setting the navigation reference and
    % turning on and off composite tracks.
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % meas maybe modified
    % discharge maybe modified
    %
    % CALLS:
    % winNav
    % updateQ
    % qaAlerts
    % plotExtrap
    
     % Pause added to fix problem with double clicking the button.
     pause (0.5);
    
    % Open figure
    handles.hWinNav=winNav('hMainGui',handles.hMainGui);
    
    % Turn all buttons off so only this figure can be open
    buttonsOff(handles);   

    % Wait for figure to close before proceeding
    waitfor(handles.hWinNav);
    
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Update discharge and GUI
    handles=updateQ(handles);
    meas=getappdata(handles.hMainGui,'measurement');
    plotExtrap(handles,meas);
    handles=qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;
    
    % Turn buttons on
    buttonsOn(handles);
    
    % Update handles
    guidata(hObject, handles);
    
function pbWTFilters_Callback(hObject, eventdata, handles)
    % Opens a figure (window) for viewing and applying water track filter and 
    % interpolation methods
        % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % meas maybe modified
    % discharge maybe modified
    %
    % CALLS:
    % winWTFilter
    % updateQ
    % qaAlerts
    % plotExtrap
    
    % Pause added to fix problem with double clicking the button.
    pause (0.3);
    % Open figure
    handles.hWinWTFilter=winWTFilter('hMainGui',handles.hMainGui);
     
    % Turn all buttons off so only this figure can be open
    buttonsOff(handles);   

    % Wait for figure to close before proceeding
    waitfor(handles.hWinWTFilter);

    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Update discharge and GUI
    handles=updateQ(handles);
    meas=getappdata(handles.hMainGui,'measurement');
    plotExtrap(handles,meas);
    handles=qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;
    
    % Turn buttons on
    buttonsOn(handles);
    
    % Update handles
    guidata(hObject, handles);

function pbExtrap_Callback(hObject, eventdata, handles)
    % Opens a figure (window) for evaluating and setting the appropriate
    % extrapolation methods
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % meas maybe modified
    % discharge maybe modified
    %
    % CALLS:
    % winExtrap
    % updateQ
    % qaAlerts
    % plotExtrap  
    
    % Pause added to fix problem with double clicking the button.
    pause (0.3);
    
    % Open figure
    handles.hWinExtrap=winExtrap('hMainGui',handles.hMainGui);

    % Turn all buttons off so only this figure can be open
    buttonsOff(handles);   

    % Wait for figure to close before proceeding
    waitfor(handles.hWinExtrap);
    set(gcf,'CloseRequestFcn',{@optionalClose});
  
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Update discharge and GUI
    handles=updateQ(handles);
    meas=getappdata(handles.hMainGui,'measurement');
    plotExtrap(handles,meas);
    handles=qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;
    
    % Turn buttons on
    buttonsOn(handles);
    
    % Update handles
    guidata(hObject, handles);
    
function pbEdges_Callback(hObject, eventdata, handles)
    % Opens a figure (window) showing the shiptrack and contour plot for
    % the edge data. The user can change the edge characteristics.
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % meas maybe modified
    % discharge maybe modified
    %
    % CALLS:
    % winEdges
    % updateQ
    % qaAlerts
    % plotExtrap
    
    % Pause added to fix problem with double clicking the button.
    pause (0.3);    
    
    % Open figure
    handles.hWinEdges=winEdges('hMainGui',handles.hMainGui);
     
    % Turn all buttons off so only this figure can be open
    buttonsOff(handles);    

    % Wait for figure to close before proceeding
    waitfor(handles.hWinEdges);
    
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     

    % Update discharge and GUI
    handles=updateQ(handles);
    meas=getappdata(handles.hMainGui,'measurement');
    plotExtrap(handles,meas);
    handles=qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;
    
    % Turn buttons on
    buttonsOn(handles);
    
    % Update handles
    guidata(hObject, handles);
    
function pbViewComments_Callback(hObject, eventdata, handles)
    % Opens a figure (window) showing all comments from raw data and those 
    % that have been entered in QRev.
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    %
    % CALLS:

    % Pause added to fix problem with double clicking the button.
    pause (0.3);
    
    % Open figure
    handles.hViewComments=winViewComments('hMainGui',handles.hMainGui);
     
    % Turn all buttons off so only this figure can be open
%     buttonsOff(handles);    

    % Wait for figure to close before proceeding
%     waitfor(handles.hViewComments);
          
    % Turn buttons on
    buttonsOn(handles);
    
    % Update handles
    guidata(hObject, handles);

function pbSave_Callback(hObject, eventdata, handles)
    % Saves the meas and discharge variables to a Matlab file.
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % Creates a Matlab file
    %
    % CALLS:
    % uiputfile
    % save
    
    % Pause added to fix problem with double clicking the button.
    pause (0.3);
    
    % Get measurement, discharge, and messages
    meas=getappdata(handles.hMainGui,'measurement');
    version=handles.version;
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    % Create default filename
    prefPath=getUserPref('Folder');
    t=datetime('now');
    prefix=datestr(t,'yyyymmdd_HHMMSS');
    matName=[prefPath prefix '_QRev.mat'];
    
    % Prompt user for filename offering the default name
    [fileName pathName]=uiputfile('*.mat','SELECT FILENAME AND LOCATION FOR MATLAB FILE',matName);

    % Save files
    if isnumeric(fileName) 
       msgbox('Files NOT SAVED');
    else
        % Automatically add file saved comment.
        username=getenv('USERNAME');
        checked=logical([meas.transects(:).checked]);
        q=clsQAData.meanQ(meas.discharge(checked),'total').*unitsQ;
        
        qstr=num2str(q,'%13.3f');
        comment={['FileSave ',datestr(t,'mm/dd/yyyy HH:MM:ss'),': ',fileName(1:end-4),' by ',username, ' Q=',qstr,' ',uLabelQ(2:end-1)]};
        
       
        
        meas_struct=clsMeasurement.obj2struct(meas);
        
        if handles.saveChecked
            meas_struct.transects=meas_struct.transects(checked);
            meas_struct.discharge=meas_struct.discharge(checked);
            meas_struct.extrapFit.normData=meas_struct.extrapFit.normData([checked,true]);
            meas_struct.extrapFit.selFit=meas_struct.extrapFit.selFit([checked,true]); 
        end
        if isempty(meas_struct.comments)
            meas_struct.comments={comment};
        else
            meas_struct.comments=[meas_struct.comments,{comment}];
        end
%         else
%             meas=addComment(meas,comment);
%         end
        setappdata(handles.hMainGui,'measurement',meas);
        
        % Save Matlab file
        matName=[pathName fileName];
        save(matName,'version','meas_struct');

        % Save the XML file
        ok=clsMeasurement.createXMLFile(meas,version,matName(1:end-4));
        if handles.styleSheet
            copyfile([pwd,'\QRevStylesheet.xsl'],pathName,'f');
        end
        handles.newData=false;
    end
    
    % Update handles
    guidata(hObject, handles);

function pbClose_Callback(hObject, eventdata, handles)
    % Closes QRev
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % Terminates the program
    %
    % CALLS:
    % close
    
    % If new data were loaded and not saved, prompt user
    if handles.newData
        selection = questdlg('New WinRiver2 or RiverSurveyor Live data have not been saved by QRev.',...
                     'Close Request Function',...
                     'Close','Save & Close','Cancel','Cancel');
    else
        selection = 'Close';
    end
    
    % Close as directed
    switch selection
        % If no new data or user says close
        case 'Close'
            delete (handles.output); 
            
        % New data saved and then close   
        case 'Save & Close'
            pbSave_Callback(hObject, eventdata, handles);
            delete (handles.output); 
    end
    
function pbComment_Callback(hObject, eventdata, handles)
    % Add comment
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % Adds comment
    %
    % CALLS:
    % commentButton
    
    % Pause added to fix problem with double clicking the button.
    pause (0.3);
    
    % Call commentButton function
    [handles]=commentButton(handles, 'QRev Main');
    
    % Update handles structure
    guidata(hObject, handles);    

function pbHelp_Callback(hObject, eventdata, handles)
    % Open pdf user's manual
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % Opens pdf file
    %
    % CALLS:
    % open

    % Pause added to fix problem with double clicking the button.
    pause (0.3);
    
    % Open pdf file
    %open('helpFiles\QRev_Users_Manual.pdf')  
    web('QRev_Help_Files\HTML\main_window.htm')
    
function puRating_Callback(hObject, eventdata, handles)
% hObject    handle to puRating (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    meas=getappdata(handles.hMainGui,'measurement');
    val=get(handles.puRating,'Value');
    str=get(handles.puRating,'String');
    meas.userRating=str{val};
    setappdata(handles.hMainGui,'measurement',meas);
    
function pbEDI_Callback(hObject, eventdata, handles)
% hObject    handle to pbEDI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)    

    % Pause added to fix problem with double clicking the button.
    pause (0.3);    
    meas=getappdata(handles.hMainGui,'measurement');
    % Open figure
    hSelectTransect=selectTransect(meas,handles);
        
    % Turn all buttons off so only this figure can be open
    buttonsOff(handles);    

    % Wait for figure to close before proceeding
    waitfor(hSelectTransect);
       
    % Get selected transect index
    transectIdx=getappdata(handles.hMainGui,'EDI_Transect_Index');
    
    % If transect selected retrieve data and open EDI GUI
    if transectIdx>0
        transect=meas.transects(transectIdx);
        discharge=meas.discharge(transectIdx);
        hWinEDI=winEDI(transect,discharge,handles.hMainGui);
        waitfor(hWinEDI);
    end
    
    buttonsOn(handles);
    
function pbUncheck_Callback(hObject, eventdata, handles)
% hObject    handle to pbUncheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Get data from hMainGui
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow; 
    
    meas=getappdata(handles.hMainGui,'measurement');
    checked=[meas.transects.checked];
    idx=find(checked==1);
    two=ones(1,length(idx)).*2;
    idx=[2,idx+2];
    
    % Update table
    handles=updateQ(handles,idx);
    meas=getappdata(handles.hMainGui,'measurement');
     % Update GUI
    plotExtrap(handles,meas);
    qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;    
    
function guiMain_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to guiMain (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
% ALT-u to uncheck all
if strcmp(eventdata.Key,'u') && strcmp(eventdata.Modifier,'alt')
    
    % Get data from hMainGui
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow; 
    
    meas=getappdata(handles.hMainGui,'measurement');
    checked=[meas.transects.checked];
    idx=find(checked==1);
    idx=[2,idx+2];
    
    % Update table
    handles=updateQ(handles,idx);
    meas=getappdata(handles.hMainGui,'measurement');
     % Update GUI
    plotExtrap(handles,meas);
    handles=qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;    

end

if strcmp(eventdata.Key,'s') && strcmp(eventdata.Modifier,'alt')
    saveText=get(handles.pbSave,'String');
    if strcmp(saveText,'Save')
        set(handles.pbSave,'String','Save Checked');
        handles.saveChecked=true;
    else
        set(handles.pbSave,'String','Save');
        handles.saveChecked=false;
    end
end

    if strcmp(eventdata.Key,'b') && strcmp(eventdata.Modifier,'alt')
        % Get file with list of files to be processed
        [fileName,handles.pathName]=uigetfile({'*.txt'},'SELECT FILE FOR PROCESSING');
     
         % Loop through the list of mmt files creating a cell array of filenames
        fullName=[handles.pathName,fileName];
        fid=fopen(fullName);
        n=0;
        readLoop=true;
        while readLoop
            n=n+1;
            lineIn=fgetl(fid);
            if lineIn==-1
                readLoop=false;
            else
                files{n,1}=lineIn;
            end % if lineIn
        end % while readLoop

        % Initialize summary
        row=1;
        col=1;
        table{row,col}='Filename';
        col=col+1;
        table{row,col}='Total';
        col=col+1;
        table{row,col}='Top';
        col=col+1;
        table{row,col}='Middel';
        col=col+1;
        table{row,col}='Bottom';
        col=col+1;
        table{row,col}='Left';
        col=col+1;
        table{row,col}='Right';
        % Process each file
        nFiles=length(files);
        for n=1:nFiles
            fullName=files{n}; 
            fullNamedec=double(fullName);
            fullName=fullName(fullNamedec<127);
            idx=findstr(fullName,'\');
            pathName=fullName(1:idx(end));
            fileName=fullName(idx(end)+1:end);
            checkedOnly=1;
            meas=clsMeasurement(source,fullName,checkedOnly);

            version=handles.version;

            % Get units multiplier
            [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);

            % Create default filename
            t=datetime('now');
            prefix=datestr(t,'yyyymmddHHMMSS');
            matName=[pathName prefix '_QRev.mat'];


            % Automatically add file saved comment.
            username=getenv('USERNAME');
            checked=logical([meas.transects(:).checked]);
            q=clsQAData.meanQ(meas.discharge(checked),'total').*unitsQ;

            qstr=num2str(q,'%13.3f');
            comment={['FileSave ',datestr(t,'mm/dd/yyyy HH:MM:ss'),': ',fileName(1:end-4),' by ',username, ' Q=',qstr,' ',uLabelQ(2:end-1)]};

            meas_struct=clsMeasurement.obj2struct(meas);

            meas=addComment(meas,comment);
          
            % Save Matlab file
            matName=[pathName fileName];
            save(matName,'version','meas_struct');

            % Save the XML file
            ok=clsMeasurement.createXMLFile(meas,version,matName(1:end-4));
            handles.newData=false;
            
            
        end
    end
    
    % Update handles
    guidata(hObject, handles);
    
function optionalClose(hObject,eventdata)
    % Opitonal close for windows x button
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % Terminates the program
    %
    % CALLS:
    % pbClose_Callback
    
    handles=guidata(hObject);
    pbClose_Callback(hObject, eventdata, handles)

function buttonsOff(handles)
    % Function to turn all buttons off
    % 
    % INPUT:
    % handles
    %
    % RESULTS:
    % Turns buttons off
    %
    % CALLS:
    % --
    
    set(handles.tblDetail,'Enable','off');
    set(handles.tblQA,'Enable','off');
    set(handles.pbOptions,'Enable','off');
    set(handles.pbSelDataFiles,'Enable','off');
    set(handles.pbViewInstrument,'Enable','off');
    set(handles.pbSystemTest,'Enable','off');
    set(handles.pbTempChk,'Enable','off');
    set(handles.pbCompassCal,'Enable','off');
    set(handles.pbMovBedTst,'Enable','off');
    set(handles.pbUserInput,'Enable','off');
    set(handles.pbDepthFilters,'Enable','off');
    set(handles.pbBTFilters,'Enable','off');
    set(handles.pbGPSFilters,'Enable','off');
    set(handles.pbWTFilters,'Enable','off');
    set(handles.pbRef,'Enable','off');
    set(handles.pbExtrap,'Enable','off');
    set(handles.pbEdges,'Enable','off');
%     set(handles.pbViewComments,'Enable','off');
    set(handles.pbSave,'Enable','off');
    set(handles.pbClose,'Enable','off');
    
function buttonsOn(handles)
    % Function to turn all buttons on
    % 
    % INPUT:
    % handles
    %
    % RESULTS:
    % Turns buttons on
    %
    % CALLS:
    % --
    
    set(handles.tblDetail,'Enable','on');
    set(handles.tblQA,'Enable','on');
    set(handles.pbOptions,'Enable','on');
    set(handles.pbSelDataFiles,'Enable','on');
    set(handles.pbViewInstrument,'Enable','on');
    set(handles.pbSystemTest,'Enable','on');
    set(handles.pbTempChk,'Enable','on');
    set(handles.pbMovBedTst,'Enable','on');
    set(handles.pbUserInput,'Enable','on');
    set(handles.pbDepthFilters,'Enable','on');
    set(handles.pbBTFilters,'Enable','on');
    set(handles.pbGPSFilters,'Enable','on');
    set(handles.pbWTFilters,'Enable','on');
    set(handles.pbExtrap,'Enable','on');    
    
    % If GPS data are available turn buttons on, if not leave off
    meas=getappdata(handles.hMainGui,'measurement');
    if isempty([meas.transects.gps])
        set(handles.pbGPSFilters,'Enable','off');
        set(handles.pbRef,'Enable','off');
    else
        set(handles.pbGPSFilters,'Enable','on');
        set(handles.pbRef,'Enable','on');
    end
    
    % Check for heading information and enable or disable the
    % compass button
    heading=unique(meas.transects(1).sensors.heading_deg.internal.data);
    if length(heading==1) & heading==0
        set(handles.pbCompassCal,'Enable','off');
    else
        set(handles.pbCompassCal,'Enable','on');
    end
    
    set(handles.pbEdges,'Enable','on');
    set(handles.pbViewComments,'Enable','on');
    set(handles.pbSave,'Enable','on');
    set(handles.pbClose,'Enable','on');
    
function buttonsDefault(handles)
    % Function sets button color to default
    % 
    % INPUT:
    % handles
    %
    % RESULTS:
    % Turns buttons color to default
    %
    % CALLS:
    % --
    buttonColor=handles.defaultColor;
    buttonFontW=handles.defaultFontW;
    buttonFontA=handles.defaultFontA;
    set(handles.pbSelDataFiles,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbViewInstrument,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbSystemTest,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbTempChk,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbCompassCal,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbMovBedTst,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbUserInput,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbDepthFilters,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbBTFilters,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbGPSFilters,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbWTFilters,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbExtrap,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA); 
    set(handles.pbGPSFilters,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbRef,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbEdges,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbViewComments,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbSave,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    set(handles.pbClose,'BackgroundColor',buttonColor,'FontWeight',buttonFontW,'FontAngle',buttonFontA);
    
function setButtonColor(handles,button,status)
    % Function to set button color based on automated qa check
    % 
    % INPUT:
    % handles
    %
    % RESULTS:
    % Sets buttons color
    %
    % CALLS:
    % --
    
    % Enable button
    set(handles.(button),'Enable','on');
    
    % Set color
    switch status
        case 'good'
            set(handles.(button),'BackgroundColor',handles.goodColor,'FontWeight',handles.goodFontW,'FontAngle',handles.goodFontA);
        case 'caution'
            set(handles.(button),'BackgroundColor',handles.cautionColor,'FontWeight',handles.cautionFontW,'FontAngle',handles.cautionFontA);
        case 'warning'
            set(handles.(button),'BackgroundColor',handles.warningColor,'FontWeight',handles.warningFontW,'FontAngle',handles.warningFontA);
        case 'default'
            set(handles.(button),'BackgroundColor',handles.defaultColor,'FontWeight',handles.defaultFontW,'FontAngle',handles.defaultFontA);    
        case 'inactive'
            set(handles.(button),'BackgroundColor',handles.defaultColor,'FontWeight',handles.defaultFontW,'FontAngle',handles.defaultFontA);    
            set(handles.(button),'Enable','off');
    end % switch    
    
function clearGUI(handles)
    % Clear and reset GUI
    % 
    % INPUT:
    % handles
    %
    % RESULTS:
    % Resets GUI
    %
    % CALLS:
    % --
    
    % Get table data
    QATableData=get(handles.tblQA,'Data');
    DetTableData=detailTableLabels(handles);
    
    % Clear details table
    DetTableData(:,2:end)={' '};
    set(handles.tblDetail,'Data',DetTableData);
    
    % Clear uncertainty table
    set(handles.tblQA,'Data',handles.ClearQATableData);
    
    % Clear summary table
    sumTableData1=get(handles.tblSum1,'Data');
    sumTableData1(:,2)={' '};
    sumTableData2=get(handles.tblSum2,'Data');
    sumTableData2(:,2)={' '};
    set(handles.tblSum1,'Data',sumTableData1);
    set(handles.tblSum2,'Data',sumTableData2);
    
    % Clear messages
    set(handles.tblMsg,'Data','');
    
    % Clear User Rating
    set(handles.puRating,'Value',1);
    
    % Clear extrapolation figure
    cla(handles.axExtrap);
    
    % Set buttons to default
    buttonsDefault(handles);

% ===============    
% Table functions
% ===============

function tblDetail_CellEditCallback(hObject, eventdata, handles)
    % Handles selection of transects to include in the measurement.
    % 
    % INPUT:
    % hObject
    % eventdata
    % handles
    %
    % RESULTS:
    % meas maybe modified
    % discharge maybe modified
    %
    % CALLS:
    % updateQ
    % qaAlerts
    % plotExtrap
    
    % Get data from main
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Set pointer to busy
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow; 
    
    % Determine row selected
    selection = eventdata.Indices;
    if selection(1)==2
        % Get table data
        tableData=get(handles.tblDetail,'Data'); 
        % Update table
        handles=updateQ(handles,selection);
    else
        tableData=getappdata(handles.hMainGui,'detailTable');
        set(handles.tblDetail,'Data',tableData);
    end % if selection
    
    % Update GUI
    meas=getappdata(handles.hMainGui,'measurement');
    plotExtrap(handles,meas);
    handles=qaAlerts(handles);
    
    % Set pointer to arrow
    set(gcf, 'pointer', 'arrow');     
    drawnow;    
    
    % Update handles
    guidata(hObject, handles);
    
function [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=unitsMultiplier(handles)
% Computes the units conversion from SI units used internally to the
% desired display units

    % Get disply units
    displayUnits=getappdata(handles.hMainGui,'Units');
    
    % Set labels and conversions for SI
    if strcmp(displayUnits,'SI')
        unitsL=1;
        unitsQ=1;
        unitsA=1;
        unitsV=1;
        uLabelL='(m)';
        uLabelQ='(m3/s)';
        uLabelA='(m2)';
        uLabelV='(m/s)';
    % Set labels and conversions for English
    else
        unitsL=1./0.3048;
        unitsQ=unitsL.^3;
        unitsA=unitsL.^2;
        unitsV=unitsL;
        uLabelL='(ft)';
        uLabelQ='(ft3/s)';
        uLabelA='(ft2)';
        uLabelV='(ft/s)';
    end   
   
function handles=updateQ(handles,varargin)
    % Computes Q and updates tables
    % 
    % INPUT:
    % handles
    % varargin: column selected from details table
    %
    % RESULTS:
    % meas maybe modified
    % discharge maybe modified
    %
    % CALLS:
    % updateExtrap
    % clsQComp
    % clsTransectData.computeCharacteristics
    % qaAlerts
    % plotExtrap
    
    % Get current data from GUI
    startCol=handles.startCol;
    startRow=handles.startRow;
    DetTableData=detailTableLabels(handles);
    tableLabels=detailTableLabels(handles);
    QATableData=get(handles.tblQA,'Data');
    
    % Get data from main
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=unitsMultiplier(handles);
    
    % If varargin has selection update meas
    if ~isempty(varargin)
        selection=varargin{1};
        cols=selection(2:end);
        numSelected=length(cols);
        for nsel=1:numSelected
            s=cols(nsel)-2;
            meas.transects(s)=changeSelection(meas.transects(s));
        end  % for nsel
            meas.extrapFit=clsComputeExtrap(meas.transects);
            meas=changeExtrapolation(meas);
            meas.discharge=clsQComp(meas);
            meas.uncertainty=clsUncertainty(meas);
            % Update user uncertainty in measurement data
            if ~isempty(QATableData{1,3})
                meas.uncertainty=addUserInput(meas.uncertainty,'cov95User',str2double(QATableData{1,3}));
            end
            if ~isempty(QATableData{2,3})
                meas.uncertainty=addUserInput(meas.uncertainty,'invalid95User',str2double(QATableData{2,3}));
            end
            if ~isempty(QATableData{3,3})
                meas.uncertainty=addUserInput(meas.uncertainty,'edges95User',str2double(QATableData{3,3}));
            end
            if ~isempty(QATableData{4,3})
                meas.uncertainty=addUserInput(meas.uncertainty,'extrapolation95User',str2double(QATableData{4,3}));
            end
            if ~isempty(QATableData{5,3})
                meas.uncertainty=addUserInput(meas.uncertainty,'movingBed95User',str2double(QATableData{5,3}));
            end
            if ~isempty(QATableData{6,3})
                meas.uncertainty=addUserInput(meas.uncertainty,'systematicUser',str2double(QATableData{6,3}));
            end
            
            meas.qa=clsQAData(meas);
            setappdata(handles.hMainGui,'measurement',meas);
    end % if isempty
    
    setappdata(handles.hMainGui,'source',meas.transects(1).adcp.manufacturer);
    
    % Compute characteristics
    [width,widthCOV,area,areaCOV,avgBoatSpeed,avgBoatCourse,avgWaterSpeed,avgWaterDir,meanDepth,maxDepth,maxWaterSpeed]=clsTransectData.computeCharacteristics(meas.transects,meas.discharge);
    
    % Initialize variables for adding data to tables
    nTransects=length(meas.transects);
    DetTableCol{1,1}='PARAMETERS';
    DetTableCol{1,2}='MEASUREMENT';
    startCol=handles.startCol;
    startRow=handles.startRow;
    
    % Add transect data to details table
    for n=1:nTransects
        
        % Use last 16 characters of fileName for column label
%         idx=find(meas.transects(n).fileName=='\',1,'last');
%         if isempty(idx)
%             idx=0;
%         end % if isempty
%         if (length(meas.transects(n).fileName)-idx)> 16
        if length(meas.transects(n).fileName)> 16
            idx=length(meas.transects(n).fileName)-16;
        else
            idx=0;
        end % if length
        DetTableCol{1,startCol+n}=meas.transects(n).fileName(idx+1:end-4);
        
        % Add data to rows and columns
        k=startRow;
        DetTableData{k,startCol+n}=logical(meas.transects(n).checked);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('<html><b>% 12.3f',meas.discharge(n).total.*unitsQ);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.3f',meas.discharge(n).top.*unitsQ);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.3f',meas.discharge(n).middle.*unitsQ);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.3f',meas.discharge(n).bottom.*unitsQ);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.3f',meas.discharge(n).left.*unitsQ);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.3f',meas.discharge(n).right.*unitsQ);
        
        k=k+2;
        DetTableData{k,startCol+n}=sprintf('% 12.1f',meas.transects(n).dateTime.transectDuration_sec);
        
        k=k+1;
        if strcmpi(meas.transects(n).startEdge,'Left')
            edge=' L';
        else
            edge=' R';
        end % if left
        DetTableData{k,startCol+n}=sprintf('% 12s',[datestr(meas.transects(n).dateTime.startSerialTime,'HH:MM:SS'),edge]);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 10s',datestr(meas.transects(n).dateTime.endSerialTime,'HH:MM:SS'));
        
        k=k+2;        
        switch meas.transects(n).boatVel.selected
            case 'btVel'
                ref='BT';
            case 'ggaVel'
                ref='GGA';
            case 'vtgVel'
                ref='VTG';
        end % switch
        DetTableData{k,startCol+n}=sprintf('% 8s',ref);
        
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 8s',meas.transects(n).boatVel.composite);
        
        k=k+1;
        switch meas.transects(n).depths.selected
            case 'btDepths'
                ref='BT';
            case 'vbDepths'
                ref='VB';
            case 'dsDepth'
                ref='DS';
        end % switch
        DetTableData{k,startCol+n}=sprintf('% 8s',ref);
              
        k=k+5;
        DetTableData{k,startCol+n}=sprintf('% 12.1f',width(n).*unitsL);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.1f',area(n).*unitsA);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.2f',avgBoatSpeed(n).*unitsV);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.1f',avgBoatCourse(n));
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.2f',avgWaterSpeed(n).*unitsV);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.2f',avgWaterDir(n));     
        k=k+2;
        DetTableData{k,startCol+n}=sprintf('% 12.2f',meas.transects(n).depths.(meas.transects(n).depths.selected).draftUse_m.*unitsL);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.2f',meas.transects(n).wVel.excludedDist.*unitsL);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12s',meas.transects(n).startEdge);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.1f',meas.transects(n).sensors.heading_deg.(meas.transects(n).sensors.heading_deg.selected).magVar_deg);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12s',meas.transects(n).extrap.topMethod);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12s',meas.transects(n).extrap.botMethod);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.4f',meas.transects(n).extrap.exponent);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.2f',meas.transects(n).edges.left.dist_m.*unitsL);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12.2f',meas.transects(n).edges.right.dist_m.*unitsL);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12s',meas.transects(n).edges.left.type);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12s',meas.transects(n).edges.right.type);
        
        k=k+11;
        perInvalid=(nansum(~meas.transects(n).depths.(meas.transects(n).depths.selected).validData(1,:))./size(meas.transects(n).depths.(meas.transects(n).depths.selected).validData(1,:),2)).*100;
        DetTableData{k,startCol+n}=sprintf('% 12.0f',perInvalid);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12s',meas.transects(n).depths.(meas.transects(n).depths.selected).interpType);
        
        k=k+2;
        
        % Bottom track
        perInvalid=(nansum(~meas.transects(n).boatVel.btVel.validData(1,:))./size(meas.transects(n).boatVel.btVel.validData(1,:),2)).*100;
        DetTableData{k,startCol+n}=sprintf('% 12.1f',perInvalid);
        
        k=k+1;
        filter='';
        if meas.transects(n).boatVel.btVel.beamFilter==4
            filter(end+1)='B';
        end % if beamFilter
        if ~strcmp(meas.transects(n).boatVel.btVel.dFilter,'Off')
            filter(end+1)='E';
        end % if error velocity
        if ~strcmp(meas.transects(n).boatVel.btVel.wFilter,'Off')
            filter(end+1)='V';
        end % if vertical velocity
        if ~strcmp(meas.transects(n).boatVel.btVel.smoothFilter,'Off')
            filter(end+1)='S';
        end % if smooth
        if length(filter)<1
            filter='None';
        end % if none       
        DetTableData{k,startCol+n}=sprintf('% 12s',filter);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12s',meas.transects(n).boatVel.btVel.interpolate);
        
        % GGA
        if ~isempty(meas.transects(n).gps)
            if ~isempty(meas.transects(n).boatVel.ggaVel)
                k=k+1;
                perInvalid=(nansum(~meas.transects(n).boatVel.ggaVel.validData(1,:))./size(meas.transects(n).boatVel.ggaVel.validData(1,:),2)).*100;
                DetTableData{k,startCol+n}=sprintf('% 12.1f',perInvalid);

                filter=num2str(meas.transects(n).boatVel.ggaVel.gpsDiffQualFilter);
                if ~strcmp(meas.transects(n).boatVel.ggaVel.gpsAltitudeFilter,'Off')
                    filter(end+1)='A';
                end % if altitude filter
                if ~strcmp(meas.transects(n).boatVel.ggaVel.gpsHDOPFilter,'Off')
                    filter(end+1)='H';
                end % if HDOP filter
                if ~strcmp(meas.transects(n).boatVel.ggaVel.smoothFilter,'Off')
                    filter(end+1)='S';
                end % if smooth filter
                if length(filter)<1
                    filter='None';
                end % if none         
                k=k+1;
                DetTableData{k,startCol+n}=sprintf('% 12s',filter);
                k=k+1;
                DetTableData{k,startCol+n}=sprintf('% 12s',meas.transects(n).boatVel.ggaVel.interpolate);
            else
                k=k+3;
            end % if gga
            
            % VTG
            if ~isempty(meas.transects(n).boatVel.vtgVel)
                k=k+1;
                perInvalid=(nansum(~meas.transects(n).boatVel.vtgVel.validData(1,:))./size(meas.transects(n).boatVel.vtgVel.validData(1,:),2)).*100;
                DetTableData{k,startCol+n}=sprintf('% 12.1f',nansum(~meas.transects(n).boatVel.vtgVel.validData(1,:)));

                filter=num2str(meas.transects(n).boatVel.vtgVel.gpsDiffQualFilter);
                if ~strcmp(meas.transects(n).boatVel.vtgVel.gpsAltitudeFilter,'Off')
                    filter(end+1)='A';
                end % if differential
                if ~strcmp(meas.transects(n).boatVel.vtgVel.gpsHDOPFilter,'Off')
                    filter(end+1)='H';
                end % if HDOP filter
                if ~strcmp(meas.transects(n).boatVel.vtgVel.smoothFilter,'Off')
                    filter(end+1)='S';
                end % if smooth
                if length(filter)<1
                    filter='None';
                end % if none
                k=k+1;
                DetTableData{k,startCol+n}=sprintf('% 12s',filter);
                k=k+1;
                DetTableData{k,startCol+n}=sprintf('% 12s',meas.transects(n).boatVel.vtgVel.interpolate);
            else
                k=k+3;
            end % if vtg
        else
            k=k+6;
        end % if gps
        k=k+1;
        
        % Water track
         filter='';
        if meas.transects(n).wVel.beamFilter==4
            filter(end+1)='B';
        end % if beam filter
        if ~strcmp(meas.transects(n).wVel.dFilter,'Off')
            filter(end+1)='E';
        end % if error velocity
        if ~strcmp(meas.transects(n).wVel.wFilter,'Off')
            filter(end+1)='V';
        end % if vertical velocity
        if ~strcmp(meas.transects(n).wVel.smoothFilter,'Off')
            filter(end+1)='S';
        end % if smooth
        if length(filter)<1
            filter='None';
        end % if none
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12s',filter);
        k=k+1;
        DetTableData{k,startCol+n}=sprintf('% 12s',meas.transects(n).wVel.interpolateEns);
    end % for n
    
    % Add measurement summary data to details table
    checked=logical([meas.transects(:).checked]);
    idxChecked=find(checked==true);
    
    % Summary statistics include only checked transects
    if ~isempty(idxChecked)
        
        % Mean discharge values
        k=startRow+1;
        DetTableData{k,startCol}=sprintf('<html><b>% 12.3f',clsQAData.meanQ(meas.discharge(checked),'total').*unitsQ);
        DetTableData{k,1}=[tableLabels{k,1},' ',uLabelQ];
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12.3f',clsQAData.meanQ(meas.discharge(checked),'top').*unitsQ);
        DetTableData{k,1}=[tableLabels{k,1},' ',uLabelQ];
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12.3f',clsQAData.meanQ(meas.discharge(checked),'middle').*unitsQ);
        DetTableData{k,1}=[tableLabels{k,1},' ',uLabelQ];
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12.3f',clsQAData.meanQ(meas.discharge(checked),'bottom').*unitsQ);
        DetTableData{k,1}=[tableLabels{k,1},' ',uLabelQ];
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12.3f',clsQAData.meanQ(meas.discharge(checked),'left').*unitsQ);
        DetTableData{k,1}=[tableLabels{k,1},' ',uLabelQ];
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12.3f',clsQAData.meanQ(meas.discharge(checked),'right').*unitsQ);
        DetTableData{k,1}=[tableLabels{k,1},' ',uLabelQ];
        k=k+2;
        
        % Compute total dureation
        temp=meas.transects(checked);
        temp=[temp(:).dateTime];
        totalDuration=nansum([temp(:).transectDuration_sec]);
        DetTableData{k,startCol}= sprintf('% 12.1f',totalDuration);
        k=k+1;
        
        % Determine start time
        idx=find(checked==true,1,'first');
        DetTableData{k,startCol}=sprintf('% 12s',datestr(meas.transects(idx).dateTime.startSerialTime,'HH:MM:SS'));
        k=k+1;
        
        % Determine end time
        idx=find(checked==true,1,'last');
        DetTableData{k,startCol}=sprintf('% 12s',datestr(meas.transects(idx).dateTime.endSerialTime,'HH:MM:SS'));
        
        k=k+5;
        
        % Check for moving-bed tests
        if isempty(meas.mbTests)
            mb='Unknown';
            correction='N/A';
        else
            mbData=meas.mbTests;
            validData=find([mbData.selected]==1);
            movingBed=[mbData(validData).movingBed];

            idx=strfind(movingBed,'Yes');
            if isempty(idx)
                mb='No';
                correction='No';
            else
                mb='Yes';          
                use2Correct=[meas.mbTests.use2Correct];

                if any(use2Correct)
                    correction='Yes';
                else
                    correction='No';
                end % if use2Correct
            end %if isempty
        end % if moving-bed tests
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 10s',mb);
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 10s',correction);
        
        k=k+2;
        
        % Measurement characteristics
        DetTableData{k,startCol}=sprintf('% 12.1f',width(nTransects+1).*unitsL);
        DetTableData{k,1}=[tableLabels{k,1},' ',uLabelL];
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12.1f',area(nTransects+1).*unitsA);
        DetTableData{k,1}=[tableLabels{k,1},' ',uLabelA];
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12.2f',avgBoatSpeed(nTransects+1).*unitsV);
        DetTableData{k,1}=[tableLabels{k,1},' ',uLabelV];
        k=k+1;
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12.2f',avgWaterSpeed(nTransects+1).*unitsV);
        DetTableData{k,1}=[tableLabels{k,1},' ',uLabelV];
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12.2f',avgWaterDir(nTransects+1)); 
        
        k=k+2;
        
        % Consistency checks
        [topChk,botChk,expChk,navRefChk,leftChk,rightChk,draftChk,magvarChk]=clsTransectData.checkConsistency(meas.transects);
        if isnumeric(draftChk)
            DetTableData{k,startCol}=sprintf('% 12.2f',draftChk.*unitsL);
            DetTableData{k,1}=[tableLabels{k,1},' ',uLabelL];
        else
            DetTableData{k,startCol}=sprintf('% 12s',draftChk);
        end % if draftChk
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12.2f',meas.transects(1).wVel.excludedDist.*unitsL);
        DetTableData{k,1}=[tableLabels{k,1},' ',uLabelL];
        k=k+2;
        if isnumeric(magvarChk)
            DetTableData{k,startCol}=sprintf('% 12.1f',magvarChk); 
        else
            DetTableData{k,startCol}=sprintf('% 12s',magvarChk);
        end % if magvarChk
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12s',topChk);
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12s',botChk);
        k=k+1;
        if isnumeric(expChk)
            DetTableData{k,startCol}=sprintf('% 12.4f',expChk);
        else
            DetTableData{k,startCol}=sprintf('% 12s',expChk);
        end % if expChk
        k=k+1;
        DetTableData{k,1}=[tableLabels{k,1},' ',uLabelL];
        k=k+1;
        DetTableData{k,1}=[tableLabels{k,1},' ',uLabelL];
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12s',leftChk);
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12s',rightChk);
        
        k=k+1;
        
        % Pre-measurement quality checks
        if isempty(meas.sysTest(1).timeStamp)
            result='No';
        else
            result='Yes';
        end % if sysTest
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12s',result);
        k=k+1;
        nFailedTests=getappdata(handles.hMainGui,'FailedSystemTests');
        DetTableData{k,startCol}=sprintf('% 12.0f',nFailedTests);
        
        if isempty(meas.compassCal(1).timeStamp)
            result='No';
        else
            result='Yes';
        end % if compassCal
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12s',result);
        
        if isempty(meas.compassEval(1).timeStamp)
            result='No';
        else
            result='Yes';
        end % if compassEval
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12s',result);
        
        if ~isempty(meas.mbTests)
            testQuality={meas.mbTests.testQuality};
            mb={meas.mbTests.movingBed};
            if sum(strcmp('Good',testQuality))>0
                mbt='Yes';
            elseif sum(strcmp('Warnings',testQuality))>0
                mbt='Warnings';
            elseif sum(strcmp('Errors',testQuality))>0
                mbt='Errors';
            elseif strcmp(testQuality{1},'Valid')
                mbt='Yes';
            elseif sum(strcmp('Manual',testQuality))>0
                mbt='Manual';
            else
                mbt='Errors';
            end % if testQuality
        else
            mbt='None';
        end % if moving-bed tests
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12s',mbt);
        
        if ~strcmp(mbt,'None')
            if sum(strcmp('Yes',mb))>0
                mbResult='Yes';
            else
                mbResult='No';
            end % if moving-bed
        else
            mbResult='Unknown';
        end % if moving-bed tests
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12s',mbResult);
        
        if isempty(meas.extTempChk.user)
            k=k+1;
            DetTableData{k,startCol}=sprintf('% 12s','None');
        else
            k=k+1;
            DetTableData{k,startCol}=sprintf('% 12.1f',meas.extTempChk.user);
        end % if extTempChk
        
        temp=[];
        for n=1:nTransects
            temp=[temp, meas.transects(n).sensors.temperature_degC.(meas.transects(n).sensors.temperature_degC.selected).data];
        end % for n
        range=nanmax(temp)-nanmin(temp);
        k=k+1;
        DetTableData{k,startCol}=sprintf('% 12.1f',range);
        
        %
        % tblSummary
        % ------
        SumTableData1{1,1}='Q:';
        if meas.uncertainty.cov==0
            SumTableData1{1,2}=sprintf('     --  ');
        else             
            SumTableData1{1,2}=sprintf('% 10.2f',meas.uncertainty.cov);
        end
        
        SumTableData1{2,1}='Width:';
        if widthCOV==0
            SumTableData1{2,2}=sprintf('     --  ');
        else
            SumTableData1{2,2}=sprintf('% 10.2f',widthCOV);
        end
        
        SumTableData1{3,1}='Area:';
        if areaCOV==0
            SumTableData1{3,2}=sprintf('     --  ');
        else
            SumTableData1{3,2}=sprintf('% 10.2f',areaCOV);
        end
        
        SumTableData2{1,1}='Left/Right Edge:';
        meanQ=clsQAData.meanQ(meas.discharge(checked),'total');
        meanLeft=clsQAData.meanQ(meas.discharge(checked),'left');
        perLeft=(meanLeft./meanQ).*100; 
        meanRight=clsQAData.meanQ(meas.discharge(checked),'right');
        perRight=(meanRight./meanQ).*100;
        SumTableData2{1,2}=sprintf('% 6.2f / % 6.2f',perLeft,perRight);
        
        SumTableData2{2,1}='Invalid Cells:';
        perCells=(clsQAData.meanQ(meas.discharge(checked),'intCells')./meanQ).*100;
        SumTableData2{2,2}=sprintf('% 12.2f',perCells);
        
        SumTableData2{3,1}='Invalid Ens:';
        perEns=(clsQAData.meanQ(meas.discharge(checked),'intEns')./meanQ).*100;
        SumTableData2{3,2}=sprintf('% 12.2f',perEns);
       
        % QA Table
        % --------
        if isnan(meas.uncertainty.cov95)
            QATableData{1,2}=sprintf('         --  ');
        else
            QATableData{1,2}=sprintf('% 12.1f',meas.uncertainty.cov95); 
        end
        QATableData{2,2}=sprintf('% 12.1f',meas.uncertainty.invalid95);
        QATableData{3,2}=sprintf('% 12.1f',meas.uncertainty.edges95);
        QATableData{4,2}=sprintf('% 12.1f',meas.uncertainty.extrapolation95);
        QATableData{5,2}=sprintf('% 12.1f',meas.uncertainty.movingBed95);
        QATableData{6,2}=sprintf('% 12.1f',meas.uncertainty.systematic);
        if isnan(meas.uncertainty.total95)
            QATableData{7,2}=sprintf('         --  ');
        else            
            QATableData{7,2}=sprintf('% 12.1f',meas.uncertainty.total95);
        end
        if isnan(meas.uncertainty.total95User)
            QATableData{7,3}=sprintf('         --  ');
        else 
            QATableData{7,3}=sprintf('% 12.1f',meas.uncertainty.total95User);
        end
        
    else
        QATableData(:,2)={' '};
        DetTableData(:,2)={' '};
        SumTableData1={' '};
        SumTableData2={' '};
    end % if checked
    
    % Store table data
    setappdata(handles.hMainGui,'detailTable',DetTableData);
    setappdata(handles.hMainGui,'qaTable',QATableData);
    
    % Update GUI with table data
    [DetTableData,QATableData]=applyFormat(DetTableData,QATableData,meas);
    edit=true(1,nTransects+2);
    edit(1,1:2)=false;
    set(handles.tblDetail,'ColumnEditable',edit);
    set(handles.tblQA,'ColumnEditable',[false,false,true]);
    set(handles.tblDetail,'ColumnName',DetTableCol);
    set(handles.tblQA,'Data',QATableData);
    set(handles.tblDetail,'Data',DetTableData);
    set(handles.tblSum1,'Data',SumTableData1);
    set(handles.tblSum2,'Data',SumTableData2);
    
    % Update user rating
    if isempty(meas.userRating)
        meas.userRating='Not Rated';
        setappdata(handles.hMainGui,'measurement',meas);
    end
    switch meas.userRating
        case 'Not Rated'
            handleValue=1;
        case 'Excellent (< 2%)'
            handleValue=2;
        case 'Good (2 - 5%)'
            handleValue=3;
        case 'Fair (5 - 8%)'
            handleValue=4;
        case 'Poor (> 8%)'
            handleValue=5;
    end
    set(handles.puRating,'Value',handleValue);
    drawnow

function [DetTableData,QATableData]=applyFormat(DetTableData,QATableData,meas)
% Applys a format to the table that highlights values that have generated
% warnings or caution about the quality of the data and its affect on Q.
%
% INPUT:
%
% DetTableData: data for details table
%
% QATableData: data for QA table
%
% meas: object of clsMeasurement
%
% OUTPUT:
%
% DetTableData: formatted data for details table
%
% QATableData: formatted data for QA table      
    % Check for compatibility with older files
    if isfield(meas.qa.transects,'uncertainty')
        % QA for 2 transect measurements
        if meas.qa.transects.uncertainty==1
            QATableData(1,2)={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(QATableData{1,2}),'</center></BODY></HTML>']};
        end

        % QA for duration
        if meas.qa.transects.duration==1
            DetTableData(10,2)={['<HTML><BODY bgColor="yellow" width="50px"><center>',num2str(DetTableData{10,2}),'</center></BODY></HTML>']};
        end

        % QA for number of checked transects
        if meas.qa.transects.number<0
            DetTableData(2,1)={['<HTML><BODY bgColor="red" width="50px"><strong><center>',DetTableData{2,1},'</center></strong></BODY></HTML>']};
        elseif meas.qa.transects.number==1
            DetTableData(2,1)={['<HTML><BODY bgColor="yellow" width="50px"><center>',DetTableData{2,1},'</center></BODY></HTML>']};
%         else
%             DetTableData{2,1}='Use';
        end

        % QA for reciprocal transects
        if meas.qa.transects.recip==2
            idx1=strfind(DetTableData{2,1},'<center>')+8;
            idx2=strfind(DetTableData{2,1},'</')-1;
            if isempty(idx1)
                temp=DetTableData{2,1};
            else
                temp=DetTableData{2,1}(idx1:idx2);
            end
             DetTableData(2,1)={['<HTML><BODY bgColor="red" width="50px"><strong><center>',temp,'</center></strong></BODY></HTML>']};
%         else
%             DetTableData{2,1}='Use';
        end

        % QA for consistent sign of total discharge
        if meas.qa.transects.sign==2
            nTransects=length(meas.transects);
            for n=1:nTransects
                DetTableData(3,n+2)={['<HTML><BODY bgColor="red" width="50px"><strong><center>',num2str(DetTableData{3,n+2}),'</center></strong></BODY></HTML>']};
            end
        end
    end
    
function tblQA_CellEditCallback(hObject, eventdata, handles)
% Handles selection of transects to include in the measurement.
    % 
    % INPUT:
    % hObject    handle to tblQA (see GCBO)
    % eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
    %	Indices: row and column indices of the cell(s) edited
    %	PreviousData: previous data for the cell(s) edited
    %	EditData: string(s) entered by the user
    %	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
    %	Error: error string when failed to convert EditData to appropriate value for Data
    % handles    structure with handles and user data (see GUIDATA)
    %
    % RESULTS:
    % meas maybe modified
    %
    % CALLS:
    % addUserInput

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');

    % Get data from table
    tableData=get(handles.tblQA,'Data');

    % Add new user input to table
    if length(eventdata.EditData)>0
        tableData{eventdata.Indices(1),eventdata.Indices(2)}=sprintf('%12.1f',str2double(eventdata.EditData));
    else
        tableData{eventdata.Indices(1),eventdata.Indices(2)}=[];
    end % if

    % Update user uncertainty in measurement data
    switch eventdata.Indices(1)
        case 1
            meas.uncertainty=addUserInput(meas.uncertainty,'cov95User',str2double(tableData(eventdata.Indices(1),3)));
            handles = commentButton( handles, 'Justification for User Provided Uncertainty' );
        case 2
            meas.uncertainty=addUserInput(meas.uncertainty,'invalid95User',str2double(tableData(eventdata.Indices(1),3)));
            handles = commentButton( handles, 'Justification for User Provided Uncertainty' );
        case 3
            meas.uncertainty=addUserInput(meas.uncertainty,'edges95User',str2double(tableData(eventdata.Indices(1),3)));
            handles = commentButton( handles, 'Justification for User Provided Uncertainty' );
        case 4
            meas.uncertainty=addUserInput(meas.uncertainty,'extrapolation95User',str2double(tableData(eventdata.Indices(1),3)));
            handles = commentButton( handles, 'Justification for User Provided Uncertainty' );
        case 5
            meas.uncertainty=addUserInput(meas.uncertainty,'movingBed95User',str2double(tableData(eventdata.Indices(1),3)));
            handles = commentButton( handles, 'Justification for User Provided Uncertainty' );
        case 6
            meas.uncertainty=addUserInput(meas.uncertainty,'systematicUser',str2double(tableData(eventdata.Indices(1),3)));
            handles = commentButton( handles, 'Justification for User Provided Uncertainty' );
    end % switch
    
    % Update total uncertainty in table
    if isnan(meas.uncertainty.total95User)
        tableData{7,3}=sprintf('         --  ');
    else
        tableData{7,3}=sprintf('%12.1f',meas.uncertainty.total95User);
    end
    
    % Store data
    set(handles.tblQA,'Data',tableData);
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Update handles
    guidata(hObject, handles);

% ============================    
% Extrapolation plot functions
% ============================

function plotExtrap(handles,meas)
    % Plots the extrapolation showing only the measurement medians and the
    % current fit
    % 
    % INPUT:
    % handles
    % meas
    %
    % RESULTS:
    % axExtrap gets plotted
    %
    % CALLS:
    % plotMed
    % plotfit
    % plotScale
    % plotLabel
    
    % Clear figure
    cla(handles.axExtrap)
    
    % Identify valid data
    valid=~isnan(meas.extrapFit.normData(end).unitNormalized);
    
    % Only call plot if data are valid
    if sum(sum(valid))>0
        % Plot measurement median points
        plotMed(meas.extrapFit.normData(end),handles.axExtrap);
        % Plot composite profile fit
        plotfit(meas.extrapFit.selFit(end),handles.axExtrap);
        % Plot scale and labels
        plotScale(meas.extrapFit.normData(end),handles.axExtrap);
        plotLabel(handles.axExtrap);  
    end
    
function plotMed (normData,h)
    % plots the median values as squares and horizontal lines between
    % the 25 and 75 percentiles for each transect in normData.
    % 
    % INPUT:
    % normData - clsNormData
    % h - handle for figure
    % type - data type (discharge or velocity)
    %
    % RESULTS:
    % data plotted to h
    %
    % CALLS:
    % --

    % Assign variables from normData
    unitNormMed=normData.unitNormalizedMed;
    avgz=normData.unitNormalizedz;
    unitNormNo=normData.unitNormalizedNo;
    unit25=normData.unitNormalized25;
    unit75=normData.unitNormalized75;
    datatype=normData.dataType;
    validData=normData.validData;

    % Plot all median values in red
    hold (h,'on')
    plot(h,unitNormMed,avgz,'sr','MarkerFaceColor','r')

    % Plot innerquartile range bars around medians in red
    for j=1:20
        hold (h,'on')
        plot(h,[unit25(j) unit75(j)],[avgz(j) avgz(j)],'-r')
    end % for j

    % Plot combined median values that meet in black
    hold (h,'on')
    plot(h,unitNormMed(validData),avgz(validData),'sk','MarkerFaceColor','k')

    % Plot innerquartile range bars around median values that meet criteria
    % in blue
    for k=1:length(validData)
        hold (h,'on')
        plot(h,[unit25(validData(k)) unit75(validData(k))],[avgz(validData(k)) avgz(validData(k))],'-k','LineWidth',2)
    end % for k
  
    hold (h,'off')

function plotScale (normData,h)
    % Sets the scale for the plot based on the data plotted.
    % 
    % INPUT:
    % normData - clsNormData
    % h - handle for figure
    %
    % RESULTS:
    % sets scale limits on h
    %
    % CALLS:
    % --
    
    % Compute the overall minimum value of the 25 percentile and
    % the overall maximum value of the 75 percentile.
    minavgall=nan(length(normData),1);
    maxavgall=nan(length(normData),1);
    minavgall=nanmin(normData.unitNormalized25(normData.validData));
    maxavgall=nanmax(normData.unitNormalized75(normData.validData));

    % Use percentiles to set axes limits
    minavg=min(minavgall);
    maxavg=max(maxavgall);
    if minavg>0 && maxavg>0
        minavg=0;
        lower=0;
    else
        lower=minavg.*1.2;
    end % if
    if maxavg<0 && minavg<0
        upper=0;
    else
        upper=maxavg.*1.2;
    end % if

    % Scale axes
    ylim(h,[0 1]);
    xlim(h,[lower upper]);
    box (h,'on')
    grid(h,'On')

function plotLabel (h)
    % Label axes based on type of data plotted.
    % 
    % INPUT:
    % normData - clsNormData
    % h - handle for figure
    % type - data type (discharge or velocity)
    %
    % RESULTS:
    % set axis lables on h
    %
    % CALLS:
    % --
    
    xlabel(h,'Normalized Unit Q','FontSize',10)
    ylabel(h,'Normalized Distance from Streambed','FontSize',10);

function plotfit(fitData,h)
    % Plots composite of multiple transects as a heavy black line.
    % 
    % INPUT:
    % fitData - clsFitData
    % h - handle for figure
    %
    % RESULTS:
    % data plotted to h
    %
    % CALLS:
    % --

    hold (h,'on')
    
    % Plot automatically selected fit
    h1=plot(h,fitData.uAuto,fitData.zAuto,'-g','LineWidth',3);
    
    % Plot fit selected for discharge computation
    h2=plot(h,fitData.u,fitData.z,'-k','LineWidth',2); 
    
    % Legend
    if ~isempty(h1)
        legend([h1,h2],'Auto','Selected','Location','NorthWest');
    end

    hold (h,'off')    
  
% =========    
% QA Alerts
% =========
function handles=qaAlerts(handles)
    % Apply QA alert colors to buttons
    % 
    % INPUT:
    % handles
    %
    % RESULTS:
    % messages maybe created/modified
    % button colors maybe changed
    %
    % CALLS:
    % qaTransects
    % qaSystemTest
    % qaCompassCal
    % qaTemperature
    % qaMovingBedTest
    % qaUserInput
    % qaExtrapolation
    % updateMessages
    
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    % Set button colors
    setButtonColor(handles,'pbSelDataFiles',meas.qa.transects.status);
    setButtonColor(handles,'pbSystemTest',meas.qa.systemTest.status);
    setButtonColor(handles,'pbCompassCal',meas.qa.compass.status);
    setButtonColor(handles,'pbTempChk',meas.qa.temperature.status);
    setButtonColor(handles,'pbMovBedTst',meas.qa.movingbed.status);
    setButtonColor(handles,'pbViewInstrument',meas.qa.user.status);
    setButtonColor(handles,'pbDepthFilters',meas.qa.depths.status);
    setButtonColor(handles,'pbBTFilters',meas.qa.btVel.status);
    if ~isempty(strfind([meas.qa.ggaVel.status,meas.qa.vtgVel.status],'warning'))
        status='warning';
    elseif ~isempty(strfind([meas.qa.ggaVel.status,meas.qa.vtgVel.status],'caution'))
        status='caution';
    elseif ~isempty(strfind([meas.qa.ggaVel.status,meas.qa.vtgVel.status],'good'))
        status='good';
    elseif ~isempty(strfind([meas.qa.ggaVel.status,meas.qa.vtgVel.status],'inactive'))
        status='inactive';
    else
        status='default';
    end
    setButtonColor(handles,'pbGPSFilters',status);
    setButtonColor(handles,'pbWTFilters',meas.qa.wVel.status);
    setButtonColor(handles,'pbExtrap',meas.qa.extrapolation.status);
    setButtonColor(handles,'pbEdges',meas.qa.edges.status);
   
    % Update messages
    handles=updateMessages(handles)
       
function handles=updateMessages(handles)
    % Updates the GUI with error and warning messages
    %
    % INPUT:
    % handles
    %
    % RESULTS:
    % Displays messages in GUI
    %
    % CALLS:
    % --
    
    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');
    
    % Identify properties to be reported
    prop=properties(meas.qa);
    propidx=strfind(prop,'Threshold');
    propidx=cellfun(@isempty,propidx);
    qaProp=prop(propidx);
    
    % Initialize variables
    nProperties=length(qaProp);
    nout=0;
    messageout={};
    
    % Loop through each property
    for n=1:nProperties
        
        % Determine number of messages for that property
        nmessages=size(meas.qa.(qaProp{n,:}).messages,1);
        if nmessages>0
            
            % Consolidate messages
            for j=1:nmessages
                nout=nout+1;
                messageout(nout,:)= {meas.qa.(qaProp{n}).messages{j,:}};
            end % for j
        end % if nmessages
    end % for n
    if ~isempty(messageout)
        messageout = sortrows(messageout, 2);
    else
        messageout2=messageout;
    end
    
    for n = 1:size(messageout,1)
        if messageout{n,2} == 2
%             currentFolder = pwd;
%             messageout(n,:) = {sprintf('<html><img style="width:5px; height: 5px;" src="file:/%scaution.png"></img></html>', handles.sourceFolder) sprintf('<html><td style="font-size: 12pt; font-style: italic;">%s</td></html>', messageout{n,1}) messageout{n,3}};
            messageout2(n,:)={['<html><img style="width:5px; height: 5px;" src="file:',handles.cautionIcon,'"></img></html>'],['<html><td style="font-size: 14pt; font-style: italic;">',messageout{n,1},'</td></html>'],messageout{n,3}};
        elseif  messageout{n,2} == 1
%             currentFolder = pwd;
%              messageout2(n,:) = {sprintf('<html><img style="width:5px; height: 5px;" src="file:%s"></img></html>', handles.warningIcon) sprintf('<html><td style="font-size: 14pt; font-weight: bold; ">%s</td></html>', messageout{n,1}) messageout{n,3}};
             messageout2(n,:)={['<html><img style="width:5px; height: 5px;" src="file:',handles.warningIcon,'"></img></html>'],['<html><td style="font-size: 14pt; font-weight: bold;">',messageout{n,1},'</td></html>'],messageout{n,3}};

        end
    end
        
    
    set(handles.tblMsg,'Data',messageout2);
    set(handles.uipanel5, 'Title', sprintf('Messages (%d)', size(messageout2,1)))

function tblMsg_CellSelectionCallback(hObject, eventdata, handles)
if ~isempty(eventdata.Indices)
  idx = eventdata.Indices(1);
  module = eventdata.Source.Data{idx,3}; 
  
  switch module
      case 1
         pbSelDataFiles_Callback(hObject, eventdata, handles);
      case 2
         pbViewInstrument_Callback(hObject, eventdata, handles);
      case 3
         pbSystemTest_Callback(hObject, eventdata, handles);
      case 4
         pbCompassCal_Callback(hObject, eventdata, handles);
      case 5
         pbTempChk_Callback(hObject, eventdata, handles);
      case 6
          pbMovBedTst_Callback(hObject, eventdata, handles);
      case 7
          pbBTFilters_Callback(hObject, eventdata, handles);
      case 8
          pbGPSFilters_Callback(hObject, eventdata, handles);
      case 9
          pbRef_Callback(hObject, eventdata, handles);
      case 10
          pbDepthFilters_Callback(hObject, eventdata, handles);
      case 11
          pbWTFilters_Callback(hObject, eventdata, handles);
      case 12
          pbExtrap_Callback(hObject, eventdata, handles);
      case 13
         pbEdges_Callback(hObject, eventdata, handles);
      
  end
end





%==========================================================================
%================NO MODIFIED CODE BELOW HERE===============================
%==========================================================================

function puReference_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





% --- Executes during object creation, after setting all properties.
function puRating_CreateFcn(hObject, eventdata, handles)
% hObject    handle to puRating (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected cell(s) is changed in tblDetail.
function tblDetail_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to tblDetail (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)



% --- Executes during object creation, after setting all properties.
function tblSum1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tblSum1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
set(hObject, 'Data', cell(3,2));


% --- Executes during object creation, after setting all properties.
function tblSum2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tblSum2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
set(hObject, 'Data', cell(3,2));










% --- Executes on button press in pbEDI.



% --- Executes on button press in pbUncheck.



% --- Executes when entered data in editable cell(s) in tblMsg.
function tblMsg_CellEditCallback(hObject, eventdata, handles)
    eventdata
% hObject    handle to tblMsg (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when selected cell(s) is changed in tblMsg.

% hObject    handle to tblMsg (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)
