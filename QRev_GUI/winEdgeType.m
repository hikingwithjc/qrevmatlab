function varargout = winEdgeType(varargin)
%WINEDGETYPE M-file for winEdgeType.fig
%      WINEDGETYPE, by itself, creates a new WINEDGETYPE or raises the existing
%      singleton*.
%
%      H = WINEDGETYPE returns the handle to a new WINEDGETYPE or the handle to
%      the existing singleton*.
%
%      WINEDGETYPE('Property','Value',...) creates a new WINEDGETYPE using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to winEdgeType_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      WINEDGETYPE('CALLBACK') and WINEDGETYPE('CALLBACK',hObject,...) call the
%      local function named CALLBACK in WINEDGETYPE.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winEdgeType

% Last Modified by GUIDE v2.5 16-Aug-2016 16:47:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winEdgeType_OpeningFcn, ...
                   'gui_OutputFcn',  @winEdgeType_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before winEdgeType is made visible.
function winEdgeType_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for winTempSource
    handles.output = hObject;

    % Get main gui handle
%     hInput=find(strcmp(varargin,'winEdges'));
%     if ~isempty(hInput)
        handles.hMainGui=varargin{1};
        handles.parentGUI=varargin{2};
%     end
    
    % Get transect selected
    handles.selected=varargin{3};
    
    % Set the window position to lower left
    setWindowPosition(hObject,handles);
    
    % Change comment icon
    commentIcon(handles.pbComment);
    
    % Update Panel to reflect bank being edited
    set(handles.bgEdgeType,'Title',[varargin{3},' Edge Type']);
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    set(handles.txtUser,'String',uLabelQ);
    
    % Display current start bank
    if strcmpi(varargin{4},'Triangular')
        set(handles.rbTri,'Value',1);
        handles.s=struct('process','Cancel','type',varargin{4},'value',[]);
    elseif strcmpi(varargin{4},'Rectangular')
        set(handles.rbRec,'Value',1);
        handles.s=struct('process','Cancel','type',varargin{4},'value',[]);
    elseif strcmpi(varargin{4},'Custom')
        set(handles.rbCust,'Value',1);
        set(handles.edCust,'String',sprintf('% 1.5f',varargin{5}));
        handles.s=struct('process','Cancel','type',varargin{4},'value',varargin{5});
        if isempty(get(handles.edCust,'String'))
            set(handles.pbOne,'Enable','off');
            set(handles.pbAll,'Enable','off');
        end
    elseif strcmpi(varargin{4},'User Q')
        set(handles.rbUser,'Value',1);
        set(handles.edUser,'String',sprintf('% 6.3f',varargin{5}));
        handles.s=struct('process','Cancel','type',varargin{4},'value',varargin{5});
        if isempty(get(handles.edUser,'String'))
            set(handles.pbOne,'Enable','off');
            set(handles.pbAll,'Enable','off');
        end
    end
    handles.parentGUI.UserData=handles.s;
    % Update handles structure
    guidata(hObject, handles);
    

% --- Outputs from this function are returned to the command line.
function varargout = winEdgeType_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pbOne.
function pbOne_Callback(hObject, eventdata, handles)
% hObject    handle to pbOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    handles.s.process='One';
    handles.parentGUI.UserData=handles.s;
    guidata(hObject, handles);
    
    % Close winStartBank
    delete (handles.output); 
    
% --- Executes on button press in pbAll.
function pbAll_Callback(hObject, eventdata, handles)
% hObject    handle to pbAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    handles.s.process='All';
    handles.parentGUI.UserData=handles.s;
    guidata(hObject, handles);
    
    % Close winStartBank
    delete (handles.output);  

% --- Executes on button press in pbCancel.
function pbCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pbCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Close winStartBank
    handles.s.process='Cancel';
    handles.parentGUI.UserData=handles.s;
    guidata(hObject, handles);
    delete (handles.output);
% --- Executes on button press in pbComment.
function pbComment_Callback(hObject, eventdata, handles)
% hObject    handle to pbComment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % Add comment
    [handles]=commentButton(handles, 'SOS');
    % Update handles structure
    guidata(hObject, handles);  

% --- Executes on button press in pbHelp.
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    %open('helpFiles\QRev_Users_Manual.pdf') 

web('QRev_Help_Files\HTML\edges.htm')

function edUser_Callback(hObject, eventdata, handles)
% hObject    handle to edUser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    % Get units multiplier
    [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier(handles);
    
    handles.s.value=str2double(get(handles.edUser,'String'))./unitsQ;
    
    set(handles.pbOne,'Enable','on');
    set(handles.pbAll,'Enable','on');
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edUser_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edUser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected object is changed in bgEdgeType.
function bgEdgeType_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in bgEdgeType 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    if get(handles.rbTri,'Value')
        handles.s.type='Triangular';
        set(handles.pbOne,'Enable','on');
        set(handles.pbAll,'Enable','on');
    elseif get(handles.rbRec,'Value')
        handles.s.type='Rectangular';
        set(handles.pbOne,'Enable','on');
        set(handles.pbAll,'Enable','on');        
    elseif get(handles.rbCust,'Value')
        handles.s.type='Custom';
        c=get(handles.edUser,'String');
        if isempty(c)
            set(handles.pbOne,'Enable','off');
            set(handles.pbAll,'Enable','off');
        else
            handles.s.value=str2double(c);
        end
    elseif get(handles.rbUser,'Value')
        handles.s.type='User Q';  
        q=get(handles.edUser,'String');
        if isempty(q)
            set(handles.pbOne,'Enable','off');
            set(handles.pbAll,'Enable','off');
        else
            handles.s.value=str2double(q);
        end
    end
guidata(hObject, handles);



function edCust_Callback(hObject, eventdata, handles)
% hObject    handle to edCust (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edCust as text
%        str2double(get(hObject,'String')) returns contents of edCust as a double

    
    handles.s.value=str2double(get(handles.edCust,'String'));
    
    set(handles.pbOne,'Enable','on');
    set(handles.pbAll,'Enable','on');
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edCust_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edCust (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
