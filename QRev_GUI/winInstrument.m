function varargout = winInstrument(varargin)
% WININSTRUMENT MATLAB code for winInstrument.fig
%      WININSTRUMENT, by itself, creates a new WININSTRUMENT or raises the existing
%      singleton*.
%
%      H = WININSTRUMENT returns the handle to a new WININSTRUMENT or the handle to
%      the existing singleton*.
%
%      WININSTRUMENT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WININSTRUMENT.M with the given input arguments.
%
%      WININSTRUMENT('Property','Value',...) creates a new WININSTRUMENT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before winInstrument_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to winInstrument_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help winInstrument

% Last Modified by GUIDE v2.5 21-Jun-2016 11:08:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @winInstrument_OpeningFcn, ...
                   'gui_OutputFcn',  @winInstrument_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before winInstrument is made visible.
function winInstrument_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to winInstrument (see VARARGIN)

% Choose default command line output for winInstrument
handles.output = hObject;

% Get main gui handle
hInput=find(strcmp(varargin,'hMainGui'));
if ~isempty(hInput)
    handles.hMainGui=varargin{hInput+1};
end

setWindowPosition(hObject,handles);

commentIcon(handles.pbComment);

% Retrieve measurement data
meas=getappdata(handles.hMainGui,'measurement');

% Station name and number
set(handles.edStaName,'String',meas.stationName);
set(handles.edStaNumber,'String',meas.stationNumber);
% Apply formatting to station name
if meas.qa.user.staName==1
    set(handles.edStaName,'BackgroundColor','yellow');
else
    set(handles.edStaName,'BackgroundColor',[1,1,1]);
end

% Apply formatting to station number
if meas.qa.user.staNumber==1
    set(handles.edStaNumber,'BackgroundColor','yellow');
else
    set(handles.edStaNumber,'BackgroundColor',[1,1,1]);
end   
% Use first transect
transect=meas.transects(1);

% Populate table with instrument information
tableData{1,1}=transect.adcp.serialNum;
tableData{2,1}=transect.adcp.manufacturer;
tableData{3,1}=transect.adcp.model;
tableData{4,1}=transect.adcp.firmware;
tableData{5,1}=[num2str(transect.adcp.frequency_hz'),' kHz'];
tableData{6,1}=num2str(transect.wVel.waterMode);
tableData{7,1}=num2str(transect.boatVel.btVel.bottomMode);
maxCell=nanmax(nanmax(transect.depths.(transect.depths.selected).depthCellSize_m.*100));
minCell=nanmin(nanmin(transect.depths.(transect.depths.selected).depthCellSize_m.*100));
if abs(maxCell-minCell)<eps
    tableData{8,1}=[num2str(minCell),' cm'];
else
    tableData{8,1}=[num2str(minCell),' - ',num2str(maxCell),' cm'];
end

% Update table in GUI
set(handles.tblInstrument,'Data',tableData);

% Set the processing button
switch meas.processing
    case 'RSL'
        set(handles.rbSonTek,'Value',1);
    case 'WR2'
        set(handles.rbTRDI,'Value',1);
    case 'QRev'
        set(handles.rbQRev,'Value',1);
end
set(handles.uibgProc,'Visible','off')
set(handles.uiFilterButtons,'Visible','off')
set(handles.pbOriginal,'Visible','off')
set(handles.pbAuto,'Visible','off')
drawnow


% Update handles structure
guidata(hObject, handles);

function uibgProc_SelectionChangedFcn(hObject, eventdata, handles)
% --- Executes when selected object is changed in uibgProc.

    % Get measurement data
    meas=getappdata(handles.hMainGui,'measurement');

    % Set processing based on selected button
    switch get(eventdata.NewValue,'Tag')
        case 'rbSonTek'
            settings=clsMeasurement.currentSettings(meas);
            settings=clsMeasurement.RSLDefaultInterp(settings);
            settings.processing='RSL';
        case 'rbTRDI'
            settings=clsMeasurement.currentSettings(meas);
            settings=clsMeasurement.WR2DefaultInterp(settings);
            settings.processing='WR2';
        case 'rbQRev'
            settings=clsMeasurement.currentSettings(meas);
            settings=clsMeasurement.QRevDefaultInterp(settings);
            settings.processing='QRev';                
    end
    % Apply settings
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    meas=applySettings(meas,settings);     
    set(gcf, 'pointer', oldpointer);     
    drawnow;

    % Save measurement data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Update handles structure
    guidata(hObject, handles);
        
% --- Outputs from this function are returned to the command line.
function varargout = winInstrument_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pbCloseInstrument.
function pbCloseInstrument_Callback(hObject, eventdata, handles)
% hObject    handle to pbCloseInstrument (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

delete (handles.output);


function edStaName_Callback(hObject, eventdata, handles)
% hObject    handle to edStaName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edStaName as text
%        str2double(get(hObject,'String')) returns contents of edStaName as a double

meas=getappdata(handles.hMainGui,'measurement');
meas=changeStationName(meas,get(hObject,'String'));
if ~isempty(get(hObject,'String'))
    set(handles.edStaName,'BackgroundColor',[1,1,1]);
end

setappdata(handles.hMainGui,'measurement',meas);

function edStaNumber_Callback(hObject, eventdata, handles)
% hObject    handle to edStaNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edStaNumber as text
%        str2double(get(hObject,'String')) returns contents of edStaNumber as a double

meas=getappdata(handles.hMainGui,'measurement');
meas=changeStationNumber(meas,get(hObject,'String'));
if ~isempty(get(hObject,'String'))
    set(handles.edStaNumber,'BackgroundColor',[1,1,1]);
end
setappdata(handles.hMainGui,'measurement',meas);



% --- Executes on button press in pbOriginal.
function pbOriginal_Callback(hObject, eventdata, handles)
    meas=getappdata(handles.hMainGui,'measurement');
    if get(handles.rbSonTek,'Value')
        meas.processing='RSL';
        settings=clsMeasurement.RSLDefaultInterp(meas.initialSettings);
    elseif get(handles.rbTRDI,'Value')
        meas.processing='WR2';
        settings=clsMeasurement.WR2DefaultInterp(meas.initialSettings);
    elseif get(handles.rbQRev,'Value')
        meas.processing='QRev';
        settings=clsMeasurement.QRevDefaultInterp(meas.initialSettings);                
    end
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    meas=applySettings(meas,settings);     
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);

    % Update handles structure
    guidata(hObject, handles);



% --- Executes on button press in pbComment.
function pbComment_Callback(hObject, eventdata, handles)
% Add comment
    [handles]=commentButton(handles, 'Instrument & Processing');
    % Update handles structure
    guidata(hObject, handles);


% --- Executes on button press in pbAuto.
function pbAuto_Callback(hObject, eventdata, handles)
% hObject    handle to pbAuto (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    meas=getappdata(handles.hMainGui,'measurement');
    if get(handles.rbSonTek,'Value')
        settings=clsMeasurement.QRevDefaultSettings(meas);
        settings=clsMeasurement.RSLDefaultInterp(settings);
        settings.processing='RSL';
    elseif get(handles.rbTRDI,'Value')
        settings=clsMeasurement.QRevDefaultSettings(meas);
        settings=clsMeasurement.WR2DefaultInterp(settings);
        settings.processing='WR2';
    elseif get(handles.rbQRev,'Value')
        settings=clsMeasurement.QRevDefaultSettings(meas);
        settings=clsMeasurement.QRevDefaultInterp(settings); 
        settings.processing='QRev';               
    end
    oldpointer = get(gcf, 'pointer');      
    set(gcf, 'pointer', 'watch');     
    drawnow;     
    meas=applySettings(meas,settings);     
    set(gcf, 'pointer', oldpointer);     
    drawnow;
    
    % Store data
    setappdata(handles.hMainGui,'measurement',meas);
    
    % Update handles structure
    guidata(hObject, handles);


% --- Executes on button press in pbHelp.
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%open('helpFiles\QRev_Users_Manual.pdf')  
web('QRev_Help_Files\HTML\adcp_site_info_button.htm')



% --- Executes during object creation, after setting all properties.
function edStaNumber_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edStaNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function edStaName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edStaName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on key press with focus on figInstrument and none of its controls.
function figInstrument_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figInstrument (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
% ALT-g to plot to Google Earth
if strcmp(eventdata.Key,'p') && strcmp(eventdata.Modifier,'alt')
    if strcmp(get(handles.uibgProc,'Visible'),'off')
        set(handles.uibgProc,'Visible','on')
        set(handles.uiFilterButtons,'Visible','on')
        set(handles.pbOriginal,'Visible','on')
        set(handles.pbAuto,'Visible','on')
    else
        set(handles.uibgProc,'Visible','off')
        set(handles.uiFilterButtons,'Visible','off')
        set(handles.pbOriginal,'Visible','off')
        set(handles.pbAuto,'Visible','off')
    end
end
