function updateStatus(textIn,varargin)
    persistent hText;
    if ~ishandle(hText)
        hText=[];
    end
    if ~isempty(varargin)
        if ishandle(varargin{:})
            hText=varargin{:};
        end
    end
    if ~isempty(hText)
        if ischar(textIn)
            set(hText,'Data',{'' textIn ''});
        else
            set(hText,'Data',textIn);
        end
        drawnow;
    end
end

