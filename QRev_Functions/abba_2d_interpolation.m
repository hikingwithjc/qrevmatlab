function interpolated_data = abba_2d_interpolation(data_cell, valid_data,...
    cells_above_sl, y_cell_centers, y_cell_size, y_normalize, y_depth, ...
    x_shiptrack)
% Interpolates values for invalid cells using the neighboring cells above, 
% below, before, and after and and inverse distance averaging.
    
    n_data = length(data_cell);
    interpolation_points = find_neighbors(valid_data, cells_above_sl, y_cell_centers, ...
        y_cell_size, y_normalize, y_depth);
    n_points = size(interpolation_points, 1);
    interpolated_data = cell(n_points, n_data+1);
    if ~isnan(x_shiptrack)
        for n=1:n_points
            interpolated_data{n,1} = interpolation_points{n, 1};
            distances = compute_distances(interpolation_points{n, 1}, interpolation_points{n, 2}, x_shiptrack, y_cell_centers);
            for m=1: n_data
                interpolated_value = idw_interpolation(data_cell{m}, interpolation_points{n, 2}, distances);
                 interpolated_data{n,m+1} = interpolated_value;
            end
        end
    end
end

function neighbors = find_neighbors(valid_data, cells_above_sl, y_centers, ...
    y_cell_size, y_normalize, y_depth)

    y_top = y_centers - 0.5 .* y_cell_size;
    y_bottom = y_centers + 0.5 .* y_cell_size;
    y_bottom_actual = y_centers + 0.5 * y_cell_size;
    
    if ~isnan(y_normalize)
        y_top = round(bsxfun(@rdivide, y_top, y_normalize), 3);
        y_bottom = round(bsxfun(@rdivide, y_bottom,y_normalize), 3);
        y_centers =round(bsxfun(@rdivide, y_centers, y_normalize), 3);
    end

    valid_data_float = double(valid_data);
    valid_data_float(~cells_above_sl) = nan;
    [row, col] = find(valid_data_float == 0);
    neighbors = cell(length(row),2);
    for n=1:length(row)
        neighbors{n, 1} = [row(n), col(n)];
        k = 1;
        above=find_above(row(n), col(n), valid_data);
        if ~isnan(above)
            points(k, :) = above;
            k = k + 1;
        end
        below=find_below(row(n), col(n), valid_data);
        if ~isnan(below)
            points(k, :) = below;
            k = k + 1;
        end
        
        y_match1 = (y_centers >= y_top(row(n), col(n))) & (y_centers <= y_bottom(row(n), col(n)));
        y_match2 = (y_top(row(n), col(n)) >= y_top) & (y_bottom(row(n), col(n)) <= y_bottom);
        y_match = y_match1 | y_match2;
        y_match = y_match & valid_data;
        
        before = find_before(row(n), col(n), y_match, y_depth, y_bottom_actual);
        if ~isnan(before)
            for m=1:size(before, 1)
                points(k, :) = before(m,:);
                k = k + 1;
            end
        end
        
        after = find_after(row(n), col(n), y_match,y_depth, y_bottom_actual);
        if ~isnan(after)
            for m=1:size(after, 1)
                points(k, :) = after(m,:);
                k = k + 1;
            end
        end
        if exist('points')
            neighbors{n, 2} = points;
        else
            n;
        end
        clear points
    end
    
    
end

function above_idx=find_above(row, col, valid_data)
% Finds the nearest valid cell above row,col
    % Initialize counter
    above_idx = row-1;
    % Find nearest valid cell above target
    while above_idx >= 1 && ~valid_data(above_idx, col)
        above_idx = above_idx - 1;
    end
    if above_idx >= 1
        above_idx = [above_idx, col];
    else
        above_idx = nan;
    end
end

function below_idx=find_below(row, col, valid_data)
% Find nearest valid cell below row,col
    % Initialize counter
    below_idx = row + 1;
    % Determine cell row index limit
    n_cells=size(valid_data, 1);
    % Find nearest valid cell below row,col
    while below_idx <= n_cells && ~valid_data(below_idx, col)
        below_idx = below_idx + 1;
    end
    if below_idx <= n_cells
        below_idx = [below_idx, col];
    else
        below_idx = nan;
    end
end

function before_idx = find_before(row, col, y_match, y_depth, y_bottom)
% Find nearest valid cell at same level as row, col before col
    % Initialize counter
    before_ens = col - 1;
    % Loop until an ensemble is found that has valid data within the vertical range of the target while honoring
    % the bathymetry. If the streambed is encountered while searching for a previously valid ensemble then
    % it is determined that there is no available valid data before the target that can be used.
    found = 0;
    while before_ens > 0 && ~found
        if y_bottom(row, col) < y_depth(before_ens) & any(y_match(:, before_ens))
            found = 1;
        elseif y_bottom(row, col) > y_depth(before_ens)
            before_ens = -999;
            found = 1;
        else
            before_ens = before_ens - 1;
        end
    end
    % Find and store the indices of all cells from the identified ensemble
    % that are within the vertical range of the target
    if before_ens > 0
        rows = find(y_match(:, before_ens));
        for n=1:length(rows)
            before_idx(n,:) = [rows(n), before_ens];
        end
    else
        before_idx = nan;
    end
end

function after_idx = find_after(row, col, y_match, y_depth, y_bottom)
% Find nearest valid cell at same level as row, col after col
    % Initialize counter
    after_ens = col + 1;
    % # Loop until an ensemble is found that has valid data within the vertical range of the target while honoring
    % the bathymetry. If the streambed is encountered while searching for a next valid ensemble then
    % it is determined that there is no available valid data after the target that can be used.
    n_ens = size(y_match, 2);
    found = 0;
    while after_ens <= n_ens  && ~found
        if (y_bottom(row, col) < y_depth(after_ens)) & any(y_match(:, after_ens))
            found = 1;
        elseif y_bottom(row, col) > y_depth(after_ens)
            after_ens = -999;
            found = 1;
        else
            after_ens = after_ens + 1;
        end
    end
    % Find and store the indices of all cells from the identified ensemble
    % that are within the vertical range of the target
    if after_ens <= n_ens & after_ens > 0
        rows = find(y_match(:, after_ens));
        for n=1:length(rows)
            after_idx(n,:) = [rows(n), after_ens];
        end
    else
        after_idx = nan;
    end
end

function distances = compute_distances(target, neighbors, x, y)
% Computes the distance between the target and the neighbors
    target_y = y(target(1),target(2));
    target_x = x(target(2));
    n_neighbors = size(neighbors, 1);
    distances=nan(n_neighbors,1);
    for n=1:n_neighbors
        row = neighbors(n,1);
        ens = neighbors(n,2);
        distances(n) = sqrt((y(row, ens) - target_y).^2 + (x(ens) - target_x).^2);
    end
end

function interpolated_value = idw_interpolation(data, neighbor_indices, distances)
% Interpolate value using neighbors and inverse distance weighting
    sum_of_weights = sum(1./distances);
    weighted_sum = 0;
    n_neighbors = size(neighbor_indices, 1);
    for n = 1: n_neighbors
        row = neighbor_indices(n,1);
        col = neighbor_indices(n,2);
        
        weighted_sum = weighted_sum + data(row, col) * (1./distances(n));
    end
    interpolated_value = weighted_sum ./ sum_of_weights;
end


