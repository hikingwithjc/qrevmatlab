function updateStatus_QRevGUI(textIn,varargin)
    persistent hText;
    if ~isempty(varargin)
        hText=varargin{:};
    end
    set(hText,'String',textIn);
    drawnow;
end

