function dir=azdeg2rad(angle)
    dir=deg2rad(90-angle);
   idx=find(dir<0);
    if ~isempty(idx)
        dir(idx)=dir(idx)+2.*pi;
    end
end