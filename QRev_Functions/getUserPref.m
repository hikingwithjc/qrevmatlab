function [ value ] = getUserPref( prop )
% This function reads a user preference file in
% UserName\appdata\Local\QRev and returns the value in from
% userPref.(prop).

    % Define Local AppData Folder
    appData=getenv('LOCALAPPDATA');
    QRevPrefFile=[appData,'\QRev\QRevPref.mat'];
    
    % Check to see if folder exists
    try 
        load (QRevPrefFile)
        value=userPref.(prop);
    catch       
        value=nan;
    end

end

