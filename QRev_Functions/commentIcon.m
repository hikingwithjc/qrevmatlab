function commentIcon(h)
     commentIcon=imread('comment_icon.png','png');
     set(h,'Units','pixels');
     pos=get(h,'Position');
     pos(3:4)=[35,35];
     set(h,'Position',pos);
     set(h,'Units','normalized');
     set(h,'CData',commentIcon);
     set(h,'String','');
end