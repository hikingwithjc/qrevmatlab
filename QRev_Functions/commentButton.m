function [ handles ] = commentButton( handles, varargin )
% Handles comment button pushes for all dialogs

    options.Resize='on';
    t=datetime('now');
    defaultText={[varargin{:},' ',datestr(t,'mm/dd/yyyy HH:MM:ss'),': ']};
    comment=commentdlg('Enter Comment:','COMMENT',[10 80],defaultText,options);
    
    if ~isempty(comment)
%         if ~isempty(comment{:})
%             if ~isempty(varargin)
%                 comment{1}=[varargin{:},' ',datestr(t,'mm/dd/yyyy HH:MM:ss'),': ',comment{:}];
%             end
            meas=getappdata(handles.hMainGui,'measurement');
            meas=addComment(meas,comment);
            setappdata(handles.hMainGui,'measurement',meas);
            if isfield(handles,'meas')
                handles.meas=addComment(handles.meas,comment);
            end
%         end
    end
end

