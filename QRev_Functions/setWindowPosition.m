function setWindowPosition(hObject,handles)
%Set window position
    set(hObject,'Units',handles.hMainGui.Units);
    pos=hObject.Position;
    mainpos=handles.hMainGui.Position;
    newpos=[mainpos(1:2),pos(3:4)];
    set(hObject,'Position',newpos);
end

