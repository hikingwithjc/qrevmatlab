function tableScrollView( hObject, row, col )
% This function uses Java to set the view of the table to the selected row
% and column.
mTable=findobj(hObject);
jTable=mTable.getViewport.getView;
jTable.setRowSelectionAllowed(0);
jTable.setColumnSelectionAllowed(0);
jTable.changeSelection(row-1,col-1,false,false); 
% jTable.repaint;
end

