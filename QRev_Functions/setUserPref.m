function setUserPref( prop,varargin )
% This function creates a user preference file in
% UserName\appdata\Local\QRev and/or saves varargin if provided in the the
% structure variable prop.

    % Define Local AppData Folder
    appData=getenv('LOCALAPPDATA');
    QRevPrefFile=[appData,'\QRev\QRevPref.mat'];
    
    % Check to see if folder exists
    try 
        load (QRevPrefFile)
    catch       
        mkdir(QRevPrefFile(1:end-13));
    end
    
    % Set default folder
    if isempty(varargin)
        if strcmp(prop,'Folder')
            userPref.Folder=getenv('USERPROFILE');
        elseif strcmp(prop,'Units');
            userPref.Units='English';
        elseif strcmp(prop,'styleSheet')
            userPref.styleSheet=0;
        end 
    else
        userPref.(prop)=varargin{:};
    end
    save(QRevPrefFile, 'userPref')
    
end

