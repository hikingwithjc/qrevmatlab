function dir=rad2azdeg(angle)
    dir=-1.*rad2deg(angle)+90;
   idx=find(dir<0);
    if ~isempty(idx)
        dir(idx)=dir(idx)+360;
    end
end