Changes 3.43 to 3.63
1. Modified the way messages are formatted in xml output
2. Changes to handle invalid data without crashing
3. Fixed bug where left distance moved QA was used for right bank
4. Fixed issue with intrpolated discharge for shallow ensembles not included in ensemble Q
5. Changed computation of area to be consistent with AreaComp2
6. Change mean depth to be area/width
7. Change wt interpolation method to abba rather than Matlab linear
8. Change EDI computations to include a correction factor for moving-bed correction
9. Improved handling of invalid data

Changes 3.42 to 3.43
1. Fixed bug that did not properly identify missing (lost) ensembles if the typical ensemble duration was greater 
than 1.5 sec. This affected mode 13 StreamPro data.

Changes 3.41 to 3.42
1. Fixed bug in bottom discharge extrapolation. Previous versions failed to estimate the 
bottom discharge for ensembles that had no valid data but where depth cell velocities were interpolated.

Changes 3.40 to 3.41
1. Fixed bug with discharge percent change not updating after multiple speed of sound changes.
2. Fixed bug in edges table where wrong row was highlighted if some transects were unchecked.
3. Fixed bug in edges shiptrack so it only displays valid orginal data and not interpolated data.
4. Fixed bug no allowing manual entry of 0 salinity.

Changes from 3.35 to 3.40
1. Fixed bug causing crash when TRDI PT3 test was incomplete.
2. Modified uncertainty estimate for invalid and edges to use absolute values.
3. Fixed issue with multi-line comments in XML file.
4. Fixed draft check to only evaluate checked transects.
5. XML file now only contains the summary information from a SonTek compass calibration.
6. Fixed bug in moving-bed test if loop compass error and direction could not be computed.
7. Added message for missing samples in SonTek data.
8. Modified PT3 tests to reduce false positives.
9. Added check to boat and wt to check warn if all data are invalid.
10. Edge sign check changed to only show message if edge discharge is greater than 0.5%
11. Added option to store a style sheet with the xml output file.
12. Added _ separator between date and time in default filename.
13. Default compass data view changed to evaluation if evaluation is available.
14. Added upper limit of 45 ppt to salinty input dialog.
15. Added Power/Power default exponent to XML output to support NVE request.
16. Update help file.

Changes to QRev from 3.34 to 3.35
1. Fixed bug causing crash when ADCP had no compass.

Changes to QRev from 3.33 to 3.34
1. Fixed bug causing 25 cm excluded distance to be applied to RiverPro data.

Changes to QRev from 3.31 to 3.33
1. Fixed bug using uncheck all.
2. Fixed bug manually loading system test or compass calibration or evaluation
3. Changed legend location to best
4. Fixed bugs in GUI causing wrong transects to be highlighted
5. Fixed bug reprocessing QRev files associated with moving-bed tests
6. Fixed bug causing changes in draft hOffset, hSource, and magVar to not update QA properly

Changes to QRev from 3.30 to 3.31
1. Fixed bug creating structure for test results from existing QRev files
2. Fixed bug setting status of system test results and button color
3. Fixed bug causing end edge discharge to be applied at start edge
4. Fixed bug preventing user uncertainty from being recomputed after changing a filter or extrapolation
5. Fixed bug displaying number of ensembles to use for edge
6. Changed the algorithm for determining PT3 failure
7. Modified the computation for extrapolation uncertainty to prevent a manual setting from biasing the uncertainty low
8. Added reprocessing of moving-bed tests to reprocessing of existing QRev data files.
9. The mag field, if available, is automatically displayed in the heading graph
10. Modified the excluded distance filter to round to 2 digits to prevent differences when converting between units.

Changes to QRev from 3.28 to 3.30
1. Fixed bugs in messages table that were opening wrong windows.
2. Modified code to interpolate invalid data from composite tracks using the composite tracks rather than the primary data.
3. Modified code to interpolate invalid data from composite depths using the composite depths rather than the primary data.
4. Modified draft check to use display units and limit the comparison to 2 decimal spots which is what is displayed in the GUI.
5. Added code to interpolate discharge for ensembles that are too shallow for a valid depth cell.
6. Modified code in the way that TRDI mmt file setting for composite depths are processed to QRev settings. 
7. Moved composite depth computation to end of data input flow.
8. Modified code to allow the view comments button to be accessible even when another window is open.
9. Fixed bug with computePerDiff in extrapolation sensitivity when no transect data provided.
10. Added valid depth method to xml output.
11. Added code to check compass cal errors against USGS recommendations
12. Added code to handle files processed with QBatch.
13. Fixed bug with variable names for mean and max depth and max water speed.
14. Fixed bug with order of warnings and cautions for system test checks.


Changes to QRev from 3.24 to 3.28

1. Messages are now in a table, sorted with warnings on top, and identified with a symbol and font.
2. Clicking on a message will open the associated window, same as clicking on associated button.
3. Extrapolation discharge sensitivity table is now referenced to the currently selected fit, rather than to power/power 0.1667.
4. Clicking a row in the extrapolation discharge sensitivity table will set the fit to those parameters.
5. The compass error from the evaluation of a TRDI ADCP and if available, from RiverSurveyor G3 compass is now shown in the Compass/P/R window.
6. The system test window now displays the total number of tests.
7. The PT3 test is now decoded and the correlation in lag 3 is checked to ensure that the correlation is less than 15% of the lag 0 correlation.
8. A minimum threshold for the boat track vertical velocity filter is set to 0.01 m/s.
9. A bug which applied both the magvar and heading offset to all compass data when loading has been fixed. Now the magvar is only applied to the internal compass and the heading offset only applied to the external compass. This is consistent with how it was handled in the Compass/P/R window.
10. Label for Google Earth plots was changed to filename only, not including the path.
11. Fixed other minor bugs.

Changes to QRev since from 3.23 to 3.24

1. Fixed compiler issue causing the plot to Google Earth to fail
2. Fixed a bug reading longitude data when preceded by a leading zero.

Changes to QRev since release of version 3.21 (in chronological order)

1. Improved the handling of illegal xml characters in mmt file.
2. Added a minimum threshold to the COV QA check for stationary tests.
3. Changed the QA check for number of valid ensembles to a duration based check for stationary moving-bed test.
4. Fixed bug causing edges GUI not to update with changes to table.
5. Added a button to copy comments from the view comments window so they could be pasted in another application.
6. Changed Display Units buttons to Options and created an Options window for units and save all or save checked only.
7. Added an icon button to main window for uncheck all.
8. Change version to 3.22.
9. Fixed bug in when selecting an extrapolation fit where numerical storage prevented 0.1 = 0.1	
10. Fixed issue with RSL revision numbers which need to be 2 digits (02 not 2).
11. Fixed issue with UTC time from GGA which contained an invalid character causing the str2double function resulting in NaN.
12. Added fix for invalid prefPath from corrupt AppData/Local/QRev
13. Added code to reset the close option from extrap to the main window when extrap is closed.
14. Added code to compute the edge velocity using the full profile assuming a 1/6th power fit to estimate the unmeasured portions of the profile.
15. Fixed bug associated with changing all moving-bed tests to manual.
16. Added new message when moving-bed tests are selected manually.
17. Fixed issue with moving-bed tests when data were collected in language other than English.
18. Change version to 3.23


Changes in QRev from 3.12 to 3.21

- Update help to add pdf file which can be searched.
- Fixed bug when plotting multiple transects with magnetic data in the Compass/P/R window.
- Fixed bug with transect name display when some transects were not checked in GPS window. 
- Fixed bug causing Use in main table to be shown in red when there were no errors.
- Fixed bug checking for invalid edge ensembles due to excluded distance.
- Fixed bug when using up arrow in Edge window.
- Fixed bug in xml output for change in speed of sound.
- Fixed crash when loading transect with only 1 ensemble.
- Updated use of arrow keys in tables to avoid table and plots getting out of synch when arrow keys were pressed too quickly.
- Added point marker to vertical beam in depth plots.
- Modified code to make buttons inactive if no transects are checked.
- Added EDI interface for Equal Discharge Increment sampling
- In the extrap window, added message to user that they need to set Fit to manual if the want to use an automatically opitimized fit but they have change the threshold, subsection, or data type.

Changes in QRev from version 3.10 to 3.12
1) fixed bug if number of unchecked transects was greater than number of checked
2) fixed bug where comments on uncertainty table were not being saved
3) fixed issue with edge distance check using all transects rather than just checked transects
4) fixed bug where temperature messages were based on all transects rather than just the checked transects
5) fixed bug where magError and GPS heading were saved as column vector rather than row vector
6) fixed bug where max and min pitch and roll limits were not set for G2 compass, now set to nan
7) improved scaling for magError and pitch and roll, 2) linked x axes of top and bottom plots
8) added code to add qTotalCaution to older QRev files
9) fixed bug when there is no pitch and roll data (old SonTek files)
10)fixed bug when selecting use when moving-bed test is not user valid
11)fixed bug when loading QRev files from 2.9x by adding code to create the qTotalCaution field
12) fixed bug when G2 compass was processed with current RSL resulting in Compass pitch and roll limits set to large number
13) added version numbers to popup when loading older QRev file
14) kml Google earth file now plots the full transect filename
15) added a minimum threshold to the bottom track error velocity automatic filter
16) fixed bug interpolating depths for lost ensembles
17)  fixed bug preventing automatic excluded distance for RioPro
18) fixed bug graphing of invalid data when excluded distance is set
19) fixed bug plotting data when all data are invalid
20) fixed bug applying user specified edge Q in RiverSurveyor Live
21) fixed bug when no station name or number was provided for compatibility with XML standards 
