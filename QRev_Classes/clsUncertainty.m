classdef clsUncertainty
    % This class computes the uncertainty of the discharge measurement
    % using automated methods defined by the methods in this class and
    % provides optional user override of the automatically generated
    % values.
    
    % Originally developed as part of the QRev processing program
    % Programmer: David S. Mueller, Hydrologist, USGS, OSW, dmueller@usgs.gov
    % Other Contributors: none
    % Latest revision: 5/29/2015
    
    properties (SetAccess = private)
        cov                 % Coefficient of variation for all used transect discharges
        cov95               % COV inflated by the 95% converage factor
        invalid95           % Estimated 95% uncertainty for dicharge in invalid bins and ensembles
        edges95             % Estimated 95% uncertainty for the computed edge discharges
        extrapolation95     % Estimated 95% uncertainty in discharge due to top and bottom extrapolations
        movingBed95         % Estimated 95% uncertainty due to moving-bed tests and conditions
        systematic          % Systematic error estimated at 1.5%
        total95             % Estimated 95% uncertainty in discharge using automated values
        cov95User           % User provided value for random uncertainty
        invalid95User       % User provided estimate of uncertainty for invalid data
        edges95User         % User provided estimate of uncertainty for edges
        extrapolation95User % User provided estimate of uncertainty for top and bottom extrapolation
        movingBed95User     % User provided estimate of uncertainty due to moving-bed conditions
        systematicUser      % User provided estimate of systematic uncertainty
        total95User         % Estimated 95% uncertainty in discharge using user provide values to override automated values
    end
    
    methods
        function obj=clsUncertainty(meas)
        % Contructs the object using static methods to compute the
        % automated uncertainties. User uncertainties are set to empty.
        %
        % INPUT:
        %
        % meas: object of clsMeasurement
        %
        % OUTPUT:
        % 
        % obj: object of clsUncertainty
        
            % Allow creation of object with no input
            if nargin > 0

                % Discharge
                discharge=meas.discharge;

                % Determine transects used to compute discharge
                checked=logical([meas.transects(:).checked]);

                % Assign automatically generated uncertainties to properties
                [obj.cov, obj.cov95]=obj.ranUncertQ(discharge(checked),'total');
                obj.invalid95=obj.uncertInvalidData(discharge,checked);
                obj.edges95=obj.uncertEdge(discharge,checked);
                obj.extrapolation95=obj.uncertExtrap(meas,discharge,checked);
                obj.movingBed95=obj.movingBedUncert(meas);
                obj.systematic=1.5;

                % Set user define uncertainties to empty
                if isempty(meas.uncertainty)
                    obj.cov95User=[];
                    obj.invalid95User=[];
                    obj.edges95User=[];
                    obj.extrapolation95User=[];
                    obj.movingBed95User=[];
                    obj.systematicUser=[];
                    obj.total95User=[];
                else
                    obj.cov95User=meas.uncertainty.cov95User;
                    obj.invalid95User=meas.uncertainty.invalid95User;
                    obj.edges95User=meas.uncertainty.edges95User;
                    obj.extrapolation95User=meas.uncertainty.extrapolation95User;
                    obj.movingBed95User=meas.uncertainty.movingBed95User;
                    obj.systematicUser=meas.uncertainty.systematicUser;
                end
                
                % Call method to compute uncertainty
                obj=estUncert(obj);
            end % nargin 
        end % clsUncertainty
        
        function obj=estUncert(obj)
        % Compute the uncertainty of the measurement using the
        % automatically computed uncertainties and user overrides.
        %
        % INPUT:
        %
        % obj: object of clsUncertainty
        %
        % OUTPUT:
        %
        % obj: object of clsUncertainty
        
            % Compute the total uncertainty using automatically generated
            % values
            obj.total95=2.*sqrt((obj.cov95/2).^2+(obj.invalid95./2).^2+...
                (obj.edges95./2).^2+(obj.extrapolation95./2).^2+...
                (obj.movingBed95./2).^2+obj.systematic.^2);
            
            % Override random uncertainty if user input provided
            if ~isempty(obj.cov95User)
                random=obj.cov95User;
            else
                random=obj.cov95;
            end
            
            % Override uncertainty for invalid data if user input provided
            if ~isempty(obj.invalid95User)
                invalid=obj.invalid95User;
            else
                invalid=obj.invalid95;
            end
            
            % Override uncertainty edges if user input provided
            if ~isempty(obj.edges95User)
                edges=obj.edges95User;
            else
                edges=obj.edges95;
            end
            
            % Override uncertainty for top and bottom extrapolation if user input provided
            if ~isempty(obj.extrapolation95User)
                extrapolation=obj.extrapolation95User;
            else
                extrapolation=obj.extrapolation95;
            end
            
            % Override uncertainty for moving bed conditiosn if user input provided
            if ~isempty(obj.movingBed95User)
                movingBed=obj.movingBed95User;
            else
                movingBed=obj.movingBed95;
            end
            
            % Override systematic uncertainty if user input provided
            if ~isempty(obj.systematicUser)
                system=obj.systematicUser;
            else
                system=obj.systematic;
            end
            
            % Compute total uncertainty using available user overrides
            obj.total95User=2.*sqrt((random/2).^2+(invalid./2).^2+...
                (edges./2).^2+(extrapolation./2).^2+(movingBed./2).^2+...
                system.^2);
            
        end % estimatedUncert
        
        function obj=addUserInput(obj,prop,value)
        % Sets specified property to provided user input and recomputes the
        % total uncertainty.
        %
        % INPUT:
        %
        % obj: object of clsUncertainty
        %
        % prop: name of user property to set
        %
        % value: value to assing to property
        %
        % OUTPUT:
        %
        % obj: object of clsUncertainty
        
            % Assign value to property
            if isnan(value)
                obj.(prop)=[];
            else
                obj.(prop)=value;
            end
            
            % Compute total uncertainty
            obj=estUncert(obj);
        end % addUserInput
        
        function obj=removeUserInput(obj,prop)
        % Removes user input from specified property
        %
        % INPUT:
        %
        % obj: object of clsUncertainty
        %
        % prop: name of user property to set to empty
        %
        % OUTPUT:
        %
        % obj: object of clsUncertainty
        
            % Assign value to property
            obj.(prop)=[];
            
            % Compute total uncertainty
            obj=estUncert(obj);
        end % removeUserInput
        
        function obj=resetProperty(obj,property,setting)
            obj.(property)=setting;
        end    
                    
    end % methods
    
    methods (Static)
        
        function [cov, cov95]=ranUncertQ(discharge,prop)
        % Compute 95% random uncertainty for property of discharge object.
        % Use simplified method for 2 transects.
        %
        % INPUT:
        %
        % obj: object of clsQComp
        %
        % prop: name of property in obj
        %
        % OUTPUT
        %
        % ranUncert95: 95% uncertainty of specified property for entire
        % measurement
        
            % Determine number of objects instances
            nMax=length(discharge);
            if nMax > 0
                % Combine data into single array
                for n=1:nMax
                    data(n)=discharge(n).(prop);
                end

                % Compute coefficient of variation
                avg=nanmean(data);
                sd=nanstd(data);
                cov=abs(sd/avg).*100;

                % Inflate COV to 95% level and report as percent
                cov95=abs(tinv(.025,nMax-1)).*cov./sqrt(nMax);

                % Use approximate method as taught in class to reduce the high
                % coverage factor for 2 transects and account for prior knowledge related to
                % 720 second duration analysis
                if nMax==2
                    cov95=cov.*3.3;
                end  
            else
                cov=nan;
                cov95=nan;
            end
            
        end % ranUncertQ
        
        function edgeUncert=uncertEdge(discharge,checked)
        % Compute uncertainty of edge discharge. Currently assuming random
        % plus bias in edge is within 30% of actual value.
        %
        % INPUT:
        %
        % discharge: object of clsQComp
        %
        % checked: logical vector of transects to be used to compute total
        % discharge
        %
        % OUTPUT:
        %
        % edgeUncert: estimated 95% uncertainty for edge discharge
        
            % Compute total mean discharge
            meanQ=clsQAData.meanQ(discharge(checked),'total');
            
            % Compute percent of total discharge in left edge
            meanLeft=clsQAData.meanQ(discharge(checked),'left');
            perLeft=(meanLeft./meanQ).*100;           
            
            % Compute percent of total discharge in right edge
            meanRight=clsQAData.meanQ(discharge(checked),'right');
            perRight=(meanRight./meanQ).*100;
            
            % Compute combined edge uncertainty
            perEdge=((abs(meanLeft)+abs(meanRight))./meanQ).*100;
            edgeUncert=perEdge.*0.3;
        end % uncertEdge
        
        function extrapUncert=uncertExtrap(meas,discharge,checked)
        % Compute uncertainty of top and bottom extrapolations
        %
        % INPUT:
        %
        % meas: object of clsMeasurement
        %
        % discharge: object of clsQComp
        %
        % checked: logical vector of transects used to compute final
        % discharge
        %
        % OUTPUT:
        %
        % extrapUncert: estimante 95% uncertainty in discharge due to
        % extrapolation methods
        
            % Compute mean total discharge
            qSelect=nanmean([discharge(checked).totalUncorrected]);
            
            % Create array of discharges from various extrapolation methods
            qPossible=[meas.extrapFit.qSensitivity.qPPmean,...
                        meas.extrapFit.qSensitivity.qPPoptmean,...
                        meas.extrapFit.qSensitivity.qCNSmean,...
                        meas.extrapFit.qSensitivity.qCNSoptmean,...
                        meas.extrapFit.qSensitivity.q3pNSmean,...
                        meas.extrapFit.qSensitivity.q3pNSoptmean];
                    
            % Compute difference in discharges from selected method
            diff=abs(qPossible-qSelect);
            
            % Sort differences
            diff=sort(diff)./qSelect;
            
            % Find the 4 smallest differences
            % DSM: This creates an error as it eliminates not only the
            % selected = 0 but also any less than 0.001
            idx=find(abs(diff)>0.001,4,'first');
            
            % Estimate the uncertaint as the average of the four smallest
            % differences and report as percent
            extrapUncert=mean(diff(idx)).*100;
        end % uncertExtrap
        
        function invalidUncert=uncertInvalidData(discharge,checked)
        % Computes an estimate of the uncertainty for the discharge
        % computed for invalid bins and ensembles.
        %
        % INPUT:
        %
        % discharge: object of clsQComp
        %
        % checked: logical vector of transects used to compute final
        % discharge.
        %
        % OUTPUT:
        %
        % invalidUncert: estimated 95% uncertainty in the total discharge
        % represented by uncertainty of invalid bins and ensembles
        
            % Compute mean total discharge
            meanQ=clsQAData.meanQ(discharge(checked),'total');
            
            % Compute the percent discharge in invalid cells
            perCells=abs(clsQAData.meanQ(discharge(checked),'intCells')./meanQ).*100;
            
            % Compute the percent discharge in invalid ensembles
            perEns=abs(clsQAData.meanQ(discharge(checked),'intEns')./meanQ).*100;
            
            % Compute the uncertainty for combined invalid cells and
            % ensembles
            invalidUncert=(abs(perCells)+abs(perEns)).*0.2;
        end % invalidUncert
        
        function mbUncert=movingBedUncert(meas)
        % Estimates the 95% uncertainty of the discharge due to the moving-bed
        % tests, moving-bed conditions, and navigation reference.
        %
        % INPUT:
        %
        % meas: object of clsMeasurement
        %
        % OUTPUT:
        % 
        % mbUncert: estimated 95% uncertainty of discharge associated with
        % moving bed conditions
        
            % Compute quality code from moving-bed tests
            code=meas.qa.movingbed.code;
            
            % Use code to assign uncertainty value
            switch code
                case 1
                    mbUncert=1;
                case 2
                    mbUncert=1.5;
                case 3
                    mbUncert=3;
            end % switch code

            % If bottom track is not used then the moving-bed uncertainty is
            % zero.
            if ~strcmp(meas.transects(1).boatVel.selected,'btVel')
                mbUncert=0;
            end
        end % movingBedUncert
        
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
     
                % Create object
                temp=clsUncertainty();
                % Get variable names from structure
                names=fieldnames(structIn);
                % Set properties to structure values
                nMax=length(names);
                for n=1:nMax
                    % Assign property
                    temp=resetProperty(temp, names{n},structIn.(names{n}));
                end % for n
            obj=setProperty(obj,'uncertainty',temp);
        end % reCreate            

    end % static methods
end % class

