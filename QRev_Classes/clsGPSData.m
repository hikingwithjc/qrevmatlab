classdef clsGPSData
    % Class containing the raw GPS data and algorithms to convert that raw
    % data to boat velocity
    
    properties
        % Raw properties
        rawGGALat_deg % Raw latitude in degress, [n,ensemble]
        rawGGALon_deg % Raw longitude in degrees, [n,ensemble]
        rawGGAAltitude_m % Raw altitude in meters, [n,ensemble]
        rawGGADifferential % Differential correction indicatore, [n,ensemble]
        rawGGAHDOP % Horizontal dilution of precision, [n,ensemble]
        rawGGAUTC % UTC time, hhmmss.ss, [n, ensemble]
        rawGGASerialTime % UTC time of gga data in seconds past midnight, [n,ensemble]
        rawGGANumSats % Number of satellites reported in GGA sentence, [n,ensemble]
        rawVTGCourse_deg % Course in degress, [n, ensemble]
        rawVTGSpeed_mps % Speed in m/s, [n, ensemble]
        rawVTGDeltaTime % VTG delta time (sec)
        rawVTGModeIndicator % VTG mode indicator
        rawGGADeltaTime % GGA delta time (sec)
        
        % Manufacturer assigned ensemble values
        extGGALat_deg % Raw latitude in degress, [1,ensemble]
        extGGALon_deg % Raw longitude in degrees, [1,ensemble]
        extGGAAltitude_m % Raw altitude in meters, [1,ensemble]
        extGGADifferential % Differential correction indicatore, [1,ensemble]
        extGGAHDOP % Horizontal dilution of precision, [1,ensemble]
        extGGAUTC % UTC time, hhmmss.ss, [1, ensemble]
        extGGASerialTime % UTC time of gga data in seconds past midnight, [1,ensemble]
        extGGANumSats % Number of satellites reported by software [1,ensemble]
        extVTGCourse_deg % Course in degress, [1, ensemble]
        extVTGSpeed_mps % Speed in m/s, [1, ensemble]
       
        
        % User specification
        ggaPositionMethod % Method used to process gga data for position ('End', 'Average' 'External')
        ggaVelocityMethod % Method used to process gga data for velocity ('End','Average' 'External')
        vtgVelocityMethod % Method used to process vtg data for velocity ('Average' 'External)
        
        % Computed properties for ensembles
        ggaLatEns_deg % Processed latitude in degrees, [1,ensemble]
        ggaLonEns_deg % Processed longitude in degrees, [1,ensemble]
        UTMEns_m % UTM position from processed gga data, [2,ensemble]
        ggaVelocityEns_mps % Boat velocity computed from gga data [2,ensemble]
        ggaSerialTimeEns % UTC time of gga data in seconds past midnight, [1,ensemble]
        vtgVelocityEns_mps % Boat velocity computed from vtg data [2,ensemble]
        perGoodEns % Percentage of available data used to compute ensemble value
        hdopEns % HDOP for each ensemble using velocity method
        numSatsEns % Number of satellites for each ensemble, using velocity method
        altitudeEns_m % Altitude for each ensemble, using velocity method
        diffQualEns % Differential quality for each ensemble, using velocity method    
        
    end % properties
    
    methods
        function obj=clsGPSData(rawGGAutc,rawGGAlat,rawGGAlon,rawGGAalt,rawGGAdiff,...
                rawGGAhdop,rawGGANumSats,rawGGADeltaTime,rawVTGcourse,rawVTGspeed,rawVTGDeltaTime,...
                rawVTGModeIndicator,extGGAutc,extGGAlat,extGGAlon,extGGAalt,extGGAdiff,...
                extGGAhdop,extGGANumSats,extVTGcourse,extVTGspeed,...
                GGApMethod,GGAvMethod,VTGMethod)
        % Constructor function
            % Allow creation of object with no input
            if nargin > 0            
                % Assign input data to raw properities
                if isempty(rawGGAutc)
                    obj.rawGGAUTC=nan(size(rawGGAlat));
                    obj.rawGGASerialTime=nan(size(rawGGAlat));
                else
                    obj.rawGGAUTC=rawGGAutc;            
                    obj.rawGGASerialTime=floor(rawGGAutc./10000).*3600+...
                    floor(mod(rawGGAutc,10000)./100).*60+mod(rawGGAutc,100);
                end
                obj.rawGGALat_deg=rawGGAlat;
                obj.rawGGALon_deg=rawGGAlon;
                obj.rawGGALat_deg(obj.rawGGALat_deg==0 & obj.rawGGALon_deg==0)=nan;
                obj.rawGGALat_deg(rawGGAdiff<1)=nan;
                obj.rawGGALon_deg(isnan(obj.rawGGALat_deg))=nan;
                obj.rawGGAAltitude_m=rawGGAalt;
                obj.rawGGAAltitude_m(isnan(obj.rawGGALat_deg))=nan;
                obj.rawGGADifferential=rawGGAdiff;
                obj.rawGGADifferential(isnan(obj.rawGGALat_deg))=nan;
                obj.rawGGAHDOP=rawGGAhdop;
                obj.rawGGAHDOP(isnan(obj.rawGGALat_deg))=nan;
                obj.rawGGANumSats=rawGGANumSats;
                obj.rawGGANumSats(isnan(obj.rawGGALat_deg))=nan;

                obj.rawGGASerialTime(isnan(obj.rawGGALat_deg))=nan;
                obj.rawGGADeltaTime=rawGGADeltaTime;
                obj.rawVTGCourse_deg=rawVTGcourse;
                obj.rawVTGSpeed_mps=rawVTGspeed;
                obj.rawVTGCourse_deg(obj.rawVTGCourse_deg==0 & obj.rawVTGSpeed_mps==0)=nan;
                obj.rawVTGSpeed_mps(isnan(obj.rawVTGCourse_deg))=nan;
                obj.rawVTGDeltaTime=rawVTGDeltaTime;
                obj.rawVTGModeIndicator=rawVTGModeIndicator;

                % Assign input data to ensemble values computed by other
                % software
                obj.extGGAUTC=extGGAutc;
                obj.extGGALat_deg=extGGAlat;
                obj.extGGALon_deg=extGGAlon;
                obj.extGGAAltitude_m=extGGAalt;
                obj.extGGADifferential=extGGAdiff;
                obj.extGGAHDOP=extGGAhdop;
                obj.extGGANumSats=extGGANumSats;
                obj.extGGASerialTime=floor(extGGAutc./10000).*3600+...
                    floor(mod(extGGAutc,10000)./100).*60+mod(extGGAutc,100);
                obj.extVTGCourse_deg=extVTGcourse;
                obj.extVTGSpeed_mps=extVTGspeed;

                % % Assign input data to method properities
                obj.ggaPositionMethod=GGApMethod;
                obj.ggaVelocityMethod=GGAvMethod;
                obj.vtgVelocityMethod=VTGMethod;

                % If GGA data exist compute position and velocity
                if sum(sum(~isnan(rawGGAlat)))>0 
                    obj=processGGA(obj);
                end
                % If VTG data exist compute velocity
                if sum(sum(~isnan(rawVTGspeed)))>0
                    obj=processVTG(obj);  
                end
            end % nargin
        end % constructor
        
        function obj=processGGA(obj,varargin)
        % Function to compute boat velocity from GGA data. Varargin is used
        % to specify the methods for computing the position and velocity.
        
            % Determine methods to be applied
            if ~isempty(varargin)
                pSetting=varargin{1};
                vSetting=varargin{2};
            else
                pSetting=obj.ggaPositionMethod;
                vSetting=obj.ggaVelocityMethod;
            end % varargin
            
            % Use only valid GGA data
            valid=obj.rawGGANumSats;
            valid(isnan(valid))=0;
            valid(valid>0)=1;
            GGALat_deg=obj.rawGGALat_deg;
            GGALat_deg(~valid)=nan;
            GGALon_deg=obj.rawGGALon_deg;
            GGALon_deg(~valid)=nan;
            GGASerialTime=obj.rawGGASerialTime;
            GGASerialTime(~valid)=nan;
            GGADeltaTime=obj.rawGGADeltaTime;
            GGADeltaTime(~valid)=nan;
            GGAHDOP=obj.rawGGAHDOP;
            GGAHDOP(~valid)=nan;
            GGANumSats=obj.rawGGANumSats;
            GGANumSats(~valid)=nan;
            GGAAltitude_m=obj.rawGGAAltitude_m;
            GGAAltitude_m(~valid)=nan;
            GGADifferential=obj.rawGGADifferential;
            GGADifferential(~valid)=nan;
            
            % Apply method for computing position of ensemble
            switch pSetting

                % Use ensemble data from other software
                case 'External'
                    obj.ggaLatEns_deg=obj.extGGALat_deg;
                    obj.ggaLonEns_deg=obj.extGGALon_deg;
                % Uses last valid data for each ensemble
                case 'End'
                    nEnsembles=size(GGALat_deg,1);
                    for n=1:nEnsembles
                        idx=find(~isnan(GGALat_deg(n,:)),1,'last');
                        if isempty(idx)
                            idx=1;
                        end
                        obj.ggaLatEns_deg(n)=GGALat_deg(n,idx);
                        obj.ggaLonEns_deg(n)=GGALon_deg(n,idx);
                    end % for
                case 'First'
                    nEnsembles=size(GGALat_deg,1);
                    for n=1:nEnsembles
                        idx=1;
                        obj.ggaLatEns_deg(n)=GGALat_deg(n,idx);
                        obj.ggaLonEns_deg(n)=GGALon_deg(n,idx);
                    end % for
                % Averages all position collected during the ensemble
                case 'Average'
                    obj.ggaLatEns_deg=nanmean(GGALat_deg,2);
                    obj.ggaLonEns_deg=nanmean(GGALon_deg,2);
                case 'Mindt'                 
                   dTime=abs(obj.rawGGADeltaTime);
                   dTime(~valid)=nan;
                   dTimeMin=nanmin(dTime')';
                   for n=1:length(dTimeMin)
                        use(n,:)=abs(dTime(n,:))==dTimeMin(n);
                   end
                    obj.ggaLatEns_deg=nan(1,length(dTimeMin));
                    obj.ggaLonEns_deg=nan(1,length(dTimeMin));
                    for n=1:length(dTimeMin)
                        idx=find(use(n,:)==true,1,'first');
                        if ~isempty(idx)
                            obj.ggaLatEns_deg(n)=GGALat_deg(n,idx);
                            obj.ggaLonEns_deg(n)=GGALon_deg(n,idx);
                        end % if idx
                    end % for n                    
            end % switch pSetting
            
            % Computes UTM position from processed lat and lon
            [yUTM,xUTM]=clsGPSData.computeUTM(obj.ggaLatEns_deg,obj.ggaLonEns_deg);
            obj.UTMEns_m=[xUTM;yUTM];
            
             nEnsembles=size(obj.rawGGALat_deg,1);
             lat=nan(1,nEnsembles);
             lon=nan(1,nEnsembles);
             obj.ggaSerialTimeEns=nan(1,nEnsembles);
             obj.altitudeEns_m=nan(1,nEnsembles);
             obj.diffQualEns=nan(1,nEnsembles);
             obj.hdopEns=nan(1,nEnsembles);
             obj.numSatsEns=nan(1,nEnsembles);
            
            % Apply method for computing velocity of ensemble
            switch vSetting
                % Use lat and lon for each ensemble determined by external
                % software
                case 'External'
                    lat=obj.extGGALat_deg;
                    lon=obj.extGGALon_deg;
                    obj.ggaSerialTimeEns=obj.extGGASerialTime;
                    obj.hdopEns=obj.extGGAHDOP;
                    obj.numSatsEns=obj.extGGANumSats;
                    obj.altitudeEns_m=obj.extGGAAltitude_m;
                    obj.diffQualEns=obj.extGGADifferential;
                % Average all positions during an ensemble    
                case 'Average'
                    lat=nanmean(GGALat_deg,2);
                    lon=nanmean(GGALon_deg,2);
                    obj.ggaSerialTimeEns=nanmean(GGASerialTime,1);
                    obj.hdopEns=nanmean(GGAHDOP,1);
                    obj.numSatsEns=floor(nanmean(GGANumSats,1));
                    obj.altitudeEns_m=nanmean(obj.rawGGAAltitude_m,1);
                    obj.diffQualEns=floor(nanmean(obj.rawGGADifferential,1));
                % Use the last valid data in an ensemble
                case 'End'
                     for n=1:nEnsembles
                        idx=find(~isnan(GGALat_deg(n,:)),1,'last');
                         if ~isempty(idx)
                            lat(n)=GGALat_deg(n,idx);
                            lon(n)=GGALon_deg(n,idx);
                            obj.ggaSerialTimeEns(n)=GGASerialTime(n,idx);                        
                            obj.altitudeEns_m(n)=GGAAltitude_m(n,idx);
                            obj.diffQualEns(n)=GGADifferential(n,idx);
                         end % if idx

                         if ~isempty(obj.rawGGAHDOP(n,idx))
                            obj.hdopEns(n)=GGAHDOP(n,idx);
                         end % if rawGGAHDOP
                        
                         if ~isempty(GGANumSats(n,idx))
                            obj.numSatsEns(n)=GGANumSats(n,idx);
                         end % if rawGGANumSats
                    end % for n
                case 'First'
                    for n=1:nEnsembles
                        idx=1;
                        if ~isempty(idx)
                            lat(n)=GGALat_deg(n,idx);
                            lon(n)=GGALon_deg(n,idx);
                            obj.ggaSerialTimeEns(n)=GGASerialTime(n,idx);                        
                            obj.altitudeEns_m(n)=GGAAltitude_m(n,idx);
                            obj.diffQualEns(n)=GGADifferential(n,idx);
                        end % if idx
                        if ~isempty(GGAHDOP(n,idx))
                           obj.hdopEns(n)=GGAHDOP(n,idx);
                        end % if rawGGAHDOP

                        if ~isempty(GGANumSats(n,idx))
                           obj.numSatsEns(n)=GGANumSats(n,idx);
                        end % if rawGGANumSats
                    end % for n
                    
                case 'Mindt'                 
                   dTime=abs(GGADeltaTime);
                   dTimeMin=nanmin(dTime')';
                   for n=1:length(dTimeMin)
                        use(n,:)=abs(dTime(n,:))==dTimeMin(n);
                    end

                    for n=1:length(dTimeMin)
                        idx=find(use(n,:)==true,1,'first');
                        if ~isempty(idx)
                            lat(n)=GGALat_deg(n,idx);
                            lon(n)=GGALon_deg(n,idx);
                            obj.ggaSerialTimeEns(n)=GGASerialTime(n,idx);                        
                            obj.altitudeEns_m(n)=GGAAltitude_m(n,idx);
                            obj.diffQualEns(n)=GGADifferential(n,idx);
                        end % if idx
                        if ~isempty(GGAHDOP(n,idx))
                           obj.hdopEns(n)=GGAHDOP(n,idx);
                        end % if rawGGAHDOP

                        if ~isempty(GGANumSats(n,idx))
                           obj.numSatsEns(n)=GGANumSats(n,idx);
                        end % if rawGGANumSats
                    end % for n
 
            end % switch vSetting
            
            % Identify valid values
            idxValues=find(~isnan(xUTM));
            if length(idxValues)>1
                [u,v]=clsGPSData.gga2VelTRDI(lat,lon,obj.ggaSerialTimeEns,idxValues);
                obj.ggaVelocityEns_mps=nan(2,length(lat));
                obj.ggaVelocityEns_mps(1,idxValues(2:end))=u(idxValues(2:end));
                obj.ggaVelocityEns_mps(2,idxValues(2:end))=v(idxValues(2:end));
            else
                obj.ggaVelocityEns_mps=nan(2,length(lat));
            end
            
        end % processGGA
        
        function obj=processVTG(obj,varargin)
        % Processes raw vtg data to achieve a velocity for each ensemble 
        % containing data    
  
           % Determine method used to compute ensemble velocity
           if isempty(varargin)
               setting=obj.vtgVelocityMethod;
           else 
               setting=varargin{1};
           end % varargin 

           % Use only valid data
           VTGSpeed_mps=obj.rawVTGSpeed_mps;
           VTGCourse_deg=obj.rawVTGCourse_deg;
           VTGDeltaTime=obj.rawVTGDeltaTime;
           idx=find(obj.rawVTGModeIndicator=='N');
           VTGSpeed_mps(idx)=nan;
           VTGCourse_deg(idx)=nan;
           VTGDeltaTime(idx)=nan;

           switch setting
               case 'Average'            
                   % Compute vtg velocity in x y coordinates
                   dir=azdeg2rad(VTGCourse_deg);
                   [vx,vy]=pol2cart(dir,VTGSpeed_mps);
                   vx(vx==0 & vy==0)=nan;
                   vy(isnan(vx))=nan;
                   vxMean=nanmean(vx,2);
                   vyMean=nanmean(vy,2);
                   obj.vtgVelocityEns_mps=[vxMean';vyMean'];
                   
               case 'End'
                    nEnsembles=size(VTGSpeed_mps,1);
                    for n=1:nEnsembles
                        idx=find(~isnan(VTGSpeed_mps(n,:)),1,'last');
                        if isempty(idx)
                            idx=1;
                        end
                        vtgVel(n)=VTGSpeed_mps(n,idx);
                        vtgDir(n)=VTGCourse_deg(n,idx);
                    end % for  
                    dir=azdeg2rad(vtgDir);
                    [vx,vy]=pol2cart(dir,vtgVel);
                    vx(vx==0 & vy==0)=nan;
                    vy(isnan(vx))=nan;
                    obj.vtgVelocityEns_mps=[vx;vy]; 
               case 'First'
                    nEnsembles=size(VTGSpeed_mps,1);
                    for n=1:nEnsembles
                            idx=1;
                        vtgVel(n)=VTGSpeed_mps(n,idx);
                        vtgDir(n)=VTGCourse_deg(n,idx);
                    end % for  
                    dir=azdeg2rad(vtgDir);
                    [vx,vy]=pol2cart(dir,vtgVel);
                    vx(vx==0 & vy==0)=nan;
                    vy(isnan(vx))=nan;
                    obj.vtgVelocityEns_mps=[vx;vy]; 
               case 'Mindt'                 
                   dTime=abs(VTGDeltaTime);
                   dTimeMin=nanmin(dTime')';
                   for n=1:length(dTimeMin)
                        use(n,:)=abs(dTime(n,:))==dTimeMin(n);
                    end

                    for n=1:length(dTimeMin)
                        idx=find(use(n,:)==true,1,'first');
                        if ~isempty(idx)
                            vtgSpeed(n)=VTGSpeed_mps(n,idx);
                            vtgDir(n)=VTGCourse_deg(n,idx);
                        else
                            vtgSpeed(n)=nan;
                            vtgDir(n)=nan;
                        end
                    end
                    dir=azdeg2rad(vtgDir);
                    [vx,vy]=pol2cart(dir,vtgSpeed);
                    obj.vtgVelocityEns_mps=[vx;vy]; 
                   
               case 'External'
                   dir=azdeg2rad(obj.extVTGCourse_deg);
                   [vx,vy]=pol2cart(dir,obj.extVTGSpeed_mps);
                   obj.vtgVelocityEns_mps=[vx';vy'];                 
           end % switch           
           
        end % processVTG
        
        function obj=setSonTekHDOP(obj,HDOP)
            obj.hdopEns=HDOP;
        end % setSonTekHDOP
        
        function obj=setSonTekSats(obj,sats)
            obj.numSatsEns=sats;
        end % setSonTekSats
    end % methods
            
    methods (Static)   
        function [u,v]=gga2VelTRDI(lat,lon,t,idxValues)
            for n=2:length(idxValues)
                lat1=lat(idxValues(n-1));
                lat2=lat(idxValues(n));
                lon1=lon(idxValues(n-1));
                lon2=lon(idxValues(n));
                t1=t(idxValues(n-1));
                t2=t(idxValues(n));
                L=((lat1+lat2)/2).*pi/180;
                sL=sin(L);
                Coefficient=6378137*pi/180;
                Ellipticity=1/298.257223563;
                RE=Coefficient*(1+Ellipticity*sL*sL);
                RN=Coefficient*(1-2*Ellipticity+3*Ellipticity*sL*sL);
                DeltaX(n)=RE*(lon2-lon1)*cos(L);
                DeltaY(n)=RN*(lat2-lat1);
                DeltaTime(n)=t2-t1;
                if DeltaTime(n) > 0.0001 
                    u(idxValues(n))=DeltaX(n)/DeltaTime(n);
                    v(idxValues(n))=DeltaY(n)/DeltaTime(n);
                else
                    u(idxValues(n)) = nan;
                    v(idxValues(n)) = nan;
                end
            end
        end % gga2VelTRDI
        
        function [xUTM,yUTM]=computeUTM(latIn,lonIn)
        % Function to compute UTM coordinates from latitude and longitude
        % refell and ell2utm are functions published in
        % Copyright (c) 2013, Michael R. Craymer (BSD License)
        % http://www.mathworks.com/matlabcentral/fileexchange/view_license?file_info_id=15285
        
            latIn(latIn==0)=nan;
            lonIn(lonIn==0)=nan;

            % Compute UTM coordinates    
            lat2=latIn(:).*pi./180;
            lon2=lonIn(:).*pi./180;
            [a,b, e2, finv]=refell('WGS84');
            [y,x,zone]=ell2utm(lat2,lon2,a,e2);
            xUTM=reshape(x,size(lonIn));
            yUTM=reshape(y,size(latIn));
        end %computeUTM
        
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
            % Create object
            if ~isempty(structIn)
                obj.gps=clsGPSData();
                % Get variable names from structure
                names=fieldnames(structIn);
                % Set properties to structure values
                nMax=length(names);
                for n=1:nMax
                    obj.gps.(names{n})=structIn.(names{n});
                end
            end
        end % reCreate        
    end
    
end

