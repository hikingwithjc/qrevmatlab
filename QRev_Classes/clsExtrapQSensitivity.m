classdef clsExtrapQSensitivity
    % Class to compute the sensitivity of the discharge to various
    % extrapolation methods.
    
    properties(SetAccess='private')
       qPPmean         % discharge power power 1/6
       qPPoptmean      % discharge power power optimized
       qCNSmean        % discharge constant no slip
       qCNSoptmean     % discharge constant optimized no slip
       q3pNSmean       % discharge 3-pt no slip
       q3pNSoptmean    % discharge 3-pt optimized no slip
       qPPperdiff      % power power 1/6 difference from reference
       qPPoptperdiff   % power power optimized percent difference from reference     
       qCNSperdiff     % constant no slip percent difference from reference
       qCNSoptperdiff  % constant optimized no slip percent difference from reference
       q3pNSperdiff    % 3-point no slip percent difference from reference
       q3pNSoptperdiff % 3-point optimized no slip precent difference from reference
       ppExponent      % optimized power power exponent
       nsExponent      % optimized no slip exponent
       manTop          % manually specified top method
       manBot          % manually specified bottom method
       manExp          % manually specified exponent
       qManmean        % mean discharge for manually specified extrapolations
       qManperdiff     % manually specified extrapolations percent difference from reference
    end % properties
    
    methods
        function obj=clsExtrapQSensitivity(transData,extrapFits)
        % Constructor function to compute the discharge sensitivity for 
        % various extrapolation options.    
            % Allow creation of object with no input
            if nargin > 0        
                % Determine number of transects
                nTransects=length(transData);
                measIdx=nTransects+1;

                % Determine transects used in the measurement
                checked=logical([transData(:).checked]);

                % Set optimized exponents for power and no slip extrapolations
                obj.ppExponent=extrapFits(end).ppexponent;
                obj.nsExponent=extrapFits(end).nsexponent;

                % Compute discharge for possible combinations
                qPP=clsQComp(transData,'Power','Power',0.1667);
                qPPopt=clsQComp(transData,'Power','Power',obj.ppExponent);
                qCNS=clsQComp(transData,'Constant','No Slip',0.1667);
                qCNSopt=clsQComp(transData,'Constant','No Slip',obj.nsExponent);
                q3PNS=clsQComp(transData,'3-Point','No Slip',0.1667);
                q3PNSopt=clsQComp(transData,'3-Point','No Slip',obj.nsExponent); 

                % Compute mean discharges
                obj.qPPmean=nanmean(([qPP(checked).total]));
                obj.qPPoptmean=nanmean(([qPPopt(checked).total]));
                obj.qCNSmean=nanmean(([qCNS(checked).total]));
                obj.qCNSoptmean=nanmean(([qCNSopt(checked).total]));
                obj.q3pNSmean=nanmean(([q3PNS(checked).total]));
                obj.q3pNSoptmean=nanmean(([q3PNSopt(checked).total]));

                obj=computePerDiff(obj,extrapFits,transData);
                
            end % nargin
        end % constructor
        
        function obj=computePerDiff(obj,extrapFits,varargin)
            % Determine which mean is the reference
                if strcmpi(extrapFits(end).fitMethod,'Manual')
                    obj.manTop=extrapFits(end).topMethod;
                    obj.manBot=extrapFits(end).botMethod;
                    obj.manExp=extrapFits(end).exponent;
                    if ~isempty(varargin)
                        transData=varargin{1};
                        qMan=clsQComp(transData,obj.manTop,obj.manBot,obj.manExp);
                        % Determine transects used in the measurement
                        checked=logical([transData(:).checked]);
                        obj.qManmean=nanmean(([qMan(checked).total]));
                    end
                    referenceMean = obj.qManmean;
                else
                    if strcmpi(extrapFits(end).topMethodAuto,'Power')
                        if abs(extrapFits(end).exponentAuto-0.1667)<0.0001
                            referenceMean = obj.qPPmean;
                        else
                            referenceMean = obj.qPPoptmean;
                        end
                    elseif strcmpi(extrapFits(end).topMethodAuto,'Constant')
                        if abs(extrapFits(end).exponentAuto-0.1667)<0.0001
                            referenceMean = obj.qCNSmean;
                        else
                            referenceMean = obj.qCNSoptmean;
                        end
                    else
                        if abs(extrapFits(end).exponentAuto-0.1667)<0.0001
                            referenceMean = obj.q3pNSmean;
                        else
                            referenceMean = obj.q3pNSoptmean;
                        end
                    end    
                end % if manual

                
                % Determine all of the percent differences based on the
                % reference mean
                obj.qPPperdiff=((obj.qPPmean-referenceMean)./referenceMean).*100;
                obj.qPPoptperdiff=((obj.qPPoptmean-referenceMean)./referenceMean).*100;
                obj.qCNSperdiff=((obj.qCNSmean-referenceMean)./referenceMean).*100; 
                obj.qCNSoptperdiff=((obj.qCNSoptmean-referenceMean)./referenceMean).*100;
                obj.q3pNSperdiff=((obj.q3pNSmean-referenceMean)./referenceMean).*100; 
                obj.q3pNSoptperdiff=((obj.q3pNSoptmean-referenceMean)./referenceMean).*100;
                
                %if manual exists determine its percent difference
                if strcmpi(extrapFits(end).fitMethod,'Manual')
                    obj.qManperdiff=((obj.qManmean-referenceMean)./referenceMean).*100;
                end
        end % computePerDiff
        
        function obj=resetProperty(obj,property,setting)
            obj.(property)=setting;
        end        
    end % methods
    
methods (Static)
        
        function obj=reCreate(structIn)
        % Creates object from structured data of class properties
                % Create object
                obj=clsExtrapQSensitivity();
                % Get variable names from structure
                names=fieldnames(structIn.qSensitivity);
                % Set properties to structure values
                nMax=length(names);
                for n=1:nMax
                    % Assign property
                    obj=resetProperty(obj, names{n},structIn.qSensitivity.(names{n}));
                end % for n
                obj=computePerDiff(obj,structIn.selFit);
        end % reCreate        
    end % static methods       
end % class

