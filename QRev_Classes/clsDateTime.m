classdef clsDateTime
% This class stores the date and time data
    
    properties
        date % measurement date mm/dd/yyyy
        startSerialTime % Matlab serial time for start of transect
        endSerialTime % Matlab serial time for end of transect
        transectDuration_sec % duration of transect in seconds
        ensDuration_sec % duration of each ensemble in seconds
    end % properties
    
    methods
        function obj=clsDateTime(dateIn,startIn,endIn,ensDurIn)
            % Allow creation of object with no input
            if nargin > 0            
                obj.date=dateIn;
                obj.startSerialTime=startIn;
                obj.endSerialTime=endIn;
                obj.transectDuration_sec=(endIn-startIn).*(24.*60.*60);
                obj.ensDuration_sec=ensDurIn;
            end % nargin
        end % constructor
        
        function obj=setProperty(obj,property,setting)
            obj.(property)=setting;
        end        
    end
    
    methods (Static)
        function serialTime=time2SerialTime(timeIn, source)
            if strcmpi(source,'TRDI')
                serialTime=719529+timeIn./(60*60*24);
            else
                serialTime=719529+10957+timeIn./(60*60*24);
            end % if  
        end % serialTime
        
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
            % Create object
            obj.dateTime=clsDateTime();
            % Get variable names from structure
            names=fieldnames(structIn);
            % Set properties to structure values
            nMax=length(names);
            for n=1:nMax
                obj.dateTime=setProperty(obj.dateTime,names{n},structIn.(names{n}));
            end % for n
        end % reCreate        
    end % methods
    
end % class

