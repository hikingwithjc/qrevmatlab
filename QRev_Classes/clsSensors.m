classdef clsSensors
% Container for data structures and data from various sensors
    
    properties
        heading_deg % object of clsHeadingData
        pitch_deg % pitch data, object of clsSensorStructure
        roll_deg % roll data, object of clsSensorStructure
        temperature_degC % temperature data, object of clsSensorStructure
        salinity_ppt % salinity data, object of clsSensorStructure
        speedOfSound_mps % speed of sound, object of clsSensorStructure
    end % properties
    
    methods
        % Constructor creates the empty objects for the properties
        function obj=clsSensors()
            obj.heading_deg=clsSensorStructure();
            obj.pitch_deg=clsSensorStructure();
            obj.roll_deg=clsSensorStructure();
            obj.temperature_degC=clsSensorStructure();
            obj.salinity_ppt=clsSensorStructure();
            obj.speedOfSound_mps=clsSensorStructure();           
        end % constructor
        
        function obj=addSensorData(obj,sensorName,sensorType,data,source,varargin)
        % This function will create the appropriate objects for the
        % specified property
            if strcmpi(sensorName,'heading_deg')
                obj.(sensorName).(sensorType)=clsHeadingData(data,source,varargin);
            else
                obj.(sensorName).(sensorType)=clsSensorData(data,source);
            end
        end % addSensorData
        
        function obj=setSelected(obj,sensorName,selectedName)
        % This sets the sensor to be used for computations to the selected
        % source.
            obj.(sensorName)=setSelected(obj.(sensorName),selectedName);
        end % setSelected
        

            
    end % methods 
    
    methods (Static)
        function sos=speedOfSound(temperature,salinity)
            % Speed of Sound
                % Not provided in RS Matlab file computed from equation
                % used in TRDI BBSS.
                sos=1449.2+4.6.*temperature-0.055.*temperature.^2+0.00029.*temperature.^3+(1.34-0.01.*temperature).*(salinity-35);
        end % speedOfSound
         
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
            % Create object
            obj.sensors=clsSensors();
            % Get variable names from structure
            names=fieldnames(structIn);
            % Set properties to structure values
            nMax=length(names);
            for n=1:nMax
                if isstruct(structIn.(names{n}))
                    % Create object
                    obj.sensors=clsSensorStructure.reCreate(obj.sensors,structIn.(names{n}),names{n});
                end
            end % for
        end % reCreate        
    end
end % class

