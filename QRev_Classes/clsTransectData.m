classdef clsTransectData
% This class consolidates data from other classes to form the data set for
% an individual transect.
%
% Originally developed as part of the QRev processing program
% Programmer: David S. Mueller, Hydrologist, USGS, OSW, dmueller@usgs.gov
% Other Contributors: none
% Latest revision: 3/13/2015
    
    properties 
        adcp        % object of clsInstrument
        fileName    % filename of transect data file
        wVel        % object of clsWaterData 
        boatVel     % structure for various boat velocity references including:
                        % selected name of clsBoatData, selected boat reference
                        % btVel  object of clsBoatData
                        % ggaVel object of clsBoatData
                        % vtgVel object of clsBoatData
        gps         % object of clsGPSData
        sensors     % object of clsSensorData
        depths      % object of clsDepthStructure for depth data including cell depths which are only in btDepths & refDepths
                        % selected  name of selected depth source
                        % btDepths object of clsDepthData for bottom track beam depths
                        % vbDepths object of clsDepthData for vertical beam depths
                        % dsDepths object of clsDepthData for depth sounder depths
        edges       % object of clsEdges
                        % left object of clsEdgeData
                        % right object of clsEdgeData
        extrap      % object of clsExtrapData   
        startEdge   % starting edge of transect looking downstream (Left or Right)
        dateTime    % object of clsDateTime
        checked     % transect was checked for use in mmt file assumed checked for SonTek
        inTransectIdx % index of ensemble data associated with the moving-boat portion of the transect
    end % properties
    
    methods
        
        function obj=clsTransectData(source,file,varargin)
        % Constructs object by calling appropriate methods based on manufacturer
        %
        % INPUT:
        %
        % source: manufacturer (TRDI or SonTek)
        %
        % file: object of clsMmtTRDI for TRDI, cell array of mat
        % files, including path, for SonTek
        %
        % varargin: 
        %   varargin{1}: used for TRDI data to determine discharge or
        %   moving-bed test transects are to be loaded.
        
            % Allow creation of object with no input
            if nargin > 0
                
                % Determine source of data
                if strcmp(source,'TRDI') 
                    % TRDI data
                    [obj]=TRDI(obj,file,varargin{:});                  
                    
                elseif strcmp(source,'SonTek')
                    % SonTek data
                    [obj]=SonTek(obj,file);                  
                end % source
                
                % Apply side lobe cutoff
                if ~isempty(obj(1).adcp)
                    obj=adjustSideLobe(obj);
                end
            end % nargin
        end % constructor
        
        function obj=TRDI(obj,mmt,varargin)
        % This function uses the mmt file to identify all the transects
        % associated with the measurement and then use clsPd0TRDI to read
        % the data from the raw Pd0 files. The TransectData object is
        % populated with the appropriate data from the mmt and Pd0 files.
        %
        % INPUT:
        % 
        % obj: object of clsTransectData
        %
        % mmt: object of clsMmtTRDI
        %
        % varargin: 
        %   varargin{1}: used for TRDI data to determine discharge or
        %   moving-bed test transects are to be loaded.
            
            % Set the appropriate variable names depending on the type of
            % transects to be read. 
            % Q - discharge transects 
            % MB - moving-bed test transects            
            if isempty(varargin) | strcmp(varargin{1},'Q')
                transects='transects';
                activeConfig='activeConfig';

                % Option to read only checked transects
                if varargin{2} 
                   files2Load=logical(mmt.transects.Checked);
                else
                    files2Load=ones(size(mmt.transects.Checked));
                end

            elseif strcmp(varargin{1},'MB')
                transects='mbtTransects';
                activeConfig='mbtActiveConfig';
                files2Load=ones(size(mmt.mbtTransects.Checked));
            end
            files2Loadidx=find(files2Load==1);
            
            % Get filenames from mmt file
            fileNames=mmt.(transects).Files;
            fileNames=fileNames(files2Loadidx,1);
            
            % Getting the pathname is dependent on QRev. This could be
            % changed with a pathname input.

%             pathName=getUserPref('Folder');
            pathName=strcat(mmt.path,'\')
            
            % Determine number of files
            numFiles=length(files2Loadidx);
            
            % Determine if any files are missing.
            validFiles=false(1,numFiles);
                for n=1:numFiles
                    fullName=strcat(pathName,fileNames{n});
                    FileInfo=dir(fullName);
                    if ~isempty(FileInfo) > 0
                        if FileInfo.bytes >0
                            validFiles(n)=true;
                        end
                    end
                end
                numValidFiles=sum(validFiles);     
            if numValidFiles>0
                % Preallocate objects to improve speed
                obj(numValidFiles)=clsTransectData();

                % Create a pd0Data object for each transect
                pd0Data=clsPd0TRDI(pathName,fileNames);

                % Process each transect
                % ---------------------
                n=0;
                for k=1:numFiles               

                    % Assign pd0 data to local variable to allow reuse of
                    % previously written code.
                    pd0=pd0Data(k);

                    % Check for valid pd0 structure
                    if isstruct(pd0.Wt)                   
                        n=n+1; 

                        % Set file name
                        obj(n).fileName=fileNames{k};

                        % Create depthData object
                        % -----------------------

                        % Create empty object
                        obj(n).depths=clsDepthStructure();

                        % Get and compute ensemble beam depths
                        tempDepth= pd0.Bt.depth_m;
                        % Screen out invalid depths
                        tempDepth(tempDepth<0.01)=nan;
                        % Add draft
                        tempDepth=tempDepth+mmt.(activeConfig).Offsets_Transducer_Depth(files2Loadidx(k));

                        % Compute cell data
                        [cellSizeAll_m, cellDepth_m, slCutoffPer, slLagEffect_m]=clsTransectData.computeCells(pd0);

                        %[cellSizeAll_m, cellDepth_m, cellsAboveSLbt, slCutoffPer, slLagEffect_m]=computeCellData(pd0);
                        
                        % Adjust cellDepth of draft
                        cellDepth_m=bsxfun(@plus,mmt.(activeConfig).Offsets_Transducer_Depth(files2Loadidx(k)),cellDepth_m);

                        % Create depthData object for BT
                        obj(n).depths=addDepthObject(obj(n).depths,tempDepth, 'BT',...
                            pd0.Inst.freq', mmt.(activeConfig).Offsets_Transducer_Depth(files2Loadidx(k)),...
                            cellDepth_m, cellSizeAll_m);
                        
                        % Compute cells above side lobe
                        cellsAboveSLbt=clsTransectData.sideLobeCutoff(obj(n).depths.btDepths.depthOrig_m,...
                            obj(n).depths.btDepths.draftOrig_m,...
                            obj(n).depths.btDepths.depthCellDepth_m,...
                            slLagEffect_m,...
                            'Percent',...
                            1-slCutoffPer./100);
                        
                        
                        % Check for presence of vertical beam data
                        if nanmax(nanmax(pd0.Sensor.vertBeamStatus))>0
                            tempDepth= pd0.Sensor.vertBeamRange_m;
                            % Screen out invalid depths
                            tempDepth(tempDepth<0.01)=nan;
                            % Add draft
                            tempDepth=tempDepth+mmt.(activeConfig).Offsets_Transducer_Depth(files2Loadidx(k)); 
                            % Create depthData object for vertical beam 
                            obj(n).depths=addDepthObject(obj(n).depths,tempDepth', 'VB',...
                                pd0.Inst.freq', mmt.(activeConfig).Offsets_Transducer_Depth(files2Loadidx(k)),...
                                cellDepth_m, cellSizeAll_m);
                        end % if vertBeam

                        % Check for presence of depth sounder
                        if nansum(nansum(pd0.Gps2.depth_m))>0
                            tempDepth=pd0.Gps2.depth_m;

                            % Screen out invalid depths
                            tempDepth(tempDepth<0.01)=nan;
                            % Use the last valid depth for each ensemble
                            lastDepthColIdx=sum(~isnan(tempDepth),2);
                            lastDepthColIdx(lastDepthColIdx==0)=1;
                            rowIndex=1:size(tempDepth,1);
                            idx=sub2ind(size(tempDepth),rowIndex',lastDepthColIdx);
                            lastDepth=tempDepth(idx);

                            % Determine if mmt file has a scale factor and
                            % offset for the depth sounder
                            if mmt.(activeConfig).DS_Cor_Spd_Sound(files2Loadidx(k))==0
                                scaleFactor=mmt.(activeConfig).DS_Scale_Factor;
                            else
                                scaleFactor=pd0.Sensors.sos_mps./1500;
                            end % if 

                            % Apply scale factor, offset, and draft                        
                            % Note: Only the ADCP draft is stored. The transducer
                            % draft or scaling for depth sounder data cannont be changed in QRev.
                            dsDepth=(lastDepth.*scaleFactor(k))...
                                    + mmt.(activeConfig).DS_Transducer_Depth(files2Loadidx(k))...
                                    + mmt.(activeConfig).DS_Transducer_OFF(files2Loadidx(k));

                            % Create depthData object for depth sounder
                            obj(n).depths=addDepthObject(obj(n).depths,dsDepth', 'DS',...
                                pd0.Inst.freq', mmt.(activeConfig).Offsets_Transducer_Depth(files2Loadidx(k)),...
                                cellDepth_m, cellSizeAll_m);
                        end % if depth sounder

                        % Set depth reference to value from mmt file
                        if isfield(mmt.(activeConfig),'Proc_River_Depth_Source')
                            switch mmt.(activeConfig).Proc_River_Depth_Source(files2Loadidx(k))
                                case 0
                                    obj(n).depths=setDepthReference(obj(n).depths,'BT');
                                    obj(n).depths=setProperty(obj(n).depths,'composite','Off');
                                case 1
                                    if ~isempty(obj(n).depths.dsDepths)
                                        obj(n).depths=setDepthReference(obj(n).depths,'DS');
                                        obj(n).depths=setProperty(obj(n).depths,'composite','Off');
                                    else
                                        obj(n).depths=setDepthReference(obj(n).depths,'BT');
                                        obj(n).depths=setProperty(obj(n).depths,'composite','Off');
                                    end
                                case 2
                                    if ~isempty(obj(n).depths.vbDepths)
                                        obj(n).depths=setDepthReference(obj(n).depths,'VB');
                                        obj(n).depths=setProperty(obj(n).depths,'composite','Off');
                                    else
                                        obj(n).depths=setDepthReference(obj(n).depths,'BT');
                                        obj(n).depths=setProperty(obj(n).depths,'composite','Off');
                                    end
                                case 3
                                    if isempty(obj(n).depths.vbDepths)
                                        obj(n).depths=setDepthReference(obj(n).depths,'BT');
                                        obj(n).depths=setProperty(obj(n).depths,'composite','Off');
                                    else
                                        obj(n).depths=setDepthReference(obj(n).depths,'VB');
                                        obj(n).depths=setProperty(obj(n).depths,'composite','On');
                                    end
                                case 4
                                     if ~isempty(obj(n).depths.btDepths)
                                        obj(n).depths=setDepthReference(obj(n).depths,'BT');
                                        obj(n).depths=setProperty(obj(n).depths,'composite','Off');
                                        % I think this should be On DSM 2/27/2018
                                     elseif ~isempty(obj(n).depths.vbDepths)
                                        obj(n).depths=setDepthReference(obj(n).depths,'VB');
                                        obj(n).depths=setProperty(obj(n).depths,'composite','On');
                                     elseif ~isempty(obj(n).depths.dsDepths)
                                         obj(n).depths=setDepthReference(obj(n).depths,'DS');
                                        obj(n).depths=setProperty(obj(n).depths,'composite','On');
                                     end
                                 otherwise
                                    obj(n).depths=setDepthReference(obj(n).depths,'BT');
                                    obj(n).depths=setProperty(obj(n).depths,'composite','Off'); 
                            end % switch
                        else
                            if mmt.(activeConfig).DS_Use_Process(files2Loadidx(k))>0
                                if ~isempty(obj(n).depths.dsDepths)
                                    obj(n).depths=setDepthReference(obj(n).depths,'DS');
                                else
                                    obj(n).depths=setDepthReference(obj(n).depths,'BT');
                                end
                            else
                                obj(n).depths=setDepthReference(obj(n).depths,'BT');
                            end
                            obj(n).depths=setProperty(obj(n).depths,'composite','Off');
%                             obj(n).depths=compositeDepths(obj(n).depths,'Off');
                        end % if depth source


                        % Create waterData object
                        % ----------------------- 

                        % Check for RiverRay and RiverPro data
                        firmware=num2str(pd0.Inst.firmVer(1));
                        excludedDist=0;
                        if strcmp(firmware(1:2),'56') && nanmax(isnan(pd0.Sensor.vertBeamStatus))
                            excludedDist=0.25;
                        end
                        
                        if strcmp(firmware(1:2),'44') || strcmp(firmware(1:2),'56')

                            % Process water velocities for RiverRay and
                            % RiverPro
                            obj(n).wVel=clsWaterData(pd0.Wt.vel_mps,pd0.Inst.freq',...
                                    pd0.Cfg.coordSys(1,:),'None',pd0.Wt.rssi,'Counts',excludedDist,...
                                    cellsAboveSLbt, slCutoffPer,0,'Percent',slLagEffect_m,pd0.Cfg.wm(1),pd0.Cfg.wf_cm(1)./100, pd0.Wt.corr,...
                                    pd0.Surface.vel_mps,pd0.Surface.rssi,pd0.Surface.corr,...
                                    pd0.Surface.no_cells);               
                        else 
                            % Process water velocities for non-RiverRay ADCPs
                            obj(n).wVel=clsWaterData(pd0.Wt.vel_mps,pd0.Inst.freq',...
                                pd0.Cfg.coordSys(1,:),'None',pd0.Wt.rssi,'Counts',excludedDist,...
                                cellsAboveSLbt, slCutoffPer,0,'Percent',slLagEffect_m, pd0.Cfg.wm(1), pd0.Cfg.wf_cm(1)./100,pd0.Wt.corr);
                        end % if instrument type
                        
                        % Adjust original valid data for excluded data setting
                        %obj(n).wVel=adjustOriginalDataFilter(obj(n).wVel,obj(n));

                        % Create boatData object for BT
                        % -----------------------------

                        % Intialize object
                        obj(n).boatVel=clsBoatStructure();

                        % Process bottom track data
                        obj(n).boatVel=addBoatObject(obj(n).boatVel,'TRDI',pd0.Bt.vel_mps,pd0.Inst.freq',...
                            pd0.Cfg.coordSys(1,:),'BT',mmt.(activeConfig).Proc_Use_3_Beam_BT(files2Loadidx(k)),pd0.Cfg.bm(1));

                        % Set navigation reference to BT
                        obj(n).boatVel=setNavReference(obj(n).boatVel,'BT');

                        % Compute velocities from GPS data
                        % --------------------------------

                        % Raw data
                        rawGGAutc=pd0.Gps2.utc;
                        rawGGAlat=pd0.Gps2.lat_deg;

                        % Determine correct sign for latitude
                        idx=find(pd0.Gps2.latRef=='S');
                        if ~isempty(idx)
                            rawGGAlat(idx)=rawGGAlat(idx).*-1;
                        end
                        rawGGAlon=pd0.Gps2.lon_deg;

                        % Determine correct sign for longitude
                        idx=find(pd0.Gps2.lonRef=='W');
                        if ~isempty(idx)
                            rawGGAlon(idx)=rawGGAlon(idx).*-1;
                        end

                        % Assign data to local variables
                        rawGGAalt=pd0.Gps2.alt;
                        rawGGAdiff=pd0.Gps2.corrQual;
                        rawGGAhdop=pd0.Gps2.hdop;
                        rawGGANumSats=pd0.Gps2.numSats;
                        rawVTGcourse=pd0.Gps2.courseTrue;
                        rawVTGspeed=pd0.Gps2.speedKmph.*0.2777778;
                        rawVTGDeltaTime=pd0.Gps2.vtgDeltaTime;
                        rawVTGModeIndicator=pd0.Gps2.modeIndicator;
                        rawGGADeltaTime=pd0.Gps2.ggaDeltaTime;

                        % RSL provided ensemble values, not supported for TRDI
                        % data
                        extGGAutc=[];
                        extGGAlat=[];
                        extGGAlon=[];
                        extGGAalt=[];
                        extGGAdiff=[];
                        extGGAhdop=[];
                        extGGANumSats=[];
                        extVTGcourse=[];
                        extVTGspeed=[];

                        % QRev methods GPS processing methods
                        GGApMethod='Mindt';
                        GGAvMethod='Mindt';
                        VTGMethod='Mindt';

                        % If valid gps data exist, process the data
                        if sum(sum(~isnan(rawGGAlat)))>0 | sum(sum(~isnan(rawVTGspeed)))>0

                            % Process raw GPS data
                            obj(n).gps=clsGPSData(rawGGAutc,rawGGAlat,rawGGAlon,rawGGAalt,...
                            rawGGAdiff,rawGGAhdop,rawGGANumSats,rawGGADeltaTime,rawVTGcourse,rawVTGspeed,rawVTGDeltaTime,...
                            rawVTGModeIndicator,extGGAutc,extGGAlat,extGGAlon,extGGAalt,extGGAdiff,...
                            extGGAhdop,extGGANumSats,extVTGcourse,extVTGspeed,...
                            GGApMethod,GGAvMethod,VTGMethod);

                            % If valid gga data exists create GGA boat velocity
                            % object
                            if sum(sum(~isnan(rawGGAlat)))>0 
                                obj(n).boatVel=addBoatObject(obj(n).boatVel,'TRDI',obj(n).gps.ggaVelocityEns_mps,nan,...
                                    'Earth','GGA');
                            end % if GGA data

                            % If valid vtg data exist, create VTG boat velocity
                            % object
                            if sum(sum(~isnan(rawVTGspeed)))>0
                                obj(n).boatVel=addBoatObject(obj(n).boatVel,'TRDI',obj(n).gps.vtgVelocityEns_mps,nan,...
                                'Earth','VTG',nan,nan);
                            end % if VTG data
                        end % if GPS data

                        % Create edges object
                        % -------------------

                        % Create edge object
                        obj(n).edges=clsEdges('Fixed','MeasMag');

                        % Determine number of ensembles to average
                        nensL=mmt.(activeConfig).Q_Shore_Pings_Avg(files2Loadidx(k));
                        % TRDI uses same number on left and right edges
                        nensR=nensL;

                        % Set indices for ensembles in the moving-boat
                        % portion of the transect
                        obj(n).inTransectIdx=(1:1:size(pd0.Bt.vel_mps,2));

                        % Determine left and right edge distances
                        if mmt.(activeConfig).Edge_Begin_Left_Bank(files2Loadidx(k))
                            distL=mmt.(activeConfig).Edge_Begin_Shore_Distance(files2Loadidx(k));
                            distR=mmt.(activeConfig).Edge_End_Shore_Distance(files2Loadidx(k));
                            obj(n).startEdge='Left';
                        else
                            distL=mmt.(activeConfig).Edge_End_Shore_Distance(files2Loadidx(k));
                            distR=mmt.(activeConfig).Edge_Begin_Shore_Distance(files2Loadidx(k));
                            obj(n).startEdge='Right';
                        end % if Begin_Left_Bank

                        % Create edge

                         % Create left edge object
                        switch mmt.(activeConfig).Q_Left_Edge_Type(files2Loadidx(k))
                            % Triangular edge
                            case 0
                                obj(n).edges=createEdge (obj(n).edges,'left','Triangular', distL, nensL);
                            % Rectangular edge
                            case 1
                                obj(n).edges=createEdge (obj(n).edges,'left','Rectangular', distL, nensL);
                            % Custom coefficient
                            case 2
                                coef=mmt.(activeConfig).Q_Left_Edge_Coeff(files2Loadidx(k));
                                obj(n).edges=createEdge (obj(n).edges,'left','Custom', distL, coef, nensL);
                        end % switch methodL

                        % Create right edge object
                        switch mmt.(activeConfig).Q_Right_Edge_Type(files2Loadidx(k))
                            % Triangular edge
                            case 0
                                obj(n).edges=createEdge (obj(n).edges,'right','Triangular', distR, nensR);
                            % Rectangular edge
                            case 1
                                obj(n).edges=createEdge (obj(n).edges,'right','Rectangular', distR, nensR); 
                            % Custom coefficient
                            case 2
                                coef=mmt.(activeConfig).Q_Right_Edge_Coeff(files2Loadidx(k));
                                obj(n).edges=createEdge (obj(n).edges,'right','Custom', distR, coef, nensR);
                        end % switch methodR

                        % Create extrap object
                        % --------------------

                        % Determine top method
                        switch mmt.(activeConfig).Q_Top_Method(files2Loadidx(k))
                            case 0
                                top='Power';
                            case 1
                                top='Constant';
                            case 2
                                top='3-Point';
                        end

                        % Determine bottom method
                        switch mmt.(activeConfig).Q_Bottom_Method(files2Loadidx(k))
                            case 0
                                bot='Power';
                            case 2
                                bot='No Slip';
                        end

                        % Create object
                        obj(n).extrap=clsExtrapData(top,bot,mmt.(activeConfig).Q_Power_Curve_Coeff(files2Loadidx(k)));

                        % Sensor Data
                        % -----------

                        % Intialize sensor object
                        obj(n).sensors=clsSensors();

                        % Heading
                        
                        % Internal heading
                        heading=pd0.Sensor.heading_deg';
                        headingSrc=pd0.Cfg.headSrc(1,:); 
                        
                        % WR2 only has one set of magvar and heading offset
                        magvar=mmt.(activeConfig).Offsets_Magnetic_Variation(files2Loadidx(k));
                        headingOffset=mmt.(activeConfig).Ext_Heading_Offset(files2Loadidx(k));
                        
                        % Create internal heading sensor
                        obj(n).sensors=addSensorData(obj(n).sensors,'heading_deg','internal',heading,headingSrc,magvar,headingOffset);
                       
                        % External heading
                        extHeadingCheck=find(~isnan(pd0.Gps2.heading_deg));
                        if isempty(extHeadingCheck)
                            % If no external heading set heading source to
                            % internal.
                            obj(n).sensors=setSelected(obj(n).sensors,'heading_deg','internal');
                        else
                           % Determine external heading for each ensemble
                           % using the minimum time difference.
                           dTime=abs(pd0.Gps2.hdtDeltaTime);
                           dTimeMin=nanmin(dTime')';
                           use=nan(size(dTime));
                           for ndTime=1:length(dTimeMin)
                                use(ndTime,:)=abs(dTime(ndTime,:))==dTimeMin(ndTime);
                           end
                            extHeading_deg=nan(1,length(dTimeMin));
                            for nH=1:length(dTimeMin)
                                idx=find(use(nH,:)==true,1,'first');
                                if ~isempty(idx)
                                    extHeading_deg(nH)=pd0.Gps2.heading_deg(nH,idx);
                                end % if idx
                            end % for n      
                            
                            % Create external heading sensor
                            obj(n).sensors=addSensorData(obj(n).sensors,'heading_deg','external',extHeading_deg,'GPS',magvar,headingOffset);
                            
                            % Determine heading source to use from mmt
                            % setting
                            sourceUsed=mmt.(activeConfig).Ext_Heading_Use(files2Loadidx(k));
                            if sourceUsed
                                obj(n).sensors=setSelected(obj(n).sensors,'heading_deg','external');
                            else
                                obj(n).sensors=setSelected(obj(n).sensors,'heading_deg','internal');
                            end 
                        end
                        
                        % Pitch
                        pitch=atand(tand(pd0.Sensor.pitch_deg).*cosd(pd0.Sensor.roll_deg))';
                        pitchSrc=pd0.Cfg.pitchSrc(1,:);   
                        % Create pitch sensor
                        obj(n).sensors=addSensorData(obj(n).sensors,'pitch_deg','internal',pitch,pitchSrc);
                        obj(n).sensors=setSelected(obj(n).sensors,'pitch_deg','internal');

                        % Roll
                        roll=pd0.Sensor.roll_deg';
                        rollSrc=pd0.Cfg.rollSrc(1,:); 
                        % Create roll sensor
                        obj(n).sensors=addSensorData(obj(n).sensors,'roll_deg','internal',roll,rollSrc);
                        obj(n).sensors=setSelected(obj(n).sensors,'roll_deg','internal');

                        % Temperature
                        temperature=pd0.Sensor.temperature_degc';
                        temperatureSrc=pd0.Cfg.tempSrc(1,:); 
                        % Create temperature sensor
                        obj(n).sensors=addSensorData(obj(n).sensors,'temperature_degC','internal',temperature,temperatureSrc);
                        obj(n).sensors=setSelected(obj(n).sensors,'temperature_degC','internal');

                        % Salinity
                        pd0Salinity=pd0.Sensor.salinity_ppt';
                        pd0SalinitySrc=pd0.Cfg.salSrc(1,:);
                        
                        % Create salinity sensor
                        obj(n).sensors=addSensorData(obj(n).sensors,'salinity_ppt','internal',pd0Salinity,pd0SalinitySrc);
                        mmtSalinity=mmt.(activeConfig).Proc_Salinity(n);
                        obj(n).sensors=addSensorData(obj(n).sensors,'salinity_ppt','user',mmtSalinity,'mmt');
                        obj(n).sensors=setSelected(obj(n).sensors,'salinity_ppt','internal');
   
                        % Speed of Sound
                        speedOfSound=pd0.Sensor.sos_mps';
                        speedOfSoundSrc=pd0.Cfg.sosSrc(1,:);
                        obj(n).sensors=addSensorData(obj(n).sensors,'speedOfSound_mps','internal',speedOfSound,speedOfSoundSrc);
                        
                        % The raw data are referenced to the internal SOS
                        obj(n).sensors=setSelected(obj(n).sensors,'speedOfSound_mps','internal');
                        
                        % Ensemble Times
                        % Compute time for each ensemble in seconds
                        ensTimeSec=pd0.Sensor.time(:,1).*3600+pd0.Sensor.time(:,2).*60+pd0.Sensor.time(:,3)+pd0.Sensor.time(:,4)./100;
                        % Compute the duration of each ensemble in seconds,
                        % adjusting for lost data
                        ensDeltaTime=nan(size(ensTimeSec));
                        idxtime=find(~isnan(ensTimeSec));
                        ensDeltaTime(idxtime(2:end))=nandiff(ensTimeSec(idxtime));
                        % Adjust for transects that last past midnight
                        idx24hr=find(ensDeltaTime<0);
                        ensDeltaTime(idx24hr)=24.*3600+ensDeltaTime(idx24hr);
                        ensDeltaTime=ensDeltaTime';

                        % Start date and time
                        idx=find(~isnan(pd0.Sensor.time(:,1)),1,'first');
                        startYear=pd0.Sensor.date(idx,1);
                        % StreamPro doesn't include Y2K dates.
                        if startYear<100
                            startYear=2000+pd0.Sensor.dateNotY2k(idx,1);
                        end
                        startMonth=pd0.Sensor.date(idx,2);
                        startDay=pd0.Sensor.date(idx,3);
                        startHour=pd0.Sensor.time(idx,1);
                        startMin=pd0.Sensor.time(idx,2);
                        startSec=pd0.Sensor.time(idx,3)+pd0.Sensor.time(idx,4)./100;
                        startSerialTime=datenum([startYear,startMonth,startDay,startHour,startMin,startSec]);
                        startDate=datestr(startSerialTime,'mm/dd/yyyy');

                        % End data and time
                        idx=find(~isnan(pd0.Sensor.time(:,1)),1,'last');
                        endYear=pd0.Sensor.date(idx,1);
                        % StreamPro doesn't include Y2K dates.
                        if endYear<100
                            endYear=2000+pd0.Sensor.dateNotY2k(idx,1);
                        end
                        endMonth=pd0.Sensor.date(idx,2);
                        endDay=pd0.Sensor.date(idx,3);
                        endHour=pd0.Sensor.time(idx,1);
                        endMin=pd0.Sensor.time(idx,2);
                        endSec=pd0.Sensor.time(idx,3)+pd0.Sensor.time(idx,4)./100;
                        endSerialTime=datenum([endYear,endMonth,endDay,endHour,endMin,endSec]);

                        % Create date/time object
                        obj(n).dateTime=clsDateTime(startDate,startSerialTime,endSerialTime,ensDeltaTime);

                        % Transect checked for use in discharge computation
                        obj(n).checked=mmt.(transects).Checked(files2Loadidx(k));

                        % Instrument characteristics
                        if isempty(varargin{1})
                            obj(n).adcp=clsInstrumentData('TRDI',mmt,pd0,n);
                        else
                            obj(n).adcp=clsInstrumentData('TRDI',mmt,pd0,n,varargin{1});
                        end
                        
                        obj(n).depths=compositeDepths(obj(n).depths,obj(n));
                        
                    end % if valid pd0
                end % for numFiles

                % Remove empty object
                obj=deleteEmpty(obj);
            end % if numFiles
            
        end % TRDI

        function obj=SonTek(obj,fileNames) 
        % Read and process SonTek data from *.mat files.
        %
        % INPUT:
        %
        % obj: object of clsTransectData
        %
        % fileNames: cell array of file name of *.mat files to be processed
        %
        % OUTPUT:
        %
        % obj: object of clsTransectData
        
            % Determine Number of Files to be Processed.
            if  isa(fileNames,'cell')
                numFiles=size(fileNames,2); 
                fileNames=sort(fileNames);       
            else
                numFiles=1;
                fileNames={fileNames};
            end
            
            % Preallocate objects to improve speed
            obj(numFiles)=clsTransectData();  
            
            % Create a RSData object for each transect
            RSData=clsMatSonTek(fileNames);
            
            % Process each transect
            for n=1:numFiles
                
                % Assign data structure to local variable to allow reuse of
                % previously developed code.
                RS=RSData(n);
                
                % Store file name
                idx=strfind(RS.fileName,'\');
                obj(n).fileName=RS.fileName(idx(end)+1:end);
                               
                % Instrument characteristics
                obj(n).adcp=clsInstrumentData('SonTek',RS);
                
                % BT Depths
                % ---------
                
                % Initialize depth object
                obj(n).depths=clsDepthStructure(); 
                
                % Determine maximum number of cells
                maxCells=size(RS.WaterTrack.Velocity,1); 
                % Determine number of ensembles
                numEns=size(RS.WaterTrack.Velocity,3);
                               
                % Compute cells size in m
                cell_size=RS.System.Cell_Size';
                cellSizeAll=repmat(cell_size,maxCells,1);
                
                % Compute cell depths
                top_of_cells=RS.System.Cell_Start';
                cellDepth=(repmat((1:maxCells)',1,numEns)-0.5).*cellSizeAll+repmat(top_of_cells,maxCells,1);
                
                % Assing draft to local variable in m
                draft=double(RS.Setup.sensorDepth); 
                % Convert depths to m
                depth=RS.BottomTrack.BT_Beam_Depth';
                % Set invalid depths to nan
                depth(depth==0)=nan;
                
                % Create bottom track depth object
                obj(n).depths=addDepthObject(obj(n).depths,depth, 'BT',...
                    RS.BottomTrack.BT_Frequency', draft,...
                    cellDepth, cellSizeAll);
                
                % Vertical beam depths
                % --------------------
                
                % Convert vertical beam depths to m
                depth=RS.BottomTrack.VB_Depth';
                % Set invalid depths to nan
                depth(depth==0)=nan;
                
                % Create vertical beam depth object
                % DSM 1/24/2018 the frequency should be the vertical beam
                % frequency from the transformation matrix information not
                % the botttom track frequency.
                obj(n).depths=addDepthObject(obj(n).depths,depth, 'VB',...
                    RS.BottomTrack.BT_Frequency', draft);
                
                % Set depth reference
                if RS.Setup.depthReference<0.5
                    obj(n).depths=setDepthReference(obj(n).depths,'VB');                    
                else
                    obj(n).depths=setDepthReference(obj(n).depths,'BT');                 
                end
 
                
                % Water velocities
                % ----------------
                % Rearrange 3D matrices for velocity, SNR, and correlation
                vel=permute(RS.WaterTrack.Velocity,[1,3,2]);
                
                % Correct SonTek difference velocity for error in earlier
                % transformation matrices.
                if abs(RS.Transformation_Matrices.Matrix(4,1,1))<0.5
                    vel(:,:,4)=vel(:,:,4).*2;
                end
                
                % Apply TRDI scaling to SonTek difference velocity to
                % convert to a TRDI comparable error velocity.
                vel(:,:,4)=vel(:,:,4)./(sqrt(2).*tand(25));
                
                snr=permute(RS.System.SNR,[1,3,2]);
                corr=permute(RS.WaterTrack.Correlation,[1,3,2]);
                
                % Convert velocity reference to None
                vel(:,:,1)=bsxfun(@plus,vel(:,:,1),RS.Summary.Boat_Vel(:,1)');
                vel(:,:,2)=bsxfun(@plus,vel(:,:,2),RS.Summary.Boat_Vel(:,2)');
                ref='None';
                
                % Convert coordinate system reference
                switch RS.Setup.coordinateSystem
                    case 0
                        coordRef='Beam';
                        warndlg('Beam Coordinates are not supported for all RiverSuveyor firmware releases, use Earth coordinates','!! Warning !!')
                    case 1
                        coordRef='Inst';
                         warndlg('Instrument Coordinates are not supported for all RiverSuveyor firmware releases, use Earth coordinates','!! Warning !!')
                    case 2 
                        coordRef='Earth';
                end % switch

                % Compute number of cells above side lobe cutoff 
                slCutoffPer=double(RS.Setup.extrapolation_dDiscardPercent);
                slCutoffNum=RS.Setup.extrapolation_nDiscardCells;
                if isfield(RS.Summary,'Transmit_Length')
                    slLagEffect_m=(RS.Summary.Transmit_Length'+obj(n).depths.btDepths.depthCellSize_m(1,:))./2;
                else
                    slLagEffect_m=obj(n).depths.btDepths.depthCellSize_m(1,:);
                end
                slCutoffType='Percent';
                cellsAboveSL=clsTransectData.sideLobeCutoff(obj(n).depths.btDepths.depthOrig_m,...
                obj(n).depths.btDepths.draftOrig_m,...
                obj(n).depths.btDepths.depthCellDepth_m,...
                slLagEffect_m,...
                slCutoffType,...
                1-slCutoffPer./100);

                % Correlation and water mode
                idxCorr=find(isnan(corr));
                if isempty(idxCorr)
                    wm='HD';
                elseif length(idxCorr)==length(corr)
                    wm='IC';
                else
                    wm='Variable';
                end
                
                % Excluded distance (SonTek's screening distance)
                excludedDist=double(RS.Setup.screeningDistance-RS.Setup.sensorDepth);
                if excludedDist<0
                    excludedDist=0;
                end
                
                % Create water velocity object 
                obj(n).wVel=clsWaterData(vel,RS.WaterTrack.WT_Frequency',...
                    coordRef,ref,snr,'SNR',excludedDist,...
                    cellsAboveSL, slCutoffPer,slCutoffNum,slCutoffType,slLagEffect_m,wm,excludedDist,corr); 
                
                % Bottom track velocity
                % ---------------------
                
                % Initialize boat velocity object
                obj(n).boatVel=clsBoatStructure();
                % Add bottom track velocity object
                obj(n).boatVel=addBoatObject(obj(n).boatVel,'SonTek',double(RS.BottomTrack.BT_Vel'),...
                    double(RS.BottomTrack.BT_Frequency'),coordRef,'BT');
                
                % GPS Velocities
                % --------------
                
                % Raw data
                rawGGAutc=RS.RawGPSData.GgaUTC;
                rawGGAlat=RS.RawGPSData.GgaLatitude;
                rawGGAlon=RS.RawGPSData.GgaLongitude;
                rawGGAalt=RS.RawGPSData.GgaAltitude;
                rawGGAdiff=RS.RawGPSData.GgaQuality;
                rawGGAhdop=repmat(RS.GPS.HDOP,1,size(RS.RawGPSData.GgaLatitude,2));
                rawGGANumSats=repmat(RS.GPS.Satellites,1,size(RS.RawGPSData.GgaLatitude,2));
                rawVTGcourse=RS.RawGPSData.VtgTmgTrue;
                rawVTGspeed=RS.RawGPSData.VtgSogMPS;
                invalidVTG=RS.RawGPSData.VtgMode==78;
                rawVTGcourse(invalidVTG)=nan;
                rawVTGspeed(invalidVTG)=nan;
                
                % RSL provided ensemble values
                extGGAutc=RS.GPS.Utc;
                extGGAlat=RS.GPS.Latitude;
                extGGAlon=RS.GPS.Longitude;
                extGGAalt=RS.GPS.Altitude;
                extGGAdiff=RS.GPS.GPS_Quality;
                extGGAhdop=RS.GPS.HDOP;
                extGGANumSats=RS.GPS.Satellites;
                extVTGcourse=nan(size(RS.GPS.Longitude));
                extVTGspeed=nan(size(RS.GPS.Longitude));
                
                % QRev methods
                GGApMethod='End';
                GGAvMethod='End';
                VTGMethod='Average';
                if nansum(RS.GPS.GPS_Quality)>0
                    % Create GPS object
                    obj(n).gps=clsGPSData(rawGGAutc,rawGGAlat,rawGGAlon,rawGGAalt,...
                    rawGGAdiff,rawGGAhdop,rawGGANumSats,nan,rawVTGcourse,rawVTGspeed,nan,...
                    nan,extGGAutc,extGGAlat,extGGAlon,extGGAalt,extGGAdiff,...
                    extGGAhdop,extGGANumSats,extVTGcourse,extVTGspeed,...
                    GGApMethod,GGAvMethod,VTGMethod);

                    % DSM 1/31/2018 I don't think these adds are necessary
                    % Add SonTek HDOP information
                    obj(n).gps=setSonTekHDOP(obj(n).gps,RS.GPS.HDOP');
                    % Add SonTek #Sats information
                    obj(n).gps=setSonTekSats(obj(n).gps,RS.GPS.Satellites');

                    % Add GGA velocity object
                    % -----------------------
                    obj(n).boatVel=addBoatObject(obj(n).boatVel,'SonTek',obj(n).gps.ggaVelocityEns_mps,nan,...
                        'Earth','GGA');

                    % Add VTG velocity object
                    % -----------------------
                    obj(n).boatVel=addBoatObject(obj(n).boatVel,'SonTek',obj(n).gps.vtgVelocityEns_mps,nan,...
                        'Earth','VTG',nan,nan);
                end
                % Convert track reference code
                switch RS.Setup.trackReference
                    case 1
                        ref='BT';
                    case 2 
                        ref='GGA';
                    case 3
                        ref='VTG';
                end
                
                % Set navigation reference
                obj(n).boatVel=setNavReference(obj(n).boatVel,ref);
                             
                
                % Edges
                % -----
                
                % Set SonTek methods
                obj(n).edges=clsEdges('Variable','VectorProf');
                
                % Determine left and right edge distances
                distR=double(RS.Setup.Edges_1__DistanceToBank);
                methodR=RS.Setup.Edges_1__Method;
                estQR=double(RS.Setup.Edges_1__EstimatedQ);
                distL=double(RS.Setup.Edges_0__DistanceToBank);
                methodL=RS.Setup.Edges_0__Method;
                estQL=double(RS.Setup.Edges_0__EstimatedQ);
                
                % Determine number of ensembles for each edge
                if RS.Setup.startEdge > 0.1
                    ensR=sum(RS.System.Step==2);
                    ensL=sum(RS.System.Step==4);
                    obj(n).inTransectIdx=(ensR+1:1:numEns-ensL);
                    obj(n).startEdge='Right';
                else 
                    ensR=sum(RS.System.Step==4);
                    ensL=sum(RS.System.Step==2);
                    obj(n).inTransectIdx=(ensL+1:1:numEns-ensR);
                    obj(n).startEdge='Left';
                end % if start edge
                
                % Create left edge object
                switch methodL
                    % User provided edge discharge
                    case 0
                        obj(n).edges=createEdge (obj(n).edges,'left','User Q', distL, estQL, ensL);
                    % Triangular edge
                    case 2
                        obj(n).edges=createEdge (obj(n).edges,'left','Triangular', distL, ensL);
                    % Rectangular edge
                    case 1
                        obj(n).edges=createEdge (obj(n).edges,'left','Rectangular', distL, ensL);                      
                end % switch methodL

                % Create right edge object
                switch methodR
                    % User provided edge discharge
                    case 0
                        obj(n).edges=createEdge (obj(n).edges,'right','User Q', distL, estQR, ensR);
                    % Triangular edge
                    case 2
                        obj(n).edges=createEdge (obj(n).edges,'right','Triangular', distR, ensR);
                    % Rectangular edge
                    case 1
                        obj(n).edges=createEdge (obj(n).edges,'right','Rectangular', distR, ensR);                      
                end % switch methodR
                
                % Extrapolation
                % -------------
                
                % Determine top method
                switch RS.Setup.extrapolation_Top_nFitType
                    case 0
                        top='Constant';
                    case 1
                        top='Power';
                   case 2
                        top='3-Point';
                end % switch

                % Determine bottom method
                switch RS.Setup.extrapolation_Bottom_nFitType
                    case 0
                        bot='Constant';
                    case 1
                        if RS.Setup.extrapolation_Bottom_nEntirePro > 1.1
                            bot='No Slip';
                        else
                            bot='Power';
                        end % if
                end % switch

                % Create extrapolation object
                obj(n).extrap=clsExtrapData(top,bot,RS.Setup.extrapolation_Bottom_dExponent);

                % Sensor Data
                % -----------
                
                % Initialize sensor data
                obj(n).sensors=clsSensors();
                
                % Heading from internal compass
                heading=RS.System.Heading';
                headingSrc='Internal'; 
                magvar=double(RS.Setup.magneticDeclination);
                headingOffset=double(RS.Setup.headingCorrection);
                if ~isempty(RS.Compass)
                    magError=RS.Compass.Magnetic_error(:,1)';
                else
                    magError=[];
                end
                % Create internal heading sensor
                obj(n).sensors=addSensorData(obj(n).sensors,'heading_deg','internal',heading,headingSrc,magvar,headingOffset,magError);
                
                % Heading from GPS compass
                heading=RS.System.GPS_Compass_Heading';
                if nansum(abs(diff(heading)))>0 
                    headingSrc='GPS';                                  
                    magvar=double(RS.Setup.magneticDeclination);
                    headingOffset=double(RS.Setup.hdtHeadingCorrection);
                    % Create external heading sensor
                    obj(n).sensors=addSensorData(obj(n).sensors,'heading_deg','external',heading,headingSrc,magvar,headingOffset,magError);
                end
                % Set selected reference                    
                if RS.Setup.headingSource > 1.1
                    obj(n).sensors=setSelected(obj(n).sensors,'heading_deg','external');
                else
                    obj(n).sensors=setSelected(obj(n).sensors,'heading_deg','internal');
                end
                
                % Pitch
                if isfield(RS.System,'Pitch')
                    pitch=RS.System.Pitch';
                    pitchMin=nan;
                    pitchMax=nan;
                else
                    pitch=RS.Compass.Pitch(:,1)';
                    if abs(RS.Compass.Minimum_Pitch-RS.Compass.Maximum_Pitch)<eps
                        pitchMin=nan;
                        pitchMax=nan;
                    else
                        pitchMin=RS.Compass.Minimum_Pitch;
                        pitchMax=RS.Compass.Maximum_Pitch;
                    end
                    obj(n).sensors.heading_deg.internal=setPRLimit(obj(n).sensors.heading_deg.internal,'pitchLimit',[pitchMax,pitchMin]);
                end
                pitchSrc='Internal';   
                % Create internal pitch sensor
                obj(n).sensors=addSensorData(obj(n).sensors,'pitch_deg','internal',pitch,pitchSrc);
                obj(n).sensors=setSelected(obj(n).sensors,'pitch_deg','internal');

                % Roll
                if isfield(RS.System,'Roll')
                    roll=RS.System.Roll';
                    rollMin=nan;
                    rollMax=nan;
                else
                    roll=RS.Compass.Roll(:,1)';
                    if abs(RS.Compass.Minimum_Roll-RS.Compass.Maximum_Roll)<eps
                        rollMin=nan;
                        rollMax=nan;
                    else
                        rollMin=RS.Compass.Minimum_Roll;
                        rollMax=RS.Compass.Maximum_Roll;
                    end
                    obj(n).sensors.heading_deg.internal=setPRLimit(obj(n).sensors.heading_deg.internal,'rollLimit',[rollMax,rollMin]);

                end
                rollSrc='Internal';   
                % Create internal roll sensor
                obj(n).sensors=addSensorData(obj(n).sensors,'roll_deg','internal',roll,rollSrc);
                obj(n).sensors=setSelected(obj(n).sensors,'roll_deg','internal');

                % Temperature
                if strcmpi(RS.System.Units.Temperature,'degC')
                    temperature=RS.System.Temperature';
                else
                    temperature=(5./9.).*(RS.System.Temperature'-32);
                end
                temperatureSrc='Internal'; 
                % Create internal temperature sensor
                obj(n).sensors=addSensorData(obj(n).sensors,'temperature_degC','internal',temperature,temperatureSrc);
                obj(n).sensors=setSelected(obj(n).sensors,'temperature_degC','internal');
                
                % Salinity
                salinity=RS.Setup.userSalinity;
                salinitySrc='Manual';
                % Create manual salinity sensor
                obj(n).sensors=addSensorData(obj(n).sensors,'salinity_ppt','user',salinity,salinitySrc);
                obj(n).sensors=setSelected(obj(n).sensors,'salinity_ppt','user');
                % Create an internal sensor node for compatibility with
                % future computations.
                obj(n).sensors=addSensorData(obj(n).sensors,'salinity_ppt','internal',salinity,salinitySrc);
                
                % Speed of Sound
                % Not provided in RS Matlab file computed from equation
                % used in TRDI BBSS.
                speedOfSound=clsSensors.speedOfSound(temperature,salinity);
                %speedOfSound=1449.2+4.6.*temperature-0.055.*temperature.^2+0.00029.*temperature.^3+(1.34-0.01.*temperature).*(salinity-35);
                speedOfSoundSrc='QRev';
                % Create speed of sound sensor
                obj(n).sensors=addSensorData(obj(n).sensors,'speedOfSound_mps','internal',speedOfSound,speedOfSoundSrc);
                obj(n).sensors=setSelected(obj(n).sensors,'speedOfSound_mps','internal');

                % Ensemble Times
                ensDeltaTime=diff(RS.System.Time)';
                ensDeltaTime=[0,ensDeltaTime];
                
                idxMissing=find(ensDeltaTime>1.5);
                if ~isempty(idxMissing)
                    numMissing=sum(ensDeltaTime(idxMissing))-length(idxMissing);
                    warndlg([obj(n).fileName,' is missing ',num2str(numMissing),' samples'],'Missing Samples.');
                end
                
                 % Date, start, end, and duration
                dateTemp=datestr(719529+10957+RS.System.Time(1)./(60*60*24),'mm/dd/yyyy');
                startSerialTime=clsDateTime.time2SerialTime(RS.System.Time(1),'SonTek');
                endSerialTime=clsDateTime.time2SerialTime(RS.System.Time(end),'SonTek');

                % Create data/time object
                obj(n).dateTime=clsDateTime(dateTemp,startSerialTime,endSerialTime,ensDeltaTime);
                
                % Transect checked for use in discharge computation
                obj(n).checked=1;
                           
                % Set composite depths on which is only setting in RSL
                obj(n).depths=compositeDepths(obj(n).depths,obj(n),'On'); 
            end % for numFiles
        end %SonTek 
               
        function obj=adjustSideLobe(obj)
        % Coordinates side lobe cutoff calls
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % OUTPUT:
        %
        % obj: object of clsTransectData
        
            % Determine number of transects
            nTransects=length(obj);
            
            % Adjust side lobe cutoff for each transect
            for n=1:nTransects
                obj(n).wVel=adjustSideLobe(obj(n).wVel,obj(n)); 
            end % for n
        end % adjustSideLobe
        
        function obj=changeSelection(obj)
        % Changes whether the transect is include in the final discharge
        % computations
        %
        % INPUT:
        %
        % obj: object of clsTransectData
        %
        % use: logical true or false (1 or 0)
        %
        % OUTPUT:
        %
        % obj: object of clsTransectData
        
            % Set checked property
            obj.checked=~obj.checked;
        end % changeSelection
        
        function obj=deleteEmpty(objIn)
        % Delete empty clsTransectData object
        %
        % INPUT:
        %
        % objIn: objects of clsTransectData
        %
        % OUTPUT:
        %
        % obj: object of clsTransectData
        
            % Process each object
            for n=1:length(objIn)
                % If object does not contain data delete it
                if isobject(objIn(n).wVel)
                    obj(n)=objIn(n);
                end
            end
        end % deleteEmpty
        
        function obj=setExtrapolation(obj,top,bot,exp)
            nTransects=length(obj);
            for n=1:nTransects
                obj(n).extrap=setExtrapData(obj(n).extrap,top,bot,exp);
            end
        end % setExtrapolation
        
        function obj=changeQEnsembles(obj,procMethod)
        % Sets inTransectIdx to all ensembles, except in the case of SonTek
        % data where RSL processing is applied.    
        %
        % INPUT:
        % 
        % obj: object of clsTransectData
        %
        % procMethod: processing method WR2, RSL, QRev
        %
        % OUTPUT:
        %
        % obj: object of clsTransectData
        
            % Number of transects
            nTransects=length(obj);
            
            % Check if RSL method is used
            if strcmpi('RSL',procMethod)
                % Apply RSL method
                for n=1:nTransects
                    numEns=size(obj(n).boatVel.btVel.uProcessed_mps,2);
                    % Determine number of ensembles for each edge
                    if strcmpi(obj(n).startEdge,'Right');
                        obj(n).inTransectIdx=(obj(n).edges.right.numEns2Avg+1:1:numEns-obj(n).edges.left.numEns2Avg);
                    else 
                        obj(n).inTransectIdx=(obj(n).edges.left.numEns2Avg+1:1:numEns-obj(n).edges.right.numEns2Avg);
                    end % if start edge
                end % for n
            else
                % Use all ensembles
                for n=1:nTransects
                  obj(n).inTransectIdx=(1:1:size(obj(n).boatVel.btVel.uProcessed_mps,2));
                end % for n
            end % if RSL
        end % changeQEnsembles
        
        % Edge changes
        
        function obj=changeStartEdge(obj,setting)
        % Set startEdge
        %
        % INPUT:
        %
        % obj: object of clsTransectData
        %
        % setting: start edge (Left or Right)
        %
        % OUTPUT:
        %
        % obj: object of clsTransectData
            obj.startEdge=setting;
        end
        
        function obj=changeEdge(obj,edge,property,setting)
        % Change an edge property (velocity method or coefficient).
        %
        % INPUT:
        %
        % obj: object of clsTransectData
        %
        % edge: edge object to change (left or right)
        %
        % property: property to change (recEdgeMethod or velMethod)
        %
        % setting: new property setting
        %
        % OUTPUT:
        %
        % obj: object of clsTransectData
        
            % Call edges method to change property
            obj.edges=changeProperty(obj.edges,property,setting, edge);
        end
        
        % Coordinate system changes
        
        function obj=changeCoordSys(obj,newCoordSys)
        % Coordinates changing the coordinate system of the water and boat data.
        % Current implementation only allows changes for original to higher
        % order coordinate systems: Beam - Inst - Ship - Earth
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % newCoordSys: name of new coordinate system (Beam, Int, Ship,
        % Earth
        %
        % OUTPUT:
        %
        % obj: object of clsTransectData
        
            % Determine number of transects
            nTransects=length(obj);
            
            % Change coordinate system of each transect in the object
            for n=1:nTransects
                obj(n).wVel=changeCoordSys(obj(n).wVel,newCoordSys,obj(n).sensors,obj(n).adcp);
                obj(n).boatVel=changeCoordSys(obj(n).boatVel,newCoordSys,obj(n).sensors,obj(n).adcp);                      
            end % for nTransects
        end % function changeCoordSys
        
        function obj=changeNavReference(obj,update,newNavRef)
        % Method to set the navigation reference for the water data.
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % newNavRef: new navigation reference (btVel, ggaVel, vtgVel)
        %
        % OUTPUT:
        %
        % obj: object of clsTransectData
        
            % Determine number of transects
            nTransects=length(obj);
            
            % Change coordinate system of each transect in the object
            for n=1:nTransects
                  obj(n).boatVel=changeNavReference(obj(n).boatVel,newNavRef,obj(n));
            end % n
            
            % Update the water data based on the change in navigation
            % reference
            if update
                obj=updateWater(obj);
            end
            
        end % setNavRef
        
        function obj=changeMagVar(obj,magVar)
        % Change the magnetic variation
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % magVar: magnetic variation in degrees
        %
        % OUTPUT:
        %
        % obj: object of clsTransectData
        
            % Determine number of transects
            nTransects=length(obj);
                     
            % Set magnetic variation for each transect
             for n=1:nTransects
                % Update objects
                if ~isempty(obj(n).sensors.heading_deg.external)
                    obj(n).sensors.heading_deg.external=setMagVar(obj(n).sensors.heading_deg.external,magVar,'external');
                end
                                % Apply change if necessary
                if strcmp(obj(n).sensors.heading_deg.selected,'internal')
                    oldMagVar=obj(n).sensors.heading_deg.(obj(n).sensors.heading_deg.selected).magVar_deg;
                    magVarChng=magVar-oldMagVar;
                    obj(n).sensors.heading_deg.internal=setMagVar(obj(n).sensors.heading_deg.internal,magVar,'internal');
                    obj(n).boatVel.btVel=changeMagVar(obj(n).boatVel.btVel,magVarChng);
                    obj(n).wVel=changeMagVar(obj(n).wVel,obj(n).boatVel,magVarChng);
                end
                obj(n).sensors.heading_deg.internal=setMagVar(obj(n).sensors.heading_deg.internal,magVar,'internal');

             end % for n   
             obj=updateWater(obj);
        end % changeMagVar
        
        function obj=changeOffset(obj,HOffset)
        % Change the heading offset (alignment correction)
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % HOffset: heading offset in degrees
        %
        % OUTPUT:
        %
        % obj: object of clsTransectData
        
            % Determine number of transects
            nTransects=length(obj);
                     
            % Set offset for each transect
             for n=1:nTransects
                % Update objects
                obj(n).sensors.heading_deg.internal=setAlignCorrection(obj(n).sensors.heading_deg.internal,HOffset,'internal');
                                % Apply change if necessary
                if strcmp(obj(n).sensors.heading_deg.selected,'external')
                    oldOffset=obj(n).sensors.heading_deg.(obj(n).sensors.heading_deg.selected).alignCorrection_deg;
                    offsetChng=HOffset-oldOffset;
                    %obj(n).sensors.heading_deg.external=setAlignCorrection(obj(n).sensors.heading_deg.external,HOffset,'external');
                    obj(n).boatVel.btVel=changeOffset(obj(n).boatVel.btVel,offsetChng);
                    obj(n).wVel=changeOffset(obj(n).wVel,obj(n).boatVel,offsetChng);
                end
                obj(n).sensors.heading_deg.external=setAlignCorrection(obj(n).sensors.heading_deg.external,HOffset,'external');

             end % for n 
             obj=updateWater(obj);

        end % changeOffset
        
        function obj=changeHeadingSource(obj,HSource)
            % Determine number of transects
            nTransects=length(obj);
            % Set offset for each transect
            for n=1:nTransects 
                if ~isempty(obj(n).sensors.heading_deg.(HSource))
                    oldHeading=obj(n).sensors.heading_deg.(obj(n).sensors.heading_deg.selected).data;
                    newHeading=obj(n).sensors.heading_deg.(HSource).data;
                    headingChange=newHeading-oldHeading;
                    obj(n).sensors.heading_deg=setSelected(obj(n).sensors.heading_deg,HSource);
                    obj(n).boatVel.btVel=changeHeadingSource(obj(n).boatVel.btVel,headingChange);
                    obj(n).wVel=changeHeadingSource(obj(n).wVel,obj(n).boatVel,headingChange);
                end
            end
            obj=updateWater(obj);
        end
        
        function obj=changeSOS(obj,varargin)
        % Coordinates the change in speed of sound based on manually
        % provide information from the user
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % varargin: 
        %   varargin{1}: name of parameter to be changed
        %   varargin{2}: new setting for parameter
        %
        % OUTPUT:
        %
        % obj: object of clsTransectData
        
            % Determine number of transects
            nTransects=length(obj);
                           
                % Select parameter to change
                switch varargin{1}
                    
                    % Temperature Source
                    case 'temperatureSrc' 
                        % If user temperature object is selected and
                        % doesn't exist, create it. The result should be a
                        % user temp and computed SOS.
                        % Process each transect
                        for n=1:nTransects
                            if strcmp(varargin{2},'user') && ~isobject(obj(n).sensors.temperature_degC.user)
                                adcpTemp=obj(n).sensors.temperature_degC.(obj(n).sensors.temperature_degC.selected).data;
                                temperature=repmat(nanmean(adcpTemp),size(adcpTemp));
                                obj(n).sensors=addSensorData(obj(n).sensors,'temperature_degC','user',temperature,'Manual Input');                                
                            end % if
                            % Set the temperature data to the selected source
                            obj(n).sensors=setSelected(obj(n).sensors,'temperature_degC',varargin{2});
                            % Update the speed of sound
                            [obj(n),newsos,oldsos]=clsTransectData.updateSOS(obj(n));
                            obj(n)=applySOSChange(obj(n),newsos,oldsos);
                        end % for nTransects

                    % Manual temperature    
                    case 'temperature' 
                        % Process each transect  
                        for n=1:nTransects
                            % If user object exists change the temperature, if not
                            % create the object with the provide temperature
                            adcpTemp=obj(n).sensors.temperature_degC.internal.data;
                            newUserTemp=repmat(varargin{2},size(adcpTemp));
                            if  isobject(obj(n).sensors.temperature_degC.user)
                                obj(n).sensors.temperature_degC.user=changeData(obj(n).sensors.temperature_degC.user,newUserTemp);
                            else
                                obj(n).sensors=addSensorData(obj(n).sensors,'temperature_degC','user',newUserTemp,'Manual Input');
                            end
                            % Update the speed of sound
                            [obj(n),newsos,oldsos]=clsTransectData.updateSOS(obj(n));
                            obj(n)=applySOSChange(obj(n),newsos,oldsos);
                        end % for nTransects
                        
                    % Salinity    
                    case 'salinity' 
                        for n=1:nTransects
                            if isnumeric(varargin{2})
                                % Set salinity value
                                obj(n).sensors.salinity_ppt.user=changeData(obj(n).sensors.salinity_ppt.user,varargin{2});
                                if obj(n).sensors.salinity_ppt.user.data(1)==obj(n).sensors.salinity_ppt.internal.data(1)
                                    obj(n).sensors=setSelected(obj(n).sensors,'salinity_ppt','internal');
                                else
                                    obj(n).sensors=setSelected(obj(n).sensors,'salinity_ppt','user');
                                end
                            else
                                 obj(n).sensors=setSelected(obj(n).sensors,'salinity_ppt','user');
                            end
                            % Update the speed of sound
                            [obj(n),newsos,oldsos]=clsTransectData.updateSOS(obj(n));
                            obj(n)=applySOSChange(obj(n),newsos,oldsos);
                        end % for nTransects
                    
                    % Speed of sound source    
                    case 'sosSrc' 
                        switch lower(strtrim(varargin{2}))
                            case 'internal'
                                for n=1:nTransects
                                    [obj(n),newsos,oldsos]=clsTransectData.updateSOS(obj(n),'internal','Calculated');
                                    obj(n)=applySOSChange(obj(n),newsos,oldsos);
                                end % for nTransects
                             
                            case 'computed'
                                for n=1:nTransects
                                    [obj(n),newsos,oldsos]=clsTransectData.updateSOS(obj(n),'internal','Computed');
                                    obj(n)=applySOSChange(obj(n),newsos,oldsos);
                                end % for nTransects

                            case 'user'
                                for n=1:nTransects
                                    % If user object exists change the temperature, if not
                                    % create the object with the provide temperature
                                    adcpSOS=obj(n).sensors.speedOfSound_mps.internal.data;
                                    newSOS=repmat(varargin{3},size(adcpSOS));
                                    [obj(n),newsos,oldsos]=clsTransectData.updateSOS(obj(n),'user','Manual Input',newSOS);
                                    obj(n)=applySOSChange(obj(n),newsos,oldsos);
                                end % for nTransects

                        end % switch varargin2
                    
                    % Manual speed of sound
                    case 'sos' 
                        for n=1:nTransects
                            adcpSOS=obj(n).sensors.speedOfSound_mps.internal.data;
                            newSOS=repmat(varargin{3},size(adcpSOS));
                            [obj(n),newsos,oldsos]=clsTransectData.updateSOS(obj(n),'user','Manual Input',newSOS);
                            obj(n)=applySOSChange(obj(n),newsos,oldsos);
                        end % for nTransects

                end % switch
        end % changeSOS
        
        function obj=applySOSChange(obj,newsos,oldsos)
            % Update water and BT velocities and depths for new SOS
            ratio=newsos./oldsos;
            % RiverRay is not affected by change in speed of sound
            if ~strcmp(obj.adcp.model,'RiverRay')
                obj.wVel=sosCorrection(obj.wVel,obj,ratio);
                obj.boatVel.btVel=sosCorrection(obj.boatVel.btVel,obj,ratio);
            end
            obj.depths=sosCorrection(obj.depths,ratio);
        end % applySOSChange

        
        % Water track      
        
        function obj=wtInterpolations(obj,varargin) 
        % Coordinate water velocity interpolation
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % varargin: specifies whether ensembles or cells are to be
        % interpolated and the method used for interpolation. Both settings
        % can be include in same call. 
        %   example:
        %       varargin{1}: 'Ensembles'
        %       varargin{2}: 'Linear'
        %       varargin{3}: 'Cells'
        %       varargin{4}: 'TRDI'
        %
        % OUTPUT:
        %
        % obj: objects of clsTransectData
        
            % Determine number of transects to be processed
            nTransects=length(obj);
            
            % Process each transect
            for n=1:nTransects
                obj(n).wVel=applyInterpolation(obj(n).wVel,obj(n),varargin{:});
            end % for n
            
        end % wtInterpolations
        
        function obj=wtFilters(obj,varargin)
        % Coordinate water velocity filters
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % varargin: specified in pairs or triplets, can be multiple groups
        %       varargin{n}: Filter type (Beam, Difference, Vertical,
        %       Other, Excluded, SNR, wtDepth)
        %       varargin{n+1}: Filter setting (Auto, Manual, Off)
        %       varargin{n+2}: Threshold if Manual
        %
        % OUTPUT:
        %
        % obj: objects of clsTransectData
            % Determine number of transects to process
            nTransects=length(obj);
            
            % Apply filters to each transect
            for n=1:nTransects
                obj(n).wVel=applyFilter(obj(n).wVel,obj(n),varargin{:});
            end % for n
            
        end % wtFilters
        
        function obj=updateWater(obj)
        % Method called from setNavReference, boatInterpolation and
        % boatFilters to ensure that changes in boatVel are reflected
        % in the water data.
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % OUTPUT:
        %
        % obj: objects of clsTransectData
        
            % Determine number transects to be processed
            nTransects=length(obj);
            
            % Set the navigation reference for water velocity in each transect
            for n=1:nTransects
                 obj(n).wVel=setNavReference(obj(n).wVel,obj(n).boatVel);
            end % for n
            
            % Reapply water filters and interpolations
            % Note wtFilters calls applyFilter which automatically calls
            % applyInterpolation so both filters and interpolations are
            % applied with this one call.
            obj=wtFilters(obj);
            
        end % updateWater
        
        % Navigation 
        
        function obj=boatInterpolations(obj,update,target,varargin) 
        % Coordinates boat velocity interpolations
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % target: boat velocity reference (BT or GPS)
        %
        % varargin: type of interpolation
        %
        % OUTPUT:
        %
        % obj: objects of clsTransectData
        
            % Determine number of transects
            nTransects=length(obj);
            
            % Boat velocity reference
            switch target
                
                % Interpolate bottom track boat velocity
                case 'BT'
                    for n=1:nTransects
                        obj(n).boatVel.btVel=applyInterpolation(obj(n).boatVel.btVel,obj(n),varargin{:});
                    end % for n
                    
                % Interpolate GPS based boat velocities
                case 'GPS'
                    for n=1:nTransects
                        if isprop(obj(n).boatVel,'ggaVel')
                            if ~isempty(obj(n).boatVel.ggaVel)
                                obj(n).boatVel.ggaVel=applyInterpolation(obj(n).boatVel.ggaVel,obj(n),varargin{:});
                            end
                        end
                        if isprop(obj(n).boatVel,'vtgVel')
                            if ~isempty(obj(n).boatVel.vtgVel)
                                obj(n).boatVel.vtgVel=applyInterpolation(obj(n).boatVel.vtgVel,obj(n),varargin{:});
                            end
                        end
                    end % for n
            end % switch
            
            % Apply composite tracks setting
            obj=compositeTracks(obj,0);
            
            % Update water data to reflect changes in boatVel
            if update
                obj=updateWater(obj);
            end
            
        end % boatInterpolations
        
        function obj=compositeTracks(obj,update,varargin)
        % Coordinates application of composite tracks.
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % varargin: Optional setting for composite tracks (On or Off). If
        % varargin not specified the setting currently saved in boatVel
        % object will be used.
        %
        % OUTPUT:
        %
        % obj: objects of clsTransectData
        
            % Determine number of transects to process
            nTransects=length(obj);
            
            % Determine if setting is specified
            if isempty (varargin)
                % Process each transect using saved setting
                for n=1:nTransects
                    obj(n).boatVel=compositeTracks(obj(n).boatVel,obj(n));
                end
            else
                % Process each transect using new setting
                for n=1:nTransects
                    obj(n).boatVel=compositeTracks(obj(n).boatVel,obj(n),varargin{:});
                end
            end
            
            % Update water data to reflect changes in boatVel
            if update
                obj=updateWater(obj);
            end
            
        end % compositeTracks
        
        function obj=boatFilters(obj,update,varargin)
        % Coordinates application of boat filters to bottom track data
        %
        % INPUT:
        % 
        % obj: objects of clsTransectData
        %
        % varargin: specified in pairs or triplets, can be multiple groups
        %       varargin{n}: Filter type (Beam, Difference, Vertical,
        %       Other)
        %       varargin{n+1}: Filter setting (Auto, Manual, Off)
        %       varargin{n+2}: Threshold if Manual 
        %
        % OUTPUT:
        % 
        % obj: objects of clsTransectData
        
            % Determine number of transects to be plotted
            nTransects=length(obj);
            
            % Apply filters to each transect
            for n=1:nTransects
                obj(n).boatVel.btVel=applyFilter(obj(n).boatVel.btVel,obj(n),varargin{:});
            end % for n
            
            % Update water data to reflect changes in boatVel
            if strcmp(obj(n).boatVel.selected,'btVel') & update
                obj=updateWater(obj);
            end % if
            
        end % boatFilters
        
        function obj=gpsFilters(obj,update,varargin)
        % Coordinate filters for GPS based boat velocities
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % varargin: specified in pairs or triplets, can be multiple groups
        %       varargin{n}: Filter type (Differential, Altitude, HDOP,
        %       Other)
        %       varargin{n+1}: Filter setting (Auto, Manual, Off)
        %       varargin{n+2}: Threshold if Manual 
        %
        % OUTPUT:
        %
        % obj: objects of clsTransectData
        
            % Determine number of transects to be processed
            nTransects=length(obj);
            
            % Apply filters to each GPS velocity reference in each transect
            for n=1:nTransects
                if ~isempty(obj(n).boatVel.ggaVel)
                    obj(n).boatVel.ggaVel=applyGPSFilter(obj(n).boatVel.ggaVel,obj(n),varargin{:});
                end
                if ~isempty(obj(n).boatVel.vtgVel)
                    obj(n).boatVel.vtgVel=applyGPSFilter(obj(n).boatVel.vtgVel,obj(n),varargin{:});
                end
            end % for n
            
            % Update water data to reflect changes in boatVel
            if (strcmp(obj(n).boatVel.selected,'VTG') || strcmp(obj(n).boatVel.selected,'GGA')) && update
                obj=updateWater(obj);
            end % if
            
        end % gpsFilters
        
        % Depths
               
        function obj=setDepthReference(obj, update, setting)
        % Coordinates setting the depth reference.
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % setting: depth reference (BT, VB, DS)
        %
        % OUTPUT:
        % 
        % obj: objects of clsTransectData
        
            % Determine number of transects to be processed
            nTransects=length(obj);
            
            % Set the depth reference for each transect
            for n=1:nTransects
                obj(n).depths=setDepthReference(obj(n).depths,setting);
            end % for n
            
            % Reprocess depth and side lobe cutoffs for new depth reference
            if update
                obj=processDepths(obj,update);
                obj=adjustSideLobe(obj);
            end
            
        end % setDepthReference
        
        function obj=applyAveragingMethod(obj, setting)
        % Method to apply the selected averaging method to the BT beam
        % depths to achieve a single average depth. It is only applicable
        % to the multiple beams used for BT and not VB or DS.
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % setting: averaging method (IDW, Simple)
        %
        % OUTPUT:
        %
        % obj: objects of clsTransectData
        
            % Determine number of transects to process
            nTransects=length(obj);
            
            % Compute average depth for each transect
            for n=1:nTransects
                obj(n).depths.btDepths=computeAvgBTDepth(obj(n).depths.btDepths,setting);
            end % for n
            
            % Update validData and processed depths based on new BT
            % averages
            obj=processDepths(obj,0);
            
        end % applyDepthAverageMethod            
       
        function obj=processDepths(obj,update,varargin)
        % Method applies filter, composite, and interpolation settings to
        % depth objects so that all are updated using the same filter and
        % interpolation settings.
        %
        % NOTE: the configuration of this method may be inefficient due to
        % repeated reprocessing. Need to evaluate for a more efficient
        % approach.
        %
        % INPUT:
        %
        % obj: objects of clsTransect
        %
        % varargin:
        %   varargin{1}: process to be applied (Filter, Composite,
        %   Interpolate)
        %   varargin{2}: setting for process
        %   
        %
        % OUTPUT:
        %
        % obj: objects of clsTransect
       
            % Determine number of transects to be processed
            nTransects=length(obj);
            
            % Get current settings
            filterSetting=obj(1).depths.(obj(1).depths.selected).filterType;
            interpolateSetting=obj(1).depths.(obj(1).depths.selected).interpType;
            compositeSetting=obj(1).depths.composite;
            btAvgSetting=obj(1).depths.btDepths.avgMethod;
            validMethodSetting=obj(1).depths.btDepths.validDataMethod;
            
            % if the process and setting are provide apply those settings,
            % if not simply reprocess the data using the properties stored
            % in the objects.         
            if ~isempty(varargin)
                % Determine number of variable arguments
                narg=length(varargin);
                for n=1:2:narg
                    
                    % Apply the selected process and setting
                    switch varargin{n}

                        % Apply filter and update data with new setting
                        case 'Filter'
                            filterSetting=varargin{n+1};

                        % Apply composite depths and update data with new setting
                        case 'Composite'
                            compositeSetting=varargin{n+1};

                        % Apply depth interpolation and update data with new setting
                        case 'Interpolate'
                            interpolateSetting=varargin{n+1};
                        
                        case 'AvgMethod'
                            btAvgSetting=varargin{n+1};
                            
                        case 'ValidMethod'
                            validMethodSetting=varargin{n+1};
                    end % switch 
                end % for narg
            end % if 
            for n=1:nTransects
                obj(n).depths=setValidDataMethod(obj(n).depths,validMethodSetting);
                obj(n).depths.btDepths=setAvgMethod(obj(n).depths.btDepths,btAvgSetting);
                obj(n).depths=depthFilter(obj(n).depths,obj(n),filterSetting);
                obj(n).depths=depthInterpolation(obj(n).depths,obj(n),interpolateSetting);
                obj(n).depths=compositeDepths(obj(n).depths,obj(n),compositeSetting); 
                obj(n).wVel=adjustSideLobe(obj(n).wVel,obj(n));
            end % for
            
            % Update water velocities
            if update
                obj=updateWater(obj);
            end
            
        end % depthFilter
        
        function obj=changeDraft(obj,input)
        % Changes the draft for the specified transects and selected depth.
        % 
        % INPUT:
        % 
        % obj: object of clsTransectData
        %
        % OUTPUT:
        %
        % obj: object of clsTransectData

            nTransects=length(obj);
            for n=1:nTransects
                 if ~isempty(obj(n).depths.vbDepths)
                    obj(n).depths.vbDepths=...
                        changeDraft(obj(n).depths.vbDepths,input);
                 end
                 if ~isempty(obj(n).depths.btDepths)
                    obj(n).depths.btDepths=...
                        changeDraft(obj(n).depths.btDepths,input);
                 end
            end % for n
        end % changeDraft
        
        function obj=setProperty(obj,property,setting)
            obj.(property)=setting;
        end
        
        function obj=updateDataStructure(obj)
            nTransects=length(obj);
            for n=1:nTransects
                % Salinity
                pd0Salinity=obj(n).sensors.salinity_ppt.user.dataOrig;
                pd0SalinitySrc=obj(n).sensors.salinity_ppt.user.source;

                % Create salinity sensor
                obj(n).sensors=addSensorData(obj(n).sensors,'salinity_ppt','internal',pd0Salinity,pd0SalinitySrc);
                obj(n).sensors=setSelected(obj(n).sensors,'salinity_ppt','internal');
            end
        end % updateFileStructure
    end % methods
    
    methods (Static)
        function obj=reCreate(obj,structIn)
            % Create transect object(s)
            nMax=length(structIn);
            for n=1:nMax
                % Create object
                transects(n)=clsTransectData();
              
                % Set properties
                transects(n).fileName=structIn(n).fileName;
                transects(n).startEdge=structIn(n).startEdge;
                transects(n).checked=structIn(n).checked;
                transects(n).inTransectIdx=structIn(n).inTransectIdx;
                
                % Create objects
                transects(n)=clsInstrumentData.reCreate(transects(n),structIn(n).adcp);
                transects(n)=clsWaterData.reCreate(transects(n),structIn(n).wVel);
                transects(n)=clsBoatStructure.reCreate(transects(n),structIn(n).boatVel);
                transects(n)=clsGPSData.reCreate(transects(n),structIn(n).gps);
                transects(n)=clsSensors.reCreate(transects(n),structIn(n).sensors);
                transects(n)=clsDepthStructure.reCreate(transects(n),structIn(n).depths);
                transects(n)=clsEdges.reCreate(transects(n),structIn(n).edges);
                transects(n)=clsExtrapData.reCreate(transects(n),structIn(n).extrap);
                transects(n)=clsDateTime.reCreate(transects(n),structIn(n).dateTime);
            end
            obj=setProperty(obj,'transects',transects);
           
        end % reCreate
        
        function [obj, newsos, oldsos]=sosUser(obj,varargin)
        % Compute new speed of sound from temperature and salinity
        %
        % INPUT:
        %
        % obj: object of clsTransectData
        %
        % OUPUT:
        %
        % obj: object of clsTransectData
        %
        % newsos: newly computed speed of sound
        %
        % oldsos: previously used speed of sound
        
            % Assign selected temperature data to local variable
            temperature=obj.sensors.temperature_degC.(obj.sensors.temperature_degC.selected).data;
            % Assign selected salinity to local variable
            salinity=obj.sensors.salinity_ppt.(obj.sensors.salinity_ppt.selected).data;
            oldsos=obj.sensors.speedOfSound_mps.(obj.sensors.speedOfSound_mps.selected).data;
            if strcmpi(obj.sensors.temperature_degC.selected,'internal')
               newsos=obj.sensors.speedOfSound_mps.user.dataOrig;
               obj.sensors.speedOfSound_mps.user=changeData(obj.sensors.speedOfSound_mps.user,newsos);
               obj.sensors.speedOfSound_mps.user=setSource(obj.sensors.speedOfSound_mps.user,'Internal (ADCP)');
               obj.sensors=setSelected(obj.sensors,'speedOfSound_mps','user');
            else
                % Compute new speed of sound
                newsos=clsSensors.speedOfSound(temperature,salinity);
                % Assign previously used speed of sound to local variable
                oldsos=obj.sensors.speedOfSound_mps.(obj.sensors.speedOfSound_mps.selected).data;

                % Save new speed of sound to user sensor object with a source
                % as computed
                if isobject(obj.sensors.speedOfSound_mps.user)
                    obj.sensors=setSelected(obj.sensors,'speedOfSound_mps','user');
                    obj.sensors.speedOfSound_mps.user=changeData(obj.sensors.speedOfSound_mps.user,newsos);
                    obj.sensors.speedOfSound_mps.user=setSource(obj.sensors.speedOfSound_mps.user,'Computed');
                else
                    obj.sensors=addSensorData(obj.sensors,'speedOfSound_mps','user',newsos,'Computed');
                    obj.sensors=setSelected(obj.sensors,'speedOfSound_mps','user');
                end
            end
            
        end % sosUser
       
        function [obj, newsos, oldsos]=updateSOS(obj,varargin)
            oldsos=obj.sensors.speedOfSound_mps.(obj.sensors.speedOfSound_mps.selected).data;
            if nargin==1
                if strcmpi(obj.sensors.temperature_degC.selected,'user') || strcmpi(obj.sensors.salinity_ppt.selected,'user')
                    obj.sensors=setSelected(obj.sensors,'speedOfSound_mps','internal');
                    obj.sensors.speedOfSound_mps.internal=setSource(obj.sensors.speedOfSound_mps.internal,'Computed');
                else
                    obj.sensors=setSelected(obj.sensors,'speedOfSound_mps','internal');
                    obj.sensors.speedOfSound_mps.internal=setSource(obj.sensors.speedOfSound_mps.internal,'Calculated');
                end
            elseif nargin==2
                obj.sensors=setSelected(obj.sensors,'speedOfSound_mps',varargin{1});
            
            elseif nargin==3
                if strcmpi(varargin{1},'internal') && strcmpi(varargin{2},'Computed')
                    if strcmpi(obj.sensors.temperature_degC.selected,'user') || strcmpi(obj.sensors.salinity_ppt.selected,'user')
                        obj.sensors=setSelected(obj.sensors,'speedOfSound_mps','internal');
                        obj.sensors.speedOfSound_mps.internal=setSource(obj.sensors.speedOfSound_mps.internal,'Computed');
                    else
                        obj.sensors=setSelected(obj.sensors,'speedOfSound_mps','internal');
                        obj.sensors.speedOfSound_mps.internal=setSource(obj.sensors.speedOfSound_mps.internal,'Calculated');
                    end  
                else
                    obj.sensors=setSelected(obj.sensors,'speedOfSound_mps',varargin{1});
                    obj.sensors.speedOfSound_mps.internal=setSource(obj.sensors.speedOfSound_mps.internal,varargin{2});
                end
                
            elseif nargin==4
                % If user object does not exist create it. 
                if  ~isobject(obj.sensors.speedOfSound_mps.user)
                    obj.sensors=addSensorData(obj.sensors,'speedOfSound_mps','user',varargin{3},'Manual Input');
                end
                obj.sensors=setSelected(obj.sensors,'speedOfSound_mps',varargin{1});
                obj.sensors.speedOfSound_mps.(varargin{1})=setSource(obj.sensors.speedOfSound_mps.(varargin{1}),varargin{2});
                obj.sensors.speedOfSound_mps.(varargin{1})=changeData(obj.sensors.speedOfSound_mps.(varargin{1}),varargin{3});
            end
            if strcmpi(obj.sensors.speedOfSound_mps.selected,'internal')
                if strcmpi(obj.sensors.speedOfSound_mps.internal.source,'Calculated')
                    newsos=obj.sensors.speedOfSound_mps.internal.dataOrig;
                    obj.sensors.speedOfSound_mps.internal=changeData(obj.sensors.speedOfSound_mps.internal,newsos);
                    obj.sensors=setSelected(obj.sensors,'temperature_degC','internal');
                    obj.sensors=setSelected(obj.sensors,'salinity_ppt','internal');
                else
                    temperature=obj.sensors.temperature_degC.(obj.sensors.temperature_degC.selected).data;
                    salinity=obj.sensors.salinity_ppt.(obj.sensors.salinity_ppt.selected).data;
                    newsos=clsSensors.speedOfSound(temperature,salinity);
                    obj.sensors.speedOfSound_mps.internal=changeData(obj.sensors.speedOfSound_mps.internal,newsos);
                end
            else

                % if source is user, create sensor if necessary or just update the new setting
                if isempty(obj.sensors.speedOfSound_mps.user)
                    obj.sensors=addSensorData(obj.sensors,'speedOfSound_mps','user',varargin{3},'Manual Input');                                
                else
                    obj.sensors.speedOfSound_mps.user=changeData( obj.sensors.speedOfSound_mps.user,varargin{3});
                end % if

                % Assign new speed of sound to local variable
                newsos=obj.sensors.speedOfSound_mps.(obj.sensors.speedOfSound_mps.selected).data;
            end % if internal
        end % updateSOS
        
        function [topChk,botChk,expChk,navRefChk,leftChk,rightChk,draftChk,magvarChk]=checkConsistency(transData)
        % Performs a consistency check on various settings
        %
        % INPUT:
        %
        % transData: object of clsTransectData
        %
        % OUTPUT:
        %
        % topChk: top extrapolation method or "varies" if not consistent
        %
        % botChk: bottom extrapolation method or "varies" if not consistent
        %
        % expChk: exponent or "varies" if not consistent
        %
        % navRefChk: navigation reference or "varies" if not consistent
        %
        % leftChk: left edge type or "varies" if not consistent
        %
        % rightChk: right edge type or "varies" if not consistent
        %
        % draftChk: draft or "varies" if not consistent
        %
        % magvarChk: magnetic variation or "varies" if not consistent
        
            % Determine transects used for discharge computation
            idxUse=[transData.checked];
            % Number of transects
            nTransects=length(transData);
            % Identify first transect used for discharge computation
            idx=find(idxUse==1,1,'first');
            if ~isempty(idx)
                % Assign settings from first transect to local variable
                topFirst=transData(idx).extrap.topMethod;
                botFirst=transData(idx).extrap.botMethod;
                expFirst=transData(idx).extrap.exponent;
                navRefFirst=transData(idx).wVel.navRef;
                leftFirst=transData(idx).edges.left.type;
                rightFirst=transData(idx).edges.right.type;
                draftFirst=transData(idx).depths.(transData(idx).depths.selected).draftUse_m;
                magvarFirst=transData(idx).sensors.heading_deg.(transData(idx).sensors.heading_deg.selected).magVar_deg;

                % Assign settings from first transect to local check variables
                topChk=topFirst;
                botChk=botFirst;
                expChk=expFirst;
                navRefChk=navRefFirst;
                leftChk=leftFirst;
                rightChk=rightFirst;
                draftChk=draftFirst;
                magvarChk=magvarFirst;

                % Process transects to identify changes in settings
                for n=idx+1:nTransects
                    if idxUse(n)

                        % Top extrapolation method
                        if ~strcmp(topFirst,transData(n).extrap.topMethod)
                            topChk='Varies';
                        end

                        % Bottom extrapolation method
                        if ~strcmp(botFirst,transData(n).extrap.botMethod)
                            botChk='Varies';
                        end

                        % Extrapolation exponent
                        if expFirst~=transData(n).extrap.exponent
                            expChk='Varies';
                        end

                        % Navigation reference
                        if ~strcmp(navRefFirst,transData(n).wVel.navRef)
                            navRefChk='Varies';
                        end

                        % Left edge type
                        if ~strcmp(leftFirst,transData(n).edges.left.type)
                            leftChk='Varies';
                        end

                        % Right edge type
                        if ~strcmp(rightFirst,transData(n).edges.right.type)
                            rightChk='Varies';
                        end

                        % Draft
                        if abs(draftFirst-transData(n).depths.(transData(n).depths.selected).draftUse_m)>0.001
                            draftChk='Varies';
                        end

                        % Magnetic variation
                        if abs(magvarFirst-transData(n).sensors.heading_deg.(transData(n).sensors.heading_deg.selected).magVar_deg)>0.01
                            magvarChk='Varies';
                        end
                    end % if idxUse
                end % for n
            else
                topChk='N/A';
                botChk='N/A';
                expChk='N/A';
                navRefChk='N/A';
                leftChk='N/A';
                rightChk='N/A';
                draftChk='N/A';
                magvarChk='N/A';
            end
        end % checkExtrapConsistency;
        
        function [width,widthCOV,area,areaCOV,avgBoatSpeed,avgBoatCourse,avgWaterSpeed,avgWaterDir,meanDepth,maxDepth,maxWaterSpeed]=computeCharacteristics(obj,discharge)
        % Computes characteristics of the measurement and cross section
        %
        % INPUT:
        %
        % obj: objects of clsTransectData
        %
        % discharge: objects of clsQComp
        %
        % OUTPUT:
        %
        % width: width of cross section parallel to course made good
        %
        % area: area of cross section parallel to course made good
        %
        % avgBoatSpeed: average boat speed
        %
        % avgBoatCourse: course made good by ship track
        %
        % avgWaterSpeed: Q/A
        %
        % avgWaterDir: discharge weighted water direction of measured
        % cells, does not account for unmeasured areas
        
            % Number of transects
            nTransects=length(obj);
            
            % Process each transect
            for n=1:nTransects
                % Assign properties to local variables
                inTransectIdx=obj(n).inTransectIdx;
                if ~isempty(obj(n).boatVel.(obj(n).boatVel.selected))
                    uBoat=obj(n).boatVel.(obj(n).boatVel.selected).uProcessed_mps(inTransectIdx);
                    vBoat=obj(n).boatVel.(obj(n).boatVel.selected).vProcessed_mps(inTransectIdx);
                else
                    uBoat=nan(size(obj(n).boatVel.btVel.uProcessed_mps(inTransectIdx)));
                    vBoat=nan(size(obj(n).boatVel.btVel.vProcessed_mps(inTransectIdx)));
                end
                depth=obj(n).depths.(obj(n).depths.selected).depthProcessed_m(inTransectIdx);
                validBoat=~isnan(uBoat);
                validDepth=~isnan(depth);
                validEns=all([validBoat; validDepth]);
                nEns=length(validEns);
                ensDur=obj(n).dateTime.ensDuration_sec(inTransectIdx);
                
                % Develop cumulative duration time series for valid
                % ensembles
                t=nan(1,nEns);
                cumDur=0;
                for j=1:nEns
                    cumDur=nansum([cumDur,ensDur(j)]);
                    if validEns(j)
                        t(j)=cumDur;
                        cumDur=0;
                    end
                end % for
                            
                % Compute x,y coordinates of ship track
                x=nancumsum(uBoat.*t);
                y=nancumsum(vBoat.*t);
                
                % Compute boat course and mean speed
                [courser,dmg]=cart2pol(x(end),y(end));
                xEnd(n)=x(end);
                yEnd(n)=y(end);
                % Compute width
                width(n)=dmg+obj(n).edges.left.dist_m+obj(n).edges.right.dist_m;
                
                % Compute area 
                
                avgBoatCourse(n)=rad2azdeg(courser);
                avgBoatSpeed(n)=nanmean(sqrt(uBoat.^2+vBoat.^2));
                
%                 % Unit vector for shiptrack
%                 [unitTrackx,unitTracky]=pol2cart(courser,1);
%                 
%                 % Compute area of moving-boat portion of transect
%                 areaMain=nansum((bsxfun(@times,(uBoat.*t),unitTrackx)+bsxfun(@times,(vBoat.*t),unitTracky)).*depth);
                % Compute area of moving-boat portion consistent with
                % AreaComp2
                % Unit Vector
                btTrack = [x; y]'
                unitVec = btTrack(end,:)./norm(btTrack(end,:));

                % Dot Product
                dotprod = dot(btTrack, repmat(unitVec, size(btTrack,1),1),2);

                % Projection = Dot Product * Unit Vector
                projxy = repmat(dotprod,1,size(unitVec,2)).*repmat(unitVec,size(dotprod,1),1);

                % Compute distance from origin to each projected x,y point.
                station = sqrt(projxy(:,1).^2 + projxy(:,2).^2);

                areaMain = trapz(station, depth);
                
                % Compute area of left edge
                edgeIdx=clsQComp.edgeEnsembles('left',obj(n));
                switch obj(n).edges.left.type
                    case 'Triangular'
                        coef=0.5;
                    case 'Rectangular'
                        coef=1.0;
                    case 'Custom'
                        coef=0.5+(obj(n).edges.left.custCoef-0.3535);
                    case 'User Q'
                        coef=0.5;
                end % switch
                edgeDepth=nanmean(obj(n).depths.(obj(n).depths.selected).depthProcessed_m(edgeIdx));
                areaLeft=edgeDepth.*obj(n).edges.left.dist_m.*coef;
                if isnan(areaLeft)
                    areaLeft=0;
                end
                
                % Compute area of right edge
                edgeIdx=clsQComp.edgeEnsembles('right',obj(n));
                switch obj(n).edges.right.type
                    case 'Triangular'
                        coef=0.5;
                    case 'Rectangular'
                        coef=1.0;
                    case 'Custom'
                        coef=0.5+(obj(n).edges.right.custCoef-0.3535);
                    case 'User'
                        coef=0.5;
                end % switch
                edgeDepth=nanmean(obj(n).depths.(obj(n).depths.selected).depthProcessed_m(edgeIdx));
                areaRight=edgeDepth.*obj(n).edges.right.dist_m.*coef;
                if isnan(areaRight)
                    areaRight=0;
                end
                
                % Compute total area
                area(n)=areaMain+areaLeft+areaRight;
                
                
                % Compute average water speed
                avgWaterSpeed(n)=discharge(n).total./area(n);
                
                % Compute flow direction using discharge weighting
                uWater=obj(n).wVel.uProcessed_mps(:,inTransectIdx);
                vWater=obj(n).wVel.vProcessed_mps(:,inTransectIdx);
                wght=abs(discharge(n).middleCells);
                se=nansum(nansum(uWater.*wght))./nansum(nansum((wght)));
                sn=nansum(nansum(vWater.*wght))./nansum(nansum((wght)));
                avgWaterDir(n)=atan2(se,sn)*180/pi;
                if avgWaterDir(n)<0 
                    avgWaterDir(n)=avgWaterDir(n)+360;
                end % if
                
                % Compute Mean Depth
                % Changed to area/width 9/19/2019 dsm
                meanDepth(n)=area(n) / width(n)
                %meanDepth(n)=nanmean(obj(n).depths.(obj(n).depths.selected).depthProcessed_m);
                maxDepth(n)=nanmax(obj(n).depths.(obj(n).depths.selected).depthProcessed_m);
                magWater=sqrt(uWater.^2+vWater.^2);
                maxWaterSpeed(n)=prctile(magWater(:),99);
                checked(n)=obj(n).checked;
            end % for n
            
            % Only transects used for discharge are included in averages
            checked=logical(checked);
            if sum(checked)>0
                width(nTransects+1)=nanmean(width(checked));
                widthCOV=(nanstd(width(checked))./width(nTransects+1)).*100;
                area(nTransects+1)=nanmean(area(checked));
                areaCOV=(nanstd(area(checked))./area(nTransects+1)).*100;
                avgBoatSpeed(nTransects+1)=nanmean(avgBoatSpeed(checked));
                avgWaterSpeed(nTransects+1)=clsQAData.meanQ(discharge(checked),'total')./area(nTransects+1);
                meanDepth(nTransects+1)=nanmean(meanDepth(checked));
                maxDepth(nTransects+1)=nanmax(maxDepth(checked));
                maxWaterSpeed(nTransects+1)=nanmax(maxWaterSpeed);

                % Compute average water directions using vector coordinates to
                % avoid problem averaging fluctuations across zero degrees
                waterDir=azdeg2rad(avgWaterDir);
                [x,y]=pol2cart(waterDir(checked),1);
                [theta,~]=cart2pol(mean(x),mean(y));
                avgWaterDir(nTransects+1)=rad2azdeg(theta);
                
            else
                width=nan(nTransects+1,1);
                area=nan(nTransects+1,1);
                avgBoatSpeed=nan(nTransects+1,1);
                avgWaterSpeed=nan(nTransects+1,1);
                meanDepth=nan(nTransects+1,1);
                maxDepth=nan(nTransects+1,1);
                maxWaterSpeed=nan(nTransects+1,1);
                avgWaterDir=nan(nTransects+1,1);
                widthCOV=nan(nTransects+1,1);
                areaCOV=nan(nTransects+1,1);
            end
            
        end % computeCharacteristics
               
        function [validEns,validWTCells]=validCellsEnsembles(transect)
            % Assign index of moving-boat portion of transect to local
            % variable
            inTransectIdx=transect.inTransectIdx;     
            
            % Determine valid WT ensembles based on WT and navigation data
            % For WT to be valid both WT and navigation data must be
            % present.
            if ~isempty(transect.boatVel.(transect.boatVel.selected))
                validNav=transect.boatVel.(transect.boatVel.selected).validData(1,inTransectIdx);
            else
                validNav=zeros(size(transect.boatVel.btVel.validData(1,inTransectIdx)));
            end
            validWT=transect.wVel.validData(:,inTransectIdx,1);
            validWTEns=any(validWT);
            
            % Determine valid depths
            if strcmp(transect.depths.composite,'On')
                idxNA=strcmp(transect.depths.(transect.depths.selected).depthSourceEns(transect.inTransectIdx),'NA');
                validDepth=~strcmp(transect.depths.(transect.depths.selected).depthSourceEns(transect.inTransectIdx),'IN');
                validDepth(idxNA)=0;
            else
                % Prep depth data
                validDepth=transect.depths.(transect.depths.selected).validData(transect.inTransectIdx) ;
                idx=find(isnan(transect.depths.(transect.depths.selected).depthProcessed_m(transect.inTransectIdx)));
                validDepth(idx)=0;
            end

            % Determine valid ensembles based on all data
            validEns=all([validNav;validWTEns;validDepth]);
            
            % Determine valid depth cells
            validWTCells=validWT;
        end % validCellsEnsembles
        
        function [deltaT]=adjustedEnsembleDuration(transect,varargin)
            if strcmp(transect.adcp.manufacturer,'TRDI')
                if isempty(varargin)
                    valid=~isnan(transect.wVel.uProcessed_mps);
                    validSum=sum(valid);
                else
                    validSum=~isnan(transect.boatVel.btVel.uProcessed_mps);
                end
                
                validEns=validSum>0;
                nEns=length(validEns);
                ensDur=transect.dateTime.ensDuration_sec;
                deltaT=nan(1,nEns);
                cumDur=0;
                for j=1:nEns
                    cumDur=nansum([cumDur,ensDur(j)]);
                    if validEns(j)
                        deltaT(j)=cumDur;
                        cumDur=0;
                    end
                end % for
            else
                deltaT=transect.dateTime.ensDuration_sec;
            end
        end
        
        function [cellsAboveSL]=sideLobeCutoff(depths,draft,cellDepth,slLagEffect,varargin)
        % Computes side lobe cutoff based on beam angle with no allowance
        % for lag.
        % 
        % INPUT:
        %
        % depths: all depths used to determine sidelobe cutoff
        %
        % draft: draft included in depths, assumed to be the same
        %
        % cellDepth: depth of each cell from the water surface
        %
        % slLagEffect: range usually computed as (lag+transmit+cellSize)/2
        %
        % varargin: allows specifying the percent cutoff rather than using
        % the cos of the angle.
        %
        % OUTPUT:
        %
        % cellsAboveSL: logical array
                
                % Compute minimum depths for each ensemble
                minDepths=nanmin(depths);
                       
                % Compute range from transducer
                rangeFromXducer=(minDepths-draft);
                
                % Adjust for transducer angle
                switch varargin{1}
                    case 'Percent'
                        coeff=varargin{2};
                    case 'Angle'
                        coeff=cosd(varargin{2});                   
                end
                
                % Compute sidelobe cutoff to centerline of depth cells
                cutoff=rangeFromXducer.*coeff-slLagEffect+draft;
                
                % Compute logical side lobe cutoff matrix
                cellsAboveSL=bsxfun(@minus,cellDepth,cutoff)<0;
                
        end % sideLobeCutoff
        
         function [cellSizeAll, cellDepth, slCutoffPer, slLagEffect_m]=computeCells(pd0)
        % function to compute matrices of cell size,range of cell
        % center line from transducers, and cells above the sidlobe
        % including surface cells for RR
            
            % Number of ensembles
            numEns=size(pd0.Wt.vel_mps(:,:,1),2);
            % Retreive and compute regular cell information
            regCellSize=pd0.Cfg.ws_cm'./100;     
            regCellSize(regCellSize==0)=nan;
            dist_cell1_m=pd0.Cfg.distBin1_cm'./100; 
            numRegCells=size(pd0.Wt.vel_mps,1);

            % surf* data are to accomodate RiverRay and RiverPro. pd0Read sets these
            % values to nan when reading Rio Grande or StreamPro data
            
            % Determine number and size of surface cells
            noSurfCells=pd0.Surface.no_cells';
            noSurfCells(isnan(pd0.Surface.no_cells))=0;
            maxSurfCells=nanmax(noSurfCells);
            surfCellSize=pd0.Surface.cell_size_cm'./100;
            surfCell1Dist=pd0.Surface.dist_bin1_cm'./100; 
            
            % Compute maximum number of cells
            maxCells=maxSurfCells+numRegCells;

            % Combine cell size and cell range from transducer for both
            % surface and regular cells 
            % ---------------------------------------------------
            cellDepth=nan(maxCells,numEns);
            cellSizeAll=nan(maxCells,numEns);
            for ii=1:numEns
                
                % Determine number of cells to be treated as regular cells
                if nanmax(noSurfCells)>0
                    numRegCells=maxCells-noSurfCells(ii);
                else
                    numRegCells=maxCells;
                end % if nanmax
                
                % Surface cell are present
                if noSurfCells(ii)>0
                    cellDepth(1:noSurfCells(ii),ii)=surfCell1Dist(ii)+...
                        (0:surfCellSize(ii):(noSurfCells(ii)-1)*surfCellSize(ii))';
                    cellDepth(noSurfCells(ii)+1:end,ii)=cellDepth(noSurfCells(ii),ii)+...
                        0.5*surfCellSize(ii)+0.5.*regCellSize(ii)+...
                        (0:regCellSize(ii):(numRegCells-1)*regCellSize(ii));
                    cellSizeAll(1:noSurfCells(ii),ii)=...
                        repmat(surfCellSize(ii),noSurfCells(ii),1);
                    cellSizeAll(noSurfCells(ii)+1:end,ii)=...
                        repmat(regCellSize(ii),numRegCells,1);
  
                % No surface cells
                else
                    cellDepth(1:numRegCells,ii)=dist_cell1_m(ii)+...
                        [0:regCellSize(ii):(numRegCells-1)*regCellSize(ii)];
                    cellSizeAll(1:end,ii)=repmat(regCellSize(ii),numRegCells,1);
                end % if surface cells
            end % ii
            
            
            %       
            % Firmware is used to ID RiverRay data with variable modes and
            % lags
            % --------------------------------------------------------------
            firmware=num2str(pd0.Inst.firmVer(1));
            
            % Compute slLagEffect
            lag=pd0.Cfg.lag_cm'./100;
            if strcmp(firmware(1:2),'44') || strcmp(firmware(1:2),'56')
                lagNearBottom=pd0.Cfg.lagNearBottom';
                lagNearBottom(isnan(lagNearBottom))=0;
                lag(logical(lagNearBottom))=0;
            end
            pulseLen=pd0.Cfg.xmitPulse_cm'./100;
            slLagEffect_m=(lag+pulseLen+regCellSize)./2;
            slCutoffPer=(1-cosd(pd0.Inst.beamAng(1))).*100;
        end % computeCells
    end % static methods
            
end % class

