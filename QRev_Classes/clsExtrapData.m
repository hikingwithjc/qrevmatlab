classdef clsExtrapData
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess = private)
        
        topMethodOrig % Extrapolation method for top of profile: Power, Constant, 3-Point
        botMethodOrig % Extrapolation method for bottom of profile: Power, No Slip 
        exponentOrig % Exponent for power of no slip methods
        topMethod % Extrapolation method for top of profile: Power, Constant, 3-Point
        botMethod % Extrapolation method for bottom of profile: Power, No Slip 
        exponent % Exponent for power of no slip methods      

    end
    
    methods
        function obj=clsExtrapData (top,bot,exp)
            if nargin > 0
                    obj.topMethodOrig=top;
                    obj.botMethodOrig=bot;
                    obj.topMethod=top;
                    obj.botMethod=bot;
                    obj.exponentOrig=double(exp);
                    obj.exponent=double(exp);
            end % nargin
        end % constructor
        
        function obj=setExtrapData(obj,top,bot,exp)
            if nargin > 0
                obj.topMethod=top;
                obj.botMethod=bot;
                obj.exponent=double(exp);
            end % nargin
        end % setExtrapData
            
        function obj=setProperty(obj,property,setting)
            obj.(property)=setting;
        end
            
    end % methods
    
   methods (Static)
       
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
            % Create object
            obj.extrap=clsExtrapData();
            % Get variable names from structure
            names=fieldnames(structIn);
            % Set properties to structure values
            nMax=length(names);
            for n=1:nMax
                obj.extrap=setProperty(obj.extrap,names{n},structIn.(names{n}));
            end % for n
        end % reCreate
    end % static methods    
    
end % class

