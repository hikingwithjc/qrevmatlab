classdef clsSelectFit < clsFitData
    % Class definition for data class that contains all of the automated
    % extrapolation method selection information. This inherits all the
    % properties from FitData and addes the automated selection data to it.
    % David S. Mueller 6/17/2011
    %
    %
    % Last modificaitons / validation 5/15/2012
    %7/24/2013
    % DSM modified to catch C shaped profile lines 63-66, 123
    % DSM modified to fix manual fits and how it displays line 172
    % 7/25/2013
    % DSM fixed bug in line 172, changed 0.1667 to obj.exponent
    %
    % 4/4/2014
    % DSM modified for use in QRev
    
    properties (SetAccess=private)
        fitMethod    % User selected method Automatic or Manual
        botMethodAuto % Selected extrapolation for top
        topMethodAuto % Selected extrapolation for bottom
        exponentAuto  % Selected exponent
        topfitr2     % Top fit custom r^2
        topmaxdiff   % Maximum difference between power and 3-pt at top
        botdiff      % Difference between power and no slip at z=0.1
        botrsqr      % Bottom fit r^2
        fitrsqr      % Selected fit of selected power/no slip fit
        nsexponent   % No slip optimized exponent
        ppexponent   % Power power optimized exponent
        topr2
    end % properties
    
    methods
        %==================================================================        
        function obj=clsSelectFit(normalized,fitMethod,varargin)
        % 
        % Constructor method
        % Requires a single normalized data set from NormData.
        % Accesses FitData with top Power, bottom Power, and Optimized.
        % Includes logic to make automatic selection of best extrapolation
        % method.
        %==================================================================
            obj=obj@clsFitData();
            if nargin>0
                nTransects=length(normalized);
                for n=1:nTransects
                    % Retrieve variables
                    % ------------------
                    validData=normalized(n).validData;
                    
                    % Store data in properties to object
                    obj(n).fitMethod=fitMethod;

                    % Compute power fit with optimized exponent as reference to
                    % determine if constant no slip will be more appropriate
                    ppobj=clsFitData(normalized(n),'Power','Power','optimize');

                    % Store results in object
                    obj(n).ppexponent=ppobj.exponent;
                    obj(n).residuals=ppobj.residuals;
                    obj(n).rsqr=ppobj.rsqr;
                    obj(n).exponent95confint=ppobj.exponent95confint;

                    % Begin automatic fit
                   

                        % More the 6 cells are required to compute an optimized fit. For
                        % fewer than 7 cells the default power/power fit is selected due to
                        % lack of sufficient data for a good analysis.
                        % -----------------------------------------------------------------
                        if length(obj(n).residuals)> 6
                            % Compute the difference between the top two cells of
                            % data and the optimized power fit
                            top2=sum(normalized(n).unitNormalizedMed(validData(end-1:end))...
                                -ppobj.coef.*normalized(n).unitNormalizedz(validData(end-1:end)).^(ppobj.exponent));
                            % Compute the difference between the bottom two cells of
                            % data and the optimized power fit
                            bot2=sum(normalized(n).unitNormalizedMed(validData(1:2))...
                                -ppobj.coef.*normalized(n).unitNormalizedz(validData(1:2)).^(ppobj.exponent));
                            % Compute the difference between the middle two cells of
                            % data and the optimized power fit
                            mid1=floor(length(~isnan(validData))./2);
                            mid2=sum(normalized(n).unitNormalizedMed(validData(mid1:mid1+1))...
                                -ppobj.coef.*normalized(n).unitNormalizedz(validData(mid1:mid1+1)).^(ppobj.exponent));

                            % Default fit set to power/power
                            obj(n).topMethodAuto='Power';
                            obj(n).botMethodAuto='Power';

                            % Evaluate difference in data and power fit at water surface
                            % using a linear fit through the top 4 median cells and
                            % save results.
                            linfit=regstats(normalized(n).unitNormalizedMed(validData(1:4)),normalized(n).unitNormalizedz(validData(1:4)),'linear',{'beta','r','rsquare'});
                            dsmfitr2=1-(sum(linfit.r.^2)./mean(abs(linfit.r)));
                            obj(n).topfitr2=dsmfitr2;
                            obj(n).topr2=linfit.rsquare;

                            %% Evaluate overall fit
                            % If the optimized power fit does not have an r^2
                            % better than 0.8 or if the optimized exponent if
                            % 0.1667 falls within the 95% confidence interval of
                            % the optimized fit, there is insufficient justification to
                            % change the exponent from 0.1667.
                            if ppobj.rsqr<0.8 || (0.1667>obj(n).exponent95confint(1) && 0.1667<obj(n).exponent95confint(2))
                                % If the an optimized exponent cannot be justified,
                                % the linear fit is used to determine if a constant
                                % fit at the top is a better alternative than a
                                % power fit. If the power fit is the better
                                % alternative the exponent is set to the default
                                % 0.1667 and the data is refit.
                                if abs(obj(n).topfitr2)<0.8 || obj(n).topr2<0.9 
                                    ppobj=clsFitData(normalized(n),'Power','Power','Manual',0.1667);
                                end % if top*
                            end % if 0.1667

                            %% Evaluate fit of top and bottom portions of the profile
                            % Set save selected exponent and associated fit
                            % statistics
                            obj(n).exponentAuto=ppobj.exponent;
                            obj(n).fitrsqr=ppobj.rsqr;    

                            % Compute the difference at the water surface between a
                            % linear fit of the top 4 measured cells and the best
                            % selected power fit of the whole profile.
                            obj(n).topmaxdiff=ppobj.u(end)-(sum(linfit.beta));

                            % Evaluate the difference at the bottom between power using
                            % the whole profile and power using only the bottom third.
                            nsobj=clsFitData(normalized(n),'Constant','No Slip','optimize');
                            obj(n).nsexponent=nsobj.exponent;
                            obj(n).botrsqr=nsobj.rsqr;
                            obj(n).botdiff=ppobj.u(round(ppobj.z,2)==0.1)-nsobj.u(round(nsobj.z,2)==0.1);

                            % Begin automatic selection logic
                            % ===============================

                            % A constant no slip fit condition is selected if:
                            % 
                            % 1)The top of the power fit doesn't fit the data well.
                            % This is determined to be the situation when
                            % (a) the difference at the water surface between the
                            % linear fit and the power fit is greater than 10% and
                            % (b) the difference is either positive or the difference
                            % of the top measured cell differs from the best
                            % selected power fit by more than 5%.
                            % OR
                            % 2) The bottom of the power fit doesn't fit the data 
                            % well. This is determined to be the situation when (a)  
                            % the difference between and optimized no slip fit
                            % and the selected best power fit of the whole profile
                            % is greater than 10% and (b) the optimized on slip fit has
                            % and r^2 greater than 0.6.
                            % OR
                            % 3) Flow is bidirectional. The sign of the top of the
                            % profile is different from the sign of the bottom of
                            % the profile.
                            % OR
                            % 4) The profile is C-shaped. This is determined by
                            % (a) the sign of the top and bottom difference from
                            % the best selected power fit being different than the
                            % sign of the middle difference from the best selected
                            % power fit and (b) the combined difference of the top
                            % and bottom difference from the best selected power
                            % fit being greater than 10%.
                           if (abs(obj(n).topmaxdiff)>0.1) ...                        
                                 && (obj(n).topmaxdiff>0 || abs(normalized(n).unitNormalizedMed(validData(1))-ppobj.u(end))>0.05)...
                                 || ((abs(obj(n).botdiff)>0.1)&& obj(n).botrsqr>0.6)...
                                 || (sign(normalized(n).unitNormalizedMed(validData(1)))~=sign(normalized(n).unitNormalizedMed(validData(end)))...
                              || sign(bot2).*sign(top2)==sign(mid2) && abs(bot2+top2)>0.1)

                                % Set the bototm to no slip
                                obj(n).botMethodAuto='No Slip';
                                % if the no slip fit with an optimized exponent
                                % does not have an r^2 better than 0.8 use the
                                % default 0.1667 for the no slip exponent
                                if nsobj.rsqr>0.8
                                    obj(n).exponentAuto=nsobj.exponent;
                                    obj(n).fitrsqr=nsobj.rsqr;
                                else
                                    obj(n).exponentAuto=0.1667;
                                    obj(n).fitrsqr=nan;
                                end % if 

                                % Use the no slip 95% confidence intervals if they
                                % are available.
                                if ~isempty(nsobj.exponent95confint) & ~isnan(nsobj.exponent95confint)
                                    obj(n).exponent95confint(1)=nsobj.exponent95confint(1);
                                    obj(n).exponent95confint(2)=nsobj.exponent95confint(2);
                                else
                                    obj(n).exponent95confint(1)=nan;
                                    obj(n).exponent95confint(2)=nan;
                                end % if

                                % Set the top method to constant
                                obj(n).topMethodAuto='Constant';
                            else

                                % Leave the fit set to power / power and set the
                                % best selected optimized exponent as the automatic
                                % fit exponent.
                                obj(n).exponentAuto=ppobj.exponent;
                           end % if constant / no slip
                        else

                            % If the data are insufficient for a valid analysis use
                            % the power / power fit with the default 0.1667
                            % exponent.
                           obj(n).topMethodAuto='Power';
                           obj(n).botMethodAuto='Power';
                           obj(n).exponentAuto=0.1667;
                           obj(n).nsexponent=0.1667;
                        end % if data are sufficient

                        % Update the fit using the automatically selected methods
                        % and exponent.
                        update=clsFitData(normalized(n),obj(n).topMethodAuto,obj(n).botMethodAuto,'Manual',obj(n).exponentAuto);            
                        updateAuto=update;
                     if strcmpi(fitMethod,'Manual')

                          if length(varargin)==1
                              transData=varargin{1};
                              m=find([transData.checked]==1,1,'last');
                              update=clsFitData(normalized(n),transData(m).extrap.topMethod,transData(m).extrap.botMethod,'Manual',transData(m).extrap.exponent);
                          else
                                update=clsFitData(normalized(n),varargin{2},varargin{3},'Manual',varargin{4});  
                          end                                                   

                    end % if type of fit

                    % Store fit data in object
                    obj(n).topMethod=update.topMethod;
                    obj(n).botMethod=update.botMethod;
                    obj(n).exponent=update.exponent;
                    obj(n).coef=update.coef;
                    obj(n).u=update.u;
                    obj(n).uAuto=updateAuto.u;
                    obj(n).zAuto=updateAuto.z;
                    obj(n).z=update.z;
                    obj(n).expMethod=update.expMethod;
                    obj(n).residuals=update.residuals;
                end % if nargin
            end % for n
        end % constructor

        function obj=resetProperty(obj,property,setting)
            obj.(property)=setting;
        end
        
    end % methods
    
    methods (Static)
        
        function obj=reCreate(structIn)
        % Creates object from structured data of class properties
            numTrans=length(structIn);
            for num=1:numTrans   
                % Create object
                obj(num)=clsSelectFit();
                % Get variable names from structure
                names=fieldnames(structIn(num));
                % Set properties to structure values
                nMax=length(names);
                for n=1:nMax
                    % Assign property
                    obj(num)=resetProperty(obj(num), names{n},structIn(num).(names{n}));
                end % for n
            end % for num
        end % reCreate        
    end % static methods        
end % class

