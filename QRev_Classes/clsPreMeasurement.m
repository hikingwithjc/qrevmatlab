classdef clsPreMeasurement
% clsPreMeasurement  Class for storing QA data such as compass and system tests. Also serves 
% as the container for all methods of evaluating the quality of the measurement.     
    properties (SetAccess = private)
        timeStamp % Time stamp of test
        data % Data from test
        result % Result from the test
    end % properties

    methods
        function obj=clsPreMeasurement(timeStamp, dataIn, dataType)
        % Constructor method
            if nargin > 0
                % Preallocate object array
                numFiles=size(dataIn,2);
                obj(numFiles)=clsPreMeasurement;
                for n=1:numFiles
                    obj(n).timeStamp=timeStamp{n};
                    obj(n).data=dataIn{n};
                    obj(n).result=obj.readResult(obj(n), dataType);
                end % for n
            end % nargin
        end % constructor
        
        function obj=setProperty(obj,property,setting)
            obj.(property)=setting;
        end
       
    end % methods
    
    methods(Static)
        
        function obj=reCreate(obj,structIn,type)
        % Creates obj from structured data of class properties
            numTests=length(structIn);
            for num=1:numTests
                % Create object
                temp(num)=clsPreMeasurement();
                % Get variable names from structure
                names=fieldnames(structIn(num));
                % Set properties to structure values
                nMax=length(names);
                for n=1:nMax
                    % Assign property
                    temp(num)=setProperty(temp(num),names{n},structIn(num).(names{n}));
                end % for n
                if isempty(temp(num).result)
                    switch type
                        case 'sysTest'
                            temp(num).result.sysTest=clsPreMeasurement.sysTestRead(temp(num).data);
                            if strcmp(obj.transects(1).adcp.manufacturer,'TRDI')
                               temp(num).result.pt3=clsPreMeasurement.pt3(temp(num).data);
                            end
                        case 'compassCal'
                            temp(num).result.compass=clsPreMeasurement.compassRead(temp(num).data);
                        case 'compassEval'
                            temp(num).result.compass=clsPreMeasurement.compassRead(temp(num).data);
                    end
                end
            end % for num                
            obj=setProperty(obj,type,temp);
        end % reCreate  
        
        function obj=readResult(clsPreMeasObj, dataType)
            switch dataType
                case 'TCC'
                    obj.compass = clsPreMeasObj.compassRead(clsPreMeasObj.data);
                case 'TCE'
                    obj.compass = clsPreMeasObj.compassRead(clsPreMeasObj.data);
                case 'TST'
                    obj.sysTest = clsPreMeasObj.sysTestRead(clsPreMeasObj.data);
                    obj.pt3 = clsPreMeasObj.pt3(clsPreMeasObj.data);   
                case 'SCC'
                    obj.compass = clsPreMeasObj.compassRead(clsPreMeasObj.data);
                case 'SST'
                    obj.sysTest = clsPreMeasObj.sysTestRead(clsPreMeasObj.data);
            end
        end
        
        function obj = compassRead(data)
            %match regex for compass evaluation error
            [match, splits] = regexp(char(data),'(Total error:|Double Cycle Errors:|Error from calibration:)', 'match','split');
            
            if length(match) > 0
                error = regexp(char(splits(end)),'\d+\.*\d*', 'match');

                %read in the error as a double

                obj.error = str2double(error{1});
            else
                obj.error = 'N/A';
            end
            
        end
        
        function obj = sysTestRead(data)
             %match regex for number of tests and number of failures
             numTests = regexp(char(data),'(Fail|FAIL|F A I L|Pass|PASS|NOT DETECTED|P A S S)', 'match');
             numFails = regexp(char(data),'(Fail|FAIL|F A I L)', 'match');
             
             %read in the numbers as doubles
             obj.nTests = size(numTests,2);
             obj.nFailed = size(numFails,2);
        end
        
        function obj = pt3(data)
            %-----------------begin method to parse the correlation table/s
            try
            %match regex for correlation tables
            match = regexp(char(data),'Lag.*?0', 'match');

            %count the number of correlation tables to read in
            correl_count = 0;
            for x = 1:length(match)
                match2 = regexp(char(match(x)),'Bm1','match');
                correl_count = correl_count + size(match2,2);
            end 
            
            %initialize correlation matrices
            corr_hlimit_hgain_wband = [];
            corr_hlimit_lgain_wband = [];
            corr_hlimit_hgain_nband = [];
            corr_hlimit_lgain_nband = [];
            corr_linear_hgain_wband = [];
            corr_linear_lgain_wband = [];
            corr_linear_hgain_nband = [];
            corr_linear_lgain_nband = [];
     
            %get matching strings for correlation tables
            lag_match = regexp(char(data),'Lag.*?(High|H-Gain|Sin)','match');
            
            %read depending on the number of correlation tables computed at
            %beginning of function
            if correl_count == 1
                
                %get strings to parse numbers from
                num_match = regexp(char(lag_match),'0.*?(High|H-Gain|Sin)','match');

                %get all numbers from matches
                numbers = regexp(char(num_match), '\d+\.*\d*', 'match');
                
                %convert all numbers to double and add them to an array
                nums = [];
                for x = 1:size(numbers,2)
                    if (mod(x, 5) ~= 1)
                        nums = [nums str2double(numbers(x))];
                    end
                end 

                %reshape numbers to the appropriate matrix dimensions
                corr_hlimit_hgain_wband = reshape(nums,[4,8])';
            elseif correl_count == 4
                
                 %for all strings in the correlation regular expression
                 for x = 1:size(lag_match,2)
                     
                     %count the Bm1 strings to know how many tables to read
                     bm_count = regexp(char(lag_match(x)), 'Bm1', 'match');
                     
                      %get strings to parse numbers from
                     num_match = regexp(char(lag_match(x)),'0.*?(High|H-Gain|Sin)','match');
                     
                     %get all numbers from matches
                     numbers = regexp(char(num_match), '\d+\.*\d*', 'match');
                     
                     if size(bm_count,2) == 2
                         
                            %read in all strings as doubles
                            nums = [];
                            for y = 1:size(numbers,2)
                                
                                if (mod(y, 9) ~= 1)
                                    nums = [nums str2double(numbers(y))];
                                end 
                            end
                            
                            %reshape numbers to appropriate sized matrix
                            nums = reshape(nums, [8,8])';
                            
                            %assign matrix slices to corresponding
                            %variables
                            if size(corr_hlimit_hgain_wband, 2) == 0
                                corr_hlimit_hgain_wband = nums(:,1:4);
                                corr_hlimit_lgain_wband = nums(:,5:8);
                            else
                                corr_hlimit_hgain_nband = nums(:,1:4);
                                corr_hlimit_lgain_nband = nums(:,5:8);
                            end
                     else
                           
                            %read in all strings as doubles
                            nums = [];
                            for y = 1:size(numbers,2)
                              
                                if (mod(y, 17) ~= 1)
                                    nums = [nums str2double(numbers{y})];
                                end
                            end
                            
                            %reshape numbers to appropriate sized matrix
                            nums = reshape(nums, [16,8])';
                            
                            %assign matrix slices to corresponding
                            %variables
                            corr_hlimit_hgain_wband = nums(:,1:4);
                            corr_hlimit_lgain_wband = nums(:,5:8);
                            corr_hlimit_hgain_nband = nums(:,9:12);
                            corr_hlimit_lgain_nband = nums(:,13:16);
                           
                     end %end bm count if
                 end %end for loop for lag matches
            else
                
                 %for all strings in the correlation regular expression
                 for x = 1:size(lag_match,2)
                     
                     %count the Bm1 strings to know how many tables to read
                     bm_count = regexp(char(lag_match(x)), 'Bm1', 'match');
                     
                     %get strings to parse numbers from
                     num_match = regexp(char(lag_match(x)),'0.*?(High|H-Gain|Sin)','match');
                     
                     %get all numbers from matches
                     numbers = regexp(char(num_match), '\d+\.*\d*', 'match');
                     if size(bm_count,2) == 2
                         
                        %read in all strings as doubles
                        nums = [];
                        for y = 1:size(numbers,2)
                            if (mod(y, 9) ~= 1)
                                nums = [nums str2double(numbers(y))];
                            end
                        end

                        %reshape numbers to appropriate sized matrix
                        nums = reshape(nums, [8,8])';

                        %assign matrix slices to corresponding
                        %variables
                        if size(corr_hlimit_hgain_wband, 2) == 0
                            corr_hlimit_hgain_wband = nums(:,1:4);
                            corr_hlimit_lgain_wband = nums(:,5:8);
                        elseif size(corr_hlimit_hgain_nband, 2) == 0
                            corr_hlimit_hgain_nband = nums(:,1:4);
                            corr_hlimit_lgain_nband = nums(:,5:8);
                        elseif size(corr_linear_hgain_wband, 2) == 0
                            corr_linear_hgain_wband = nums(:,1:4);
                            corr_linear_lgain_wband = nums(:,5:8);
                        else
                            corr_linear_hgain_nband = nums(:,1:4);
                            corr_linear_lgain_nband = nums(:,5:8);
                        end
                    else
                            
                            %read in all strings as doubles
                            nums = [];
                            for y = 1:size(numbers,2)
                              
                                if (mod(y, 17) ~= 1)
                                    nums = [nums str2double(numbers(y))];
                                end
                            end
                            
                            %reshape numbers to appropriate sized matrix
                            nums = reshape(nums, [16,8])';
                            
                            %assign matrix slices to corresponding
                            %variables
                            if size(corr_hlimit_hgain_wband, 2) == 0
                                corr_hlimit_hgain_wband = nums(:,1:4);
                                corr_hlimit_lgain_wband = nums(:,5:8);
                                corr_hlimit_hgain_nband = nums(:,9:12);
                                corr_hlimit_lgain_nband = nums(:,13:16);
                            else
                                corr_linear_hgain_wband = nums(:,1:4);
                                corr_linear_lgain_wband = nums(:,5:8);
                                corr_linear_hgain_nband = nums(:,9:12);
                                corr_linear_lgain_nband = nums(:,13:16);
                            end
                     end % end bm count if
                 end %end for loop for lag matches
            end % end if for correlation table count
            
            %------------------end method to parse the correlation table/s
            %------------------begin method to parse sdc,cdc,rssi
            
            %get all strings for the noise floor data
            rssi_match = regexp(char(data),'(High Gain RSSI|RSSI Noise Floor \(counts\)|RSSI \(counts\)).*?(Low|>)','match');
            rssi_nums = [];
            
            %read in noise floor strings as doubles
            for x = 1:size(rssi_match,2)
                 numbers = regexp(char(rssi_match(x)), '\d+\.*\d*', 'match');
                 for y = 1:size(numbers,2)  
                     rssi_nums = [rssi_nums str2double(numbers{y})];
                 end 
            end
            
            %get all strings for the sin duty cycle data
            sin_match = regexp(char(data),'(SIN Duty Cycle|Sin Duty Cycle \(percent\)|Sin Duty\(%\)).*?(COS|Cos)','match');
            sin_nums = [];
            
            %read in sdc strings as doubles
            for x = 1:size(sin_match,2)
                 numbers = regexp(char(sin_match(x)), '\d+\.*\d*', 'match');
                 for y = 1:size(numbers,2)  
                     sin_nums = [sin_nums str2double(numbers{y})];
                 end 
            end
            
            %get all strings for the cos duty cycle data
            cos_match = regexp(char(data),'(COS Duty Cycle|Cos Duty Cycle \(percent\)|Cos Duty\(%\)).*?(Receive|RSSI)','match');
            cos_nums = [];
            
            %read in cdc strings as doubles
            for x = 1:size(cos_match,2)
                 numbers = regexp(char(cos_match(x)), '\d+\.*\d*', 'match');
                 for y = 1:size(numbers,2)  
                     cos_nums = [cos_nums str2double(numbers{y})];
                 end 
            end
            obj=[];
            %8 statements assigning data to pt3 structure if there is data
            if size(corr_hlimit_hgain_wband,2) ~= 0
                obj.hardLimit.hw.corrTable = corr_hlimit_hgain_wband;
                obj.hardLimit.hw.sdc = sin_nums(1:4);
                obj.hardLimit.hw.cdc = cos_nums(1:4);
                obj.hardLimit.hw.noiseFloor = rssi_nums(1:4);
            end
            
            if size(corr_hlimit_lgain_wband,2) ~= 0
                obj.hardLimit.lw.corrTable = corr_hlimit_lgain_wband;
                obj.hardLimit.lw.sdc = sin_nums(5:8);
                obj.hardLimit.lw.cdc = cos_nums(5:8);
                obj.hardLimit.lw.noiseFloor = rssi_nums(5:8);
            end
            
            if size(corr_hlimit_hgain_nband,2) ~= 0
                obj.hardLimit.hn.corrTable = corr_hlimit_hgain_nband;
                obj.hardLimit.hn.sdc = sin_nums(9:12);
                obj.hardLimit.hn.cdc = cos_nums(9:12);
                obj.hardLimit.hn.noiseFloor = rssi_nums(9:12);
            end
            
            if size(corr_hlimit_lgain_nband,2) ~= 0
                obj.hardLimit.ln.corrTable = corr_hlimit_lgain_nband;
                obj.hardLimit.ln.sdc = sin_nums(13:16);
                obj.hardLimit.ln.cdc = cos_nums(13:16);
                obj.hardLimit.ln.noiseFloor = rssi_nums(13:16);
            end
            
            if size(corr_linear_hgain_wband,2) ~= 0
                obj.linear.hw.corrTable = corr_linear_hgain_wband;
                obj.linear.hw.noiseFloor = rssi_nums(17:20);
            end
            
            if size(corr_linear_lgain_wband,2) ~= 0
                obj.linear.lw.corrTable = corr_linear_lgain_wband;
                obj.linear.lw.noiseFloor = rssi_nums(21:24);
            end
            
            if size(corr_linear_hgain_nband,2) ~= 0
                obj.linear.hn.corrTable = corr_linear_hgain_nband;
                obj.linear.hn.noiseFloor = rssi_nums(25:28);
            end
            
            if size(corr_linear_lgain_nband,2) ~= 0
                obj.linear.ln.corrTable = corr_linear_lgain_nband;
                obj.linear.ln.noiseFloor = rssi_nums(29:32);
            end
            catch
                obj='Error';
            end
        end % pt 3 function
    end %static methods
end % class