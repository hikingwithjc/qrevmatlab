classdef clsEdgeData
% Stores edge discharge computation properties
%
% Originally developed as part of the QRev processing program
% Programmer: David S. Mueller, Hydrologist, USGS, OSW, dmueller@usgs.gov
% Other Contributors: none
% Latest revision: 3/13/2015 

    properties (SetAccess = private)
        type % Shape of edge: 'Triangular', 'Rectangular', 'Custom', 'User Q'
        dist_m % Distance to shore
        custCoef %  Custom coefficient provided by user
        numEns2Avg % Number of ensembles to average for depth and velocitycus
        userQ_cms % Discharge provided directly from user
    end
    
    methods
 
        function obj=clsEdgeData (type, dist, varargin)
        % Construct left or right edge object from provided inputs
        %
        % INPUTS:
        %
        % type: type of edge (Triangular, Rectangular, Custom, User Q)
        % 
        % dist_m: distance to shore
        %
        % varargin: 
        %   for custom coefficient
        %       varargin{1}: edge coefficient
        %       varargin{2}: number of edge ensembles
        %   for User Q
        %       varargin{1}: discharge supplied by user
        %       varargin{2}: optional number of edge ensembles
        %   for other Triangular and Rectangular
        %       varargin{1}: number of edge ensembles
        %
        % OUTPUT:
        %
        % obj: object of clsEdgeData
        
            % If data provided set properties
            if nargin > 0
                
                % Set properties
                obj.type=type;
                obj.dist_m=dist;
                obj.numEns2Avg=10;
                obj.userQ_cms=[];
                
                switch type                       
                    
                    % Set properties for custom coefficient
                    case 'Custom'
                        obj.custCoef=varargin{1};
                        if length(varargin)>1
                            obj.numEns2Avg=varargin{2};
                        end
                    
                    % Set properties for user specified discharge
                    case 'User Q'
                        obj.userQ_cms=varargin{1};
                        if length(varargin)>1
                            obj.numEns2Avg=varargin{2};
                        end
                        
                    % Set number of ensembles to average for Triangular and
                    % Rectangular edges
                    otherwise
                        if ~isempty(varargin{1})
                            obj.numEns2Avg=varargin{1};
                        end % if
                end % switch
                
            end % if nargin
            
        end % Constructor
          
        function obj=changeProperty(obj, prop, setting)
        % Change edge data property
        %
        % INPUT:
        %
        % obj: object of clsEdgeData
        %
        % prop: name of property to set
        %
        % setting: new setting for property
        %
        % OUTPUT:
        %
        % obj: object of clsEdgeData
            
            % Set property
            obj.(prop)=setting;
            
        end % changeProperty

    end % methods
    
    methods (Static)
        
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
            % Get variable names from structure
            names=fieldnames(structIn);
            % Set properties to structure values
            nMax=length(names);
            for n=1:nMax
                obj=changeProperty(obj,names{n},structIn.(names{n}));
            end
        end % reCreate           
        
    end % statice methods
    
end % class

