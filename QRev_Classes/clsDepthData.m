classdef clsDepthData
% Process and store depth data. Supported sources include bottom track,
% vertical beam, and external depth sounder.
%
% Originally developed as part of the QRev processing program
% Programmer: David S. Mueller, Hydrologist, USGS, OSW, dmueller@usgs.gov
% Other Contributors: none
% Latest revision: 3/13/2015  
    
    properties (SetAccess=private)
        depthOrig_m % Original multi-beam depth data from transect file (includes draftOrig), in m
        depthBeams_m % Depth data from transect file adjusted for any draft changes, in m;
        %depthBeamsProcessed_m % Filtered and processed by beam
        depthProcessed_m % Depth data filtered and interpolated
        depthFreq_Hz % Defines ADCP frequency used of each raw data point
        depthInvalidIndex % Index of depths marked invalid
        depthSource % Source of depth data ("BT", "VB", "DS")
        depthSourceEns % Source of each depth value ("BT", "VB", "DS", "IN")
        draftOrig_m % Original draft from data files, in m
        draftUse_m % Draft used in computation of depthBeams_m and depthCellDepths_m
        
        depthCellDepthOrig_m % Depth cell range from the transducer, in m
        depthCellDepth_m % Depth to centerline of depth cells, in m
        depthCellSizeOrig_m % Size of depth cells in m from raw data
        depthCellSize_m % Size of depth cells in m
        
        smoothDepth % Smoothed beam depth
        smoothUpperLimit % Smooth function upper limit of window
        smoothLowerLimit % Smooth function lower limit of window
        avgMethod   % Defines averaging method: "Simple","IDW"
        filterType % Type of filter: "None", "TRDI", "Smooth"
        interpType % Type of interpolation: "None","Linear","Smooth"
        validDataMethod % QRev or TRDI
        validData % Logical array of valid mean depth for each ensemble
        validBeams % Logical array, 1 row for each beam identifying valid data
    end % Private properties
  
    methods
        
        function obj=clsDepthData(depthIn, sourceIn, freqIn, draftIn, varargin)
        % Creates object of clsDepthData
        %
        % INPUT:
        %
        % depthIn: raw depth data
        %
        % sourceIn: source of raw data
        %
        % freqIn: acoustic frequency of depth data source
        %
        % draftIn: draft for depth source
        %
        % varargin: 
        %   varargin{1}: depth to center of each cell
        %   varargin{2}: cell size of each cell
        %
        % OUTPUT:
        % 
        % obj: object of clsDepthData
        
            % If data are provide assign to properties
            if nargin > 0
                % Set object properties to input variables
                obj.depthOrig_m=depthIn;
                obj.depthBeams_m=depthIn;
                obj.depthSource=sourceIn;
                obj.depthSourceEns=cell(1,size(depthIn,2));
                obj.depthSourceEns(1,:)={sourceIn};
                obj.depthFreq_Hz=freqIn;
                obj.draftOrig_m=draftIn;
                obj.draftUse_m=draftIn;                
                obj.filterType='None';
                obj.interpType='None';
                obj.validDataMethod='QRev';
                
                % For BT data assign additional properties
                if strcmpi(sourceIn,'BT')                
                    obj.depthCellDepthOrig_m=varargin{1};
                    obj.depthCellSizeOrig_m=varargin{2};
                    obj.depthCellSize_m=varargin{2};
                    obj.depthCellDepth_m=varargin{1};
                    obj.avgMethod='IDW';
                else
                    obj.avgMethod='None';    
                end % if BT
                
                % Apply filter
                obj=applyFilter(obj,'dummy','None');
                
            end % if nargin
            
        end % End constructor
                    
        function obj=changeDraft(obj, draft)
        % Changes the draft for the specified object
        %
        % INPUT:
        %
        % obj: object of clsDepthData
        %
        % draft: new draft for object
        %
        % OUTPUT:
        %
        % obj: object of clsDepthData
        
            % Compute draft change
            draftChange=draft-obj.draftUse_m;
            obj.draftUse_m=draft;
            
            % Apply draft to ensemble depths if BT or VB
            if ~strcmp(obj.depthSource,'DS')
                obj.depthBeams_m=obj.depthBeams_m+draftChange;
                obj.depthProcessed_m=obj.depthProcessed_m+draftChange;
            end
            
            % Apply draft to depth cell locations
            if size(obj.depthCellDepth_m,1)>0
                obj.depthCellDepth_m=obj.depthCellDepth_m+draftChange;
            end
            
        end % changeDraft          
       
        function obj=addCellData(obj,btDepths)
        % Adds cell data to depth sources with no cell data, such as
        % vertical beam and depth sounder. This allows a single object the
        % contains all the required depth data.
        %
        % INPUT:
        % 
        % obj: object of clsDepthData
        %
        % btDepth: object of clsDepthData with bottom track depths
        %
        % OUTPUT:
        %
        % obj: object of clsDepthData
        
            % Assign properties from bottom track to other object
            obj.depthCellDepthOrig_m=btDepths.depthCellDepthOrig_m;
            obj.depthCellSize_m=btDepths.depthCellSize_m;
            obj.depthCellDepth_m=btDepths.depthCellDepth_m; 
        end % addCellData
        
        function obj=setAvgMethod(obj,setting)
            obj.avgMethod=setting;
        end % setAvgMethod
        
        function obj=setValidDataMethod(obj,setting)
            obj.validDataMethod=setting;
        end % setValidDataMethod
        
        function obj=computeAvgBTDepth(obj,varargin)
        % Computes average depth for btDepths
        %
        % INPUT:
        %
        % obj: object of clsDepthData
        %
        % varargin: averaging method (Simple or IDW)
        %   
        % OUTPUT:
        %
        % obj: object of clsDepthData
        
            % Ensure object's depth source is bottom track
            if strcmp(obj.depthSource,'BT')
                
                % Change averaging method is method is specified
                if ~isempty(varargin)
                    obj.avgMethod=varargin{1};
                end
                
                % Get valid depths
                depth=obj.depthBeams_m;
                depth(~obj.validBeams)=nan;
                
                % Compute average depths
                obj.depthProcessed_m=clsDepthData.averageDepth(depth,obj.draftUse_m,obj.avgMethod);
                
                % Set depths to nan if depths are not valid beam depths
                obj.depthProcessed_m(obj.validData==0)=nan;               
            end % if BT
            
        end % computeAvgDepth
               
        function obj=applyFilter(obj,transect,varargin)
        % Coordinate the application of depth filters.
        % 
        % INPUT:
        %
        % obj: object of clsDepthData
        %
        % transect: object of clsTransectData
        %
        % varargin: filter type (None, Smooth, TRDI1, TRDI2)
        %
        % OUTPUT:
        %
        % obj: object of clsDepthData
        
            % Determing filters to apply
            if ~isempty (varargin)
                setting=varargin{1};
            else
                setting=obj.filterType;
            end % if
            
            % Apply selected filter
            obj.filterType=setting;
            switch setting
                
                % No filter
                case 'None'
                    obj=filterNone(obj);
                    
                % Lowess smooth filter
                case 'Smooth'
                    obj=filterSmooth(obj,transect);
                   
                %TRDI filter for multiples only
                case 'TRDI'
                    obj=filterTRDI(obj);
                    obj.filterType='TRDI';
            end % switch  
            
            obj=validMeanData(obj);
            
            % Update depth object
            if strcmp(obj.depthSource,'BT')
                obj=computeAvgBTDepth(obj);
            else
                obj.depthProcessed_m=obj.depthBeams_m;
                obj.depthProcessed_m(~obj.validData)=nan;
            end
            
        end % applyFilter
 
        function obj=applyInterpolation(obj,transect,varargin)
        % Coordinates application of interpolations
        %
        % INPUT:
        %
        % obj: object of clsDepthData
        %
        % transect: object of clsTransectData
        %
        % varargin: interpolation setting
        %
        % OUTPUT:
        % 
        % obj: object of clsDepthData
        
            % Determing filters to apply
            if ~isempty (varargin)
                setting=varargin{1};
            else
                setting=obj.interpType;
            end % if
            
            % Apply selected interpolation
            obj.interpType=setting;
            switch setting
                
                % No filtering
                case 'None'
                    obj=interpolateNone(obj);
                    
                % Hold last valid depth indefinitely
                case 'HoldLast'
                    obj=interpolateHoldLast(obj);
                    
                % Use value from Lowess smooth
                case 'Smooth'
                    obj=interpolateSmooth(obj);
                    
                % Linear interpolation
                case 'Linear'
                    obj=interpolateLinear(obj,transect);
            end % switch 
            
            % Identify ensembles with interpolated depths
            idx=find(obj.validData(1,:)==false);
            idx2=~isnan(obj.depthProcessed_m(idx));
            obj.depthSourceEns(idx(idx2))={'IN'};
            
        end % applyInterpolation
        
        function obj=applyComposite(obj,compDepth,compSource)
        % Applies the data from compDepth computed in clsDepthStructure to
        % clsDepthData object.
        %
        % INPUT:
        %
        % obj: object of clsDepthData
        %
        % compDepth: composite depth computed in clsDepthStructure
        %
        % compSource: source of composite depth (BT, VB, DS, 
        %
        % OUTPUT:
        %
        % obj: object of clsDepthData
        
                % Assign composite depth to property
                obj.depthProcessed_m=compDepth;
                
                % Assign appropriate composite source for each ensemble
                obj.depthSourceEns(compSource==1)={'BT'};
                obj.depthSourceEns(compSource==2)={'VB'};
                obj.depthSourceEns(compSource==3)={'DS'};
                obj.depthSourceEns(compSource==4)={'IN'};
                obj.depthSourceEns(compSource==0)={'NA'};
                
        end % applyComposite
        
        function obj=sosCorrection(obj,ratio)
        % Correct depth for new speed of sound setting
        %
        % INPUT: 
        %
        % obj: object of clsDepthData
        %
        % ratio: ration of new to old speed of sound value
        %
        % OUPUT:
        %
        % obj: object of clsDepthData
        
            % Correct unprocessed depths
            obj.depthBeams_m=obj.draftUse_m+bsxfun(@times,obj.depthBeams_m-obj.draftUse_m,ratio);
            
            % Correct processed depths
            obj.depthProcessed_m=obj.draftUse_m+bsxfun(@times,obj.depthProcessed_m-obj.draftUse_m,ratio);
            
            % Correct cell size and location
            obj.depthCellSize_m=bsxfun(@times,obj.depthCellSize_m,ratio);
            obj.depthCellDepth_m=obj.draftUse_m+bsxfun(@times,obj.depthCellDepth_m-obj.draftUse_m,ratio);
        end % sosCorrection
        
        function obj=setProperty(obj,property,setting)
            obj.(property)=setting;
        end
 
    end % methods
    
    methods (Access=private)
        
        function obj=validMeanData(obj)
            
            if strcmp(obj.depthSource,'BT')
                obj.validData=true(size(obj.validBeams(1,:)));
                nvalid=sum(obj.validBeams);
                if strcmp(obj.validDataMethod,'TRDI')
                    obj.validData(nvalid<3)=false;
                else
                    obj.validData(nvalid<2)=false;
                end
            else
                obj.validData=obj.validBeams;
            end
        end % validMeanData
            
        function obj=filterNone(obj)
        % Applies no filter to depth data
        %
        % INPUT:
        %
        % obj: object of clsDepthData
        %
        % OUTPUT:
        % 
        % obj: object of clsDepthData
        
            % Set all ensembles to have valid data
            obj.validBeams=true(size(obj.depthBeams_m));
            
            % Set ensembles with no depth data to invalid
            obj.validBeams(obj.depthBeams_m==0)=false;
            obj.validBeams(isnan(obj.depthBeams_m))=false;
            
            % Set filter type
            obj.filterType='None';
            
        end % filterNone
        
        function obj=filterSmooth(obj,transect)
        %  Filters Depths Measured by Each Beam 
        %  This filter uses a moving InterQuartile Range filter on residuals from a
        %  robust Loess smooth of the depths in each beam to identify unnatural
        %  spikes in the depth measurements from each beam. Each beam is filtered
        %  independently. The filter criteria are set to be the maximum of the IQR
        %  filter, 5% of the measured depth, or 0.1 meter.
        %
        %  Recommended filter settings:
        %   filterWidth=20;
        %   halfWidth=10;
        %   multiplier=15;
        %
        %  INPUT REQUIREMENTS
        %  depthRaw=Bt.depthBeams_m - number of beams (row) by number of ensembles 
        %               matrix (col)of measured depths in m
        %  filterWidth - number of points used in robust Loess smooth
        %  halfWidth - number of points to each side of target point used in 
        %               in computing the IQR. This is the raw number of points
        %               actual points used may be less if some are bad.
        %  multipilier - number multipilied times the IQR to determine the filter 
        %               criteria
        %
        %  OUTPUT DESCRIPTION
        %   depthFiltered - number of beams (row) by number of ensembles matrix (col)
        %                   of filtered depths in m. Depths not passing the filter
        %                   are set to nan.
        %   numFiltered - matrix of the number of dephts that were filtered out 
        %                   for each beam (row).
        %
        %   David S. Mueller, USGS, OSW
        %   9/8/2005
        %
        % INPUT:
        %
        % obj: object of clsDepthData
        %
        % OUTPUT:
        %
        % obj: object of cslDepthData
        
            % If the smooth has not been computed, compute smooth
            if isempty(obj.smoothDepth)
                
                % Set filter characteristics
                obj.filterType='Smooth';
                cycles=3;
                filterWidth=20;
                halfWidth=10;
                multiplier=15;

                % Determine number of beams
                [nBeams nEnsembles]=size(obj.depthOrig_m);
                depthRaw=obj.depthOrig_m;

                % Set bad depths set to nan
                depth=nan(size(depthRaw));
                depth(depthRaw>0)=depthRaw(depthRaw>0);

                % Arrays initialized.
                depthSmooth=nan(size(depth));
                depthRes=nan(size(depth));
                depthFilter=nan(size(depth));
                upperLimit=nan(size(depth));
                lowerLimit=nan(size(depth));
                badDepth=nan(size(depth));
                depthFiltered=depth;

                % Create position array
                if ~isempty(transect.boatVel.(transect.boatVel.selected))
                    boatVelX=transect.boatVel.(transect.boatVel.selected).uProcessed_mps;
                    boatVelY=transect.boatVel.(transect.boatVel.selected).vProcessed_mps;
                    trackX=boatVelX.*transect.dateTime.ensDuration_sec;
                    trackY=boatVelY.*transect.dateTime.ensDuration_sec;
                else
                    trackX=nan;
                    trackY=nan;
                end
                idx=find(isnan(trackX(2:end)));
                if isempty(idx)
                    x=nancumsum(sqrt(trackX.^2+trackY.^2));
                else
                    x=nancumsum(transect.dateTime.ensDuration_sec);
                end
                
                % Loop for each beam, smooth is applied to each beam
                for j=1:nBeams        
                    if sum(~isnan(depthFiltered(j,:)))./size(depthFiltered,2)>0.5
                        % Compute residuals based on robust loess smooth
                        if length(x)>1
                            depthSmooth(j,:)=smooth(x,depthFiltered(j,:),filterWidth,'rloess');
                        else
                            depthSmooth(j,:)=depthFiltered(j,:);
                        end
                        depthRes(j,:)=depth(j,:)-depthSmooth(j,:);

                        % Run filter multiple times    
                        for n=1:cycles

                            % Compute inner quartile range
                            filArray=clsDepthData.runIQR(halfWidth,depthRes(j,:)');

                            % Compute filter criteria and apply appropriate limits to criteria        
                            criteria=multiplier.*filArray;
                            idx=find(criteria<max([depth(j,:).*0.05;repmat([0.1],1,size(depth(j,:),2))]));
                            criteria(idx)=max([depth(j,idx).*0.05;repmat([0.1],1,size(idx,2))]);

                            % Compute limits        
                            upperLimit(j,:)=depthSmooth(j,:)+criteria;
                            lowerLimit(j,:)=depthSmooth(j,:)-criteria;

                        end % for cycles
                    else
                        depthSmooth(j,:)=nan;
                        upperLimit(j,:)=nan;
                        lowerLimit(j,:)=nan;
                    end % if

                end % for nBeams
                % Save smooth results to avoid recomputing them if need later
                obj.smoothDepth=depthSmooth;
                obj.smoothUpperLimit=upperLimit;
                obj.smoothLowerLimit=lowerLimit;
            
            end % if isempty
         
            % Reset validData
            obj=filterNone(obj);
            
            % Set filter type
            obj.filterType='Smooth';
            
            % Determine number of beams
            [nBeams, nEnsembles]=size(obj.depthOrig_m);
            
            % Get depth
            depthRaw=obj.depthOrig_m;

            % Set bad depths to nan
            depth=nan(size(depthRaw));
            depth(depthRaw>0)=depthRaw(depthRaw>0);
            
            % Apply filter 
            for j=1:nBeams      
%                if ~isnan(obj.smoothUpperLimit(j,:))
                if nansum(obj.smoothUpperLimit(j,:))>0
                    bad_idx=find(depth(j,:)>obj.smoothUpperLimit(j,:) | depth(j,:)<obj.smoothLowerLimit(j,:));
                    % Update depth matrix
                    depthRes(j,bad_idx)=nan;
                    % Update validData matrix
                    obj.validBeams(j,bad_idx)=false;
                else
                    bad_idx=isnan(depth(j,:));
                    obj.validBeams(j,bad_idx)=false;
                end
            end % for nBeams
            
        end % filterSmooth 
        
        function obj=filterTRDI(obj)
        % Filter used by TRDI to filter out multiple reflections that get
        % digitized as the depth.
        %
        % INPUT:
        % 
        % obj: object of clsDepthData
        % 
        % OUTPUT:
        %
        % obj: object of clsDepthData
        
            % Determine number of beams
            [nBeams nEnsembles]=size(obj.depthOrig_m);
            
            % Assign raw depth data to local variable
            depthRaw=obj.depthOrig_m;
            
            % Reset filters to none
            obj=filterNone(obj);
            
            % Set filter to TRDI
            obj.filterType='TRDI';
            
            % The depth is invalid when the difference between beams is
            % greater than 75%
            for n=1:nBeams
                depthRatio=abs(bsxfun(@ldivide,depthRaw,depthRaw(n,:)));
                exceeded=depthRatio>1.75;
                exceeded=sum(exceeded);
                obj.validBeams(n,exceeded>0)=false;
            end % for n
        end % filterTRDI
        
        function obj=interpolateNone(obj)
        % Applies no interpolation
        %
        % INPUT:
        %
        % obj: object of clsDepthData
        %
        % OUTPUT:
        %
        % obj: object of clsDepthData
            
            % Compute processed depth without interpolation
            if strcmp(obj.depthSource,'BT')
                % Bottom track depths
                obj=computeAvgBTDepth(obj);
            else
                % Vertical beam or depth sounder depths
                obj.depthProcessed_m=obj.depthBeams_m;
            end
            
            obj.depthProcessed_m(~obj.validData)=nan;
            
            % Set interpolation type
            obj.interpType='None';

        end % interpolateNone
        
        function obj=interpolateHoldLast(obj)
        % This function holds the last valid value until the next valid
        % data point.
        %
        % INPUT:
        %
        % obj: object of cslDepthData
        %
        % OUTPUT:
        %
        % obj: object of clsDepthData
        
            % Get number of ensembles
            nEnsembles=length(obj.depthProcessed_m);

            % Process data by ensemble
            for n=2:nEnsembles
                
                % If current ensemble's depth is invalid assign depth from
                % previous ensemble
                if isnan(obj.depthProcessed_m(n)) 
                    obj.depthProcessed_m(n)=obj.depthProcessed_m(n-1);
                end % if
                
            end % for n
            
        end % interpolateHoldLast
        
        function obj=interpolateSmooth(obj)
        % Apply interpolation based on loess smooth
        %
        % INPUT:
        %
        % obj: object of cslDepthData
        %
        % OUTPUT:
        %
        % obj: object of clsDepthData
        
            % Set interpolation type
            obj.interpType='Smooth';
            
            % Get depth data from object
            depthNew=obj.depthBeams_m;
            
            % Update depth data with interpolated depths
            depthNew(~obj.validBeams)=obj.smoothDepth(~obj.validBeams);
            
            % Compute processed depths with interpolated values
            if strcmp(obj.depthSource,'BT')
                
                % Temporarily change obj.depthBeams_m to compute average for
                % bottom track based depths
                tempSave=obj.depthBeams_m;
                obj.depthBeams_m=depthNew;
                obj=computeAvgBTDepth(obj);
                obj.depthBeams_m=tempSave;
                
            else
                obj.depthProcessed_m=depthNew;
            end
            
        end % interpolateSmooth
        
        function obj=interpolateLinear(obj,transect)
        % Apply linear interpolation  
        %
        % INPUT:
        %
        % obj: object of cslDepthData
        %
        % OUTPUT:
        %
        % obj: object of clsDepthData
        
            % Set interpolation type
            obj.interpType='Linear';
            
            % Create position array
            if ~isempty(transect.boatVel.(transect.boatVel.selected))
                boatVelX=transect.boatVel.(transect.boatVel.selected).uProcessed_mps;
                boatVelY=transect.boatVel.(transect.boatVel.selected).vProcessed_mps;
                trackX=boatVelX.*transect.dateTime.ensDuration_sec;
                trackY=boatVelY.*transect.dateTime.ensDuration_sec;
            else
                trackX=nan(size(transect.boatVel.btVel.uProcessed_mps));
                trackY=nan(size(transect.boatVel.btVel.vProcessed_mps));
            end
            idx=find(isnan(trackX(2:end)));
            
            % If the navigation reference has no gaps use it for
            % interpolation, if not, use time.
            if isempty(idx)
                % Compute distance along ship track
                x=nancumsum(sqrt(trackX.^2+trackY.^2));

            else
                % Compute accumlated time
                x=nancumsum(transect.dateTime.ensDuration_sec);
            end
            
            % Determine number of beams
            nBeams=size(obj.depthBeams_m,1);
            
            % Create strict monotonic arrays for depth and track by identifying duplicate
            % track values. The first track value is used and the remaining duplicates
            % are set to nan. The depth assigned to that first track value is the
            % average of all duplicates. The depths for the duplicates are then set to
            % nan. Only valid strictly monotonic track and depth data are used for the
            % input into the linear interpolation. Only the interpolated data for
            % invalid depths are added to the valid depth data to create depthNew.
            depthMono=obj.depthBeams_m;
            xMono=x;
            idx0=find(diff(x)==0);
            if ~isempty(idx0)
                group=mat2cell(idx0,1,diff([0,find(diff(idx0) ~= 1),length(idx0)]));
                nGroup=length(group);
                for k=1:nGroup
                    indices=group{k};
                    indices=[indices, indices(end)+1];
                    firstidx=indices(1);
                    depthavg=nanmean(depthMono(:,indices),2);
                    depthMono(:,indices(1))=depthavg;
                    depthMono(:,indices(2:end))=nan;
                    x(indices(2:end))=nan;
                end % for k
            end % if idx0
            
            % Interpolate each beam
            depthNew=obj.depthBeams_m;
            for n=1:nBeams
                validdepthMono=~isnan(depthMono(n,:));
                validxMono=~isnan(xMono);
                validData=obj.validBeams(n,:);
                valid=all([validdepthMono;validxMono;validData]);
                
                if sum(valid)>1
                    % Compute interpolation function from all valid data
                    depthInt(n,:) = interp1(xMono(valid),depthMono(n,valid),xMono);
                    depthNew(n,~obj.validBeams(n,:))=depthInt(n,~obj.validBeams(n,:));
                else
                    % No valid data
                    depthInt(n,:)=nan(1,length(valid));
                end % if valid
            end % for nBeams  
                       
            % Compute processed depths using the interpolated data
            if strcmp(obj.depthSource,'BT')
                % Bottom track depths
                obj.depthProcessed_m=clsDepthData.averageDepth(depthNew,obj.draftUse_m,obj.avgMethod);
            else
                % Vertical beam or depth sounder depths
                obj.depthProcessed_m=depthNew;
            end
        end % interpolateLinear
            
    end % private methods

    
    methods (Static)

        function [avgDepth]=averageDepth(depth,draft,method)
        % Compute the average depth from bottom track beam depths
        %
        % INPUT:
        % 
        % depth: array of beam depths
        %
        % draft: draft of ADCP
        %
        % method: averaging method (Simple, IDW)
        
            % Determine averaging method to use
            if strcmp(method,'Simple')
                % Simple numeric average
                avgDepth=nanmean(depth);
            else
                % Compute inverse weighted mean depth
                rng=depth-draft;
                w=1-rng./repmat(nansum(rng,1),4,1);
                avgDepth=draft+nansum((rng.*w)./repmat(nansum(w,1),4,1),1);
                avgDepth(avgDepth==draft)=nan;
            end % avgMethod
        end % averageDepth
        
        function [iqrArray]=runIQR(halfWidth,mydata)
        %  Computes a running Innerquartile Range
        %  The routine accepts a column vector as input. "halfWidth" number of data
        %  points for computing the Innerquartile Range are selected before and
        %  after the target data point, but not including the target data point.
        %  Near the ends of the series the number of points before or after are
        %  reduced. nan in the data are counted as points. The IQR is computed on
        %  the selected subset of points. The process occurs for each point in the
        %  provided column vector. A column vector with the computed IQR
        %  at each point is returned.
        %
        %   David S. Mueller, USGS, OSW
        %   9/8/2005
        %
        % INPUT:
        %
        % halfWidth: number of ensembles before and after current ensemble
        % which are used to compute the IQR
        %
        % myData: data for which the IQR is computed
        %
        % OUTPUT:
        %
        % iqrArray: innerquartile range for each ensemble
        
            % Determine number of points to process
            nPts=size(mydata,1);
            
            if nPts<20
                halfWidth=floor(nPts/2);
            end
            
            % Compute IQR for each point
            for n=1:nPts

                % Sample selection for 1st point    
                if n==1
                    sample=mydata(2:1+halfWidth);
                    
                % Sample selection a end of data set        
                elseif n+halfWidth>nPts
                    sample=[mydata(n-halfWidth:n-1) ; mydata(n+1:nPts)];                    

                % Sample selection at beginning of data set        
                elseif halfWidth>=n
                    sample=[mydata(1:n-1) ;mydata(n+1:n+halfWidth)];


                % Sample selection in body of data set
                else
                    sample=[mydata(n-halfWidth:n-1); mydata(n+1:n+halfWidth)];
                end      
                iqrArray(n)=iqr(sample);
            end % for n
        end % runIQR
        
        function obj=reCreate(obj,structIn,type)
        % Creates object from structured data of class properties
            % Create object
            obj.(type)=clsDepthData();
            % Get variable names from structure
            names=fieldnames(structIn);
            % Set properties to structure values
            nMax=length(names);
            for n=1:nMax
                obj.(type)=setProperty(obj.(type),names{n},structIn.(names{n}));
            end
            if isempty(obj.(type).depthCellSizeOrig_m)
                obj.(type).depthCellSizeOrig_m=obj.(type).depthCellSize_m;
            end
        end % reCreate        
        
    end % Static methods
end % depth Class     