classdef clsBoatData
% Class to process and store boat velocity data.
%
% Originally developed as part of the QRev processing program
% Programmer: David S. Mueller, Hydrologist, USGS, OSW, dmueller@usgs.gov
% Other Contributors: none
% Latest revision: 3/13/2015

    properties (SetAccess=private) 
        
        % Variables passed to constructor
        rawVel_mps % Contains the raw unfiltered velocity data in m/s. Rows 1-4 are
                   % beams 1,2,3,4 if beam or u,v,w,d if otherwise
        frequency_hz % Defines ADCP frequency used for velocity measurement
        origCoordSys % Defines the original raw data velocity coordinate system "Beam", "Inst", "Ship", "Earth"    
        navRef % Defines the original raw data navigation reference: "None", "BT", "GGA", "VTG"
        
        % Coordinate transformed data
        coordSys % Defines the current coordinate system "Beam", "Inst", "Ship", "Earth" for u, v, w, and d
        u_mps % Horizontal velocity in x-direction, in m/s
        v_mps % Horizontal velocity in y-direction, in m/s
        w_mps % Vertical velocity (+ up), in m/s
        d_mps % Difference in vertical velocities compute from opposing beam pairs, in m/s
        numInvalid % Number of ensembles with invalid velocity data
        bottomMode % BT mode for TRDI, 'Variable' for SonTek
        
        % Processed data
        uProcessed_mps % Horizontal velocity in x-direction filtered and interpolated
        vProcessed_mps % Horizontal velocity in y-direction filtered and interpolated
        processedSource % Source of velocity: BT, VTG, GGA, INT
        % Filter and interpolation properties
        dFilter % Difference velocity filter "Manual", "Off", "Auto"
        dFilterThreshold % Threshold for difference velocity filter
        wFilter % Vertical velocity filter "On", "Off"
        wFilterThreshold % Threshold for vertical velocity filter
        gpsDiffQualFilter % Differential correction quality (1, 2, 4)
        gpsAltitudeFilter % Change in altitude filter "Auto", "Manual", "Off"
        gpsAltitudeFilterChange % Threshold from mean for altitude filter
        gpsHDOPFilter % HDOP filter "Auto", "Manual", "Off"
        gpsHDOPFilterMax % Max acceptable value of HDOP
        gpsHDOPFilterChange % Maximum change allowed from mean
        smoothFilter % Filter based on smoothing function
        smoothSpeed % Smoothed boat speed
        smoothUpperLimit % Smooth function upper limit of window
        smoothLowerLimit % Smooth function lower limit of window
        interpolate % Type of interpolation: "None", "Linear", "Smooth" etc.
        beamFilter % 3 for 3-beam solutions, 4 for 4-beam solutions
        validData % Logical array of identifying valid and invalid data for each filter applied
                    % Row 1 - composite
                    % Row 2 - original  
                    % Row 3 - dFilter or diffQual
                    % Row 4 - wFilter or altitude
                    % Row 5 - smoothFilter 
                    % Row 6 - beamFilter or HDOP
    end % Private properties
    
    methods
        function obj=clsBoatData(source,velIn,freqIn,coordSysIn,navRefIn,varargin) 
        % Creates object and sets properties using data provided.
        % 
        % INPUT:
        %
        % source: manufacturer (TRDI, SonTek)
        %
        % velIn: boat velocity array
        %
        % freqIn: acoustic frequency boat velocity
        %
        % coordSysIn: coordinate system of boat velocity
        %
        % navRefIn: source of boat velocity (BT, GGA, VTG)
        %
        % varargin:
        %   varagin{1}: is for TRDI data and specifies whether 3 beam solutions
        % were selected in the mmt file and the bottom mode. These are not
        % specified for SonTek data.
        %   varargin{2}: bottom mode for TRDI ADCP
        %
        % OUTPUT:
        % 
        % obj: object of clsBoatData
        
            % Check for number of input arguments
            if nargin > 0
                
                % Mark invalid SonTek data as nan
                if strcmpi(source,'SonTek')
                    velIn=obj.filterSonTek(velIn);
                end % if SonTek
                
                % Assign input arguments to the object properties
                obj.rawVel_mps=velIn;
                obj.frequency_hz=freqIn;
                obj.coordSys=coordSysIn;
                obj.origCoordSys=coordSysIn;
                obj.navRef=navRefIn;
                obj.beamFilter=3;
                
                % Bottom mode is variable unless TRDI with BT reference
                obj.bottomMode='Variable';
                if strcmpi(source,'TRDI')&& strcmpi(navRefIn,'BT')
                    obj.bottomMode=varargin{2}; 
                    % Apply 3-beam setting from mmt file
                    if varargin{1}<0.5
                        obj.beamFilter=4;
                    end
                end % if TRDI
                
                if strcmp(navRefIn,'BT')
                    
                    % Boat velocities are referenced to ADCP not
                    % the streambed and thus must be reversed.
                    obj.u_mps=-1.*velIn(1,:);
                    obj.v_mps=-1.*velIn(2,:);
                    obj.w_mps=velIn(3,:);
                    obj.d_mps=velIn(4,:);
                    
                    % Default filtering applied during initial construction of
                    % object
                    obj.dFilter='Off';
                    obj.dFilterThreshold=99;
                    obj.wFilter='Off';
                    obj.wFilterThreshold=99;
                    obj.smoothFilter='Off';
                    obj.interpolate='None';
                    
                else
                    % GPS referenced boat velocity
                    obj.u_mps=velIn(1,:);
                    obj.v_mps=velIn(2,:);
                    obj.w_mps=nan;
                    obj.d_mps=nan;
                    
                    % Default filtering
                    obj.gpsDiffQualFilter=2;
                    obj.gpsAltitudeFilter='Off';
                    obj.gpsAltitudeFilterChange=3;
                    obj.gpsHDOPFilter='Off';
                    obj.gpsHDOPFilterMax=2.5;
                    obj.gpsHDOPFilterChange=1;
                    obj.smoothFilter='Off';
                    obj.interpolate='None';
                end
                
                % Assign data to processed property
                obj.uProcessed_mps=obj.u_mps;
                obj.vProcessed_mps=obj.v_mps;
                
                % Preallocate arrays
                nEnsembles=size(velIn,2);
                obj.validData=true(6,nEnsembles);
                obj.smoothSpeed=nan(1,nEnsembles);
                obj.smoothUpperLimit=nan(1,nEnsembles);
                obj.smoothLowerLimit=nan(1,nEnsembles);
                
                % Determine number of raw invalid
                % -------------------------------
                % Find invalid raw data
                validVel=true(size(obj.rawVel_mps));
                validVel(isnan(obj.rawVel_mps))=false;
               
                % Identify invalid ensembles
                if strcmp(navRefIn,'BT')
                    obj.validData(2,sum(validVel)<3)=false;
                else
                    obj.validData(2,sum(validVel)<2)=false;
                end
                
                % Combine all filter data to composite valid data
                obj.validData(1,:)=all(obj.validData(2:end,:));
                obj.numInvalid=sum(~obj.validData(1,:));
                obj.processedSource=cell(size(obj.u_mps));
                obj.processedSource(obj.validData(1,:))={navRefIn};
                obj.processedSource(~obj.validData(1,:))={'INT'};
            end % if nargin
            
        end % End constructor 
        
        function obj=changeCoordSys(velObj,newCoordSys,sensors,adcp)
        % This function allows the coordinate system to be changed.
        % Current implementation is only to allow change to a higher order
        % coordinate system Beam - Inst - Ship - Earth. 
        %
        % INPUT:
        % 
        % velObj: object of clsBoatData
        %
        % newCoordSys: new coordinate system (Beam, Inst, Ship,
        % Earth)
        %
        % sensors: object of clsSensors
        %
        % adcp: object of clsInstrumentData
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData

            % Assign the input to the output object
             obj=velObj;
            % Remove any trailing spaces
            oCoordSys=strtrim(velObj.origCoordSys);
            
            if ~strcmp(oCoordSys,newCoordSys)    
                % Assign the transformation matrix and retrive the
                % sensor data.
                tMatrix=adcp.tMatrix.matrix;
                tMatrixFreq=adcp.frequency_hz;
                p=sensors.pitch_deg.(sensors.pitch_deg.selected).data;
                r=sensors.roll_deg.(sensors.roll_deg.selected).data;
                h=sensors.heading_deg.(sensors.heading_deg.selected).data;
                
                % Modify the transformation matrix and heading, pitch,
                % and roll values based on the original coordinate
                % system so that only the needed values are used in
                % computing the new coordinate system.
                switch oCoordSys
                    case 'Beam'
                        origSys=1;
                    case 'Inst'
                        origSys=2;
                        tMatrix(:)=eye(size(tMatrix));
                    case 'Ship'
                        origSys=3;
                        p=zeros(size(h));
                        r=zeros(size(h));
                        tMatrix(:)=eye(size(tMatrix));
                    case 'Earth'
                        origSys=4;
                end % switch
                
                % Assign a value to the new coordinate system
                switch newCoordSys
                    case 'Beam'
                        newSys=1;
                    case 'Inst'
                        newSys=2;
                    case 'Ship'
                        newSys=3;
                    case 'Earth'
                        newSys=4;
                end % switch
                
                % Check to ensure the new coordinate system is a higher
                % order than the original system.
                if newSys-origSys>0 
                    % Compute trig functions for heading, pitch, and roll
                    CH=cosd(h);
                    SH=sind(h);
                    CP=cosd(p);
                    SP=sind(p);
                    CR=cosd(r);
                    SR=sind(r);  
                    
                    % Preallocate array
                    velChanged=nan(size(velObj.rawVel_mps));
                    nEns=size(velObj.rawVel_mps,2);
                    
                    % Apply matrices to data on an ensemble by ensemble basis
                    for ii=1:nEns  
                        % Compute matrix for heading, pitch, and roll
                        hprMatrix=[((CH(ii).*CR(ii))+(SH(ii).*SP(ii).*SR(ii))) (SH(ii).*CP(ii)) ((CH(ii).*SR(ii))-(SH(ii).*SP(ii).*CR(ii)));...
                                ((-1.*SH(ii).*CR(ii))+(CH(ii).*SP(ii).*SR(ii))) (CH(ii).*CP(ii)) ((-1.*SH(ii).*SR(ii))-(CH(ii).*SP(ii).*CR(ii)));...
                                (-1.*CP(ii).*SR(ii)) (SP(ii)) (CP(ii).*CR(ii))];   
                            
                        % Transform beam coordinates
                        % --------------------------
                        if strcmpi(oCoordSys,'Beam') 
                            
                            % Determine frequency index for 
                            % transformation matrix
                            if size(tMatrix,3)>1
                                idxFreq=find(tMatrixFreq==velObj.frequency(ii));
                                tMult=tMatrix(:,:,idxFreq);
                            else
                                tMult=tMatrix;
                            end
                            
                            % Get velocity data
                            vel=squeeze(velObj.rawVel_mps(:,ii));
                            
                            % Check for invalid beams
                            idx3Beam=find(isnan(vel));
                            
                            % 3-beam solution
                            if length(idx3Beam)==1 
                                
                                % Special processing for RiverRay
                                if strcmpi(adcp.model,'RiverRay')

                                    % Set beam pairing
                                    beamPair1a=1;
                                    beamPair1b=2;
                                    beamPair2a=3;
                                    beamPair2b=4;  
                                    
                                    % Set speed of sound correction variable
                                    % NOTE: Currently (20130906) WinRiver II
                                    % does not use a variable correction and
                                    % assumes the speed of sound and the
                                    % reference speed of sound are the same.                                    
                                    % sos=sensors.speedOfSound_mps.selected.data(ii);
                                    % sosReference=1536;
                                    % sosCorrection=sqrt(((2.*sosReference)./sos).^2-1);  
                                    sosCorrection=sqrt(3);
 
                                    % Reconfigure transformation matrix based
                                    % on which beam is invalid
                                    switch idx3Beam
                                        
                                        % Beam 1 invalid
                                        case beamPair1a
                                            
                                            % Double valid beam in invalid pair
                                            tMult(1:2,beamPair1b)=tMult(1:2,beamPair1b).*2;
                                            
                                            % Eliminate invalid pair from vertical velocity computations
                                            tMult(3,:)=[0 0 1./sosCorrection 1./sosCorrection];
                                            
                                            % Reconstruct transformation matrix as a 3x3 matrix
                                            tMult=tMult(1:3,[beamPair1b,beamPair2a,beamPair2b]);
                                            
                                            % Reconstruct beam velocity matrix to use only valid beams
                                            vel=vel([beamPair1b,beamPair2a,beamPair2b]);
                                            
                                            % Apply transformation matrix
                                            tempT=tMult*vel;
                                            
                                            % Correct horizontal velocity for
                                            % invalid pair with the vertical
                                            % velocity and speed of sound
                                            % correction
                                            tempT(1)=tempT(1)+tempT(3).*sosCorrection;

                                        % Beam 2 invalid
                                        case beamPair1b
                                            
                                            % Double valid beam in invalid pair
                                            tMult(1:2,beamPair1a)=tMult(1:2,beamPair1a).*2;
                                            
                                            % Eliminate invalid pair from vertical velocity computations
                                            tMult(3,:)=[0 0 1./sosCorrection 1./sosCorrection];
                                            
                                            % Reconstruct transformation matrix as a 3x3 matrix
                                            tMult=tMult(1:3,[beamPair1a,beamPair2a,beamPair2b]);
                                            
                                            % Reconstruct beam velocity matrix to use only valid beams
                                            vel=vel([beamPair1a,beamPair2a,beamPair2b]);
                                            
                                            % Apply transformation matrix
                                            tempT=tMult*vel;
                                            
                                            % Correct horizontal velocity for
                                            % invalid pair with the vertical
                                            % velocity and speed of sound
                                            % correction                                        
                                            tempT(1)=tempT(1)-tempT(3).*sosCorrection;

                                         % Beam 3 invalid
                                        case beamPair2a
                                            
                                            % Double valid beam in invalid pair
                                            tMult(1:2,beamPair2b)=tMult(1:2,beamPair2b).*2;
                                            
                                            % Eliminate invalid pair from vertical velocity computations
                                            tMult(3,:)=[1./sosCorrection 1./sosCorrection 0 0 ];
                                            
                                            % Reconstruct transformation matrix as a 3x3 matrix
                                            tMult=tMult(1:3,[beamPair1a,beamPair1b,beamPair2b]);
                                            
                                            % Reconstruct beam velocity matrix to use only valid beams
                                            vel=vel([beamPair1a,beamPair1b,beamPair2b]);
                                            
                                            % Apply transformation matrix
                                            tempT=tMult*vel;
                                            
                                            % Correct horizontal velocity for
                                            % invalid pair with the vertical
                                            % velocity and speed of sound
                                            % correction                                         
                                            tempT(2)=tempT(2)-tempT(3).*sosCorrection;

                                            % Beam 4 invalid
                                        case beamPair2b
                                            
                                            % Double valid beam in invalid pair
                                            tMult(1:2,beamPair2a)=tMult(1:2,beamPair2a).*2;
                                            
                                            % Eliminate invalid pair from vertical velocity computations
                                            tMult(3,:)=[1./sosCorrection 1./sosCorrection 0 0 ];
                                            
                                            % Reconstruct transformation matrix as a 3x3 matrix
                                            tMult=tMult(1:3,[beamPair1a,beamPair1b,beamPair2a]);
                                            
                                            % Reconstruct beam velocity matrix to use only valid beams
                                            vel=vel([beamPair1a,beamPair1b,beamPair2a]);
                                            
                                            % Apply transformation matrix
                                            tempT=tMult*vel;
                                            
                                            % Correct horizontal velocity for
                                            % invalid pair with the vertical
                                            % velocity and speed of sound
                                            % correction
                                            tempT(2)=tempT(2)+tempT(3).*sosCorrection;
                                            
                                    end % switch case 

                                   
                                else
                                    
                                    % 3 Beam solution for non-RiverRay
                                    vel3BeamZero=vel;
                                    vel3BeamZero(isnan(vel))=0;
                                    velError=tMult(4,:)*vel3BeamZero;
                                    vel(idx3Beam)=-1.*velError./tMult(4,idx3Beam);
                                    tempT=tMult*vel;
                                end % if RiverRay
                                
                                % Apply transformation matrix for 3
                                % beam solutions
                                tempTHPR=hprMatrix*tempT(1:3,:);
                                tempTHPR(4)=nan;
                                
                            else 
                                
                                % Apply transformation matrix for 4 beam solutions                       
                                tempT=tMult*squeeze(velObj.rawVel_mps(:,ii));    
                                
                                % Apply hprMatrix
                                tempTHPR=hprMatrix*tempT(1:3,:);
                                tempTHPR(4,:)=tempT(4,:);
                                
                             end % if 3-beams   
                            
                        else
                            
                            % Get velocity data
                            vel=squeeze(velObj.rawVel_mps(:,ii));
                            
                            % Apply heading pitch roll for inst and ship
                            % coordinate data
                            tempTHPR=hprMatrix*vel(1:3);
                            tempTHPR(4,:)=vel(4); 
                            
                        end % if beam coordinates

                        velChanged(:,ii)=tempTHPR';
                        
                    end % for nEns

                    % Assign results to object
                    obj.u_mps=-1.*velChanged(1,:);
                    obj.v_mps=-1.*velChanged(2,:);
                    obj.w_mps=velChanged(3,:);
                    obj.d_mps=velChanged(4,:);
                    obj.coordSys=newCoordSys;
                    obj.uProcessed_mps=obj.u_mps;
                    obj.vProcessed_mps=obj.v_mps;
                    
                end % if newsys
            end % if coordSys
         end % function changeCoordSys  
         
        function obj=changeMagVar(obj,magVarChng)
        % This function rotates the BT velocities for a change in magnetic
        % variation.
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % magVarChng: change in magnetic variation
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            % Assign bt velocities to local variables
            uP=obj.uProcessed_mps;
            vP=obj.vProcessed_mps;
            u=obj.u_mps;
            v=obj.v_mps;
            
            % Apply magvar change to processed data
            [dir, mag]=cart2pol(uP,vP);
            [uPRotated,vPRotated]=pol2cart(dir-deg2rad(magVarChng),mag);
            
            % Apply magvar change to unprocessed data
            [dir, mag]=cart2pol(u,v);
            [uRotated,vRotated]=pol2cart(dir-deg2rad(magVarChng),mag);
            
            % Update object properties
            obj.uProcessed_mps=uPRotated;
            obj.vProcessed_mps=vPRotated;
            obj.u_mps=uRotated;
            obj.v_mps=vRotated;
        end % changeMagVar         
             
        function obj=changeOffset(obj,offsetChng)
        % This function rotates the BT velocities for a change in heading
        % offset.
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % magVarChng: change in heading offset
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            % Assign bt velocities to local variables
            uP=obj.uProcessed_mps;
            vP=obj.vProcessed_mps;
            u=obj.u_mps;
            v=obj.v_mps;
            
            % Apply magvar change to processed data
            [dir, mag]=cart2pol(uP,vP);
            [uPRotated,vPRotated]=pol2cart(dir-deg2rad(offsetChng),mag);
            
            % Apply magvar change to unprocessed data
            [dir, mag]=cart2pol(u,v);
            [uRotated,vRotated]=pol2cart(dir-deg2rad(offsetChng),mag);
            
            % Update object properties
            obj.uProcessed_mps=uPRotated;
            obj.vProcessed_mps=vPRotated;
            obj.u_mps=uRotated;
            obj.v_mps=vRotated;
        end % changeOffset    
        
        function obj=changeHeadingSource(obj,headingChange)
        % This function rotates the BT velocities for a change in heading
        % offset.
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % headingChng: change in heading due to change in source
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            % Assign bt velocities to local variables
            uP=obj.uProcessed_mps;
            vP=obj.vProcessed_mps;
            u=obj.u_mps;
            v=obj.v_mps;
            
            % Apply magvar change to processed data
            [dir, mag]=cart2pol(uP,vP);
            [uPRotated,vPRotated]=pol2cart(dir-deg2rad(headingChange),mag);
            
            % Apply magvar change to unprocessed data
            [dir, mag]=cart2pol(u,v);
            [uRotated,vRotated]=pol2cart(dir-deg2rad(headingChange),mag);
            
            % Update object properties
            obj.uProcessed_mps=uPRotated;
            obj.vProcessed_mps=vPRotated;
            obj.u_mps=uRotated;
            obj.v_mps=vRotated;
        end % changeHSource   
        
        function obj=applyInterpolation(obj,transect,varargin)
        % Function to apply interpolations to navigation data.
        % 
        % INPUT: 
        %
        % obj: object of clsBoatData
        %
        % transect: object of clsTransectData
        %
        % varargin: specified interpolation method if different from that
        % saved in object
        % 
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            % Reset processed data
            obj.uProcessed_mps=obj.u_mps;
            obj.vProcessed_mps=obj.v_mps;
            obj.uProcessed_mps(~obj.validData(1,:))=nan;
            obj.vProcessed_mps(~obj.validData(1,:))=nan;
 
            % Determine interpolation methods to apply
            interp=obj.interpolate;
            if length(varargin)>0
                if ~isempty(varargin{:})
                    interp=varargin{1};
                end
            end
            
            % Apply specified interpolation method
            obj.interpolate=interp;
            switch interp
                 
                case 'None' % None
                    % Sets invalid data to nan with no interpolation
                    obj=interpolateNone(obj);

                case 'ExpandedT' % Expanded Ensemble Time
                    % Set interpolate to None as the interpolation is done in the
                    % clsQComp
                    obj=interpolateNext(obj);

                case 'Hold9' % SonTek Method
                    % Interpolates using SonTeks method of holding last valid for
                    % up to 9 samples.
                    obj=interpolateHold9(obj);

                case 'HoldLast' % Hold Last Valid
                    % Interpolates by holding last valid indefinitely
                    obj=interpolateHoldLast(obj);

                case 'Linear' % Linear
                    % Interpolates using linear interpolation
                    obj=interpolateLinear(obj,transect);
                    
                 case 'Smooth'
                     % Interpolates using smooth interpolation
                     obj=interpolateSmooth(obj,transect);
                     
                 case 'TRDI'
                     % TRDI interpolation is done in discharge
                     %obj=interpolateNext(obj);
                     % For TRDI the interpolation is done on discharge not
                     % on velocities
                     obj=interpolateNone(obj);
                     
             end % switch
        end % applyInterpolation
        
        function obj=applyFilter(obj,transect,varargin)
        % Function to apply filters to navigation data.
        % 
        % INPUT: 
        %
        % obj: object of clsBoatData
        %
        % transect: object of clsTransectData
        %
        % varargin: specified filter method(s) and associated thresholds,
        % if different from that saved in object. More than one filter can
        % be applied during a single call.
        % 
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            % Determine filter methods to apply
            if ~isempty(varargin)
                nArgs=length(varargin);
                n=1;
                while n<nArgs
                    switch varargin{n}
                        
                        % Filter based on number of valid beams
                        case 'Beam'
                            n=n+1;
                            beamFilterSetting=varargin{n};
                            obj=filterBeam(obj,beamFilterSetting);
                        
                        % Filter based on difference velocity
                        case 'Difference'
                            n=n+1;
                            dFilterSetting=varargin{n};
                            if strcmpi(dFilterSetting,'Manual')
                                n=n+1;
                                obj=filterDiffVel(obj,dFilterSetting,varargin{n});
                            else
                                obj=filterDiffVel(obj,dFilterSetting);
                            end
                            
                        % Filter based on vertical velocity    
                        case 'Vertical'
                            n=n+1;
                            wFilterSetting=varargin{n};
                            if strcmpi(wFilterSetting,'Manual')
                                n=n+1;
                                setting=varargin{n};
                                if isnan(setting)
                                    setting=obj.wFilterThreshold;
                                end
                                 obj=filterVertVel(obj,wFilterSetting,setting);
                            else
                                obj=filterVertVel(obj,wFilterSetting);
                            end   
                        
                        % Filter based on loess smooth    
                        case 'Other'
                            n=n+1;
                            obj=filterSmooth(obj,transect,varargin{n});
                            
                    end % switch
                    n=n+1;
                end % while
                
            else
                
                % Apply all filters based on stored settings
                obj=filterBeam(obj,obj.beamFilter);
                obj=filterDiffVel(obj,obj.dFilter,obj.dFilterThreshold);
                obj=filterVertVel(obj,obj.wFilter,obj.wFilterThreshold);
                obj=filterSmooth(obj,transect,obj.smoothFilter);

            end % if varargin
           
            % Apply previously specified interplation method
            obj=applyInterpolation(obj,transect);
            
        end % applyFilter
        
        function obj=applyGPSFilter(obj,transect,varargin)
        % Applies filters to GPS referenced boat velocity
        % 
        % INPUT: 
        %
        % obj: object of clsBoatData
        %
        % transect: object of clsTransectData
        %
        % varargin: specified filter method(s) and associated thresholds,
        % if different from that saved in object. More than one filter can
        % be applied during a single call.
        % 
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            
            % Determine filter methods to apply
            if ~isempty(varargin)
                nArgs=length(varargin);
                n=1;
                while n<nArgs
                    switch varargin{n}
                        
                        % Filter based on minimum differential correciton
                        % setting
                        case 'Differential'
                            n=n+1;
                            % Use setting for GGA and 1 for VTG
                            if strcmp(obj.navRef,'GGA')
                                setting=varargin{n};
                                obj=filterDiffQual(obj,transect.gps,setting);
                            else
                                obj=filterDiffQual(obj,transect.gps,1);
                            end
                        
                        % Filter based on change in altitude during transect    
                        case 'Altitude'
                            n=n+1;
                            setting=varargin{n};
                            if strcmpi(setting,'Manual')
                                n=n+1;
                                % This filter only applies to GGA data
                                if strcmp(obj.navRef,'GGA')
                                    obj=filterAltitude(obj,transect.gps,setting,varargin{n});
                                end
                            else
                                % This filter only applies to GGA data
                                if strcmp(obj.navRef,'GGA')
                                    obj=filterAltitude(obj,transect.gps,setting);
                                end
                            end
                        
                        % Filter based on change in HDOP during transect    
                        case 'HDOP'
                            n=n+1;
                            setting=varargin{n};
                            if strcmpi(setting,'Manual')
                                n=n+1;
                                max=varargin{n};
                                n=n+1;
                                change=varargin{n};
                                obj=filterHDOP(obj,transect.gps,setting,max,change);
                            else
                                obj=filterHDOP(obj,transect.gps,setting);
                            end   
                        
                        % Filter based on smooth of boat speed    
                        case 'Other'
                            n=n+1;
                            obj=filterSmooth(obj,transect,varargin{n});
                    end
                    n=n+1;
                end
            else
                
                % If not new filter is specified apply all previously
                % specified filters
                obj=filterDiffQual(obj,transect.gps);
                obj=filterAltitude(obj,transect.gps);
                obj=filterHDOP(obj,transect.gps);
                obj=filterSmooth(obj,transect,obj.smoothFilter);
            end
            
            % Apply previously specified interpolation method
            obj=applyInterpolation(obj,transect);
        end
        
        function obj=sosCorrection(obj,transect,ratio)
        % Correct boat velocity for a user specified speed of sound
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % transect: object of clsTransectData
        %
        % ratio: ratio of new to existing speed of sound
        
            % Correct velocities
            obj.u_mps=obj.u_mps.*ratio;
            obj.v_mps=obj.v_mps.*ratio;
            
            % Apply filters to corrected velocities
            obj=applyFilter(obj,transect);
            
        end %sosCorrection
        
        function obj=applyComposite(obj,uComp,vComp,compSource)
        % Apply composite tracks to object
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % uComp: composited u-velocity
        %
        % vComp: composited v-velocity
        %
        % compSource: source of velocity (BT, GGA, VTG, INT, INV)
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            % Apply to clsBoatData object
            obj.uProcessed_mps=uComp;
            obj.vProcessed_mps=vComp;
            obj.processedSource=cell(size(uComp));
            obj.processedSource(compSource==1)={'BT'};
            obj.processedSource(compSource==2)={'GGA'};
            obj.processedSource(compSource==3)={'VTG'};
            obj.processedSource(compSource==0)={'INT'};
            obj.processedSource(compSource==-1)={'INV'};
            
        end % applyComposite
                
        function obj=interpolateComposite(obj,transect)
        % This function interpolates processed data flagged invalid using linear interpolation. 
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % transect: object of clsTransectData
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            u=obj.uProcessed_mps;
            v=obj.vProcessed_mps;

            valid=~isnan(u);
            
            % Check for valid data
            if sum(valid)>1 
                
                % Compute ensTime
                ensTime=nancumsum(transect.dateTime.ensDuration_sec);
                
                % Ensure monotonic input
                diffTime=diff(ensTime(valid));
                idx=find(diffTime==0);
                monoArray=[ensTime(valid);u(valid);v(valid)];
                while ~isempty(idx)
                    monoArray(2,idx(end))=nanmean(monoArray(2,idx(end)-1:idx(end)));
                    monoArray(3,idx(end))=nanmean(monoArray(3,idx(end)-1:idx(end)));
                    monoArray(:,idx(end)+1)=[];
                    idx(end)=[];
                end
                % Apply linear interpolation
                obj.uProcessed_mps = interp1(monoArray(1,:),monoArray(2,:),ensTime);
                obj.vProcessed_mps = interp1(monoArray(1,:),monoArray(3,:),ensTime);
                
                obj.processedSource(~valid)={'INT'};
            end % if valid
            
        end % interpolateComposite
        
        function obj=setProperty(obj,property,setting)
            obj.(property)=setting;
        end

    end % methods public
    
    methods (Access=private)
        
        % Filters
        function obj=filterBeam(obj,setting)
        % The determination of invalid data depends on the whether 
        % 3-beam or 4-beam solutions are acceptable. This function can be
        % applied by specifying 3 or 4 beam solutions are setting
        % obj.beamFilter to -1 which will trigger an automatic mode. The
        % automatic mode will find all 3 beam solutions and then compare
        % the velocity of the 3 beam solutions to nearest 4 beam solution
        % before and after the 3 beam solution. If the 3 beam solution is
        % within 50% of the average of the neighboring 3 beam solutions the
        % data are deemed valid if not invalid. Thus in automatic mode only
        % those data from 3 beam solutions that appear sufficiently
        % than the 4 beam solutions are marked invalid. The process happens
        % for each ensemble. If the number of beams is specified manually
        % it is applied uniformly for the whole transect.
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % setting: setting for beam filter (3, 4, -1)
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            % Set beamFilter property
            obj.beamFilter=setting;
           
            % In manual mode determine number of raw invalid and number of
            % 3 beam solutions if selected
            if obj.beamFilter>0
                
                % Find invalid raw data
                validVel=ones(size(obj.rawVel_mps));
                validVel(isnan(obj.rawVel_mps))=0;

                % Determine how many beams or transformed coordinates are valid
                validVelSum=sum(validVel);
                valid=ones(size(validVelSum));

                % Compare number of valid beams or coordinates to filter value
                valid(validVelSum<obj.beamFilter)=false;

                % Save logical of valid data to object
                obj.validData(6,:)=valid;
               
            else
                
                % Apply automatic filter
                % ----------------------
                
                % Find all 3 beam solutions
                temp=obj;
                temp=filterBeam(temp,4);
                temp3=filterBeam(temp,3);
                valid3beams=temp3.validData(6,:)-temp.validData(6,:);
                nEns=length(temp.validData(6,:));
                %idx=find(temp.validData(6,:)==false);
                idx=find(logical(valid3beams)==true);
                
                % If 3 beam solutions exist evaluate there validity
                if isempty(idx)
                    
                    % If no 3 beam solutions allow only 4 beam solutions
                    obj=temp;

                else
                    
                    % Identify 3 beam solutions that appear to be invalid
                    
                    % Number of 3 beam solutions
                    n3BeamEns=length(idx);
                    
                    % Check each three beam solution for validity
                    for m=1:n3BeamEns  
                        
                        % Check if 3 beam idx is first or last ensemble
                        if idx(m)>1 && idx(m)<nEns
                            
                            % Find nearest 4 beam solutions before and
                            % after 3 beam solution
                            refIdxBefore=find(temp.validData(6,1:idx(m))==true,1,'last');
                            refIdxAfter=idx(m)+find(temp.validData(6,idx(m)+1:end)==true,1,'first');
                            if isempty(refIdxBefore)
                                refIdxBefore=refIdxAfter;
                            end
                            if isempty(refIdxAfter)
                                refIdxAfter=refIdxBefore;
                            end
                            if ~isempty(refIdxBefore) && ~isempty(refIdxAfter)
                                % Compute the ratio of 3-beam velocity
                                % components to neighboring 4-beam components
                                uRatio=(temp.u_mps(idx(m))./((temp.u_mps(refIdxBefore)+temp.u_mps(refIdxAfter))./2))-1;
                                vRatio=(temp.v_mps(idx(m))./((temp.v_mps(refIdxBefore)+temp.v_mps(refIdxAfter))./2))-1;
                            else
                                uRatio=1;
                                vRatio=1;
                            end
                            % If 3-beam differs from 4-beam by more the 50%
                            % mark it invalid
                            if abs(uRatio)>0.5 | abs(vRatio)>0.5
                                temp.validData(6,idx(m))=0;
                            else
                                temp.validData(6,idx(m))=1;
                            end % if ratio
                        end % if idx
                    end % for n3BeamEns
                    
                    obj=temp;
                end % if empty
                
                obj.beamFilter=-1;
            end % if >0
            
            % Combine all filter data to composite valid data
            obj.validData(1,:)=all(obj.validData(2:end,:));
            obj.numInvalid=sum(~obj.validData(1,:));
            
        end % filterBeam
         
        function obj=filterDiffVel(obj,setting,varargin)
        % Applies either manual or automatic filtering of the difference
        % (error) velocity. The automatic mode is based on the following:
        %  This filter is based on the assumption that the water error velocity
        %  should follow a gaussian distribution. Therefore, 5 iqr
        %  should encompass all of the valid data. The standard deviation and
        %  limits (multiplier*standard deviation) are computed in an iterative 
        %  process until filtering out additional data does not change the computed 
        %  standard deviation. 
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % setting: difference velocity setting (Off, Manual, Auto)
        %
        % varargin: if manual, the user specified threshold
        %
        % OUPUT:
        % 
        % obj: object of clsBoatData
        
            % Set difference filter properties
            obj.dFilter=setting;
            if ~isempty(varargin)
                obj.dFilterThreshold=varargin{1};
            end
            
            % Set multiplier
            multiplier=5;
            minimumWindow=0.01;
            
            % Get difference data from object
            dVel=obj.d_mps;
            
            % Apply selected method
            switch obj.dFilter
                case 'Manual'
                    dVelMaxRef=abs(obj.dFilterThreshold);
                    dVelMinRef=-1.*dVelMaxRef;
                case 'Off'
                    dVelMaxRef=nanmax(dVel)+99;
                    dVelMinRef=nanmin(dVel)-99;
                case 'Auto'
                    % Initialize variables
                    dVelFiltered=dVel(1:end); 
                    
                    % Fix to zeros in SonTek M9 data
%                     dVelFiltered(dVelFiltered==0)=nan;
                    
                    % Initialize variables
                    stdDiff=1;
                    k=1;
                    
                    % Loop until no additional data are removed
                    while stdDiff(k)~=0 && k<1000 && ~isnan(stdDiff(k))
                        k=k+1;
                        
                        % Compute standard deviation
                        dVelStd=iqr(dVelFiltered);%nanstd(dVelFiltered);
                        thresholdWindow=multiplier*dVelStd;
                        if thresholdWindow<minimumWindow
                            thresholdWindow=minimumWindow;
                        end
                        
                        % Compute maximum and minimum thresholds
                        dVelMaxRef=nanmedian(dVelFiltered)+thresholdWindow;
                        dVelMinRef=nanmedian(dVelFiltered)-thresholdWindow;
                        
                        % Identify valid and invalid data
                        dVelBadIdx=find(dVelFiltered > dVelMaxRef |...
                                            dVelFiltered < dVelMinRef);
                        dVelGoodIdx=find(dVelFiltered <= dVelMaxRef &...
                                            dVelFiltered >= dVelMinRef);
                                        
                        % Update filtered data array
                        dVelFiltered=dVelFiltered(dVelGoodIdx);
                        
                        % Determine differences due to last filter
                        % interation
                        dVelStd2=iqr(dVelFiltered);%nanstd(dVelFiltered);    
                        stdDiff(k)=dVelStd2-dVelStd;
                        numBinsFiltered(k)=size(dVelBadIdx,2);
                        
                    end % while
                    
            end % switch
                               
            %  Set valid data row 3 for difference velocity filter results
            obj.validData(3,:)=false;
            obj.validData(3,(dVel <= dVelMaxRef & dVel >= dVelMinRef))=true; 
            obj.validData(3,~obj.validData(2,:))=true;
            obj.validData(3,isnan(obj.d_mps))=true;
            obj.dFilterThreshold=dVelMaxRef;
            
            % Combine all filter data to composite valid data
            obj.validData(1,:)=all(obj.validData(2:end,:));
            obj.numInvalid=sum(~obj.validData(1,:));
            
        end % applyDiffVelFilter
        
        function obj=filterVertVel(obj,setting,varargin)
        % Applies either manual or automatic filtering of the vertical
        % velocity. Uses same assumptions as difference filter.
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % setting: filter setting (Off, Manual, Auto)
        %
        % varargin: if setting is manual, the user specified threshold
        % 
        % OUTPUT:
        % 
        % obj: object of clsBoatData
            
            % Set vertical velocity filter properties
            obj.wFilter=setting;
            if ~isempty(varargin)
                obj.wFilterThreshold=varargin{1};
            end
            
            % Set multiplier
            multiplier=5;
            minimumWindow=0.01;
            
            % Get vertical velocity data from object
            wVel=obj.w_mps;
            
            % Apply selected method
            switch obj.wFilter
                
                case 'Manual'
                    wVelMaxRef=abs(obj.wFilterThreshold);
                    wVelMinRef=-1.*wVelMaxRef;
                    
                case 'Off'
                    wVelMaxRef=nanmax(wVel)+1;
                    wVelMinRef=nanmin(wVel)-1;
                    
                case 'Auto'
                    % Initialize variables
                    wVelFiltered=wVel(1:end); 
                    stdDiff=1;
                    i=0;
                    
                    % Loop until no additional data are removed
                    while stdDiff~=0 && i<1000 && ~isnan(stdDiff)
                        i=i+1;
                        
                        % Compute inner quartile range
                        wVelStd=iqr(wVelFiltered);

                        % Compute standard deviation
                        thresholdWindow=multiplier*wVelStd;
                        if thresholdWindow<minimumWindow
                            thresholdWindow=minimumWindow;
                        end
                        
                        % Compute maximum and minimum thresholds
                        wVelMaxRef=nanmedian(wVelFiltered)+thresholdWindow;
                        wVelMinRef=nanmedian(wVelFiltered)-thresholdWindow;
                        
                        % Identify valid and invalid data
                        wVelBadIdx=find(wVelFiltered > wVelMaxRef |...
                                            wVelFiltered < wVelMinRef);
                        wVelGoodIdx=find(wVelFiltered <= wVelMaxRef &...
                                            wVelFiltered >= wVelMinRef);
                                        
                        % Update filtered data array
                        wVelFiltered=wVelFiltered(wVelGoodIdx);
                        
                        % Determine differences due to last filter
                        % interation 
                        wVelStd2=nanstd(wVelFiltered);    
                        stdDiff=wVelStd2-wVelStd;
                        numBinsFiltered(i)=size(wVelBadIdx,2);
                        
                    end % while
                    
            end % switch
                               
            %  Set valid data row 4 for difference velocity filter results
            obj.validData(4,:)=false;
            obj.validData(4,(wVel <= wVelMaxRef & wVel >= wVelMinRef))=true; 
            obj.validData(4,~obj.validData(2,:))=true;
            obj.wFilterThreshold=wVelMaxRef;
            
            % Combine all filter data to composite valid data
            obj.validData(1,:)=all(obj.validData(2:end,:));
            obj.numInvalid=sum(~obj.validData(1,:));
            
        end % applyVertVelFilter

        function obj=filterSmooth(obj,transect,setting)
        %  This filter employs a running trimmed standard deviation filter to
        %  identify and mark spikes in the boat speed. First a robust Loess 
        %  smooth is fitted to the boat speed time series and residuals between
        %  the raw data and the smoothed line are computed. The trimmed standard
        %  deviation is computed by selecting the number of residuals specified by
        %  "halfwidth" before the target point and after the target point, but not
        %  including the target point. These values are then sorted, and the points
        %  with the highest and lowest values are removed from the subset, and the 
        %  standard deviation of the trimmed subset is computed. The filter
        %  criteria are determined by multiplying the standard deviation by a user
        %  specified multiplier. This criteria defines a maximum and minimum
        %  acceptable residual. Data falling outside the criteria are set to nan.
        %  
        %  Recommended filter setting are:
        %   filterWidth=10;
        %   halfWidth=10;
        %   multiplier=9;
        %
        %   David S. Mueller, USGS, OSW
        %   9/8/2005
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % transect: object of clsTransectData
        %
        % setting: filter setting (On, Off)
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            % Set property
            obj.smoothFilter=setting;
            
            % Compute ensTime
            ensTime=nancumsum(transect.dateTime.ensDuration_sec);
        
            % Determine if smooth filter should be applied
            if strcmpi(obj.smoothFilter,'On')
                
                % Boat velocity components
                bVele=obj.u_mps;
                bVeln=obj.v_mps;

                % Set filter parameters
                filterWidth=10;
                halfWidth=10;
                multiplier=9;
                cycles=3;

                % Inialize variables
                dir=nan(size(bVele));
                speed=nan(size(bVele));
                speedSmooth=nan(size(bVele));
                speedRes=nan(size(bVele));
                speedFiltered=nan(size(bVele));
                bVeleFiltered=bVele;
                bVelnFiltered=bVeln;
                btBad=nan(size(bVele));

                % Compute speed and direciton of boat
                [dir, speed]=cart2pol(bVele,bVeln);
                dir=90-(dir.*180/pi);

                % Compute residuals from a robust Loess smooth
                speedSmooth=smooth(ensTime,speed,filterWidth,'rloess')';
                speedRes=speed-speedSmooth;

                % Apply a trimed standard deviation filter multiple times
                speedFiltered=speed;
                for i=1:cycles
                    [filArray]=clsBoatData.runStdTrim(halfWidth,speedRes');

                    % Compute filter bounds
                    upperLimit=speedSmooth+multiplier.*filArray;
                    lowerLimit=speedSmooth-multiplier.*filArray;

                    % Apply filter to residuals
                    btBad_idx=find(speed>upperLimit | speed<lowerLimit);
                    speedRes(btBad_idx)=nan;
                end
                
                % Update validData property
                obj.validData(5,:)=true;
                obj.validData(5,btBad_idx)=false;
                obj.validData(5,~obj.validData(2,:))=true;
                obj.smoothUpperLimit=upperLimit;
                obj.smoothLowerLimit=lowerLimit;
                obj.smoothSpeed=speedSmooth;
                
            else
                
            % Not filter applied all data assumed valid
                obj.validData(5,:)=true;
                obj.smoothUpperLimit=nan;
                obj.smoothLowerLimit=nan;
                obj.smoothSpeed=nan;
            end % if smoothFilter
            
            % Combine all filter data to composite valid data
            obj.validData(1,:)=all(obj.validData(2:end,:));
            obj.numInvalid=sum(~obj.validData(1,:));
            
        end % applySmoothFilter
        
        function obj=filterDiffQual(obj,gpsData,varargin)
        % Filters GPS data based on the minimum acceptable differential
        % correction quality.
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % gpsData: object of clsGPSData
        %
        % varargin: new setting for filter
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            % New filter setting, if provided
            if ~isempty(varargin)
                obj.gpsDiffQualFilter=varargin{1};
            end
            
            % Reset validData property
            obj.validData(3,:)=true;
            obj.validData(6,:)=true;
            
            % Determine and apply appropriate filter type
            obj.validData(3,isnan(gpsData.diffQualEns))=false;
            if ~isempty(obj.gpsDiffQualFilter)
                switch obj.gpsDiffQualFilter

                    case 1 % autonomous
                        obj.validData(3,gpsData.diffQualEns<1)=false;

                    case 2 % differential correction
                        obj.validData(3,gpsData.diffQualEns<2)=false;

                    case 4 % RTK
                        obj.validData(3,gpsData.diffQualEns<4)=false;

                end % switch
                % If there is no indication of the quality assume 1 for vtg
                if strcmp(obj.navRef,'VTG')
                    obj.validData(3,isnan(gpsData.diffQualEns))=true;
                end
            end
            
            % Combine all filter data to composite valid data
            obj.validData(1,:)=all(obj.validData(2:end,:));
            obj.numInvalid=sum(~obj.validData(1,:));
            
        end % filterDiffQual
        
        function obj=filterAltitude(obj,gpsData,varargin)
        % Filter GPS data based on a change in altitude. Assuming the data
        % are collected on the river the altitude should not change
        % substantially during the transect. Since vertical resolution is
        % about 3 x worse that horizontal resolution the automatic filter
        % threshold is set to 3 m, which should ensure submeter horizontal 
        % accuracy.
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % gpsData: object of clsGPSData
        %
        % varargin: 
        %   varargin{1}: new setting for filter (Off, Manual, Auto)
        %   varargin{2}: change threshold
        
            % New filter settings if provided
            if ~isempty(varargin)
                obj.gpsAltitudeFilter=varargin{1};
                if length(varargin)>1
                    obj.gpsAltitudeFilterChange=varargin{2};
                end % if
            end % if
            
            % Set threshold for Auto
            if strcmp(obj.gpsAltitudeFilter,'Auto')
                obj.gpsAltitudeFilterChange=3;
            end % if
            
            % Set all data to valid
            obj.validData(4,:)=true;
            obj.validData(6,:)=true;
            
            % Manual or Auto is selected, apply filter
            if ~strcmp(obj.gpsAltitudeFilter,'Off')
               
               % Initialize variables
               numValidOld=sum(obj.validData(4,:));
               k=0;
               change=1;
               
               % Loop until no change in the number of valid ensembles
               while k<100 && change>0.1
                   
                   % Compute mean using valid ensembles
                   altMean=nanmean(gpsData.altitudeEns_m(obj.validData(2,:)));
                   
                   % Compute difference for each ensemble
                   diff=abs(gpsData.altitudeEns_m-altMean);
                   
                   % Mark invalid those ensembles with differences greater
                   % that the change threshold
                   obj.validData(4,diff>obj.gpsAltitudeFilterChange)=false;
                   k=k+1;
                   numValid=sum(obj.validData(4,:));
                   change=numValidOld-numValid;
                   
               end % while
               
            end % if
            
            % Combine all filter data to composite valid data
            obj.validData(1,:)=all(obj.validData(2:end,:));
            obj.numInvalid=sum(~obj.validData(1,:));
            
        end % filterAltitude
            
        function obj=filterHDOP(obj,gpsData,varargin)
        % Filter GPS data based on both a maximum HDOP and a change in HDOP
        % over the transect.
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % gpsData: object of clsGPSData
        %
        % varargin:
        %   varargin{1}: filter setting (On, Off, Auto)
        %   varargin{2}: maximum threshold
        %   varargin{3}: change threshold
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        if isempty(gpsData.hdopEns)
            obj.validData(6,1:size(obj.validData,2))=true;
        else
            % New settings, if provided
            if ~isempty(varargin)
                obj.gpsHDOPFilter=varargin{1};
                if length(varargin)>1
                    obj.gpsHDOPFilterMax=varargin{2};
                    obj.gpsHDOPFilterChange=varargin{3};
                end % if
            end % if
            
            % Settings for Auto mode
            if strcmp(obj.gpsHDOPFilter,'Auto')
                obj.gpsHDOPFilterChange=3;
                obj.gpsHDOPFilterMax=4;
            end % if
            
            % Set all ensembles to valid
            obj.validData(6,:)=true;
            
            % Apply filter for Manual or Auto
            if ~strcmp(obj.gpsHDOPFilter,'Off')
                
               % Initialize variables
               numValidOld=sum(obj.validData(6,:));
               k=0;
               change=1;
               
               % Apply max filter
               obj.validData(6,gpsData.hdopEns>obj.gpsHDOPFilterMax)=false;
               
               % Loop until the number of valid ensembles does not change
               while k<100 && change>0.1
                   
                   % Compute mean HDOP for all valid ensembles
                   hdopMean=nanmean(gpsData.hdopEns(obj.validData(6,:)));
                   
                   % Compute the difference in HDOP and the mean for all
                   % ensembles.
                   diff=abs(gpsData.hdopEns-hdopMean);
                   
                   % If the change is HDOP or the value of HDOP is greater
                   % than the threshold setting mark the data invalid
                   obj.validData(6,diff>obj.gpsHDOPFilterChange)=false;

                   k=k+1;
                   numValid=sum(obj.validData(6,:));
                   change=numValidOld-numValid;
                   numValidOld=numValid;
               end % while
               
            end % if
            
            % Combine all filter data to composite valid data
            obj.validData(1,:)=all(obj.validData(2:end,:));
            obj.numInvalid=sum(~obj.validData(1,:));
        end % isempty
        end % filterHDOP       
            
        % Interpolations
        % --------------
        
        function obj=interpolateHold9(obj)
        % This function applies SonTek's approach to maintaining the last 
        % valid boat speed for up to nine invalid samples.
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            % Initialize variables
            nEnsembles=size(obj.u_mps,2);
            nInvalid=0;
            
            % Get data from object
            obj.uProcessed_mps=obj.u_mps;
            obj.vProcessed_mps=obj.v_mps;
            obj.uProcessed_mps(~obj.validData(1,:))=nan;
            obj.vProcessed_mps(~obj.validData(1,:))=nan;
            
            % Process data by ensembles
            for n=2:nEnsembles
                % Check if ensemble is invalid and number of consectutive
                % invalids is less than 9
                if ~obj.validData(1,n) && nInvalid<9
                    obj.uProcessed_mps(n)=obj.uProcessed_mps(n-1);
                    obj.vProcessed_mps(n)=obj.vProcessed_mps(n-1);
                    nInvalid=nInvalid+1;
                else
                    nInvalid=0;
                end % if
            end % for n
            
        end % interpolateSonTekMethod
        
        function obj=interpolateNone(obj)
        % This function removes any interpolation from the data and sets
        % all filtered data to nan;
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            % Reset processed data, to remove interpolations
            obj.uProcessed_mps=obj.u_mps;
            obj.vProcessed_mps=obj.v_mps;
            obj.uProcessed_mps(~obj.validData(1,:))=nan;
            obj.vProcessed_mps(~obj.validData(1,:))=nan;
        end % interpolateNone
        
        function obj=interpolateHoldLast(obj)
        % This function holds the last valid value until the next valid
        % data point.
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % OUTPUT:
        %
        % obj: object of clBoatData
        
            % Get number of ensembles
            nEnsembles=size(obj.u_mps,2);
            
            % Get data from object
            obj.uProcessed_mps=obj.u_mps;
            obj.vProcessed_mps=obj.v_mps;
            obj.uProcessed_mps(~obj.validData(1,:))=nan;
            obj.vProcessed_mps(~obj.validData(1,:))=nan;
            
            % Process data by ensemble
            for n=2:nEnsembles
                if ~obj.validData(1,n) 
                    obj.uProcessed_mps(n)=obj.uProcessed_mps(n-1);
                    obj.vProcessed_mps(n)=obj.vProcessed_mps(n-1);
                end % if
            end % for n
            
        end % interpolateHoldLast
        
        function obj=interpolateNext(obj)
        % This function uses the next valid data to back fill for invalid
        % data.
        %
        % INPUT:
        % 
        % obj: object of clsBoatData
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            % Get valid ensembles
            validEns=obj.validData(1,:);
            
            % Process ensembles
            nEns=length(validEns);
            for n=nEns-1:-1:1
                if validEns(n)==false
                    obj.uProcessed_mps(n)=obj.uProcessed_mps(n+1);
                    obj.vProcessed_mps(n)=obj.vProcessed_mps(n+1);
                end % if validEns
            end % for n
            
        end % interpolateNext
        
        function obj=interpolateSmooth(obj,transect)
        % This function interpolates data flagged invalid using the smooth
        % function.
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % transect: object of clsTransectData
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            % Get data from object
            u=obj.u_mps;
            v=obj.v_mps;
            u(~obj.validData(1,:))=nan;
            v(~obj.validData(1,:))=nan;
            
            % Compute ensTime
            ensTime=nancumsum(transect.dateTime.ensDuration_sec);
            
            % Apply smooth to each component
            uSmooth=smooth(ensTime,u,10,'rloess')';
            vSmooth=smooth(ensTime,v,10,'rloess')';
            
            % Save data in object
            obj.uProcessed_mps=u;
            obj.vProcessed_mps=v;
            obj.uProcessed_mps(isnan(u))=uSmooth(isnan(u));
            obj.vProcessed_mps(isnan(v))=vSmooth(isnan(v));
            
        end % interpolateSmooth
        
        function obj=interpolateLinear(obj,transect)
        % This function interpolates data flagged invalid using linear interpolation. 
        %
        % INPUT:
        %
        % obj: object of clsBoatData
        %
        % transect: object of clsTransectData
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
            % Get data from object
            u=obj.u_mps;
            v=obj.v_mps; 

            valid=~isnan(u);
            
            % Check for valid data
            if sum(valid)>1 && sum(obj.validData(1,:))>1
                
                % Compute ensTime
                ensTime=nancumsum(transect.dateTime.ensDuration_sec);
                
                % Apply linear interpolation
                obj.uProcessed_mps = interp1(ensTime(obj.validData(1,:)),u(obj.validData(1,:)),ensTime);
                obj.vProcessed_mps = interp1(ensTime(obj.validData(1,:)),v(obj.validData(1,:)),ensTime);
            end % if valid
            
        end % interpolateLinear
    
    end % methods private
        
    methods (Static)

        function velOut=filterSonTek(velIn)
        % Determines invalid raw BT samples for SonTek data
        %
        % INPUT:
        %
        % velIn: array of velocity data
        %
        % OUTPUT:
        %
        % velOut: array of velocity data with invalid ensembles set to nan
        
            % Identify all samples where the velocity did not change
            test1=abs(diff(velIn,1,2))<0.00001;
            
            % Identify all samples with all zero values
            test2=nansum(abs(velIn),1)<0.00001;
            test2=test2(2:end).*4;
            
            % Combine criteria
            testSum=sum(test1,1)+test2;
            
            % Develop logical vector of invalid ensembles
            invalidLogical=false(size(testSum));
            invalidLogical(testSum>3)=true;
            invalidLogical=[false, invalidLogical]; 
            if nansum(velIn(:,1))==0
                invalidLogical(:,1)=true;
            end
            
            % Set invalid ensembles to nan
            velOut=velIn;
            velOut(:,invalidLogical)=nan;
        end % filterSonTek
        
        function [filArray]=runStdTrim(halfWidth,mydata)
        %  The routine accepts a column vector as input. "halfWidth" number of data
        %  points for computing the standard deviation are selected before and
        %  after the target data point, but not including the target data point.
        %  Near the ends of the series the number of points before or after are
        %  reduced. nan in the data are counted as points. The selected subset of
        %  points are sorted and the points with the highest and lowest values are
        %  removed from the subset and the standard deviation computed on the
        %  remaining points in the subset. The process occurs for each point in the
        %  provided column vector. A column vector with the computed standard
        %  deviation at each point is returned.
        %
        %   David S. Mueller, USGS, OSW
        %   9/8/2005
        %
        % INPUT:
        %
        % halfwidth: number of ensembles on each side of target ensemble to
        % use for computing trimmed standard deviation
        %
        % mydata: data to be processed
        %
        % OUTPUT:
        %
        % filArray: column vector with computed standard deviations

            % Determine number of points to process
            nPts=size(mydata,1);
            if nPts<20
                halfWidth=floor(nPts/2);
            end
            % Compute standard deviation for each point
            for i=1:nPts

                % Sample selection for 1st point    
                if i==1
                    sample=mydata(2:1+halfWidth);
                    
                % Sample selection a end of data set        
                elseif i+halfWidth>nPts
                    sample=[mydata(i-halfWidth:i-1) ; mydata(i+1:nPts)];
                    
                % Sample selection at beginning of data set        
                elseif halfWidth>=i
                    sample=[mydata(1:i-1) ;mydata(i+1:i+halfWidth)];

                % Sample selection in body of data set
                else
                    sample=[mydata(i-halfWidth:i-1); mydata(i+1:i+halfWidth)];
                end

                % Sort and compute trimmed standard deviation
                sample=sort(sample);
                filArray(i)=nanstd(sample(2:size(sample,1)-1));
            end % for i
            
        end % runStdTrim
        
        function obj=reCreate(obj,structIn,type)
        % Creates object from structured data of class properties
            % Create object
            obj.(type)=clsBoatData();
            % Get variable names from structure
            names=fieldnames(structIn);
            % Set properties to structure values
            nMax=length(names);
            for n=1:nMax
                obj.(type)=setProperty(obj.(type),names{n},structIn.(names{n}));
            end
        end % reCreate
       
    end % methods static
    
end % clsBoatData

