classdef clsSensorStructure
% Interface class for data in clsSensors
    
    properties
        selected % the selected sensor reference name ('internal', 'external', 'user')
        internal % contains the data from the internal sensor
        external % contains the data from an external sensor
        user % contains user supplied value
    end % properties
    
    methods
        function obj=clsSensorStructure ()
        % Create empty structure
        end % Constructor
        
        function obj=setSelected(obj,selectedName)
        % Set the selected source for the specified object
            obj.selected=selectedName;
        end % setSelected      
    end % methods
    
    methods (Static)
        function obj=reCreate(obj,structIn,type)
        % Creates object from structured data of class properties
            % Create object
            obj.(type)=clsSensorStructure();
            % Get variable names from structure
            names=fieldnames(structIn);
            % Set properties to structure values
            nMax=length(names);
            for n=1:nMax
                if isfield(structIn,names{n})
                    if strcmp(type,'heading_deg')
                        obj.(type)=clsHeadingData.reCreate(obj.(type),structIn.(names{n}),names{n});
                    elseif strcmp(type,'salinity_ppt')
                        if ~isstruct(structIn.internal)
                            obj.(type)=clsSensorData.reCreate(obj.(type),structIn.user,'internal');
                            obj.(type)=setSelected(obj.(type),'internal');
                            obj=addSensorData(obj,'salinity_ppt','user',0,'default');
                         else
                            % Create object
                            obj.(type)=clsSensorData.reCreate(obj.(type),structIn.(names{n}),names{n});
                        end
                    else
                        % Create object
                        obj.(type)=clsSensorData.reCreate(obj.(type),structIn.(names{n}),names{n});
                    end
                else
                    % Assign property
                    obj.(type).(names{n})=structIn.(names{n});
                end
            end % for
        end % reCreate 
    end % static methods
end % class

