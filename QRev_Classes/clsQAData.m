classdef clsQAData
% clsQAData  Class for storing QA data such as compass and system tests. Also serves 
% as the container for all methods of evaluating the quality of the measurement.     
    properties (SetAccess = private)
        qRunThresholdCaution % Caution threshold for interpolated discharge for a run of invalid ensembles, in percent.
        qRunThresholdWarning % Warning threshold for interpolated discharge for a run of invalid ensembles, in percent.
        qTotalThresholdWarning % qTotalThresholdWarning: Warning threshold for total interpolated discharge for invalid ensembles, in percent.
        qTotalThresholdCaution
%         ensThresholdCaution %
        transects % Data structure for quality assurance checks of transects.
        systemTest % Data structure for quality assurance checks of system tests.
        compass % Data structure for quality assurance checks of compass tests and evaluations.
        temperature % Data structure for quality assurance checks of temperature comparisons and change.
        movingbed % Data structure for quality assurance checks of moving-bed tests and conditions.
        user % Data structure for quality assurance checks of user input data.
        depths % Data structure for quality assurance checks of depth data.
        btVel % Data structure for quality assurance checks of bottom track velocities.
        ggaVel % Data structure for quality assurance checks of GGA boat velocities.
        vtgVel % Data structure for quality assurance checks of VTG boat velocities.
        wVel % Data structure for quality assurance checks of water track velocities.
        extrapolation % Data structure for quality assurance checks of extrapolations.
        edges % Data structure for quality assurance checks of edge discharge estimates.
        

    end % properties

    methods
        function obj=clsQAData(meas)
        % Constructor method
            if nargin > 0
                    % Thresholds
                    obj.qRunThresholdCaution=3;
                    obj.qRunThresholdWarning=5;
                    obj.qTotalThresholdWarning=25;
                    obj.qTotalThresholdCaution=10;

                    obj=transectsQA(obj,meas);
                    obj=systemTestQA(obj,meas);
                    obj=compassQA(obj,meas);
                    obj=temperatureQA(obj,meas);
                    obj=movingbedQA(obj,meas);
                    obj=userQA(obj,meas);
                    obj=depthsQA(obj,meas);
                    obj=boatQA(obj,meas);
                    obj=waterQA(obj,meas);
                    obj=extrapolationQA(obj,meas);
                    obj=edgesQA(obj,meas);
            end % nargin
        end % constructor
        
        function obj=transectsQA(obj,meas)
        % Apply quality checks to transects
        %
        % INPUT:
        %
        % obj: object of clsQAData
        %
        % meas: object of clsMeasurement
        %
        % OUTPUT:
        %
        % obj: object of clsQAData
    
            % Assume good results
            obj.transects.status='good';

            % Initialize messages
            nmessage=0;
            obj.transects.messages={};
            obj.transects.recip=0;
            obj.transects.sign=0;
            obj.transects.duration=0;
            obj.transects.number=0;
            obj.transects.uncertainty=0;

            checked=logical([meas.transects.checked]);
            nTransects=length(meas.transects);
            
            %%
            numChecked=sum(checked);
            if numChecked>=1
                % Check for duration
                dateTime=[meas.transects(checked).dateTime];
                totalDuration=nansum([dateTime.transectDuration_sec]);
            else
                totalDuration=0;
            end
            
            obj.transects.duration=0;
            if totalDuration<720
                obj.transects.status='caution';
                nmessage=nmessage+1;
                obj.transects.messages(nmessage,:)= {'Transects: Duration of selected transects is less than 720 seconds;' 2 0};
                obj.transects.duration=1;
            end
            
            k=0;
            for n=1:nTransects
                if checked(n)
                    k=k+1;
                    if strcmp(meas.transects(n).adcp.manufacturer,'SonTek')
                        idxMissing=find(dateTime(k).ensDuration_sec > 1.5);
                        avgDuration = (nansum(dateTime(k).ensDuration_sec)-nansum(dateTime(k).ensDuration_sec(idxMissing)))...
                                        ./(length(dateTime(k).ensDuration_sec)-length(idxMissing));
                        if ~isempty(idxMissing)
                            numMissing=round((sum(dateTime(k).ensDuration_sec(idxMissing))./avgDuration)-length(idxMissing));
                        else
                            numMissing=0;
                        end
                    else
                        idxMissing=find(isnan(dateTime(k).ensDuration_sec));
                        numMissing=length(idxMissing)-1;
                    end
                    if numMissing>0
                       nmessage=nmessage+1;
                       obj.transects.messages(nmessage,:)= {['Transects: ',meas.transects(n).fileName,' is missing ',num2str(numMissing),' ensembles'],2,0};
                       obj.transects.status='caution';
                    end
                end
            end

            switch numChecked
                case 0
                    obj.transects.status='warning';
                    nmessage=nmessage+1;
                    obj.transects.messages(nmessage,:)= {'TRANSECTS: No transects selected;' 1 0};  
                    obj.transects.number=2;

                case 1
                    obj.transects.status='caution';
                    nmessage=nmessage+1;
                    obj.transects.messages(nmessage, :)= {'Transects: Only one transect selected;' 2 0};
                    obj.transects.number=1;

                otherwise
                    obj.transects.number=numChecked;
                    if numChecked==2
                        % Determine transects used to compute discharge
                        checked=logical([meas.transects(:).checked]);

                        % Assign automatically generated uncertainties to properties
                        [cov, ~]=clsUncertainty.ranUncertQ(meas.discharge(checked),'total');
                        if cov > 2
                            obj.transects.status='caution';
                            nmessage=nmessage+1;
                            obj.transects.messages(nmessage,:)= {'Transects: Uncertainty would be reduced by additional transects;' 2 0};
                            obj.transects.uncertainty=1;
                        end
                    end

                    % Check for consistent sign
                    if sum(checked)>=1
                        signCheck=unique(sign([meas.discharge(checked).total]));
                        obj.transects.sign=0;
                        if signCheck>1
                            obj.transects.status='warning';
                            nmessage=nmessage+1;
                            obj.transects.messages(nmessage,:)= {'TRANSECTS: Sign of total Q is not consistent. One or more start banks may be incorrect;' 1 0};
                            obj.transects.sign=2;
                        end


                        % Check for reciprocal transects
                        startEdge={meas.transects(checked).startEdge};
                        numLeft=sum(strcmp(startEdge,'Left'));
                        numRight=sum(strcmp(startEdge,'Right'));
                        obj.transects.recip=0;
                        if numLeft~=numRight
                            obj.transects.status='warning';
                            nmessage=nmessage+1;
                            obj.transects.messages(nmessage,:)= {'TRANSECTS: Transects selected are not reciprocal transects;' 1 0};
                            obj.transects.recip=2;
                        end
                    end % if checked
            end % switch      
            
            % Check for zero discharge transects
            zeroQ=any([meas.discharge(checked).total]==0);
            if zeroQ
                obj.transects.status='warning';
                nmessage=nmessage+1;
                obj.transects.messages(nmessage,:)= {'TRANSECTS: One or more transects have zero Q;' 1 0};
                obj.transects.number=-1;
            end
        end % transectsQA
        
        function obj=systemTestQA(obj,meas)
        % Apply quality checks to system tests
        %
        % INPUT:
        %
        % obj: object of clsQAData
        %
        % meas: object of clsMeasurement
        %
        % OUTPUT:
        %
        % obj: object of clsQAData

            % Initialize message counter
            nmessage=0;
            obj.systemTest.messages={};
            obj.systemTest.status='good';

            % Determine if a system test is present
            if isempty(meas.sysTest(1).timeStamp)
                % No system test present
                obj.systemTest.status='warning';
                nmessage=nmessage+1;
                obj.systemTest.messages(nmessage,:)= {'SYSTEM TEST: No system test;' 1 3};
                
            else
%                 lag_compare = [];
                pt3Fail=false;
                nTests=length(meas.sysTest);
                for n=1:nTests
                    %pt3 check
                    %if pt3 exists
                    if isfield(meas.sysTest(n).result, 'pt3')
                        
                        %assess hard limit tests
                        if isfield(meas.sysTest(n).result.pt3, 'hardLimit')

                            pt3_hl = meas.sysTest(n).result.pt3.hardLimit;

                            %Run two tests on hardlimit, high gain, wide bandwidth:
                            % 50% test for lags 3-7
                            % 25% test for lag 7
                            % a sum of 2 tests must fail for pt3Fail to be
                            % true
                            if isfield(pt3_hl, 'hw')
                                corr_table = pt3_hl.hw.corrTable;
                                init_vals = corr_table(1,:) * 0.5;
                                lagCheck=init_vals(1)' < corr_table(4:8,:)';
%                                 byBeamCheck=sum(lagCheck,2)>3;
%                                 allBeamCheck=sum(lagCheck,1)>3;
%                                 if sum(byBeamCheck)>0 | sum(allBeamCheck)>0
                                lag7Check=corr_table(1,:).*0.25 < corr_table(8,:);
                                if sum(sum(lagCheck))+sum(lag7Check) > 1
                                    pt3Fail=true;
                                end
%                                 lag_compare = [lag_compare lagCheck];
                            end
%                             %if lw is a field run .15 test for lags 3-7
%                             if isfield(pt3_hl, 'lw')
%                                 corr_table = pt3_hl.lw.corrTable;
%                                 init_vals = corr_table(1,:) * .15;
%                                 lagCheck=init_vals(1)' < corr_table(4:8,:)';
%                                 byBeamCheck=sum(lagCheck,2)>3;
%                                 allBeamCheck=sum(lagCheck,1)>3;
%                                 if sum(byBeamCheck)>0 | sum(allBeamCheck)>0
%                                     pt3Fail=true;
%                                 end
%                                 lag_compare = [lag_compare lagCheck];
%                             end
%                             %if hn is a field run .15 test for lags 3-7
%                             if isfield(pt3_hl, 'hn')
%                                 corr_table = pt3_hl.hn.corrTable;
%                                 init_vals = corr_table(1,:) * .15;
%                                 lagCheck=init_vals(1)' < corr_table(4:8,:)';
%                                 byBeamCheck=sum(lagCheck,2)>3;
%                                 allBeamCheck=sum(lagCheck,1)>3;
%                                 if sum(byBeamCheck)>0 | sum(allBeamCheck)>0
%                                     pt3Fail=true;
%                                 end
%                                 lag_compare = [lag_compare lagCheck];
%                             end
%                             %if ln is a field run .15 test for lags 3-7
%                             if isfield(pt3_hl, 'ln')
%                                 corr_table = pt3_hl.hw.corrTable;
%                                 init_vals = corr_table(1,:) * .15;
%                                 lagCheck=init_vals(1)' < corr_table(4:8,:)';
%                                 byBeamCheck=sum(lagCheck,2)>3;
%                                 allBeamCheck=sum(lagCheck,1)>3;
%                                 if sum(byBeamCheck)>0 | sum(allBeamCheck)>0
%                                     pt3Fail=true;
%                                 end
%                                 lag_compare = [lag_compare lagCheck];
%                             end
                        end %if hard limit is a field
%                         if isfield(meas.sysTest(n).result.pt3, 'linear')
% 
%                             pt3_linear = meas.sysTest(n).result.pt3.linear;
% 
%                             %if hw is a field run .15 test for lags 3-7
%                             if isfield(pt3_linear, 'hw')
%                                 corr_table = pt3_linear.hw.corrTable;
%                                 init_vals = corr_table(1,:) * .15;
%                                 lagCheck=init_vals(1)' < corr_table(4:8,:)';
%                                 byBeamCheck=sum(lagCheck,2)>3;
%                                 allBeamCheck=sum(lagCheck,1)>3;
%                                 if sum(byBeamCheck)>0 | sum(allBeamCheck)>0
%                                     pt3Fail=true;
%                                 end
%                                 lag_compare = [lag_compare lagCheck];
%                             end
%                             %if lw is a field run .15 test for lags 3-7
%                             if isfield(pt3_linear, 'lw')
%                                 corr_table = pt3_linear.lw.corrTable;
%                                 init_vals = corr_table(1,:) * .15;
%                                 lagCheck=init_vals(1)' < corr_table(4:8,:)';
%                                 byBeamCheck=sum(lagCheck,2)>3;
%                                 allBeamCheck=sum(lagCheck,1)>3;
%                                 if sum(byBeamCheck)>0 | sum(allBeamCheck)>0
%                                     pt3Fail=true;
%                                 end
%                                 lag_compare = [lag_compare lagCheck];
%                             end
%                             %if hn is a field run .15 test for lags 3-7
%                             if isfield(pt3_linear, 'hn')
%                                 corr_table = pt3_linear.hn.corrTable;
%                                 init_vals = corr_table(1,:) * .15;
%                                 lagCheck=init_vals(1)' < corr_table(4:8,:)';
%                                 byBeamCheck=sum(lagCheck,2)>3;
%                                 allBeamCheck=sum(lagCheck,1)>3;
%                                 if sum(byBeamCheck)>0 | sum(allBeamCheck)>0
%                                     pt3Fail=true;
%                                 end
%                                 lag_compare = [lag_compare lagCheck];
%                             end
%                             %if ln is a field run .15 test for lags 3-7
%                             if isfield(pt3_linear, 'ln')
%                                 corr_table = pt3_linear.hw.corrTable;
%                                 init_vals = corr_table(1,:) * .15;
%                                 lagCheck=init_vals(1)' < corr_table(4:8,:)';
%                                 byBeamCheck=sum(lagCheck,2)>3;
%                                 allBeamCheck=sum(lagCheck,1)>3;
%                                 if sum(byBeamCheck)>0 | sum(allBeamCheck)>0
%                                     pt3Fail=true;
%                                 end
%                                 lag_compare = [lag_compare lagCheck];
%                             end
%                         end %if linear is a field

                        
                    end %if pt3 exists
                end % for all tests
                
                %if there are any errors
                if pt3Fail
                   nmessage=nmessage+1;
                   obj.systemTest.messages(nmessage,:)={'System Test: One or more PT3 tests in the system test indicate potential EMI;',2,3};
                   obj.systemTest.status='caution';
                end         
                
                % System test present
                
                for n=1:nTests
                    failedIdx=strfind(meas.sysTest(n).data,'FAIL');
                    if isempty(failedIdx)
                        failedTests(n)=0;
                    else
                        failedTests(n)=length(failedIdx);
                    end % if isempty
                end % for n
                obj.systemTest.nFailedTests=nansum(failedTests);
                obj.systemTest.validTests=failedTests==0;
                
                % If test is present, determine if there are any fails
                if sum(obj.systemTest.validTests)<nTests
                    if sum(obj.systemTest.validTests)>0
                        nmessage=nmessage+1;
                        obj.systemTest.messages(nmessage,:)= {'System Test: One or more system test sets have at least one test that failed;', 2, 3};        
                        obj.systemTest.status='caution';
                    else
                        nmessage=nmessage+1;
                        obj.systemTest.messages(nmessage,:)= {'SYSTEM TEST: All system test sets have at least one test that failed;', 1, 3};        
                        obj.systemTest.status='warning';
                    end              
                end % if sum

                       
            end % if isempty

        end % systemTestQA
        
        function obj=compassQA(obj,meas)
        % Apply quality checks to compass
        %
        % INPUT:
        %
        % obj: object of clsQAData
        %
        % meas: object of clsMeasurement
        %
        % OUTPUT:
        %
        % obj: object of clsQAData

            % Initialize messages
            messages={};
            nmessage=0;
            
            % Does ADCP have a compass?
            heading=unique(meas.transects(1).sensors.heading_deg.internal.data);
            if length(heading==1) & heading==0
                % No compass
                obj.compass.status='inactive';
                obj.compass.status1=[];
                obj.compass.status2=[];
            else
                % ADCP has a compass. Is it needed?
                
                % Check for loop test
                nTests=length(meas.mbTests);
                loop=false;
                for n=1:nTests
                    if strcmp(meas.mbTests(n).type,'Loop')
                        loop=true;
                    end
                end % for loop tests

                % Check for GPS data
                gps=false;
                if ~isempty([meas.transects.gps])
                    gps=true;
                end % if GPS
                
                % Compass calibration required if loop or GPS used
                if loop || gps
                    % Compass required
                    
                    % Check ADCP manufacturer
                    if strcmpi(meas.transects(1).adcp.manufacturer,'SonTek')
                        % SonTek ADCP
                        
                        % Check for calibration
                        n=length(meas.compassCal);
                        if isempty(meas.compassCal(n).timeStamp)
                            
                            % No compass calibration
                            nmessage=nmessage+1;
                            messages(nmessage,:) = {'COMPASS: No compass calibration;' 1 4};
                            obj.compass.status1='warning';
                            
                        else
                            % If the G3 compass data has been decoded
                            % evaluate the result of the calibration. If
                            % the data are not available the default is to
                            % assume the calibration was good.
                            if isfield(meas.compassCal(end).result, 'compass') 
                                if ~strcmp(meas.compassCal(end).result.compass.error,'N/A')
                                    if meas.compassCal(end).result.compass.error <= 0.2
                                    	obj.compass.status1='good';
                                    else
                                        nmessage=nmessage+1;
                                        messages(nmessage,:) = {'COMPASS: Calibration result > 0.2 deg;' 2 4};
                                        obj.compass.status1='caution';
                                    end
                                else
                                    % Compass calibrated
                                    obj.compass.status1='good';
                                end
                            else
                                % Compass calibrated
                                obj.compass.status1='good';
                            end
                            
                        end % SonTek compass calibration
                    
                    else
                        % TRDI ADCP
                        
                        % Check for calibration
                        n=length(meas.compassCal);
                        if isempty(meas.compassCal(n).timeStamp)
                            
                            % No calibration, check for evaluation
                            n=length(meas.compassEval);
                            if isempty(meas.compassEval(n).timeStamp)
                                
                                % No calibration or evaluation
                                nmessage=nmessage+1;
                                messages(nmessage,:)={'COMPASS: No compass calibration;' 1 4};
                                obj.compass.status1='warning';
                                
                            else
                                
                                % Evaluation but no calibration
                                nmessage=nmessage+1;
                                messages(nmessage,:)={'Compass: No compass calibration;' 2 4};
                                obj.compass.status1='caution';
                            end % if compass evaluation
                        else
                            % Compass calibrated
                            
                            % Check for evaluation
                            n=length(meas.compassEval);
                            if isempty(meas.compassEval(n).timeStamp)
                                
                                % No evaluation
                                nmessage=nmessage+1;
                                messages(nmessage,:)={'Compass: No compass evaluation;' 2 4};
                                obj.compass.status1='caution';
                                
                            else
                                
                                if isfield(meas.compassEval(end).result, 'compass')
                                    if meas.compassEval(end).result.compass.error <= 1
                                        obj.compass.status1='good';
                                    else
                                        obj.compass.status1='caution';
                                        nmessage=nmessage+1;
                                        messages(nmessage,:)={'Compass: Evaluation result > 1 deg;' 2 4};
                                    end
                                else
                                    obj.compass.status1='good';
                                end                               
                            end % if compass evaluation
                        end % Compass calibration
                    end % Is SonTek?
                else
                    nC=length(meas.compassCal);
                    nE=length(meas.compassEval);
                    if nC<1 & nE<1
                        obj.compass.status1='default';
                    else
                        obj.compass.status1='good';
                    end % compass cal or eval
                end % Calibration required GPS or Loop

                obj.compass.status2='good';
                % Get data from checked transects
                checked=logical([meas.transects.checked]);
                if sum(checked)>=1
                    depths=[meas.transects(checked).depths];
                    sensors=[meas.transects(checked).sensors];
                                 
                    for n=1:length(depths)
                        magvar(n)=sensors(n).heading_deg.(sensors(n).heading_deg.selected).magVar_deg;
                    end

                    % Check for magvar consistency
                    magvarCheck=unique(round(magvar,3));
                    obj.compass.magvar=0;
                    if length(magvarCheck)>1
                        nmessage=nmessage+1;
                        messages(nmessage,:)={'Compass: Magnetic variation is not consistent among transects;' 2 4};
                        obj.compass.status2='caution';
                        obj.compass.magvar=1;
                    end    

                    % Check for magvar with GPS
                    if ~isempty([meas.transects.gps])
                        magvarCheck2=abs(magvarCheck)>0;
                        idx=find(magvar==0);
                        if ~isempty(idx)
                            nmessage=nmessage+1;
                            messages(nmessage,:)={'COMPASS: Magnetic variation is 0 and GPS data are present;' 1 4};
                            obj.compass.status2='warning';
                            obj.compass.magvar=2;
                            obj.compass.magvarIdx=idx;
                        end % if
                    end % if
                end % if checked   
                
                % Check pitch and roll
                nTransects=sum(checked);
                calRollIdx=[];
                calPitchIdx=[];
                magErrorIdx=[];
                if nTransects>0
                    for n=1:nTransects
                        selectedPitch=sensors(n).pitch_deg.selected;
                        meanPitch(n)=nanmean(sensors(n).pitch_deg.(selectedPitch).data);
                        stdPitch(n)=nanstd(sensors(n).pitch_deg.(selectedPitch).data);
                        selectedRoll=sensors(n).roll_deg.selected;
                        meanRoll(n)=nanmean(sensors(n).roll_deg.(selectedRoll).data);
                        stdRoll(n)=nanstd(sensors(n).roll_deg.(selectedRoll).data);

                        % If pitch and roll limist for compass calibration
                        % exist determine what transects have violations
                        if ~isempty(sensors(n).heading_deg.internal.rollLimit)
                            idxMax=find(sensors(n).roll_deg.(selectedRoll).data > sensors(n).heading_deg.internal.rollLimit(1,1));
                            idxMin=find(sensors(n).roll_deg.(selectedRoll).data < sensors(n).heading_deg.internal.rollLimit(1,2));
                            idx=[];
                            if ~isempty(idxMax)
                                idx(1:length(idxMax),1)=idxMax;
                            end
                            if ~isempty(idxMin)
                                idx(1:length(idxMin),2)=idxMin;
                            end

                            if ~isempty(idx)
                                calRollIdx=[calRollIdx,n];
                            end
                            idxMax=find(sensors(n).pitch_deg.(selectedPitch).data > sensors(n).heading_deg.internal.pitchLimit(1,1));
                            idxMin=find(sensors(n).pitch_deg.(selectedPitch).data < sensors(n).heading_deg.internal.pitchLimit(1,2));
                            if ~isempty(idxMax)
                                idx(1:length(idxMax),1)=idxMax;
                            end
                            if ~isempty(idxMin)
                                idx(1:length(idxMin),2)=idxMin;
                            end
                            if ~isempty(idx)
                                calPitchIdx=[calPitchIdx,n];
                            end
                        end

                        % If internal compass is used check magnetic error if
                        % available and determine if any transects exceed the
                        % threshold.
                        if strcmp(sensors(n).heading_deg.selected,'internal')
                            idx=[];
                            if ~isempty(sensors(n).heading_deg.internal.magError)
                                idx=find(sensors(n).heading_deg.internal.magError>2);
                            end
                            if ~isempty(idx)
                                magErrorIdx=[magErrorIdx,n];
                            end
                        end
                    end
                    obj.compass.meanPitchIdxW=find(abs(meanPitch)>8);
                    obj.compass.meanPitchIdxC=find(abs(meanPitch)>4 & abs(meanPitch)<=8);
                    if ~isempty(obj.compass.meanPitchIdxW)
                        obj.compass.status2='warning';
                        nmessage=nmessage+1;
                        messages(nmessage,:)={'PITCH: One or more transects have a mean pitch > 8 deg;' 1 4};
                    elseif ~isempty(obj.compass.meanPitchIdxC)
                        if strcmp(obj.compass.status2,'good')
                            obj.compass.status2='caution';
                        end
                        nmessage=nmessage+1;
                        messages(nmessage,:)={'Pitch: One or more transects have a mean pitch > 4 deg;' 2 4};
                    end

                    obj.compass.meanRollIdxW=find(abs(meanRoll)>8);
                    obj.compass.meanRollIdxC=find(abs(meanRoll)>4 & abs(meanRoll)<=8);
                    if ~isempty(obj.compass.meanRollIdxW)
                        obj.compass.status2='warning';
                        nmessage=nmessage+1;
                        messages(nmessage,:)= {'ROLL: One or more transects have a mean roll > 8 deg;' 1 4};
                    elseif ~isempty(obj.compass.meanRollIdxW) 
                        if strcmp(obj.compass.status2,'good')
                            obj.compass.status2='caution';
                        end
                        nmessage=nmessage+1;
                        messages(nmessage,:)={'Roll: One or more transects have a mean roll > 4 deg;' 2 4};
                    end

                    obj.compass.stdPitchIdx=find(nanmax(stdPitch)>5);
                    if ~isempty(obj.compass.stdPitchIdx)
                        if strcmp(obj.compass.status2,'good')
                            obj.compass.status2='caution';
                        end
                        nmessage=nmessage+1;
                        messages(nmessage,:)={'Pitch: One or more transects have a pitch std dev > 5 deg;' 2 4};
                    end

                    obj.compass.stdRollIdx=find(nanmax(stdRoll)>5);
                    if ~isempty(obj.compass.stdRollIdx)
                        if strcmp(obj.compass.status2,'good')
                            obj.compass.status2='caution';
                        end
                        nmessage=nmessage+1;
                        messages(nmessage,:)={'Roll: One or more transects have a roll std dev > 5 deg;' 2 4};
                    end

                    obj.compass.calPitchIdx=calPitchIdx;
                    if ~isempty(calPitchIdx)  
                        if strcmp(obj.compass.status2,'good')
                            obj.compass.status2='caution';
                        end
                        nmessage=nmessage+1;
                        messages(nmessage,:)={'Compass: One or more transects have pitch exceeding calibration limits;' 2 4};
                    end

                    obj.compass.calRollIdx=calRollIdx;
                    if ~isempty(calRollIdx)
                        if strcmp(obj.compass.status2,'good')
                            obj.compass.status2='caution';
                        end
                        nmessage=nmessage+1;
                        messages(nmessage,:)={'Compass: One or more transects have roll exceeding calibration limits;' 2 4};
                    end

                    obj.compass.magErrorIdx=magErrorIdx;    
                    if ~isempty(magErrorIdx)
                        if strcmp(obj.compass.status2,'good')
                            obj.compass.status2='caution';
                        end
                        nmessage=nmessage+1;
                        messages(nmessage,:)={'Compass: One or more transects have a change in mag field exceeding 2%;' 2 4};
                    end
                end 
                if strcmpi(obj.compass.status1,'warning') || strcmpi(obj.compass.status2,'warning')
                    obj.compass.status='warning';
                elseif strcmpi(obj.compass.status1,'caution') || strcmpi(obj.compass.status2,'caution')
                    obj.compass.status='caution';
                else
                    obj.compass.status='good';
                end
               
                
            end % ADCP has compass
            obj.compass.messages=messages;
        end % compassQA
               
        function obj=temperatureQA(obj,meas)
        % Apply quality checks to compass
        %
        % INPUT:
        %
        % obj: object of clsQAData
        %
        % meas: object of clsMeasurement
        %
        % OUTPUT:
        %
        % obj: object of clsQAData
      
            % Run quality assurance checks
            [code,obj.temperature.messages]=clsQAData.tempQACheck(meas);
    
            % Set button color
            switch code
                case 1
                    obj.temperature.status='good';
                case 2
                    obj.temperature.status='caution';
                case 3
                    obj.temperature.status='warning';
            end % switch
          
        end % temperatureQA

        function obj=movingbedQA(obj,meas)
        % Apply quality checks to moving-bed tests
        %
        % INPUT:
        %
        % obj: object of clsQAData
        %
        % meas: object of clsMeasurement
        %
        % OUTPUT:
        %
        % obj: object of clsQAData   
        
            % Initialize messages
            nmessage=0;
            messages={};
            obj.movingbed.code=0;
            % Are there moving-bed tests?
            if isempty(meas.mbTests)
                
                % No moving-bed test
                nmessage=nmessage+1;
                messages(nmessage,:)={'MOVING-BED TEST: No moving bed test;' 1 6};
                obj.movingbed.status='warning';
                obj.movingbed.code=3;
                
            else
                
                % Moving-bed tests available
                mbData=meas.mbTests;
                
                % Are the tests valid
                validData=[mbData.userValid];
                if sum(validData)==0
                    
                    % No valid tests
                    nmessage=nmessage+1;
                    messages(nmessage,:)={'MOVING-BED TEST: No valid moving-bed test based on user input;' 1 6};
                    obj.movingbed.status='warning';
                    obj.movingbed.code=3;
                else
                
                    % Check for duplicate valid moving-bed tests
                    trans=[mbData(validData).transect];
                    fileNames={trans.fileName};
                    number=length(unique(fileNames));
                    if number<length(fileNames)
                        nmessage=nmessage+1;
                        messages(nmessage,:)={'MOVING-BED TEST: Duplicate moving-bed test files marked valid;' 1 6};
                        obj.movingbed.status='warning';
                        obj.movingbed.code=3;
                    end
                end
                
                if obj.movingbed.code==0
                                      
                    % ID selected test
                    idxSelected=find([mbData.selected]==1);
                    % Tests are valid, check quality
                    testQuality={mbData(idxSelected).testQuality};
                    mb={mbData(idxSelected).movingBed};
                    
                    % Quality good
                    if sum(strcmp('Good',testQuality))>0
                        obj.movingbed.status='good';
                        obj.movingbed.code=1;
                        
                        % Is there a moving bed
                        if sum(strcmp('Yes',mb))>0
                            
                            % Moving-bed present
                            nmessage=nmessage+1;
                            messages(nmessage,:)={'Moving-Bed Test: A moving-bed is present, use GPS or moving-bed correction;' 2 6};
                            obj.movingbed.code=2;
                            
                            % Are the tests stationary tests
                            mbData=meas.mbTests;
                            testSelected=[mbData.selected];
                            if sum(ismember({mbData(testSelected).type},'Loop'))<1 && sum(strcmp('Yes',mb))>0
                                
                                % Are there 3 or more stationary tests
                                if length(meas.mbTests)<3
                                    
                                    % Is GPS available
                                    if isempty([meas.transects.gps]) 
                                        
                                        % Less than 3 stationary tests and
                                        % no GPS
                                        nmessage=nmessage+1;
                                        messages(nmessage,:)={'Moving-Bed Test: Less than 3 stationary tests available for moving-bed correction;' 2 6};
                                    end % if GPS
                                end % if number of stationary tests
                            end % if stationary test
                            obj.movingbed.status='caution'; 
                        end % if moving bed
                        
                    % Test quality has warnings
                    elseif sum(strcmp('Warnings',testQuality))>0
                        nmessage=nmessage+1;
                        messages(nmessage,:)={'Moving-Bed Test: The moving-bed test(s) has warnings, please review tests to determine validity;' 2 6};
                        obj.movingbed.status='caution';
                        obj.movingbed.code=2;
                    elseif sum(strcmp('Manual',testQuality))>0
                        nmessage=nmessage+1;
                        messages(nmessage,:)={'MOVING-BED TEST: The user has manually forced the use of some tests;' 1 6};
                        obj.movingbed.status='warning';
                        obj.movingbed.code=3;
                    else
                        % Test quality has critical errors
                        nmessage=nmessage+1;
                        messages(nmessage,:)={'MOVING-BED TEST: The moving-bed test(s) have critical errors and will not be used;' 1 6};
                        obj.movingbed.status='warning';
                        obj.movingbed.code=3;
                    end % if test quality
                    
                    % Check multiple loops for consistency
                    lidxLoop=ismember({mbData.type},'Loop');
                    lidxLoopQual=~ismember({mbData.testQuality},'Error');
                    lidxLoopUserValid=[mbData.userValid];
                    lidxValidLoop=all([lidxLoop;lidxLoopQual;lidxLoopUserValid]);
                    if sum(lidxValidLoop)>1
                        mbUnique=unique({mbData(lidxValidLoop).movingBed});
                        if length(mbUnique)>1
                            nmessage=nmessage+1;
                            messages(nmessage,:)={'Moving-Bed Test: Results of valid loops are not consistent, review moving-bed tests;' 2 6};
                            if obj.movingbed.code<3
                                obj.movingbed.code=2;
                                obj.movingbed.status='caution';
                            end
                        end
                    end

                        
                end % if valid tests
            end % if moving-bed tests
            obj.movingbed.messages=messages;
        end % movingbedQA
        
        function obj=userQA(obj,meas)
        % Apply quality checks to compass
        %
        % INPUT:
        %
        % obj: object of clsQAData
        %
        % meas: object of clsMeasurement
        %
        % OUTPUT:
        %
        % obj: object of clsQAData
        
            % Assume good results
            obj.user.status='good';

            % Initialize messages
            nmessage=0;
            obj.user.messages={};

            % Check for Station Name
            obj.user.staName=0;
            if isempty(meas.stationName)
                nmessage=nmessage+1;
                obj.user.messages(nmessage,:)={'Site Info: Station name not entered;' 2 2};
                obj.user.status='caution';
                obj.user.staName=1;
            end

            % Check for Station Number
            obj.user.staNumber=0;
            if isempty(meas.stationNumber)
                nmessage=nmessage+1;
                obj.user.messages(nmessage,:)= {'Site Info: Station number not entered;' 2 2};
                obj.user.status='caution';
                obj.user.staNumber=1;
            end
        end % userQA
        
        function obj=depthsQA(obj,meas)
        % Function applies the quality assurance checks to the depths.
        %
        % INPUT:
        %
        % obj: object of clsQAData
        %
        % meas: object of clsMeasurement
        %
        % OUTPUT:
        %
        % obj: object of clsQAData
        
            % Initialize variables
            checked=logical([meas.transects.checked]);
            if sum(checked)>0
                nTransects=length(checked);
                obj.depths.qTotal=nan(nTransects,1);
                obj.depths.qMaxRun=nan(nTransects,1);
                obj.depths.qTotalCaution=false(nTransects,1);
                obj.depths.qRunCaution=false(nTransects,1);
                obj.depths.qTotalWarning=false(nTransects,1);
                obj.depths.qRunWarning=false(nTransects,1);
                obj.depths.allInvalid=false(nTransects,1);

                % Initialize messages
                nmessage=0;
                obj.depths.messages={};
                obj.depths.status='good';
                
                % Check draft
                trans=meas.transects(checked);
                depthClass=[trans.depths];
                depthRef=depthClass(1).selected;
                depthsData=[depthClass.(depthRef)];
                drafts=[depthsData.draftUse_m];

                % Check for draft consistency

                [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=clsMeasurement.unitsMultiplier();
                if ~isempty(unitsL)
                    draftCheck=unique(round(drafts.*unitsL,2));
                else
                    draftCheck=unique(round(drafts,3));
                end
                obj.depths.draft=0;
                if length(draftCheck)>1
                    nmessage=nmessage+1;
                    obj.depths.messages(nmessage,:)={'Depth: Transducer depth is not consistent among transects;', 2 10};
                    obj.depths.status='caution';
                    obj.depths.draft=1;
                end

                % Check for zero draft
                idx=find(draftCheck<0.01);
                if ~isempty(idx)
                    nmessage=nmessage+1;
                    obj.depths.messages(nmessage,:)={'DEPTH: Transducer depth is too shallow, likely 0;' 1 10};
                    obj.depths.status='warning';
                    obj.depths.draft=2;
                end

                % Apply thresholds to each checked transect
                for n=1:nTransects
                    if checked(n)

                        if strcmp(meas.transects(n).depths.composite,'On')
                            idxNA=strcmp(meas.transects(n).depths.(meas.transects(n).depths.selected).depthSourceEns(meas.transects(n).inTransectIdx),'NA');
                            depthValid=~strcmp(meas.transects(n).depths.(meas.transects(n).depths.selected).depthSourceEns(meas.transects(n).inTransectIdx),'IN');
                            depthValid(idxNA)=0;
                        else
                            % Prep depth data
                            depthValid=meas.transects(n).depths.(meas.transects(n).depths.selected).validData(meas.transects(n).inTransectIdx);
                            idx=find(isnan(meas.transects(n).depths.(meas.transects(n).depths.selected).depthProcessed_m(meas.transects(n).inTransectIdx)));
                            depthValid(idx)=0;
                        end

                        if nansum(depthValid)==0
                            obj.depths.allInvalid(n)=true;
                        end

                        % Compute characteristics
                        [obj.depths.qTotal(n), obj.depths.qMaxRun(n), obj.depths.ensInvalid(n)]...
                            =clsQAData.invalidQA(depthValid,meas.discharge(n));
                        qTotalPercent=abs((obj.depths.qTotal(n)./meas.discharge(n).total).*100);
                        qMaxRunPercent=abs((obj.depths.qMaxRun(n)./meas.discharge(n).total).*100);
                        ensInvalidPercent=abs((obj.depths.ensInvalid(n)./length(meas.transects(n).boatVel.btVel.uProcessed_mps)).*100);


                        % Apply total interpolated discharge threshold
                        if (qTotalPercent > obj.qTotalThresholdWarning)
                            obj.depths.qTotalWarning(n)=true;
                        end

                        if (qTotalPercent > obj.qTotalThresholdCaution)
                            obj.depths.qTotalCaution(n)=true;
                        end

                        % Apply cluster warning threshold
                        if (qMaxRunPercent > obj.qRunThresholdWarning)
                            obj.depths.qRunWarning(n)=true;
                        end

                        % Apply cluster caution threshold
                        if (qMaxRunPercent > obj.qRunThresholdCaution)
                            obj.depths.qRunCaution(n)=true;
                        end                            
                    end
                end  

                % Generate user feedback 
                if obj.depths.draft==1
                    obj.depths.status='caution';
                end

                if sum(obj.depths.qRunWarning)>0
                    nmessage=nmessage+1;
                    obj.depths.messages(nmessage,:)={['DEPTH: Int. Q for consecutive invalid ensembles exceeds ',num2str(obj.qRunThresholdWarning),'%;'] 1 10};
                    obj.depths.status='warning';
                elseif sum(obj.depths.qRunCaution)>0    
                    nmessage=nmessage+1;
                    obj.depths.messages(nmessage,:)={['Depth: Int. Q for consecutive invalid ensembles exceeds ',num2str(obj.qRunThresholdCaution),'%;'] 2 10};
                    obj.depths.status='caution';            
                end

                if sum(obj.depths.qTotalWarning)>0
                    nmessage=nmessage+1;
                    obj.depths.messages(nmessage,:)={['DEPTH: Int. Q for invalid ensembles in a transect exceeds ',num2str(obj.qTotalThresholdWarning),'%;'] 1 10};
                    obj.depths.status='warning';
                elseif sum(obj.depths.qTotalCaution)>0
                    nmessage=nmessage+1;
                    obj.depths.messages(nmessage,:)={['Depth: Int. Q for invalid ensembles in a transect exceeds ',num2str(obj.qTotalThresholdCaution),'%;'] 2 10};
                    obj.depths.status='caution';
                end

                if obj.depths.draft==2
                    obj.depths.status='warning';
                end

                if nansum(obj.depths.allInvalid)>0
                    nmessage=nmessage+1;
                    obj.depths.messages(nmessage,:)={['DEPTH: There are no valid depths for one or more transects.'] 2 10};
                    obj.depths.status='warning';
                end
            else
                obj.depths.status='inactive';
                obj.depths.messages={};
            end
        end % depthsQA
            
        function obj=boatQA(obj,meas)
        % Function applies the quality assurance checks to the boat velocity data.
        %
        % INPUT:
        %
        % obj: object of clsQAData
        %
        % meas: object of clsMeasurement
        %
        % OUTPUT:
        %
        % obj: object of clsQAData
        
            % Get transect info
            checked=logical([meas.transects.checked]);
            nTransects=length(checked);

            % Initialize data and filter labels
            velClasses={'btVel','ggaVel','vtgVel'};
            prefixWarning={'BT-','GGA-','VTG-'};
            prefixCaution={'bt-','gga-','vtg-'};
            prefix2={'All: ','Original: ','ErrorVel: ','VertVel: ','Other: ','3Beams: ';...
                     'All: ','Original: ','DGPS: ','Altitude: ','Other: ','HDOP: ';...
                     'All: ','Original: ','DGPS: ','Altitude: ','Other: ','HDOP: ';};
     
            % Set number of filters for BT, GGA, VTG
            numFilters=[6,6,2];
            
            % Loop for data types
            for m=1:3             
                
                % Initialize messages
                nmessage=0;
                obj.(velClasses{m}).messages={};
                
                % Initialize properties
                obj.(velClasses{m}).qTotalCaution=false(nTransects,6);
                obj.(velClasses{m}).qRunCaution=false(nTransects,6);
                obj.(velClasses{m}).qTotalWarning=false(nTransects,6);
                obj.(velClasses{m}).qRunWarning=false(nTransects,6);
                obj.(velClasses{m}).allInvalid=false(nTransects,1);
                statusSwitch=0;
                avgSpeedCheck = 0;
                % Loop for filters
                for k=1:numFilters(m)
                     obj.(velClasses{m}).status='inactive';
                    % Loop for transects
                    for n=1:nTransects
                       
                        if checked(n)
                    
                            % Check to see if transect has the specified
                            % property
                            if ~isempty(meas.transects(n).boatVel.(velClasses{m}))
                                
                                % Assume status is good
                                obj.(velClasses{m}).status='good';
                            
                                % Compute characteristics
                                [obj.(velClasses{m}).qTotal(n,k), obj.(velClasses{m}).qMaxRun(n,k), obj.(velClasses{m}).ensInvalid(n,k)]=clsQAData.invalidQA(...
                                    meas.transects(n).boatVel.(velClasses{m}).validData(k,meas.transects(n).inTransectIdx),...
                                    meas.discharge(n));
                                qTotalPercent=abs((obj.(velClasses{m}).qTotal(n,k)./meas.discharge(n).total).*100);
                                qMaxRunPercent=abs((obj.(velClasses{m}).qMaxRun(n,k)./meas.discharge(n).total).*100);
                                ensInvalidPercent=abs((obj.(velClasses{m}).ensInvalid(n,k)./length(meas.transects(n).boatVel.btVel.uProcessed_mps)).*100);                            
                                
                                % Check if all data are invalid
                                if k==1
                                    sumValid=nansum(meas.transects(n).boatVel.(velClasses{m}).validData(1,meas.transects(n).inTransectIdx));
                                    if sumValid==0
                                        obj.(velClasses{m}).allInvalid(n)=true;
                                    end
                                end
                                
                                % Apply total interpolated discharge threshold
                                if (qTotalPercent > obj.qTotalThresholdWarning)
                                    obj.(velClasses{m}).qTotalWarning(n,k)=true;
                                end
                                
                                if (qTotalPercent > obj.qTotalThresholdCaution)
                                    obj.(velClasses{m}).qTotalCaution(n,k)=true;
                                end

                                % Apply cluster warning threshold
                                if (qMaxRunPercent > obj.qRunThresholdWarning)
                                    obj.(velClasses{m}).qRunWarning(n,k)=true;
                                end

                                % Apply cluster caution threshold
                                if (qMaxRunPercent > obj.qRunThresholdCaution)
                                    obj.(velClasses{m}).qRunCaution(n,k)=true;
                                end
                                
                                 %check if vtg is selected and mean speed is greater or
                                %equal to .24 m/s
                                if strcmpi(velClasses{m},'vtgVel') && strcmpi(meas.transects(n).boatVel.selected,'vtgVel') && avgSpeedCheck == 0
                                    avgSpeed = nanmean(sqrt(meas.transects(n).boatVel.vtgVel.u_mps.^2+meas.transects(n).boatVel.vtgVel.v_mps.^2));
                                    if (avgSpeed < 0.24)
                                        obj.(velClasses{m}).qTotalCaution(n,3)=true;
                                        nmessage=nmessage+1;
                                        units = getUserPref('Units');
                                        
                                        if strcmp(units,'English')
                                            obj.(velClasses{m}).messages(nmessage,:)={'vtg-AvgSpeed: VTG data may not be accurate for average boat speed less than 0.8 ft/s;' 2 8};                    
                                        else
                                            obj.(velClasses{m}).messages(nmessage,:)={'vtg-AvgSpeed: VTG data may not be accurate for average boat speed less than 0.24 m/s;' 2 8};
                                        end
                                        avgSpeedCheck = 1;
                                    end
                                end
                            end % isempty
                        end % if checked
                    end % for n

                    % Generate user feedback for ensemble clusters
                    if sum(obj.(velClasses{m}).qRunWarning(:,k))>0
                        nmessage=nmessage+1;
                        if strcmp(prefixWarning{m},'BT-')
                            module_code = 7;
                        else
                            module_code = 8;
                        end
                        obj.(velClasses{m}).messages(nmessage,:)={[prefixWarning{m},prefix2{m,k},'Int. Q for consecutive invalid ensembles exceeds ',num2str(obj.qRunThresholdWarning),'%;'] 1 module_code};
                        statusSwitch=2;
                    elseif sum(obj.(velClasses{m}).qRunCaution(:,k))>0    
                        nmessage=nmessage+1;
                        if strcmp(prefixCaution{m},'bt-')
                            module_code = 7;
                        else
                            module_code = 8;
                        end
                        obj.(velClasses{m}).messages(nmessage,:)={[prefixCaution{m},prefix2{m,k},'Int. Q for consecutive invalid ensembles exceeds ',num2str(obj.qRunThresholdCaution),'%;'] 2 module_code};
                        if statusSwitch<1
                            statusSwitch=1;
                        end            
                    end
                        
                    % Generate user feedback for total invalid Q
                    if sum(obj.(velClasses{m}).qTotalWarning(:,k))>0
                        nmessage=nmessage+1;
                        if strcmp(prefixWarning{m},'BT-')
                            module_code = 7;
                        else
                            module_code = 8;
                        end
                        obj.(velClasses{m}).messages(nmessage,:)={[prefixWarning{m},prefix2{m,k},'Int. Q for invalid ensembles in a transect exceeds ',num2str(obj.qTotalThresholdWarning),'%;'] 1 module_code};
                        statusSwitch=2;
                    elseif sum(obj.(velClasses{m}).qTotalCaution(:,k))>0    
                        nmessage=nmessage+1;
                        if strcmp(prefixCaution{m},'bt-')
                            module_code = 7;
                        else
                            module_code = 8;
                        end
                        obj.(velClasses{m}).messages(nmessage,:)={[prefixCaution{m},prefix2{m,k},'Int. Q for invalid ensembles in a transect exceeds ',num2str(obj.qTotalThresholdCaution),'%;'] 2 module_code};
                        if statusSwitch<1
                            statusSwitch=1;
                        end                           
                    end
                end % for k
                
                % Generate user feedback for all invalid
                if sum(obj.(velClasses{m}).allInvalid)>0
                    obj.(velClasses{m}).status='warning';
                    nmessage=nmessage+1;
                    module_code = 7;
                    obj.(velClasses{m}).messages(nmessage,:)={[prefixWarning{m},prefix2{m,1},'There are no valid data for one or more transects.'] 1 module_code};
                end
                
                if statusSwitch==2
                    obj.(velClasses{m}).status='warning';
                elseif statusSwitch==1
                    obj.(velClasses{m}).status='caution';
                end
                

               
            end % for m
        end % boatQA
            
        function obj=waterQA(obj,meas)
        % Function applies the quality assurance checks to the water velocity data.
        %
        % INPUT:
        %
        % obj: object of clsQAData
        %
        % meas: object of clsMeasurement
        %
        % OUTPUT:
        %
        % obj: object of clsQAData
        
            % Get transect info
            checked=logical([meas.transects.checked]);
            nTransects=length(checked);
            
            % Initialize properties
            obj.wVel.qTotal=nan(nTransects,8);
            obj.wVel.qMaxRun=nan(nTransects,8);
            obj.wVel.qTotalCaution=false(nTransects,8);
            obj.wVel.qRunCaution=false(nTransects,8);
            obj.wVel.qTotalWarning=false(nTransects,8);
            obj.wVel.qRunWarning=false(nTransects,8);
            obj.wVel.allInvalid=false(nTransects,1);
            statusSwitch=0;
            
            % Initialize data and filter labels
            prefix2={'All: ','Original: ','ErrorVel: ','VertVel: ','Other: ','3Beams: ','SNR:'};
              
            % Set filter index
            if strcmp(meas.transects(1).adcp.manufacturer,'TRDI')
                filterIdx=[1, 2, 3, 4, 5, 6];
            else
                filterIdx=[1, 2, 3, 4, 5, 6, 8];
            end
            
            % Initialize messages
            nmessage=0;
            obj.wVel.messages={};
            if sum(checked)>0
                % Loop for filters
                for j=1:length(filterIdx)
                    k=filterIdx(j);

                    % Loop for transects
                    for n=1:nTransects

                        if checked(n)                  
                            validOrig=nansum(squeeze(meas.transects(n).wVel.validData(:,meas.transects(n).inTransectIdx,2)));
                            validOrig(validOrig>0)=1;
                            % Compute characteristics
                             valid=nansum(squeeze(meas.transects(n).wVel.validData(:,meas.transects(n).inTransectIdx,k)));
                             valid(valid>0)=1;
                             if k>2
                                 valid=(valid-validOrig);
                                 valid=(valid~=-1);
                             end
                             
                             % Check to see if all data are invalid
                             if k==1
                                 if nansum(valid)<1
                                     obj.wVel.allInvalid(n)=true;
                                 end
                             end
                             
                            [obj.wVel.qTotal(n,k), obj.wVel.qMaxRun(n,k), obj.wVel.ensInvalid(n,k)]=clsQAData.invalidQA(...
                                valid, meas.discharge(n));
                            qTotalPercent=abs((obj.wVel.qTotal(n,k)./meas.discharge(n).total).*100);
                            qMaxRunPercent=abs((obj.wVel.qMaxRun(n,k)./meas.discharge(n).total).*100);
                            ensInvalidPercent=abs((obj.wVel.ensInvalid(n,k)./length(meas.transects(n).boatVel.btVel.uProcessed_mps)).*100);

                            % Apply total interpolated discharge threshold
                            if (qTotalPercent > obj.qTotalThresholdWarning)
                                obj.wVel.qTotalWarning(n,k)=true;
                            end

                            % Compute % Q in invalid cells and ensembles
                            validCells=squeeze(meas.transects(n).wVel.validData(:,meas.transects(n).inTransectIdx,k));
                            qInvalidTotal=nansum(meas.discharge(n).middleCells(~validCells))+nansum(meas.discharge(n).topEns(~valid))+nansum(meas.discharge(n).bottomEns(~valid));
                            qInvalidPercent=abs((qInvalidTotal/meas.discharge(n).total).*100);
                            if (qInvalidPercent > obj.qTotalThresholdCaution)
                                obj.wVel.qTotalCaution(n,k)=true;
                            end

                            % Apply cluster warning threshold
                            if (qMaxRunPercent > obj.qRunThresholdWarning)
                                obj.wVel.qRunWarning(n,k)=true;
                            end

                            % Apply cluster caution threshold
                            if (qMaxRunPercent > obj.qRunThresholdCaution)
                                obj.wVel.qRunCaution(n,k)=true;
                            end
                        end % if checked
                    end % for n

                    % Generate user feedback for ensemble clusters
                    if sum(obj.wVel.qRunWarning(:,k))>0
                        nmessage=nmessage+1;
                        obj.wVel.messages(nmessage,:)={['WT-',prefix2{j},'Int. Q for consecutive invalid ensembles exceeds ',num2str(obj.qRunThresholdWarning),'%;'] 1 11};
                        statusSwitch=2;
                    elseif sum(obj.wVel.qRunCaution(:,k))>0    
                        nmessage=nmessage+1;
                        obj.wVel.messages(nmessage,:)={['wt-',prefix2{j},'Int. Q for consecutive invalid ensembles exceeds ',num2str(obj.qRunThresholdCaution),'%;'] 2 11};
                        if statusSwitch<1
                            statusSwitch=1;
                        end
                    end

                    % Generate user feedback for total invalid Q
                    if sum(obj.wVel.qTotalWarning(:,k))>0
                        nmessage=nmessage+1;
                        obj.wVel.messages(nmessage,:)={['WT-',prefix2{j},'Int. Q for invalid ensembles in a transect exceeds ',num2str(obj.qTotalThresholdWarning),'%;'] 1 11};
                        statusSwitch=2;
                    end

                    if sum(obj.wVel.qTotalCaution(:,k))>0    
                        nmessage=nmessage+1;
                        obj.wVel.messages(nmessage,:)={['wt-',prefix2{j},'Int. Q for invalid cells and ensembles in a transect exceeds ',num2str(obj.qTotalThresholdCaution),'%;'] 2 11};
                        if statusSwitch<1
                            statusSwitch=1;
                        end
                    end
                end % for j
                
                % Generate user feedback for all invalid
                if sum(obj.wVel.allInvalid)>0
                    nmessage=nmessage+1;
                    obj.wVel.messages(nmessage,:)={['WT-',prefix2{1},'There are no valid data for one or more transects.'] 1 11};
                    statusSwitch=2;
                end
                
                obj.wVel.status='good';
                if statusSwitch==2
                    obj.wVel.status='warning';
                elseif statusSwitch==1
                    obj.wVel.status='caution';
                end
            else
                obj.wVel.status='inactive';
            end
        end % waterQA
        
        function obj=extrapolationQA(obj,meas)
        % Apply quality checks to extrapolation methods
        %
        % INPUT:
        %
        % obj: object of clsQAData
        %
        % meas: object of clsMeasurement
        %
        % OUTPUT:
        %
        % obj: object of clsQAData
        
             % Initialize variables
            nmessage=0;
            obj.extrapolation.messages={};

            % Apply results to messages and button colors 
            
            checked=logical([meas.transects.checked]);
            if sum(checked)>0
                obj.extrapolation.status='good';  
                extrapUncert=clsUncertainty.uncertExtrap(meas,meas.discharge,checked);

                if abs(extrapUncert)>2
                    nmessage=nmessage+1;
                    obj.extrapolation.messages(nmessage,:) ={'Extrapolation: The extrapolation uncertainty is more than 2 percent;' 2 12};
                    nmessage=nmessage+1;
                    obj.extrapolation.messages(nmessage,:)={'    Carefully review the extrapolation;' 2 12};
                    obj.extrapolation.status='caution';
                end  
            else
                obj.extrapolation.status='inactive';
            end
        end % extrapolationQA
        
        function obj=edgesQA(obj,meas)
        % Apply quality checks to edge estimates
        %
        % INPUT:
        %
        % obj: object of clsQAData
        %
        % meas: object of clsMeasurement
        %
        % OUTPUT:
        %
        % obj: object of clsQAData
        
            % Initialize messages
            nmessage=0;
            obj.edges.messages={};
            
            % ID check transects
            checked=logical([meas.transects.checked]);
           
            if sum(checked)>0
                % Set default status to good
                obj.edges.status='good';
                
                % Left and right discharges
                leftQ=[meas.discharge(checked).left];
                rightQ=[meas.discharge(checked).right];
                totalQ=[meas.discharge(checked).total];

                % Check left edge for q>5%
                obj.edges.leftQ=0;
                leftPercent=(nanmean(leftQ)./nanmean(totalQ)).*100;
                if abs(leftPercent)>5
                    obj.edges.status='caution';
                    nmessage=nmessage+1;
                    obj.edges.messages(nmessage,:)= {'Edges: Left edge Q is greater than 5%;' 2 13};
                    obj.edges.leftQ=1;                
                end

                % Check right edge for q>5%
                obj.edges.rightQ=0;
                rightPercent=(nanmean(rightQ)./nanmean(totalQ)).*100;
                if abs(rightPercent)>5
                    obj.edges.status='caution';
                    nmessage=nmessage+1;
                    obj.edges.messages(nmessage,:)={'Edges: Right edge Q is greater than 5%;' 2 13};
                    obj.edges.rightQ=1;                
                end            
            
                % Check for consistent sign
                signCheckLeft=unique(sign([meas.discharge(checked).left]));
                obj.edges.leftSign=0;
                if length(signCheckLeft)>1 && abs(leftPercent)>0.5
                    obj.edges.status='caution';
                    nmessage=nmessage+1;
                    obj.edges.messages(nmessage,:)= {'Edges: Sign of left edge Q is not consistent;' 2 13};
                    obj.edges.leftSign=1;
                end
                
                signCheckRight=unique(sign([meas.discharge(checked).right]));
                obj.edges.rightSign=0;
                if length(signCheckRight)>1 && abs(rightPercent)>0.5
                    obj.edges.status='caution';
                    nmessage=nmessage+1;
                    obj.edges.messages(nmessage,:)= {'Edges: Sign of right edge Q is not consistent;' 2 13};
                    obj.edges.rightSign=1;
                end
                
                % Check distance moved 
                nTransects=length(meas.transects);
                k=0;
                for n=1:nTransects
                    if checked(n)
                        k=k+1;
                        [distMovedRight(k), distMovedLeft(k), dmg(k)]=clsQAData.edgeDistanceMoved(meas.transects(n));
                        edgeDistLeft(k)=meas.transects(n).edges.left.dist_m;
                        edgeDistRight(k)=meas.transects(n).edges.right.dist_m;
                    end
                end % for nTransects
                dmg5per = 0.05.*nanmean(dmg);
                avgEdgeDist=nanmean(edgeDistRight);
                threshold=nanmin([dmg5per,avgEdgeDist]);
                obj.edges.rightDistMovedIdx=find(distMovedRight>threshold);
                avgEdgeDist=nanmean(edgeDistLeft);
                threshold=nanmin([dmg5per,avgEdgeDist]);
                obj.edges.leftDistMovedIdx=find(distMovedLeft>threshold);
                if ~isempty(obj.edges.rightDistMovedIdx)
                    obj.edges.status='caution';
                    nmessage=nmessage+1;
                    obj.edges.messages(nmessage,:)= {'Edges: Excessive boat movement in right edge ensembles;' 2 13};
                end
                if ~isempty(obj.edges.leftDistMovedIdx)
                    obj.edges.status='caution';
                    nmessage=nmessage+1;
                    obj.edges.messages(nmessage,:)={'Edges: Excessive boat movement in left edge ensembles;' 2 13};
                end

                % Check for edge ensembles marked invalid due to excluded
                % distance.
                excludedCheck=false;
                for n=1:nTransects
                    if checked(n)
                        excludedData=squeeze(meas.transects(n).wVel.validData(:,:,7));
                        ensSumExcludedData=nansum(excludedData);
                        cellsAboveSL=nansum(meas.transects(n).wVel.cellsAboveSL);
                        ensSumExcludedData=~(ensSumExcludedData==cellsAboveSL);
                        numInvalidBeginning=find(ensSumExcludedData>=1,1,'first');
                        numInvalidEnd=length(ensSumExcludedData)-find(ensSumExcludedData>=1,1,'last');
                        if ~isempty(numInvalidBeginning)
                            if numInvalidBeginning>1 || numInvalidEnd>1
                                excludedCheck=true;
                            end
                        end
                    end
                end
                if excludedCheck
                    obj.edges.status='caution';
                    nmessage=nmessage+1;
                    obj.edges.messages(nmessage,:)={'Edges: The excluded distance caused invalid ensembles in an edge check edge distance;' 2 13};
                end

                % Check left edge for zero discharge            
                obj.edges.leftzero=0; 
                leftZero=find(leftQ==0, 1);
                if ~isempty(leftZero)
                    obj.edges.status='warning';
                    nmessage=nmessage+1;
                    obj.edges.messages(nmessage,:)={'EDGES: Left edge has zero Q;' 1 13};
                    obj.edges.leftzero=2;
                end

                % Check right edge for zero discharge
                obj.edges.rightzero=0;
                rightZero=find(rightQ==0, 1);
                if ~isempty(rightZero)
                    obj.edges.status='warning';
                    nmessage=nmessage+1;
                    obj.edges.messages(nmessage,:)={'EDGES: Right edge has zero Q;' 1 13};
                    obj.edges.rightzero=2;
                end

                [~,~,~,~,leftChk,rightChk,~,~]=clsTransectData.checkConsistency(meas.transects);
                % Check left edge for consistent edge type
                obj.edges.leftType=0;
                if strcmp(leftChk,'Varies')
                    obj.edges.status='warning';
                    nmessage=nmessage+1;
                    obj.edges.messages(nmessage,:)= {'EDGES: Left edge type is not consistent;' 1 13};
                    obj.edges.leftType=2;
                end

                % Check right edge for consistent edge type
                obj.edges.rightType=0;
                if strcmp(rightChk,'Varies')
                    obj.edges.status='warning';
                    nmessage=nmessage+1;
                    obj.edges.messages(nmessage,:)={'EDGES: Right edge type is not consistent;' 1 13};
                    obj.edges.rightType=2;
                end
            else
                obj.edges.status='inactive';
            end

        end % edgesQA
        
        function obj=resetProperty(obj,property,setting)
            obj.(property)=setting;
        end  
        
        function obj=updateDataStructure(obj)
            % Update compass qa
            obj.compass.magvar=obj.user.magvar;
            obj.user=rmfield(obj.user,'magvar');
            obj.compass.calPitchIdx=[];
            obj.compass.calRollIdx=[];
            obj.compass.magErrorIdx=[];
            obj.compass.meanPitchIdxC=[];
            obj.compass.meanPitchIdxW=[];
            obj.compass.meanRollIdxC=[];
            obj.compass.meanRollIdxW=[];
            obj.compass.status1=obj.compass.status;
            obj.compass.status2=[];
            obj.compass.stdPitchIdx=[];
            obj.compass.stdRollIdx=[];


            % Update depths qa
            obj.depths.draft=obj.user.draft;
            obj.user=rmfield(obj.user,'draft');
            obj.depths.qTotalCaution=false(size(obj.depths.qRunWarning));
            
            % Update bt qa
            obj.btVel.qTotalCaution=false(size(obj.btVel.qRunWarning));
            
            % Update gga qa
            obj.ggaVel.qTotalCaution=false(size(obj.ggaVel.qRunWarning));
            
            % Update vtg qa
            obj.vtgVel.qTotalCaution=false(size(obj.vtgVel.qRunWarning));

            % Update vtg qa
            obj.wVel.qTotalCaution=false(size(obj.wVel.qRunWarning));

            % Update edges qa
            obj.edges.leftDistMovedIdx=[];
            obj.edges.rightDistMovedIdx=[];
        end % updateFileStructure
                   
    end % methods
    
    methods (Static)
        function [code,messages]=tempQACheck(meas)
        % Function to check the quality of the temperature data
        % 
        % Arguments In:
        % meas  clsMeasurement
        %
        % Arguments Out:
        % code  code
            nTransects=length(meas.transects);
            checked=logical([meas.transects.checked]);
            temp=nan;
            % Construct and array of all temperature data for the
            % measurement
            for n=1:nTransects
                if checked(n)
                    temp=[temp, meas.transects(n).sensors.temperature_degC.(meas.transects(n).sensors.temperature_degC.selected).data];
                end
            end
            
            % Compute the range of temperatures for the measurement
            range=nanmax(temp)-nanmin(temp);
            % Assign a quality code to the temperature stability
            nmessage=0;
            messages={};
            if range > 2
                nmessage=nmessage+1;
                messages(nmessage,:)= {['TEMPERATURE: Temperature range is ', num2str(range,'%3.1f'),' degrees C which is greater than 2 degrees;'] 1 5};
                check(1)=3;            
            elseif range > 1
                check(1)=2;
                nmessage=nmessage+1;
                messages(nmessage,:)= {['Temperature: Temperature range is ', num2str(range,'%3.1f'),' degrees C which is greater than 1 degree;'] 2 5};

            else
                check(1)=1;
            end

            % Assign a quality code based on whether an independent
            % temperature was entered
            if isempty(meas.extTempChk.user)
                nmessage=nmessage+1;
                messages(nmessage,:)= {'Temperature: No independent temperature reading;' 2 5};
                check(2)=2;
            % Assign a quality code based on the difference between the
            % mean ADCP and the independent temperature readings
            elseif isempty(meas.extTempChk.adcp)
                diff=abs(meas.extTempChk.user-nanmean(temp));
                if  diff < 2
                    check(2)=1;
                else
                    nmessage=nmessage+1;
                    messages(nmessage,:)={['TEMP.: The difference between ADCP and reference is > 2:  ',num2str(diff,'%3.1f'),' C;'] 1 5};
                    check(2)=3;
                end
            % Assign a quality code based on the difference between the
            % entered ADCP and the independent temperature readings
            else
                diff=abs(meas.extTempChk.user-meas.extTempChk.adcp);
                if diff < 2
                    check(2)=1;
                else
                    nmessage=nmessage+1;
                    messages(nmessage,:)={['TEMP.: The difference between ADCP and reference is > 2:  ', num2str(diff,'%3.1f'),' C;'] 1 5};
                    check(2)=3;
                end
            end
            % Report the worst code
            code=max(check);
        end % tempQACheck

        function avg=meanQ(obj,prop)
        % Compute mean of specified property of object
            nMax=length(obj);
            data=[];
            % Combine data into single array
            data=[obj(:).(prop)];
            % Compute mean or return zero if no data
            if ~isempty(data)
                avg=nanmean(data);
            else
                avg=0;
            end
        end % meanQ
        
        function cov=covQ(obj,prop)
        % Compute coefficient of variation of specified property of object
            nMax=length(obj);
            % Combine data into single array
            for n=1:nMax
                data(n)=obj(n).(prop);
            end
            % Compute average and standard deviation
            avg=nanmean(data);
            sd=nanstd(data);
            % Compute coefficient of variation
            cov=abs(sd/avg);
        end % covQ    
        
        %function [qInvalidTotal, qInvalidMaxRun, ensInvalid, ensMaxInvalid, ensStartInvalid, ensEndInvalid]=invalidQA(valid,discharge)
        function [qInvalidTotal, qInvalidMaxRun, ensInvalid]=invalidQA(valid,discharge)
        % Function computed the total invalid discharge in ensembles which
        % have invalid data in the specified object. The function also
        % computes the maximum run or cluster of ensembles with the maximum
        % interpolated discharge.
        %
        % INPUT:
        %
        % valid: logical vector of valid ensembles.
        % 
        % discharge: object of class discharge associated with same
        % transect as obj
        %
        % OUTPUT:
        %
        % qInvalidTotal: total interpolated discharge for invalid ensembles
        %
        % qInvalidMaxRun: maximum interpolated discharge for a run of
        % invalid ensembles
        %
        % ensInvalid: total number of invalid ensembles
            
            % Compute the total interpolated discharge for invalid
            % ensembles
            qInvalidTotal=nansum(discharge.middleEns(~valid))+nansum(discharge.topEns(~valid))+nansum(discharge.bottomEns(~valid));

            % Total number of invalid ensembles
            ensInvalid=nansum(~valid);
            
            % Compute the indices of where changes occur
            validRun = find(diff([-1 valid -1]) ~= 0); 
            % Determine length of each run
            runlength = diff(validRun);
            % Determine length of false (0) runs
            runlength0 = runlength(1+(valid(1)==1):2:end);
%             ensMaxInvalid=(nanmax(runlength0)./length(valid)).*100;
          
            % For each run compute the discharge
            nRuns=length(runlength0);
            
%             if nRuns>1
%                 ensStartInvalid=(runlength0(1)./length(valid)).*100;
%                 ensEndInvalid=(runlength0(end)./length(valid)).*100;
%             end
            
            
            if valid(1)==1
                nStart=2;
            else
                nStart=1;
            end
            nEnd=length(validRun)-1;
            if nRuns>1
                m=0;
                for n=nStart:2:nEnd
                    m=m+1;
                    idxStart=validRun(n);
                    idxEnd=validRun(n+1)-1;
                    qInvalidRun(m)=nansum(discharge.middleEns(idxStart:idxEnd))...
                        +nansum(discharge.topEns(idxStart:idxEnd))...
                        +nansum(discharge.bottomEns(idxStart:idxEnd));
                end
                % Determine the maximum discharge in a single run
                qInvalidMaxRun=nanmax(abs(qInvalidRun));
            else
                qInvalidMaxRun=0;
            end
        end 
        
        function [distMovedRight, distMovedLeft, dmg]=edgeDistanceMoved(transect)

            selectedRef=transect.boatVel.selected;
            % Compute processed track
            ensDuration=transect.dateTime.ensDuration_sec;
            if ~isempty(transect.boatVel.(selectedRef))
                boatUProcessed=transect.boatVel.(selectedRef).uProcessed_mps;
                boatVProcessed=transect.boatVel.(selectedRef).vProcessed_mps;
            else
                boatUProcessed=nan(size(transect.boatVel.btVel.uProcessed_mps));
                boatVProcessed=nan(size(transect.boatVel.btVel.vProcessed_mps));
            end
            boatXProcessed=nancumsum(boatUProcessed.*ensDuration);
            boatYProcessed=nancumsum(boatVProcessed.*ensDuration);
            ensembles=1:length(boatUProcessed);
            dmg=sqrt(boatXProcessed(end).^2+boatYProcessed(end).^2);
            % Compute left distance moved
            edgeIdx=clsQComp.edgeEnsembles('left',transect);
            if ~isempty(edgeIdx)
                if strcmpi(transect.startEdge,'left')
                    edgeEnsIdx=1:edgeIdx(end);
                    boatX=boatXProcessed(edgeEnsIdx)-boatXProcessed(edgeEnsIdx(1));
                    boatY=boatYProcessed(edgeEnsIdx)-boatYProcessed(edgeEnsIdx(1));
                    distMovedLeft=sqrt(boatX(end).^2+boatY(end).^2);
                else
                    edgeEnsIdx=edgeIdx(1):ensembles(end);
                    boatX=boatXProcessed(edgeEnsIdx)-boatXProcessed(edgeEnsIdx(end));
                    boatY=boatYProcessed(edgeEnsIdx)-boatYProcessed(edgeEnsIdx(end));
                    distMovedLeft=sqrt(boatX(1).^2+boatY(1).^2);
                end
                
            else
                distMovedLeft=nan;
            end
            
            % Compute right distance moved
            edgeIdx=clsQComp.edgeEnsembles('right',transect);
            if ~isempty(edgeIdx)
                if strcmpi(transect.startEdge,'right')
                    edgeEnsIdx=1:edgeIdx(end);
                    boatX=boatXProcessed(edgeEnsIdx)-boatXProcessed(edgeEnsIdx(1));
                    boatY=boatYProcessed(edgeEnsIdx)-boatYProcessed(edgeEnsIdx(1));
                    distMovedRight=sqrt(boatX(end).^2+boatY(end).^2);
                else
                    edgeEnsIdx=edgeIdx(1):ensembles(end);
                    boatX=boatXProcessed(edgeEnsIdx)-boatXProcessed(edgeEnsIdx(end));
                    boatY=boatYProcessed(edgeEnsIdx)-boatYProcessed(edgeEnsIdx(end));
                    distMovedRight=sqrt(boatX(1).^2+boatY(1).^2);
                end
                
            else
                distMovedRight=nan;
            end  
        end % edgeDistanceMoved
        
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
     
                % Create object
                temp=clsQAData(obj);
                
%                 % Get variable names from structure
%                 names=fieldnames(structIn);
%                 % Set properties to structure values
%                 nMax=length(names);
%                 for n=1:nMax 
%                     if isprop(temp,names{n})
%                         temp=resetProperty(temp, names{n},structIn.(names{n}));
%                     end
%                 end % for n
%                     % Assign property
%                     if ~isempty(find(not(cellfun('isempty',strfind(names,'ensThresholdCaution')))))
%                         temp=resetProperty(temp, 'qTotalThresholdCaution',10);
%                         temp.btVel.qTotalCaution=false(size(structIn.btVel.qTotalWarning));
%                         temp.btVel=rmfield(temp.btVel,'ensInvalidCaution');
%                         temp.ggaVel.qTotalCaution=false(size(structIn.ggaVel.qTotalWarning));
%                         temp.ggaVel=rmfield(temp.ggaVel,'ensInvalidCaution');
%                         temp.vtgVel.qTotalCaution=false(size(structIn.vtgVel.qTotalWarning));
%                         temp.vtgVel=rmfield(temp.vtgVel,'ensInvalidCaution');
%                         temp.wVel.qTotalCaution=false(size(structIn.wVel.qTotalWarning));
%                         temp.wVel=rmfield(temp.wVel,'ensInvalidCaution');    
%                         temp.depths.qTotalCaution=false(size(structIn.depths.qTotalWarning));
%                         temp.depths=rmfield(temp.depths,'ensInvalidCaution');                        
%                     end
%                 
%                 if isfield(temp.user,'magvar')
%                     
%                     % Update compass qa
%                     temp.compass.magvar=temp.user.magvar;
%                     temp.user=rmfield(temp.user,'magvar');
%                     temp.compass.calPitchIdx=[];
%                     temp.compass.calRollIdx=[];
%                     temp.compass.magErrorIdx=[];
%                     temp.compass.meanPitchIdxC=[];
%                     temp.compass.meanPitchIdxW=[];
%                     temp.compass.meanRollIdxC=[];
%                     temp.compass.meanRollIdxW=[];
%                     temp.compass.status1=temp.compass.status;
%                     temp.compass.status2=[];
%                     temp.compass.stdPitchIdx=[];
%                     temp.compass.stdRollIdx=[];
%                     
% 
%                     % Update depths qa
%                     temp.depths.draft=temp.user.draft;
%                     temp.user=rmfield(temp.user,'draft');
%                     
%                     
%                     % Update edges qa
%                     temp.edges.leftDistMovedIdx=[];
%                     temp.edges.rightDistMovedIdx=[];
%                 end
            obj=setProperty(obj,'qa',temp);
        end % reCreate 
    end % static methods
end % class

