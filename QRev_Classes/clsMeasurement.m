classdef clsMeasurement
% This class is a container for all of the other objects and variables
% associated with a single measurement.
%
% Originally developed as part of the QRev processing program
% Programmer: David S. Mueller, Hydrologist, USGS, OSW, dmueller@usgs.gov
% Other Contributors: none
% Latest revision: 3/13/2015
    
    properties 
        stationName % Name of location of measurement
        stationNumber % Station number for measurement
        transects % object of clsTransectData
        mbTests % object of clsMovingBedTest
        sysTest % object of clsPreMeasurement containing system test output
        compassCal % object of clsPreMeasurement containing compass calibration output
        compassEval % object of clsPreMeasurement containing compass evaluation output
        extTempChk % structure that stores manual and ADCP temperature check data
        extrapFit % object of clsComputeExtrap containing all of the data used in determining the extrapolation fit
        processing % variable sets the processing algorithms (SonTek, TRDI, QRev)
        comments % User supplied comments from RSL, WR2, or QRev
        discharge % object of clsQComp
        uncertainty % object of clsUncertainty
        initialSettings % settings as originally loaded from manufacturer data files
        qa % object of clsQAData
        userRating % Rating provided by user
    end % properties
    
    methods
        
        function obj=clsMeasurement(source,varargin)    
        % Creates measurement object
        %
        % INPUT:
        %
        % source: type of data ('SonTek','TRDI','QRev')
        %
        % varargin:
        %   source=TRDI
        %       varargin{1}: full name of mmt file including path
        %       varargin{2}: load only checked files
        %   source=SonTek
        %       varargin{1}: cell array of mat files including path
        %   source=QRev
        %       varargin{1}: full name of QRev.mat file including path
        try
            % Allow creation of object with no input
            if nargin > 0            
                % Initialize premeasurement objects and variables
                obj.sysTest=clsPreMeasurement();
                obj.compassCal=clsPreMeasurement();
                obj.compassEval=clsPreMeasurement();
                obj.extTempChk=struct('user','',...
                                    'adcp','',...
                                    'units','C');
                % Load data
                if strcmpi(source,'TRDI')
                    % Load TRDI ADCP data
                    obj=loadTRDI(obj, varargin{1:2});
                    if length(varargin)>2
                        procType=varargin{3};
                    else
                        procType='QRev';
                    end
                else
                    % Load SonTek ADCP data
                    obj=loadSonTek(obj,varargin{1});
                    if length(varargin)>1
                        procType=varargin{2};
                    else
                        procType='QRev';
                    end
                end % if
                switch obj.transects(1).boatVel.selected
                    case 'btVel'
                        ref='BT';
                    case 'ggaVel'
                        ref='GGA';
                    case 'vtgVel'
                        ref='VTG';
                end
                if ~isempty(obj.mbTests)
                    obj.mbTests=autoUse2Correct(obj.mbTests,ref);
                end
                % Save intial settings
                updateStatus('Save initial settings');
                obj.initialSettings=clsMeasurement.currentSettings(obj);           

                % Set processing
                switch procType
                    case 'QRev'
                        updateStatus('Apply QRev default settings');
                        settings=clsMeasurement.QRevDefaultSettings(obj);
                        settings.processing='QRev';           
                        % Apply settings and compute discharge
                        obj=applySettings(obj,settings);
                    case 'None'
                        updateStatus('Processing with no filters and interpolation')
                        settings=clsMeasurement.NoFilterInterpSettings(obj);
                        settings.processing='None';           
                        % Apply settings and compute discharge
                        obj=applySettings(obj,settings);
                    case 'Original'
                        % Compute discharge
                        obj.discharge=clsQComp(obj);                 
                end
            end % nargin
        catch exception
           msgString=getReport(exception, 'extended','hyperlinks','off');
           errordlg(msgString);
        end
            
        end % constructor
        
        % Load TRDI files
        % ---------------
        function obj = loadTRDI(obj, varargin)
        % Reads TRDI ADCP data from mmt and pd0 files.
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % fileIn: full name of mmt file including path
        %
        % OUTPUT:
        % 
        % obj: object of clsMeasurement

            % Read mmt file
            updateStatus('Reading mmt file');
            mmt=clsMmtTRDI(varargin{1});
            
            % Station name and number
            obj.stationName=mmt.siteInfo.Name;
            if isnan(obj.stationName)
                obj.stationName='';
            end
 
            obj.stationNumber=mmt.siteInfo.Number;
            if isnan(obj.stationNumber)
                obj.stationNumber='';
            end

            % Set processing property
            obj.processing='WR2';

            % Create transect objects for TRDI data
            if length(varargin)>1
                obj.transects=clsTransectData('TRDI',mmt,'Q',varargin{2});
            else
                obj.transects=clsTransectData('TRDI',mmt);
            end

            % Create object for pre-measurement tests
            if isstruct(mmt.qaqc) || isstruct(mmt.mbtTransects)
                obj=qaqcTRDI(obj, mmt);
            end % isstruct
            % Save comments from mmt file in comments
            if ~isnan(mmt.siteInfo.Remarks)
                remark={['MMT Remarks: ',mmt.siteInfo.Remarks]};
                obj=addComment(obj, remark);
            end
            % Save notes from mmt file in comments
            if isfield(mmt.transects,'NoteDate')
                nNotes=length(mmt.transects.NoteDate);
                for k=1:nNotes
                    if ~isempty(mmt.transects.NoteDate{k})
                        notedate=datestr(datenum(mmt.transects.NoteDate{k},'yy/mm/dd HH:MM:SS'),'mm/dd/yyyy HH:MM:SS');
                        note={['File: ',mmt.transects.NoteFileNo{k},' ',notedate, ': ',mmt.transects.NoteText{k}]};
                        obj=addComment(obj,note);
                    end
                end
            end
            
            % Get external temperature
            obj.extTempChk.user=mmt.siteInfo.Water_Temperature; 
            
            % Convert to earth coordinates
            updateStatus('Converting to earth coordinates');
            obj.transects=changeCoordSys(obj.transects,'Earth');
            
            % Set navigation reference
            updateStatus('Setting navigation reference');
            if isfield(mmt.siteInfo,'Reference')
                obj.transects=changeNavReference(obj.transects,0,mmt.siteInfo.Reference);
            else
                obj.transects=changeNavReference(obj.transects,0,'BT');
            end % isfield

            % Set Composite Tracks
            % This feature not currently supported in WinRiver 2, and is
            % turned off in QRev by default.
            
            % Get threshold settings from mmt file
            updateStatus('Applying processing settings');
            obj=thresholdsTRDI(obj, mmt);
            
            % Apply boat interpolations
            updateStatus('Applying filters and interpolation to BT');
            obj.transects=boatInterpolations(obj.transects,0,'BT','None'); 
            
            % Apply GPS interpolations
            if ~isempty([obj.transects.gps])
                updateStatus('Applying filters and interpolation to GPS');
                 obj.transects=boatInterpolations(obj.transects,0,'GPS','HoldLast'); 
            end
            
            obj.transects=updateWater(obj.transects);
            
            % Apply water track filters and interpolations
            updateStatus('Applying filters and interpolation to WT');

            % NOTE: This is where some computation efficiency could be
            % gained by not recomputing each time but applying all filters,
            % interpolating and then computing Q.
            obj.transects=wtFilters(obj.transects,'wtDepth','On');
            obj.transects=wtInterpolations(obj.transects,'Ensembles','None');
            obj.transects=wtInterpolations(obj.transects,'Cells','TRDI');   
            
            % Apply speed of sound adjustment if necessary
            nTransects=length(obj.transects);
            for n=1:nTransects
                mmtSOSMethod=mmt.activeConfig.Proc_Speed_of_Sound_Correction(n);
                if mmtSOSMethod==1
    %                 nTransects=length(obj.transects);
    %                 for n=1:nTransects
                        % Change internal speed of sound to SOS computed from salinity and temperature
    %                     temperature=obj(n).sensors.temperature_degC.(obj(n).sensors.temperature_degC.selected).data;
    %                     obj(n).sensors=setSelected(obj(n).sensors,'salinity_ppt','user');
    %                     salinity=obj(n).sensors.salinity_ppt.(obj(n).sensors.salinity_ppt.selected).data;
    %                     newsos=clsSensors.speedOfSound(temperature,salinity);
    %                     obj(n).sensors.speedOfSound_mps.internal=changeData(obj(n).sensors.speedOfSound_mps.internal,newsos);
    %                     obj(n).sensors.speedOfSound_mps.internal=setSource(obj(n).sensors.speedOfSound_mps.internal,'Computed');                               
                    obj = changeSOS(obj,n,'salinity','user');    
                    obj = changeSOS(obj,n,'sosSrc','Computed',[]);
    %                 end
                end

                if mmtSOSMethod==2
                        obj=changeSOS(obj,n,'sosSrc','user',mmt.activeConfig.Proc_Fixed_Speed_of_Sound(n)); 
                end
            end
            
        end % loadTRDI
        
        function obj = qaqcTRDI(obj, mmt)
        % Processes premeasurement qaqc test, calibrations, and evaluations
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        % 
        % mmt: object of clsMmtTRDI
        %
        % OUTPUT:
        %
        % obj: object of clsMeasurement

            % ADCP Test
            updateStatus('Processing system test');
            if isfield(mmt.qaqc,'RG_Test_TimeStamp')
                obj.sysTest=clsPreMeasurement(mmt.qaqc.RG_Test_TimeStamp,mmt.qaqc.RG_Test,'TST');
            else 
                obj.sysTest=clsPreMeasurement();
            end % isfield

            % Compass calibration
            updateStatus('Processing compass calibrations and evaluations')
            if isfield(mmt.qaqc,'Compass_Cal_Timestamp') 
                obj.compassCal=clsPreMeasurement(mmt.qaqc.Compass_Cal_Timestamp,mmt.qaqc.Compass_Cal_Test, 'TCC');
            else 
                obj.compassCal=clsPreMeasurement();
            end % isfield

            % Compass evaluation
            if isfield(mmt.qaqc,'Compass_Eval_Timestamp') 
                obj.compassEval=clsPreMeasurement(mmt.qaqc.Compass_Eval_Timestamp,mmt.qaqc.Compass_Eval_Test,'TCE');
            else 
                obj.compassEval=clsPreMeasurement();
            end % isfield
            
            % Moving-bed tests
            updateStatus('Processing moving-bed tests');

            % Get filenames from mmt file

            if isstruct(mmt.mbtTransects)            
                fileNames=mmt.('mbtTransects').Files;
                % Getting the pathname is dependent on QRev. This could be
                % changed with a pathname input.
                pathName=getUserPref('Folder');

                % Determine number of files
                numFiles=size(fileNames,1);
                % Determine if any files are missing.
                validFiles=false(1,numFiles);
                for n=1:numFiles
                    fullName=strcat(pathName,fileNames{n});
                    FileInfo=dir(fullName);
                    if ~isempty(FileInfo) > 0
                        if FileInfo.bytes >0
                            validFiles(n)=true;
                        end
                    end
                end
                numValidFiles=sum(validFiles);     
                if numValidFiles>0
                    obj.mbTests=clsMovingBedTests('TRDI',mmt);
                    % Save notes from mmt file in comments
                    if isfield(mmt.mbtTransects,'NoteDate')
                        [r,c]=size(mmt.mbtTransects.NoteDate);
                        for n=1:r    
                            for k=1:c
                                if ~isempty(mmt.mbtTransects.NoteDate{n,k})
                                    notedate=datestr(datenum(mmt.mbtTransects.NoteDate{n,k},'yy/mm/dd HH:MM:SS'),'mm/dd/yyyy HH:MM:SS');
                                    note={['File: ',mmt.mbtTransects.Files{n},' ',notedate, ': ',mmt.mbtTransects.NoteText{n,k}]};
                                    obj=addComment(obj,note);
                                end
                            end
                        end
                    end
                end % if 
            end
        end % qaqcTRDI
        
        function obj = thresholdsTRDI(obj, mmt)
        % Retrieve and apply manual filter settings from mmt file
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        % 
        % mmt: object of clsMmtTRDI
        %
        % OUTPUT:
        %
        % obj: object of clsMeasurement
        
            % WT filter threshold setting
            numBeamWT=clsMeasurement.set3BeamWTThresholdTRDI(mmt);
            diffVelThresholdWT=clsMeasurement.setDiffVelThresholdWTTRDI(mmt);
            vertVelThresholdWT=clsMeasurement.setVertVelThresholdWTTRDI(mmt);

            % BT filter threshold settings
            numBeamBT=clsMeasurement.set3BeamBTThresholdTRDI(mmt);
            diffVelThresholdBT=clsMeasurement.setDiffVelThresholdBTTRDI(mmt);
            vertVelThresholdBT=clsMeasurement.setVertVelThresholdBTTRDI(mmt);

            % Depth filter and averaging settings
            depthWeighting=clsMeasurement.setDepthWeightingTRDI(mmt);
            depthValidMethod='TRDI';
            depthScreening=clsMeasurement.setDepthScreeningTRDI(mmt);

            % Apply settings
            updateStatus('Apply initial settings');
            nTransects=length(obj.transects);
            for n=1:nTransects
                % Apply WT settings
                wtSettings={'Beam',numBeamWT,'Difference','Manual',diffVelThresholdWT,'Vertical','Manual',vertVelThresholdWT};
                obj.transects(n).wVel=applyFilter(obj.transects(n).wVel,obj.transects(n),wtSettings{:});

                % Apply BT settings
                btSettings={'Beam',numBeamBT,'Difference','Manual',diffVelThresholdBT,'Vertical','Manual',vertVelThresholdBT};
                obj.transects(n).boatVel.btVel=applyFilter(obj.transects(n).boatVel.btVel,obj.transects(n),btSettings{:});

                % Apply depth settings
                obj.transects(n).depths=setValidDataMethod(obj.transects(n).depths,depthValidMethod);
                obj.transects(n).depths=depthFilter(obj.transects(n).depths,obj.transects(n),depthScreening);
                obj.transects(n).depths.btDepths=computeAvgBTDepth(obj.transects(n).depths.btDepths,depthWeighting);
                obj.transects(n).depths=compositeDepths(obj.transects(n).depths,obj.transects(n));  
            end % for n

        end % thresholdsTRDI
        
        % Load SonTek files
        % -----------------
        function obj = loadSonTek(obj, filesIn)
        % Reads SonTek ADCP data from *.mat files
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % fileIn: full name of *.mat files of transects including path
        %
        % OUTPUT:
        % 
        % obj: object of clsMeasurement
        
            % Create transect objects for SonTek data
            obj.transects=clsTransectData('SonTek',filesIn);
            
            if  isa(filesIn,'cell')
                numFiles=size(filesIn,2); 
                filesIn=sort(filesIn);       
            else
                numFiles=1;
                fileNames={filesIn};
            end
            RS=clsMatSonTek(filesIn(1));
            names=fieldnames(RS);
            idx=strfind(names,'SiteInfo');
            idx=[idx{:}];
            obj.stationName='';
            obj.stationNumber='';
            if ~isempty(idx)
                if ~isempty(RS.SiteInfo)
                    if isfield(RS.SiteInfo,'Site_Name')
                        % Station name and number
                        if uint8(RS.SiteInfo.Site_Name)~=0
                            obj.stationName=RS.SiteInfo.Site_Name;
                        end
                        if uint8(RS.SiteInfo.Station_Number)~=0
                            obj.stationNumber=RS.SiteInfo.Station_Number;
                        end
                    else
                        % Station name and number
                        if uint8(RS.SiteInfo.('Site Name'))~=0
                            obj.stationName=RS.SiteInfo.('Site Name');
                        end
                        if uint8(RS.SiteInfo.('Station Number'))~=0
                            obj.stationNumber=RS.SiteInfo.('Station Number');
                        end
                    end
                end
            end
            
            % Set processing property
            obj.processing='RSL';

            % Create objects for pre-measurement qaqc
            obj=qaqcSonTek(obj);
            
            % Convert to earth coordinate
            updateStatus('Converting to earth coordinates');
            obj.transects=changeCoordSys(obj.transects,'Earth');

            % Set navigation reference
            updateStatus('Setting navigation reference');
            obj.transects=changeNavReference(obj.transects,0,obj.transects(1).boatVel.selected);
            
            % Set Composite Tracks
            % The RSL setting for composite tracks is not stored in the
            % *.mat output file. Automated analysis of the data could be
            % used to turn composite tracks on automatically. This is
            % planned for a future release.
            
            % Apply boat interpolations
            updateStatus('Applying filters and interpolation to BT');
            obj.transects=boatInterpolations(obj.transects,0,'BT','Hold9');
            
            % Apply GPS interpolations
            if ~isempty([obj.transects.gps])
                updateStatus('Applying filters and interpolation to GPS');
                 obj.transects=boatInterpolations(obj.transects,0,'GPS','None'); 
            end

            % Set depth averaging to simple as QRev default is IDW
            updateStatus('Applying filters and interpolation to depths');
            obj.transects=applyAveragingMethod(obj.transects, 'Simple');
            
            % Depth interpolation
            obj.transects=processDepths(obj.transects,0,'Interpolate','HoldLast');
            
            obj.transects=updateWater(obj.transects);
            
            % Apply water track filters and interpolations
            updateStatus('Applying Filters and Interpolation to WT');
            obj.transects=wtFilters(obj.transects,'wtDepth','On');
            obj.transects=wtInterpolations(obj.transects,'Ensembles','None');
            obj.transects=wtInterpolations(obj.transects,'Cells','TRDI');  

        end % loadSonTek

        function obj = qaqcSonTek(obj)
        % Processes system test and compass calibrations
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % OUTPUT:
        % 
        % obj: object of clsMeasurement

            % Get data path
            % NOTE: This is dependent on QRev. Need to replace by passing path to
            % path to function so that it is independent of QRev.

            prefPath=getUserPref('Folder');
            
            % Get current path
            currentPath=pwd;
            
            % Determine if the CompassCal folder exists   
            if exist([prefPath,'CompassCal'],'dir')==7
                
                updateStatus('Processing compass calibration');
                % Set CompassCal folder
                ccFolder=[prefPath,'CompassCal\'];
                
                % Get listing of all new G3 compass calibrations
                files=dir([ccFolder,'*.ccal']);
                
                % If the list is empty look for pre-G3 calibrations
                if isempty(files)
                    files=dir([ccFolder,'*.txt']);
                    % Process pre-G3 calibrations
                    for n=1:length(files)
                        timeStamp{n}=files(n).name(11:24);
                        test(n)=clsMeasurement.readTextFile([ccFolder,files(n).name]);
                    end % for n
                else
                    % Process G3 calibrations
                    for n=1:length(files)
                        timeStamp{n}=files(n).name(1:19);
                        test(n)=clsMeasurement.readTextFile([ccFolder,files(n).name]);
                    end % for n
                end % if files

                % Create object
                 obj.compassCal=clsPreMeasurement(timeStamp,test,'SCC');
                 
            else
                % Create empty object
                obj.compassCal=clsPreMeasurement();
            end % if exist
            clear test
            % Determine if SystemTest folder exists
            if exist([prefPath,'SystemTest'],'dir')==7
                
                updateStatus('Processing system test');
                % Set system folder
                sysFolder=[prefPath,'SystemTest\'];
                
                % Get listing of *.txt files
                files=dir([sysFolder,'*.txt']);
                
                % Process system test files
                for n=1:length(files)
                    timeStamp{n}=files(n).name(11:24);
                    test(n)=clsMeasurement.readTextFile([sysFolder,files(n).name]);
                end % for n
                
                % Create object
                obj.sysTest=clsPreMeasurement(timeStamp,test,'SST');
                
            else
                % Create empty object
                obj.sysTest=clsPreMeasurement();   
            end % if exist
            
            % Moving-bed tests
            updateStatus('Process moving-bed tests');
            obj=sonTekMovingBedTests(obj);
            
        end % qaqcSonTek

        function obj = sonTekMovingBedTests(obj)
        % Search for and load all appropriately labeled RSL moving-bed tests
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % OUTPUT:
        % 
        % obj: object of clsMeasurement

            % Get data path
            % NOTE: This is dependent on QRev. Need to replace by passing path to
            % path to function so that it is independent of QRev.

            prefPath=getUserPref('Folder');
                      
            % Get listing of all *.mat files
            files=dir([prefPath,'*.mat']);
            
            % Loop through files searching for loop and stationary
            % moving-bed tests
            numFiles=length(files);
            n=0;            
            for j=1:numFiles
                file=files(j).name;
                
                % Loop moving-bed test
                if strcmpi(file(1:4),'Loop')
                    type={'Loop'};
                    n=n+1;
                    % Initialize moving-bed test object, if necessary
                    if isempty(obj.mbTests)
                        obj.mbTests=clsMovingBedTests();
                    end
                    % Create moving-bed test object
                    obj.mbTests(n)=clsMovingBedTests('SonTek',type,{[prefPath,file]});
                    
                % Stationary moving-bed test
                elseif strcmpi(file(1:4),'Smba')
                    type={'Stationary'};
                    n=n+1;
                    % Initialize moving-bed test object, if necessary
                    if isempty(obj.mbTests)
                        obj.mbTests=clsMovingBedTests();
                    end
                    % Create moving-bed test object
                    obj.mbTests(n)=clsMovingBedTests('SonTek',type,{[prefPath,file]});
                end % if strcmp
            end % for j
            if ~isempty(obj.mbTests)
                obj.mbTests=autoUse2Correct(obj.mbTests);
            end
        end % sonTekMovingBedTests
        
        % Load QRev file
        % --------------
        function obj = loadQRev(obj,meas_struct)
        % Create measurement object from structured data input
        
            % Set properties
            obj.stationName=meas_struct.stationName;
            obj.stationNumber=meas_struct.stationNumber; 
            obj.processing=meas_struct.processing;
            if isfield(meas_struct, 'userRating')
                obj.userRating=meas_struct.userRating;
            else
                obj.userRating=[];
            end
            obj.extTempChk=meas_struct.extTempChk;
            obj.comments=meas_struct.comments;
            obj.initialSettings=meas_struct.initialSettings;
            
            % Create object(s)
            obj=clsTransectData.reCreate(obj,meas_struct.transects);
            obj=clsMovingBedTests.reCreate(obj,meas_struct.mbTests);
            obj=clsPreMeasurement.reCreate(obj,meas_struct.sysTest,'sysTest');
            obj=clsPreMeasurement.reCreate(obj,meas_struct.compassCal,'compassCal'); 
            obj=clsPreMeasurement.reCreate(obj,meas_struct.compassEval,'compassEval');
            obj=clsComputeExtrap.reCreate(obj,meas_struct.extrapFit);
            obj=clsQComp.reCreate(obj,meas_struct.discharge);
            obj=clsUncertainty.reCreate(obj,meas_struct.uncertainty);
            obj=clsQAData.reCreate(obj,meas_struct.qa);
          
        end % loadQRev
        
        function obj=setProperty(obj,property,setting)
            obj.(property)=setting;
        end
        
        % Add pre-measurement tests
        % -------------------------
        function obj = addSysTest(obj,fullName)
        % Function to read a file containg the system test.
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % fullName: file name, including path, containing system test
        %
        % OUTPUT:
        % 
        % obj: object of clsMeasurement
        
            % Open and read file
            fid=fopen(fullName,'r');
            dataIn=fread(fid);
            
            % Parse special characters
            dataIn=dataIn(dataIn>10);
            
            % Convert to string
            test={char(dataIn)'}; 
            
            % Close file
            fclose(fid);
            
            % Determine file name from the full name
            idx=find(fullName=='\',1,'last');
            if isempty(idx)
                idx=0;
            end
            fileName=fullName(idx+1:end-4);
            n=length(obj.sysTest);
            
            if strcmp (obj.transects(1).adcp.manufacturer,'SonTek')
               dataType='SST';
            else
                dataType='TST';
            end
            
            % Create object using filename as timeStamp
            if isempty(obj.sysTest(n).timeStamp)
                obj.sysTest=clsPreMeasurement({fileName},test,dataType);
            else  
                obj.sysTest(n+1)=clsPreMeasurement({fileName},test,dataType);
            end
            obj.qa=systemTestQA(obj.qa,obj);
        end % addSysTest
        
        function obj = addCompassCal(obj,fullName)
        % Function to read a file for the compass calibration.
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % fullName: full name, including path, of file containing compass
        % calibration
        %
        % OUTPUT:
        % 
        % obj: object of clsMeasurement  
        
            % Open and read file
            fid=fopen(fullName,'r');
            dataIn=fread(fid);
            
            % Eliminate special characters
            dataIn=dataIn(dataIn>10);
            
            % Convert to string
            test={char(dataIn)'};   
            
            % Close file
            fclose(fid);
            
            % Determine filename
            idx=find(fullName=='\',1,'last');
            if isempty(idx)
                idx=0;
            end
            fileName=fullName(idx+1:end-4);
            n=length(obj.compassCal);
            
            if strcmp (obj.transects(1).adcp.manufacturer,'SonTek')
               dataType='SCC';
            else
                dataType='TCC';
            end            
            % Create object using filename as timeStamp
            if isempty(obj.compassCal(n).timeStamp)
                obj.compassCal=clsPreMeasurement({fileName},test,dataType);
            else
                obj.compassCal(n+1)=clsPreMeasurement({fileName},test,dataType);
            end
            obj.qa=compassQA(obj.qa,obj);
        end % addCompassCal
        
        function obj = addCompassEval(obj,fullName)
        % Function to read a file for the compass evaluation test.
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % fullName: full name, including path, of file containing compass
        % calibration
        %
        % OUTPUT:
        % 
        % obj: object of clsMeasurement 
        
            % Open and read file
            fid=fopen(fullName,'r');
            dataIn=fread(fid);
            
            % Eliminate special characters
            dataIn=dataIn(dataIn>10);
            
            % Convert to string
            test={char(dataIn)'};  
            
            % Close file
            fclose(fid);
            
            % Determine filename
            idx=find(fullName=='\',1,'last');
            if isempty(idx)
                idx=0;
            end
            fileName=fullName(idx+1:end-4);
            
            if strcmp (obj.transects(1).adcp.manufacturer,'SonTek')
                dataType='SCC';
            else
                dataType='TCE';
            end
            
            % Create object using filename for timeStamp
            if isempty(obj.compassEval.timeStamp)
                obj.compassEval=clsPreMeasurement({fileName},test,dataType);
            else
                n=length(obj.compassEval);
                obj.compassEval(n+1)=clsPreMeasurement({fileName},test,dataType);
            end
            obj.qa=compassQA(obj.qa,obj);
        end % addCompassEval
        
        function obj = addMovingBedTest (obj,source,fullName,varargin)
        % Allows adding a moving-bed test. Test must be in an mmt file for
        % TRDI data. Test can be any *.mat file generated from
        % RiverSurveyor Live for SonTek data.
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % source: manufacturer of ADCP for moving-bed test (TRDI, SonTek)
        %
        % fullName: full name, including path, of *.mat or *.mmt file
        %
        % varargin:
        %   varargin{1}: type of moving-bed test
        %
        % OUTPUT:
        % 
        % obj: object of clsMeasurement         
            
            % Determine source of moving-bed test and create object
            % appropriately
            if strcmp(source,'TRDI')
                mmtMB=clsMmtTRDI(fullName);
                obj.mbTests=clsMovingBedTests('TRDI',mmtMB);
            else
                obj.mbTests=clsMovingBedTests('SonTek',varargin{:},fullName);
            end
            obj.qa=movingbedQA(obj.qa,obj);
        end % addMovingBedTest
        
        % Measurement changes and updates
        % -------------------------------
        function obj = changeExtrapolation(obj)
        % Function to change extrapolation methods
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % OUTPUT:
        % 
        % obj: object of clsMeasurement 
        
            % Determine number of transects
            nTransects=length(obj.transects);
            
            % Apply the selected method from extrapFit to each transect.
            for n=1:nTransects
                top=obj.extrapFit.selFit(end).topMethod;
                bot=obj.extrapFit.selFit(end).botMethod;
                exp=obj.extrapFit.selFit(end).exponent;
                obj.transects(n).extrap=setExtrapData(obj.transects(n).extrap,top,bot,exp);
            end % for
        end % changeExtrapolation
        
        function obj = updateExtrap(obj)
        % Updates the extrapolation methods for all transects
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % OUTPUT:
        % 
        % obj: object of clsMeasurement
            try
                % Update profiles
                obj.extrapFit=processProfiles(obj.extrapFit,obj.transects,'q');
            catch exception
               msgString=getReport(exception, 'extended','hyperlinks','off');
               errordlg(msgString,'clsMeasurement Error');
            end            
        end % updateExtrap
        
        function obj = setManualExtrap(obj,top,bot,exp)
            nTransects=length(obj.transects);
            for n=1:nTransects
                obj.transects(n).extrap=setExtrapData(obj.transects(n).extrap,top,bot,exp);
                obj.extrapFit=changeFitMethod(obj.extrapFit,obj.transects,'Manual',n,'All',{top},{bot},exp);
            end       
            obj.extrapFit=changeFitMethod(obj.extrapFit,obj.transects,'Manual',nTransects+1,'All',{top},{bot},exp);
        end % setManualExtrap
        
        function obj = changeDraft(obj,draft,varargin)
        % Applies a change in magnetic variation to one or all transects
        % and update the discharge and uncertaint computations.
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % draft: new draft
        %
        % varargin: transect number to apply change to, if empty change is
        % applied to all transects.
        %
        % OUTPUT:
        %
        % obj: object of clsMeasurement
        try
            % Get current settings
            s=clsMeasurement.currentSettings(obj);
           
            if isempty(varargin)
                % Apply change to all transects
                obj.transects=changeDraft(obj.transects,draft);
                % Update data, discharge, and uncertainty
                obj=applySettings(obj,s);
            else
                obj.transects(varargin{:})=changeDraft(obj.transects(varargin{:}),draft);
                obj=applySettings(obj,s,varargin{:});
            end
        catch exception
           msgString=getReport(exception, 'extended','hyperlinks','off');
           errordlg(msgString,'clsMeasurement Error');
        end    
        end % changeDraft
        
        function obj = changeMagVar(obj,magVar,varargin)
        % Applies a change in magnetic variation to one or all transects
        % and update the discharge and uncertaint computations.
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % magVar: new magnetic variation
        %
        % varargin: transect number to apply change to, if empty change is
        % applied to all transects.
        %
        % OUTPUT:
        %
        % obj: object of clsMeasurement
        try
            % Get current settings
            s=clsMeasurement.currentSettings(obj);
            nTransects=length(obj.transects);
            n=1;
            recompute=false;
            while n<=nTransects && recompute==false
                if strcmp({obj.transects(n).sensors.heading_deg.selected},'internal');
                    recompute=true;
                end
                n=n+1;
            end
            if isempty(varargin)
                % Apply change to all transects
                obj.transects=changeMagVar(obj.transects,magVar);
                if recompute
                    % Update data, discharge, and uncertainty
                    obj=applySettings(obj,s);
                end
            else
                obj.transects(varargin{:})=changeMagVar(obj.transects(varargin{:}),magVar);
                if recompute
                    obj=applySettings(obj,s,varargin{:});
                end
            end
        catch exception
           msgString=getReport(exception, 'extended','hyperlinks','off');
           errordlg(msgString,'clsMeasurement Error');
        end    
        end % changeMagVar
        
        function obj = changeOffset(obj,HOffset,varargin)
        % Applies a change in heading offset to one or all transects
        % and update the discharge and uncertaint computations.
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % HOffset: new heading offset
        %
        % varargin: transect number to apply change to, if empty change is
        % applied to all transects.
        %
        % OUTPUT:
        %
        % obj: object of clsMeasurement
        try
            % Get current settings
            s=clsMeasurement.currentSettings(obj);
            nTransects=length(obj.transects);
            n=1;
            recompute=false;
            while n<=nTransects && recompute==false
                if strcmp({obj.transects(n).sensors.heading_deg.selected},'external');
                    recompute=true;
                end
                n=n+1;
            end
            if isempty(varargin)
                % Apply change to all transects
                obj.transects=changeOffset(obj.transects,HOffset);
                if recompute
                    % Update data, discharge, and uncertainty
                    obj=applySettings(obj,s);
                end
            else
                obj.transects(varargin{:})=changeOffset(obj.transects(varargin{:}),HOffset);
                if recompute
                    obj=applySettings(obj,s,varargin{:});
                end
            end
        catch exception
           msgString=getReport(exception, 'extended','hyperlinks','off');
           errordlg(msgString,'clsMeasurement Error');
        end    
        end % changeOffset
        
        function obj = changeHeadingSource(obj,HSource,varargin)
            % Get current settings
            s=clsMeasurement.currentSettings(obj);
            if isempty(varargin)
                % Apply change to all transects
                obj.transects=changeHeadingSource(obj.transects,HSource);
                % Update data, discharge, and uncertainty
                obj=applySettings(obj,s);
            else
                obj.transects(varargin{:})=changeHeadingSource(obj.transects(varargin{:}),HSource);
                obj=applySettings(obj,s,varargin{:});
            end
            
        end % changeHeadingSource
        
        function obj = changeSOS(obj,varargin)
        % Applies a change in speed of sound to one or all transects
        % and update the discharge and uncertaint computations.
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % varargin: 
        %   {1}: optional, transect number to apply change to, if empty change is
        % applied to all transects.
        %   {1/2}: sensor property to change
        %
        %   {2/3}: setting for sensor property
        %
        % OUTPUT:
        %
        % obj: object of clsMeasurement   
            try
                % Get current settings
                s=clsMeasurement.currentSettings(obj); 

                % Check to see if transect number specified
                 if isnumeric(varargin{1});
                     % Process only the specified transect
                     obj.transects(varargin{1})=changeSOS(obj.transects(varargin{1}),varargin{2:end});
                 else
                    % Process all transects
                    obj.transects=changeSOS(obj.transects,varargin{1},varargin{2:end}); 
                 end % is numeric
                obj=applySettings(obj,s);
            catch exception
               msgString=getReport(exception, 'extended','hyperlinks','off');
               errordlg(msgString,'clsMeasurement Error');
            end
        end
        
        function obj = applySettings(obj,s,varargin)
        % Applies reference, filter, and interpolation settings
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % s: data structure of reference, filter, and interpolation
        % settings
        %
        % varargin: transect number to apply settings to only 1 transects.
        % If empty settings are applied to all transects.
        %
        % OUTPUT:
        % 
        % obj: object of clsMeasurement 
            try    
%                 if isempty(varargin)
                    transects=obj.transects;
%                 else
%                     transects=obj.transects(varargin{:});
%                 end
                nTransects=length(transects);

                % Moving-boat ensembles
                if isfield(s,'processing')
                    if ~strcmpi(s.processing,obj.processing)
                        transects=changeQEnsembles(transects,s.processing);
                        obj.processing=s.processing;
                    end
                end



                % Navigation reference
                if ~strcmpi(obj.transects(1).boatVel.selected,s.NavRef)
                    transects=changeNavReference(transects,0,s.NavRef);
                    if ~isempty(obj.mbTests)
                        obj.mbTests=autoUse2Correct(obj.mbTests,s.NavRef);
                    end
                end

                % Composite tracks
                % Changing the nav reference applies the current setting for
                % composite tracks, check to see if a change is needed.
                if ~strcmpi(obj.transects(1).boatVel.composite,s.CompTracks)
                    transects=compositeTracks(transects,0,s.CompTracks);
                end


                % Set difference velocity BT filter
                if strcmpi(s.BTdFilter,'Manual')
                    BTdFilter={s.BTdFilter,s.BTdFilterThreshold};
                else
                    BTdFilter={s.BTdFilter};
                end

                % Set vertical velocity  BTfilter
                if strcmpi(s.BTwFilter,'Manual')
                    BTwFilter={s.BTwFilter,s.BTwFilterThreshold};
                else
                    BTwFilter={s.BTwFilter};
                end

                % Apply BT settings
                btSettings={'Beam',s.BTbeamFilter,'Difference',BTdFilter{:},'Vertical',BTwFilter{:},'Other',s.BTsmoothFilter};
                transects=boatFilters(transects,0,btSettings{:});

                % BT Interpolation
                transects=boatInterpolations(transects,0,'BT',s.BTInterpolation);

                % GPS filter settings
                if ~isempty([transects.gps])

                    % GGA
                    if isprop(obj.transects(1).boatVel,'ggaVel')

                        % Set GGA altitude filter
                        if strcmpi(s.ggaAltitudeFilter,'Manual')
                            ggaAltitudeFilter={s.ggaAltitudeFilter,s.ggaAltitudeFilterChange};
                        else
                            ggaAltitudeFilter={s.ggaAltitudeFilter};
                        end

                        % Set GGA HDOP filter
                        if strcmpi(s.GPSHDOPFilter,'Manual')
                            ggaHDOPFilter={s.GPSHDOPFilter,s.GPSHDOPFilterMax,s.GPSHDOPFilterChange};
                        else
                            ggaHDOPFilter={s.GPSHDOPFilter};
                        end

                        % Apply GGA filters
                        ggaSettings={'Differential',s.ggaDiffQualFilter,'Altitude',ggaAltitudeFilter{:},'HDOP',ggaHDOPFilter{:},'Other',s.GPSSmoothFilter};
                        transects=gpsFilters(transects,0,ggaSettings{:});
                    end

                    % VTG
                    if isprop(obj.transects(1).boatVel,'vtgVel')

                        % Set VTG HDOP filter
                        if strcmpi(s.GPSHDOPFilter,'Manual')
                            vtgSettings={'HDOP',s.GPSHDOPFilter,s.GPSHDOPFilterMax,s.GPSHDOPFilterChange,'Other',s.GPSSmoothFilter};
                        else
                            vtgSettings={'HDOP',s.GPSHDOPFilter,'Other',s.GPSSmoothFilter};
                        end

                        % Apply VTG filters
                        transects=gpsFilters(transects,0,vtgSettings);
                    end

                    % GPS Interpolation
                    transects=boatInterpolations(transects,0,'GPS',s.GPSInterpolation);
                end

                % Set depth reference
                transects=setDepthReference(transects,0,s.depthReference);

                % Apply depth settings and 
                % update water data with navigation and depth data changes
                transects=processDepths(transects,1,'Filter',s.depthFilterType,...
                    'Interpolate',s.depthInterpolation,...
                    'Composite',s.depthComposite,'AvgMethod',s.depthAvgMethod,'ValidMethod',s.depthValidMethod);  

                % Set WT difference velocity filter
                if strcmpi(s.WTdFilter,'Manual')
                    WTdFilter={s.WTdFilter,s.WTdFilterThreshold};
                else
                    WTdFilter={s.WTdFilter};
                end

                % Set WT vertical velocity filter
                if strcmpi(s.WTwFilter,'Manual')
                    WTwFilter={s.WTwFilter,s.WTwFilterThreshold};
                else
                    WTwFilter={s.WTwFilter};
                end

                % Apply WT settings
                wtSettings={'Beam',s.WTbeamFilter,'Difference',WTdFilter{:},'Vertical',WTwFilter{:},'Other',s.WTsmoothFilter,'SNR',s.WTsnrFilter,'wtDepth',s.WTwtDepthFilter,'Excluded',s.WTExcludedDistance};    
                transects=wtFilters(transects,wtSettings{:});

                % Recompute extrapolations
                % NOTE: Extrapolations should be determined prior to WT
                % interpolations because the TRDI approach for power/power
                % using the power curve and exponent to estimate invalid cells.
                if isempty(obj.extrapFit) || strcmpi(obj.extrapFit.fitMethod,'Automatic') 
                    obj.extrapFit=clsComputeExtrap(transects,0);
                    top=obj.extrapFit.selFit(end).topMethod;
                    bot=obj.extrapFit.selFit(end).botMethod;
                    exp=obj.extrapFit.selFit(end).exponent;
                    transects=setExtrapolation(transects,top,bot,exp);
                else
                    if ~isfield(s,'extrapTop')
                        s.extrapTop=obj.extrapFit.selFit(end).topMethod;
                        s.extrapBot=obj.extrapFit.selFit(end).botMethod;
                        s.extrapExp=obj.extrapFit.selFit(end).exponent;
                    end
                    transects=setExtrapolation(transects,s.extrapTop,s.extrapBot,s.extrapExp);
                    for n=1:nTransects
                        obj.extrapFit=changeFitMethod(obj.extrapFit,transects,'Manual',n,'All',s.extrapTop,s.extrapBot,s.extrapExp);
                    end       
                    obj.extrapFit=changeFitMethod(obj.extrapFit,transects,'Manual',nTransects+1,'All',s.extrapTop,s.extrapBot,s.extrapExp);

                end

                % WT Interpolation
                transects=wtInterpolations(transects,'Ensembles',s.WTEnsInterpolation);
                transects=wtInterpolations(transects,'Cells',s.WTCellInterpolation);

                % Edge Methods
                for n=1:nTransects
                    transects(n).edges=changeProperty(transects(n).edges,'velMethod',s.edgeVelMethod);
                    transects(n).edges=changeProperty(transects(n).edges,'recEdgeMethod',s.edgeRecEdgeMethod);
                end

%                 if isempty(varargin)
                    obj.transects=transects;
%                 else
%                     obj.transects(varargin{:})=transects;
%                 end

                % Update sensitivities for water track interpolations
                obj.extrapFit=updateQSensitivity(obj.extrapFit,obj.transects);

                % Compute discharge
                obj.discharge=clsQComp(obj);

                % Assess measurement quality
                obj.qa=clsQAData(obj);

                % Compute uncertainty
                obj.uncertainty=clsUncertainty(obj);

                % Complete quality assessment
                obj.qa=clsQAData(obj);

            catch exception
               msgString=getReport(exception, 'extended','hyperlinks','off');
               errordlg(msgString,'clsMeasurement Error');
            end            
        end % applySettings
        
        function obj=updateQ(obj)
        % Updates the discharge, apply changes to measurement data.
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % OUTPUT:
        % 
        % obj: object of clsMeasurement
            try
                % Update meas object for changes in extrapolation method that
                % may occur due to changes in the meas object
                obj=updateExtrap(obj);

                % Compute discharge using update meas object
                obj.discharge=clsQComp(obj);
            catch exception
               msgString=getReport(exception, 'extended','hyperlinks','off');
               errordlg(msgString,'clsMeasurement Error');
            end
        end % updateQ
        
        function obj = addComment(obj,comment)
        % Adds a user comment to the measurement
        %
        % INPUT:
        %
        % obj: object of clsMeasurement
        %
        % comment: user provided comment string
        %
        % OUTPUT:
        % 
        % obj: object of clsMeasurement 
        
            nComments=length(obj.comments);
            obj.comments{nComments+1}=comment;
        end % addComment
        
        function obj = changeStationName(obj,name)
            obj.stationName=name;
            obj.qa=userQA(obj.qa,obj);
        end % changeStationName
        
        function obj = changeStationNumber(obj,number)
            obj.stationNumber=number;
            obj.qa=userQA(obj.qa,obj);
        end % changeStationNumber
        
        function obj = movingBedTestChange(obj,prop,setting)
            if strcmp(prop,'use2Correct')
                obj.mbTests=setUse(obj.mbTests,setting);
            elseif strcmp(prop,'userValid')
                obj.mbTests=setValid(obj.mbTests,setting);
                obj.mbTests=autoUse2Correct(obj.mbTests,obj.transects(1).wVel.navRef);
            elseif strcmp(prop,'manualOverride')
                obj.mbTests(setting)=setUse(obj.mbTests(setting),true);
                obj.mbTests(setting)=setProperty(obj.mbTests(setting),'movingBed','Yes');
                obj.mbTests(setting)=setProperty(obj.mbTests(setting),'selected',true);
                obj.mbTests(setting)=setProperty(obj.mbTests(setting),'testQuality','Manual');
                obj.mbTests=autoUse2Correct(obj.mbTests,obj.transects(1).wVel.navRef);
            elseif strcmp(prop,'manualCancel')
                obj.mbTests(setting)=setUse(obj.mbTests(setting),false);
                obj.mbTests(setting)=setProperty(obj.mbTests(setting),'movingBed','Unknown');
                obj.mbTests(setting)=setProperty(obj.mbTests(setting),'selected',false);
                obj.mbTests(setting)=setProperty(obj.mbTests(setting),'testQuality','Errors');  
                obj.mbTests=autoUse2Correct(obj.mbTests,obj.transects(1).wVel.navRef);
            end
            obj.discharge=clsQComp(obj);
            obj.qa=movingbedQA(obj.qa,obj);
            obj.uncertainty=clsUncertainty(obj);
        end % movingBedTestChange
        
        function obj=autoUse2Correct(obj)
            obj.mbTests=autoUse2Correct(obj.mbTests,obj.transects(1).wVel.navRef);
            obj.discharge=clsQComp(obj);
            obj.qa=movingbedQA(obj.qa,obj);
            obj.uncertainty=clsUncertainty(obj);
        end
        
        function obj=updateDataStructure(obj)
            obj.qa=updateDataStructure(obj.qa);
            obj.transects=updateDataStructure(obj.transects);
        end % updateFileStructure
        
    end % methods 
    
    methods (Static)
        
        function dataOut=readTextFile(fileName)
        % Reads an ASCII text file and returns data
        %
        % INPUT: 
        %
        % fileName: file name, including path, of text file
        %
        % OUTPUT:
        %
        % dataOut: string of text from file with special characters
        % removed

            % Open and read file
            fid=fopen(fileName,'r');
            dataIn=fread(fid);
            
            % Eliminate special characters
            dataIn(dataIn==9)=32;
            dataIn=dataIn(dataIn>10);
            
            % Convert to string
            dataOut={char(dataIn)'};           
            
            % Close file
            fclose(fid);
        end % readTextFile
        
        % TRDI settings from mmt
        % ----------------------
        function numBeamWTOut=set3BeamWTThresholdTRDI(mmt)
        % Get 3-beam processing for WT from mmt file
        %
        % INPUT:
        %
        % mmt: object of clsMmtTRDI
        %
        % OUTPUT:
        %
        % numBeamWTOut: minimum number of allowable beams for WT
        
            % Check consistency of 3-Beam WT Solutions
            use3BeamWT=mmt.activeConfig.Proc_Use_3_Beam_WT;
                        
            % Use setting from 1st transect for all transects
            switch use3BeamWT(1)
                case 0
                    numBeamWTOut=4;
                case 1
                    numBeamWTOut=3;
            end % switch
        end % use3BeamWTThresholdTRDI

        function numBeamBTOut=set3BeamBTThresholdTRDI(mmt)
        % Get 3-beam processing for BT from mmt file
        %
        % INPUT:
        %
        % mmt: object of clsMmtTRDI
        %
        % OUTPUT:
        %
        % numBeamBTOut: minimum number of allowable beams for BT
        
            % Check consistency of 3-Beam BT Solutions
            use3BeamBT=mmt.activeConfig.Proc_Use_3_Beam_BT;
            
            % Use setting from 1st transect for all transects
            switch use3BeamBT(1)
                case 0
                    numBeamBTOut=4;
                case 1
                    numBeamBTOut=3;
            end % switch
        end % use3BeamBTThresholdTRDI

        function diffVelThresholdWTOut=setDiffVelThresholdWTTRDI(mmt)
        % Get difference (error) velocity threshold for WT from mmt
        %
        % INPUT:
        %
        % mmt: object of clsMmtTRDI
        %
        % OUTPUT:
        %
        % diffVelThresholdWTOut: difference velocity threshold (m/s)
        
            % Check consistency of difference (error) velocity for WT
            diffVelThresholdWT=mmt.activeConfig.Proc_WT_Error_Vel_Threshold;

            % Use setting from 1st transect for all transects
            diffVelThresholdWTOut=diffVelThresholdWT(1);
        end % setDiffVelThresholdWTTRDI

        function diffVelThresholdBTOut=setDiffVelThresholdBTTRDI(mmt)
        % Get difference (error) velocity threshold for BT from mmt
        %
        % INPUT:
        %
        % mmt: object of clsMmtTRDI
        %
        % OUTPUT:
        %
        % diffVelThresholdBTOut: difference velocity threshold (m/s)
        
            % Check consistency of difference (error) velocity for BT
            diffVelThresholdBT=mmt.activeConfig.Proc_BT_Error_Vel_Threshold;
            
%             % Warn if setting is not consistent for all transects
%             if abs(sum(diff(diffVelThresholdBT)))>0.009
%                 warndlg(['The processing setting for using the difference (error) velocity for ', ...
%                         'bottom track is not consistent. QRev will only use the value',...
%                             'from the 1st transect.'],'Difference (error) Velocity BT WARNING');
%             end % diffVelThresholdBT 
            
            % Use setting from 1st transect for all transects
            diffVelThresholdBTOut=diffVelThresholdBT(1);
        end % setDiffVelThresholdBTTRDI

        function vertVelThresholdWTOut=setVertVelThresholdWTTRDI(mmt)
        % Get the vertical velocity threshold for WT from mmt 
        %
        % INPUT:
        %
        % mmt: object of clsMmtTRDI
        %
        % OUTPUT:
        %
        % vertVelThresholdWTOut: vertical velocity threshold (m/s)   
        
            % Check consistency of vertical velocity for WT
            vertVelThresholdWT=mmt.activeConfig.Proc_WT_Up_Vel_Threshold;
            
            % Use setting from 1st transect for all transects
            vertVelThresholdWTOut=vertVelThresholdWT(1);
        end % setVertVelThresholdWTTRDI

        function vertVelThresholdBTOut=setVertVelThresholdBTTRDI(mmt)
        % Set the vertical velocity threshold for BT  
        %
        % INPUT:
        %
        % mmt: object of clsMmtTRDI
        %
        % OUTPUT:
        %
        % vertVelThresholdBTOut: vertical velocity threshold (m/s) 
        
            % Check consistency of vertical velocity for BT
            vertVelThresholdBT=mmt.activeConfig.Proc_BT_Up_Vel_Threshold;

            % Use setting from 1st transect for all transects
            vertVelThresholdBTOut=vertVelThresholdBT(1);
        end % setVertVelThresholdBTTRDI

        function depthWeightingSetting=setDepthWeightingTRDI(mmt)
        % Get the average depth method from mmt
        %
        % INPUT:
        %
        % mmt: object of clsMmtTRDI
        %
        % OUTPUT:
        %
        % depthWeightingSetting: method for computing average depth 
        
            % Check consistency of depth averaging method
            depthWeighting=mmt.activeConfig.Proc_Use_Weighted_Mean;
            
            % Warn if setting is not consistent for all transects
            if abs(sum(diff(depthWeighting)))>0.1
                    depthWeightingSetting='IDW';
            else
                % Assign method
                switch depthWeighting(1)
                    case 0
                        depthWeightingSetting='Simple';
                    case 1
                        depthWeightingSetting='IDW';
                end % switch
            end % depthWeighting  
        end % setDepthWeightingTRDI

        function depthScreeningSetting=setDepthScreeningTRDI(mmt)
        % Get the depth screening setting from mmt
        %
        % INPUT:
        %
        % mmt: object of clsMmtTRDI
        %
        % OUTPUT:
        %
        % depthScreeningSetting: depth screening setting
        
            % Check consistency of depth screening 
            depthScreen=mmt.activeConfig.Proc_Screen_Depth;
            
            % Warn if setting is not consistent for all transects
            if abs(sum(diff(depthScreen)))>0.1
%                 warndlg(['The processing setting for the depth screening ', ...
%                         'is not consistent. QRev will use turn depth screening on.'],...
%                         'Depth Screening WARNING');
                    depthScreeningSetting='TRDI';
            else
                % Assign setting
                if depthScreen(1)
                    depthScreeningSetting='TRDI';
                else
                    depthScreeningSetting='None';
                end
            end % depthScreening
        end % setDepthScreeningTRDI   
        
        % Process, Filter, and Interpolation Settings
        % -------------------------------------------
        function settings = currentSettings(obj)
        % Saves the current settings for a measurement. Since all
        % settings in QRev are consistent among all transects in a
        % measurement only the settings from the first transect are saved.
        % 
        % INPUT:
        % 
        % obj: object of clsMeasurement
        %
        % OUTPUT:
        %
        % settings: data structure with settings
            checked=[obj.transects.checked];
            firstIdx=find(checked==1,1,'first');
        
            % Navigation reference
            settings.NavRef=obj.transects(firstIdx).boatVel.selected;
            
            % Composite tracks
            settings.CompTracks=obj.transects(firstIdx).boatVel.composite;
            
            % Water track settings
            settings.WTbeamFilter=obj.transects(firstIdx).wVel.beamFilter;
            settings.WTdFilter=obj.transects(firstIdx).wVel.dFilter;
            settings.WTdFilterThreshold=obj.transects(firstIdx).wVel.dFilterThreshold;
            settings.WTwFilter=obj.transects(firstIdx).wVel.wFilter;
            settings.WTwFilterThreshold=obj.transects(firstIdx).wVel.wFilterThreshold;
            settings.WTsmoothFilter=obj.transects(firstIdx).wVel.smoothFilter;
            settings.WTsnrFilter=obj.transects(firstIdx).wVel.snrFilter;
            settings.WTwtDepthFilter=obj.transects(firstIdx).wVel.wtDepthFilter;
            settings.WTEnsInterpolation=obj.transects(firstIdx).wVel.interpolateEns;
            settings.WTCellInterpolation=obj.transects(firstIdx).wVel.interpolateCells; 
            settings.WTExcludedDistance=obj.transects(firstIdx).wVel.excludedDist;
            
            % Bottom track settings
            settings.BTbeamFilter=obj.transects(firstIdx).boatVel.btVel.beamFilter;
            settings.BTdFilter=obj.transects(firstIdx).boatVel.btVel.dFilter;
            settings.BTdFilterThreshold=obj.transects(firstIdx).boatVel.btVel.dFilterThreshold;
            settings.BTwFilter=obj.transects(firstIdx).boatVel.btVel.wFilter;
            settings.BTwFilterThreshold=obj.transects(firstIdx).boatVel.btVel.wFilterThreshold;
            settings.BTsmoothFilter=obj.transects(firstIdx).boatVel.btVel.smoothFilter;
            settings.BTInterpolation=obj.transects(firstIdx).boatVel.btVel.interpolate;
            
            % GPS Settings
            if ~isempty([obj.transects.gps])
                % GGA settings
                if isprop(obj.transects(firstIdx).boatVel,'ggaVel')
                    if ~isempty(obj.transects(firstIdx).boatVel.ggaVel)
                        settings.ggaDiffQualFilter=obj.transects(firstIdx).boatVel.ggaVel.gpsDiffQualFilter;
                        settings.ggaAltitudeFilter=obj.transects(firstIdx).boatVel.ggaVel.gpsAltitudeFilter;
                        settings.ggaAltitudeFilterChange=obj.transects(firstIdx).boatVel.ggaVel.gpsAltitudeFilterChange;
                        settings.GPSHDOPFilter=obj.transects(firstIdx).boatVel.ggaVel.gpsHDOPFilter;
                        settings.GPSHDOPFilterMax=obj.transects(firstIdx).boatVel.ggaVel.gpsHDOPFilterMax;
                        settings.GPSHDOPFilterChange=obj.transects(firstIdx).boatVel.ggaVel.gpsHDOPFilterChange;
                        settings.GPSSmoothFilter=obj.transects(firstIdx).boatVel.ggaVel.smoothFilter;
                        settings.GPSInterpolation=obj.transects(firstIdx).boatVel.ggaVel.interpolate;
                    else
                        settings.ggaDiffQualFilter=1;
                        settings.ggaAltitudeFilter='Off';
                        settings.ggaAltitudeFilterChange=[];

                        settings.ggasmoothFilter=[];
                        if ~isfield(settings,'GPSInterpolation')
                            settings.GPSInterpolation='None'; 
                        end
                        if ~isfield(settings,'GPSHDOPFilter')
                            settings.GPSHDOPFilter='Off';
                            settings.GPSHDOPFilterMax=[];
                            settings.GPSHDOPFilterChange=[]; 
                        end
                        if ~isfield(settings,'GPSSmoothFilter')
                            settings.GPSSmoothFilter='Off';
                        end
                    end
                        
                end
                % VTG settings
                if isprop(obj.transects(firstIdx).boatVel,'vtgVel')
                    if ~isempty(obj.transects(firstIdx).boatVel.vtgVel)
                        settings.GPSHDOPFilter=obj.transects(firstIdx).boatVel.vtgVel.gpsHDOPFilter;
                        settings.GPSHDOPFilterMax=obj.transects(firstIdx).boatVel.vtgVel.gpsHDOPFilterMax;
                        settings.GPSHDOPFilterChange=obj.transects(firstIdx).boatVel.vtgVel.gpsHDOPFilterChange;
                        settings.GPSSmoothFilter=obj.transects(firstIdx).boatVel.vtgVel.smoothFilter;
                        settings.GPSInterpolation=obj.transects(firstIdx).boatVel.vtgVel.interpolate;
                    else
                        settings.vtgsmoothFilter='Off';
                        if ~isfield(settings,'GPSInterpolation')
                            settings.GPSInterpolation='None'; 
                        end
                        if ~isfield(settings,'GPSHDOPFilter')
                            settings.GPSHDOPFilter='Off';
                            settings.GPSHDOPFilterMax=[];
                            settings.GPSHDOPFilterChange=[]; 
                        end
                        if ~isfield(settings,'GPSSmoothFilter')
                            settings.GPSSmoothFilter='Off';
                        end
                    end
                end
                
            end
            
            % Depth Settings
            settings.depthAvgMethod=obj.transects(firstIdx).depths.btDepths.avgMethod;
            settings.depthValidMethod=obj.transects(firstIdx).depths.btDepths.validDataMethod;
            
            % Depth settings are always applied to all available depth
            % sources. Only those saved in the btDepths are used here but
            % are applied to all sources.
            settings.depthFilterType=obj.transects(firstIdx).depths.btDepths.filterType;
            settings.depthReference=obj.transects(firstIdx).depths.selected;
            settings.depthComposite=obj.transects(firstIdx).depths.composite;
            settings.depthInterpolation=obj.transects(firstIdx).depths.(obj.transects(firstIdx).depths.selected).interpType;
            
            % Extrap Settings
            settings.extrapTop=obj.transects(firstIdx).extrap.topMethod;
            settings.extrapBot=obj.transects(firstIdx).extrap.botMethod;
            settings.extrapExp=obj.transects(firstIdx).extrap.exponent;
            
            % Edge Settings
            settings.edgeVelMethod=obj.transects(firstIdx).edges.velMethod;
            settings.edgeRecEdgeMethod=obj.transects(firstIdx).edges.recEdgeMethod;
            
        end % currentSettings
        
        function defaultSettings = QRevDefaultSettings(obj)
        % QRev default reference and filter settings for a measurement. 
        % 
        % INPUT:
        % 
        % obj: object of clsMeasurement
        %
        % OUTPUT:
        %
        % defaultSettings: data structure with settings 
        
            % Navigation reference (NEED LOGIC HERE)
            defaultSettings.NavRef=obj.transects(1).boatVel.selected;
            
            % Composite tracks
            defaultSettings.CompTracks='Off';
            
            % Water track filter settings
            defaultSettings.WTbeamFilter=-1;
            defaultSettings.WTdFilter='Auto';
            defaultSettings.WTdFilterThreshold=nan;
            defaultSettings.WTwFilter='Auto';
            defaultSettings.WTwFilterThreshold=nan;
            defaultSettings.WTsmoothFilter='Off';
            if strcmp(obj.transects(1).adcp.manufacturer,'TRDI')
                defaultSettings.WTsnrFilter='Off';
            else
                defaultSettings.WTsnrFilter='Auto';
            end % if TRDI
            temp=[obj.transects(:).wVel];
            excludedDist=nanmin([temp(:).excludedDist]);
            if excludedDist < 0.158 && strcmp(obj.transects(1).adcp.model,'M9')
                defaultSettings.WTExcludedDistance=0.16;
            else
                defaultSettings.WTExcludedDistance=excludedDist;
            end % excludedDist
            
            % Bottom track filter settings
            defaultSettings.BTbeamFilter=-1;
            defaultSettings.BTdFilter='Auto';
            defaultSettings.BTdFilterThreshold=nan;
            defaultSettings.BTwFilter='Auto';
            defaultSettings.BTwFilterThreshold=nan;
            defaultSettings.BTsmoothFilter='Off';

            % GGA filter settings
            defaultSettings.ggaDiffQualFilter=2;
            defaultSettings.ggaAltitudeFilter='Auto';
            defaultSettings.ggaAltitudeFilterChange=nan;

            % VTG filter settings
            defaultSettings.vtgsmoothFilter=nan;
            
            % GGA and VTG filter settings
            defaultSettings.GPSHDOPFilter='Auto';
            defaultSettings.GPSHDOPFilterMax=nan;
            defaultSettings.GPSHDOPFilterChange=nan;
            defaultSettings.GPSSmoothFilter='Off';

            % Depth Averaging
            defaultSettings.depthAvgMethod='IDW';
            defaultSettings.depthValidMethod='QRev';
            
            % Depth Reference

            % Default to 4 beam depth average
            defaultSettings.depthReference='btDepths';
            % Depth settings
            defaultSettings.depthFilterType='Smooth';
            
            defaultSettings.depthComposite='On';
             
            %Interpolation setttings
            defaultSettings=clsMeasurement.QRevDefaultInterp(defaultSettings);
            
            % Edge Settings
             defaultSettings.edgeVelMethod='MeasMag';
%              defaultSettings.edgeVelMethod='Profile';
             defaultSettings.edgeRecEdgeMethod='Fixed';
            
        end % QRevDefaultSettings
        
        function defaultSettings = NoFilterInterpSettings(obj)
        % QRev default reference and filter settings for a measurement. 
        % 
        % INPUT:
        % 
        % obj: object of clsMeasurement
        %
        % OUTPUT:
        %
        % defaultSettings: data structure with settings 
        
            % Navigation reference (NEED LOGIC HERE)
            defaultSettings.NavRef=obj.transects(1).boatVel.selected;
            
            % Composite tracks
            defaultSettings.CompTracks='Off';
            
            % Water track filter settings
            defaultSettings.WTbeamFilter=3;
            defaultSettings.WTdFilter='Off';
            defaultSettings.WTdFilterThreshold=nan;
            defaultSettings.WTwFilter='Off';
            defaultSettings.WTwFilterThreshold=nan;
            defaultSettings.WTsmoothFilter='Off';
            defaultSettings.WTsnrFilter='Off';

            temp=[obj.transects(:).wVel];
            excludedDist=nanmin([temp(:).excludedDist]);
            defaultSettings.WTExcludedDistance=excludedDist;
            
            % Bottom track filter settings
            defaultSettings.BTbeamFilter=3;
            defaultSettings.BTdFilter='Off';
            defaultSettings.BTdFilterThreshold=nan;
            defaultSettings.BTwFilter='Off';
            defaultSettings.BTwFilterThreshold=nan;
            defaultSettings.BTsmoothFilter='Off';

            % GGA filter settings
            defaultSettings.ggaDiffQualFilter=1;
            defaultSettings.ggaAltitudeFilter='Off';
            defaultSettings.ggaAltitudeFilterChange=nan;

            % VTG filter settings
            defaultSettings.vtgsmoothFilter=nan;
            
            % GGA and VTG filter settings
            defaultSettings.GPSHDOPFilter='Off';
            defaultSettings.GPSHDOPFilterMax=nan;
            defaultSettings.GPSHDOPFilterChange=nan;
            defaultSettings.GPSSmoothFilter='Off';

            % Depth Averaging
            defaultSettings.depthAvgMethod='IDW';
            defaultSettings.depthValidMethod='QRev';
            
            % Depth Reference

            % Default to 4 beam depth average
            defaultSettings.depthReference='btDepths';
            % Depth settings
            defaultSettings.depthFilterType='None';
            defaultSettings.depthComposite='Off';
             
            % Interpolation settings
            defaultSettings.BTInterpolation='None';
            defaultSettings.WTEnsInterpolation='None';
            defaultSettings.WTCellInterpolation='None';
            defaultSettings.GPSInterpolation='None';
            defaultSettings.depthInterpolation='None';
            defaultSettings.WTwtDepthFilter='Off';
            
            
            % Edge Settings
             defaultSettings.edgeVelMethod='MeasMag';
%              defaultSettings.edgeVelMethod='Profile';
             defaultSettings.edgeRecEdgeMethod='Fixed';
            
        end % NoFilterInterpSettings
        
        function settings=QRevDefaultInterp(settings)
        % Adds QRev default interpolation settings to existing settings 
        % data structure. 
        % 
        % INPUT:
        % 
        % settings: data structure of reference and filter settings
        %
        % OUTPUT:
        %
        % settings: data structure with reference, filter, and interpolation
        % settings 
        
            % Interpolation settings
            settings.BTInterpolation='Linear';
            settings.WTEnsInterpolation='abba';
            settings.WTCellInterpolation='abba';
            settings.GPSInterpolation='Linear';
            settings.depthInterpolation='Linear';
            settings.WTwtDepthFilter='On';
            
        end % QRevDefaultInterp
        
        function settings=WR2DefaultInterp(settings)
        % Adds WinRiver 2 default interpolation settings to existing settings 
        % data structure. 
        % 
        % INPUT:
        % 
        % settings: data structure of reference and filter settings
        %
        % OUTPUT:
        %
        % settings: data structure with reference, filter, and interpolation
        % settings 
        
            % Interpolation settings
            settings.BTInterpolation='None';
            settings.WTEnsInterpolation='None';
            settings.WTCellInterpolation='TRDI';
            settings.GPSInterpolation='HoldLast';
            settings.depthInterpolation='None';
            settings.WTwtDepthFilter='On';
        end % WR2DefaultInterp
        
        function settings=RSLDefaultInterp(settings)
        % Adds RiverSurveyor Live default interpolation settings to existing  
        % settings data structure. 
        % 
        % INPUT:
        % 
        % settings: data structure of reference and filter settings
        %
        % OUTPUT:
        %
        % settings: data structure with reference, filter, and interpolation
        % settings   
        
            % Interpolation settings
            settings.BTInterpolation='Hold9';
            settings.WTEnsInterpolation='None';
            settings.WTCellInterpolation='None';
            settings.GPSInterpolation='None';
            settings.depthInterpolation='HoldLast';
            settings.WTwtDepthFilter='On';
        end % RSLDefaultInterp
        
        % Units handler
        % -------------
        function [unitsL,unitsQ,unitsA,unitsV,uLabelL,uLabelQ,uLabelA,uLabelV]=unitsMultiplier(varargin)
        % Computes the units conversion from SI units used internally to the
        % desired display units. This function is tied to QRev GUI and
        % could be changed to remove dependence.
        % 
        % INPUT:
        % 
        % handles: handles data structure from QRev
        %
        % OUTPUT:
        %
        % unitsL: units multiplier for length
        %
        % unitsQ: units multiplier for discharge
        %
        % unitsA: units multiplier for area
        %
        % unitsV: units multiplier for velocity
        %
        % uLabelL: units label for length
        %
        % uLabelQ: units label for discharge
        %
        % uLabelA: units label for area
        %
        % uLabelV: units label for velocity

            % Get display units
            displayUnits=getUserPref('Units');
            
            % SI units
            if strcmp(displayUnits,'SI')
                unitsL=1;
                unitsQ=1;
                unitsA=1;
                unitsV=1;
                uLabelL='(m)';
                uLabelQ='(m3/s)';
                uLabelA='(m2)';
                uLabelV='(m/s)';
                
            % English units
            else
                unitsL=1./0.3048;
                unitsQ=unitsL.^3;
                unitsA=unitsL.^2;
                unitsV=unitsL;
                uLabelL='(ft)';
                uLabelQ='(ft3/s)';
                uLabelA='(ft2)';
                uLabelV='(ft/s)';
            end   
        end % unitsHandler
        
        % XML Output
        % ----------
        function ok=createXMLFile(meas,version,filename)
        % This function writes an XML formatted file for use by SVMobile.
        % Any changes to this format need to be coordinated with the
        % SVMobile programming team.
        %
        % INPUT:
        %
        % meas: object of clsMeasurement
        %
        % messages: QRev automated quality checking messages
        %
        % version: version of QRev
        %
        % filename: filename for XML file without the xml extension
        %
        % OUTPUT:
        %
        % ok: true if file written successfully, false if not
         try
            uncertainty=meas.uncertainty;
            discharge=meas.discharge;
            nTransects=length(meas.transects);
            checked=logical([meas.transects.checked]);
            first=find(checked==1,1,'first');
            %% Start XML
            %<?xml-stylesheet type="text/xsl" href="QRevStylesheet.xsl"?>

            docNode = com.mathworks.xml.XMLUtils.createDocument('Channel');
                Channel = docNode.getDocumentElement;
                    Channel.setAttribute('QRevVersion',version);
                    idx=strfind(filename,'\');
                    Channel.setAttribute('QRevFilename',filename(idx(end)+1:end));

                    %% SiteInformation Node
                    if ~isempty(meas.stationName)||~isempty(meas.stationNumber)
                        SiteInformation=clsMeasurement.createNode(docNode,docNode,'SiteInformation');
                    
                        % StationName Node
                        if ~isempty(meas.stationName)
                            StationName=clsMeasurement.createNode(docNode,SiteInformation,'StationName','char',meas.stationName);
                            SiteInformation.appendChild(StationName);
                        end
                        
                        % SiteId Node
                        if ~isempty(meas.stationNumber) & double(meas.stationNumber)~=0
                            SiteId=clsMeasurement.createNode(docNode,SiteInformation,'SiteID','char',meas.stationNumber);
                            SiteInformation.appendChild(SiteId);
                        end
                    end
                    %% QA Node
                    QA=clsMeasurement.createNode(docNode,docNode,'QA');

                        % DiagnosticTestResult Node
                        if ~isempty(meas.sysTest(1).data)
                            nTests=size(meas.sysTest,2);
                            failedIdx=strfind(meas.sysTest(nTests).data,'FAIL');
                            if isempty(failedIdx)
                                testResult='Pass';
                            else
                                testResult=[num2str(length(failedIdx)),' Failed'];
                            end
                        else
                            testResult='None';
                        end
                        DiagnosticTestResult=clsMeasurement.createNode(docNode,QA,'DiagnosticTestResult','char',testResult);
                        QA.appendChild(DiagnosticTestResult);

                        % CompassCalibrationResult Node
                        compError=nan;
                        if ~isempty(meas.compassEval(1).data)
                            nEvals=size(meas.compassEval,2);
                            lastEval=meas.compassEval(nEvals);
                            % StreamPro, RR (ISM)
                            idx=strfind(lastEval.data,'Typical Heading Error: <'); 
                            if isempty(idx)
                                % Rio Grande
                                idx=strfind(lastEval.data,'>>> Total error:'); 
                                if ~isempty(idx)
                                    idxStart=idx(end)+17;
                                    idxEnd=idxStart+10;
                                    compError=sscanf(lastEval.data(idxStart:idxEnd),'%g');
                                else
                                    compError=[];
                                end % is empty
                            else
                                % StreamPro, RR (ISM)
                                idxStart=idx(end)+24;
                                idxEnd=idxStart+10;
                                compError=sscanf(lastEval.data(idxStart:idxEnd),'%g');  
                            end
                            % Evaluation error could not be determined
                            if isnan(compError)
                                CompassCalibrationResult=clsMeasurement.createNode(docNode,QA,'CompassCalibrationResult','char','Yes');
                            elseif isempty(compError)
                                CompassCalibrationResult=clsMeasurement.createNode(docNode,QA,'CompassCalibrationResult','char','No');
                            else 
                                CompassCalibrationResult=clsMeasurement.createNode(docNode,QA,'CompassCalibrationResult','char',sprintf('Max %4.1f',compError)); 
                            end
                        else
                            % No compass evaluation
                            if ~isempty(meas.compassCal(1).data)
                                CompassCalibrationResult=clsMeasurement.createNode(docNode,QA,'CompassCalibrationResult','char','Yes');
                            else
                                CompassCalibrationResult=clsMeasurement.createNode(docNode,QA,'CompassCalibrationResult','char','No');
                            end
                        end
                        QA.appendChild(CompassCalibrationResult);

                        % MovingBedTestType Node
                        if isempty(meas.mbTests)
                            MovingBedTestType=clsMeasurement.createNode(docNode,QA,'MovingBedTestType','char','None');
                        else
                            selectedIdx=find([meas.mbTests.selected]==true);
                            if isempty(selectedIdx)
                                selectedIdx=length(meas.mbTests);
                            end
                            MovingBedTestType=clsMeasurement.createNode(docNode,QA,'MovingBedTestType','char',meas.mbTests(selectedIdx).type);
                        
                            QA.appendChild(MovingBedTestType);

                            % MovingBedTestResult Node
                            movingBed=[meas.mbTests(selectedIdx).movingBed];
                            if isempty(movingBed)
                                MovingBedTestResult=clsMeasurement.createNode(docNode,QA,'MovingBedTestResult','char','Unknown');
                            else
                                idx=strfind(movingBed,'Yes');
                                if isempty(idx)
                                    MovingBedTestResult=clsMeasurement.createNode(docNode,QA,'MovingBedTestResult','char','No');
                                else
                                    MovingBedTestResult=clsMeasurement.createNode(docNode,QA,'MovingBedTestResult','char','Yes'); 
                                end
                            end
                            QA.appendChild(MovingBedTestResult);
                        end
                        %% DiagnosticTest Node
                        if ~isempty(meas.sysTest)
                            nTests=size(meas.sysTest,2);
                            testText=' ';
                            for n=1:nTests
                                testText=[testText meas.sysTest(n).data];
                            end
                            DiagnosticTest=clsMeasurement.createNode(docNode,docNode,'DiagnosticTest');
                            QA.appendChild(DiagnosticTest);
                            Text=clsMeasurement.createNode(docNode,DiagnosticTest,'Text','char',testText);
                            DiagnosticTest.appendChild(Text);
                        end

                        %% CompassCalibration Node
                        compassText=' ';
                        if ~isempty(meas.compassCal(1).data)
                            nCals=size(meas.compassCal,2);
                            for n=1:nCals
                                if strcmpi(meas.transects(1).adcp.manufacturer,'SonTek')
                                    idx=strfind(meas.compassCal(n).data,'CAL_TIME');
                                    compassText=[compassText meas.compassCal(n).data(idx:end)];
                                else
                                    compassText=[compassText meas.compassCal(n).data];
                                end
                            end
                        end
                        if ~isempty(meas.compassEval(1).data)
                            nEvals=size(meas.compassEval,2);
                            for n=1:nEvals
                                if strcmpi(meas.transects(1).adcp.manufacturer,'SonTek')
                                    idx=strfind(meas.compassEval(n).data,'CAL_TIME');
                                    compassText=[compassText meas.compassEval(n).data(idx:end)];
                                else
                                    compassText=[compassText meas.compassEval(n).data];
                                end
                            end
                        end
                        if length(compassText)>1
                            CompassCalibration=clsMeasurement.createNode(docNode,docNode,'CompassCalibration');
                            QA.appendChild(CompassCalibration);
                            Text=clsMeasurement.createNode(docNode,DiagnosticTest,'Text','char',compassText);
                            CompassCalibration.appendChild(Text);
                        end

                        %% MovingBedTest Node
                        if ~isempty(meas.mbTests)
                            nTest=length(meas.mbTests);
                            for n=1:nTest
                                MovingBedTest=clsMeasurement.createNode(docNode,docNode,'MovingBedTest');
                                QA.appendChild(MovingBedTest);

                                % Filename
                                Filename=clsMeasurement.createNode(docNode,MovingBedTest,'Filename','char',meas.mbTests(n).transect.fileName);
                                MovingBedTest.appendChild(Filename);

                                % TestType
                                TestType=clsMeasurement.createNode(docNode,MovingBedTest,'TestType','char',meas.mbTests(n).type);
                                MovingBedTest.appendChild(TestType);

                                % Duration
                                Duration=clsMeasurement.createNode(docNode,MovingBedTest,'Duration','double',num2str(meas.mbTests(n).duration_sec));
                                Duration.setAttribute('unitsCode','sec');
                                MovingBedTest.appendChild(Duration); 

                                % PercentInvalidBT
                                PercentInvalidBT=clsMeasurement.createNode(docNode,MovingBedTest,'PercentInvalidBT','double',num2str(meas.mbTests(n).percentInvalidBT));
                                MovingBedTest.appendChild(PercentInvalidBT);  

                                % HeadingDifference
                                HeadingDifference=clsMeasurement.createNode(docNode,MovingBedTest,'HeadingDifference','double',num2str(meas.mbTests(n).compassDiff_deg));
                                HeadingDifference.setAttribute('unitsCode','deg');
                                MovingBedTest.appendChild(HeadingDifference); 

                                % MeanFlowDirection
                                MeanFlowDirection=clsMeasurement.createNode(docNode,MovingBedTest,'MeanFlowDirection','double',num2str(meas.mbTests(n).flowDir_deg));
                                MeanFlowDirection.setAttribute('unitsCode','deg');
                                MovingBedTest.appendChild(MeanFlowDirection); 

                                % MovingBedDirection
                                MovingBedDirection=clsMeasurement.createNode(docNode,MovingBedTest,'MovingBedDirection','double',num2str(meas.mbTests(n).mbDir_deg));
                                MovingBedDirection.setAttribute('unitsCode','deg');
                                MovingBedTest.appendChild(MovingBedDirection);

                                % DistanceUpstream
                                DistanceUpstream=clsMeasurement.createNode(docNode,MovingBedTest,'DistanceUpstream','double',num2str(meas.mbTests(n).distUS_m));
                                DistanceUpstream.setAttribute('unitsCode','m');
                                MovingBedTest.appendChild(DistanceUpstream);    

                                % MeanFlowSpeed
                                MeanFlowSpeed=clsMeasurement.createNode(docNode,MovingBedTest,'MeanFlowSpeed','double',num2str(meas.mbTests(n).flowSpd_mps));
                                MeanFlowSpeed.setAttribute('unitsCode','mps');
                                MovingBedTest.appendChild(MeanFlowSpeed); 

                                % MovingBedSpeed
                                MovingBedSpeed=clsMeasurement.createNode(docNode,MovingBedTest,'MovingBedSpeed','double',num2str(meas.mbTests(n).mbSpd_mps));
                                MovingBedSpeed.setAttribute('unitsCode','mps');
                                MovingBedTest.appendChild(MovingBedSpeed);   

                                % PercentMovingBed
                                PercentMovingBed=clsMeasurement.createNode(docNode,MovingBedTest,'PercentMovingBed','double',num2str(meas.mbTests(n).percentMB));
                                MovingBedTest.appendChild(PercentMovingBed);  

                                % TestQuality
                                TestQuality=clsMeasurement.createNode(docNode,MovingBedTest,'TestQuality','char',meas.mbTests(n).testQuality);
                                MovingBedTest.appendChild(TestQuality);    

                                % MovingBedPresent
                                MovingBedPresent=clsMeasurement.createNode(docNode,MovingBedTest,'MovingBedPresent','char',meas.mbTests(n).movingBed);
                                MovingBedTest.appendChild(MovingBedPresent);

                                % UseToCorrect
                                if meas.mbTests(n).use2Correct
                                    use='Yes';
                                else
                                    use='No';
                                end
                                UseToCorrect=clsMeasurement.createNode(docNode,MovingBedTest,'UseToCorrect','char',use);
                                MovingBedTest.appendChild(UseToCorrect);

                                % UserValid
                                if meas.mbTests(n).userValid
                                    use='Yes';
                                else
                                    use='No';
                                end
                                UserValid=clsMeasurement.createNode(docNode,MovingBedTest,'UserValid','char',use);
                                MovingBedTest.appendChild(UserValid);
                                
                                % Message
                                nout=0;
                                messageout={};
                                for k=1:length(meas.mbTests(n).messages)
                                    if length(meas.mbTests(n).messages{k})>1
                                        nout=nout+1;
                                        messageout(nout)={['\r\n ' meas.mbTests(n).messages{k}]};
                                    end
                                end

                                % Convert to character string and output to GUI
                                outstr=char(messageout);
                                if ~isempty(outstr)
                                    outstr=sprintf(outstr(1:end,:)'); 
                                    Message=clsMeasurement.createNode(docNode,MovingBedTest,'Message','char',outstr);
                                    MovingBedTest.appendChild(Message);  
                                end % if outstr
                            end % for n     
                        end % mbTests

                        %% TemperatureCheck Node
                        TemperatureCheck=clsMeasurement.createNode(docNode,docNode,'TemperatureCheck');
                        QA.appendChild(TemperatureCheck);   

                            % VerificationTemperature
                            if ~isempty(meas.extTempChk.user)
                                VerificationTemperature=clsMeasurement.createNode(docNode,TemperatureCheck,'VerificationTemperature','double',num2str(meas.extTempChk.user));
                                VerificationTemperature.setAttribute('unitsCode','degC');
                                TemperatureCheck.appendChild(VerificationTemperature);
                            end % if user

                            % InstrumentTemperature
                            if ~isempty(meas.extTempChk.adcp)
                                InstrumentTemperature=clsMeasurement.createNode(docNode,TemperatureCheck,'InstrumentTemperature','double',num2str(meas.extTempChk.adcp));
                                InstrumentTemperature.setAttribute('unitsCode','degC');
                                TemperatureCheck.appendChild(InstrumentTemperature);
                            end % if adcp

                            % TemperatureChange
                            nTransects=length(meas.transects);
                            temp=nan;
                            for n=1:nTransects   
                                % Check for situation where user has entered a constant temperature
                                if length(meas.transects(n).sensors.temperature_degC.(meas.transects(n).sensors.temperature_degC.selected).data)>1
                                    % Temperatures for ADCP
                                    temp=[temp, meas.transects(n).sensors.temperature_degC.(meas.transects(n).sensors.temperature_degC.selected).data];
                                else
                                    % User specified constant temperature
                                    temp=[temp, repmat(meas.transects(n).sensors.temperature_degC.(meas.transects(n).sensors.temperature_degC.selected).data,size(meas.transects(n).sensors.temperature_degC.internal.data))];
                                end % if
                            end % for n
                            range=nanmax(temp)-nanmin(temp);
                            TemperatureChange=clsMeasurement.createNode(docNode,TemperatureCheck,'TemperatureChange','double',num2str(range));
                            TemperatureChange.setAttribute('unitsCode','degC');
                            TemperatureCheck.appendChild(TemperatureChange);    

                        %% QRev_Message
                        % Identify properties to be reported
                        prop=properties(meas.qa);
                        propidx=strfind(prop,'Threshold');
                        propidx=cellfun(@isempty,propidx);
                        qaProp=prop(propidx);

                        % Initialize variables
                        nProperties=length(qaProp);
                        nout=0;
                        messageout={};

                        % Loop through each property
                        for n=1:nProperties
                            % Determine number of messages for that property
                            nmessages=size(meas.qa.(qaProp{n}).messages,1);
                            if nmessages>0
                                % Consolidate messages
                                for j=1:nmessages
                                    nout=nout+1;
                                    messageout(nout)={[meas.qa.(qaProp{n}).messages{j,1}]};
                                end
                            end
                        end
                        % Convert to character string and output to GUI
                        if ~isempty(messageout)
                            outstr= sprintf('%s\n',messageout{:});                
                            QRev_Message=clsMeasurement.createNode(docNode,QA,'QRev_Message','char',outstr);
                            QA.appendChild(QRev_Message);  
                        end % if outstr

                    %% Instrument Node
                    Instrument=clsMeasurement.createNode(docNode,docNode,'Instrument');

                        % Manufacturer
                        Manufacturer=clsMeasurement.createNode(docNode,Instrument,'Manufacturer','char',meas.transects(first).adcp.manufacturer);
                        Instrument.appendChild(Manufacturer);  

                        % Model
                        Model=clsMeasurement.createNode(docNode,Instrument,'Model','char',meas.transects(first).adcp.model);
                        Instrument.appendChild(Model);  

                        % SerialNumber
                        temp=meas.transects(first).adcp.serialNum;
                        if isnumeric(temp)
                            temp=num2str(temp);
                        end
                        SerialNumber=clsMeasurement.createNode(docNode,Instrument,'SerialNumber','char',temp);
                        Instrument.appendChild(SerialNumber); 

                        % FirmwareVersion
                        temp=meas.transects(first).adcp.firmware;
                        if isnumeric(temp)
                            temp=num2str(temp);
                        end % if isnumeric
                        FirmwareVersion=clsMeasurement.createNode(docNode,Instrument,'FirmwareVersion','char',temp);
                        Instrument.appendChild(FirmwareVersion);

                        % Frequency
                        temp=meas.transects(first).adcp.frequency_hz;
                        if size(temp,1)>1
                            temp='Multi';
                        elseif isnumeric(temp)
                            temp=num2str(temp);
                        end % if 
                        Frequency=clsMeasurement.createNode(docNode,Instrument,'Frequency','char',temp);
                        Frequency.setAttribute('unitsCode','kHz');
                        Instrument.appendChild(Frequency); 

                        % BeamAngle
                        BeamAngle=clsMeasurement.createNode(docNode,Instrument,'BeamAngle','double',num2str(meas.transects(first).adcp.beamAngle_deg));
                        BeamAngle.setAttribute('unitsCode','deg');
                        Instrument.appendChild(BeamAngle);

                        % BlankingDistance
                        wVel=[meas.transects.wVel];
                        blank=[wVel.blankingDistance_m];
                        if isnumeric(blank(1))
                            temp=nanmean(blank(checked));
                            if meas.transects(1).wVel.excludedDist>temp
                                temp=meas.transects(1).wVel.excludedDist;
                            end
                        else
                            temp=meas.transects(1).wVel.excludedDist;
                        end
                        BlankingDistance=clsMeasurement.createNode(docNode,Instrument,'BlankingDistance','double',num2str(temp));
                        BlankingDistance.setAttribute('unitsCode','m');
                        Instrument.appendChild(BlankingDistance); 

                        % InstrumentConfiguration
                        % Find nonempty cells
                        if iscell(meas.transects(1).adcp.configurationCommands)
                            idx=find(~cellfun(@isempty,meas.transects(1).adcp.configurationCommands));
                            temp=char(meas.transects(1).adcp.configurationCommands(idx));
                        else
                            temp=char(meas.transects(1).adcp.configurationCommands);
                        end
                        if ~isempty(temp)
                            temp=sprintf(temp(1:end,:)');
                            InstrumentConfiguration=clsMeasurement.createNode(docNode,Instrument,'InstrumentConfiguration','char',temp);
                            Instrument.appendChild(InstrumentConfiguration);    
                        end
                    %% Processing Node
                    Processing=clsMeasurement.createNode(docNode,docNode,'Processing');

                        % SoftwareVersion
                        SoftwareVersion=clsMeasurement.createNode(docNode,Processing,'SoftwareVersion','char',version);
                        Processing.appendChild(SoftwareVersion);

                        % Type
                        temp=meas.processing;
                        Type=clsMeasurement.createNode(docNode,Processing,'Type','char',temp);
                        Processing.appendChild(Type);

                        % AreaComputationMethod
                        AreaComputationMethod=clsMeasurement.createNode(docNode,Processing,'AreaComputationMethod','char','Parallel');
                        Processing.appendChild(AreaComputationMethod);

                        %% Navigation Node
                        Navigation=clsMeasurement.createNode(docNode,docNode,'Navigation');
                        Processing.appendChild(Navigation);

                            % Reference
                            Reference=clsMeasurement.createNode(docNode,Navigation,'Reference','char',meas.transects(first).wVel.navRef);
                            Navigation.appendChild(Reference);

                            % CompositeTrack
                            CompositeTrack=clsMeasurement.createNode(docNode,Navigation,'CompositeTrack','char',meas.transects(first).boatVel.composite);
                            Navigation.appendChild(CompositeTrack);

                            % MagneticVariation
                            magvar=meas.transects(first).sensors.heading_deg.(meas.transects(first).sensors.heading_deg.selected).magVar_deg;
                            MagneticVariation=clsMeasurement.createNode(docNode,Navigation,'MagneticVariation','double',num2str(magvar));
                            MagneticVariation.setAttribute('unitsCode','deg');
                            Navigation.appendChild(MagneticVariation);

                            % BeamFilter
                            temp=meas.transects(first).boatVel.(meas.transects(first).boatVel.selected).beamFilter;
                            if temp<0
                                temp='Auto';
                            else
                                temp=num2str(temp);
                            end % if temp
                            BeamFilter=clsMeasurement.createNode(docNode,Navigation,'BeamFilter','char',temp);
                            Navigation.appendChild(BeamFilter);

                            % ErrorVelocityFilter
                            temp=meas.transects(first).boatVel.(meas.transects(first).boatVel.selected).dFilter;
                            if strcmpi(temp,'Manual')
                                temp=num2str(meas.transects(first).boatVel.(meas.transects(first).boatVel.selected).dFilterThreshold);
                            end % if temp
                            ErrorVelocityFilter=clsMeasurement.createNode(docNode,Navigation,'ErrorVelocityFilter','char',temp);
                            ErrorVelocityFilter.setAttribute('unitsCode','mps');
                            Navigation.appendChild(ErrorVelocityFilter);

                            % VerticalVelocityFilter
                            temp=meas.transects(first).boatVel.(meas.transects(first).boatVel.selected).wFilter;
                            if strcmpi(temp,'Manual')
                                temp=num2str(meas.transects(first).boatVel.(meas.transects(first).boatVel.selected).wFilterThreshold);
                            end % if temp
                            VerticalVelocityFilter=clsMeasurement.createNode(docNode,Navigation,'VerticalVelocityFilter','char',temp);
                            VerticalVelocityFilter.setAttribute('unitsCode','mps');
                            Navigation.appendChild(VerticalVelocityFilter);

                            % OtherFilter
                            temp=meas.transects(first).boatVel.(meas.transects(first).boatVel.selected).smoothFilter;
                            OtherFilter=clsMeasurement.createNode(docNode,Navigation,'OtherFilter','char',temp);
                            Navigation.appendChild(OtherFilter);

                            % GPSDifferentialQualityFilter
                            temp=meas.transects(first).boatVel.(meas.transects(first).boatVel.selected).gpsDiffQualFilter;
                            if ~isempty(temp)
                                if isnumeric(temp)
                                    temp=num2str(temp);
                                end
                                GPSDifferentialQualityFilter=clsMeasurement.createNode(docNode,Navigation,'GPSDifferentialQualityFilter','char',temp);
                                Navigation.appendChild(GPSDifferentialQualityFilter);
                            end % if isempty

                            % GPSAltitudeFilter
                            temp=meas.transects(first).boatVel.(meas.transects(first).boatVel.selected).gpsAltitudeFilter;
                            if ~isempty(temp)
                                if strcmpi(temp,'Manual')
                                    temp=num2str(meas.transects(first).boatVel.(meas.transects(first).boatVel.selected).gpsAltitudeFilterChange);
                                end % if temp
                                GPSAltitudeFilter=clsMeasurement.createNode(docNode,Navigation,'GPSAltitudeFilter','char',temp);
                                GPSAltitudeFilter.setAttribute('unitsCode','m');
                                Navigation.appendChild(GPSAltitudeFilter);
                            end % if isempty

                            % HDOPChangeFilter
                            temp=meas.transects(first).boatVel.(meas.transects(first).boatVel.selected).gpsHDOPFilter;
                            if ~isempty(temp)
                                if strcmpi(temp,'Manual')
                                    temp=num2str(meas.transects(first).boatVel.(meas.transects(first).boatVel.selected).gpsHDOPFilterChange);
                                end % if temp
                                HDOPChangeFilter=clsMeasurement.createNode(docNode,Navigation,'HDOPChangeFilter','char',temp);
                                Navigation.appendChild(HDOPChangeFilter);
                            end % if isempty

                            % HDOPThresholdFilter
                            temp=meas.transects(first).boatVel.(meas.transects(first).boatVel.selected).gpsHDOPFilter;
                            if ~isempty(temp)
                                if strcmpi(temp,'Manual')
                                    temp=num2str(meas.transects(first).boatVel.(meas.transects(first).boatVel.selected).gpsHDOPFilterMax);
                                end % if temp
                                HDOPThresholdFilter=clsMeasurement.createNode(docNode,Navigation,'HDOPThresholdFilter','char',temp);
                                Navigation.appendChild(HDOPThresholdFilter);
                            end % if isempty

                            % InterpolationType
                            temp=meas.transects(first).boatVel.(meas.transects(first).boatVel.selected).interpolate;
                            InterpolationType=clsMeasurement.createNode(docNode,Navigation,'InterpolationType','char',temp);
                            Navigation.appendChild(InterpolationType);

                        %% Depth Node
                        Depth=clsMeasurement.createNode(docNode,docNode,'Depth');
                        Processing.appendChild(Depth);

                            % Reference
                            switch meas.transects(first).depths.selected
                                case 'btDepths'
                                    temp='BT';
                                case 'vbDepths'
                                    temp='VB';
                                case 'dsDepths'
                                    temp='DS';
                            end
                            Reference=clsMeasurement.createNode(docNode,Depth,'Reference','char',temp);
                            Depth.appendChild(Reference);

                            % CompositeDepth
                            CompositeDepth=clsMeasurement.createNode(docNode,Depth,'CompositeDepth','char',meas.transects(first).depths.composite);
                            Depth.appendChild(CompositeDepth);

                            % ADCPDepth
                            temp=meas.transects(first).depths.(meas.transects(first).depths.selected).draftUse_m;
                            ADCPDepth=clsMeasurement.createNode(docNode,Navigation,'ADCPDepth','double',num2str(temp));
                            ADCPDepth.setAttribute('unitsCode','m');
                            Depth.appendChild(ADCPDepth);
                            % ADCPDepth
                            k=0;
                            for n=1:nTransects
                                if checked(n)
                                    k=k+1;
                                    draft(k)=meas.transects(n).depths.(meas.transects(n).depths.selected).draftUse_m;
                                end % if checked
                            end % for n
                            numdrafts=unique(draft);
                            if length(numdrafts)>1
                                temp='No';
                            else
                                temp='Yes';
                            end % numdrafts
                            ADCPDepthConsistent=clsMeasurement.createNode(docNode,Navigation,'ADCPDepthConsistent','boolean',temp);
                            Depth.appendChild(ADCPDepthConsistent);

                            % FilterType
                            temp=meas.transects(first).depths.(meas.transects(first).depths.selected).filterType;
                            FilterType=clsMeasurement.createNode(docNode,Depth,'FilterType','char',temp);
                            Depth.appendChild(FilterType);

                            % InterpolationType
                            temp=meas.transects(first).depths.(meas.transects(first).depths.selected).interpType;
                            InterpolationType=clsMeasurement.createNode(docNode,Depth,'InterpolationType','char',temp);
                            Depth.appendChild(InterpolationType);

                            % AveragingMethod
                            temp=meas.transects(first).depths.(meas.transects(first).depths.selected).avgMethod;
                            AveragingMethod=clsMeasurement.createNode(docNode,Depth,'AveragingMethod','char',temp);
                            Depth.appendChild(AveragingMethod);
                            
                            % ValidDataMethod
                            temp=meas.transects(first).depths.(meas.transects(first).depths.selected).validDataMethod;
                            ValidDataMethod=clsMeasurement.createNode(docNode,Depth,'ValidDataMethod','char',temp);
                            Depth.appendChild(ValidDataMethod);

                        %% WaterTrack Node
                        WaterTrack=clsMeasurement.createNode(docNode,docNode,'WaterTrack');
                        Processing.appendChild(WaterTrack);

                            % ExcludedDistance
                            temp=meas.transects(first).wVel.excludedDist;
                            ExcludedDistance=clsMeasurement.createNode(docNode,WaterTrack,'ExcludedDistance','double',num2str(temp));
                            ExcludedDistance.setAttribute('unitsCode','m');
                            WaterTrack.appendChild(ExcludedDistance);

                            % BeamFilter
                            temp=meas.transects(first).wVel.beamFilter;
                            if temp<0
                                temp='Auto';
                            else
                                temp=num2str(temp);
                            end % if temp
                            BeamFilter=clsMeasurement.createNode(docNode,WaterTrack,'BeamFilter','char',temp);
                            WaterTrack.appendChild(BeamFilter);

                            % ErrorVelocityFilter
                            temp=meas.transects(first).wVel.dFilter;
                            if strcmpi(temp,'Manual')
                                temp=num2str(meas.transects(first).wVel.dFilterThreshold);
                            end % if temp
                            ErrorVelocityFilter=clsMeasurement.createNode(docNode,WaterTrack,'ErrorVelocityFilter','char',temp);
                            ErrorVelocityFilter.setAttribute('unitsCode','mps');
                            WaterTrack.appendChild(ErrorVelocityFilter);

                            % VerticalVelocityFilter
                            temp=meas.transects(first).wVel.wFilter;
                            if strcmpi(temp,'Manual')
                                temp=num2str(meas.transects(first).wVel.wFilterThreshold);
                            end % if temp
                            VerticalVelocityFilter=clsMeasurement.createNode(docNode,WaterTrack,'VerticalVelocityFilter','char',temp);
                            VerticalVelocityFilter.setAttribute('unitsCode','mps');
                            WaterTrack.appendChild(VerticalVelocityFilter);

                            % OtherFilter
                            temp=meas.transects(first).wVel.smoothFilter;
                            OtherFilter=clsMeasurement.createNode(docNode,WaterTrack,'OtherFilter','char',temp);
                            WaterTrack.appendChild(OtherFilter);

                            % SNRFilter
                            temp=meas.transects(first).wVel.snrFilter;
                            if ~isempty(temp)
                                SNRFilter=clsMeasurement.createNode(docNode,WaterTrack,'SNRFilter','char',temp);
                                WaterTrack.appendChild(SNRFilter);
                            end % if isempty

                            % CellInterpolation
                            temp=meas.transects(first).wVel.interpolateCells;
                            CellInterpolation=clsMeasurement.createNode(docNode,WaterTrack,'CellInterpolation','char',temp);
                            WaterTrack.appendChild(CellInterpolation);

                            % EnsembleInterpolation
                            temp=meas.transects(first).wVel.interpolateEns;
                            EnsembleInterpolation=clsMeasurement.createNode(docNode,WaterTrack,'EnsembleInterpolation','char',temp);
                            WaterTrack.appendChild(EnsembleInterpolation);

                        %% Edge Node
                        Edge=clsMeasurement.createNode(docNode,docNode,'Edge');
                        Processing.appendChild(Edge);

                            %RectangularEdgeMethod
                            temp=meas.transects(first).edges.recEdgeMethod;
                            RectangularEdgeMethod=clsMeasurement.createNode(docNode,Edge,'RectangularEdgeMethod','char',temp);
                            Edge.appendChild(RectangularEdgeMethod);

                            %VelocityMethod
                            temp=meas.transects(first).edges.velMethod;
                            VelocityMethod=clsMeasurement.createNode(docNode,Edge,'VelocityMethod','char',temp);
                            Edge.appendChild(VelocityMethod);

                            % LeftType
                            k=0;
                            type={};
                            for n=1:nTransects
                                if checked(n)
                                    k=k+1;
                                    type{k}=meas.transects(n).edges.left.type;
                                end
                            end
                            numType=unique(type);
                            if length(numType)>1
                                temp='Varies';
                            else
                                temp=numType{:};
                            end
                            LeftType=clsMeasurement.createNode(docNode,Edge,'LeftType','char',temp);
                            Edge.appendChild(LeftType);

                            % LeftEdgeCoefficient
                            switch temp
                                case 'User Q'
                                    temp='N/A';
                                case 'Varies'
                                    temp='N/A';
                                otherwise
                                    k=0;
                                    coef=[];
                                    for n=1:nTransects
                                        if checked(n)
                                            k=k+1;
                                            coef(k)=clsQComp.edgeCoef('left',meas.transects(n));
                                        end
                                    end
                                    numCoef=unique(coef);
                                    if length(numCoef)>1
                                        temp='Varies';
                                    else
                                        temp=num2str(coef(1));
                                    end
                            end % switch
                            LeftEdgeCoefficient=clsMeasurement.createNode(docNode,Edge,'LeftEdgeCoefficient','char',temp);
                            Edge.appendChild(LeftEdgeCoefficient);

                            % RightType
                            k=0;
                            type={};
                            for n=1:nTransects
                                if checked(n)
                                    k=k+1;
                                    type{k}=meas.transects(n).edges.right.type;
                                end
                            end
                            numType=unique(type);
                            if length(numType)>1
                                temp='Varies';
                            else
                                temp=numType{:};
                            end
                            RightType=clsMeasurement.createNode(docNode,Edge,'RightType','char',temp);
                            Edge.appendChild(RightType);

                            % RightEdgeCoefficient
                            switch temp
                                case 'User Q'
                                    temp='N/A';
                                case 'Varies'
                                    temp='N/A';
                                otherwise
                                    k=0;
                                    coef=[];
                                    for n=1:nTransects
                                        if checked(n)
                                            k=k+1;
                                            coef(k)=clsQComp.edgeCoef('right',meas.transects(n));
                                        end
                                    end
                                    numCoef=unique(coef);
                                    if length(numCoef)>1
                                        temp='Varies';
                                    else
                                        temp=num2str(coef(1));
                                    end
                            end % switch
                            RightEdgeCoefficient=clsMeasurement.createNode(docNode,Edge,'RightEdgeCoefficient','char',temp);
                            Edge.appendChild(RightEdgeCoefficient);

                        %% Extrapolation Node
                        Extrapolation=clsMeasurement.createNode(docNode,docNode,'Extrapolation');
                        Processing.appendChild(Extrapolation);  

                            % TopMethod
                            temp=meas.transects(first).extrap.topMethod;
                            TopMethod=clsMeasurement.createNode(docNode,Extrapolation,'TopMethod','char',temp);
                            Extrapolation.appendChild(TopMethod);

                            % BottomMethod
                            temp=meas.transects(first).extrap.botMethod;
                            BottomMethod=clsMeasurement.createNode(docNode,Extrapolation,'BottomMethod','char',temp);
                            Extrapolation.appendChild(BottomMethod);

                            % Exponent
                            temp=meas.transects(first).extrap.exponent;
                            Exponent=clsMeasurement.createNode(docNode,Extrapolation,'Exponent','double',num2str(temp));
                            Extrapolation.appendChild(Exponent);

                        %% Sensor Node
                        Sensor=clsMeasurement.createNode(docNode,docNode,'Sensor');
                        Processing.appendChild(Sensor);    

                            % TemperatureSource
                            clear temp
                            k=0;
                            for n=1:nTransects
                                if checked(n)
                                    k=k+1;
                                    temp{k}=meas.transects(k).sensors.temperature_degC.selected;
                                end
                            end
                            sources=unique(temp);
                            if length(sources)>1
                                temp='Varies';
                            else
                                temp=sources{1};
                            end
                          
                            TemperatureSource=clsMeasurement.createNode(docNode,Sensor,'TemperatureSource','char',temp);
                            Sensor.appendChild(TemperatureSource);

                            % Salinity
                            clear temp
                            k=0;
                            for n=1:nTransects
                                if checked(n)
                                    k=k+1;
                                    temp{k}=meas.transects(n).sensors.salinity_ppt.selected;
                                    sal(k)=nanmean(meas.transects(n).sensors.salinity_ppt.(temp{k}).data);
                                end % if checked
                            end
                            sources=unique(temp);
                            if length(sources)>1
                                temp='Varies';
                            else
                                temp=sources{1};
                            end
%                             temp=meas.transects(first).sensors.salinity_ppt.selected;
                            if strcmpi(temp,'internal')
                                temp='ADCP';
                            elseif strcmpi(temp,'user')
%                                 k=0;
%                                 for n=1:nTransects
%                                     if checked(n)
%                                         k=k+1;
%                                         sal(k)=nanmean(meas.transects(n).sensors.salinity_ppt.(temp).data);
%                                     end % if checked
%                                 end % for n
                                numsal=unique(sal);
                                if numsal>1
                                    temp='Varies';
                                else
                                    temp=num2str(sal(1));
                                end % numsal 
                            end
                            Salinity=clsMeasurement.createNode(docNode,Sensor,'Salinity','char',temp);
                            Salinity.setAttribute('unitsCode','ppt');
                            Sensor.appendChild(Salinity);

                            % SpeedofSound
                            clear temp
                            k=0;
                            for n=1:nTransects
                                if checked(n)
                                    k=k+1;
                                    temp{k}=meas.transects(n).sensors.speedOfSound_mps.selected;
                                    sos(k)=nanmean(meas.transects(n).sensors.speedOfSound_mps.(temp{k}).data);    
                                end % if checked
                            end
                            sources=unique(temp);
                            if length(sources)>1
                                temp='Varies';
                            else
                                temp=sources{1};
                            end
%                             temp=meas.transects(first).sensors.speedOfSound_mps.selected;
                            if strcmpi(temp,'internal')
                                temp='ADCP';
                            elseif strcmpi(temp,'user')
                                type=meas.transects(n).sensors.speedOfSound_mps.(temp).source;
                                if strcmpi(type,'Calculated')
                                    temp='Calc';
                                else
%                                     k=0;
%                                     for n=1:nTransects
%                                         if checked(n)
%                                             k=k+1;
%                                             
%                                         end % if checked
%                                     end % for n
                                    numsos=unique(sos);
                                    if numsos>1
                                        temp='Varies';
                                    else
                                        temp=num2str(sos(1));
                                    end % numsal     
                                end % if calculated
                            end % if user
                            SpeedofSound=clsMeasurement.createNode(docNode,Sensor,'SpeedofSound','char',temp);
                            SpeedofSound.setAttribute('unitsCode','mps');
                            Sensor.appendChild(SpeedofSound);
                    %% Transect Node
                    [width,widthCOV,area,areaCOV,avgBoatSpeed,avgBoatCourse,avgWaterSpeed,avgWaterDir,meanDepth,maxDepth,maxWaterSpeed]=clsTransectData.computeCharacteristics(meas.transects,discharge);
                    for n=1:nTransects 
                          
                            if checked(n)
                                Transect=clsMeasurement.createNode(docNode,docNode,'Transect'); 
                                % Filename
                                temp=meas.transects(n).fileName;
                                Filename=clsMeasurement.createNode(docNode,Sensor,'Filename','char',temp);
                                Transect.appendChild(Filename);

                                % StartDateTime
                                temp=datestr(meas.transects(n).dateTime.startSerialTime, 'mm/dd/yyyy HH:MM:SS');
                                StartDateTime=clsMeasurement.createNode(docNode,Sensor,'StartDateTime','char',temp);
                                Transect.appendChild(StartDateTime);

                                % EndDateTime
                                temp=datestr(meas.transects(n).dateTime.endSerialTime, 'mm/dd/yyyy HH:MM:SS');
                                EndDateTime=clsMeasurement.createNode(docNode,Sensor,'EndDateTime','char',temp);
                                Transect.appendChild(EndDateTime);

                                %% Discharge Node
                                Discharge=clsMeasurement.createNode(docNode,docNode,'Discharge');
                                Transect.appendChild(Discharge);

                                    % Top
                                    temp=num2str(discharge(n).top);
                                    Top=clsMeasurement.createNode(docNode,Discharge,'Top','double',temp);
                                    Top.setAttribute('unitsCode','cms');
                                    Discharge.appendChild(Top);

                                    % Middle
                                    temp=num2str(discharge(n).middle);
                                    Middle=clsMeasurement.createNode(docNode,Discharge,'Middle','double',temp);
                                    Middle.setAttribute('unitsCode','cms');
                                    Discharge.appendChild(Middle);

                                    % Bottom
                                    temp=num2str(discharge(n).bottom);
                                    Bottom=clsMeasurement.createNode(docNode,Discharge,'Bottom','double',temp);
                                    Bottom.setAttribute('unitsCode','cms');
                                    Discharge.appendChild(Bottom);

                                    % Left
                                    temp=num2str(discharge(n).left);
                                    Left=clsMeasurement.createNode(docNode,Discharge,'Left','double',temp);
                                    Left.setAttribute('unitsCode','cms');
                                    Discharge.appendChild(Left);

                                    % Right
                                    temp=num2str(discharge(n).right);
                                    Right=clsMeasurement.createNode(docNode,Discharge,'Right','double',temp);
                                    Right.setAttribute('unitsCode','cms');
                                    Discharge.appendChild(Right);

                                    % Total
                                    temp=num2str(discharge(n).total);
                                    Total=clsMeasurement.createNode(docNode,Discharge,'Total','double',temp);
                                    Total.setAttribute('unitsCode','cms');
                                    Discharge.appendChild(Total);

                                    % MovingBedPercentCorrection
                                    temp=num2str(((discharge(n).total./discharge(n).totalUncorrected)-1).*100);
                                    MovingBedPercentCorrection=clsMeasurement.createNode(docNode,Discharge,'MovingBedPercentCorrection','double',temp);
                                    Discharge.appendChild(MovingBedPercentCorrection);

                                %% Edge Node
                                Edge=clsMeasurement.createNode(docNode,docNode,'Edge');
                                Transect.appendChild(Edge);

                                    % StartEdge
                                    temp=meas.transects(n).startEdge;
                                    StartEdge=clsMeasurement.createNode(docNode,Edge,'StartEdge','char',temp);
                                    Edge.appendChild(StartEdge);

                                    % RectangularEdgeMethod
                                    temp=meas.transects(n).edges.recEdgeMethod;
                                    RectangularEdgeMethod=clsMeasurement.createNode(docNode,Edge,'RectangularEdgeMethod','char',temp);
                                    Edge.appendChild(RectangularEdgeMethod);

                                    % VelocityMethod
                                    temp=meas.transects(n).edges.velMethod;
                                    VelocityMethod=clsMeasurement.createNode(docNode,Edge,'VelocityMethod','char',temp);
                                    Edge.appendChild(VelocityMethod);

                                    % LeftType
                                    temp=meas.transects(n).edges.left.type;
                                    LeftType=clsMeasurement.createNode(docNode,Edge,'LeftType','char',temp);
                                    Edge.appendChild(LeftType);

                                    % LeftEdgeCoefficient and LeftUserQ
                                    switch temp
                                        case 'User'
                                            temp=num2str(meas.transects(n).edges.left.userQ_cms);
                                            LeftUserQ=clsMeasurement.createNode(docNode,Edge,'LeftUserQ','double',temp);
                                            LeftUserQ.setAttribute('unitsCode','cms');
                                            Edge.appendChild(LeftUserQ);
                                        otherwise
                                            temp=clsQComp.edgeCoef('left',meas.transects(n));
                                            temp=num2str(temp);
                                            LeftEdgeCoefficient=clsMeasurement.createNode(docNode,Edge,'LeftEdgeCoefficient','double',temp);
                                            Edge.appendChild(LeftEdgeCoefficient);
                                    end % switch

                                    % LeftDistance
                                    temp=num2str(meas.transects(n).edges.left.dist_m);
                                    LeftDistance=clsMeasurement.createNode(docNode,Edge,'LeftDistance','double',temp);
                                    LeftDistance.setAttribute('unitsCode','m');
                                    Edge.appendChild(LeftDistance);

                                    % LeftNumberEnsembles
                                    temp=num2str(meas.transects(n).edges.left.numEns2Avg);
                                    LeftNumberEnsembles=clsMeasurement.createNode(docNode,Edge,'LeftNumberEnsembles','double',temp);
                                    Edge.appendChild(LeftNumberEnsembles);

                                    % RightType
                                    temp=meas.transects(n).edges.right.type;
                                    RightType=clsMeasurement.createNode(docNode,Edge,'RightType','char',temp);
                                    Edge.appendChild(RightType);

                                    % RightEdgeCoefficient and RightUserQ
                                    switch temp
                                        case 'User'
                                            temp=num2str(meas.transects(n).edges.right.userQ_cms);
                                            RightUserQ=clsMeasurement.createNode(docNode,Edge,'RightUserQ','double',temp);
                                            RightUserQ.setAttribute('unitsCode','cms');
                                            Edge.appendChild(RightUserQ);
                                        otherwise
                                            temp=clsQComp.edgeCoef('right',meas.transects(n));
                                            temp=num2str(temp);
                                            RightEdgeCoefficient=clsMeasurement.createNode(docNode,Edge,'RightEdgeCoefficient','double',temp);
                                            Edge.appendChild(RightEdgeCoefficient);
                                    end % switch

                                    % RightDistance
                                    temp=num2str(meas.transects(n).edges.right.dist_m);
                                    RightDistance=clsMeasurement.createNode(docNode,Edge,'RightDistance','double',temp);
                                    RightDistance.setAttribute('unitsCode','m');
                                    Edge.appendChild(RightDistance);

                                    % RightNumberEnsembles
                                    temp=num2str(meas.transects(n).edges.right.numEns2Avg);
                                    RightNumberEnsembles=clsMeasurement.createNode(docNode,Edge,'RightNumberEnsembles','double',temp);
                                    Edge.appendChild(RightNumberEnsembles);

                                %% Sensor Node
                                Sensor=clsMeasurement.createNode(docNode,docNode,'Sensor');
                                Transect.appendChild(Sensor);    

                                    % TemperatureSource
                                    temp=meas.transects(n).sensors.temperature_degC.selected;
                                    TemperatureSource=clsMeasurement.createNode(docNode,Sensor,'TemperatureSource','char',temp);
                                    Sensor.appendChild(TemperatureSource);

                                    % MeanTemperature
                                    temp=nanmean(meas.transects(n).sensors.temperature_degC.(temp).data);
                                    temp=num2str(temp);
                                    MeanTemperature=clsMeasurement.createNode(docNode,Sensor,'MeanTemperature','double',temp);
                                    MeanTemperature.setAttribute('unitsCode','degC');
                                    Sensor.appendChild(MeanTemperature);

                                    % MeanSalinity
                                    temp=meas.transects(n).sensors.salinity_ppt.selected;
                                    temp=nanmean(meas.transects(n).sensors.salinity_ppt.(temp).data);
                                    temp=num2str(temp);
                                    MeanSalinity=clsMeasurement.createNode(docNode,Sensor,'MeanSalinity','double',temp);
                                    MeanSalinity.setAttribute('unitsCode','ppt');
                                    Sensor.appendChild(MeanSalinity);

                                    % SpeedofSoundSource
                                    temp=meas.transects(n).sensors.speedOfSound_mps.selected;
                                    if strcmpi(temp,'internal')
                                        temp='ADCP';
                                    end
                                    if strcmpi(temp,'user')
                                        type=meas.transects(n).sensors.speedOfSound_mps.(temp).source;
                                        if strcmpi(type,'Calculated')
                                            temp='Calc';
                                        else
                                            temp='User'; 
                                        end % if calculated
                                    end % if user
                                    SpeedofSoundSource=clsMeasurement.createNode(docNode,Sensor,'SpeedofSoundSource','char',temp);
                                    Sensor.appendChild(SpeedofSoundSource);

                                    % SpeedofSound
                                    temp=meas.transects(n).sensors.speedOfSound_mps.selected;
                                    temp=nanmean(meas.transects(n).sensors.speedOfSound_mps.(temp).data);
                                    temp=num2str(temp);
                                    SpeedofSound=clsMeasurement.createNode(docNode,Sensor,'SpeedofSound','double',temp);
                                    SpeedofSound.setAttribute('unitsCode','mps');
                                    Sensor.appendChild(SpeedofSound);

                                %% Other Node
                                Other=clsMeasurement.createNode(docNode,docNode,'Other');
                                Transect.appendChild(Other);


                                    % Duration
                                    temp=num2str(meas.transects(n).dateTime.transectDuration_sec);
                                    Duration=clsMeasurement.createNode(docNode,Other,'Duration','double',temp);
                                    Duration.setAttribute('unitsCode','sec');
                                    Other.appendChild(Duration);

                                    % Width
                                    temp=num2str(width(n));
                                    Width=clsMeasurement.createNode(docNode,Other,'Width','double',temp);
                                    Width.setAttribute('unitsCode','m');
                                    Other.appendChild(Width);

                                    % Area
                                    temp=num2str(area(n));
                                    Area=clsMeasurement.createNode(docNode,Other,'Area','double',temp);
                                    Area.setAttribute('unitsCode','sqm');
                                    Other.appendChild(Area);

                                    % MeanBoatSpeed
                                    temp=num2str(avgBoatSpeed(n));
                                    MeanBoatSpeed=clsMeasurement.createNode(docNode,Other,'MeanBoatSpeed','double',temp);
                                    MeanBoatSpeed.setAttribute('unitsCode','mps');
                                    Other.appendChild(MeanBoatSpeed);

                                    % QoverA
                                    temp=num2str(avgWaterSpeed(n));
                                    QoverA=clsMeasurement.createNode(docNode,Other,'QoverA','double',temp);
                                    QoverA.setAttribute('unitsCode','mps');
                                    Other.appendChild(QoverA);

                                    % CourseMadeGood
                                    temp=num2str(avgBoatCourse(n));
                                    CourseMadeGood=clsMeasurement.createNode(docNode,Other,'CourseMadeGood','double',temp);
                                    CourseMadeGood.setAttribute('unitsCode','deg');
                                    Other.appendChild(CourseMadeGood);

                                    % MeanFlowDirection
                                    temp=num2str(avgWaterDir(n));
                                    MeanFlowDirection=clsMeasurement.createNode(docNode,Other,'MeanFlowDirection','double',temp);
                                    MeanFlowDirection.setAttribute('unitsCode','deg');
                                    Other.appendChild(MeanFlowDirection);

                                    % NumberofEnsembles
                                    temp=num2str(size(meas.transects(n).wVel.uProcessed_mps,2));
                                    NumberofEnsembles=clsMeasurement.createNode(docNode,Other,'NumberofEnsembles','double',temp);
                                    Other.appendChild(NumberofEnsembles);

                                    [validEns,validWTCells]=clsTransectData.validCellsEnsembles(meas.transects(n));

                                    % PercentInvalidBins
                                    temp=(1-(nansum(nansum(validWTCells))./nansum(nansum(meas.transects(n).wVel.cellsAboveSL)))).*100;
                                    temp=num2str(temp);
                                    PercentInvalidBins=clsMeasurement.createNode(docNode,Other,'PercentInvalidBins','double',temp);
                                    Other.appendChild(PercentInvalidBins);

                                    % PercentInvalidEnsembles
                                    temp=(1-(nansum(validEns)./size(meas.transects(n).wVel.uProcessed_mps,2))).*100;
                                    temp=num2str(temp);
                                    PercentInvalidEns=clsMeasurement.createNode(docNode,Other,'PercentInvalidEns','double',temp);
                                    Other.appendChild(PercentInvalidEns);

                                    % MeanPitch
                                    temp=meas.transects(n).sensors.pitch_deg.selected;
                                    temp=num2str(nanmean(meas.transects(n).sensors.pitch_deg.(temp).data));
                                    MeanPitch=clsMeasurement.createNode(docNode,Other,'MeanPitch','double',temp);
                                    MeanPitch.setAttribute('unitsCode','deg');
                                    Other.appendChild(MeanPitch);

                                    % MeanRoll
                                    temp=meas.transects(n).sensors.roll_deg.selected;
                                    temp=num2str(nanmean(meas.transects(n).sensors.roll_deg.(temp).data));
                                    MeanRoll=clsMeasurement.createNode(docNode,Other,'MeanRoll','double',temp);
                                    MeanRoll.setAttribute('unitsCode','deg');
                                    Other.appendChild(MeanRoll);

                                    % PitchStdDev
                                    temp=meas.transects(n).sensors.pitch_deg.selected;
                                    temp=num2str(nanstd(meas.transects(n).sensors.pitch_deg.(temp).data));
                                    PitchStdDev=clsMeasurement.createNode(docNode,Other,'PitchStdDev','double',temp);
                                    PitchStdDev.setAttribute('unitsCode','deg');
                                    Other.appendChild(PitchStdDev);

                                    % RollStdDev
                                    temp=meas.transects(n).sensors.roll_deg.selected;
                                    temp=num2str(nanstd(meas.transects(n).sensors.roll_deg.(temp).data));
                                    RollStdDev=clsMeasurement.createNode(docNode,Other,'RollStdDev','double',temp);
                                    RollStdDev.setAttribute('unitsCode','deg');
                                    Other.appendChild(RollStdDev);

                                    % ADCPDepth
                                    temp=meas.transects(n).depths.selected;
                                    temp=num2str(meas.transects(n).depths.(temp).draftUse_m);
                                    ADCPDepth=clsMeasurement.createNode(docNode,Other,'ADCPDepth','double',temp);
                                    ADCPDepth.setAttribute('unitsCode','m');
                                    Other.appendChild(ADCPDepth);
                            end % if checked
                    end % for n

                    %% ChannelSummary
                    ChannelSummary=clsMeasurement.createNode(docNode,docNode,'ChannelSummary'); 

                                %% Discharge Node
                                Discharge=clsMeasurement.createNode(docNode,docNode,'Discharge');
                                ChannelSummary.appendChild(Discharge);

                                    % Top
                                    temp=num2str(clsQAData.meanQ(discharge(checked),'top'));
                                    Top=clsMeasurement.createNode(docNode,Discharge,'Top','double',temp);
                                    Top.setAttribute('unitsCode','cms');
                                    Discharge.appendChild(Top);

                                    % Middle
                                    temp=num2str(clsQAData.meanQ(discharge(checked),'middle'));
                                    Middle=clsMeasurement.createNode(docNode,Discharge,'Middle','double',temp);
                                    Middle.setAttribute('unitsCode','cms');
                                    Discharge.appendChild(Middle);

                                    % Bottom
                                    temp=num2str(clsQAData.meanQ(discharge(checked),'bottom'));
                                    Bottom=clsMeasurement.createNode(docNode,Discharge,'Bottom','double',temp);
                                    Bottom.setAttribute('unitsCode','cms');
                                    Discharge.appendChild(Bottom);

                                    % Left
                                    temp=num2str(clsQAData.meanQ(discharge(checked),'left'));
                                    Left=clsMeasurement.createNode(docNode,Discharge,'Left','double',temp);
                                    Left.setAttribute('unitsCode','cms');
                                    Discharge.appendChild(Left);

                                    % Right
                                    temp=num2str(clsQAData.meanQ(discharge(checked),'right'));
                                    Right=clsMeasurement.createNode(docNode,Discharge,'Right','double',temp);
                                    Right.setAttribute('unitsCode','cms');
                                    Discharge.appendChild(Right);

                                    % Total
                                    temp=num2str(clsQAData.meanQ(discharge(checked),'total'));
                                    Total=clsMeasurement.createNode(docNode,Discharge,'Total','double',temp);
                                    Total.setAttribute('unitsCode','cms');
                                    Discharge.appendChild(Total);

                                    % MovingBedPercentCorrection
                                    temp=num2str(((clsQAData.meanQ(discharge(checked),'total')./clsQAData.meanQ(discharge(checked),'totalUncorrected'))-1).*100);
                                    MovingBedPercentCorrection=clsMeasurement.createNode(docNode,Discharge,'MovingBedPercentCorrection','double',temp);
                                    Discharge.appendChild(MovingBedPercentCorrection);

                                %% Uncertainty Node
                                Uncertainty=clsMeasurement.createNode(docNode,docNode,'Uncertainty');
                                ChannelSummary.appendChild(Uncertainty);


                                    % COV
                                    temp=num2str(uncertainty.cov);
                                    COV=clsMeasurement.createNode(docNode,Uncertainty,'COV','double',temp);
                                    Uncertainty.appendChild(COV);

                                    % Random
                                    temp=num2str(uncertainty.cov95);
                                    AutoRandom=clsMeasurement.createNode(docNode,Uncertainty,'AutoRandom','double',temp);
                                    Uncertainty.appendChild(AutoRandom);

                                    % InvalidData
                                    temp=num2str(uncertainty.invalid95);
                                    AutoInvalidData=clsMeasurement.createNode(docNode,Uncertainty,'AutoInvalidData','double',temp);
                                    Uncertainty.appendChild(AutoInvalidData);

                                    % Edge
                                    temp=num2str(uncertainty.edges95);
                                    AutoEdge=clsMeasurement.createNode(docNode,Uncertainty,'AutoEdge','double',temp);
                                    Uncertainty.appendChild(AutoEdge);

                                    % Extrapolation
                                    temp=num2str(uncertainty.extrapolation95);
                                    AutoExtrapolation=clsMeasurement.createNode(docNode,Uncertainty,'AutoExtrapolation','double',temp);
                                    Uncertainty.appendChild(AutoExtrapolation);

                                    % MovingBed
                                    temp=num2str(uncertainty.movingBed95);
                                    AutoMovingBed=clsMeasurement.createNode(docNode,Uncertainty,'AutoMovingBed','double',temp);
                                    Uncertainty.appendChild(AutoMovingBed);
                                    
                                    % Systematic
                                    temp=num2str(uncertainty.systematic);
                                    AutoSystematic=clsMeasurement.createNode(docNode,Uncertainty,'AutoSystematic','double',temp);
                                    Uncertainty.appendChild(AutoSystematic);

                                    % TotalAuto
                                    temp=num2str(uncertainty.total95);
                                    AutoTotal=clsMeasurement.createNode(docNode,Uncertainty,'AutoTotal','double',temp);
                                    Uncertainty.appendChild(AutoTotal);

                                    % UserRandom
                                    temp=num2str(uncertainty.cov95User);
                                    if ~isempty(temp)
                                        UserRandom=clsMeasurement.createNode(docNode,Uncertainty,'UserRandom','double',temp);
                                        Uncertainty.appendChild(UserRandom);
                                    end

                                    % UserInvalidData
                                    temp=num2str(uncertainty.invalid95User);
                                    if ~isempty(temp)
                                        UserInvalidData=clsMeasurement.createNode(docNode,Uncertainty,'UserInvalidData','double',temp);
                                        Uncertainty.appendChild(UserInvalidData);
                                    end

                                    % UserEdge
                                    temp=num2str(uncertainty.edges95User);
                                    if ~isempty(temp)
                                        UserEdge=clsMeasurement.createNode(docNode,Uncertainty,'UserEdge','double',temp);
                                        Uncertainty.appendChild(UserEdge);
                                    end

                                    % UserExtrapolation
                                    temp=num2str(uncertainty.extrapolation95User);
                                    if ~isempty(temp)
                                        UserExtrapolation=clsMeasurement.createNode(docNode,Uncertainty,'UserExtrapolation','double',temp);
                                        Uncertainty.appendChild(UserExtrapolation);
                                    end

                                    % UserMovingBed
                                    temp=num2str(uncertainty.movingBed95User);
                                    if ~isempty(temp)
                                        UserMovingBed=clsMeasurement.createNode(docNode,Uncertainty,'UserMovingBed','double',temp);
                                        Uncertainty.appendChild(UserMovingBed);
                                    end
                                    
                                    % UserSystematic
                                    temp=num2str(uncertainty.systematicUser);
                                    if ~isempty(temp)
                                        UserSystematic=clsMeasurement.createNode(docNode,Uncertainty,'UserSystematic','double',temp);
                                        Uncertainty.appendChild(UserSystematic);
                                    end
                                   
                                    % User Total
                                    temp=num2str(uncertainty.total95User);
                                    UserTotal=clsMeasurement.createNode(docNode,Uncertainty,'UserTotal','double',temp);
                                    Uncertainty.appendChild(UserTotal);

                                    % Random
                                    if exist('UserRandom')
                                        temp=num2str(uncertainty.cov95User,'%5.1f');
                                        temp=[temp,'*'];
                                    else
                                        temp=num2str(uncertainty.cov95,'%5.1f');
                                    end
                                    Random=clsMeasurement.createNode(docNode,Uncertainty,'Random','char',temp);
                                    Uncertainty.appendChild(Random);

                                    % InvalidData
                                    if exist('UserInvalidData')
                                        temp=num2str(uncertainty.invalid95User,'%5.1f');
                                        temp=[temp,'*'];
                                    else
                                        temp=num2str(uncertainty.invalid95,'%5.1f');
                                    end
                                    InvalidData=clsMeasurement.createNode(docNode,Uncertainty,'InvalidData','char',temp);
                                    Uncertainty.appendChild(InvalidData);

                                    % Edge
                                    if exist('UserEdge')
                                        temp=num2str(uncertainty.edges95User,'%5.1f');
                                        temp=[temp,'*'];
                                    else
                                        temp=num2str(uncertainty.edges95,'%5.1f');
                                    end
                                    Edge=clsMeasurement.createNode(docNode,Uncertainty,'Edge','char',temp);
                                    Uncertainty.appendChild(Edge);

                                    % Extrapolation
                                    if exist('UserExtrapolation')
                                        temp=num2str(uncertainty.extrapolation95User,'%5.1f');
                                        temp=[temp,'*'];
                                    else
                                        temp=num2str(uncertainty.extrapolation95,'%5.1f');
                                    end
                                    Extrapolation=clsMeasurement.createNode(docNode,Uncertainty,'Extrapolation','char',temp);
                                    Uncertainty.appendChild(Extrapolation);

                                    % MovingBed
                                    if exist('UserMovingBed')
                                        temp=num2str(uncertainty.movingBed95User,'%5.1f');
                                        temp=[temp,'*'];
                                    else
                                        temp=num2str(uncertainty.movingBed95,'%5.1f');
                                    end
                                    MovingBed=clsMeasurement.createNode(docNode,Uncertainty,'MovingBed','char',temp);
                                    Uncertainty.appendChild(MovingBed);
                                    
                                    % Systematic
                                    if exist('UserSystematic')
                                        temp=num2str(uncertainty.systematicUser,'%5.1f');
                                        temp=[temp,'*'];
                                    else
                                        temp=num2str(uncertainty.systematic,'%5.1f');
                                    end
                                    Systematic=clsMeasurement.createNode(docNode,Uncertainty,'Systematic','char',temp);
                                    Uncertainty.appendChild(Systematic);

                                    % Total
                                    user=[uncertainty.cov95User,uncertainty.invalid95User,uncertainty.edges95User,uncertainty.extrapolation95User,uncertainty.movingBed95User,uncertainty.systematicUser];
                                    if ~isempty(user)
                                        temp=num2str(uncertainty.total95User,'%5.1f');
                                        temp=[temp,'*'];
                                    else
                                        temp=num2str(uncertainty.total95,'%5.1f');
                                    end
                                    Total=clsMeasurement.createNode(docNode,Uncertainty,'Total','char',temp);
                                    Uncertainty.appendChild(Total);

                                %% Other Node
                                Other=clsMeasurement.createNode(docNode,docNode,'Other');
                                ChannelSummary.appendChild(Other);

                                    % MeanWidth
                                    temp=num2str(width(end));
                                    MeanWidth=clsMeasurement.createNode(docNode,Other,'MeanWidth','double',temp);
                                    MeanWidth.setAttribute('unitsCode','m');
                                    Other.appendChild(MeanWidth);
                                    
                                    % WidthCOV
                                    temp=num2str(widthCOV);
                                    WidthCOV=clsMeasurement.createNode(docNode,Other,'WidthCOV','double',temp);
                                    Other.appendChild(WidthCOV);
                                    
                                    % MeanArea
                                    temp=num2str(area(end));
                                    MeanArea=clsMeasurement.createNode(docNode,Other,'MeanArea','double',temp);
                                    MeanArea.setAttribute('unitsCode','sqm');
                                    Other.appendChild(MeanArea);
                                    
                                    % AreaCOV
                                    temp=num2str(areaCOV);
                                    AreaCOV=clsMeasurement.createNode(docNode,Other,'AreaCOV','double',temp);
                                    Other.appendChild(AreaCOV);

                                    % MeanBoatSpeed
                                    temp=num2str(avgBoatSpeed(end));
                                    MeanBoatSpeed=clsMeasurement.createNode(docNode,Other,'MeanBoatSpeed','double',temp);
                                    MeanBoatSpeed.setAttribute('unitsCode','mps');
                                    Other.appendChild(MeanBoatSpeed);

                                    % MeanQoverA
                                    temp=num2str(avgWaterSpeed(end));
                                    MeanQoverA=clsMeasurement.createNode(docNode,Other,'MeanQoverA','double',temp);
                                    MeanQoverA.setAttribute('unitsCode','mps');
                                    Other.appendChild(MeanQoverA);

                                    % MeanCourseMadeGood
                                    temp=num2str(avgBoatCourse(end));
                                    MeanCourseMadeGood=clsMeasurement.createNode(docNode,Other,'MeanCourseMadeGood','double',temp);
                                    MeanCourseMadeGood.setAttribute('unitsCode','deg');
                                    Other.appendChild(MeanCourseMadeGood);

                                    % MeanFlowDirection
                                    temp=num2str(avgWaterDir(end));
                                    MeanFlowDirection=clsMeasurement.createNode(docNode,Other,'MeanFlowDirection','double',temp);
                                    MeanFlowDirection.setAttribute('unitsCode','deg');
                                    Other.appendChild(MeanFlowDirection);

                                    % MeanDepth
                                    temp=num2str(meanDepth(end));
                                    MeanDepth=clsMeasurement.createNode(docNode,Other,'MeanDepth','double',temp);
                                    MeanDepth.setAttribute('unitsCode','m');
                                    Other.appendChild(MeanDepth);

                                    % MaximumDepth
                                    temp=num2str(maxDepth(end));
                                    MaximumDepth=clsMeasurement.createNode(docNode,Other,'MaximumDepth','double',temp);
                                    MaximumDepth.setAttribute('unitsCode','m');
                                    Other.appendChild(MaximumDepth);

                                    % MaximumWaterSpeed
                                    temp=num2str(maxWaterSpeed(end));
                                    MaximumWaterSpeed=clsMeasurement.createNode(docNode,Other,'MaximumWaterSpeed','double',temp);
                                    MaximumWaterSpeed.setAttribute('unitsCode','mps');
                                    Other.appendChild(MaximumWaterSpeed);

                                    % NumberofTransects
                                    temp=num2str(nansum(checked));
                                    NumberofTransects=clsMeasurement.createNode(docNode,Other,'NumberofTransects','integer',temp);
                                    Other.appendChild(NumberofTransects);

                                    % Duration
                                    for n=1:nTransects    
                                        duration(n)=meas.transects(n).dateTime.transectDuration_sec;
                                    end
                                    temp=num2str(nansum(duration(checked)));
                                    Duration=clsMeasurement.createNode(docNode,Other,'Duration','double',temp);
                                    Duration.setAttribute('unitsCode','sec');
                                    Other.appendChild(Duration);

                                    % Percent Q
                                    meanQ=clsQAData.meanQ(meas.discharge(checked),'total');
                                    meanLeft=clsQAData.meanQ(meas.discharge(checked),'left');
                                    perLeft=(meanLeft./meanQ).*100; 
                                    meanRight=clsQAData.meanQ(meas.discharge(checked),'right');
                                    perRight=(meanRight./meanQ).*100;
                                    perCells=(clsQAData.meanQ(meas.discharge(checked),'intCells')./meanQ).*100;
                                    perEns=(clsQAData.meanQ(meas.discharge(checked),'intEns')./meanQ).*100;
                                    
                                    % Left Edge
                                    temp=num2str(perLeft);
                                    LeftQPer=clsMeasurement.createNode(docNode,Other,'LeftQPer','double',temp);
                                    Other.appendChild(LeftQPer);         
                                    
                                    % Right Edge
                                    temp=num2str(perRight);
                                    RightQPer=clsMeasurement.createNode(docNode,Other,'RightQPer','double',temp);
                                    Other.appendChild(RightQPer);  
                                    
                                    % Invalid Cells
                                    temp=num2str(perCells);
                                    InvalidCellsQPer=clsMeasurement.createNode(docNode,Other,'InvalidCellsQPer','double',temp);
                                    Other.appendChild(InvalidCellsQPer); 
                                    
                                    % Invalid Ensembles
                                    temp=num2str(perEns);
                                    InvalidEnsQPer=clsMeasurement.createNode(docNode,Other,'InvalidEnsQPer','double',temp);
                                    Other.appendChild(InvalidEnsQPer);                                      
                                    
                                    % User Rating
                                    UserRating=clsMeasurement.createNode(docNode,Other,'UserRating','text',meas.userRating);
                                    Other.appendChild(UserRating);
                                    
                                    % Q P/P 1/6th
                                    DischargePPDefault=clsMeasurement.createNode(docNode,Other,'DischargePPDefault','double',num2str(meas.extrapFit.qSensitivity.qPPmean));
                                    Other.appendChild(DischargePPDefault);
                                    
                                    % User input messages
                                    nmessages=length(meas.comments);
                                    if nmessages>0
                                        % Consolidate messages
                                        nout=0;
                                        messageout={};
                                        for j=1:nmessages
                                            nRows=size(meas.comments{j},1);
                                            for row=1:nRows
                                                
                                                if size(meas.comments{j}{row}, 1) == 1
                                                    nout=nout+1;

                                                    if sum(meas.comments{j}{row}==' ')==length(meas.comments{j}{row})
                                                        messageout(nout)={'  '};
                                                    else
                                                        messageout(nout)={[strtrim(meas.comments{j}{row}) '  ']};
                                                    end
                                                else
                                                    for subrow=1:size(meas.comments{j}{row}, 1)
                                                       nout=nout+1;
                                                       if sum(meas.comments{j}{1}(subrow,:)==' ')==length(meas.comments{j}{1}(subrow,:))
                                                            messageout(nout)={'  '};
                                                       else
                                                            messageout(nout)={[strtrim(meas.comments{j}{1}(subrow,:)) '  ']};
                                                       end 
                                                    end
                                                end
                                            end
                                            messageout(nout)={[messageout{nout} '|||']};
                                        end
                                    else
                                        messageout={};
                                    end

                                    % Convert to character string and output to GUI
                                    if ~isempty(messageout)
%                                         outstr=sprintf('%s|||',messageout{:});  
                                        outstr=sprintf('%s',messageout{:}); 
                                        UserComment=clsMeasurement.createNode(docNode,QA,'UserComment','char',outstr);
                                        Channel.appendChild(UserComment);  
                                    end % if outstr 
                                    
           % Add processing instruction for stylesheet to xml file
            docRootNode = docNode.getDocumentElement;  
            style = docNode.createProcessingInstruction ( 'xml-stylesheet', 'type= "text/xsl" href="QRevStylesheet.xsl"');                       
            docNode.insertBefore (style, docRootNode);
            
            % Create xml file
            xmlwrite([filename '.xml'],docNode);
            ok=true;
         catch exception
             ok=false;      
             msgString=getReport(exception, 'extended','hyperlinks','off');
             errordlg(msgString);
         end % try
        end % createXMLFile
        
        function newNode = createNode(docNode, parent, child, varargin )
        % Creates a newNode in the XML structure.

            newNode=docNode.createElement(child);
            if ~isempty(varargin)
                newNode.setAttribute('type',varargin{1});
                newNode.appendChild(docNode.createTextNode(varargin{2})); 
            else
                parent.getDocumentElement.appendChild(newNode);      
            end

        end % createNode
       
        % Object to Structure
        % -------------------
        function s=obj2struct (objIn)
            % Converts an object, including nested objects to a data structure
            % resembling the object.
                objLength=length(objIn);
                for k=1:objLength
                    s(1,k)=struct(objIn(k));
                    names=fieldnames(s(1,k));
                    nNames=length(names);
                    for n=1:nNames
                        className=class(s(1,k).(names{n}));
                        if strcmp(className(1:3),'cls')
                           s(1,k).(names{n})=clsMeasurement.obj2struct(s(1,k).(names{n}));
                        end
                    end
                end
        end
        
         
    end % Static methods
end % class

