classdef clsQComp
% Computes the discharge for each transect. 
%
% Originally developed as part of the QRev processing program
% Programmer: David S. Mueller, Hydrologist, USGS, OSW, dmueller@usgs.gov
% Other Contributors: none
% Latest revision: 3/13/2015
    
    properties (SetAccess=private)
        top % Transect total extrapolated top discharge
        middle % Transect total measured middle discharge including interpolations
        bottom % Transect total extrapolated bottom discharge
        topEns % Extrapolated top discharge by ensemble
        middleCells % Measured middle discharge including interpolation by cell
        middleEns % Measured middle discharge including interpolations by ensemble
        bottomEns % Extrapolate bottom discharge by ensemble
        left % Left edge discharge
        leftidx % Ensembles used for left edge
        right % Right edge discharge
        rightidx % Ensembles used for righ edge
        totalUncorrected % Total discharge for transect uncorrected for moving-bed, if required
        total % Total discharge with moving-bed correction applied if necessary
        correctionFactor % Moving-bed correction factor, if required
        intCells % Total discharge computed for invalid depth cells excluding invalid ensembles
        intEns % Total discharge computed for invalid ensembles
    end % properties
    
    methods
        function obj=clsQComp(dataIn,varargin)
        % Construct the clsQComp object. Discharge is computed using the
        % data provided to the method. Water data provided are assumed to
        % be corrected for the navigation reference. If a moving-bed
        % correction is to be applied it is computed and applied. The TRDI
        % method using expanded delta time is applied if the processing
        % method is WR2.
        %
        % INPUT:
        % 
        % dataIn: Object of clsTransectData or clsMeasurement. 
        % 
        % varargin: Allows overriding the extrapolation methods specified
        % in dataIn.
        %   varargin{1}: top extrapolation method
        %   varargin{2}: bottom extrapolation method
        %   varargin{3}: extrapolation exponent
        
            % Determine if input data has been provide
            if nargin>0  

                % Determine type of data provided
                if strcmp(class(dataIn),'clsTransectData')
                    transData=dataIn;
                    
                    % Use bottom track interpolation settings to determine
                    % the appropriate algorithms to apply.
                    if strcmpi(transData(1).boatVel.btVel.interpolate,'None')
                        processing='WR2';
                    elseif strcmpi(transData(1).boatVel.btVel.interpolate,'Linear')
                        processing='QRev';
                    else
                        processing='RSL';
                    end
                    
                else
                    % If the dataIn is clsMeasurement assign variables.
                    meas=dataIn;
                    transData=meas.transects;
                    processing=meas.processing;
                end
                
                % Determine number of transects
                nTransects=length(transData);
                
                % Preallocate class
                obj(nTransects)=clsQComp();
                
                % Set moving-bed correction flag to false. May be overriden
                % later.
                correctionFlag=false;
                
                % Process each transect
                for n=1:nTransects
                    obj(n).correctionFactor=1;
                    % Compute cross product
                    xprod=obj.crossProduct(transData(n));
                    
                    % Get index of ensembles in moving-boat portion of
                    % transect
                    inTransectIdx=transData(n).inTransectIdx;
                    
                    % If method is TRDI adjust deltaT
                    if strcmpi(processing,'WR2') 
                        % TRDI uses expanded delta time to handle invalid
                        % ensembles which can be caused by invalid BT,
                        % WT, or depth. QRev by default handles this invalid
                        % data through linear interpolation of the invalid
                        % data type. This if statement and associated code is
                        % required to maintain compatibility with WinRiver
                        % II discharge computations.
                        
                        % Determine valid ensembles
                        validEns=any(~isnan(xprod));
                        validEns=validEns(inTransectIdx);
                        
                        % Compute the ensemble duration using TRDI approach
                        % of expanding delta time to compensate for invalid
                        % ensembles.
                        nEns=length(validEns);
                        ensDur=transData(n).dateTime.ensDuration_sec(inTransectIdx);
                        deltaT=nan(1,nEns);
                        cumDur=0;
                        idx=1;
                        for j=idx:nEns
                            cumDur=nansum([cumDur,ensDur(j)]);
                            if validEns(j)
                                deltaT(j)=cumDur;
                                cumDur=0;
                            end
                        end % for
                        
                    else
                        % For non-WR2 processing use actual ensemble
                        % duration.
                        deltaT=transData(n).dateTime.ensDuration_sec(inTransectIdx);
                    end % if
                        
                    % Compute measured or middle discharge
                    qMidCells=obj.dischargeMiddleCells(xprod,transData(n),deltaT);
                    obj(n).middleCells=qMidCells;
                    obj(n).middleEns=nansum(qMidCells,1);
                                    
                    % Compute the top discharge
                    obj(n).topEns=obj.dischargeTop(xprod,transData(n),deltaT,varargin{:});

                    obj(n).top=nansum(obj(n).topEns);       

                    % Compute the bottom discharge
                    obj(n).bottomEns=obj.dischargeBot(xprod,transData(n),deltaT,varargin{:});
                    obj(n).bottom=nansum(obj(n).bottomEns);

                    % Compute interpolated cell and ensemble discharge from
                    % computed measured discharge
                    obj(n)=interpolateNoCells(obj(n),transData(n));
                    obj(n).middle=nansum(obj(n).middleEns);
                    [obj(n).intCells, obj(n).intEns]= obj.dischargeInterpolated(obj(n).topEns,qMidCells,obj(n).bottomEns,transData(n));
                    
                    % Compute right edge discharge
                    if ~strcmp(transData(n).edges.right.type,'User Q')
                        obj(n).right=obj.dischargeEdge('right',transData(n),varargin{:});
                    else
                        obj(n).right=transData(n).edges.right.userQ_cms;
                    end
                    
                    % Compute left edge discharge
                    if ~strcmp(transData(n).edges.left.type,'User Q')
                        obj(n).left=obj.dischargeEdge('left',transData(n),varargin{:});
                    else
                        obj(n).left=transData(n).edges.left.userQ_cms;
                    end
                    
                    % Compute moving-bed correction, if applicable. Two
                    % checks are used to account for the way the meas object
                    % is created.
                    
                    % Check to see if the mbTests property of
                    % clsMeasurement exists
                    if isprop(dataIn,'mbTests')
                        mbData=meas.mbTests;
                        
                        % Determine if the mbData object is empty
                        if ~isempty(mbData)
                            
                            % Determine if any of the moving-bed tests
                            % indicated a moving-bed
                            mbValid=[mbData.selected];
                            mb=ismember({mbData(mbValid).movingBed},'Yes');
                            if sum(mb)>0
                                use2Correct=logical([mbData(:).use2Correct]);
                                
                                % Determine if a moving-bed test is to be
                                % used for correction.
                                if sum(use2Correct)>0
                                    
                                    % Make sure bottom track is the
                                    % navigation reference and composite
                                    % tracks are turned off.
                                    if strcmp(transData(n).boatVel.selected,'btVel') && strcmp(transData(n).boatVel.composite,'Off')
                                        
                                        % Apply appropriate moving-bed test
                                        % correction method
                                        if sum(ismember({mbData(use2Correct).type},'Stationary'))>0
                                            obj(n).correctionFactor=clsQComp.stationaryCorrection(obj(n).top,obj(n).middle,obj(n).bottom,transData(n),mbData,deltaT);
                                        else
                                            obj(n).correctionFactor=clsQComp.loopCorrection(obj(n).top,obj(n).middle,obj(n).bottom,transData(n),mbData(use2Correct),deltaT);
                                        end % if 
                                    else
                                        % Set a flag to generate a warning.
                                        correctionFlag=true;
                                    end % if 
                                end % if use2Correct
                            end % if mb
                        end % if empty
                    end % if mbTests
                    
                    % Compute total discharge
                    obj(n).totalUncorrected=nansum([obj(n).top,obj(n).middle,obj(n).bottom,obj(n).left,obj(n).right]);
                    
                    % Compute final discharge using correction if
                    % applicable.
                    if isempty(obj(n).correctionFactor) || obj(n).correctionFactor==1
                        obj(n).total=obj(n).totalUncorrected;
                    else
                        obj(n).total=obj(n).left+obj(n).right+(obj(n).middle+obj(n).bottom+obj(n).top).*obj(n).correctionFactor;
                    end % if correctionFactor
                    
                end % for n
                if correctionFlag
                    warndlg('To apply moving-bed correction you must be referenced to BT with composite tracks turned off','Moving-bed Correction');
                end
            end % if nargin
        end % constructor     
        
        function obj=interpolateNoCells(obj,transData)
            % Computes discharge for ensembles where the depth is too
            % shallow for any valid depth cells. The computation is done
            % using interpolation of unit discharge defined as the ensemble
            % discharge divided by the depth of the ensemble and the
            % duration of the ensemble. The independent variable for the
            % interpolation is the track distance. After interpolation the
            % discharge for the interpolated ensembles is computed by
            % multiplying the interpolated value by the depth and duration
            % of those ensembles to achieve discharge for those ensembles.
            %
            % INPUT:
            % obj: object of clsQComp
            % transData: single object of clsTransectData
            %
            % OUTPUT:
            % obj: object of clsQComp with middleEns updated to include
            % interpolated ensembles.
            
            % Compute the discharge in each ensemble
            qByEnsemble=obj.topEns+obj.middleEns+obj.bottomEns;
            % Identify ensembles with valid discharge
            validQ=find(~isnan(qByEnsemble));
            % Compute the unit discharge by depth for each ensemble
            unitQByDepth=(qByEnsemble./transData.depths.(transData.depths.selected).depthProcessed_m(transData.inTransectIdx))./transData.dateTime.ensDuration_sec(transData.inTransectIdx);

            % Compute the ship track
            boatVelX=transData.boatVel.(transData.boatVel.selected).uProcessed_mps;
            boatVelY=transData.boatVel.(transData.boatVel.selected).vProcessed_mps;
            trackX=boatVelX.*transData.dateTime.ensDuration_sec;
            trackY=boatVelY.*transData.dateTime.ensDuration_sec;
            x=nancumsum(sqrt(trackX.^2+trackY.^2));

            % Create strict monotonic vector for 1-D interpolation
            % ====================================================
            qMono=unitQByDepth;
            xMono=x(transData.inTransectIdx);
            % Identify duplicate values
            idx0=find(diff(x)==0);
            % Eliminate duplicate values
            if ~isempty(idx0)
                % Create cells that include indices of groups of repeated
                % data values
                group=mat2cell(idx0,1,diff([0,find(diff(idx0) ~= 1),length(idx0)]));
                % Process each group of repeated data values to eliminate
                % the duplication. The dependent variable is computed as
                % the mean value associated with duplicated independent
                % variable.
                nGroup=length(group);
                for k=1:nGroup
                    indices=group{k};
                    indices=[indices, indices(end)+1];
                    firstidx=indices(1);
                    qavg=nanmean(qMono(indices),2);
                    qMono(indices(1))=qavg;
                    qMono(indices(2:end))=nan;
                    xMono(indices(2:end))=nan;
                end % for k
            end % if idx0
            % Determine valid indices for monitonic vector
            validqMono=~isnan(qMono);
            validxMono=~isnan(xMono);
            valid=all([validqMono;validxMono]);

            if sum(valid)>1
                % Compute interpolation function from all valid data
                unitQInt = interp1(xMono(valid),qMono(valid),x);
            else
                unitQInt=0;
            end % if valid
            
            % Compute the discharge in each ensemble based on interpolated
            % data.
            qInt=unitQInt.*transData.depths.(transData.depths.selected).depthProcessed_m.*transData.dateTime.ensDuration_sec;
            qInt=qInt(transData.inTransectIdx);
            % Update the middleEns with interpolated discharges.
            idx=find(isnan(qByEnsemble));
            obj.middleEns(idx)=qInt(idx);
        end
        
        
        function obj=resetProperty(obj,property,setting)
            obj.(property)=setting;
        end        
        
    end % methods     
    
    methods (Static)
        
        % Measured or middle discharge
        % ----------------------------
        function xprod=crossProduct(varargin)
        % Computes the cross product using data from an object of
        % clsTransectData or input for water and navigation arrays.
        %
        % INPUT:
        %
        % varargin: variable argument in allows either an object of
        % clsTransectData or arrays of water and navigation data to be
        % used.
        %   if clsTransectData
        %       varargin(1) is an object of clsTransectData and that is
        %       only input
        %   if arrays of water and navigation data
        %       varargin{1} is array of water velocity in x-direction
        %       varargin{2} is array of water velocity in y-direction
        %       varargin{3} is vector of navigation velocity in x-direction
        %       varargin{4} is vector of navigation velocity in y-direction
        %
        % OUTPUT:
        %
        % xprod: cross product of navigation and water velocities
        
            % Determine type of input
            if nargin<2
                
                % Assign object of clsTransectData to local variable
                transect=varargin{1};
                
                % Prepare water track data
                cellsAboveSL=double(transect.wVel.cellsAboveSL);
                cellsAboveSL(cellsAboveSL<0.5)=nan;
                wVelx=transect.wVel.uProcessed_mps.*cellsAboveSL;
                wVely=transect.wVel.vProcessed_mps.*cellsAboveSL;
                
                % Get navigation data from object properties
                if ~isempty(transect.boatVel.(transect.boatVel.selected))
                    bVelx=transect.boatVel.(transect.boatVel.selected).uProcessed_mps;
                    bVely=transect.boatVel.(transect.boatVel.selected).vProcessed_mps;
                else
                    bVelx=nan(size(transect.boatVel.btVel.uProcessed_mps));
                    bVely=nan(size(transect.boatVel.btVel.vProcessed_mps));
                end
                % Get start edge
                startEdge=transect.startEdge;
            else
                % Assign data arrays to local variables
                wVelx=varargin{1};
                wVely=varargin{2};
                bVelx=varargin{3};
                bVely=varargin{4};
                startEdge=varargin{5};
            end
            
            % Compute cross product
            xprod=bsxfun(@times,wVelx,bVely)-bsxfun(@times,wVely,bVelx);
            
            % Determine sign of cross product
            if strcmp(startEdge,'Right')
                dir=1;
            else
                dir=-1;
            end
            xprod=xprod.*dir;
            
        end % crossProduct
        
        function qMidCells=dischargeMiddleCells(xprod,transect,deltaT)
        % Computes the discharge in the measured or middle portion of the
        % cross section.
        %
        % INPUT:
        % 
        % xprod: cross product computed from the crossProduct method
        % transect: object of clsTransectData
        
        % deltaT: duration of each ensemble computed from clsQComp
        % constructor
        %
        % OUTPUT:
        % qMidCells: discharge in each bin or depth cell
        
            % Assign properties from transect object to local variables
            inTransectIdx=transect.inTransectIdx;
            cellSize=transect.depths.(transect.depths.selected).depthCellSize_m;
            
            % Determine is xprod contains edge data and process
            % appropriately
            if size(xprod,2)>size(inTransectIdx,2)
                qMidCells=bsxfun(@times,xprod(:,inTransectIdx).*cellSize(:,inTransectIdx),deltaT);
            else
                qMidCells=bsxfun(@times,xprod.*cellSize(:,inTransectIdx),deltaT);
            end
            
        end % dischargeMeasured

        % Top extraplation
        % ----------------
        function qTop=dischargeTop(xprod,transect,deltaT,varargin)
        % Coordinates computation of the extrapolated top discharge.
        %
        % INPUT:
        % 
        % xprod: cross product computed from the crossProduct method
        %
        % transect: object of clsTransectData
        %
        % deltaT: duration of each ensemble computed from clsQComp
        % constructor
        %
        % varargin: allows specifying top and bottom extrapolations
        % independent of transect object
        %   varargin{1}: top extrapolation method
        %   varargin{2}: not used in this method
        %   varargin{3}: exponent
        %
        %
        % OUTPUT:
        % qTop: top extrapolated discharge for each ensemble
            
            % Determine extrapolation methods and exponent
            if ~isempty(varargin)
                topMethod=varargin{1};
                exponent=varargin{3};
            else
                topMethod=transect.extrap.topMethod;
                exponent=transect.extrap.exponent;
            end
            
            % Get index for ensembles in moving-boat portion of transect
            inTransectIdx=transect.inTransectIdx;
            
            % Compute top variables
            [idxTop,idxTop3,topRng]=clsQComp.topVariables(xprod,transect);
            idxTop=idxTop(inTransectIdx);
            idxTop3=idxTop3(:,inTransectIdx);
            topRng=topRng(inTransectIdx);
            
            % Get data from transect object
            cellSize=transect.depths.(transect.depths.selected).depthCellSize_m(:,inTransectIdx);
            cellDepth=transect.depths.(transect.depths.selected).depthCellDepth_m(:,inTransectIdx);
            depthEns=transect.depths.(transect.depths.selected).depthProcessed_m(inTransectIdx);
            
            % Compute z
            z=bsxfun(@minus,depthEns,cellDepth);
            z(z<0.5.*cellSize)=nan;
            
            % Use only valid data
            validData=~isnan(xprod(:,inTransectIdx));
            z(~validData)=nan;
            cellSize(~validData)=nan;
            cellDepth(~validData)=nan;
            
            % Compute top discharge
            qTop=clsQComp.extrapolateTop(topMethod,exponent,idxTop,idxTop3,topRng,...
                    xprod(:,inTransectIdx),cellSize,cellDepth,depthEns,deltaT,z);
                
        end % dischargeTop
        
        function topValue=extrapolateTop(topMethod,exponent,idxTop,idxTop3,topRng,...
                    component,cellSize,cellDepth,depthEns,deltaT,z)
        % Computes the top extrapolated value of the provided component 
        % using the specified extrapolation method.
        %
        % INPUT:
        % 
        % topMethod: top extrapolation method (Power,Constant,3-Point)
        %
        % exponent: exponent for the power extrapolation method
        %
        % idxTop: index to the topmost valid depth cell in each ensemble
        %
        % idxTop3: index to the top 3 valid depth cells in each ensemble
        % 
        % topRng: range from the water surface to the top of the topmost
        % cell.
        % 
        % component: the variable to be extrapolated
        %
        % cellSize: array of cellSizes (n cells x n ensembles)
        %
        % cellDepth: depth of each cell (n cells x n ensembles)
        %
        % depthEns: bottom depth for each ensemble
        %
        % deltaT: duration of each ensemble computed by clsQComp
        % constructor
        %
        % z: relative depth from the bottom of each depth cell computed in
        % discharge top method
        %
        % OUTPUT:
        %
        % topValue: total for the specified component integrated over
        % the top range
                  
            % Identify method
            switch topMethod

                % Top power extrapolation
                case 'Power'                     
                    coef=((exponent+1).*nansum(component.*cellSize))./...
                         nansum(((z+0.5.*cellSize).^(exponent+1))-...
                         ((z-0.5.*cellSize).^(exponent+1)));
                    topValue=deltaT.*(coef./(exponent+1)).*...
                        (depthEns.^(exponent+1)-(depthEns-topRng).^(exponent+1));

                % Top constant extrapolation
                case 'Constant'   
                    nEnsembles=length(deltaT);
                    topValue=nan(1,nEnsembles);
                    for j=1:nEnsembles
                        if idxTop(j)~=0
                            topValue(j)=deltaT(j).*component(idxTop(j),j).*topRng(j);
                        end % if idxTop
                    end % for j

                % Top 3-point extrapolation    
                case '3-Point'
                    % Determine number of bins available in each profile
                    validData=~isnan(component);
                    nBins=nansum(validData,1);
                    % Determine number of ensembles
                    nEnsembles=length(deltaT);
                    % Preallocate qtop vector
                    topValue=nan(1,nEnsembles);
                    
                    % Loop through each ensemble
                    for j=1:nEnsembles
                        
                        % If less than 6 bins use constant at the top
                        if nBins(j)<6 & nBins(j)>0 & idxTop(j)~=0
                             topValue(j)=deltaT(j).*component(idxTop(j),j).*topRng(j);
                        end % if nbins

                        % If 6 or more bins use 3-pt at top
                        if nBins(j)>5
                            sumd=nansum(cellDepth(idxTop3(1:3,j),j));
                            sumd2=nansum(cellDepth(idxTop3(1:3,j),j).^2);
                            sumQ=nansum(component(idxTop3(1:3,j),j));
                            sumQd=nansum(component(idxTop3(1:3,j),j).*cellDepth(idxTop3(1:3,j),j));
                            delta=3*sumd2-sumd.^2;
                             A=(3.*sumQd-sumQ.*sumd)./delta;
                             B=(sumQ.*sumd2-sumQd.*sumd)./delta;
                            % Compute discharge for 3-pt fit                      
                            Qo=(A.*topRng(j).^2)./2+B.*topRng(j);
                            topValue(j)=deltaT(j).*Qo;
                        end % if nbins
                    end % for j
            end % switch
        end % extrapolateTop
        
        function [idxTop,idxTop3,topRng]=topVariables(xprod,transect)
        % Computes the index to the top and top 3 valid cells in each
        % ensemble and the range from the water surface to the top of the
        % topmost cell.
        %
        % INPUT:
        %
        % xprod: cross product computed from the crossProduct method
        %
        % transect: object of clsTransectData
        %
        % OUTPUT:
        %
        % idxTop: index to the topmost valid depth cell in each ensemble
        %
        % idxTop3: index to the top 3 valid depth cells in each ensemble
        % 
        % topRng: range from the water surface to the top of the topmost
        % cell.
        
        
            % Get data from transect object
            validData1=transect.wVel.validData(:,:,1);
            validData2=~isnan(xprod);
            validData=logical(validData1.*validData2);
            cellSize=transect.depths.(transect.depths.selected).depthCellSize_m;
            cellDepth=transect.depths.(transect.depths.selected).depthCellDepth_m;

            % Preallocate variables
            nEnsembles=size(validData,2);
            idxTop=zeros(1,size(validData,2));
            idxTop3=zeros(3,size(validData,2));
            topRng=nan(1,nEnsembles);

            % Loop through ensembles
            for n=1:nEnsembles
                % Identify topmost 1 and 3 valid cells
                idxTemp=find(~isnan(xprod(:,n)),3,'first');
                if ~isempty(idxTemp)
                    idxTop(n)=idxTemp(1);
                    if length(idxTemp)>2;
                        idxTop3(:,n)=idxTemp;
                    end % if
                    % Compute top range
                    topRng(n)=cellDepth(idxTop(n),n)-0.5.*cellSize(idxTop(n),n);
                else
                    topRng(n)=0;
                    idxTop(n)=1;
                end %if    
            end % for n
        end % topVariables
        
        % Bottom extrapolation
        % --------------------
        function qBot=dischargeBot(xprod,transect,deltaT,varargin)
        % Coordinates computation of the extrapolated bottom discharge.
        %
        % INPUT:
        % 
        % xprod: cross product computed from the crossProduct method
        %
        % transect: object of clsTransectData
        %
        % deltaT: duration of each ensemble computed from clsQComp
        % constructor
        %
        % varargin: allows specifying top and bottom extrapolations
        % independent of transect object
        %   varargin{1}: not used in this method
        %   varargin{2}: bottom extrapolation method
        %   varargin{3}: exponent
        %
        %
        % OUTPUT:
        % qTop: top extrapolated discharge for each ensemble
        
            % Determine extrapolation methods and exponent
            if ~isempty(varargin)
                botMethod=varargin{2};
                exponent=varargin{3};
            else
                botMethod=transect.extrap.botMethod;
                exponent=transect.extrap.exponent;
            end
            
            % Get index for ensembles in moving-boat portion of transect
            inTransectIdx=transect.inTransectIdx;
            xprod=xprod(:,inTransectIdx);
            
            % Compute bottom variables
            [idxBot,botRng]=clsQComp.botVariables(xprod,transect);
            
            % Get data from transect properties
            cellSize=transect.depths.(transect.depths.selected).depthCellSize_m(:,inTransectIdx);
            cellDepth=transect.depths.(transect.depths.selected).depthCellDepth_m(:,inTransectIdx);
            depthEns=transect.depths.(transect.depths.selected).depthProcessed_m(inTransectIdx);
            
            % Compute z
            z=bsxfun(@minus,depthEns,cellDepth);
            validData=~isnan(xprod);
            z(~validData)=nan;
            z(z<0.5.*cellSize)=nan;
            cellSize(~validData)=nan;
            cellDepth(~validData)=nan;
            
            % Compute bottom discharge
            qBot=clsQComp.extrapolateBottom(botMethod,exponent,idxBot,botRng,...
                            xprod,cellSize,cellDepth,depthEns,deltaT,z);
        end % function dischargeBot

        function botValue=extrapolateBottom(botMethod,exponent,idxBot,botRng,...
                            component,cellSize,cellDepth,depthEns,deltaT,z)
        % Computes the bottom extrapolated value of the provided component 
        % using the specified extrapolation method.
        %
        % INPUT:
        % 
        % botMethod: bottom extrapolation method (Power,Constant,3-Point)
        %
        % exponent: exponent for the power extrapolation method
        %
        % idxBot: index to the bottom most valid depth cell in each ensemble
        %
        % botRng: range from the streambed to the bottom of the bottom most
        % cell.
        % 
        % component: the variable to be extrapolated
        %
        % cellSize: array of cellSizes (n cells x n ensembles)
        %
        % cellDepth: depth of each cell (n cells x n ensembles)
        %
        % depthEns: bottom depth for each ensemble
        %
        % deltaT: duration of each ensemble computed by clsQComp
        % constructor
        %
        % z: relative depth from the bottom of each depth cell computed in
        % discharge top method
        %
        % OUTPUT:
        %
        % botValue: total for the specified component integrated over
        % the top range                        
                       
            % Indentify bottom method
            switch botMethod
                
                % Bottom power extrapolation
                case 'Power' 
                    coef=((exponent+1).*nansum(component.*cellSize))./...
                         nansum(((z+0.5.*cellSize).^(exponent+1))-((z-0.5.*cellSize).^(exponent+1)));
                    botValue=deltaT.*(coef./(exponent+1)).*(botRng.^(exponent+1)); 
                    
                % Bottom no slip extrapolation    
                case 'No Slip'
                    % Valid data in the lower 20% of the water column or
                    % the last valid depth cell are used to compute the no
                    % slip power fit.
                    cutoffDepth=0.8.*depthEns;
                    depthOK=(cellDepth>repmat(cutoffDepth,size(cellDepth,1),1));
                    componentOK=~isnan(component);
                    useNS=depthOK.*componentOK;
                    for j=1:length(deltaT)
                        if idxBot(j)~=0
                            useNS(idxBot(j),j)=1;
                        end % if idxBot
                    end % for j
                    useNS(useNS==0)=nan;

                    % Create cross product and z arrays for the data to be
                    % used in no slip computations.
                    componentNS=useNS.*component;
                    zns=useNS.*z;
                    coef=((exponent+1).*nansum(componentNS.*cellSize))./...
                        nansum(((zns+0.5.*cellSize).^(exponent+1))-...
                        ((zns-0.5.*cellSize).^(exponent+1)));

                    % Compute the bottom discharge of each profile
                    botValue=deltaT.*(coef./(exponent+1)).*(botRng.^(exponent+1)); 
            end % switch botMethod
        end % extrapolateBottom
        
        function [idxBot,botRng]=botVariables(xprod,transect)
        % Computes the index to the bottom most valid cell in each
        % ensemble and the range from the bottom to the bottom of the
        % bottom most cell.
        %
        % INPUT:
        %
        % xprod: cross product computed from the crossProduct method
        %
        % transect: object of clsTransectData
        %
        % OUTPUT:
        %
        % idxBot: index to the bottom most valid depth cell in each ensemble
        %
        % botRng: range from the streambed to the bottom of the bottom most
        % cell.
                    
            % Identify valid data
            inTransectIdx=transect.inTransectIdx;
            validData1=transect.wVel.validData(:,inTransectIdx,1);
            validData2=~isnan(xprod);
            validData=logical(validData1.*validData2);

            % Assign transect properties to local variables
            cellSize=transect.depths.(transect.depths.selected).depthCellSize_m(:,inTransectIdx);
            cellDepth=transect.depths.(transect.depths.selected).depthCellDepth_m(:,inTransectIdx);
            depthEns=transect.depths.(transect.depths.selected).depthProcessed_m(inTransectIdx);

            % Preallocate variables
            nEnsembles=size(validData,2);
            idxBot=zeros(1,size(validData,2));
            botRng=nan(1,nEnsembles);

            for n=1:nEnsembles
                % Identify bottom most valid cell
                idxTemp=find(~isnan(xprod(:,n))==true,1,'last');
                if ~isempty(idxTemp)
                    idxBot(n)=idxTemp; 
                    % Compute bottom range
                    botRng(n)=depthEns(n)-cellDepth(idxBot(n),n)-0.5.*cellSize(idxBot(n),n);
                else
                    botRng(n)=0;
                end % if
            end % for n
        end % botVariables
        
        % Edge discharge
        % --------------
        function edge=dischargeEdge(edgeLoc,transect,varargin)
        % Computes edge discharge.
        %
        % INPUT:
        %
        % edgeLoc: edge location (left or right)
        %
        % transect: object of clsTransectData
        %
        % varargin: allows specifying top and bottom extrapolations
        % independent of transect object
        %   varargin{1}: top extrapolation method
        %   varargin{2}: bottom extrapolation method
        %   varargin{3}: exponent
        %
        % OUTPUT:
        % 
        % edge: computed edge discharge
        
      
            % Determine what ensembles to use for edge computation. The
            % method of determining varies by manufacturer.
            edgeIdx=clsQComp.edgeEnsembles(edgeLoc,transect);
            
            % Average depth for the edge ensembles
            depth=transect.depths.(transect.depths.selected).depthProcessed_m(edgeIdx);
            depthAvg=nanmean(depth);
            
            % Edge distance
            edgeDist=transect.edges.(edgeLoc).dist_m;
            
            % Compute edge velocity and sign
            [edgeVelMag,edgeVelSign]=clsQComp.edgeVelocity(edgeIdx,transect,varargin{:});
            
            % Compute edge coefficient
            coef=clsQComp.edgeCoef(edgeLoc,transect);
            
            % Compute edge discharge
            edge=coef.*depthAvg.*edgeVelMag.*edgeDist.*edgeVelSign;
            if isnan(edge)
                edge=0;
            end % if nan
        end % function dischargeEdge
             
        function edgeIdx=edgeEnsembles(edgeLoc,transect)
        % This function computes the starting and ending ensemble
        % numbers for an edge using either the method used by TRDI
        % which used the specified number of valid ensembles or SonTek
        % which uses the specified number of ensembles prior to screening
        % for valid data.
        %
        % INPUT:
        %
        % edgeLoc: edge location (left or right)
        %
        % transect: object of clsTransectData
        %
        % OUTPUT:
        % 
        % edgeIdx: indices of ensembles used to compute edge discharge
        
            % Assign number of ensembles in edge to local variable
            numEdgeEns=transect.edges.(edgeLoc).numEns2Avg;
            
            % TRDI method.
            if strcmp(transect.adcp.manufacturer,'TRDI')
                % Determine the indices of the edge ensembles 
                % which contain the specified number of
                % valid ensembles
                validEns=clsQComp.validEns(transect);
                if strcmpi(edgeLoc,transect.startEdge)
                    edgeIdx=find(validEns==1,numEdgeEns,'first');
                else
                    edgeIdx=find(validEns==1,numEdgeEns,'last');
                end % edgeLoc

            % SonTek method
            else
                % Determine the indices of the edge ensembles as
                % collected by RiverSurveyor. There is no check as to
                % whether the ensembles contain valid data.
                if strcmpi(edgeLoc,transect.startEdge)
                    edgeIdx=1:1:numEdgeEns;
                else
                    nEnsembles=length(transect.depths.(transect.depths.selected).depthProcessed_m);
                    edgeIdx=1+nEnsembles-numEdgeEns:1:nEnsembles;
                end % edgeLoc
            end % if manufacturer
        end % edgeEnsembles
        
        function [edgeVelMag,edgeVelSign]=edgeVelocity(edgeIdx,transect,varargin)
        % Coordinates computation of edge velocity
        %
        % INPUT:
        %
        % edgeIdx: indices of ensembles used to compute edge discharge
        %
        % transect: object of clsTransectData
        %
        % varargin: allows specifying top and bottom extrapolations
        % independent of transect object
        %   varargin{1}: top extrapolation method
        %   varargin{2}: bottom extrapolation method
        %   varargin{3}: exponent
        %
        % OUTPUT:
        % 
        % edgeVelMag: magnitude of edge velocity
        %
        % edgeVelSign: sign of edge velocity (discharge)
        
            % Check to make sure there is edge data
            if ~isempty(edgeIdx)
                
                % Compute edge velocity using specified method
                switch transect.edges.velMethod
                    
                    % Used by TRDI
                    case 'MeasMag'
                        [edgeVelMag,edgeVelSign]=clsQComp.edgeVelocityTRDI(edgeIdx,transect);
                    
                    % Used by SonTek
                    case 'VectorProf'
                        [edgeVelMag,edgeVelSign]=clsQComp.edgeVelocitySonTek(edgeIdx,transect,varargin{:});
                    
                    % USGS proposed method
                    case 'Profile'
                        [edgeVelMag,edgeVelSign]=clsQComp.edgeVelocityProfile(edgeIdx,transect);
                end % switch velMethod
                
            % If no data set edge velocity to 0
            else
                edgeVelSign=1;
                edgeVelMag=0;
            end % if edgeIdx
        end % edgeVelocity
        
        function [edgeVelMag,edgeVelSign]=edgeVelocityTRDI(edgeIdx,transect)
        % Compute edge velocity magnitude and sign using TRDI's method, 
        % which uses a only the measured data and no extrapolation.
        %
        % INPUT:
        %
        % edgeIdx: indices of ensembles used to compute edge discharge
        %
        % transect: object of clsTransectData
        %
        % OUTPUT:
        % 
        % edgeVelMag: magnitude of edge velocity
        %
        % edgeVelSign: sign of edge velocity (discharge)
            
            % Assign water velocity to local variables
            xVel=transect.wVel.uProcessed_mps(:,edgeIdx);
            yVel=transect.wVel.vProcessed_mps(:,edgeIdx);
            
            % Use only valid data
            valid=double(transect.wVel.validData(:,edgeIdx,1));
            valid(valid==0)=nan;
            xVel=xVel.*valid;
            yVel=yVel.*valid;
            
            % Compute the mean velocity components
            xVelAvg=nanmean(nanmean(xVel,1));
            yVelAvg=nanmean(nanmean(yVel,1));
            
            % Compute magnitude and direction
             [edgeDir,edgeVelMag]=cart2pol(xVelAvg,yVelAvg);

            % Compute unit vector to help determine sign
            [unitWaterX, unitWaterY]=pol2cart(edgeDir,1);
            if strcmp(transect.startEdge,'Right')
                dir=1;
            else
                dir=-1;
            end
            
            % Compute unit boat vector to help determine sign
            ensDeltaTime=transect.dateTime.ensDuration_sec;
            inTransectIdx=transect.inTransectIdx;
            if ~isempty(transect.boatVel.(transect.boatVel.selected))
                bVelx=transect.boatVel.(transect.boatVel.selected).uProcessed_mps;
                bVely=transect.boatVel.(transect.boatVel.selected).vProcessed_mps;
            else
                bVelx=nan(size(transect.boatVel.btVel.uProcessed_mps));
                bVely=nan(size(transect.boatVel.btVel.vProcessed_mps));
            end
            trackX=nancumsum(bVelx(inTransectIdx).*ensDeltaTime(inTransectIdx));
            trackY=nancumsum(bVely(inTransectIdx).*ensDeltaTime(inTransectIdx));
            [boatDir,boatMag]=cart2pol(trackX(end),trackY(end));
            [unitTrackX, unitTrackY]=pol2cart(boatDir,1);
            unitXProd=(unitWaterX.*unitTrackY-unitWaterY.*unitTrackX).*dir;
            edgeVelSign=sign(unitXProd);
        end % edgeVelocityTRDI
                
        function [edgeVelMag,edgeVelSign]=edgeVelocitySonTek(edgeIdx,transect,varargin)  
        % Compute edge velocity magnitude and sign using SonTek's method, 
        % which uses profile extrapolation and a projects the velocity 
        % perpendicular to the course made good.    
        %
        % INPUT:
        %
        % edgeIdx: indices of ensembles used to compute edge discharge
        %
        % transect: object of clsTransectData
        %
        % varargin: allows specifying top and bottom extrapolations
        % independent of transect object
        %   varargin{1}: top extrapolation method
        %   varargin{2}: bottom extrapolation method
        %   varargin{3}: exponent
        %
        % OUTPUT:
        % 
        % edgeVelMag: magnitude of edge velocity
        %
        % edgeVelSign: sign of edge velocity (discharge)            

            % Determine extrapolation methods and exponent
            if ~isempty(varargin)
                topMethod=varargin{1};
                botMethod=varargin{2};
                exponent=varargin{3};
            else
                topMethod=transect.extrap.topMethod;
                botMethod=transect.extrap.botMethod;
                exponent=transect.extrap.exponent;
            end

            % Compute boat track excluding the start edge ensembles but
            % including the end edge ensembles. This the way SonTek does this
            % as of version 3.7.
            ensDeltaTime=transect.dateTime.ensDuration_sec;
            inTransectIdx=transect.inTransectIdx;
            if ~isempty(transect.boatVel.(transect.boatVel.selected))
                bVelx=transect.boatVel.(transect.boatVel.selected).uProcessed_mps;
                bVely=transect.boatVel.(transect.boatVel.selected).vProcessed_mps;
            else
                bVelx=nan(size(transect.boatVel.btVel.uProcessed_mps));
                bVely=nan(size(transect.boatVel.btVel.vProcessed_mps));
            end
            trackX=nancumsum(bVelx(inTransectIdx(1):end).*ensDeltaTime(inTransectIdx(1):end));
            trackY=nancumsum(bVely(inTransectIdx(1):end).*ensDeltaTime(inTransectIdx(1):end));
            
            % Compute the unit vector for the boat track
            [boatDir,boatMag]=cart2pol(trackX(end),trackY(end));
            [unitTrackX, unitTrackY]=pol2cart(boatDir,1);

            % Get velocities for edge ensembles
            xVel=transect.wVel.uProcessed_mps(:,edgeIdx);
            yVel=transect.wVel.vProcessed_mps(:,edgeIdx);
            validVelEns=nansum(transect.wVel.validData(:,edgeIdx,1));

            % Filter edge data
            % According to SonTek the RSL code does recognize that edge samples
            % can vary in their cell size.  It deals with this issue by 
            % remembering the cell size and cell start for the first edge sample.
            % Any subsequent edge sample is included in the average only if it 
            % has the same cell size and cell start as the first sample.

            % Get cell size for edge ensembles
            cellSize=transect.depths.(transect.depths.selected).depthCellSize_m(:,edgeIdx);
            cellDepth=transect.depths.(transect.depths.selected).depthCellDepth_m(:,edgeIdx);

            % Find first valid edge ensemble
            idxFirstValidEns=find(validVelEns>0,1,'first');
            
            % No valid edge ensembles
            if isempty(idxFirstValidEns)
                edgeVelMag=0;
                edgeVelSign=1;
                idxFirstValidCell=[];
            else
                % Compute reference size and valid data
                refCellSize=cellSize(1,idxFirstValidEns);
                refCellDepth=cellDepth(1,idxFirstValidEns);
                valid=true(size(edgeIdx));
                valid(cellSize(1,:)~=refCellSize)=false;
                valid(cellDepth(1,:)~=refCellDepth)=false;

                % Compute profile components
                xProfile=nanmean(xVel(:,valid),2);
                yProfile=nanmean(yVel(:,valid),2);

                % Find first valid cell in profile
                idxFirstValidCell=find(~isnan(xProfile),1,'first'); 
            end
            
            % If there is no valid data set velocity to 0
            if isempty(idxFirstValidCell)
                edgeVelMag=0;
                edgeVelSign=1;
            
            else 
                
                % Compute cell size and depth for mean profile
                cellSize(isnan(xVel))=nan;
                cellSize(:,~valid)=nan;
                cellSizeEdge=nanmean(cellSize,2);
                cellDepth(isnan(xVel))=nan;
                cellDepth(:,~valid)=nan;
                cellDepthEdge=nanmean(cellDepth,2);

                % SonTek cuts off the mean profile based on the side lobe cutoff of
                % the mean of the shallowest beams in the edge ensembles.

                % Determine valid original beam and cell depths
                depthBTBeamOrig=transect.depths.btDepths.depthOrig_m(:,edgeIdx);
                depthBTBeamOrig(:,~valid)=nan;
                draftBTBeamsOrig=transect.depths.btDepths.draftOrig_m;
                depthCellDepthOrig=transect.depths.btDepths.depthCellDepthOrig_m(:,edgeIdx);
                depthCellDepthOrig(:,~valid)=nan;
              
                % Compute minimum mean depth
                minRawDepths=nanmin(depthBTBeamOrig);
                minDepth=nanmean(minRawDepths);
                minDepth=minDepth-draftBTBeamsOrig;
                 
                % Compute last valid cell by computing the side lobe cutoff based
                % on the mean of the minimum beam depths of the valid edge
                % ensembles               
                if strcmp(transect.wVel.slCutoffType,'Percent')
                    % Apply simple side lobe cutoff
                    slDepth=minDepth-(transect.wVel.slCutoffPer./100).*minDepth;
                else
                    % Apply side lobe cutoff with additional cells to be cut as
                    % specified by the user
                    slDepth=minDepth-(transect.wVel.slCutoffPer./100).*minDepth-transect.wVel.slCutoffNum.*cellSize(1,1);
                end
                
                % Adjust sidelobe depth for draft
                slDepth=slDepth+draftBTBeamsOrig;
                for k=1:size(cellDepth,2)
                 aboveSL(:,k)=cellDepth(:,k)<(slDepth+nanmax(cellSize(:,k)));
                end
                aboveSLProfile=nansum(aboveSL,2);
                validIdx=(aboveSLProfile<nanmax(aboveSLProfile)+1) & (aboveSLProfile>0);
                % Compute the number of cells above the side lobe cutoff
                remainingDepth=slDepth-cellDepthEdge(idxFirstValidCell);
                idx=find(~isnan(cellSize),1,'first');
                nCells=floor(remainingDepth/refCellSize);                    
                if isnan(nCells)
                    nCells=0;
                end
                nCells(nCells<0)=0;
                
                % Determine index of bottom most valid cells
                idxLastValidCell=idxFirstValidCell+nCells;
                if idxLastValidCell>length(xProfile)
                    xProfile(~validIdx)=NaN;
                    yProfile(~validIdx)=NaN;
                else
                    idxLastValidCell=find(~isnan(xProfile(1:idxLastValidCell)),1,'last');

                    % Mark the cells in the profile below the sidelobe invalid
                    xProfile((idxLastValidCell+1):end) = NaN;
                    yProfile((idxLastValidCell+1):end) = NaN;
                end
                
                % Find the top most 3 valid cells
                idxFirst3ValidCells=find(~isnan(xProfile),3,'first');
                
                % Compute the mean measured velocity components for the edge
                % profile
                xProfileMean=nanmean(xProfile);
                yProfileMean=nanmean(yProfile);

                % Compute average depth of edge
                depthEns=transect.depths.(transect.depths.selected).depthProcessed_m(edgeIdx);
                depthEns(~valid)=nan;
                depthAvg=nanmean(depthEns);

                % Determine top, mid, bottom range for the profile     
                topRngEdge=cellDepthEdge(idxFirstValidCell)-0.5.*refCellSize;
                if idxLastValidCell>length(xProfile)
                   midRngEdge=nansum(cellSizeEdge(validIdx));
                else
                    midRngEdge=nansum(cellSizeEdge(idxFirstValidCell:idxLastValidCell));                
                end
                

                % Compute z
                zEdge=depthAvg-cellDepthEdge;
                zEdge(idxLastValidCell+1:end)=nan;
                zEdge(zEdge<0)=nan;
                idxLastValidCell=find(~isnan(zEdge),1,'last');
                botRngEdge=depthAvg-cellDepthEdge(idxLastValidCell)-0.5*cellSizeEdge(idxLastValidCell);

                % Compute the top extrapolation for x-component
                topVelx=clsQComp.extrapolateTop(topMethod,exponent,idxFirstValidCell,...
                    idxFirst3ValidCells,topRngEdge,xProfile,...
                    cellSizeEdge,cellDepthEdge,depthAvg,1,zEdge)./topRngEdge;

                % Compute the bottom extrapolation for x-component
                botVelx=clsQComp.extrapolateBottom(botMethod,exponent,idxLastValidCell,botRngEdge,...
                            xProfile,cellSizeEdge,cellDepthEdge,depthAvg,1,zEdge)./botRngEdge;

                % Compute the top extrapolation for the y-component
                topVely=clsQComp.extrapolateTop(topMethod,exponent,idxFirstValidCell,...
                    idxFirst3ValidCells,topRngEdge,yProfile,...
                    cellSizeEdge,cellDepthEdge,depthAvg,1,zEdge)./topRngEdge;

                % Compute the bottom extrapolation for y-component
                 botVely=clsQComp.extrapolateBottom(botMethod,exponent,idxLastValidCell,botRngEdge,...
                            yProfile,cellSizeEdge,cellDepthEdge,depthAvg,1,zEdge)./botRngEdge;

                % Compute the edge velocity vector including the
                % extrapolated velocities
                vEdgeX=(topVelx.*topRngEdge+xProfileMean.*midRngEdge+botVelx.*botRngEdge)./depthAvg;
                vEdgeY=(topVely.*topRngEdge+yProfileMean.*midRngEdge+botVely.*botRngEdge)./depthAvg;

                % Compute magnitude of edge velocity perpendicular to CMG
                edgeVelMag=vEdgeX.*-unitTrackY + vEdgeY.*unitTrackX;

                % Compute edge discharge
                if strcmpi(transect.startEdge,'Right')
                    edgeVelSign=-1;
                else
                    edgeVelSign=1;
                end 
            end % if isempty
        end % edgeVelocitySonTek   
        
        function [edgeVelMag,edgeVelSign]=edgeVelocityProfile(edgeIdx,transect)  
        % Compute edge velocity magnitude using the mean velocity of each 
        % ensemble. The mean velocity of each ensemble is computed by first
        % computing the mean direction of the velocities in the ensemble,
        % then projecting the velocity in each cell in that direction and
        % fitting the 1/6th power curve to the projected profile. The mean
        % velocity magnitude from each ensemble is then averaged.
        %
        % The sign of the velocity magnitude is computed using the same
        % approach used in WinRiver II. The cross product of the unit
        % vector of the shiptrack and the unit vector of the edge water
        % samples computed from the mean u and v velocities is used to
        % determine the sign of the velocity magnitude.
        %
        % INPUT:
        %
        % edgeIdx: indices of ensembles used to compute edge discharge
        %
        % transect: object of clsTransectData
        %
        % OUTPUT:
        % 
        % edgeVelMag: magnitude of edge velocity
        %
        % edgeVelSign: sign of edge velocity (discharge)   
        
            % Assign water velocity to local variables
            xVel=transect.wVel.uProcessed_mps(:,edgeIdx);
            yVel=transect.wVel.vProcessed_mps(:,edgeIdx);

            % Use only valid data
            valid=double(transect.wVel.validData(:,edgeIdx,1));
            valid(valid==0)=nan;
            xVel=xVel.*valid;
            yVel=yVel.*valid;
            
            % Compute edge velocity magnitude and direction
            % =============================================
           
            % Initialize local variables
            nEns=length(edgeIdx);
            vEns=nan(1,nEns);
            u=nan(1,nEns);
            v=nan(1,nEns);
            
            % Process each edge ensemble
            for n=1:nEns

                % Use ensembles with some valid data
                selectedEns=edgeIdx(n);
                validEns=sum(isnan(xVel(:,n)));

                if validEns>0
                    
                    % Setup variables
                    vx=xVel(:,n);
                    vy=yVel(:,n);
                    cellSize=transect.depths.btDepths.depthCellSize_m(:,selectedEns);
                    depthCellDepth=transect.depths.btDepths.depthCellDepth_m(:,selectedEns);
                    depth=transect.depths.btDepths.depthProcessed_m(:,selectedEns);
                    cellSize(isnan(vx))=nan;
                    depthCellDepth(isnan(vx))=nan;

                    % Compute projected velocity profile for an ensemble
                    vxAvg=nansum(vx.*cellSize)./nansum(cellSize);
                    vyAvg=nansum(vy.*cellSize)./nansum(cellSize);
                    [ensDir,~]=cart2pol(vxAvg,vyAvg);
                    [vUnit(1), vUnit(2)]=pol2cart(ensDir,1);
                    vProjMag=dot([vx,vy],repmat(vUnit,length(vx),1),2);

                    % Compute z value for each cell
                    z=(depth-depthCellDepth);
                    z(isnan(vProjMag))=nan;

                    % Compute coefficient for 1/6th power curve
                    b=1/6.;
                    a=((b+1).*nansum(vProjMag.*cellSize))./nansum(((z+0.5.*cellSize).^(b+1))-((z-0.5.*cellSize).^(b+1)));

                    % Compute mean water speed by integrating power curve
                    vEns(n)=((a./(b+1)).*(depth.^(b+1)))./depth;
                    
                    % Compute the mean velocity components from the mean
                    % water speed and direction
                    [u(n),v(n)]=pol2cart(ensDir,vEns(n));
               
                else
                    
                    % No valid data in ensemble
                    vEns(n)=nan;
                    u(n)=nan;
                    v(n)=nan;
                    
                end % if valid
                
            end % for n
            
            % Compute the mean velocity components of the edge velocity as
            % the mean of the mean ensemble components
            uAvg=nanmean(u);
            vAvg=nanmean(v);
            
            % Compute the edge velocity magnitude
            [edgeVelDir,edgeVelMag]=cart2pol(uAvg,vAvg);
            
%             % If no heading (no compass) compute mean from magnitudes
%             heading=transect.sensors.heading_deg.(transect.sensors.heading_deg.selected).data;
%             if length(unique(heading))<2
%                 edgeVelMag=nanmean(vEns);
%             end

            % Compute edge velocity sign
            % ==========================

            % Compute unit vector to help determine sign
            [unitWaterX, unitWaterY]=pol2cart(edgeVelDir,1);

            % Compute unit boat vector to help determine sign
            ensDeltaTime=transect.dateTime.ensDuration_sec;
            inTransectIdx=transect.inTransectIdx;
            if ~isempty(transect.boatVel.(transect.boatVel.selected))
                bVelx=transect.boatVel.(transect.boatVel.selected).uProcessed_mps;
                bVely=transect.boatVel.(transect.boatVel.selected).vProcessed_mps;
            else
                bVelx=nan(size(transect.boatVel.btVel.uProcessed_mps));
                bVely=nan(size(transect.boatVel.btVel.vProcessed_mps));
            end
            trackX=nancumsum(bVelx(inTransectIdx).*ensDeltaTime(inTransectIdx));
            trackY=nancumsum(bVely(inTransectIdx).*ensDeltaTime(inTransectIdx));
            [boatDir,~]=cart2pol(trackX(end),trackY(end));
            [unitTrackX, unitTrackY]=pol2cart(boatDir,1);
            
            % Account for direction of boat travel 
            if strcmp(transect.startEdge,'Right')
                dir=1;
            else
                dir=-1;
            end
            
            % Compute cross product from unit vectors
            unitXProd=(unitWaterX.*unitTrackY-unitWaterY.*unitTrackX).*dir;
            
            % Determine sign
            edgeVelSign=sign(unitXProd);


        end % edgeVelocityProfile
        
        function coef=edgeCoef(edgeLoc,transect)
        % Returns the edge coefficient based on the edge settings and 
        % transect object
        %
        % INPUT:
        %
        % edgeLoc: edge location (left or right)
        %
        % transect: object of clsTransectData
        %
        % OUTPUT:
        % 
        % coef: edge coefficient accounting for velocity distribution 
        % and edge shape  
        
            % Process appropriate edge type
            switch transect.edges.(edgeLoc).type
                
                % Triangular edge
                case 'Triangular' 
                    coef=0.3535;
                    
                % Rectangular edge
                case 'Rectangular'
                    % Rectangular edge coefficient depends on the
                    % recEdgeMethod. 'Fixed' is compatible with the method
                    % used by TRDI. 'Variable' is compatible with the
                    % method used by SonTek.
                    if strcmp(transect.edges.recEdgeMethod,'Fixed')
                        
                        % Fixed method
                        coef=0.91;

                    else
                        
                        % Variable method
                        
                        % Get edge distance
                        dist=transect.edges.(edgeLoc).dist_m;
                        
                        % Get edge esembles to use
                        [edgeIdx]=clsQComp.edgeEnsembles(edgeLoc,transect);                                       
                        
                        % Compute mean depth for edge
                        depthEdge=nanmean(transect.depths.(transect.depths.selected).depthProcessed_m(edgeIdx));
                        
                        % Compute coefficient using equation 34 from
                        % Principle of River Discharge Measurement, SonTek,
                        % 2003
                        coef= (1-((0.35./4).*(depthEdge./dist).*(1-exp(-4.*(dist./depthEdge)))))./...
                            (1-0.35.*exp(-4.*(dist./depthEdge)));         
                    end % if
                
                % User supplied custom coefficient    
                case 'Custom'
                    coef=transect.edges.(edgeLoc).custCoef;
                
                % If the user enters an edge discharge the coef is empty    
                case 'User Q'
                    coef=[];
                    
            end % switch type
        end % edgeCoef
  
        % Moving-bed corrections
        % ----------------------
        function correctionFactor=loopCorrection(topQ,middleQ,bottomQ,transData,mbData,deltaT)
        % Computes the discharge correction factor from the loop moving-bed
        % test.
        %
        % INPUT:
        % 
        % topQ: top discharge from extrapolation
        % middleQ: computed middle discharge
        % bottomQ: bottom discharge from extrapolation
        % transData: object of clsTransectData
        % mbData: object of clsMovingBedTests
        % deltaT: duration of each ensemble, computed in clsQComp
        % constructor
        % 
        % OUTPUT:
        %
        % correctionFactor: correctionFactor to be applied to the discharge
        % to correct for moving-bed effects.
                       
            % Assign object properties to local variables
            movingBedSpeed=mbData.mbSpd_mps;
            inTransectIdx=transData.inTransectIdx;
            cellsAboveSL=transData.wVel.cellsAboveSL(:,inTransectIdx);
            u=transData.wVel.uProcessed_mps(:,inTransectIdx).*cellsAboveSL;
            v=transData.wVel.vProcessed_mps(:,inTransectIdx).*cellsAboveSL;
            binDepth=transData.depths.(transData.depths.selected).depthCellDepth_m(:,inTransectIdx);
            depth=transData.depths.(transData.depths.selected).depthProcessed_m(inTransectIdx);
            btU=transData.boatVel.btVel.uProcessed_mps(inTransectIdx);
            btV=transData.boatVel.btVel.vProcessed_mps(inTransectIdx);

            % Compute uncorrected discharge excluding the edges
            qOrig=topQ+middleQ+bottomQ;

            % Compute near-bed velocities
            [nbU, nbV, unitNBU, unitNBV]=clsMovingBedTests.nearBedVelocity(u,v,depth,binDepth);
            nbSpeed=sqrt(nbU.^2+nbV.^2);
            nbUMean=nanmean(nbU);
            nbVMean=nanmean(nbV);
            nbSpeedMean=sqrt(nbUMean.^2+nbVMean.^2);
            movingBedSpeedEns=movingBedSpeed.*(nbSpeed./nbSpeedMean);
            Umb=movingBedSpeedEns.*unitNBU;
            Vmb=movingBedSpeedEns.*unitNBV;

            % Correct water velocities
            uAdj=bsxfun(@plus,u,Umb);
            vAdj=bsxfun(@plus,v,Vmb);

            % Correct boat velocities
            btUAdj=btU+Umb;
            btVAdj=btV+Vmb;

            % Compute corrected cross product
            xprod=clsQComp.crossProduct(transData);
            xprodIn=clsQComp.crossProduct(uAdj,vAdj,btUAdj,btVAdj,transData.startEdge);
            xprod(:,inTransectIdx)=xprodIn;

            % Compute corrected discharges
            qMidCells=clsQComp.dischargeMiddleCells(xprod,transData,deltaT);
            qTop=clsQComp.dischargeTop(xprod,transData,deltaT);
            qBot=clsQComp.dischargeBot(xprod,transData,deltaT);
            qAdj=nansum(nansum(qMidCells))+nansum(qTop)+nansum(qBot);

            % Compute correction factor
            correctionFactor=qAdj./qOrig;

        end % loopCorrection        
        
        function correctionFactor=stationaryCorrection(topQ,middleQ,bottomQ,transData,mbData,deltaT )
        % Computes the discharge correction factor from stationary moving-bed
        % tests.
        %
        % INPUT:
        % 
        % topQ: top discharge from extrapolation
        % middleQ: computed middle discharge
        % bottomQ: bottom discharge from extrapolation
        % transData: object of clsTransectData
        % mbData: object of clsMovingBedTests
        % deltaT: duration of each ensemble, computed in clsQComp
        % constructor
        % 
        % OUTPUT:
        %
        % correctionFactor: correctionFactor to be applied to the discharge
        % to correct for moving-bed effects.
        
            % Find and composite results of all stationary test that are 
            % selected for use.
            nMBTests=length(mbData);
            nStaTests=0;
            for n=1:nMBTests
                if strcmp(mbData(n).type,'Stationary') && mbData(n).use2Correct     
                    nStaTests=nStaTests+1;
                    mbSpd(nStaTests)=mbData(n).mbSpd_mps;
                    nearBedSpeed(nStaTests)=mbData(n).nearBedSpeed_mps;
                end
            end

            if nStaTests>0

                % Compute linear regression coefficient forcing through zero to relate
                % near-bed velocity to moving-bed velocity.
                corrcoef=nearBedSpeed'\mbSpd';
                
                % Assign object properties to local variables
                inTransectIdx=transData.inTransectIdx;
                cellsAboveSL=transData.wVel.cellsAboveSL(:,inTransectIdx);
                u=transData.wVel.uProcessed_mps(:,inTransectIdx).*cellsAboveSL;
                v=transData.wVel.vProcessed_mps(:,inTransectIdx).*cellsAboveSL;
                binDepth=transData.depths.(transData.depths.selected).depthCellDepth_m(:,inTransectIdx);
                depth=transData.depths.(transData.depths.selected).depthProcessed_m(inTransectIdx);
                btU=transData.boatVel.btVel.uProcessed_mps(inTransectIdx);
                btV=transData.boatVel.btVel.vProcessed_mps(inTransectIdx);


                % Compute near-bed velocities
                [nbU, nbV, unitNBU, unitNBV]=clsMovingBedTests.nearBedVelocity(u,v,depth,binDepth);

                % Compute moving-bed vector for each ensemble
                mbU=corrcoef.*nbU;
                mbV=corrcoef.*nbV;

                % Compute Adjusted Water and Boat Velocities
                uAdj=bsxfun(@plus,u,mbU);
                vAdj=bsxfun(@plus,v,mbV);
                btUAdj=btU+mbU;
                btVAdj=btV+mbV; 

                % Compute uncorrected discharge excluding the edges
                qOrig=topQ+middleQ+bottomQ;

                % Compute corrected discharge excluding edges
                xprod=clsQComp.crossProduct(transData);
                xprodIn=clsQComp.crossProduct(uAdj,vAdj,btUAdj,btVAdj,transData.startEdge);
                xprod(:,inTransectIdx)=xprodIn;
                
                % Compute corrected discharges
                qMidCells=clsQComp.dischargeMiddleCells(xprod,transData,deltaT);
                qTop=clsQComp.dischargeTop(xprod,transData,deltaT);
                qBot=clsQComp.dischargeBot(xprod,transData,deltaT);
                qAdj=nansum(nansum(qMidCells))+nansum(qTop)+nansum(qBot);

                % Compute correction factor
                correctionFactor=qAdj./qOrig; 
                
            end %if nStaTests
        end % stationaryCorrection           
        
        % Other static methods
        % --------------------
        function validEns=validEns(transData)
        % Determines which ensembles contain sufficient valid data to
        % allow computation of discharge.
        % Allows interpolated depth and boat velocity but requires valid
        % non-interpolated water velocity.
        %
        % INPUT:
        % transData: clsTransectData
        % 
        % OUTPUT:
        % validEns: logical vector
            
            % Get index of ensembles in moving-boat portion of transect
            inTransectIdx=transData.inTransectIdx;
            
            % Get selected navigation refrence
            selected=transData.boatVel.selected;
            
            % Depending on type of interpolation determine the valid
            % navigation ensembles
            if ~isempty(transData.boatVel.(selected))
                if strcmp(transData.boatVel.(selected).interpolate,'TRDI')
                    navValid=transData.boatVel.(selected).validData(1,inTransectIdx);
                else
                    navValid=~isnan(transData.boatVel.(selected).uProcessed_mps(inTransectIdx));
                end
            else
                navValid=false(size(transData.boatVel.btVel.validData(1,inTransectIdx)));
            end
            
            % Depending on type of interpolation determine the valid water
            % track ensembles
%             if strcmp(transData.wVel.interpolateEns,'TRDI')
                waterValid=any(transData.wVel.validData(:,inTransectIdx,1));
%             else
%                 waterValid=any(~isnan(transData.wVel.uProcessed_mps(:,inTransectIdx)));
%             end
            
            % Determine the ensembles with valid depth
            depthValid=~isnan(transData.depths.(transData.depths.selected).depthProcessed_m(inTransectIdx));
            
            % Determine the ensembles with valid depth, navigation, and
            % water data
            validEns=navValid & waterValid & depthValid;
        end % validEns
        
        function [qintCells, qintEns]= dischargeInterpolated(qTopEns,qMidCells,qBottomEns,transect)
        % Determines the amount of discharge in interpolated cells and
        % ensembles
        %
        % INPUT:
        % 
        % qTopEns: top extrapolated discharge in each ensemble
        %
        % qMidCells: middle of measured discharge in each ensemble
        %
        % qBottomEns: bottom extrapolated discharge in each ensemble
        %
        % transect: object of clsTransectData
        %
        % OUTPUT:
        %
        % qintCells: discharge in interpolated cells
        % 
        % qintEns: discharge in interpolated ensembles

            [validEns,validWTCells]=clsTransectData.validCellsEnsembles(transect);
            
            % Compute interpolated cell discharge
            qintCells=nansum(nansum(qMidCells(~validWTCells)));
            
            % Method to compute invalid ensemble discharge depends on if
            % navigation data are interpolated (QRev) or if expanded delta
            % time is used to compute discharge for invalid ensembles(TRDI)
            if strcmpi(transect.boatVel.btVel.interpolate,'None') 
                
               % Compute discharge in invalid ensembles for expanded delta 
               %time situation 
               
               % Find index of invalid ensembles followed by a valid ensemble
               invalidStr=num2str(double(~validEns));
               invalidStr=invalidStr(~isspace(invalidStr));
               idx=strfind(invalidStr,'10');
               
               % Check for invalid ensembles
               if isempty(idx)
                   
                   % No invalid ensembles
                   qintEns=0;
               else
                   
                   % Increase index to reference valid ensemble
                   idx=idx+1;
                   
                   % Sum discharge in valid ensembles following invalid
                   % ensemble
                   qintEns=nansum(qMidCells(:,idx))+qTopEns(idx)+qBottomEns(idx);
                   
                   % Determine number of invalid ensembles preceding valid
                   % ensemble
                   [runlength0,~]=clsQComp.computeRunLength(validEns);
                   
                   % Adjust runlength0 array for situation where the
                   % transect ends with invalid ensembles
                   if length(runlength0)>length(qintEns)
                       runlength0=runlength0(1:end-1);
                   end
                   
                   % Adjust discharge to removed the discharge that would
                   % have been measured in the valid ensemble
                   qintEns=nansum(qintEns.*(runlength0./(runlength0+1)));
               end
               
            else
                
                % Compute invalid ensembles for situation where all data
                % were interpolated
                qintEns=nansum(nansum(qMidCells(:,~validEns)))+nansum(qTopEns(~validEns))+nansum(qBottomEns(~validEns));
            end
            
        end % dischargeInterpolated      
       
        function [runlength0, runlength1]=computeRunLength(V)
        % Computes how many false or true consecutive values in every run
        % of true or false in provide logical vector.
        %
        % INPUT:
        %
        % V: logical vector
        %
        % OUTPUT:
        %
        % runlength0: run lengths of false (0)
        %
        % runlength1: run lengths of true (1)
            
            % Compute the indices of where changes occur
            validRun = [find(diff([-1 V -1]) ~= 0)]; 
            % Determine length of each run
            runlength = diff(validRun);
            % Determine length of false (0) runs
            runlength0 = runlength(1+(V(1)==1):2:end);
            % Determine length of true (0) runs
            runlength1 = runlength(1+(V(1)==0):2:end);
            
        end % computeRunLength
              
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
            numTrans=length(structIn);
            for num=1:numTrans        
                % Create object
                temp(num)=clsQComp();
                % Get variable names from structure
                names=fieldnames(structIn(num));
                % Set properties to structure values
                nMax=length(names);
                for n=1:nMax
                    % Assign property
                    temp(num)=resetProperty(temp(num), names{n},structIn(num).(names{n}));
                end % for n
            end % for num
            obj=setProperty(obj,'discharge',temp);
        end % reCreate            
    end % static methods 
    
end % class

