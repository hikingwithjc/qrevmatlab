classdef clsNormData
    %
    % Class definition for normalized data. The input data are normalized
    % for relative depth and relative unit discharge or velocity. The
    % constuctor method allows an object to be formed without any data or
    % creates a new object from OriginData or completes the properties of
    % an object that is already of class NormData. The constuctor method
    % also allows only a portion of the data to be used in the
    % normalization process by specifying the data extent.
    % David S. Mueller, 2/18/2011
    %
    % Modified 6/18/2011, dsm
    % 1) Added DisplayNos
    % 2) Cleaned up variable names and added comments
    %
    % Modified 10/17/2011, dsm
    % 3) Added validData index to identify data meeting threshold criteria.
    % Changed threshold from a number to a percent of the median number
    % of valid cells in a normalized cell. This is applied to each transect
    % and to the composite so that the composite will need proportionaly
    % more in each normalized cell since it is compiling all transects.
    %
    % Last modifications / validations 5/15/2012 dsm
    %
    % Modified 4/12/2013 dsm
    % 4) Added check for valid velocity when determining which data fit
    % within a 5% increment. This affects the avgz value. See line 187.
    % 
    % Modified 3/3/2014 dsm
    % 5) Modified for use in QRev
    
    properties (SetAccess=private)
        fileName             % name of transect file
        cellDepthNormalized  % normalized depth of cell
        unitNormalized       % normalized discharge or velocity for all depth cells
        unitNormalizedMed    % median of normalized data within 5% partitions
        unitNormalizedNo     % number of data points in each median
        unitNormalizedz      % relative depth for each median (5% increments)
        unitNormalized25     % value for which 25% of normalized values are smaller
        unitNormalized75     % value for which 25% of normalized values are larger
        dataType             % type of data (v, q, V, or Q)
        dataExtent
        validData            % index of median values with point count greater than threshold cutoff
    end % properties
    
    methods
        %==================================================================
        function obj=clsNormData(transData,datatype,threshold,varargin)
        %
        % Constructor method
        % If no arguments are provided the object is formed with no data in
        % the properties. If datain is of class OriginData the normalized
        % data are computed from the original data. If datain is of class
        % NormData then the unitNormalized, cellDepthNormalized, and
        % dataType properties must be previously defined and the remaining
        % properties will be computed.
        %==================================================================
            
            % If no arguments just create object
            if nargin>0
                %
                % If the data extent is not defined set dataextent to zero
                % to trigger all data to be used.
                % --------------------------------------------------------
                if ~isempty(varargin)
                    dataextent=varargin{1};
                else
                    dataextent=[0 100];
                end
                
                % Determine number of transects to be processed
                nTransects=length(transData);
                
                % Preallocate objects to improve speed
                obj(nTransects+1)=clsNormData();
                nCells=nan(nTransects,1);
                nEns=nan(nTransects,1);
                
                %% Process each transect
                for n=1:nTransects
                    idx=find(transData(n).fileName=='\',1,'last');
                    if isempty(idx)
                        idx=0;
                    end
                    filename=transData(n).fileName(idx+1:end);
                    inTransectIdx=transData(n).inTransectIdx;
                    cellDepth=transData(n).depths.(transData(n).depths.selected).depthCellDepth_m(:,inTransectIdx);
                    cellsAboveSL=transData(n).wVel.cellsAboveSL(:,inTransectIdx);
                    cellDepth(~cellsAboveSL)=nan;
                    depthEns=transData(n).depths.(transData(n).depths.selected).depthProcessed_m(inTransectIdx);
                    wVelx=transData(n).wVel.uProcessed_mps(:,inTransectIdx);
                    wVely=transData(n).wVel.vProcessed_mps(:,inTransectIdx);
                    invalidData=~transData(n).wVel.validData(:,inTransectIdx,1);
                    wVelx(invalidData)=nan;
                    wVely(invalidData)=nan;
                    nCells(n)=size(wVely,1);
                    nEns(n)=size(wVely,2);
                    if ~isempty(transData(n).boatVel.(transData(n).boatVel.selected))
                        btVelx=transData(n).boatVel.(transData(n).boatVel.selected).uProcessed_mps(inTransectIdx);
                        btVely=transData(n).boatVel.(transData(n).boatVel.selected).vProcessed_mps(inTransectIdx);
                    else
                        btVelx=nan(size(transData(n).boatVel.btVel.uProcessed_mps(inTransectIdx)));
                        btVely=nan(size(transData(n).boatVel.btVel.vProcessed_mps(inTransectIdx)));
                    end
                    
                    % Compute normalized cell depth by average depth in each ensemble
                    normcellDepth=bsxfun(@rdivide,cellDepth,depthEns);
                    normcellDepth(normcellDepth<0)=nan;

                    % If datatype is discharge compute unit discharge for
                    % each cell
                    if strcmp(datatype,'q')
                        
                        % Compute the cross product for each cell
                        unit=bsxfun(@times,wVelx,btVely)-...
                               bsxfun(@times,wVely,btVelx);
                    else

                        % Compute mean velocity components in each ensemble
                        wVelMean1=nanmean(wVelx);
                        wVelMean2=nanmean(wVely);

                        % Compute a unit vector in the mean flow direction for each ensemble
                        [dir, ~]=cart2pol(wVelMean1,wVelMean2);
                        [unitVec(1,:), unitVec(2,:)]=pol2cart(dir,1);

                        % Compute the velocity magnitude in the direction of the mean velocity
                        % of each ensemble using the dot product
                        unit=nan(size(wVelx));
                        for i=1:size(wVelx,1)
                            unit(i,:)=dot([wVelx(i,:); wVely(i,:)],unitVec);
                        end
                    end
                    
                    % Compute total 
                    meas=nansum(nansum(unit));

                    % Adjust to postive value
                    if meas<0
                        unit=unit.*-1;
                    end
                    %
                    % Compute normalize unit values
                    unitNorm=bsxfun(@rdivide,unit,abs(nanmean(unit)));

                    % Apply extents if they have been specified
                    if dataextent(1)~=0 || dataextent(2)~=100

                        % unit discharge is computed here because the
                        % unitNorm could be based on velocity
                        unit=bsxfun(@times,wVelx,btVely)-...
                               bsxfun(@times,wVely,btVelx);
                        unitens=nansum(unit);
                        unittotal=nancumsum(unitens);

                        % Adjust so total discharge is positive
                        if unittotal(end)<0
                            unittotal=unittotal.*-1;
                        end

                        % Apply extents
                        unitlower=unittotal(end).*dataextent(1)./100;
                        unitupper=unittotal(end).*dataextent(2)./100;
                        idxextent=find(unittotal>unitlower & unittotal<unitupper);
                        unitNorm=unitNorm(:,idxextent);
                        normcellDepth=normcellDepth(:,idxextent);
                        [nCells(n),nEns(n)]=size(unitNorm);
                    end

                    % If whole profile is negative make positive
                    idxNeg1=nan(size(unitNorm,2),1);
                    idxNeg2=nan(size(unitNorm,2),1);
                    for c=1:size(unitNorm,2)
                        idxNeg1(c)=length(find(unitNorm(:,c)<0));
                        idxNeg2(c)=length(find(~isnan(unitNorm(:,c))));
                    end
                    idxNeg=idxNeg1==idxNeg2;
                    unitNorm(:,idxNeg)=unitNorm(:,idxNeg).*-1;  
                    obj(n).fileName=filename;
                    obj(n).cellDepthNormalized=normcellDepth;
                    obj(n).unitNormalized=unitNorm;
                    clear unitVec
                end % for n  
                
                %% Create object for measurement composite
                % Create arrays of composited data
                maxcells=nanmax(nCells);
                nEns=[0,nEns'];
                sumEns=cumsum(nEns);
                obj(nTransects+1).unitNormalized(1:maxcells,1:sumEns(end))=nan;
                obj(nTransects+1).cellDepthNormalized(1:maxcells,1:sumEns(end))=nan;
                for n=1:nTransects  
                    if transData(n).checked==1
                        obj(nTransects+1).unitNormalized(1:nCells(n),1+sumEns(n):sumEns(n+1))=obj(n).unitNormalized;
                        obj(nTransects+1).cellDepthNormalized(1:nCells(n),1+sumEns(n):sumEns(n+1))=obj(n).cellDepthNormalized;
                    end
                end
                obj(nTransects+1).fileName='Measurement';
                
                %% Compute median values and other statistics
                for n=1:nTransects+1
                
                    % Initialize variables
                    avgInterval=0:0.05:1;
                    unitNormMed=nan(1,length(avgInterval)-1);
                    unitNormMedNo=nan(1,length(avgInterval)-1);
                    unit25=nan(1,length(avgInterval)-1);
                    unit75=nan(1,length(avgInterval)-1);
                    avgz=nan(1,length(avgInterval)-1);
                    
                    % Process each normalized increment
                    for i=1:length(avgInterval)-1
                        idx=find(obj(n).cellDepthNormalized>avgInterval(i) & obj(n).cellDepthNormalized<=avgInterval(i+1) & ~isnan(obj(n).unitNormalized));
                        unitNormMed(i)=nanmedian((obj(n).unitNormalized(idx)));
                        unitNormMedNo(i)=sum(~isnan(obj(n).unitNormalized(idx)));
                        unit25(i)=prctile(obj(n).unitNormalized(idx),25);
                        unit75(i)=prctile(obj(n).unitNormalized(idx),75);
                        avgz(i)=1-nanmean(obj(n).cellDepthNormalized(idx));
                    end
                    
                    % Mark increments invalid if they don't have sufficient
                    % data
                    cutoff=nanmedian(unitNormMedNo(unitNormMedNo>0)).*(threshold./100);
                    valid=find(unitNormMedNo>cutoff);
                    
                    % Store data
                    obj(n).unitNormalizedMed=unitNormMed;
                    obj(n).unitNormalizedNo=unitNormMedNo;
                    obj(n).unitNormalized25=unit25;
                    obj(n).unitNormalized75=unit75;
                    obj(n).dataType=datatype;
                    obj(n).unitNormalizedz=avgz;
                    obj(n).dataExtent=dataextent;
                    obj(n).validData=valid;
                end % for n
            end % if nargin
        end % Constructor
        
        function obj=changeExtents(obj,extents)
            nmax=length(obj);
            for n=1:nmax
                obj(n).dataExtent=extents;
            end % for
        end
        
        function obj=resetProperty(obj,property,setting)
            obj.(property)=setting;
        end
    end % methods
    
    methods (Static)
        
        function obj=reCreate(structIn)
        % Creates object from structured data of class properties
            numTrans=length(structIn);
            for num=1:numTrans        
                % Create object
                obj(num)=clsNormData();
                % Get variable names from structure
                names=fieldnames(structIn(num));
                % Set properties to structure values
                nMax=length(names);
                for n=1:nMax
                    % Assign property
                    obj(num)=resetProperty(obj(num), names{n},structIn(num).(names{n}));
                end % for n
            end % for num
        end % reCreate        
    end % static methods    
end % class

