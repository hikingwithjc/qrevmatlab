classdef clsDepthStructure
% This class organizes the various possible sources for depth to streambed
% into a single structured class and establishes a refDepths property that
% contains the select source for discharge computations. The depthCell data
% is only contained in the btDepths and the refDepths as they depend on the
% draft of the btDepths and my be independent of the drafts for the other
% sources. The setDraft method handles changes to the draft of the redDepth
% to make sure it is applied properly and applied to the appropriate
% source. 
    
    properties
        selected % name of object of clsDepthData that constains the depth data for q computations
        btDepths % object of clsDepthData for bt depth data
        vbDepths % object of clsDepthData for vertical beam depth data
        dsDepths % object of clsDepthData for depth sounder depth data
        composite % Turn composite depths "On" or "Off"
        
        
    end % Properties
    
    methods
        
        function obj=clsDepthStructure()
        % Constructor only creates the structure. Data are added using the
        % addDepthObject method.
        end % Constructor
        
        function obj=addDepthObject(obj,depthIn, sourceIn, freqIn, draftIn, varargin)
        % Adds a clsDepthData object to the appropriate property
            % If varargin is not empty the contents need to be split into
            % variables before being passed to clsDepthData.
            if ~isempty(varargin)
                cellDepth=varargin{1};
                cellSize=varargin{2};
            end % if
            % Create appropriate object    
            switch sourceIn
                case 'BT'
                    obj.btDepths=clsDepthData(depthIn, sourceIn, freqIn, draftIn, cellDepth, cellSize);
                case 'VB'
                    obj.vbDepths=clsDepthData(depthIn, sourceIn, freqIn, draftIn);
                    obj.vbDepths=addCellData(obj.vbDepths,obj.btDepths);
                case 'DS'
                    obj.dsDepths=clsDepthData(depthIn, sourceIn, freqIn, draftIn);
                    obj.dsDepths=addCellData(obj.dsDepths,obj.btDepths);
            end % switch
        end % addDepthObject
        
        function obj=setDepthReference(obj,reference)
        % This function will set the selected depth reference to the 
        % specified depth reference
            switch reference
                case 'BT' 
                    obj.selected='btDepths';
                case 'btDepths'
                    obj.selected='btDepths';
                case 'VB'
                    obj.selected='vbDepths';
                case 'vbDepths'
                    obj.selected='vbDepths';
                case 'DS'
                    obj.selected='dsDepths';
                case 'dsDepths'
                    obj.selected='dsDepths';
            end % switch
        end % setDepthReference
        
        function obj=setValidDataMethod(obj,setting)
            obj.btDepths=setValidDataMethod(obj.btDepths,setting);
        end % setValidDataMethod
        
        function obj=compositeDepths(obj,transect,varargin)                
        % Depth compositing is based on the following assumptions:
        %
        % 1. If a depth sounder is available the user must have assumed the
        % ADCP beams (BT or vertical) might have problems and it will be
        % the second alternative if not selected as the preferred source.
        %
        % 2. For 4-beam BT depths, if 3 beams are valid the average is
        % considered valid. It may be based on interpolation of the
        % invalid beam. However, if only 2 beams are valid even
        % though they may be interpolated and included in the
        % average the average will be replaced by an alternative if
        % available. If no alternative is available the multi-beam 
        % average based on available beams and interpolation will be used.
            
            % If new composite setting is provided it is used, if not the
            % setting saved in the object is used.
            if isempty(varargin)
                setting=obj.composite;
            else
                setting=varargin{1};
                obj.composite=setting;
            end
            
            % The primary depth reference is the selected reference. 
            ref=obj.selected;
            
            available=[~isempty(obj.btDepths),~isempty(obj.vbDepths),~isempty(obj.dsDepths)];
            
            % Composite depths turned on
            if strcmpi(setting,'On') & sum(available)>1
                
                % Prepare vector of valid BT averages, which are defined as
                % having at least 2 valid beams prior to interpolation
                
                btValid=obj.btDepths.validData;
                nEnsembles=size(btValid,2);
                btFiltered=obj.btDepths.depthProcessed_m;
                btFiltered(~btValid)=nan;

                % Prepare vertical beam data, using only data prior to
                % interpolation
                if ~isempty(obj.vbDepths)
                    vbFiltered=obj.vbDepths.depthProcessed_m;
                    vbFiltered(~obj.vbDepths.validData)=nan;
                else
                    vbFiltered=nan(1,nEnsembles);
                end % if vbDepths

                % Prepare depth sounder data, using only data prior to
                % interpolation
                if ~isempty(obj.dsDepths)
                    dsFiltered=obj.dsDepths.depthProcessed_m;
                    dsFiltered(~obj.dsDepths.validData)=nan;
                else
                    dsFiltered=nan(1,nEnsembles);
                end % if dsDepths

                % Composite depth using the following orders of precedence   
                % BT, DS, VB; or VB, DS, BT; or DS, VB,BT. Any remaining 
                % invalid depths are replaced with interpolated depths from
                % the primary reference.
                compSource=nan(size(btFiltered));
                switch ref
                    case 'btDepths'
                        compDepth=btFiltered;
                        compSource(~isnan(compDepth))=1;
                        compDepth(isnan(compDepth))=dsFiltered(isnan(compDepth));
                        compSource(~isnan(compDepth) & isnan(compSource))=3;
                        compDepth(isnan(compDepth))=vbFiltered(isnan(compDepth));
                        compSource(~isnan(compDepth) & isnan(compSource))=2;
                        if ~isempty(obj.vbDepths)
                            intCompDepths = clsDepthStructure.intComposite(obj.vbDepths,transect,compDepth);
                        else
                            intCompDepths = clsDepthStructure.intComposite(obj.dsDepths,transect,compDepth);
                        end
                        compDepth=intCompDepths;
%                         compDepth(isnan(compDepth))=obj.btDepths.depthProcessed_m(isnan(compDepth));
                        compSource(~isnan(compDepth) & isnan(compSource))=4;


                    case 'vbDepths'
                        compDepth=vbFiltered;
                        compSource(~isnan(compDepth))=2;
                        compDepth(isnan(compDepth))=dsFiltered(isnan(compDepth));
                        compSource(~isnan(compDepth) & isnan(compSource))=3;
                        compDepth(isnan(compDepth))=btFiltered(isnan(compDepth));
                        compSource(~isnan(compDepth) & isnan(compSource))=1;
                        if ~isempty(obj.vbDepths)
                            intCompDepths = clsDepthStructure.intComposite(obj.vbDepths,transect,compDepth);
                        else
                            intCompDepths = clsDepthStructure.intComposite(obj.dsDepths,transect,compDepth);
                        end
                        compDepth=intCompDepths;
%                         compDepth(isnan(compDepth))=obj.vbDepths.depthProcessed_m(isnan(compDepth));
                        compSource(~isnan(compDepth) & isnan(compSource))=4;
                        
                    case 'dsDepths'
                        compDepth=dsFiltered;
                        compSource(~isnan(compDepth))=3;
                        compDepth(isnan(compDepth))=vbFiltered(isnan(compDepth));
                        compSource(~isnan(compDepth) & isnan(compSource))=2;
                        compDepth(isnan(compDepth))=btFiltered(isnan(compDepth));
                        compSource(~isnan(compDepth) & isnan(compSource))=1;
                        if ~isempty(obj.vbDepths)
                            intCompDepths = clsDepthStructure.intComposite(obj.vbDepths,transect,compDepth);
                        else
                            intCompDepths = clsDepthStructure.intComposite(obj.dsDepths,transect,compDepth);
                        end
                        compDepth=intCompDepths;
%                         compDepth(isnan(compDepth))=obj.dsDepths.depthProcessed_m(isnan(compDepth));
                        compSource(~isnan(compDepth) & isnan(compSource))=4;
                end % switch

                % Save composite depth to depthProcessed of selected
                % primary reference
                obj.(ref)=applyComposite(obj.(ref),compDepth,compSource);
            else
                compSource=zeros(size(obj.(ref).depthProcessed_m));
   
                switch ref
                    case 'btDepths'
                        compSource(obj.(ref).validData)=1;
                    case 'vbDepths'
                        compSource(obj.(ref).validData)=2;
                    case 'dsDepths'
                        compSource(obj.(ref).validData)=3;
                end % switch
                temp=applyInterpolation(obj.(ref),transect);
                compDepth=temp.depthProcessed_m;
                obj.(ref)=applyComposite(obj.(ref),compDepth,compSource);
            end % if
        end % compositeDepths
                
        function obj=setDraft(obj, target, draft)
        % This function will change the refDepth draft. The associated
        % depth object will also be updated because clsDepthData is a
        % handle class. The computations are actually done in clsDepthData
        % as the data are private to that class.
            if strcmp(target,'ADCP')
                obj.btDepths=changeDraft(obj.btDepths, draft);
                obj.vbDepths=changeDraft(obj.vbDepths, draft);
            else
                obj.dsDepths=changeDraft(obj.dsDepths, draft);
            end
        end % setDraft 
        
        function obj=depthFilter(obj,transect,varargin)
        % Method to apply filter to all available depth sources, so that
        % all sources have the same filter applied.
            if ~isempty(obj.btDepths)
                obj.btDepths=applyFilter(obj.btDepths,transect,varargin{:});
            end
            if ~isempty(obj.vbDepths)
                obj.vbDepths=applyFilter(obj.vbDepths,transect,varargin{:});
            end
            if ~isempty(obj.dsDepths)
                obj.dsDepths=applyFilter(obj.dsDepths,transect,varargin{:});
            end
        end % depthFilter
        
        function obj=depthInterpolation(obj,transect,varargin)
        % Method to apply interpolation to all available depth sources, so that
        % all sources have the same filter applied.
            if ~isempty(obj.btDepths)
                obj.btDepths=applyInterpolation(obj.btDepths,transect,varargin{:});
            end
            if ~isempty(obj.vbDepths)
                obj.vbDepths=applyInterpolation(obj.vbDepths,transect,varargin{:});
            end
            if ~isempty(obj.dsDepths)
                obj.dsDepths=applyInterpolation(obj.dsDepths,transect,varargin{:});
            end
        end % depthFilter
        
        function obj=sosCorrection(obj,ratio)
        % Correct depths for change in speed of sound
        %
        % INPUT:
        % 
        % obj: object of clsDepthStructure
        %
        % ratio: ratio of new to old speed of sound values
        %
        % OUTPUT:
        %
        % obj: object of clsDepthStructure
        
            % Bottom track depths
            if ~isempty(obj.btDepths)
               obj.btDepths=sosCorrection(obj.btDepths,ratio);
            end
            
            % Vertical beam depths
            if ~isempty(obj.vbDepths)
               obj.vbDepths=sosCorrection(obj.vbDepths,ratio);  
            end     
        end
        
        function obj=setProperty(obj,property,setting)
            obj.(property)=setting;
        end
        
    end % methods
    
    methods(Static)
        function intCompDepths = intComposite(obj,transect,compDepth)
        % Use linear interpolation to interpolate depths determined from
        % composite depths but where none of the depth sources had a valid
        % depth. Interpolation is done on the composite. The object is used
        % temporarily but not
        % modified permanently.
            obj=setProperty(obj,'depthSource','CD');
            obj=setProperty(obj,'depthBeams_m',compDepth);
            valid=~isnan(compDepth);
            obj=setProperty(obj,'validBeams',valid);
            obj=applyInterpolation(obj,transect,'Linear');
            intCompDepths=obj.depthProcessed_m;

        end % intCompDepths
        
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
            % Create object
            obj.depths=clsDepthStructure();
            % Get variable names from structure
            names=fieldnames(structIn);
            % Set properties to structure values
            nMax=length(names);
            for n=1:nMax
                if isstruct(structIn.(names{n}))
                    % Create object
                    obj.depths=clsDepthData.reCreate(obj.depths,structIn.(names{n}),names{n});
                else
                    % Assign property
                    obj.depths.(names{n})=structIn.(names{n});
                end
            end % for
        end % reCreate
    end % static methods    
end % class

