classdef clsSensorData
% The class stores typically time series data for (pitch, roll,
% temperature, salinity, and speed of sound and its source.
    properties (SetAccess=private)
        data
        dataOrig
        source
    end % properties
    
    methods
        function obj=clsSensorData(dataIn,sourceIn)
            if nargin > 0
                obj.data=dataIn;
                obj.dataOrig=dataIn;
                obj.source=sourceIn;      
            end  % nargin
        end % constructor
        
        function obj=changeData(obj,dataIn)
            obj.data=dataIn;
        end
            
        function obj=setSource(obj,sourceIn)
            obj.source=sourceIn;
        end
    end % methods
    
    methods (Static)
        function obj=reCreate(obj,structIn,type)
        % Creates object from structured data of class properties
            % Create object
            obj.(type)=clsSensorData();
            if isstruct(structIn)
                % Get variable names from structure
                names=fieldnames(structIn);
                % Set properties to structure values
                nMax=length(names);
                for n=1:nMax
                     obj.(type).(names{n})=structIn.(names{n});
                end % for n
            else
                obj.(type)=structIn;
            end
        end % reCreate
    end % static methods    
end % class

