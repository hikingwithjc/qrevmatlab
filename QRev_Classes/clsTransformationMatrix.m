classdef clsTransformationMatrix
% Determines the transformation matrix for the specified ADCP model from 
% the data provided.
%
% Originally developed as part of the QRev processing program
% Programmer: David S. Mueller, Hydrologist, USGS, OSW, dmueller@usgs.gov
% Other Contributors: none
% Latest revision: 3/11/2015

    properties
        source % Source of matrix (Nominal, ADCP)
        matrix % Transformation matrix, 4X4 matrix for TRDI, 4x4x3 or 2 for SonTek.
    end % properties
    
    methods
        function obj=clsTransformationMatrix(manufacturer,varargin)
        % Uses the manufacturer and model to determine how to parse the
        % transformation matrix. If no transformation matrix
        % information is available a nominal transformation matrix for
        % that model is assumed.
        % 
        % INPUT:
        %
        % manufacturer: 'TRDI' or 'SonTek'
        %
        % varargin: For TRDI, varargin(1) is the ADCP model and varargin(2)
        % is the output from the system test. For SonTek, varargin(1) is
        % the loaded Matlab data structure.
            % Allow creation of object with no input
            if nargin > 0            
                % ID manufacturer
                if strcmp(manufacturer,'TRDI')
                    % TRDI ADCP model
                    adcpModel=varargin{1};
                    % Set nominal matrix based on model                   
                    temp=[1.4619 -1.4619 0 0 ; 0 0 -1.4619 1.4619;...
                            0.2661 0.2661 0.2661 0.2661; 1.0337 1.0337 -1.0337 -1.0337];
                    if strcmpi(adcpModel,'RiverRay')
                        temp=[1 -1 0 0 ; 0 0 -1 1; 0.2887 0.2887 0.2887 0.2887;...
                            0.7071 0.7071 -0.7071 -0.7071];
                    end

                    % Retrieve transformation matrix from ADCP output, if
                    % available.
                    dataIn=varargin{2};
                    obj.source='Nominal';
                    if strcmp(dataIn,'Nominal')
                        obj.source='Nominal';
                    elseif strcmpi(adcpModel,'Rio Grande')
                        % Rio Grande
                        idx=strfind(dataIn,'Instrument Transformation Matrix (Down):');
                        if ~isempty(idx)
                            cellMatrix=textscan(dataIn(idx+48:idx+356),'%f');
                            temp=permute(reshape(cellMatrix{1},[8,4]),[2,1]);
                            obj.source='ADCP';
                        end
                    elseif strcmpi(adcpModel,'StreamPro')
                        % StreamPro
                        idx=strfind(dataIn,'>PS3');
                        if ~isempty(idx)
                            temp2=str2num(dataIn(idx+5:idx+138));
                            if ~isempty(temp2)
                                temp=temp2;
                            end
                            obj.source='ADCP';
                        end
                    elseif strcmpi(adcpModel,'RiverRay')
                        % RiverRay
                        idx=strfind(dataIn,'Instrument Transformation Matrix');
                        if ~isempty(idx)
                            idx2=strfind(dataIn(idx:end),':');
                            idx3=idx+idx2(1);
                            if ~isempty(idx2)
                                idx4=strfind(dataIn(idx3:end),'>');
                                idx5=idx3+idx4(1)-2;
                                if ~isempty(idx4)
                                    temp=str2num(dataIn(idx3:idx5));
                                    obj.source='ADCP';
                                end
                            end
                        end
                    elseif strcmpi(adcpModel,'RiverPro')
                        idx=strfind(dataIn,'Instrument Transformation Matrix');
                        if ~isempty(idx)
                            idx2=strfind(dataIn(idx:end),':');
                            idx3=idx+idx2(1);
                            if ~isempty(idx2)
                                idx4=strfind(dataIn(idx3:end),'Has V-Beam');
                                idx5=idx3+idx4(1)-2;
                                if ~isempty(idx4)
                                    temp=str2num(dataIn(idx3:idx5));
                                    obj.source='ADCP';
                                end
                            end
                        end
                    elseif strcmpi(adcpModel,'RioPro')
                        idx=strfind(dataIn,'Instrument Transformation Matrix');
                        if ~isempty(idx)
                            idx2=strfind(dataIn(idx:end),':');
                            idx3=idx+idx2(1);
                            if ~isempty(idx2)
                                idx4=strfind(dataIn(idx3:end),'Has V-Beam');
                                idx5=idx3+idx4(1)-2;
                                if ~isempty(idx4)
                                    temp=str2num(dataIn(idx3:idx5));
                                    obj.source='ADCP';
                                end
                            end
                        end
                    elseif strcmp(adcpModel,'pd0')
                        temp=dataIn.Inst.tMatrix;                        
                    end
                    obj.matrix=temp(1:4,1:4);
                else
                    % SonTek M9/S5
                    RS=varargin{1};
                    obj.source='ADCP';
                    % Note: for M9 this is a 4x4x3 matrix (3000,500,1000)
                    % Note: for S5 this is a 4X4X2 matrix (3000,1000);
                    obj.matrix=RS;
                end % manufacturer
            end %nargin
        end % constructor 
    end % methods
    
    methods (Static)
        function obj=reCreate(structIn)
            obj=clsTransformationMatrix();
            obj.source=structIn.source;
            obj.matrix=structIn.matrix;
        end
    end % static methods
end % class

