classdef clsBoatStructure
% This class organizes the various possible sources for boat velocity 
% into a single structured class and establishes a selected property that
% contains the select source for velocity and discharge computations. 
%
% Originally developed as part of the QRev processing program
% Programmer: David S. Mueller, Hydrologist, USGS, OSW, dmueller@usgs.gov
% Other Contributors: none
% Latest revision: 3/13/2015   

    properties
        selected % name of clsBoatData object to be used for discharge computations
        btVel % object of clsBoatData for bottom track velocity
        ggaVel % object of clsBoatData for gga velocity
        vtgVel % object of clsBoatData for vtg velocity
        composite % setting for composite tracks "On", "Off
    end % Properties
    
    methods
        
        function obj=clsBoatStructure()
        % Constructor only creates the structure. Data are added using the
        % addBoatVelObject method.
        %
        % INPUT:
        %
        % None
        %
        % OUTPUT:
        %
        % obj: object of clsBoatData
        
        % Composite track information is not currently provided by the
        % manufacturers. Future versions may try to determine this setting
        % from SonTek data.
            % Allow creation of object with no input
%             if nargin > 0        
                obj.composite='Off';
%             end % nargin
        end % Constructor
        
        function obj=addBoatObject(obj,source,velIn,freqIn,coordSysIn,navRefIn,varargin)
        % Adds a clsBoatData object to the appropriate property
        %
        % INPUT:
        % 
        % obj: object of clsBoatStructure
        % 
        % source: name of manufacturer
        %
        % velIn: boat velocity array
        %
        % freqIn: acoustic frequency, if available
        %
        % coordSysIn: coordinate system of data in velIn
        %
        % navRefIn: source of boat
        %
        % varargin: used for bottom track velocities
        %   varargin{1}: 3 beam solution setting
        %   varargin{2}: bottom mode
        %
        % OUTPUT:
        %
        % obj: object of clsBoatStructure
        
            % Create appropriate object    
            switch navRefIn
                case 'BT'
                    obj.btVel=clsBoatData(source,velIn,freqIn,coordSysIn,navRefIn,varargin{1:end});
                   
                case 'GGA'
                    obj.ggaVel=clsBoatData(source,velIn,freqIn,coordSysIn,navRefIn);
                    
                case 'VTG'
                    obj.vtgVel=clsBoatData(source,velIn,freqIn,coordSysIn,navRefIn);
            end % switch
            
        end % addDepthObject
        
        function obj=setNavReference(obj,reference)
        % This function will set the navigation reference property to the
        % specified object reference
        %
        % INPUT:
        % 
        % obj: object of clsBoatStructure
        %
        % reference: selected navigation reference (BT, GGA, VTG)
        %
        % OUTPUT:
        %
        % obj: object of clsBoatStructure
        
            % Set selected reference
            switch reference
                case 'BT'
                    obj.selected='btVel';
                case 'GGA'
                    obj.selected='ggaVel';
                case 'VTG'
                    obj.selected='vtgVel';
            end % switch
            
        end % setNavReference
        
        function obj=changeNavReference(obj,reference,transect)
        % This function changes the navigation reference to the specified
        % object reference and recomputes composite tracks, if necessary.
        % 
        % INPUT:
        %
        % obj: object of clsBoatStructure
        %
        % reference: new navigation reference
        %
        % transect: object of clsTransectData
        
            % Assign new reference
            switch reference
                case 'BT' 
                    obj.selected='btVel';
                case 'btVel'
                    obj.selected='btVel';
                case 'GGA' 
                    obj.selected='ggaVel';
                case 'ggaVel'
                    obj.selected='ggaVel';
                case 'VTG' 
                    obj.selected='vtgVel';
                case 'vtgVel'
                    obj.selected='vtgVel';
            end % switch
            
            % Update composite tracks
            obj=compositeTracks(obj,transect);
            
        end % setNavReference
        
        function obj=changeCoordSys(obj,newCoordSys,sensors,adcp)
        % This function will change the coordinate system of the boat 
        % velocity reference. 
        %
        % INPUT:
        % 
        % obj: object of clsBoatStructure
        %
        % newCoordSys: specified new coordinate system
        %
        % sensors: object of clsSensors
        %
        % adcp: object of clsInstrumentData
        %
        % OUTPUT:
        %
        % obj: object of clsBoatStructure
        
            % Change coordinate system for all available boat velocity
            % sources
            if ~isempty(obj.btVel)
                obj.btVel=changeCoordSys(obj.btVel,newCoordSys,sensors,adcp);
            end
            if ~isempty(obj.ggaVel)
                obj.ggaVel=changeCoordSys(obj.ggaVel,newCoordSys,sensors,adcp);
            end
            if ~isempty(obj.vtgVel)
                obj.vtgVel=changeCoordSys(obj.vtgVel,newCoordSys,sensors,adcp);
            end
            
        end % changeCoordSys 
                          
        function obj=compositeTracks(obj,transect, varargin)   
        % If new composite setting is provided it is used, if not the
        % setting saved in the object is used.
        %
        % INPUT:
        % 
        % obj: object of clsBoatStructure
        %
        % transect: object of clsTransectData
        %
        % varargin: new setting for composite tracks
        %
        % OUTPUT:
        %
        % obj: object of clsBoatStructure 
        
            % Set composite setting
            if isempty(varargin)
                % From object
                setting=obj.composite;
            else
                % New setting
                setting=varargin{1};
                obj.composite=setting;
            end % if isempty
                       
            % Composite depths turned on
            if strcmpi(setting,'On') 
                
                % Prepare BT data
                if ~isempty(obj.btVel)
                    % Get BT velocities                    
                    uBT=obj.btVel.uProcessed_mps;
                    vBT=obj.btVel.vProcessed_mps;
                    % Set to invalid all interpolated velocities
                    validBT=obj.btVel.validData(1,:);
                    uBT(~validBT)=nan;
                    vBT(~validBT)=nan;
                end % if btVel
                
                % Prepare GGA data
                if ~isempty(obj.ggaVel)
                    % Get gga velocities                    
                    uGGA=obj.ggaVel.uProcessed_mps;
                    vGGA=obj.ggaVel.vProcessed_mps;
                    % Set to invalid all interpolated velocities
                    validGGA=obj.ggaVel.validData(1,:);
                    uGGA(~validGGA)=nan;
                    vGGA(~validGGA)=nan;
                else
                    uGGA=nan(size(uBT));
                    vGGA=nan(size(vBT));
                end % if ggaVel
                
                % Prepare VTG data
                if ~isempty(obj.vtgVel)
                    % Get gga velocities                    
                    uVTG=obj.vtgVel.uProcessed_mps;
                    vVTG=obj.vtgVel.vProcessed_mps;
                    % Set to invalid all interpolated velocities
                    validVTG=obj.vtgVel.validData(1,:);
                    uVTG(~validVTG)=nan;
                    vVTG(~validVTG)=nan;
                else
                    uVTG=nan(size(uBT));
                    vVTG=nan(size(vBT));
                end % if vtgVel                
                    
                % Process BT
                if ~isempty(obj.btVel)
                    
                    % Initialize composite source
                    compSource=nan(size(uBT));
                    
                    % Process u velocity component
                    uComp=uBT;
                    compSource(~isnan(uComp))=1;

                    % If BT data are not valid try VTG and set composite
                    % source
                    uComp(isnan(uComp))=uVTG(isnan(uComp));
                    compSource(~isnan(uComp) & isnan(compSource))=3;
                    
                    % If there are still invalid boat velocities, try GGA
                    % and set composite source
                    uComp(isnan(uComp))=uGGA(isnan(uComp));
                    compSource(~isnan(uComp) & isnan(compSource))=2;
                    
                    % If there are still invalid boat velocities, use
                    % interpolated values if present and set composite
                    % source
%                     uComp(isnan(uComp))=obj.btVel.uProcessed_mps(isnan(uComp));
                    compSource(~isnan(uComp) & isnan(compSource))=0;
                    
                    % Set composite source to invalid for all remaining
                    % invalid boat velocity data
                    compSource(isnan(compSource))=-1;

                    % Process v velocity component. Assume that the
                    % composite source is the same as the u component.
                    vComp=vBT;
                    vComp(isnan(vComp))=vVTG(isnan(vComp));
                    vComp(isnan(vComp))=vGGA(isnan(vComp));
                    vComp(isnan(vComp))=obj.btVel.vProcessed_mps(isnan(vComp));

                    % 
                    
                    % Apply the composite settings to the bottom track 
                    % clsBoatData objects
                    obj.btVel=applyComposite(obj.btVel,uComp,vComp,compSource);
                    obj.btVel=interpolateComposite(obj.btVel,transect);
                end
                
                % Process GGA
                if ~isempty(obj.ggaVel)
                    
                    % Initialize the composite source
                    compSource=nan(size(uBT));
                    
                    % Process the u velocity component
                    uComp=uGGA;
                    compSource(~isnan(uComp))=2;

                    % If GGA data are not valid try VTG and set composite
                    % source
                    uComp(isnan(uComp))=uVTG(isnan(uComp));
                    compSource(~isnan(uComp) & isnan(compSource))=3;
                    
                    % If there are still invalid boat velocities, try BT
                    % and set composite source
                    uComp(isnan(uComp))=uBT(isnan(uComp));
                    compSource(~isnan(uComp) & isnan(compSource))=1;
                    
                    % If there are still invalid boat velocities, use
                    % interpolated values, if present and set composite
                    % source
%                     uComp(isnan(uComp))=obj.ggaVel.uProcessed_mps(isnan(uComp));
                    compSource(~isnan(uComp) & isnan(compSource))=0;
                    
                    % Set composite source to invalid for all remaining
                    % invalid boat velocity data
                    compSource(isnan(compSource))=-1;
                    
                    % Process v velocity component. Assume that the
                    % composite source is the same as the u component.
                    vComp=vGGA;
                    vComp(isnan(vComp))=vVTG(isnan(vComp));
                    vComp(isnan(vComp))=vBT(isnan(vComp));
                    vComp(isnan(vComp))=obj.ggaVel.vProcessed_mps(isnan(vComp));

                    % Apply the composite settings to the gga clsBoatData
                    % object
                    obj.ggaVel=applyComposite(obj.ggaVel,uComp,vComp,compSource);
                    obj.ggaVel=interpolateComposite(obj.ggaVel,transect);
                end
                
                % Process VTG
                if ~isempty(obj.vtgVel)
                    
                    % Intialize composite source
                    compSource=nan(size(uBT));
                    
                    % Process u velocity component
                    uComp=uVTG;
                    compSource(~isnan(uComp))=3;

                    % If VTG data are not valid try GGA and set composite
                    % source
                    uComp(isnan(uComp))=uGGA(isnan(uComp));
                    compSource(~isnan(uComp) & isnan(compSource))=2;
                    
                    % If there are still invalid boat velocities, try BT
                    % and set composite source
                    uComp(isnan(uComp))=uBT(isnan(uComp));
                    compSource(~isnan(uComp) & isnan(compSource))=1;
                    
                    % If there are still invalid boat velocities, use
                    % interpolated values if present and set composite
                    % source
%                     uComp(isnan(uComp))=obj.vtgVel.uProcessed_mps(isnan(uComp));
                    compSource(~isnan(uComp) & isnan(compSource))=0;
                    
                    % Set composite source to invalid for all remaining
                    % invalid boat velocity data
                    compSource(isnan(compSource))=-1;

                    % Process v velocity component. Assume that the
                    % composite source is the same as the u component.
                    vComp=vVTG;
                    vComp(isnan(vComp))=vGGA(isnan(vComp));
                    vComp(isnan(vComp))=vBT(isnan(vComp));
                    vComp(isnan(vComp))=obj.vtgVel.vProcessed_mps(isnan(vComp));

                    % Apply the composite settings to the vtg clsBoatData
                    % object
                    obj.vtgVel=applyComposite(obj.vtgVel,uComp,vComp,compSource);
                    obj.vtgVel=interpolateComposite(obj.vtgVel,transect);
                end
                
            else
                % Composit tracks off
                % ===================
                
                % Use only interpolations for BT
                if ~isempty(obj.btVel)
                    obj.btVel=applyInterpolation(obj.btVel,transect);
                    compSource=nan(size(obj.btVel.uProcessed_mps));
                    compSource(obj.btVel.validData(1,:))=1;
                    compSource(isnan(compSource) & ~isnan(obj.btVel.uProcessed_mps))=0; 
                    compSource(isnan(compSource))=-1;
                       
                    obj.btVel=applyComposite(obj.btVel,obj.btVel.uProcessed_mps,obj.btVel.vProcessed_mps,compSource);
                end
                
                % Use only interpolations for GGA
                if ~isempty(obj.ggaVel)
                    obj.ggaVel=applyInterpolation(obj.ggaVel,transect);
                    compSource=nan(size(obj.ggaVel.uProcessed_mps));
                    compSource(obj.ggaVel.validData(1,:))=2;
                    compSource(isnan(compSource) & ~isnan(obj.ggaVel.uProcessed_mps))=0; 
                    compSource(isnan(compSource))=-1;
                    obj.ggaVel=applyComposite(obj.ggaVel,obj.ggaVel.uProcessed_mps,obj.ggaVel.vProcessed_mps,compSource);
                end
                
                % Use only interpolations for VTG
                if ~isempty(obj.vtgVel)
                    obj.vtgVel=applyInterpolation(obj.vtgVel,transect);
                    compSource=nan(size(obj.vtgVel.uProcessed_mps));
                    compSource(obj.vtgVel.validData(1,:))=3;
                    compSource(isnan(compSource) & ~isnan(obj.vtgVel.uProcessed_mps))=0; 
                    compSource(isnan(compSource))=-1;
                    obj.vtgVel=applyComposite(obj.vtgVel,obj.vtgVel.uProcessed_mps,obj.vtgVel.vProcessed_mps,compSource);
                end
                
            end % if composite tracks on
        end % compositeTracks
    end % methods
    
    methods(Static)
        function boat_track = compute_boat_track(transect)
            boat_track.distance_m = nan;
            boat_track.track_x_m = nan;
            boat_track.track_y_m = nan;
            boat_track.dmg_m = nan;
            
            if ~isempty(transect.boatVel.selected) 
                track_x = transect.boatVel.(transect.boatVel.selected).uProcessed_mps.*transect.dateTime.ensDuration_sec;
                track_y = transect.boatVel.(transect.boatVel.selected).vProcessed_mps.*transect.dateTime.ensDuration_sec;
            end
                
            if sum(~isnan(track_x)) > 1
                boat_track.distance_m = nancumsum(sqrt(track_x.^2 + track_y.^2));
                boat_track.track_x_m = nancumsum(track_x);
                boat_track.track_y_m = nancumsum(track_y);
                boat_track.dmg_m = sqrt(track_x.^2 + track_y.^2); 
            end
        end
        
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
            % Create object
            obj.boatVel=clsBoatStructure();
            % Get variable names from structure
            names=fieldnames(structIn);
            % Set properties to structure values
            nMax=length(names);
            for n=1:nMax
                if isstruct(structIn.(names{n}))
                    % Create boatData object
                    obj.boatVel=clsBoatData.reCreate(obj.boatVel,structIn.(names{n}),names{n});
                else
                    % Assign boatVel property
                    obj.boatVel.(names{n})=structIn.(names{n});
                end
            end % for
        end % reCreate
    end % static methods
end % class


