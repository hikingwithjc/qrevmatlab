classdef clsWaterData
% Class to process and store water velocity data.
%
% Originally developed as part of the QRev processing program
% Programmer: David S. Mueller, Hydrologist, USGS, OSW, dmueller@usgs.gov
% Other Contributors: none
% Latest revision: 3/31/2015

    properties (SetAccess=private) 
        rawVel_mps % Contains the raw unfiltered velocity data in m/s. Rows 1-4 are
                % beams 1,2,3,4 if beam or u,v,w,d if otherwise
        frequency % Defines ADCP frequency used for velocity measurement
        origCoordSys % Defines the original raw data velocity coordinate system "Beam", "Inst", "Ship", "Earth"
        origNavRef % Defines the original raw data navigation reference: "None", "BT", "GGA", "VTG"
        corr % Correlation values for WT, if available
        rssi % Returned acoustic signal strength.
        rssiUnits % Units for returned acoustic signal strength: "Counts", "dB", "SNR"
        waterMode % WaterMode for TRDI or 'Variable' for SonTek
        blankingDistance_m % Distance below transducer where data is marked invalid due to potential ringing interference
        cellsAboveSL % Logical array of depth cells above the sidelobe cutoff based on selected depth reference
        cellsAboveSLbt % Logical array of depth cells above the sidelobe cutoff based on BT
        slLagEffect_m % Side lobe distance due to lag and transmit length
        % Data computed in this class
        uEarthNoRef_mps % Horizontal velocity in x-direction with no boat referenced applied, in m/s
        vEarthNoRef_mps % Horizontal velocity in y-direction with no boat referenced applied, in m/s
        u_mps % Horizontal velocity in x-direction, earth coord, nav referenced, in m/s
        v_mps % Horizontal velocity in y-direction, earth coord, nav referenced, in m/s
        uProcessed_mps % Horizontal velocity in x-direction, earth coord, nav referenced, filtered, and interpolated
        vProcessed_mps % Horizontal velocity in y-direction, earth coord, nav referenced,filtered and interpolated
        w_mps % Vertical velocity (+ up), in m/s
        d_mps % Difference in vertical velocities compute from opposing beam pairs, in m/s
        invalidIndex % Index of ensembles with no valid raw velocity data
        numInvalid % Estimated number of depth cells in ensembles with no valid raw velocity data
        validData % 3-D logical array of valid data
                    % Dim3 1 - composite
                    % Dim3 2 - original, cells above side lobe
                    % Dim3 3 - dFilter
                    % Dim3 4 - wFilter
                    % Dim3 5 - smoothFilter
                    % Dim3 6 - beamFilter
                    % Dim3 7 - excluded
                    % Dim3 8 - snrFilter
                    % Dim3 9 - validDepthFilter

    end % Private properties
    
    properties
        % These properties can be changed by user, typically from GUI
        % -----------------------------------------------------------
        beamFilter % 3 for 3-beam solutions, 4 for 4-beam solutions        
        dFilter % Difference velocity filter "On", "Off"
        dFilterThreshold % Threshold for difference velocity filter
        wFilter % Vertical velocity filter "On", "Off"
        wFilterThreshold % Threshold for vertical velocity filter
        excludedDist % Distance below transducer for which data are excluded or marked invalid
        smoothFilter % Filter based on smoothing function
        smoothSpeed % Smoothed boat speed
        smoothUpperLimit % Smooth function upper limit of window
        smoothLowerLimit % Smooth function lower limit of window 
        snrFilter % SNR filter for SonTek data
        snrRng % Range of beam averaged SNR
        wtDepthFilter % WT in ensembles with invalid WT are marked invalid
        interpolateEns % Type of interpolation: "None", "ExpandedT","Hold9","HoldLast","Linear","TRDI"
        interpolateCells % Type of cell interpolation: "None", "TRDI", "Linear"
        coordSys % Defines the velocity coordinate system "Beam", "Inst", "Ship", "Earth"
        navRef % Defines the navigation reference: "None", "BT", "GGA", "VTG"
        slCutoffPer % Percent cutoff defined by cos(angle)
        slCutoffNum % User specified number of cells to cutoff above slCutoff
        slCutoffType % "Percent" or "Number"
    end % Properties
    
    methods
        function obj=clsWaterData(velIn,freqIn,coordSysIn,navRefIn,rssiIn,...
                rssiUnitsIn,excludedDist,cellsAboveSL,slCutoffPerIn,slCutoffNumIn,slCutoffTypeIn,...
                slLagEffect_m,wm,blank,varargin)
         % Constructor method for clsWaterData
         %
         % INPUT:
         %
         % velIn: Contains the raw unfiltered velocity data in m/s. Rows 1-4 are
         %        beams 1,2,3,4 if beam or u,v,w,d if otherwise
         %
         % freqIn: Defines ADCP frequency used for velocity measurement
         %
         % coordSysIn: Defines the original raw data velocity coordinate system "Beam", "Inst", "Ship", "Earth"
         %
         % navRefIn: Defines the original raw data navigation reference: "None", "BT", "GGA", "VTG"
         %
         % rssiIn: Returned acoustic signal strength.
         %
         % rssiUnitsIn: Units for returned acoustic signal strength: "Counts", "dB", "SNR"
         %
         % excludedDist: Distance below transducer for which data are excluded or marked invalid
         %
         % cellsAboveSL: Logical array of depth cells above the sidelobe cutoff based on selected depth reference
         %
         % slCutoffPerIn: Percent cutoff defined by cos(angle)
         %
         % slCutoffNumIn: User specified number of cells to cutoff above slCutoff
         %
         % slCutoffTypeIn: "Percent" or "Number"
         %
         % slLagEffect_m: Side lobe distance due to lag and transmit length
         %
         % wm: waterMode for TRDI or 'Variable' for SonTek
         %
         % varargin:
         %  varargin{1}: Correlation values for WT, if available
         %  varargin{2}: Velocity for RiverRay surface bins
         %  varargin{3}: Returned acoustic signal strength for RiverRay surface cells
         %  varargin{4}: Correlation for RiverRay surface cells
         %  varargin{5}: Number of RiverRay surface cells
         %
         % OUTPUT:
         %
         % obj: object of clsWaterData
         
            % Check for arguments to assign to properties
            if nargin > 0

                % No correlation data
                if isempty(varargin)
                    obj.rawVel_mps=velIn;
                    obj.rssi=rssiIn; 
                    obj.rssiUnits=rssiUnitsIn;
                    obj.corr=nan(size(rssiIn));
                    
                % Correlation data provided
                elseif length(varargin)==1 
                    obj.rawVel_mps=velIn;
                    obj.rssi=rssiIn; 
                    obj.rssiUnits=rssiUnitsIn;
                    obj.corr=varargin{1};
                        
                % Surface cells data
                elseif length(varargin)>1
                    obj.rssiUnits=rssiUnitsIn;
                    corrIn=varargin{1};
                    surfaceVel=varargin{2};
                    surfaceRssi=varargin{3};
                    surfaceCorr=varargin{4};
                    noSurfCells=varargin{5};
                    noSurfCells(isnan(noSurfCells))=0;
                    maxCells=size(cellsAboveSL,1);
                    numEns=size(cellsAboveSL,2);
                    numRegCells=size(velIn,1);
                    maxSurfCells=maxCells-numRegCells;
                    
                    % Combine Surface Velocity Bins and Regular Velocity Bins into 
                    % one matrix
                    obj.rawVel_mps=nan(maxCells,numEns,4);
                    if maxSurfCells>0
                        obj.rawVel_mps(1:maxSurfCells,:,:)=surfaceVel(1:maxSurfCells,:,:);
                        obj.rssi(1:maxSurfCells,:,:)=surfaceRssi(1:maxSurfCells,:,:);
                        obj.corr(1:maxSurfCells,:,:)=surfaceCorr(1:maxSurfCells,:,:);
                    end
                    for iCell=1:numEns
                        obj.rawVel_mps(noSurfCells(iCell)+1:noSurfCells(iCell)+numRegCells,iCell,:)=velIn(1:numRegCells,iCell,:);
                        obj.rssi(noSurfCells(iCell)+1:noSurfCells(iCell)+numRegCells,iCell,:)=rssiIn(1:numRegCells,iCell,:);
                        obj.corr(noSurfCells(iCell)+1:noSurfCells(iCell)+numRegCells,iCell,:)=corrIn(1:numRegCells,iCell,:);
                    end                    
                end % varargin    
                
                % Set object properties from input data
                obj.frequency=freqIn;               
                obj.origCoordSys=coordSysIn;
                obj.coordSys=coordSysIn;
                obj.origNavRef=navRefIn;
                obj.navRef=navRefIn;
                obj.u_mps=squeeze(obj.rawVel_mps(:,:,1));
                obj.v_mps=squeeze(obj.rawVel_mps(:,:,2));
                obj.w_mps=squeeze(obj.rawVel_mps(:,:,3));
                obj.d_mps=squeeze(obj.rawVel_mps(:,:,4)); 
                
                % Because Matlab pads arrays with zeros and RR data has
                % variable number of bins, the raw data may be padded with
                % zeros. The next 4 statements changes those to nan.
                if length(varargin)>1  
                    obj.u_mps(obj.u_mps==0)=nan;
                    obj.v_mps(obj.v_mps==0)=nan;
                    obj.w_mps(obj.w_mps==0)=nan;
                    obj.d_mps(obj.d_mps==0)=nan;
                end
                
                obj.waterMode=wm;
                obj.excludedDist=excludedDist;   
                if isnumeric(blank)
                    obj.blankingDistance_m=blank;
                else
                    obj.blankingDistance_m=excludedDist;
                end
                obj.cellsAboveSL=cellsAboveSL;
                obj.cellsAboveSLbt=cellsAboveSL;
                obj.slCutoffPer=slCutoffPerIn;
                obj.slCutoffNum=slCutoffNumIn;
                obj.slCutoffType=slCutoffTypeIn;
                obj.slLagEffect_m=slLagEffect_m;

                % Set filter defaults to no filtering and no interpolation
                obj.beamFilter=3;
                obj.dFilter='Off';
                obj.dFilterThreshold=99;
                obj.wFilter='Off';
                obj.wFilterThreshold=99;
                obj.smoothFilter='Off';
                obj.interpolateEns='None';            
                obj.interpolateCells='None';
                
                % Determine original valid
                % ========================
                % Initialize validData property
                obj.validData=repmat(obj.cellsAboveSL,[1,1,9]);
                
                % Find invalid raw data
                validVel=repmat(obj.cellsAboveSL,[1,1,4]);
                validVel(isnan(obj.rawVel_mps))=0;
                if length(varargin)>1
                    validVel(obj.rawVel_mps==0)=0;
                end

                % Identify invalid velocity data (less than 3 valid beams)
                validVelSum=sum(validVel,3);
                validData2=obj.cellsAboveSL;
                validData2(validVelSum<3)=false;

                % Set validData property for original data
                obj.validData(:,:,2)=validData2;
                
               % Combine all filter data to composite valid data
                obj=allValidData(obj);
                
                % Esimate the number of cells in invalid ensembles using
                % adjacent valid ensembles
                validData2Sum=nansum(obj.validData(:,:,2));
                obj.invalidIndex=find(validData2Sum==0);
                nInvalid=length(obj.invalidIndex);
                for n=1:nInvalid
                    
                    % Find first valid ensemble
                    idx1=find(validData2Sum(1:obj.invalidIndex(n))>0,1,'last');
                    if isempty(idx1)
                        idx1=obj.invalidIndex(n);
                    end % idx1 empty
                    
                    % Find next valid ensemble
                    idx2=find(validData2Sum(obj.invalidIndex(n):end)>0,1,'first');
                    if isempty(idx2)
                        idx2=obj.invalidIndex(n);
                    end % idx2 empty
                    
                    % Estimate number of cells in invalid ensemble
                    obj.numInvalid(n)=floor((validData2Sum(idx1)+validData2Sum(idx2))./2);
                end % for n
                
                % Set processed data to non-interpolated valid data
                obj.uProcessed_mps=obj.u_mps;
                obj.vProcessed_mps=obj.v_mps;
                obj.uProcessed_mps(~obj.validData(:,:,1))=nan;
                obj.vProcessed_mps(~obj.validData(:,:,1))=nan;
                
                % Compute SNR range if SNR data provided
                if strcmp(rssiUnitsIn,'SNR')
                    obj=computeSNRRng(obj);
                end
            end % if nargin
        end % End constructor 
        
        function obj=setProperty(obj,property,setting)
            obj.(property)=setting;
        end
        
        function obj=changeCoordSys(velObj,newCoordSys,sensors,adcp)
        % This function allows the coordinate system to be changed.
        % Current implementation is only to allow change to a higher order
        % coordinate system Beam - Inst - Ship - Earth.
        %
        % INPUT:
        %
        % velObj: object of clsWaterData
        %
        % newCoordSys: new coordinate system (Beam, Inst, Ship,
        %              Earth)
        %
        % sensors: object of clsSensors
        %
        % adcp: object of clsInstrumentData
        % 
        % OUTPUT:
        % 
        % obj: object of clsWaterData
        
            % Assign the input to the output object
            obj=velObj;
            
            % Remove any trailing spaces
            oCoordSys=strtrim(velObj.origCoordSys);
                       
            if ~strcmp(oCoordSys,newCoordSys) 
                
                % Assign the transformation matrix and retrive the
                % sensor data.
                tMatrix=adcp.tMatrix.matrix;
                tMatrixFreq=adcp.frequency_hz;
                p=sensors.pitch_deg.(sensors.pitch_deg.selected).data;
                r=sensors.roll_deg.(sensors.roll_deg.selected).data;
                h=sensors.heading_deg.(sensors.heading_deg.selected).data;
                
                % Modify the transformation matrix and heading, pitch,
                % and roll values based on the original coordinate
                % system so that only the needed values are used in
                % computing the new coordinate system.
                switch deblank(oCoordSys)
                    case 'Beam'
                        origSys=1;
                    case 'Inst'
                        origSys=2;
                    case 'Ship'
                        origSys=3;
                        p=zeros(size(h));
                        r=zeros(size(h));
                        tMatrix(:)=eye(size(tMatrix));
                    case 'Earth'
                        origSys=4;
                end % switch
                
                % Assign a value to the new coordinate system
                switch deblank(newCoordSys)
                    case 'Beam'
                        newSys=1;
                    case 'Inst'
                        newSys=2;
                    case 'Ship'
                        newSys=3;
                    case 'Earth'
                        newSys=4;
                end % switch
                
                % Check to ensure the new coordinate system is a higher
                % order than the original system.
                if newSys-origSys>0 
                    
                    % Compute trig functions for heading, pitch, and roll
                    CH=cosd(h);
                    SH=sind(h);
                    CP=cosd(p);
                    SP=sind(p);
                    CR=cosd(r);
                    SR=sind(r); 
                    
                    % Preallocate array
                    velChanged=nan(size(velObj.rawVel_mps));
                    nEns=size(velObj.rawVel_mps,2);
                    
                    % Apply matrices on an ensemble by ensemble basis
                    for ii=1:nEns  
                        
                        % Compute matrix for heading, pitch, and roll
                        hprMatrix=[((CH(ii).*CR(ii))+(SH(ii).*SP(ii).*SR(ii))) (SH(ii).*CP(ii)) ((CH(ii).*SR(ii))-(SH(ii).*SP(ii).*CR(ii)));...
                                ((-1.*SH(ii).*CR(ii))+(CH(ii).*SP(ii).*SR(ii))) (CH(ii).*CP(ii)) ((-1.*SH(ii).*SR(ii))-(CH(ii).*SP(ii).*CR(ii)));...
                                (-1.*CP(ii).*SR(ii)) (SP(ii)) (CP(ii).*CR(ii))]; 
                        
                        % Transform beam coordinates
                        % --------------------------
                        if strcmpi(oCoordSys,'Beam')
                            
                            % Determine frequency index for transformation
                            % matrix
                            if size(tMatrix,3)>1
                                idxFreq=find(tMatrixFreq==velObj.frequency(ii));
                                tMult=tMatrix(:,:,idxFreq);
                            else
                                tMult=tMatrix;
                            end
                            
                            % Get velocity data
                            velBeams=squeeze(velObj.rawVel_mps(:,ii,:))';

                            % Apply transformation matrix for 4 beam solutions   
                            if size(velBeams,1) == 4
                                tempT=tMult*velBeams;  
                            else
                                tempT=tMult*velBeams';
                            end
                            
                            % Apply hprMatrix
                            tempTHPR=hprMatrix*tempT(1:3,:);
                            tempTHPR(4,:)=tempT(4,:);
                            
                            % Check for invalid beams
                            invalidIdx=isnan(velBeams);
                            
                            % Identify rows requiring 3 beam solutions
                            nInvalidCol=sum(invalidIdx,1);
                            colIdx=find(nInvalidCol==1);  
                            
                            % Compute 3 beam solution, if necessary
                            if ~isempty(colIdx) 
                                for i3=1:length(colIdx)
                                    tempT=nan(4,1);
                                    tMult3Beam=tMult;
                                    % ID invalid beam
                                    vel3Beam=velBeams(:,colIdx(i3));
                                    idx3Beam=find(isnan(vel3Beam));
                        
                                    % 3 Beam solution for non-RiverRay
                                    vel3BeamZero=vel3Beam;
                                    vel3BeamZero(isnan(vel3Beam))=0;
                                    velError=tMult(4,:)*vel3BeamZero;
                                    vel3Beam(idx3Beam)=-1.*velError./tMult(4,idx3Beam);
                                    tempT=tMult*vel3Beam;

                                    % Apply transformation matrix for 3
                                    % beam solutions
                                    tempTHPR(1:3,colIdx(i3))=hprMatrix*tempT(1:3,:);
                                    tempTHPR(4,colIdx(i3))=nan;                                   
                                end % for i3                     
                            end % if colIdx
                            
                        else
                            % Get velocity data
                            velRaw=squeeze(velObj.rawVel_mps(:,ii,:))';
                            tempTHPR=hprMatrix*velRaw(1:3,:);
                            tempTHPR(4,:)=velRaw(4,:);
                        end % if Beam
                        
                        % Update object
                        tempTHPR=tempTHPR';
                        obj.u_mps(:,ii)=tempTHPR(:,1);
                        obj.v_mps(:,ii)=tempTHPR(:,2);
                        obj.w_mps(:,ii)=tempTHPR(:,3);
                        obj.d_mps(:,ii)=tempTHPR(:,4);
                    end % for nEns
                    
                    % Because Matlab pads arrays with zeros and RR data has
                    % variable number of bins, the raw data may be padded with
                    % zeros. The next 4 statements changes those to nan.
                    obj.u_mps(obj.u_mps==0)=nan;
                    obj.v_mps(obj.v_mps==0)=nan;
                    obj.w_mps(obj.w_mps==0)=nan;
                    obj.d_mps(obj.d_mps==0)=nan; 
                    
                    % Assign processed object properties
                    obj.uProcessed_mps=obj.u_mps;
                    obj.vProcessed_mps=obj.v_mps;
                    
                    % Assign coordinate system and reference properties
                    obj.coordSys=newCoordSys;
                    obj.navRef=obj.origNavRef;
                    
                else
                    
                    % Reset velocity properties to raw values
                    obj.u_mps=obj.rawVel_mps(:,:,1);
                    obj.v_mps=obj.rawVel_mps(:,:,2);
                    obj.w_mps=obj.rawVel_mps(:,:,3);
                    obj.d_mps=obj.rawVel_mps(:,:,4);
                    
                    if strcmp(adcp.manufacturer,'TRDI')
                        % Because Matlab pads arrays with zeros and RR data has
                        % variable number of bins, the raw data may be padded with
                        % zeros. The next 4 statements changes those to nan.
                        obj.u_mps(obj.u_mps==0)=nan;
                        obj.v_mps(obj.v_mps==0)=nan;
                        obj.w_mps(obj.w_mps==0)=nan;
                        obj.d_mps(obj.d_mps==0)=nan;     
                    end
                    
                    % Assign processed properties
                    obj.uProcessed_mps=obj.u_mps;
                    obj.vProcessed_mps=obj.v_mps;
                end % if newsys
                
            else
                
                % Reset velocity properties to raw values
                obj.u_mps=obj.rawVel_mps(:,:,1);
                obj.v_mps=obj.rawVel_mps(:,:,2);
                obj.w_mps=obj.rawVel_mps(:,:,3);
                obj.d_mps=obj.rawVel_mps(:,:,4);
                
                if strcmp(adcp.manufacturer,'TRDI')
                    % Because Matlab pads arrays with zeros and RR data has
                    % variable number of bins, the raw data may be padded with
                    % zeros. The next 4 statements changes those to nan.
                    obj.u_mps(obj.u_mps==0)=nan;
                    obj.v_mps(obj.v_mps==0)=nan;
                    obj.w_mps(obj.w_mps==0)=nan;
                    obj.d_mps(obj.d_mps==0)=nan;   
                end
                
                % Assign processed properties
                obj.uProcessed_mps=obj.u_mps;
                obj.vProcessed_mps=obj.v_mps; 
                
            end % if coordSys
            
            if strcmpi(newCoordSys,'Earth')
                obj.uEarthNoRef_mps=obj.u_mps;
                obj.vEarthNoRef_mps=obj.v_mps;
            end

        end % function changeCoordSys
        
        function obj=setNavReference(obj,boatVel)
        % This function sets the navigation reference. The current reference
        % is first removed from the velocity and then the selected reference
        % is applied.

            % Apply selected navigation reference
            if ~isempty(boatVel.(boatVel.selected))
                obj.u_mps=bsxfun(@plus,obj.uEarthNoRef_mps,boatVel.(boatVel.selected).uProcessed_mps);
                obj.v_mps=bsxfun(@plus,obj.vEarthNoRef_mps,boatVel.(boatVel.selected).vProcessed_mps);
                obj.navRef=boatVel.(boatVel.selected).navRef;
            else
                obj.u_mps=nan(size(obj.uEarthNoRef_mps));
                obj.v_mps=nan(size(obj.vEarthNoRef_mps));
                switch boatVel.selected
                case 'btVel'
                    obj.navRef='BT';
                case 'ggaVel'
                    obj.navRef='GGA';
                case 'vtgVel'
                    obj.navRef='VTG';
                end
            end

            validData2=obj.cellsAboveSL;
            validData2(isnan(obj.u_mps))=false;
            obj.validData(:,:,2)=validData2;
            % Duplicate original to other filters that have yet to be
            % applied 
            obj.validData(:,:,3:9)=repmat(obj.validData(:,:,2),[1,1,7]);
                
            % Combine all filter data and update processed properties
            obj=allValidData(obj);
            
        end % setNavReference   
        
        function obj=changeMagVar(obj,boatVel,magVarChng)
            uNR=obj.uEarthNoRef_mps;
            vNR=obj.vEarthNoRef_mps;
            [dir, mag]=cart2pol(uNR,vNR);
            [uNRrotated,vNRrotated]=pol2cart(dir-deg2rad(magVarChng),mag);
            obj.uEarthNoRef_mps=uNRrotated;
            obj.vEarthNoRef_mps=vNRrotated;
            obj=setNavReference(obj,boatVel);
        end % changeMagVar
        
        function obj=changeOffset(obj,boatVel,offsetChng)
            uNR=obj.uEarthNoRef_mps;
            vNR=obj.vEarthNoRef_mps;
            [dir, mag]=cart2pol(uNR,vNR);
            [uNRrotated,vNRrotated]=pol2cart(dir-deg2rad(offsetChng),mag);
            obj.uEarthNoRef_mps=uNRrotated;
            obj.vEarthNoRef_mps=vNRrotated;
            obj=setNavReference(obj,boatVel);
        end % changeOffset
        
        function obj=changeHeadingSource(obj,boatVel,heading)
            uNR=obj.uEarthNoRef_mps;
            vNR=obj.vEarthNoRef_mps;
            [dir, mag]=cart2pol(uNR,vNR);
            [uNRrotated,vNRrotated]=pol2cart(dir-deg2rad(repmat(heading,size(mag,1),1)),mag);
            obj.uEarthNoRef_mps=uNRrotated;
            obj.vEarthNoRef_mps=vNRrotated;
            obj=setNavReference(obj,boatVel);
        end % changeOffset
        
        function obj=applyInterpolation(obj,transect,varargin)
            obj.uProcessed_mps=nan(size(obj.u_mps));
            obj.vProcessed_mps=nan(size(obj.v_mps));
            obj.uProcessed_mps(obj.validData(:,:,1))=obj.u_mps(obj.validData(:,:,1));
            obj.vProcessed_mps(obj.validData(:,:,1))=obj.v_mps(obj.validData(:,:,1));

            % Determine interpolation methods to apply
            ensInterp=obj.interpolateEns;
            cellsInterp=obj.interpolateCells;
            if ~isempty(varargin)
                nArgs=length(varargin);
                for n=1:2:nArgs
                    if strcmpi(varargin{n},'Ensembles')
                        ensInterp=varargin{n+1};
                    elseif strcmpi(varargin{n},'Cells')
                        cellsInterp=varargin{n+1};
                    end
                end
            end
            
            % Apply specified ensemble interpolation method
            if strcmp(ensInterp,'abba') | strcmp(cellsInterp,'abba')
                obj=interpolate_abba(obj, transect);
            else
                switch ensInterp
                    case 'None' % None
                        % Sets invalid data to nan with no interpolation
                        obj=interpolateEnsNone(obj);

                    case 'ExpandedT' % Expanded Ensemble Time
                        % Set interpolate to None as the interpolation is done in the
                        % clsQComp
                        obj=interpolateEnsNext(obj);

                    case 'Hold9' % SonTek Method
                        % Interpolates using SonTeks method of holding last valid for
                        % up to 9 samples.
                        obj=interpolateEnsHoldLast9(obj);

                    case 'Hold' % Hold Last Valid
                        % Interpolates by holding last valid indefinitely
                        obj=interpolateEnsHoldLast(obj);

                    case 'Linear' % Linear
                        % Interpolates using linear interpolation
                        obj=interpolateEnsLinear(obj,transect);  

                    case 'TRDI'
                        % TRDI is applied in discharge
                        obj=interpolateEnsNone(obj);
                        obj.interpolateEns=ensInterp;
                end

                % Apply specified cell interpolation method
                switch cellsInterp
                    case 'None' % None
                        % Sets invalid data to nan with no interpolation
                        obj=interpolateCellsNone(obj);
                    case 'TRDI' % TRDI
                        % Use TRDI method to interpolate invalid interior cells
                        % clsQComp
                        obj=interpolateCellsTRDI(obj,transect);       

                    case 'Linear' % Linear All
                        % Uses linear interpolation to interpolate velocity for all
                        % invalid bins including those in invalid ensembles.
                        % up to 9 samples.
                        obj=interpolateCellsLinear(obj,transect);
                end % switch
            end
        end % applyInterpolation
               
        function obj=applyFilter(obj,transect,varargin)
            % Determine filters to apply
            if ~isempty(varargin)
                nArgs=length(varargin);
                n=1;
                while n<nArgs
                    switch varargin{n}
                        case 'Beam'
                            n=n+1;
                            beamFilterSetting=varargin{n};
                            obj=filterBeam(obj,beamFilterSetting, transect);
                            
                        case 'Difference'
                            n=n+1;
                            dFilterSetting=varargin{n};
                            if strcmpi(dFilterSetting,'Manual')
                                n=n+1;
                                obj=filterDiffVel(obj,dFilterSetting,varargin{n});
                            else
                                obj=filterDiffVel(obj,dFilterSetting);
                            end
                            
                        case 'Vertical'
                            n=n+1;
                            wFilterSetting=varargin{n};
                            if strcmpi(wFilterSetting,'Manual')
                                n=n+1;
                                setting=varargin{n};
                                if isnan(setting)
                                    setting=obj.wFilterThreshold;
                                end
                                 obj=filterVertVel(obj,wFilterSetting,setting);
                            else
                                obj=filterVertVel(obj,wFilterSetting);
                            end   
                            
                        case 'Other'
                            n=n+1;
                            obj=filterSmooth(obj,transect,varargin{n});
                            
                        case 'Excluded'
                            n=n+1;
                            obj=filterExcluded(obj,transect,varargin{n});
                        case 'SNR'
                            n=n+1;
                            obj=filterSNR(obj,varargin{n});
                        case 'wtDepth'
                            n=n+1;
                            obj=filterWTDepth(obj,transect,varargin{n});
                    end % switch
                    n=n+1;
                end
            else
                obj=filterBeam(obj,obj.beamFilter,transect);
                obj=filterDiffVel(obj,obj.dFilter,obj.dFilterThreshold);
                obj=filterVertVel(obj,obj.wFilter,obj.wFilterThreshold);
                obj=filterSmooth(obj,transect,obj.smoothFilter);
                obj=filterExcluded(obj,transect,obj.excludedDist);
                obj=filterSNR(obj,obj.snrFilter);
            end
            obj=applyInterpolation(obj,transect);
        end % applyfilter
        
        function obj=sosCorrection(obj,transect,ratio)
            obj.u_mps=bsxfun(@times,obj.u_mps,double(ratio));
            obj.v_mps=bsxfun(@times,obj.v_mps,double(ratio));
            obj.uEarthNoRef_mps=bsxfun(@times,obj.uEarthNoRef_mps,double(ratio));
            obj.vEarthNoRef_mps=bsxfun(@times,obj.vEarthNoRef_mps,double(ratio));
            obj=applyFilter(obj,transect);
        end %sosCorrection
        
        function obj=adjustSideLobe(obj,transect)
            depthSelected=transect.depths.selected;
            cellsAboveSLBT=obj.cellsAboveSLbt;
            
            % Compute cutoff for vertical beam depths
            if strcmp(depthSelected,'vbDepths')
                slCutoffVB=((transect.depths.(depthSelected).depthProcessed_m-transect.depths.(depthSelected).draftUse_m).*cosd(transect.adcp.beamAngle_deg))-obj.slLagEffect_m+transect.depths.(depthSelected).draftUse_m;
                cellsAboveSLVB=bsxfun(@lt,round(transect.depths.(depthSelected).depthCellDepth_m,2),round(slCutoffVB,2));
                idx=find(transect.depths.btDepths.validData==false);
                cellsAboveSLBT(:,idx)=cellsAboveSLVB(:,idx);
                cellsAboveSL=cellsAboveSLBT & cellsAboveSLVB;
            else
                cellsAboveSL=cellsAboveSLBT;
            end
             
            % Adjust cutoff to ensure all cells are above depth sounder
            % depths
            if strcmp(depthSelected,'dsDepths')
                slCutoffDS=transect.depths.(depthSelected).depthProcessed_m-obj.slLagEffect_m;
                cellsAboveSLDS=bsxfun(@lt,round(transect.depths.(depthSelected).depthCellDepth_m,2),round(slCutoffDS,2));
                idx=find(transect.depths.btDepths.validData==false);
                cellsAboveSLBT(:,idx)=cellsAboveSLDS(:,idx);
                cellsAboveSL=cellsAboveSLBT & cellsAboveSLDS;
            else
                cellsAboveSL=cellsAboveSLBT;
            end
            
            
            % Compute cutoff from interpolated depths
            % Find ensembles with no valid beam depths
            idx=find(nansum(transect.depths.(depthSelected).validBeams)==0);
            if  ~isempty(idx) 
                % Use interpolated mean depth to estimate cutoff
                if ~isempty(idx)
                    if length(obj.slLagEffect_m)>1
                        slLagEffect_m=obj.slLagEffect_m(idx);
                    else
                        slLagEffect_m=obj.slLagEffect_m;
                    end
                    slCutoffInt=((transect.depths.(depthSelected).depthProcessed_m(idx)-transect.depths.(depthSelected).draftUse_m).*cosd(transect.adcp.beamAngle_deg))-slLagEffect_m+transect.depths.(depthSelected).draftUse_m; 
                    cellsAboveSL(:,idx)= bsxfun(@lt,transect.depths.(depthSelected).depthCellDepth_m(:,idx),slCutoffInt);
                end
            end
            
            % Find ensembles with at least 1 invalid beam depth
            idx=find(nansum(transect.depths.(depthSelected).validBeams)<4);
            if  ~isempty(idx) 
                % Use interpolated mean depth to estimate cutoff
                if ~isempty(idx)
                    if length(obj.slLagEffect_m)>1
                        slLagEffect_m=obj.slLagEffect_m(idx);
                    else
                        slLagEffect_m=obj.slLagEffect_m;
                    end
                    slCutoffInt=((transect.depths.(depthSelected).depthProcessed_m(idx)-transect.depths.(depthSelected).draftUse_m).*cosd(transect.adcp.beamAngle_deg))-slLagEffect_m+transect.depths.(depthSelected).draftUse_m; 
                    cellsAboveSLInt=true(size(cellsAboveSL));
                    cellsAboveSLInt(:,idx)= bsxfun(@lt,transect.depths.(depthSelected).depthCellDepth_m(:,idx),slCutoffInt);
                end
                cellsAboveSL(cellsAboveSLInt==0)=0;
            end         
            obj.cellsAboveSL=cellsAboveSL;
            validVel=~isnan(obj.u_mps);
            obj.validData(:,:,2)=logical(obj.cellsAboveSL.*validVel);
            obj=allValidData(obj);
            obj=computeSNRRng(obj);
            obj=applyFilter(obj,transect);
            obj=applyInterpolation(obj,transect);
        end % adjustSideLobe
        
        function obj=adjustOriginalDataFilter(obj,transect)
            obj=filterExcluded(obj,transect,obj.excludedDist);
            obj.validData(:,:,2)=obj.validData(:,:,2)+(-1.*(obj.validData(:,:,7)-obj.cellsAboveSL));
        end
    end
        
   methods (Access=private)
       
        function obj=allValidData(obj)
        % Combines the results of all filters to determine a final set of
        % valid data
            nCells=nansum(obj.cellsAboveSL);
            nFilters=length(obj.validData(1,1,2:end));
            sumFilters=nansum(obj.validData(:,:,2:end),3)./nFilters;
            valid=true(size(obj.cellsAboveSL));
            valid(sumFilters<1)=false;
            obj.validData(:,:,1)=valid;
        end % allValidData              
               
        function obj=filterBeam(obj,setting,transect)
        % The determination of invalid data depends on the whether 
        % 3-beam or 4-beam solutions are acceptable. This function can be
        % applied by specifying 3 or 4 beam solutions are setting
        % obj.beamFilter to -1 which will trigger an automatic mode. The
        % automatic mode will find all 3 beam solutions and then compare
        % the velocity of the 3 beam solutions to nearest 4 beam solution
        % before and after the 3 beam solution. If the 3 beam solution is
        % within 50% of the average of the neighboring 3 beam solutions the
        % data are deemed valid if not invalid. Thus in automatic mode only
        % those data from 3 beam solutions that appear sufficiently
        % than the 4 beam solutions are marked invalid. The process happens
        % for each ensemble. If the number of beams is specified manually
        % it is applied uniformly for the whole transect.
        
            % Set beamFilter property
            obj.beamFilter=setting;
            % In manual mode determine number of raw invalid and number of
            % 3 beam solutions if selected
            if obj.beamFilter>0
               
                % Find invalid raw data
                validVel=repmat(logical(obj.cellsAboveSL),[1,1,4]);
                validVel(isnan(obj.rawVel_mps))=0;

                % Determine how many beams or transformed coordinates are valid
                validVelSum=sum(validVel,3);
                valid=obj.cellsAboveSL;

                % Compare number of valid beams or velocity coordinates to filter value
                valid((validVelSum<obj.beamFilter & validVelSum>2))=false;

                % Save logical of valid data to object
                obj.validData(:,:,6)=logical(valid);
               
            else
                
                % Apply automatic filter, 
                obj=automatic_beam_filter_abba_interpolation(obj, transect);
            end
               
        end % applyBeamFilter
        
        function obj=automatic_beam_filter_abba_interpolation(obj, transect)
            % Create temporary object
            temp=obj;
            
            % Apply 3 beam filter to temporary object
            temp=filterBeam(temp,4,transect);

            % Create matrix of valid data with nan below sidelobe
            valid=double(temp.validData(:,:,6));
            valid(~temp.cellsAboveSL)=nan;
            
            % Find cells with 3 beam solutions
            idx = find(valid == 0);

            if ~isempty(idx)
                distance_along_shiptrack = transect.boatVel.compute_boat_track(transect).distance_m;
                depth_selected = transect.depths.(transect.depths.selected);
                interpolated_data = abba_2d_interpolation({temp.u_mps, temp.v_mps},...
                                    temp.validData(:,:,6), temp.cellsAboveSL,...
                                    depth_selected.depthCellDepth_m,...
                                    depth_selected.depthCellSize_m,...
                                    depth_selected.depthProcessed_m,...
                                    depth_selected.depthProcessed_m,...
                                    distance_along_shiptrack);
                 
                % Reset nan to 0
                valid(isnan(valid))=0;
                
                % Compute the ratio of estimated value to actual 3 beam solution
                for n=1:size(interpolated_data, 1)
                    row = interpolated_data{n,1}(1);
                    col = interpolated_data{n,1}(2);
                    if isempty(interpolated_data(n,2))
                        uRatio=1;
                    else
                        uRatio=(temp.u_mps(row, col)./interpolated_data{n,2})-1;
                    end
                    if isempty(interpolated_data(n,3))
                        vRatio=1;
                    else
                        vRatio=(temp.v_mps(row, col)./interpolated_data{n,3})-1;
                    end
                    % If 3-beam differs from 4-beam by more the 50%
                    % mark it invalid
                    if abs(uRatio) < 0.5 | abs(vRatio) < 0.5
                        valid(row, col)=1;
                    end % if
                end
                obj.validData(:,:,6)=logical(valid);
            else
                obj.validData(:,:,6)=temp.validData(:,:,6);
            end % isempty
            
            % Combine all filter data and update processed properties
            obj=allValidData(obj);
        end % automatic_beam_filter_abba_interpolation

        function obj=automatic_beam_filter_old(obj, transect)
         % Apply automatic filter, 
            % Create temporary object
            temp=obj;
            % Apply 3 beam filter to temporary object
            temp=filterBeam(temp,4,transect);
            % Determine number of ensembles
            nEns=length(temp.validData(:,:,6));
            % Create matrix of valid data with nan below sidelobe
            valid=double(temp.validData(:,:,6));
            valid(~temp.cellsAboveSL)=nan;
            % Find cells with 3 beam solutions
            [r,c]=find(valid==0);
            if ~isempty(r)
                % Find cells with 4 beam solutions
                [validR,validC]=find(valid==1);
                % Valid water u and v for cells with 4 beam solutions
                validU=temp.u_mps(valid==1);
                validV=temp.v_mps(valid==1);
                % Use griddata to estimate water speed of cells with 3 beam
                % solutions
                F=scatteredInterpolant(validC,validR,validU);
                estU=F(c,r);
                F=scatteredInterpolant(validC,validR,validV);
                estV=F(c,r);
                % Compute the ratio of estimated value to actual 3 beam solution
                idx = sub2ind(size(temp.u_mps), r,c);
                if isempty(estU)
                    uRatio=1;
                else
                    uRatio=(temp.u_mps(idx)./estU)-1;
                end
                if isempty(estV)
                    vRatio=1;
                else
                    vRatio=(temp.v_mps(idx)./estV)-1;
                end

                % If 3-beam differs from 4-beam by more the 50%
                % mark it invalid
                numRatio=size(uRatio,1); 
                valid(isnan(valid))=0;
                for n=1:numRatio
                    if abs(uRatio(n))<0.5 | abs(vRatio(n))<0.5
                        valid(idx(n))=1;
                    end % if
                end % for n
                obj.validData(:,:,6)=logical(valid);
            else
                obj.validData(:,:,6)=temp.validData(:,:,6);
            end % isempty
                
            % Combine all filter data and update processed properties
            obj=allValidData(obj);
        end
        
        function obj=filterDiffVel(obj,setting,varargin)
        % Applies either manual or automatic filtering of the difference
        % (error) velocity. The automatic mode is based on the following:
        %  This filter is based on the assumption that the water error velocity
        %  should follow a gaussian distribution. Therefore, 5 standard deviations
        %  should encompass all of the valid data. The standard deviation and
        %  limits (multiplier*standard deviation) are computed in an iterative 
        %  process until filtering out additional data does not change the computed 
        %  standard deviation. 
            % Set difference filter properties
            obj.dFilter=setting;
            if ~isempty(varargin)
                obj.dFilterThreshold=varargin{1};
            end
            
            % Set multiplier
            multiplier=5;
            % Get difference data from object
            dVel=obj.d_mps;
            
            % Apply selected method
            switch obj.dFilter
                case 'Manual'
                    dVelMaxRef=abs(obj.dFilterThreshold);
                    dVelMinRef=-1.*dVelMaxRef;
                case 'Off'
                    dVelMaxRef=nanmax(nanmax(dVel))+1;
                    dVelMinRef=nanmin(nanmin(dVel))-1;
                case 'Auto'
                    % Initialize variables
                    dVelFiltered=dVel(1:end); 
                    stdDiff=1;
                    i=0;
                    % Loop until no additional data are removed
                    while stdDiff~=0 && i<1000
                        i=i+1;
                        % Compute standard deviation
                        dVelStd=iqr(dVelFiltered);%nanstd(dVelFiltered);
        
                        % Compute maximum and minimum thresholds
                        dVelMaxRef=nanmedian(dVelFiltered)+multiplier*dVelStd;
                        dVelMinRef=nanmedian(dVelFiltered)-multiplier*dVelStd;
                        
                        % Identify valid and invalid data
                        dVelBadIdx=find(dVelFiltered > dVelMaxRef |...
                                            dVelFiltered < dVelMinRef);
                        dVelGoodIdx=find(dVelFiltered <= dVelMaxRef &...
                                            dVelFiltered >= dVelMinRef);
                        % Update filtered data array
                        dVelFiltered=dVelFiltered(dVelGoodIdx);
                        
                        % Determine differences due to last filter
                        % interation
                        dVelStd2=iqr(dVelFiltered);%nanstd(dVelFiltered);    
                        stdDiff=dVelStd2-dVelStd;
                        numBinsFiltered(i)=size(dVelBadIdx,2);
                    end % while
            end % switch
                               
            %  Set valid data row 3 for difference velocity filter results
            dVelBadIdx=find(dVel > dVelMaxRef |...
                    dVel < dVelMinRef);
            valid=obj.cellsAboveSL;

            valid(dVelBadIdx)=false;
            obj.validData(:,:,3)=valid;            
                
            % Set threshold property
            obj.dFilterThreshold=dVelMaxRef;
            
            % Combine all filter data and update processed properties
            obj=allValidData(obj);

        end % applyDiffVelFilter
        
        function obj=filterVertVel(obj,setting,varargin)
        % Applies either manual or automatic filtering of the difference
        % (error) velocity. The automatic mode is based on the following:
        %  This filter is based on the assumption that the water error velocity
        %  should follow a gaussian distribution. Therefore, 4 standard deviations
        %  should encompass all of the valid data. The standard deviation and
        %  limits (multiplier*standard deviation) are computed in an iterative 
        %  process until filtering out additional data does not change the computed 
        %  standard deviation. 
            
            % Set vertical velocity filter properties
            obj.wFilter=setting;
            if ~isempty(varargin)
                obj.wFilterThreshold=varargin{1};
            end
            
            % Set multiplier
            multiplier=5;
            
            % Get difference data from object
            wVel=obj.w_mps;
            
            % Apply selected method
            switch obj.wFilter
                case 'Manual'
                    wVelMaxRef=abs(obj.wFilterThreshold);
                    wVelMinRef=-1.*wVelMaxRef;
                case 'Off'
                    wVelMaxRef=nanmax(nanmax(wVel))+1;
                    wVelMinRef=nanmin(nanmin(wVel))-1;
                case 'Auto'
                    % Initialize variables
                    wVelFiltered=wVel(1:end); 
                    stdDiff=1;
                    i=0;
                    % Loop until no additional data are removed
                    while stdDiff~=0 && i<1000
                        i=i+1;
                        % Compute standard deviation
                        wVelStd=iqr(wVelFiltered);%nanstd(dVelFiltered);
        
                        % Compute maximum and minimum thresholds
                        wVelMaxRef=nanmedian(wVelFiltered)+multiplier*wVelStd;
                        wVelMinRef=nanmedian(wVelFiltered)-multiplier*wVelStd;
                        
                        % Identify valid and invalid data
                        wVelBadIdx=find(wVelFiltered > wVelMaxRef |...
                                            wVelFiltered < wVelMinRef);
                        wVelGoodIdx=find(wVelFiltered <= wVelMaxRef &...
                                            wVelFiltered >= wVelMinRef);
                        % Update filtered data array
                        wVelFiltered=wVelFiltered(wVelGoodIdx);
                        
                        % Determine differences due to last filter
                        % interation
                        wVelStd2=iqr(wVelFiltered);%nanstd(dVelFiltered);    
                        stdDiff=wVelStd2-wVelStd;
                        numBinsFiltered(i)=size(wVelBadIdx,2);
                    end % while
            end % switch
                               
            %  Set valid data row 3 for difference velocity filter results
            wVelBadIdx=find(wVel > wVelMaxRef |...
                    wVel < wVelMinRef);
            valid=obj.cellsAboveSL;

            valid(wVelBadIdx)=false;
            obj.validData(:,:,4)=valid;            
            
            % Set threshold property
            obj.wFilterThreshold=wVelMaxRef;
            
            % Combine all filter data and update processed properties
            obj=allValidData(obj);
        end % applyVertVelFilter
        
        function obj=filterSmooth(obj,transect,setting)
        %filterBoatSpeed  Running Standard Deviation Filter for Water Speed
        %  This filter employs a running trimmed standard deviation filter to
        %  identify and mark spikes in the water speed. First a robust Loess 
        %  smooth is fitted to the water speed time series and residuals between
        %  the raw data and the smoothed line are computed. The trimmed standard
        %  deviation is computed by selecting the number of residuals specified by
        %  "halfwidth" before the target point and after the target point, but not
        %  including the target point. These values are then sorted, and the points
        %  with the highest and lowest values are removed from the subset, and the 
        %  standard deviation of the trimmed subset is computed. The filter
        %  criteria are determined by multiplying the standard deviation by a user
        %  specified multiplier. This criteria defines a maximum and minimum
        %  acceptable residual. Data falling outside the criteria are set to nan.
        %  
        %  Recommended filter setting are:
        %   filterWidth=10;
        %   halfWidth=10;
        %   multiplier=9;
        %
        %   David S. Mueller, USGS, OSW
        %   9/8/2005
        
            % Set property
            obj.smoothFilter=setting;
            
            % Compute ensTime
            ensTime=nancumsum(transect.dateTime.ensDuration_sec);
            
            % Determine if smooth filter should be applied
            if strcmpi(obj.smoothFilter,'Auto')
                % Boat velocity components
                wVele=obj.u_mps;
                wVeln=obj.v_mps;

                % Set filter parameters
                filterWidth=10;
                halfWidth=10;
                multiplier=9;
                cycles=3;

                % Inialize variables
                dir=nan(size(wVele));
                speed=nan(size(wVele));
                speedSmooth=nan(size(wVele));
                speedRes=nan(size(wVele));
                speedFiltered=nan(size(wVele));
                wVeleFiltered=wVele;
                wVelnFiltered=wVeln;
                wtBad=nan(size(wVele));

                % Compute mean speed and direction of water
                wVeleAvg=nanmean(wVele);
                wVelnAvg=nanmean(wVeln);
                [dir, speed]=cart2pol(wVeleAvg,wVelnAvg);
                dir=rad2azdeg(dir);
                % Compute residuals from a robust Loess smooth
                speedSmooth=smooth(ensTime,speed,filterWidth,'rloess')';
                speedRes=speed-speedSmooth;

                % Apply a trimed standard deviation filter multiple times
                speedFiltered=speed;
                for i=1:cycles
                    [filArray]=clsBoatData.runStdTrim(halfWidth,speedRes');

                    % Compute filter bounds
                    upperLimit=speedSmooth+multiplier.*filArray;
                    lowerLimit=speedSmooth-multiplier.*filArray;

                    % Apply filter to residuals
                    wtBad_idx=find(speed>upperLimit | speed<lowerLimit);
                    speedRes(wtBad_idx)=nan;
                end
                % Update validData property
                valid=obj.cellsAboveSL;

                valid(:,wtBad_idx)=false;
                obj.validData(:,:,5)=valid;   
                obj.smoothUpperLimit=upperLimit;
                obj.smoothLowerLimit=lowerLimit;
                obj.smoothSpeed=speedSmooth;
            else
            % No filter applied
                obj.validData(:,:,5)=obj.cellsAboveSL;
                obj.smoothUpperLimit=nan;
                obj.smoothLowerLimit=nan;
                obj.smoothSpeed=nan;
            end % if smoothFilter
            
            % Combine all filter data and update processed properties
            obj=allValidData(obj); 
        end % applySmoothFilter   
        
        function obj=filterExcluded(obj,transect,setting)
        % Marks all data with the cell top greater than the setting invalid 
            cellDepth=transect.depths.(transect.depths.selected).depthCellDepth_m;
            cellSize=transect.depths.(transect.depths.selected).depthCellSize_m;
            draft=transect.depths.(transect.depths.selected).draftUse_m;
            topCellDepth=cellDepth-0.5.*cellSize;
            threshold=round((setting+draft),3);
            exclude=round(topCellDepth,3)<=threshold;
            valid=obj.cellsAboveSL;
            valid(exclude)=false;
            obj.validData(:,:,7)= valid;
            
            % Set threshold property
            obj.excludedDist=setting;
            
            % Combine all filter data and update processed properties
            obj=allValidData(obj);
        end
        
        function obj=filterSNR(obj,setting)
            % Set property
            obj.snrFilter=setting;
            
            if strcmpi(setting,'Auto')
                if ~isempty(obj.snrRng)
                    badSNRIdx=obj.snrRng>12;
                    valid=obj.cellsAboveSL;

                    badSNRarray=repmat(badSNRIdx,size(valid,1),1);
                    valid(badSNRarray)=false;
                    obj.validData(:,:,8)=valid;
                    % Combine all filter data and update processed properties
                    obj=allValidData(obj);
                end % if
            else
                obj.validData(:,:,8)=obj.cellsAboveSL;
            end % if
            
            % Combine all filter data and update processed properties
            obj=allValidData(obj);
        end % filterSNR
        
        function obj=filterWTDepth(obj,transect,setting)
            obj.wtDepthFilter=setting;
            valid=obj.cellsAboveSL;

            if strcmpi(setting,'On')
                valid(:,isnan(transect.depths.(transect.depths.selected).depthProcessed_m))=false;
            end
            obj.validData(:,:,9)=valid;
            
            % Combine all filter data and update processed properties
            obj=allValidData(obj);
        end % filterWTDepth              
        
        function obj=computeSNRRng(obj)
            if strcmpi(obj.rssiUnits,'SNR')
                % Process snr data so only data above sidelobe cutoff are used for
                % analysis
                snr=obj.rssi;
                cellsAboveSL=double(obj.cellsAboveSL);
                cellsAboveSL(cellsAboveSL<0.5)=nan;
                snrAdj=bsxfun(@times,snr,cellsAboveSL);

                % Compute average snr for each beam in each ensemble
                snrAvg=squeeze(nanmean(snrAdj,1))';

                % Compute the spread in snr averages for each ensemble
                obj.snrRng=nanmax(snrAvg)-nanmin(snrAvg);
            end % if
        end % computeSNRRng
        
        function obj=interpolate_abba(obj, transect)
            % Set property
            obj.interpolateEns='abba';
            obj.interpolateCells = 'abba';
            
            % Get valid data based on all filters applied
            valid=obj.validData(:,:,1);
            
            % Intialize Processed velocity data variables
            obj.uProcessed_mps=obj.u_mps;
            obj.vProcessed_mps=obj.v_mps;
            
            % Set invalid data to nan in processed velocity data variables
            obj.uProcessed_mps(~valid)=nan;
            obj.vProcessed_mps(~valid)=nan; 
            
            % Find cells with invalid data
            idx = find(valid == 1);

            if ~isempty(idx)
                distance_along_shiptrack = transect.boatVel.compute_boat_track(transect).distance_m;
                depth_selected = transect.depths.(transect.depths.selected);
                if ~isnan(distance_along_shiptrack)
                    interpolated_data = abba_2d_interpolation({obj.uProcessed_mps, obj.vProcessed_mps},...
                                        valid, obj.cellsAboveSL,...
                                        depth_selected.depthCellDepth_m,...
                                        depth_selected.depthCellSize_m,...
                                        depth_selected.depthProcessed_m, ...
                                        depth_selected.depthProcessed_m,...
                                        distance_along_shiptrack);
%                 interpolated_data = abba_2d_interpolation({obj.uProcessed_mps, obj.vProcessed_mps},...
%                                     valid, obj.cellsAboveSL,...
%                                     depth_selected.depthCellDepth_m,...
%                                     depth_selected.depthCellSize_m,...
%                                     nan,...
%                                     distance_along_shiptrack);                                
                
                    % Compute the ratio of estimated value to actual 3 beam solution
                    for n=1:size(interpolated_data, 1)
                        row = interpolated_data{n,1}(1);
                        col = interpolated_data{n,1}(2);
                        obj.uProcessed_mps(row, col) = interpolated_data{n, 2};
                        obj.vProcessed_mps(row, col) = interpolated_data{n, 3};
                    end
                    obj.uProcessed_mps(~obj.validData(:,:,7))=nan;
                    obj.vProcessed_mps(~obj.validData(:,:,7))=nan;
                else
                    obj.uProcessed_mps(:,:) = nan;
                    obj.vProcessed_mps(:,:) = nan;
                    obj.validData(:,:,1) = 0;
                end
            end
        end % interpolate abba
        
        function obj=interpolateEnsNext(obj)
            % Set interpolation property for ensembles
            obj.interpolateEns='ExpandedT';
            
            % Set processed data to nan for all invalid data
            valid=obj.validData(:,:,1);
            obj.uProcessed_mps=obj.u_mps;
            obj.vProcessed_mps=obj.v_mps;
            obj.uProcessed_mps(~valid(:,:,1))=nan;
            obj.vProcessed_mps(~valid(:,:,1))=nan; 
            
            % Identify ensembles with no valid data
            validEns=any(valid);
            nEns=length(validEns);
            
            % Set the invalid ensembles to the data in the next valid
            % ensemble.
            for n=nEns-1:-1:1
                if validEns(n)==false
                    obj.uProcessed_mps(:,n)=obj.uProcessed_mps(:,n+1);
                    obj.vProcessed_mps(:,n)=obj.vProcessed_mps(:,n+1);
                end % if validEns
            end % for n
        end % interpolateNext
        
        function obj=interpolateEnsHoldLast(obj)
        % Interpolates velocity data for invalid ensembles by repeating the
        % last valid data until new valid data is found
        
            % Set property
            obj.interpolateEns='HoldLast';
            
            % Get valid data based on all filters applied
            valid=obj.validData(:,:,1);
            
            % Intialize Processed velocity data variables
            obj.uProcessed_mps=obj.u_mps;
            obj.vProcessed_mps=obj.v_mps;
            
            % Set invalid data to nan in processed velocity data variables
            obj.uProcessed_mps(~valid(:,:,1))=nan;
            obj.vProcessed_mps(~valid(:,:,1))=nan;       
            
            % Determine ensembles with valid data
            validEns=any(valid);
            
            % Process each ensemble beginning with the second ensemble
            nEns=length(validEns);
            for n=2:nEns
                % If ensemble is invalid fill in with previous ensemble
                if validEns(n)==false
                    obj.uProcessed_mps(:,n)=obj.uProcessed_mps(:,n-1);
                    obj.vProcessed_mps(:,n)=obj.vProcessed_mps(:,n-1);
                end % if validEns
            end % for n
        end % interpolateHoldLast
        
        function obj=interpolateEnsHoldLast9(obj)
        % Interpolates velocity data for invalid ensembles by repeating the
        % last valid data for up to 9 ensembles or until new valid data is
        % found. If more the 9 consectutive ensembles are invalid the
        % ensembles beyond the 9th remain invalid. This is for
        % compatibility with SonTek RiverSurveyor Live.
            
            % Set property
            obj.interpolateEns='Hold9';
            
            % Get valid data based on all filters applied            
            valid=obj.validData(:,:,1);
            
            % Intialize Processed velocity data variables
            obj.uProcessed_mps=obj.u_mps;
            obj.vProcessed_mps=obj.v_mps;
            
            % Set invalid data to nan in processed velocity data variables
            obj.uProcessed_mps(~valid(:,:,1))=nan;
            obj.vProcessed_mps(~valid(:,:,1))=nan; 
            
            % Determine ensembles with valid data
            validEns=any(valid);
            
            % Process each ensemble beginning with the second ensemble
            nEns=length(validEns);
            nInvalid=0;
            for n=2:nEns
                % If ensemble is invalid and there are 9 or less consectutive
                % invalid ensembles fill in with previous ensemble.
                if validEns(n)==false && nInvalid<10
                    nInvalid=nInvalid+1;
                    obj.uProcessed_mps(:,n)=obj.uProcessed_mps(:,n-1);
                    obj.vProcessed_mps(:,n)=obj.vProcessed_mps(:,n-1);
                else
                    % Reset counter for a valid ensemble
                    nInvalid=0;
                end % if validEns
            end % for n
        end % interpolateHoldLast9
        
        function obj=interpolateEnsNone(obj)
        % Applies no interpolation for invalid ensembles
            
            % Set property
            obj.interpolateEns='None';
            % Intialize Processed velocity data variables
            obj.uProcessed_mps=obj.u_mps;
            obj.vProcessed_mps=obj.v_mps;
            % Set invalid data to nan in processed velocity data variables
            obj.uProcessed_mps(~obj.validData(:,:,1))=nan;
            obj.vProcessed_mps(~obj.validData(:,:,1))=nan;    
        end % interpolateNone
        
        function obj=interpolateEnsLinear(obj,transect)
        % Use linear interpolation as computed by Matlab's
        % scatteredInterpolant function to interpolate velocity data for
        % ensembles with no valid velocities.
            
            % Set property
            obj.interpolateEns='Linear';
            
            % Get valid data based on all filters applied
            valid=obj.validData(:,:,1);
            
            % Intialize Processed velocity data variables
            obj.uProcessed_mps=obj.u_mps;
            obj.vProcessed_mps=obj.v_mps;
            
            % Set invalid data to nan in processed velocity data variables
            obj.uProcessed_mps(~valid(:,:,1))=nan;
            obj.vProcessed_mps(~valid(:,:,1))=nan; 
            
            % Determine ensembles with valid data
            validEns=any(valid);
            
            if sum(validEns)>1
                % Determine number of ensembles
                nEns=length(validEns);

                % Compute z
                z=double(bsxfun(@rdivide,bsxfun(@minus,transect.depths.(transect.depths.selected).depthProcessed_m,...
                    transect.depths.(transect.depths.selected).depthCellDepth_m),...
                    transect.depths.(transect.depths.selected).depthProcessed_m));

                % Create position array
                if ~isempty(transect.boatVel.(transect.boatVel.selected))
                    if nansum(transect.boatVel.(transect.boatVel.selected).validData(1,:))>0
                        boatVelX=transect.boatVel.(transect.boatVel.selected).uProcessed_mps;
                        boatVelY=transect.boatVel.(transect.boatVel.selected).vProcessed_mps;
                        trackX=boatVelX.*transect.dateTime.ensDuration_sec;
                        trackY=boatVelY.*transect.dateTime.ensDuration_sec;
                        track=nancumsum(sqrt(trackX.^2+trackY.^2));
                        trackArray=repmat(track,size(obj.uProcessed_mps,1),1);

                        % Determine index of all valid data
                        validZ=~isnan(z);
                        validCombined=valid & validZ;
                                               
                        % Compute interpolation function from all valid data
                        Fu=scatteredInterpolant(double(z(validCombined)),double(trackArray(validCombined)),...
                            double(obj.uProcessed_mps(validCombined)),'linear','linear');
                        Fv=scatteredInterpolant(double(z(validCombined)),double(trackArray(validCombined)),...
                            double(obj.vProcessed_mps(validCombined)),'linear','linear');

                        % Process each ensemble
                        u=obj.uProcessed_mps;
                        v=obj.vProcessed_mps;
                        if nansum(nansum(validCombined))>0
                            % Restrict interpolation to between the first
                            % and last valid ensemble.
                            start=find(validEns==true,1,'first');
                            last=find(validEns==true,1,'last');
                            for n=start:last
                                % If the ensemble is invalid, interplated data for that
                                % ensemble
                                if ~validEns(n) 
                                    tempu=Fu(z(:,n),trackArray(:,n));
                                    if ~isempty(tempu)
                                        u(:,n)=tempu;
                                        v(:,n)=Fv(z(:,n),trackArray(:,n));
                                    else
                                        n
                                    end
                                end % if validEns
                            end % for n 

                            % Retain only data above sidelobe cutoff
                            obj.uProcessed_mps=nan(size(u));
                            obj.vProcessed_mps=nan(size(v));
                            processedValidCells=estimateProcessedValidCells(obj,transect);
                            obj.uProcessed_mps(processedValidCells)=u(processedValidCells);
                            obj.vProcessed_mps(processedValidCells)=v(processedValidCells);
                        end
                    end
                end
            end
        end % interpolateEnsLinear
               
        function obj=interpolateCellsLinear(obj,transect)
        % Use linear interpolation as computed by Matlab's
        % scatteredInterpolant function to interpolate velocity data for
        % depth cells and ensembles with no valid velocities.  
            
            % Set property
            obj.interpolateCells='Linear';
            
            % Get valid data based on all filters applied
            valid=obj.validData(:,:,1);
            
            % Intialize Processed velocity data variables
            obj.uProcessed_mps=obj.u_mps;
            obj.vProcessed_mps=obj.v_mps;
            
            % Set invalid data to nan in processed velocity data variables
            obj.uProcessed_mps(~valid(:,:,1))=nan;
            obj.vProcessed_mps(~valid(:,:,1))=nan;    
            
            % Compute z
            z=bsxfun(@rdivide,bsxfun(@minus,transect.depths.(transect.depths.selected).depthProcessed_m,...
                transect.depths.(transect.depths.selected).depthCellDepth_m),...
                transect.depths.(transect.depths.selected).depthProcessed_m);
            
            % Create position array
            boatVelX=transect.boatVel.(transect.boatVel.selected).uProcessed_mps;
            boatVelY=transect.boatVel.(transect.boatVel.selected).vProcessed_mps;
            trackX=boatVelX.*transect.dateTime.ensDuration_sec;
            trackY=boatVelY.*transect.dateTime.ensDuration_sec;
            track=nancumsum(sqrt(trackX.^2+trackY.^2));
            trackArray=repmat(track,size(obj.uProcessed_mps,1),1);
            
            % Determine index of all valid data
            validZ=~isnan(z);
            validCombined=valid & validZ;
            
            % Compute interpolation function from all valid data
            Fu=scatteredInterpolant(z(validCombined),trackArray(validCombined),obj.uProcessed_mps(validCombined),'natural','none');
            Fv=scatteredInterpolant(z(validCombined),trackArray(validCombined),obj.vProcessed_mps(validCombined),'natural','none');            
            
            % Compute data for all locations from interpolation function
            uInt=Fu(z(:),trackArray(:));
            vInt=Fv(z(:),trackArray(:));
            
            % Reshape the vector into an array
            u=reshape(uInt,size(z));
            v=reshape(vInt,size(z));
            
            % Retain only data above sidelobe cutoff
            obj.uProcessed_mps=nan(size(u));
            obj.vProcessed_mps=nan(size(v));
            processedValidCells=estimateProcessedValidCells(obj);
            obj.uProcessed_mps(processedValidCells)=u(processedValidCells);
            obj.vProcessed_mps(processedValidCells)=v(processedValidCells);
        end % interpolateCellsLinear
             
        function obj=interpolateCellsTRDI(obj,transect)
        % This function computes the velocity for the invalid cells using
        % the methods in WinRiver II, but applied to velocity components.
        % Although WinRiver II applies to discharge which theoretically is 
        % more correct, mathematically applying to discharge or velocity
        % components is identical. By applying to velocity components the 
        % user can see the velocity data interpolated.
        % Power fit uses the power fit equation and no slip uses linear interpolation.
            
            % Set property
            obj.interpolateCells='TRDI';
            depths=transect.depths.(transect.depths.selected);
            valid=obj.validData(:,:,1);
            cellDepth=depths.depthCellDepth_m;
            zAll=repmat(depths.depthProcessed_m,size(cellDepth,1),1)-cellDepth;
            zAll(zAll<0)=nan;
            z=zAll;
            z(isnan(obj.uProcessed_mps))=nan;
            zadj=nan(size(z));
            nCells=size(obj.uProcessed_mps,1);
            nEns=size(obj.uProcessed_mps,2);
            cellSize=depths.depthCellSize_m;
            exponent=transect.extrap.exponent;
            botMethod=transect.extrap.botMethod;
                    
            % Loop to identify first and last valid cell in each ensemble,
            for n=1:nEns
                idxFirst=find(valid(:,n)==true,1,'first');
                idxLast=find(valid(:,n)==true,1,'last');
                idxMiddle=find(valid(idxFirst:idxLast,n)==false);
                idxMiddle=idxMiddle+idxFirst-1;

                if ~isempty(idxMiddle)
                    zadj(idxMiddle,n)=zAll(idxMiddle,n);
            
                    % Determine appropriate interpolation method
                    switch botMethod

                        case 'Power'
                            % Compute interpolated u-velocities
                            z2=(z(:,n)-0.5.*cellSize(:,n));
                            z2(z2<0)=nan;
                            coef=((exponent+1).*nansum(obj.uProcessed_mps(:,n).*cellSize(:,n)))./...
                                 nansum(((z(:,n)+0.5.*cellSize(:,n)).^(exponent+1))-(z2.^(exponent+1)));
                            temp=coef.*zadj(:,n).^exponent;
                            
                            obj.uProcessed_mps(idxMiddle,n)=temp(idxMiddle);
                            % Compute interpolated v-velocities
                            coef=((exponent+1).*nansum(obj.vProcessed_mps(:,n).*cellSize(:,n)))./...
                                 nansum(((z(:,n)+0.5.*cellSize(:,n)).^(exponent+1))-(z2.^(exponent+1)));
                            temp=coef.*zadj(:,n).^exponent;
                            obj.vProcessed_mps(idxMiddle,n)=temp(idxMiddle);

                        case 'No Slip'
                            obj.uProcessed_mps(idxMiddle,n)=interp1(cellDepth(valid(:,n),n),obj.uProcessed_mps(valid(:,n),n),cellDepth(idxMiddle,n),'linear');
                            obj.vProcessed_mps(idxMiddle,n)=interp1(cellDepth(valid(:,n),n),obj.vProcessed_mps(valid(:,n),n),cellDepth(idxMiddle,n),'linear');
                    end % switch botMethod
                end % if idxMiddle
            end % for n    
        end % interpolateCellsTRDI

        function processedValidCells=estimateProcessedValidCells(obj,transect)
         % Esimate number of cells for invalid ensembles
                processedValidCells=obj.validData(:,:,1);
                validData2Sum=nansum(obj.validData(:,:,1));
                invalidEnsIdx=find(validData2Sum==0);
                nInvalid=length(invalidEnsIdx);
                for n=1:nInvalid
                    idx1=find(validData2Sum(1:invalidEnsIdx(n))>0,1,'last');
                    idx2=find(validData2Sum(invalidEnsIdx(n):end)>0,1,'first');
                    idx2=invalidEnsIdx(n)+idx2-1;              
                    idx12=find(processedValidCells(:,idx1)==1,1,'last');
                    idx22=find(processedValidCells(:,idx2)==1,1,'last');
                    depthCellDepth=transect.depths.btDepths.depthCellDepth_m;
                    cutoff=nanmax([depthCellDepth(idx12,idx1),depthCellDepth(idx22,idx2)]);
                    processedValidCells(depthCellDepth(:,invalidEnsIdx(n))<cutoff,invalidEnsIdx(n))=1;  
                end % for n
                % Apply excluded distance
                processedValidCells=logical(processedValidCells.*obj.validData(:,:,7));
        end % cells4InvalidEns
        
        function obj=interpolateCellsNone(obj)
            % Set property
            obj.interpolateCells='None';
            
            % Get valid data based on all filters applied
            valid=obj.validData(:,:,1);
            
            % Intialize Processed velocity data variables
            obj.uProcessed_mps=obj.u_mps;
            obj.vProcessed_mps=obj.v_mps;
            
            % Set invalid data to nan in processed velocity data variables
            obj.uProcessed_mps(~valid(:,:,1))=nan;
            obj.vProcessed_mps(~valid(:,:,1))=nan;    
        end % interpolateCellsNone

   end
   methods (Static)
       
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
            % Create object
            obj.wVel=clsWaterData();
            % Get variable names from structure
            names=fieldnames(structIn);
            % Set properties to structure values
            nMax=length(names);
            for n=1:nMax
                obj.wVel=setProperty(obj.wVel,names{n},structIn.(names{n}));
            end % for n
        end % reCreate
    end % static methods

end

