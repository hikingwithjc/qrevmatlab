classdef clsComputeExtrap
    % Class to compute the optimized or manually specified extrapolation
    % methods.
    
    properties (SetAccess=private)
        threshold % Threshold as a percent for determining if a median is valid
        subsection % Percent of discharge. Does not account for transect direction.
        fitMethod % Method used to determine fit. Automatic or Manual
        normData % Object of class clsNormData
        selFit % Object of class clsSelectFit
        qSensitivity % Object of class clsExtrapQSensitivity
        messages % Variable for messages to user      
    end % properties
    
    methods
        function obj=clsComputeExtrap(transData,varargin)
        % Constructor function for extrap. Defaults to automatic fit.
            if nargin>0     
                obj.threshold=20;
                obj.subsection=[0 100];
                 obj.fitMethod='Automatic';
                obj=processProfiles(obj,transData,'q');
                % Compute the sensitivity of the final discharge to changes in
                % extrapolation methods
                if isempty(varargin)
                    obj.qSensitivity=clsExtrapQSensitivity(transData,obj.selFit);
                end
            end % if nargin
        end % constructor   
        
        function obj=processProfiles(obj,transData,dataType)  
        % Function that serves and the main control for other classes and
        % functions
        
            % Determine number of transects
            nTransects=length(transData);
            
            % Compute normalized data
            obj.normData=clsNormData(transData,dataType,obj.threshold,obj.subsection);
            
            % Compute the fit for the selected method
            if strcmp(obj.fitMethod,'Manual')
                obj.selFit=clsSelectFit(obj.normData,obj.fitMethod,transData);
            else
                obj.selFit=clsSelectFit(obj.normData,obj.fitMethod);
            end
            if ~isempty(obj.selFit(end).topfitr2)
                % Evaluate if there is a potential that a 3-point top method
                % may be appropriate
                if (obj.selFit(end).topfitr2>0.9 || obj.selFit(end).topr2>0.9) && abs(obj.selFit(end).topmaxdiff)>0.2
                    obj.messages{1}='The measurement profile may warrant a 3-point fit at the top';
                end % if
            end
        end % processProfiles
        
        function obj=updateQSensitivity(obj,transData)
            obj.qSensitivity=clsExtrapQSensitivity(transData,obj.selFit);
        end % updateQSensitivity
        
        function obj=changeFitMethod(obj,transData,newFitMethod,n,varargin)
        % Function to change the extrapolation methods and update the
        % discharge sensitivity computations
            obj.fitMethod=newFitMethod;
            obj.selFit(n)=clsSelectFit(obj.normData(n),newFitMethod,varargin{:});
            obj.qSensitivity=clsExtrapQSensitivity(transData,obj.selFit);
        end % changeFitMethod
        
        function obj=changeThreshold(obj,transData,datatype,threshold)
        % Function to change the threshold for accepting the increment
        % median as valid. The threshold is in percent of the median number
        % of points in all increments.
            obj.threshold=threshold;
            obj=processProfiles(obj,transData,datatype);
            obj.qSensitivity=clsExtrapQSensitivity(transData,obj.selFit);
        end % changeThreshold
        
        function obj=changeExtents(obj,transData,datatype,extents)
        % Function allows the data to be subsection by specifying the
        % percent cumulative discharge for the start and end points.
        % Currenlty this function does not consider transect direction.
%             autoTop=obj.selFit(end).topMethodAuto;
%             autoBot=obj.selFit(end).botMethodAuto;
%             autoExp=obj.selFit(end).exponentAuto;
                obj.subsection=extents;
                obj=processProfiles(obj,transData,datatype);
%         obj.selFit(end)=resetProperty(obj.selFit(end),'topMethodAuto',autoTop);
%         obj.selFit(end)=resetProperty(obj.selFit(end),'botMethodAuto',autoBot);
%         obj.selFit(end)=resetProperty(obj.selFit(end),'exponentAuto',autoExp); 
%         obj.selFit(end)=resetProperty(obj.selFit(end),'fitMethod','Manual');
%         obj.fitMethod='Manual';
                obj.qSensitivity=clsExtrapQSensitivity(transData,obj.selFit);
                
        end % changeExtents
        
        function obj=changeDataType(obj,transData,datatype)
            obj=processProfiles(obj,transData,datatype);
            obj.qSensitivity=clsExtrapQSensitivity(transData,obj.selFit);
        end % changeDataType
        
        function obj=setProperty(obj,transData,prop,setting)
        % Function to allow changing any property and reprocessing the
        % extrapolation
            obj.(prop)=setting;
            idx=find(cell2mat({transData.checked})==1);
            obj=processProfiles(obj,transData(idx),obj.normData.dataType);
        end % setProperty 
        
        function obj=resetProperty(obj,property,setting)
            obj.(property)=setting;
        end
    end % methods
    
    methods (Static)
        
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
                % Create object
                temp=clsComputeExtrap();
                obj=setProperty(obj,'extrapFit',temp);
                % Get variable names from structure
                names=fieldnames(structIn);
                % Set properties to structure values
                nMax=length(names);
                for n=1:nMax
                    if isstruct(structIn.(names{n}))
                        switch names{n}
                            case 'normData'
                                temp=clsNormData.reCreate(structIn.normData);
                                obj.extrapFit=resetProperty(obj.extrapFit,'normData',temp);
                            case 'selFit'
                                temp=clsSelectFit.reCreate(structIn.selFit);
                                obj.extrapFit=resetProperty(obj.extrapFit,'selFit',temp);
                            case 'qSensitivity'
                                temp=clsExtrapQSensitivity.reCreate(structIn);
                                obj.extrapFit=resetProperty(obj.extrapFit,'qSensitivity',temp);                                
                        end
                    else
                        % Assign property
                        obj.extrapFit=resetProperty(obj.extrapFit, names{n},structIn.(names{n}));
                    end
                end % for n
        end % reCreate        
    end % static methods
end % class

