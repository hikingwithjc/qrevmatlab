classdef clsMovingBedTests
    % Class to process and store moving-bed test data and results 
    
    properties (SetAccess=private)
        type % Loop or Stationary
        transect % object of clsTransectData
        
        duration_sec % duration of test in secs
        percentInvalidBT % percent of invalid bottom track
        compassDiff_deg % Difference in heading for out and back of loop
        flowDir_deg % Mean flow direction from loop test
        mbDir_deg % Moving-bed or closure error direction
        distUS_m % Distance moved upstream in m
        flowSpd_mps % Magnitude of water velocity in mps
        mbSpd_mps % Magnitude of moving-bed velocity in mps
        percentMB % Potential error due to moving bed in percent
        movingBed % Moving-bed determined 'Yes' 'No'
        userValid % Logical to allow user to determine if test should be considered a valid test
        testQuality % Quality of test 'Valid' 'Warnings' 'Errors'
        use2Correct % Use this test to correct discharge
        selected % Selected as valid moving-bed test to use for correction or determining moving-bed condition
        messages % Cell array of warning and error messages based on data processing
        
        nearBedSpeed_mps % Mean near-bed water speed for test in mps
        
        stationaryUSTrack % Upstream component of the bottom track referenced ship track
        stationaryCSTrack % Cross-stream component of the bottom track referenced ship track
        stationaryMBVel % Moving-bed velocity by ensemble
    end % properties
    
    methods
        function obj=clsMovingBedTests(source,varargin)
        % Constructor function
            if nargin > 0  
                
                % Determine source of test
                if strcmp(source,'TRDI')
                    obj=mbTRDI(obj,varargin{:});
                else
                    obj=mbSonTek(obj,varargin{:});
                end

                % Preallocate objects to improve speed
                numTests=length(obj);
                
                for n=1:numTests
                    
                    % Convert data to earth coordinates and set the
                    % navigation reference to BT for both boat and water
                    % data.
                    obj(n).transect=changeCoordSys(obj(n).transect,'Earth');
                    obj(n).transect=changeNavReference(obj(n).transect,1,'BT');

                    % Adjust data for default manufacturer specific
                    % handling of invalid data
                    deltaT=clsTransectData.adjustedEnsembleDuration(obj(n).transect,'mbt');
                    
                    % Process the moving-bed test using the appropriate
                    % methods
                    if strcmp(obj(n).type,'Loop')
                        if strcmp(source,'TRDI')
                            obj(n)=loopTest(obj(n),deltaT);
                        else
                            obj(n)=loopTest(obj(n));
                        end
                    elseif strcmp(obj(n).type,'Stationary')
                        obj(n)=stationaryTest(obj(n));
                    else
                        errordlg('Invalid moving-bed test identifier specified','clsMovingBedTests');
                    end % if
                end % for n
                
                obj=autoUse2Correct(obj);
                
            end % if naragin
        end % constructor
        
        function obj=mbTRDI(obj,mmt)
        % Function to create object properties for TRDI moving-bed tests.
            % Create transect object(s)
            transects=clsTransectData('TRDI',mmt,'MB');
            if ~isempty(transects(1).adcp)
                nTransects=length(transects);
                skipn=0;
                for n=1:nTransects
                    fileMatchLogical = strfind(mmt.mbtTransects.Files, transects(n).fileName);
                    idx = find(not(cellfun('isempty', fileMatchLogical)),1,'first');
                    % Store transects object in property
                    obj(n).transect=transects(n);
                    % Store information on whether to used this test for
                    % correction.
                      obj(n).userValid=true;
                    % Identify type of test
                    obj(n).type=char(mmt.mbtTransects.Type(idx));

                end % for
            end % if isempty
        end % mbTRDI
        
        function obj=mbSonTek(obj,varargin)
        % Function to create object properties for SonTek moving-bed tests.
            % Create transects object for test data
            typeCell=varargin(1);
            fileCell=varargin(2);
            transects=clsTransectData('SonTek',fileCell{:});
            nFiles=length(fileCell{:});
            typeCell=typeCell{:};
            % Create moving-bed object(s)
            obj(nFiles)=clsMovingBedTests();
            % Assign transect and test type properties
            for n=1:nFiles
                obj(n).type=typeCell{n};
                obj(n).transect=transects(n);
                obj(n).use2Correct=false;
                obj(n).userValid=true;
            end % for
        end % mbSonTek
        
        function obj=loopTest(obj,varargin)
        % Process loop moving-bed test
            %% Assign data from transect to local variables
            obj.transect=boatInterpolations(obj.transect,0,'BT','Linear');
            obj.transect=boatInterpolations(obj.transect,0,'GPS','Linear');
            transData=obj.transect;
            inTransectIdx=transData.inTransectIdx;
%             inTransectIdx=1:1:length(transData.boatVel.btVel.uProcessed_mps);
            nEnsembles=length(inTransectIdx);
            btValid=transData.boatVel.btVel.validData(1,inTransectIdx);
            % Check that there is some valid BT data
            if nansum(btValid)>0
                wtU=transData.wVel.uProcessed_mps(:,inTransectIdx);
                wtV=transData.wVel.vProcessed_mps(:,inTransectIdx);
                if isempty(varargin)
                    ensDuration=transData.dateTime.ensDuration_sec(inTransectIdx);
                else
                    ensDuration=varargin{:};
                end %if 
                btU=transData.boatVel.btVel.uProcessed_mps(inTransectIdx);
                btV=transData.boatVel.btVel.vProcessed_mps(inTransectIdx);
                binSize=transData.depths.btDepths.depthCellSize_m(:,inTransectIdx);

                %% Compute flow speed and direction
                % Compute discharge weighted mean velocity components for the
                % purposed of computing the mean flow direction
                xprod=clsQComp.crossProduct(transData);
                q=clsQComp.dischargeMiddleCells(xprod,transData,ensDuration);
                wght=abs(q);
                se=nansum(nansum(wtU.*wght))./nansum(nansum((wght)));
                sn=nansum(nansum(wtV.*wght))./nansum(nansum((wght)));
                [dir, flowSpeedq]=cart2pol(se,sn);
                obj.flowDir_deg=rad2azdeg(dir);

                % Compute the area weighted mean velocity components for the
                % purposed of computing the mean flow speed.
                % SEEMS LIKE THE FLOW SPEED AND DIRECTION SHOULD BE HANDLED THE
                % SAME NOT DIFFERENTLY
                wghtarea=bsxfun(@times,bsxfun(@times,sqrt(btU.^2+btV.^2),binSize),ensDuration);
                idx=find(~isnan(wtU));
                se=nansum(wtU(idx).*wghtarea(idx))./nansum(wghtarea(idx));
                sn=nansum(wtV(idx).*wghtarea(idx))./nansum(wghtarea(idx));
                [dirA, obj.flowSpd_mps]=cart2pol(se,sn);
                flowDirA=rad2azdeg(dirA);       

                %% Compute moving-bed velocity and potential error in discharge
                % Compute closure distance and direction
                btX=nancumsum(btU.*ensDuration);
                btY=nancumsum(btV.*ensDuration);
                [dir, obj.distUS_m]=cart2pol(btX(end),btY(end));
                obj.mbDir_deg=rad2azdeg(dir);

                % Compute duration of test
                obj.duration_sec=nansum(ensDuration);

                % Compute the moving-bed velocity
                obj.mbSpd_mps=obj.distUS_m./obj.duration_sec;

                % Compute potential error in BT referenced discharge
                obj.percentMB=(obj.mbSpd_mps./(obj.flowSpd_mps+obj.mbSpd_mps)).*100;

                %% Assess invalid bottom track 
                % Compute percent invalid bottom track
                obj.percentInvalidBT=(nansum(~btValid)./length(btValid)).*100;

                % Determine if more than 9 consectutive seconds of invalid BT occured
                consectBTtime=zeros(1,nEnsembles);
                for n=2:nEnsembles
                    if btValid(n)==0
                        consectBTtime(n)=consectBTtime(n-1)+ensDuration(n);
                    else
                        consectBTtime(n)=0;
                    end
                end
                maxConsectBTTime=nanmax(consectBTtime);

                %% Evaluate compass calibration based on flow direction

                % Find apex of loop
                % Adapted from
                % http://www.mathworks.de/matlabcentral/newsreader/view_thread/164048
                L1=[btX(1) btY(1) 0];
                L2=[btX(end) btY(end) 0];
                for n=1:nEnsembles
                    P=[btX(n) btY(n) 0];
                    distance(n) = norm(cross(L2-L1,P-L1))/norm(L2-L1);
                end
                dmgidx=find(distance==max(distance),1,'First');      

                % Compute flow direction on outgoing part of loop
                uOut=wtU(:,1:dmgidx);
                vOut=wtV(:,1:dmgidx);
                wght=abs(q(:,1:dmgidx));
                se=nansum(uOut(:).*wght(:))./nansum(wght(:));
                sn=nansum(vOut(:).*wght(:))./nansum(wght(:));
                [dir, ~]=cart2pol(se,sn);
                flowDir1=rad2azdeg(dir);

                % Compute unweighted flow direction in each cell
                [dir, ~]=cart2pol(uOut,vOut);
                flowDirCell=rad2azdeg(dir);

                % Compute difference from mean and correct to +/- 180
                vDirCorr=flowDirCell-flowDir1;
                vDiridx=vDirCorr>180;
                vDirCorr(vDiridx)=360-vDirCorr(vDiridx);
                vDiridx=vDirCorr<-180;
                vDirCorr(vDiridx)=360+vDirCorr(vDiridx);

                % Number of valid weights
                idx2=find(~isnan(wght));
                nwght=length(idx2); 

                % Compute 95% uncertainty using weighted standard deviation
                uncert1=2.*sqrt(nansum(nansum(wght.*(vDirCorr).^2))./(((nwght-1)*nansum(nansum(wght)))./nwght))./sqrt(nwght);

                % Compute flow direction on returning part of loop
                uRet=wtU(:,dmgidx+1:end);
                vRet=wtV(:,dmgidx+1:end);
                wght=abs(q(:,dmgidx+1:end));
                se=nansum(uRet(:).*wght(:))./nansum(wght(:));
                sn=nansum(vRet(:).*wght(:))./nansum(wght(:));
                [dir, ~]=cart2pol(se,sn);
                flowDir2=rad2azdeg(dir);  

                % Compute unweighted flow direction in each cell
                [dir, ~]=cart2pol(uRet,vRet);
                flowDirCell=rad2azdeg(dir);

                % Compute difference from mean and correct to +/- 180
                vDirCorr=flowDirCell-flowDir2;
                vDiridx=vDirCorr>180;
                vDirCorr(vDiridx)=360-vDirCorr(vDiridx);
                vDiridx=vDirCorr<-180;
                vDirCorr(vDiridx)=360+vDirCorr(vDiridx);

                % Number of valid weights
                idx2=find(~isnan(wght));
                nwght=length(idx2);  

                % Compute 95% uncertainty using weighted standard deviation
                uncert2=2.*sqrt(nansum(nansum(wght.*(vDirCorr).^2))./(((nwght-1)*nansum(nansum(wght)))./nwght))./sqrt(nwght);

                % Compute and report difference in flow direction
                diffDir=abs(flowDir1-flowDir2);        
                if diffDir>180
                    diffDir=diffDir-360;
                end
                obj.compassDiff_deg=diffDir;
                uncert=uncert1+uncert2;

                % Compute potential compass error
              idx=find(~isnan(btX),1,'last');
                width=sqrt((btX(dmgidx)-btX(idx)./2).^2 + (btY(dmgidx)-btY(idx)./2).^2);
                compassError=(2*width.*sind(diffDir./2).*100)./(obj.duration_sec.*obj.flowSpd_mps);


                %% Apply error and warning criteria

                % Initialize message counter
                m=1;
                obj.testQuality='Good';
                velCriteria=0.012; % mps

                % Low water velocity
                if obj.flowSpd_mps<0.25
                    obj.messages{m}='WARNING: The water velocity is less than recommended minimum for this test and could cause the loop method to be inaccurate.  CONSIDER USING A STATIONARY TEST TO CHECK MOVING-BED CONDITIONS.'; 
                    m=m+1;
                    obj.testQuality='Warnings';
                end % if

                % Percent invalid bottom track
                if obj.percentInvalidBT>20
                    obj.messages{m}='ERROR: Percent invalid bottom track exceeds 20 percent. THE LOOP IS NOT ACCURATE. TRY A STATIONARY MOVING-BED TEST.';
                    m=m+1;
                    obj.testQuality='Errors';
                elseif obj.percentInvalidBT>5
                    obj.messages{m}='WARNING: Percent invalid bottom track exceeds 5 percent. Loop may not be accurate. PLEASE REVIEW DATA.';
                    m=m+1;
                    obj.testQuality='Warnings';
                end % if perBadBT

                % More than 9 consecutive seconds of invalid BT
                if maxConsectBTTime>9
                   obj.messages{m}='ERROR: Bottom track is invalid for more than 9 consecutive seconds. THE LOOP IS NOT ACCURATE. TRY A STATIONARY MOVING-BED TEST.';
                    m=m+1;
                    obj.testQuality='Errors';
                end
                
                if ~isempty(compassError) || ~isnan(diffDir)
                    if abs(compassError) > 5 && abs(diffDir)>3  && abs(diffDir)>uncert
                        obj.messages{m}='ERROR: Difference in flow direction between out and back sections of loop could result in a 5 percent or greater error in final discharge. REPEAT LOOP AFTER COMPASS CAL. OR USE A STATIONARY MOVING-BED TEST.';
                        m=m+1;
                        obj.testQuality='Errors';
                    end % if
                end
            else
                m=1;
                obj.messages{m}='ERROR: Loop has no valid bottom track data. REPEAT OR USE A STATIONARY MOVING-BED TEST.';
                m=m+1;
                obj.testQuality='Errors';
            end
            % If loop is valid then evaluate moving-bed condition
            if ~strcmp(obj.testQuality,'Errors')
                % Check minimum moving-bed velocity criteria
                if obj.mbSpd_mps>0.012
                    % Check that closure error is in upstream direction
                    if (abs(obj.flowDir_deg-obj.mbDir_deg)>135 && abs(obj.flowDir_deg-obj.mbDir_deg)< 225)
                        % Check if moving-bed speed is greater than 1%
                        % of the mean flow speed
                       if obj.percentMB>1
                           obj.messages{m}='Loop Indicates a Moving Bed -- Use GPS as reference. If GPS is unavailable or invalid use the loop method to correct the final discharge.';
                           m=m+1;
                           obj.movingBed='Yes';
                       else
                           obj.messages{m}='Moving Bed Velocity < 1% of Mean Velocity -- No Correction Recommended';
                           m=m+1;
                           obj.movingBed='No';
                       end % if
                    else
                        obj.messages{m}='ERROR: Loop closure error not in upstream direction. REPEAT LOOP or USE STATIONARY TEST';
                        m=m+1;
                        obj.testQuality='Errors';
                        obj.movingBed='Unknown';
                   end % if
                else
                    obj.messages{m}='Moving-bed velocity < Minimum moving-bed velocity criteria -- No correction recommended';
                    m=m+1;
                    obj.movingBed='No';
                end % if
            else
                obj.messages{m}='ERROR: Due to ERRORS noted above this loop is NOT VALID. Please consider suggestions.';
                m=m+1;
                obj.movingBed='Unknown';
            end % if
        end % loopTest
        
        function obj=stationaryTest(obj)
        % Processed the stationary moving-bed tests   
            %% Assign data from transect to local variables
            transData=obj.transect;
            inTransectIdx=transData.inTransectIdx;
            nEnsembles=length(inTransectIdx);
            btValid=transData.boatVel.btVel.validData(1,inTransectIdx);
            % Check to see that there is some valid bottom track data
            if nansum(btValid)>0
                wtU=transData.wVel.uProcessed_mps(:,inTransectIdx);
                wtV=transData.wVel.vProcessed_mps(:,inTransectIdx);
                ensDuration=transData.dateTime.ensDuration_sec(inTransectIdx);
                btU=transData.boatVel.btVel.uProcessed_mps(inTransectIdx);
                btV=transData.boatVel.btVel.vProcessed_mps(inTransectIdx);

                binDepth=transData.depths.btDepths.depthCellDepth_m(:,inTransectIdx);
                depthEns=transData.depths.(transData.depths.selected).depthProcessed_m(inTransectIdx);

                [nbU, nbV, unitNBU, unitNBV]=clsMovingBedTests.nearBedVelocity(wtU,wtV,depthEns,binDepth);

                % Compute bottom track parallel to water velocity
                unitNBVel=[unitNBU;unitNBV];
                btVel=[btU;btV]; 
                btVelUpStrm=-1.*dot(btVel,unitNBVel,1);
                btUpStrmDist=btVelUpStrm.*ensDuration;
                btUpStrmDistCum=nancumsum(btUpStrmDist);

                % Compute bottom track perpendicular to water velocity
                [nbVelAng, ~]=cart2pol(unitNBU,unitNBV);
                [nbVelUnitCS(:,1),nbVelUnitCS(:,2)]=pol2cart(nbVelAng+pi./2,ones(size(nbVelAng)));
                btVelCS=dot(btVel,nbVelUnitCS',1);
                btCSStrmDist=btVelCS.*ensDuration;
                btCSStrmDistCum=nancumsum(btCSStrmDist);

                % Compute cumulative mean moving bed velocity 
                validBTVelUpStrm=~isnan(btVelUpStrm);
                mbVel=nancumsum(btVelUpStrm)./nancumsum(validBTVelUpStrm);

                % Compute the average ensemble velocities corrected for moving bed
                if mbVel(end)>0
                    uCorrected=bsxfun(@plus,wtU,(unitNBVel(1,:).*btVelUpStrm));
                    vCorrected=bsxfun(@plus,wtV,(unitNBVel(2,:).*btVelUpStrm));
                else
                    uCorrected=wtU;
                    vCorrected=wtV;
                end
                %
                % Compute the mean of the ensemble magnitudes
                % Mean is computed using magnitudes because if a Streampro with no
                % compass is the data source the change in direction could be
                % either real change in water direction or an uncompensated turn of
                % the floating platform. This approach is the best compromise when
                % there is no compass or the compass is unreliable, which is often
                % why the stationary method is used. A weighted average is used
                % to account for the possible change in cell size within and
                % ensemble for the RiverRay and RiverPro.
                mag=sqrt(uCorrected.^2+vCorrected.^2);
                depthCellSize=transData.depths.btDepths.depthCellSize_m(:,inTransectIdx);
                depthCellSize(isnan(mag))=nan;
                magw=mag.*depthCellSize;
                avgVel=nansum(magw(:))./nansum(depthCellSize(:));
                potErrorPer=(mbVel(end)./(avgVel)).*100;
                if potErrorPer<0 
                    potErrorPer=0;
                end

                % Compute percent invalid bottom track
                obj.percentInvalidBT=(nansum(~btValid)./length(btValid)).*100;
                obj.distUS_m= btUpStrmDistCum(end);
                obj.duration_sec=nansum(ensDuration);
                obj.compassDiff_deg=[];
                obj.flowDir_deg=[];
                obj.mbDir_deg=[];
                obj.flowSpd_mps=avgVel; 
                obj.mbSpd_mps=mbVel(end);              
                obj.percentMB=potErrorPer; 
                obj.nearBedSpeed_mps=sqrt(nanmean(nbU).^2+nanmean(nbV).^2);
                obj.stationaryUSTrack=btUpStrmDistCum;
                obj.stationaryCSTrack=btCSStrmDistCum;
                obj.stationaryMBVel=mbVel;

                %% Quality check
                m=1;
                obj.testQuality='Good';
                % Check duration
                if obj.duration_sec<300
                    obj.messages{m}='WARNING - Duration of stationary test is less than 5 minutes';
                    m=m+1;
                    obj.testQuality='Warnings';
                end % if

                % Check validity of mean moving-bed velocity
                if obj.duration_sec>60
                    mbVelStd=nanstd(mbVel(end-30:end));
                    cov=mbVelStd./mbVel(end);
                    if cov>0.25 && mbVelStd>0.03
                        obj.messages{m}='WARNING - Moving-bed velocity may not be consistent. Average maybe inaccurate.';
                        m=m+1;
                        obj.testQuality='Warnings';
                    else % if
                        cov=nan;
                    end
                end

                % Check percentage of invalid BT data
                %if sum(btValid)<=150
                if nansum(ensDuration(validBTVelUpStrm))<=120

                    obj.messages{m}='ERROR � Total duration of valid BT data is insufficient for a valid test.';
                    m=m+1;
                    obj.testQuality='Errors';
                    obj.movingBed='Unknown';
                elseif obj.percentInvalidBT> 10
                    obj.messages{m}='WARNING - Number of ensembles with invalid bottom track exceeds 10%';
                    m=m+1;
                    obj.testQuality='Warnings';
                end % if
                
                % Determine if the test indicates a moving bed
                if ~strcmp(obj.testQuality,'Errors')
                    if obj.percentMB>1
                        obj.movingBed='Yes';
                    else
                        obj.movingBed='No';
                    end % if moving bed
                end

            else
                m=1;
                obj.messages{m}='ERROR - Stationary moving-bed test has no valid bottom track data.';
                m=m+1;
                obj.testQuality='Errors';
                obj.movingBed='Unknown';
            end
           
                 
            
        end % stationaryTest
        
        function obj=autoUse2Correct(obj,varargin)
        % Applies logic to determine which moving-bed tests should be used
        % for correcting bottom track referenced discharges with moving-bed
        % conditions.
        %
        % INPUT:
        % 
        % obj: object of clsMovingBedTests
        %
        % OUTPUT:
        %
        % obj: object of clsMovingBedTests
            
            % Initialize variables
            for n=1:length(obj)
                obj(n).use2Correct=false;
                obj(n).selected=false;
            end
            

                % Select moving-bed tests
             
                % Valid test according to user
                lidxUser=[obj.userValid]==1;
                % Valid test according to quality assessment
                lidxNoErrors=~strcmp({obj.testQuality},'Errors');
                % Identify type of tests
                testType={obj.type};
                lidxStationary=strcmp(testType, 'Stationary'); 
                lidxLoop=strcmp(testType, 'Loop');
                % Combine
                lidxValidLoop=all([lidxUser;lidxNoErrors;lidxLoop]);
                lidxValidStationary=all([lidxUser;lidxNoErrors;lidxStationary]);
                
                % Check flow speed
                flowSpeed=[obj(lidxLoop).flowSpd_mps];
                
%                  flowSpeed=[obj.flowSpd_mps];
                lidxFlowSpeed=flowSpeed>0.25;

                % Determine if there are valid loop tests
                if any(lidxValidLoop) && any(lidxFlowSpeed) 
                    lidxLoops2Select=(double(lidxValidLoop(lidxLoop))+double(lidxFlowSpeed))>1;
                    % Select last loop
                    idxSelect=find(lidxLoops2Select==1,1,'Last');
                    if isempty(idxSelect)
                        idxSelect=length(obj);
                    else
                        idx=find(lidxLoop==1);
                        idxSelect=idx(idxSelect);
                    end
                    obj(idxSelect).selected=true;
                    % If there is a moving-bed set use2Correct
                    if strcmp(obj(idxSelect).movingBed,'Yes')
                        obj(idxSelect).use2Correct=true;
                    end
                    
                % If there are no valid loops look for valid stationary
                % tests
                elseif any(lidxValidStationary)
                    % Select all valid stationary tests
                    for n=1:length(lidxValidStationary)
                        if lidxValidStationary(n)
                            obj(n).selected=true;
                        end
                    end
                    % Determine if any stationary tests have a moving bed
                    idxMB=find(strcmp({obj(lidxValidStationary).movingBed},'Yes'));
                    if ~isempty(idxMB)
                        % Use all stationary tests if there is a moving bed
                        for n=1:length(lidxValidStationary)
                            if lidxValidStationary(n)
                                obj(n).use2Correct=true;
                            end
                        end
                    end
                elseif any(lidxValidLoop)
                    % Select last loop
                    idxSelect=find(lidxValidLoop==1,1,'Last');
                    obj(idxSelect).selected=true;
                    % If there is a moving-bed set use2Correct
                    if strcmp(obj(idxSelect).movingBed,'Yes')
                        obj(idxSelect).use2Correct=true;
                    end
                end
                
                
            % If the navigation reference for discharge computations is set
            % GPS then none of test should be used for correction. The
            % selected test should be used to determine if there is a valid
            % moving-bed and a moving-bed condition.
            if isempty(varargin)
                ref='BT';
            else
                ref=varargin{1};
            end
            
            if ~strcmp(ref,'BT')   
                for n=1:length(obj)
                    obj(n).use2Correct=false;
                end
            end
                    
            
%             % Determine if there is a moving-bed condition
%             
%             idxMB=find(strcmp({obj(idxValid).movingBed},'Yes'));
%             
%             % Determine which test should be used to correct BT referenced
%             % discharges.
%             if ~isempty(idxMB) && strcmp(ref,'BT')
%             
%                 % Index invalid moving-bed tests
%                 idxErrors=find(strcmp({obj.testQuality},'Errors'));
%                 idxUser=find([obj.userValid]==0);
%                 
% 
%                 % Do not use invalid test
%                 testType={obj.type};
%                 testType(idxErrors)={''};
%                 testType(idxUser)={''};
% 
%                 % Determine indices of usable stationary and loop tests
%                 idxStationary=find(strcmp(testType, 'Stationary')); 
%                 idxLoop=find(strcmp(testType, 'Loop'));
% 
%                 % If only stationary use all
%                 if isempty(idxLoop)
%                     for n=1:length(idxStationary)
%                         obj(idxStationary(n)).use2Correct=true;
%                     end
%                 else
%                     if isempty(idxStationary)
%                         % Use only 1 loop. Use the last loop with no warnings
%                         % or the last loop if they all have warnings
%                         idxValidLoop=find(strcmp({obj(idxLoop).testQuality},'Good'));
%                         if ~isempty(idxValidLoop) && strcmpi(obj(idxValidLoop(end)).movingBed,'Yes')
%                                 obj(idxValidLoop(end)).use2Correct=true;
%                         elseif strcmpi(obj(idxLoop(end)).movingBed,'Yes')
%                             obj(idxLoop(end)).use2Correct=true;
%                         end % if idxValidLoop
%                     else
%                         % If both stationary and loop tests are present, use a
%                         % valid loop. I there is no valid loop then use
%                         % stationary tests.
%                         idxValidLoop=find(strcmp({obj(idxLoop).testQuality},'Good'));
%                         if ~isempty(idxValidLoop)
%                             obj(idxValidLoop(end)).use2Correct=true;
%                         else
%                             for n=1:length(idxStationary)
%                                 obj(idxStationary(n)).use2Correct=true;
%                             end
%                         end % if idxValidLoop
%                     end % if idxStationary
%                 end % if idxLoop
%             end % if idxMB
        end % autoUse2Correct
        
        function obj=setValid(obj,setting)
            nTests=length(setting);
            for n=1:nTests
                obj(n).userValid=setting(n);
            end % for
        end % setValid
        
        function obj=setUse(obj,setting)
            nTests=length(setting);
            for n=1:nTests
                obj(n).use2Correct=setting(n);
            end % for
        end % setUse
        
        function obj=setProperty(obj,property,setting)
            obj.(property)=setting;
        end
    end % methods
    
    methods (Static)
        
        function [nbU, nbV, unitNBU, unitNBV]=nearBedVelocity(u,v,depth,binDepth)            
        % Compute near bed velocities
            % Compute z near bed as 10% of depth
            % ----------------------------------
            zNearBed=depth.*0.1;
            %
            % Begin computing near-bed velocities
            % -----------------------------------
            nEnsembles=size(u,2);
            nbU=nan(1,nEnsembles);
            nbV=nan(1,nEnsembles);
            unitNBU=nan(1,nEnsembles);
            unitNBV=nan(1,nEnsembles);
            zDepth=nan(1,nEnsembles);
            uMean=nan(1,nEnsembles);
            vMean=nan(1,nEnsembles);
            speedNearBed=nan(1,nEnsembles);
            for n=1:nEnsembles
                idx=find(~isnan(u(:,n)),2,'last');
                if ~isempty(idx)
                    % Compute near-bed velocity
                    zDepth(n)=depth(n)-nanmean(binDepth(idx,n));
                    uMean(n)=nanmean(u(idx,n));
                    vMean(n)=nanmean(v(idx,n));
                    nbU(n)=(uMean(n)./(zDepth(n).^(1/6)))*(zNearBed(n)^(1/6));
                    nbV(n)=(vMean(n)./(zDepth(n).^(1/6)))*(zNearBed(n)^(1/6));
                    speedNearBed(n)=sqrt(nbU(n).^2+nbV(n).^2);
                    unitNBU(n)=nbU(n)/speedNearBed(n);
                    unitNBV(n)=nbV(n)/speedNearBed(n);
                end % if
            end % for n
        end % nearBedVelocity
        
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
            if ~isempty(structIn)
                temp=clsMovingBedTests();
                obj=setProperty(obj,'mbTests',temp);
                numTests=length(structIn);
                for num=1:numTests
                    % Create object

                    % Get variable names from structure
                    names=fieldnames(structIn(num));
                    % Set properties to structure values
                    nMax=length(names);
                    for n=1:nMax
                        if isstruct(structIn(num).(names{n}))
                            % Create object
                            temp=clsMeasurement();
                            temp=clsTransectData.reCreate(temp,structIn(num).transect);
                            obj.mbTests(num)=setProperty(obj.mbTests(num),'transect',temp.transects);
                        else
                            % Assign property
                            obj.mbTests(num).(names{n})=structIn(num).(names{n});
                        end
                    end % for n
                end % for num
            end % isempty
        end % reCreate
    end % methods static
        
end % class

