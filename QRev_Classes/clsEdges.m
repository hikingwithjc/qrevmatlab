classdef clsEdges
% Sets the general properties that apply to both edges and
% contains objects for both the left and right edges.
%
% Originally developed as part of the QRev processing program
% Programmer: David S. Mueller, Hydrologist, USGS, OSW, dmueller@usgs.gov
% Other Contributors: none
% Latest revision: 3/13/2015

    properties (SetAccess=private)
        recEdgeMethod % Method used to determine coef for rec. edge 'Fixed', 'Variable'
        velMethod % Method used to compute the velocity used 'MeasMag', 'VectorProf'
        left % Object of clsEdgeData for left edge
        right % Object of clsEdgeData for right edge
    end % properties
    
    methods
        
        function obj=clsEdges(recEdgeMethod,velMethod)
        % Constructor method sets general edge methods
        %
        % INPUT:
        %
        % recEdgeMethod: rectangular edge method (Fixed, Variable)
        %
        % velMethod: method used to compute the velocity (MeasMag,
        % VectorProf)
        %
        % OUTPUT:
        %
        % obj: object of clsEdges
            % Allow creation of object with no input
            if nargin > 0        
                % Set properties
                obj.recEdgeMethod=recEdgeMethod;
                obj.velMethod=velMethod;
            end % nargin
        end % constructor
        
        function obj=changeProperty(obj,prop,setting,varargin)
        % Change edge property
        %
        % INPUT:
        %
        % obj: object of clsEdges
        %
        % prop: name of property
        %
        % setting: property setting
        %
        % varargin: edge to change (left, right)
        %   
        % OUTPUT:
        %
        % obj: object of clsEdges
        
            % Determine if property to be set is part of clsEdges or
            % clsEdgeData
            if isempty(varargin)
                % Set property
                obj.(prop)=setting;
            else
                % Set property for appropriate edge of clsEdgeData
                obj.(varargin{1})=changeProperty(obj.(varargin{1}),prop,setting);
            end
        end % changeProperty
        
        function obj=createEdge(obj,edgeLoc,type, dist, varargin)
        % Create clsEdge properity which is an object of clsEdgeData for
        % each edge.
        %
        % INPUT:
        %
        % obj: object of clsEdges
        %
        % edgeLoc: left or right
        %
        % type: type of edge (Triangular, Rectangular, Custom, User Q)
        % 
        % dist: distance to shore
        %
        % varargin: value of coefficient for Custom or discharge for User Q
        
            % Create object
            obj.(edgeLoc)=clsEdgeData (type, dist, varargin{:});
            
        end % createEdge
        
        function obj=reCreateEdge(obj,edgeLoc)
            obj.(edgeLoc)=clsEdgeData();
        end
    end % methods
       
    methods(Static)
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
            % Create object
            obj.edges=clsEdges();
            % Get variable names from structure
            names=fieldnames(structIn);
            % Set properties to structure values
            nMax=length(names);
            for n=1:nMax
                if isstruct(structIn.(names{n}))
                    % Create object
                    obj.edges=reCreateEdge(obj.edges,names{n});
                    obj.edges.(names{n})=clsEdgeData.reCreate(obj.edges.(names{n}),structIn.(names{n}));
                else
                    % Assign property
                    obj.edges=changeProperty(obj.edges,names{n},structIn.(names{n}));
                end
            end % for
        end % reCreate
    end % static methods  
        
end % class

