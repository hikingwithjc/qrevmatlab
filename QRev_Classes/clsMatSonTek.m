classdef clsMatSonTek
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        fileName
        BottomTrack
        Compass
        GPS
        Processing
        RawGPSData
        Setup
        SiteInfo
        Summary
        System
        SystemHW
        Transformation_Matrices
        WaterTrack
    end
    
    methods
        
        function obj=clsMatSonTek(fileNames)
        
            % Allow for no arguments
            if nargin > 0
                %Preallocate object array
                numFiles=max(size(fileNames));
                obj(numFiles)=clsMatSonTek();
                % Load data files and create object arrays
                for n=1:numFiles
                    fullName=fileNames{n};
                    idx=find(fullName=='\',1,'last');
                    if isempty(idx)
                        idx=0;
                    end % if isempty
                    updateStatus(['Reading ', fullName(idx+1:end)]);
                    obj(n).fileName=fileNames{n};
                    load (fullName);
                    
                    % Change all single data types to double
                    names=fieldnames(Setup);
                    kNames=length(names);
                    for k=1:kNames
                        if strcmp(class(Setup.(names{k})),'single')
                            Setup.(names{k})=double(Setup.(names{k}));
                        end
                    end
                    if strcmp(BottomTrack.Units.BT_Depth,'m')
                        obj(n).BottomTrack=BottomTrack;
                        obj(n).GPS=GPS;
                        obj(n).Setup=Setup;
                        obj(n).Summary=Summary;
                        obj(n).System=System;
                        obj(n).WaterTrack=WaterTrack;
                    else
                        obj(n).BottomTrack=clsMatSonTek.unitHandler(BottomTrack);
                        obj(n).GPS=clsMatSonTek.unitHandler(GPS);
                        obj(n).Setup=clsMatSonTek.unitHandler(Setup);
                        obj(n).Summary=clsMatSonTek.unitHandler(Summary);
                        obj(n).System=clsMatSonTek.unitHandler(System);
                        obj(n).WaterTrack=clsMatSonTek.unitHandler(WaterTrack);
                    end % if units 
                    obj(n).Processing=Processing;
                    obj(n).RawGPSData=RawGPSData;
                    if exist('SiteInfo')
                        obj(n).SiteInfo=SiteInfo;
                    end    
                    if exist('SystemHW')
                        obj(n).SystemHW=SystemHW;
                    end
                    obj(n).Transformation_Matrices=Transformation_Matrices;
                    if exist('Compass','var')
                        obj(n).Compass=Compass;
                    end
                end % for n
            end % if nargin
        end % Constructor                       
    end % Methods   
    
    methods (Static)
        function structOut=unitHandler(structIn)
        % Function to convert SonTek Matlab output file from English units to SI
    
            % Determine variable names
            structOut=structIn;
            varNames=fieldnames(structIn.Units);
            nNames=length(varNames);

            % Loop through variable names and apply appropriate corrections
            for n=1:nNames
                switch structIn.Units.(varNames{n})
                    case 'ft'
                        structOut.Units.(varNames{n})='m';
                        structOut.(varNames{n})=structIn.(varNames{n}).*0.3048;
                    case 'ft/s'
                        structOut.Units.(varNames{n})='m/s';
                        structOut.(varNames{n})=structIn.(varNames{n}).*0.3048;
                    case 'degF'
                        structOut.Units.(varNames{n})='degC';
                        structOut.(varNames{n})=(structIn.(varNames{n})-32).*(5./9);
                    case 'cfs'
                        structOut.Units.(varNames{n})='cms';
                        structOut.(varNames{n})=structIn.(varNames{n}).*0.3048.^3;
                end % switch
            end
        end % unitHandler
    end % static methods
end % Class

