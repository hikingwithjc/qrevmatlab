classdef clsHeadingData
% This class store and manipulates heading data.
    
    properties
        data % corrected heading data
        originalData % original uncorrected heading data
        source % source of heading data (internal, external)
        magVar_deg % magnetic variation for these heading data
        magVarOrig_deg % Orignial magnetic variation
        alignCorrection_deg % alignment correction to align compass with instrument
        magError % Percent change in mean magnetic field from calibration
        pitchLimit
        rollLimit
    end
    
    methods
        function obj=clsHeadingData(dataIn,sourceIn,varargin)
        % Constructor for clsHeadingData  
            % Allow creation of object with no input
            if nargin > 0       
                varargin=varargin{1};
                obj.originalData=dataIn;
                obj.source=sourceIn;
                obj.magVar_deg=varargin{1};
                obj.magVarOrig_deg=varargin{1};
                obj.alignCorrection_deg=varargin{2};
                if strcmpi(sourceIn,'Internal') | strcmpi(sourceIn, 'Int. Sensor')
                    % Correct the original data for the magvar
                    obj.data=obj.originalData+obj.magVar_deg; 
                else
                    obj.data=obj.originalData+obj.alignCorrection_deg; 
                end
                obj.data=clsHeadingData.fixUpperLimit(obj.data);
                obj.data=clsHeadingData.interpHeading(obj.data);
                if length(varargin)>2
                    obj.magError=varargin{3};
                end
            end % nargin
        end % constructor
    
        function obj=setMagVar(obj,magVar,hSource)
        % Applies a new magvar to the object
            obj.magVar_deg=magVar;
            if strcmp(hSource,'internal')
                obj.data=obj.originalData+obj.magVar_deg;
                obj.data=clsHeadingData.fixUpperLimit(obj.data);
            end
        end % changeMagVar
        
        function obj=setAlignCorrection(obj,alignCorrection,hSource)
        % Applies a new alignment correction to the object
            obj.alignCorrection_deg=alignCorrection;
            if strcmp(hSource,'external')
                obj.data=obj.originalData+obj.alignCorrection_deg;
                obj.data=clsHeadingData.fixUpperLimit(obj.data);
            end
        end % changeAlignCorrection
        
        function obj=setPRLimit(obj,type,limits)
            obj.(type)=limits;
        end
    end % methods
    
    methods (Static)
        function heading=fixUpperLimit(heading)
            idx=find(heading>360);
            if ~isempty(idx)
                heading(idx)=heading(idx)-360;
            end
            idx=find(heading<0);
            if ~isempty(idx)
                heading(idx)=heading(idx) + 360;
            end
        end % fixUpperLimit
        
        function obj=reCreate(obj,structIn,type)
        % Creates object from structured data of class properties
            % Create object
            obj.(type)=clsHeadingData();
            if isstruct(structIn)
                % Get variable names from structure
                names=fieldnames(structIn);
                % Set properties to structure values
                nMax=length(names);
                for n=1:nMax
                     obj.(type).(names{n})=structIn.(names{n});
                end % for n
            else
                obj.(type)=structIn;
            end
        end % reCreate
        
        function heading=interpHeading(heading)
        % Interpolate invalid heading. Use linear interpolation if there
        % are valid values on either side of the invalid heading. If the
        % invalid heading occurs at the beginning of the time series back
        % fill using the 1st valid. If the invalid heading occurs at the
        % end of the time series forward fill with the last valid heading.
            % Find ensembles with invalid heading
            idx=find(isnan(heading));

            % If invalid headings exist, interpolate values
            if ~isempty(idx)
                % Identify first and last valid headings
                firstIdx=find(~isnan(heading),1,'first');
                lastIdx=find(~isnan(heading),1,'last');
                nIdx=length(idx);
                
                % Process each invalid heading
                for n=1:nIdx
                    % Identify ensembles with valid headings before and
                    % after invalid heading
                    beforeIdx=find(~isnan(heading(1:idx(n))),1,'last');
                    afterIdx=find(~isnan(heading(idx(n):end)),1,'first')+idx(n)-1;
                    
                    % If invalid heading is at beginning back fill
                    if isempty(beforeIdx)
                        heading(idx(n))=heading(firstIdx);
                    % If invalid heading is at end forward fill
                    elseif isempty(afterIdx)
                        heading(idx(n))=heading(lastIdx);
                    % If invalid heading is in middle interpolate
                    else
                        
                        if abs(heading(beforeIdx) - heading(afterIdx)) <180
                            c=0;
                        else
                            c=360;
                        end
                        heading(idx(n))=(((heading(afterIdx)-heading(beforeIdx)+c)./(beforeIdx-afterIdx)).*(beforeIdx-idx(n)))+heading(beforeIdx);
                        if heading(idx(n))>360
                            heading(idx(n))=heading(idx(n))-360;
                        end
%                         test(1)=heading(beforeIdx)>180;
%                         test(2)=heading(afterIdx)>180;
%                         if ~xor(test(1),test(2))
%                             c=0;
%                         elseif test(1)
%                             c=360;
%                         elseif test(2)
%                             c=-360;
%                         end
%                         heading(idx(n))=(((heading(afterIdx)-heading(beforeIdx)+c)./(beforeIdx-afterIdx)).*(beforeIdx-idx(n)))+heading(beforeIdx);
%                         if heading(idx(n))>360
%                             heading(idx(n))=heading(idx(n))-360;
%                         end
                    end
                end
            end
        end % interpHeading
    end
end % class

