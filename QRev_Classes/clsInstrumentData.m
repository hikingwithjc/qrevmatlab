classdef clsInstrumentData
% Container for characteristics of the ADCP used to make the measurement
    
    properties
        serialNum  % serial number of ADCP
        manufacturer  % manufacturer of ADCP (SonTek, TRDI)
        model  % model of ADCP (Rio Grande, StreamPro, RiverRay, M9, S5)
        firmware % firmware version
        frequency_hz  % frequency of ADCP (could be "Multi")
        beamAngle_deg  % angle of beam from vertical
        beamPattern  % pattern of beams (concave or convex)
		tMatrix % object of clsTransformationMatrix
        configurationCommands % configuration commands sent to ADCP
    end % properties
    
    methods
        
        function obj=clsInstrumentData(manufacturer, varargin)
        % This function constructs the clsInstrumentData object from the
        % mmt and pd0 files for TRDI or a mat file for SonTek.
            % Allow creation of object with no input
            if nargin > 0        
                obj.manufacturer=manufacturer;
                % Process TRDI data
                % -----------------
                if strcmpi(manufacturer,'TRDI')
                    mmt=varargin{1};
                    pd0=varargin{2};
                    n=varargin{3};
                    config='fieldConfig';
                    if length(varargin)>3
                        if strcmpi(varargin{4},'MB')
                            config='mbtFieldConfig';                        
                        end
                    end

                    % Instrument frequency
                    obj.frequency_hz=pd0.Inst.freq(1);

                    % Firmware
                    obj.firmware=pd0.Inst.firmVer(1);

                    % Instrument beam angle and pattern
                    obj.beamAngle_deg=pd0.Inst.beamAng(1);
                    obj.beamPattern=pd0.Inst.pat(1,1:end);

                    % Instrument characteristics
                    obj.serialNum=mmt.siteInfo.ADCPSerialNmb;
                    if isnumeric(obj.firmware)
                        modelSwitch=floor(obj.firmware);
                    else
                        modelSwitch=str2double(obj.firmware(1:2));
                    end
                    switch modelSwitch
                        case 10
                            obj.model='Rio Grande';
                            obj.configurationCommands=['Fixed';mmt.(config).Fixed_Commands(n,:)';...
                                'Wizard';mmt.(config).Wizard_Commands(n,:)';...
                                'User';mmt.(config).User_Commands(n,:)'];

                        case 31
                            obj.model='StreamPro';
                            obj.frequency_hz=2000;
                            obj.configurationCommands=['Fixed';mmt.(config).Fixed_Commands_StreamPro(n,:)';...
                                'Wizard';mmt.(config).Wizard_Commands(n,:)';...
                                'User';mmt.(config).User_Commands(n,:)'];

                        case 44
                            obj.model='RiverRay';
                            obj.configurationCommands=['Fixed';mmt.(config).Fixed_Commands_RiverRay(n,:)';...
                                'Wizard';mmt.(config).Wizard_Commands(n,:)';...
                                'User';mmt.(config).User_Commands(n,:)'];

                        case 56
                            obj.model='RiverPro';
                            if pd0.Cfg.nBeams<5
                                if isfield(mmt.qaqc,'RG_Test')
                                    idx=findstr(mmt.qaqc.RG_Test{1},'RioPro');
                                    if ~isempty(idx)
                                        obj.model='RioPro';
                                    end
                                end
                            end
                            if ~isfield(mmt.(config),'Fixed_Commands_RiverPro')
                                Fixed_Commands=' ';
                            else
                                Fixed_Commands=mmt.(config).Wizard_Commands(n,:)';
                            end
                            if isfield(mmt.(config),'Wizard_Commands')
                                Wizard_Commands=mmt.(config).Wizard_Commands(n,:)';
                            else
                                Wizard_Commands=' ';
                            end
                            if isfield(mmt.(config),'User_Commands')
                                User_Commands=mmt.(config).User_Commands(n,:)';
                            else
                                User_Commands=' ';
                            end
                            obj.configurationCommands=['Fixed';Fixed_Commands;...
                                'Wizard';Wizard_Commands;...
                                'User';User_Commands];


                    end % switch   

                    % Obtain transformation matrix from one of the available sources
                    if ~isnan(pd0.Inst.tMatrix(1,1))
                        obj.tMatrix=clsTransformationMatrix('TRDI','pd0',pd0);
                    elseif strcmp(obj.model,'RiverRay')
                        obj.tMatrix=clsTransformationMatrix('TRDI',obj.model,'Nominal');
                    else
                        if isstruct(mmt.qaqc)
                            if isfield(mmt.qaqc,'RG_Test_TimeStamp')
                                idx=findstr(mmt.qaqc.RG_Test{1},obj.model);
                                if ~isempty(idx)
                                    obj.tMatrix=clsTransformationMatrix('TRDI',obj.model,mmt.qaqc.RG_Test{1});
                                else
                                    obj.tMatrix=clsTransformationMatrix('TRDI',obj.model,'Nominal');
                                end
                            elseif isfield(mmt.qaqc,'Compass_Cal_Timestamp') 
                                idx=findstr(mmt.qaqc.Compass_Cal_Test{1},obj.model);
                                if ~isempty(idx)
                                    obj.tMatrix=clsTransformationMatrix('TRDI',obj.model,mmt.qaqc.Compass_Cal_Test{1});
                                else
                                    obj.tMatrix=clsTransformationMatrix('TRDI',obj.model,'Nominal');
                                end
                            elseif isfield(mmt.qaqc,'Compass_Eval_Timestamp') 
                                idx=findstr(mmt.qaqc.Compass_Eval_Test{1},obj.model);
                                if ~isempty(idx)
                                    obj.tMatrix=clsTransformationMatrix('TRDI',obj.model,mmt.qaqc.Compass_Eval_Test{1});
                                else
                                    obj.tMatrix=clsTransformationMatrix('TRDI',obj.model,'Nominal');
                                end
                            end % isfield
                        else
                            obj.tMatrix=clsTransformationMatrix('TRDI',obj.model,'Nominal');
                        end % isstruct
                    end



                % Process SonTek data
                % -------------------
                elseif strcmpi(manufacturer,'SonTek')
                    RS=varargin{1};
                     % Instrument frequency
                    obj.frequency_hz=RS.Transformation_Matrices.Frequency;

                    % Firmware
                    if ~isempty(RS.SystemHW)
                        revision=num2str(RS.SystemHW.FirmwareRevision);
                        if length(revision)<2
                            revision=['0',revision];
                        end
                        obj.firmware=[num2str(RS.SystemHW.FirmwareVersion),'.',revision];
                    else
                        obj.firmware='';
                    end

                    % Transformation matrices
                    obj.tMatrix=clsTransformationMatrix('SonTek',RS.Transformation_Matrices.Matrix);

                    % Instrument beam angle and pattern
                    obj.beamAngle_deg=25;
                    obj.beamPattern='Convex';

                    % Instrument serial number
                    obj.serialNum=RS.System.SerialNumber;

                    % Instrument model
                    if obj.frequency_hz(3)>0
                        obj.model='M9';
                    else
                        obj.model='S5';
                    end % if frequency_hz
                    obj.configurationCommands=[];
                end % if manufacturer
            end % nargin
        end % constructor
          
    end % methods
    
    methods (Static)
        function obj=reCreate(obj,structIn)
        % Creates object from structured data of class properties
            % Create object
            obj.adcp=clsInstrumentData();
            % Get variable names from structure
            names=fieldnames(structIn);
            % Set properties to structure values
            nMax=length(names);
            for n=1:nMax
                 if isstruct(structIn.(names{n}))
                     obj.adcp.tMatrix=clsTransformationMatrix.reCreate(structIn.(names{n}));
                 else
                    obj.adcp.(names{n})=structIn.(names{n});
                 end
            end
        end
    end % static methods
    
end % class

