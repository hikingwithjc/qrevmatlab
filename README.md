

# QRev <img src="QRev_Main.png" alt="" align="right" border="0" height="400" width="600">

**QRev** is a Matlab program developed by OSW to to compute the discharge from a moving-boat ADCP measurement using data collected with any of the Teledyne RD Instrument (TRDI) or SonTek bottom tracking ADCPs. QRev improves the consistency and efficiency of processing streamflow measurements by providing:

* Automated data quality checks with feedback to the user
* Automated data filtering
* Automated application of extrap, LC, and SMBA algorithms
* Consistent processing algorithms independent of the ADCP used to collect the data
* Improved handing of invalid data
* An estimated uncertainty to help guide the user in rating the measurement


**For a full description and instructions on the use of QRev click** **[HERE](https://hydroacoustics.usgs.gov/movingboat/QRev.shtml)** **to view the QRev page on the USGS Office of Surface Water Hydroacoustics website.**

***

# Development
**QRev** is an active project and is undergoing continuous development. Versions available on this repository may be versions that are being tested. If you would like to contribute, you will need to register for a user ID to log into my.usgs.gov/bitbucket and coordinate any development with the project coordinator, David Mueller, USGS, Office of Surface Water <dmueller@usgs.gov>.

## Requirements and Dependencies
### Source Code

QRev is written using Matlab 2015b (version 8.6) and makes use of the Statistics and Machine Learning Toolbox (version 10.1) and the Curve Fitting Toolbox (version 3.5.2), which are available at a cost from Mathworks. All other dependencies are included in the repository.

### Executable
Executables for versions that are in development may be available from this repository. Executable versions being used by the USGS are available from the [QRev](https://hydroacoustics.usgs.gov/movingboat/QRev.shtml) page on the USGS Office of Surface Water Hydroacoustics website.

Instructions for using the executable files are available [HERE](https://hydroacoustics.usgs.gov/movingboat/QRev.shtml#Installation).


## Bugs
In order to provide support for QRev and to provide an efficient means to communicate with users and allow users an efficient and organized means of providing suggestions and comments, you are encouraged to register for the OSW Hydroacoustic Forum. In the forum you will find a "QRev" board under Hydroacoustics Moving-Boat Deployments. Open the QRev board and click "Notify" to automatically receive emails on any bug fixes or issues identified with QRev. This is the only way of being automatically notified if there has been an identified problem or if a new version has been released. You are also encouraged to report any problems you encounter with QRev and attach the associated files so that any identified problem can be resolved. To access the OSW Hydroacoustics Forums you must be a registered user of the forums.

[Register for access to OSW Hydroacoustics Forums](https://hydroacoustics.usgs.gov/software/Forum_Reg1.html)

OSW Hydroacoustics Forums for [Registered Users](https://simon.er.usgs.gov/smf/index.php?board=53)

## Disclaimer
This software has been approved for release by the U.S. Geological Survey (USGS). Although the software has been subjected to rigorous review, the USGS reserves the right to update the software as needed pursuant to further analysis and review. No warranty, expressed or implied, is made by the USGS or the U.S. Government as to the functionality of the software and related material nor shall the fact of release constitute any such warranty. Furthermore, the software is released on condition that neither the USGS nor the U.S. Government shall be held liable for any damages resulting from its authorized or unauthorized use.

## License

Copyright / License - CC0 1.0: The person who associated a work with this deed has dedicated the work to the public domain by waiving all of his or her rights to the work worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law. You can copy, modify, distribute and perform the work, even for commercial purposes, all without asking permission. 

In no way are the patent or trademark rights of any person affected by CC0, nor are the rights that other persons may have in the work or in how the work is used, such as publicity or privacy rights.

Unless expressly stated otherwise, the person who associated a work with this deed makes no warranties about the work, and disclaims liability for all uses of the work, to the fullest extent permitted by applicable law.

When using or citing the work, you should not imply endorsement by the author or the affirmer.

Publicity or privacy: The use of a work free of known copyright restrictions may be otherwise regulated or limited. The work or its use may be subject to personal data protection laws, publicity, image, or privacy rights that allow a person to control how their voice, image or likeness is used, or other restrictions or limitations under applicable law.

Endorsement: In some jurisdictions, wrongfully implying that an author, publisher or anyone else endorses your use of a work may be unlawful.

3rd party code is covered by the copyright and license associated with those codes.

## Author
David S Mueller  
U.S. Geological Survey  
9818 Bluegrass Parkway  
Louisville, KY  
<dmueller@usgs.gov>